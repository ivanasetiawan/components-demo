module.exports = function(grunt) {

    grunt.initConfig(require('./src/components/app-icons/icon-task-config'));
    grunt.loadNpmTasks('grunt-grunticon');
    grunt.loadNpmTasks('grunt-svgmin');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-clean');

    grunt.registerTask('icons', ['copy:temp', 'svgmin', 'grunticon', 'copy:loader', 'clean']);
};
