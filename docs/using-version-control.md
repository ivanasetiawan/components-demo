# Using version control

## Branches

[Github Flow](https://guides.github.com/introduction/flow/) is used. tldr; feature branches from and to Master.

todo: branch naming update

## Commit messages

The commit messages should be in a consistent format so they are easy to understand, meaningful and can be parsed by scripts automatically.

The messages should be in [convential changelog format](https://github.com/ajoslin/conventional-changelog/blob/v0.0.17/CONVENTIONS.md):

* starts with an action type (eg. `feat` for feature enhancements, `fix` for bug fix)
* the scope (in between brackets), which is typically the name of the view or component
* followed by the subject (eg. what is the feature enhancement?)
* the [JIRA issue key](https://confluence.atlassian.com/display/JIRACLOUD/Processing+JIRA+issues+with+commit+messages) when applicable
* and optionally an additional description in the body with things like breaking changes.

Example: `feat(object-map): lazy load gmaps when map section is in view. OBJ-1337`

Make sure to [read the conventions](https://github.com/ajoslin/conventional-changelog/blob/v0.0.17/CONVENTIONS.md).

Note: JIRA is able to find the issue key as long as its anywhere in the commit message. It does not need to be at the start of it.
