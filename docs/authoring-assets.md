# Authoring assets

To encourage module based development the front-end guide allows you to place assets on the app, view and component level. All assets will be bundled into `dist/assets/` during the build process. All you need to do is place them in a directory called `assets/`:

	src/assets/*						 	>> dist/assets/
	src/components/my-component/assets/*	>> dist/assets/components/my-component/
	src/components/my-view/assets/*			>> dist/assets/views/my-view/

## Stubs

Stubs are files needed for demo purposes, but should not be bundled into the distribution for production. For instance a detail page could have a placeholder image. In that case we should not treat it is an asset but as a stub. Instead of putting them in the `assets/` directory, place them into a directory called `stubs/`:

	src/components/my-component/stubs/*	>> dist/stubs/components/my-component/
	src/components/my-view/stubs/*			>> dist/stubs/views/my-view/

## Optimising assets

Assets which should be optimised before copied to the distribution directory, should be placed in the `raw-assets/` directory rather than the `assets/` directory. These assets will be optimised and placed into `assets/` by running:

	$ npm run build-assets

This task is not triggered automatically by the regular build/watch tasks since they only need to run when raw assets are added. The optimised assets are under version control and will therefore be directly available to others.

### Images

Images which should be optimised should be placed in a `raw-assets/images/` directory, either on an app, view or component level. Images in these directories will be processed using [imagemin](https://github.com/imagemin/imagemin#imagemin--) and placed into a relative `assets/images/` directory:

	src/assets-raw/images/img.png                           >> src/assets/images/img.png
	src/components/my-component/assets-raw/images/img.png   >> src/components/my-component/assets/images/img.png
	src/views/my-view/assets-raw/images/img.png             >> src/views/my-view/assets/images/img.png


## Using assets

Since the path to the asset in the distribution files is different from the source, the front-end guide comes with a few helpers. For this we'll use an example asset `demo-logo.svg` which is part of the app-logo component and therefore placed in `src/components/app-logo/assets/demo-logo.svg`. The asset will eventually be copied to `dist/assets/components/app-logo/demo-logo.svg`.

### Asset in template

To use the asset in a template use the [**`paths.assets`** variable](authoring-templates.md#template-variables) which resolves to the relative web path (the `dist/assets/` directory). Example:

	```html

		<img src="{{ paths.assets }}components/app-logo/demo-logo.svg" alt="demo">
	```
If you are using a stub instead of an asset, use `{{ paths.stubs }}` instead. Make sure that the file is in `stubs/` instead of `assets/`.

### Asset in stylesheet

To use the asset in a stylesheet use the **`#{$pathToAssets}`** inside the string of the relative asset path. Example:

	```css

		// src/components/app-logo/app-logo.scss
		.app-logo {
			background-image: "#{$pathToAssets}components/app-logo/demo-logo.svg";
		}

	```

## Icons

Icons are generated and managed using [grunticon](https://www.npmjs.com/package/grunt-grunticon).

"grunticon is a Grunt.js task that makes it easy to manage icons and background images for all devices, preferring HD (retina) SVG icons but also provides fallback support for standard definition browsers, and old browsers alike. From a CSS perspective, it's easy to use, as it generates a class referencing each icon, and doesn't use CSS sprites.

grunticon takes a folder of SVG/PNG files and outputs them to CSS in 3 formats: svg data urls, png data urls, and a third fallback CSS file with references to regular png images, which are also automatically generated and placed in a folder."

There is a component called **app-icons** that holds the raw svg icons and the configuration for grunticon.

A typical workflow to add a new icon would be:

* Add an SVG icon file to components/app-icons/icons
* `npm run install-grunticon` (this will install all the grunt dependencies that you need locally. Only needed once.)
* `$ npm run icons`
* Use the icon as you wish

All the icons will be generated with a `icon-` prefix. So  `arrow-left.svg` will become `.icon-arrow-left` on the generated css.

##### Artifactory 404 issue
One of the `grunticon-lib` dependencies can not be installed via our Artifactory NPM registry. Work around this by renaming the `.npmrc` file before running `npm run install-grunticon`. This will get the dependencies directly from npmjs.org. Restore the original name afterwards.

### Advanced icon options

The `icon-task-config.js` file inside the component allows for some specific configurations for the icons ouput.

```

    // src/components/app-icons/grunticon-options.json
    "selectors": {
        "arrow-left": [".back"]
    }
```

You can define more classes to be created besides the prefixed class name.
In the example above it would generate the following css:

    ```css

        .back,
        .icon-arrow-left { background-image: url('data:image/png;base64,...'); background-repeat: no-repeat; }

    ```

For more detailed information about grunticon visit the [documentation](https://www.npmjs.com/package/grunt-grunticon#the-grunticon-task).

##### todo:
- Switch to `gulpicon` package when we have time, so we no longer have to use Grunt for this
- Solve artifactory `worker-farm` 404 issue

## Watch assets

All source files in the `assets/` directories are watched by the watch task and automatically copied to the distribution directory when changed. You can use this feature by running:

	$ npm run watch


