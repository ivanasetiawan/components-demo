# Authoring styles (SCSS/CSS)

The front-end guide uses [SCSS](http://sass-lang.com/) for module based CSS development.

"Sass is the most mature, stable, and powerful professional grade CSS extension language in the world."

## Define module styles

Each module should declare it's own presentation in it's own SCSS file.

### Module namespace

The aim of module based development is that a module can exist on its own without affecting or being affected by other modules. To prevent styles from leaking into or from other modules we suggest using the module name as a namespace selector:

```css

	.my-module {
		// add style rules for component within `my-module` namespace.
		.nested-item { }
	}
```

Or alternatively as a selector prefix (faster and more flexible):

```css

	.my-module-header { }
	.my-module-body   { }
	// etc
```

### Module dependencies

Modules may depend on the styling of other modules. SCSS supports dependency management using the `@import` directive. This allows us to require other files to be part of the output CSS using `@import "path/to/file"`. The [app-header component](../src/components/app-header/app-header.scss) is an example of how we could put this concept to use:

```css

	@import "../app-core-styles/styles/mixins/clearfix";
	@import "../app-core-styles/styles/variables";
	@import "../app-core-styles/app-core-styles";

	.app-header {
		position: relative;
		.app-logo    { float: left; }
		.main-menu   { float: left; }
		.user-menu   { float: right; }
		.search-menu { float: right; }
	}
```


## Main index

The front-end guide assumes [`src/index.scss`](../src/index.scss) to be the main style index. All views which should be bundled into the project should be included in this file. The view's themselves will import the necessary scss dependecies.


## Default SCSS template

New modules can be generated using `gulp create_module`. Depending on the module type selected in this task, it creates a module based on the `src/components/_template/` or `src/views/_template/`. These directories contain the [component SCSS template](../src/components/_template/template.scss) and [view scss template](../src/views/_template/template.scss). You can modify these as you want. The `MODULE_NAME` constant is automatically substituted by the name of the new module.


## Include styles in HTML template

By default the main stylesheet is referenced in the [`base-view.html`](../src/views/_base-view/base-view.html) `<head>`:

	{# in `src/views/_base-view/base-view.html`: #}
    {% block styles %}
        <link rel="stylesheet" href="{{ paths.assets }}index.css">
    {% endblock %}

You can [overwrite or extend](authoring-templates.md#template-slots) this style slot like any other block:

	{# in `src/views/custom-view/custom-view.html`: #}
	{% extends "views/_base-view/base-view.html" %}
	{% block styles %}
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
		{{ super() }}
	{% endblock %}

## Coding conventions

### Only tag names & class names as CSS selectors

For a clear separation of concerns De Voorhoede uses **only tag names & class** names as CSS selectors (hooks). Never use IDs as CSS hooks.

### Limit nesting

SCSS allows us to [nest rules](http://sass-lang.com/documentation/file.SASS_REFERENCE.html#nested_rules). This is useful for namespacing, pseudo classes and module/selector specific media queries. However try to limit nesting to two levels deep as it makes rules over-specific which makes the project harder to maintain. Nesting many levels means style rules start to mimic the content structure which is clearly not a separation of concerns.
