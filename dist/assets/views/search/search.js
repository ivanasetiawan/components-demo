'use strict';

require('./../_base-view/base-view');
require('../../components/advertisements/advertisements');
require('../../components/advertisements-lazy/advertisements-lazy');
require('../../components/search-content/search-content');
require('../../components/dialog/dialog');
require('../../components/filter-flyout/filter-flyout');
require('../../components/instant-search/instant-search');
require('../../components/makelaar-ads/makelaar-ads');
require('../../components/object-type-filter/object-type-filter');
require('../../components/pagination/pagination');
require('../../components/radio-group/radio-group');
require('../../components/search-header/search-header');
require('../../components/search-save/search-save');
require('../../components/search-sidebar/search-sidebar');
require('../../components/search-price-toggle/search-price-toggle');
require('../../components/top-position/top-position');
require('../../components/applied-filters/applied-filters');
