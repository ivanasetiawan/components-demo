/**
 * user-object-rating view
 * [description]
 */

require('./../_base-view/base-view');
require('../../components/rating/rating');
require('../../components/object-rating-form/object-rating-form');
