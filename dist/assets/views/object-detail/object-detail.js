/**
 * object-detail view
 */

require('./../_base-view/base-view');
require('./../../components/image-error-fallback/image-error-fallback');
require('./../../components/object-description/object-description');
require('./../../components/object-kenmerken/object-kenmerken');
require('./../../components/object-map/object-map');
require('./../../components/media-viewer/media-viewer');
require('./../../components/object-media/object-media');
require('./../../components/object-contact/object-contact');
require('./../../components/log-request/log-request');
require('./../../components/opt-in/opt-in');
require('./../../components/tooltip/tooltip');
require('./../../components/user-save-object/user-save-object');
require('./../../components/object-detail-interaction/object-detail-interaction');
require('./../../components/advertisements-lazy/advertisements-lazy');
require('./../../components/related-objects/related-objects');
