/**
 * nieuwbouwproject-detail view
 */
require('./../_base-view/base-view');
require('./../../components/object-description/object-description');
require('./../../components/nieuwbouwproject-woningtypen/nieuwbouwproject-woningtypen');
require('./../../components/related-objects/related-objects');
require('./../../components/object-actions/object-actions');

