/**
 * user-reviews view
 * View containing all reviews (and sorting options) for a makelaar.
 */

require('../../components/user-review/user-review');
require('../../components/user-review-paging/user-review-paging');
