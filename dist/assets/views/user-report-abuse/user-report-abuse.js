/**
 * user-report-abuse view
 * View with a form to report abuse regarding reviews.
 */

require('./../_base-view/base-view');
require('../../components/user-report-form/user-report-form');
