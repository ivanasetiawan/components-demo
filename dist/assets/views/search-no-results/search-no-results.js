/**
 * search-no-results view
 * displays message when applying a filter set with no matches
 */

'use strict';

require('./../_base-view/base-view');
require('../../components/expandible/expandible');
require('../../components/instant-search/instant-search');
require('../../components/makelaar-ads/makelaar-ads');
require('../../components/save-search/save-search');
require('../../components/search-header/search-header');
require('../../components/search-sidebar/search-sidebar');
require('../../components/search-no-results/search-no-results');
require('../../components/radius-filter/radius-filter');
require('../../components/user-save-object/user-save-object');

