/**
 * Base view
 * Foundation for all views, bootstraps app-wide components
 */

require('../../components/error-bar/error-bar');
require('../../components/app-header/app-header');
require('../../components/app-hotkeys/app-hotkeys');
require('../../components/cookie-policy/cookie-policy');
