/**
 * object-detail view
 * [description]
 */

require('./../_base-view/base-view');
require('../../components/async-object-rating/async-object-rating');
require('../../components/object-rating-form/object-rating-form');
require('../../components/user-saved-objects-sorting/user-saved-objects-sorting');
require('../../components/user-my-logged-in-as/user-my-logged-in-as');
