/**
 * makelaars-kantoor view
 * [description]
 */

require('../../components/makelaars-about/makelaars-about');
require('../../components/object-contact/object-contact');
require('../../components/makelaars-contact/makelaars-contact');
require('../../components/makelaars-features/makelaars-features');
require('./../../components/related-objects/related-objects');
require('../../components/makelaars-twitterfeed/makelaars-twitterfeed');
require('./../../components/media-viewer/media-viewer');
