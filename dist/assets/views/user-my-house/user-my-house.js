/**
 * user-my-house view
 * View for the users to view their house on funda, and how it performs.
 */

require('./../_base-view/base-view');
require('../../components/dialog/dialog');
require('../../components/graph/graph');
require('../../components/user-my-house-datasource/user-my-house-datasource');
require('../../components/user-my-house-graph/user-my-house-graph');
require('../../components/user-my-house-header/user-my-house-header');
require('../../components/user-my-house-options/user-my-house-options');
require('../../components/user-my-house-statistics/user-my-house-statistics');
