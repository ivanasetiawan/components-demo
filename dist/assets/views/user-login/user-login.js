/**
 * user-login view
 * [description]
 */

require('../../components/dialog/dialog');
require('../../components/notification/notification');
require('../../components/placeholder-fallback/placeholder-fallback');
require('../../components/social-login/social-login');
require('../../components/user-login/user-login.js');
