'use strict';

require('./../_base-view/base-view');

require('../../components/dialog/dialog');
require('../../components/instant-search/instant-search');
require('../../components/filter-reset/filter-reset');
require('../../components/makelaars-name/makelaars-name');
require('../../components/notification/notification');
require('../../components/pagination/pagination');
require('../../components/radio-group/radio-group');
require('../../components/search-header/search-header');
require('../../components/search-sidebar/search-sidebar');
