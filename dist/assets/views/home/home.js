/**
 * home koop / default view
 */

require('./../_base-view/base-view');
require('../../components/autocomplete/autocomplete');
require('../../components/notification-cookie/notification-cookie');
require('../../components/soort-notification-cookie/soort-notification-cookie');
require('../../components/notification-status/notification-status');
require('../../components/range-filter/range-filter');
require('../../components/expandible/expandible');
require('../../components/search-block/search-block');
