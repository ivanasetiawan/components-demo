/**
 * https://github.com/zurb/twentytwenty
 * Altered/stripped to suit funda's needs for content-funda-huis
 *
 * Requires jQuery Move Events (1.3.6)
 * https://github.com/stephband/jquery.event.move/tree/native
 */
/*eslint-disable*/
(function($) {
    var TWENTYTWENTY_LABEL_BEFORE = '.twentytwenty-label--before';
    var TWENTYTWENTY_LABEL_AFTER = '.twentytwenty-label--after';

    $.fn.twentytwenty = function(options) {

        options = $.extend({default_offset_pct: 0.5}, options);
        return this.each(function() {

            var sliderPct = options.default_offset_pct;
            var container = $(this);

            container.wrap("<div class='twentytwenty-wrapper twentytwenty-horizontal'></div>");
            var beforeImg = container.find("img:first");
            var afterImg = container.find("img:last");
            container.append("<div class='twentytwenty-handle'></div>");
            var slider = container.find(".twentytwenty-handle");
            slider.append('<span class="icon-move-white"></span>');
            container.addClass("twentytwenty-container");
            beforeImg.addClass("twentytwenty-before");
            afterImg.addClass("twentytwenty-after");

            var $beforeLabel = $(TWENTYTWENTY_LABEL_BEFORE);
            var $afterLabel = $(TWENTYTWENTY_LABEL_AFTER);
            var $labels = $(TWENTYTWENTY_LABEL_BEFORE + ', ' + TWENTYTWENTY_LABEL_AFTER);

            var calcOffset = function(dimensionPct) {
                var w = beforeImg.width();
                var h = beforeImg.height();
                return {
                    w: w + "px",
                    h: h + "px",
                    cw: (dimensionPct * w) + "px",
                    ch: (dimensionPct * h) + "px"
                };
            };

            var adjustContainer = function(offset) {
                beforeImg.css("clip", "rect(0," + offset.cw + "," + offset.h + ",0)");
                container.css("height", offset.h);
            };

            var adjustSlider = function(pct) {
                var offset = calcOffset(pct);
                slider.css("left", offset.cw);
                adjustContainer(offset);
            };

            $(window).on("resize.twentytwenty", function(e) {
                adjustSlider(sliderPct);
            });

            var offsetX = 0;
            var offsetY = 0;
            var imgWidth = 0;
            var imgHeight = 0;

            slider.on("movestart", function(e) {
                if ((e.distX > e.distY && e.distX < -e.distY) || (e.distX < e.distY && e.distX > -e.distY)) {
                    e.preventDefault();
                }
                container.addClass("active");
                offsetX = container.offset().left;
                offsetY = container.offset().top;
                imgWidth = beforeImg.width();
                imgHeight = beforeImg.height();
            });

            slider.on("moveend", function(e) {
                container.removeClass("active");
            });

            slider.on("move", function(e) {
                if (container.hasClass("active")) {
                    sliderPct = (e.pageX - offsetX) / imgWidth;

                    if (sliderPct < 0) {
                        sliderPct = 0;
                    }
                    if (sliderPct > 1) {
                        sliderPct = 1;
                    }

                    if (sliderPct < 0.22) {
                        $beforeLabel.addClass('is-hidden');
                    } else if (sliderPct > 0.78) {
                        $afterLabel.addClass('is-hidden');
                    } else {
                        $labels.removeClass('is-hidden');
                    }

                    adjustSlider(sliderPct);
                }
            });

            container.find("img").on("mousedown", function(event) {
                event.preventDefault();
            });

            // because of srcset-related race conditions, make sure we start twentytwenty at the right moment
            // wait for onload
            afterImg[0].onload = () => {
                $(window).trigger("resize.twentytwenty");
            };
            if ((typeof afterImg.naturalHeight !== "undefined" && afterImg.naturalHeight !== 0)) {
                // image already loaded/loading
                afterImg[0].onload = () => {};
                $(window).trigger("resize.twentytwenty");
            }
        });
    };
})(jQuery);

//initialize
var TWENTYTWENTY_COMPONENT_SELECTOR = '[data-content-funda-huis-twentytwenty]';
jQuery(TWENTYTWENTY_COMPONENT_SELECTOR).twentytwenty();

/*eslint-enable*/
