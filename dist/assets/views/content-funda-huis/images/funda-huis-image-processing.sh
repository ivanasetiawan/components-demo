#!/usr/bin/env bash

[ -d ./result ] || mkdir -p result

for filename in *.jpg;
do
	base=$(basename -s.jpg "${filename}")
	for resolution in 3840 1920 1440 1080 720 360;
	do
		convert -strip -quality 85 -resize ${resolution} "./${filename}" "./result/${base}_${resolution}.jpg"
	done
done
