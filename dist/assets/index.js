(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
(function() {

  var supportCustomEvent = window.CustomEvent;
  if (!supportCustomEvent || typeof supportCustomEvent == 'object') {
    supportCustomEvent = function CustomEvent(event, x) {
      x = x || {};
      var ev = document.createEvent('CustomEvent');
      ev.initCustomEvent(event, !!x.bubbles, !!x.cancelable, x.detail || null);
      return ev;
    };
    supportCustomEvent.prototype = window.Event.prototype;
  }

  /**
   * Finds the nearest <dialog> from the passed element.
   *
   * @param {Element} el to search from
   * @return {HTMLDialogElement} dialog found
   */
  function findNearestDialog(el) {
    while (el) {
      if (el.nodeName.toUpperCase() == 'DIALOG') {
        return /** @type {HTMLDialogElement} */ (el);
      }
      el = el.parentElement;
    }
    return null;
  }

  /**
   * Blur the specified element, as long as it's not the HTML body element.
   * This works around an IE9/10 bug - blurring the body causes Windows to
   * blur the whole application.
   *
   * @param {Element} el to blur
   */
  function safeBlur(el) {
    if (el && el.blur && el != document.body) {
      el.blur();
    }
  }

  /**
   * @param {!NodeList} nodeList to search
   * @param {Node} node to find
   * @return {boolean} whether node is inside nodeList
   */
  function inNodeList(nodeList, node) {
    for (var i = 0; i < nodeList.length; ++i) {
      if (nodeList[i] == node) {
        return true;
      }
    }
    return false;
  }

  /**
   * @param {!HTMLDialogElement} dialog to upgrade
   * @constructor
   */
  function dialogPolyfillInfo(dialog) {
    this.dialog_ = dialog;
    this.replacedStyleTop_ = false;
    this.openAsModal_ = false;

    // Set a11y role. Browsers that support dialog implicitly know this already.
    if (!dialog.hasAttribute('role')) {
      dialog.setAttribute('role', 'dialog');
    }

    dialog.show = this.show.bind(this);
    dialog.showModal = this.showModal.bind(this);
    dialog.close = this.close.bind(this);

    if (!('returnValue' in dialog)) {
      dialog.returnValue = '';
    }

    this.maybeHideModal = this.maybeHideModal.bind(this);
    if ('MutationObserver' in window) {
      // IE11+, most other browsers.
      var mo = new MutationObserver(this.maybeHideModal);
      mo.observe(dialog, { attributes: true, attributeFilter: ['open'] });
    } else {
      dialog.addEventListener('DOMAttrModified', this.maybeHideModal);
    }
    // Note that the DOM is observed inside DialogManager while any dialog
    // is being displayed as a modal, to catch modal removal from the DOM.

    Object.defineProperty(dialog, 'open', {
      set: this.setOpen.bind(this),
      get: dialog.hasAttribute.bind(dialog, 'open')
    });

    this.backdrop_ = document.createElement('div');
    this.backdrop_.className = 'backdrop';
    this.backdropClick_ = this.backdropClick_.bind(this);
  }

  dialogPolyfillInfo.prototype = {

    get dialog() {
      return this.dialog_;
    },

    /**
     * Maybe remove this dialog from the modal top layer. This is called when
     * a modal dialog may no longer be tenable, e.g., when the dialog is no
     * longer open or is no longer part of the DOM.
     */
    maybeHideModal: function() {
      if (!this.openAsModal_) { return; }
      if (this.dialog_.hasAttribute('open') &&
          document.body.contains(this.dialog_)) { return; }

      this.openAsModal_ = false;
      this.dialog_.style.zIndex = '';

      // This won't match the native <dialog> exactly because if the user set
      // top on a centered polyfill dialog, that top gets thrown away when the
      // dialog is closed. Not sure it's possible to polyfill this perfectly.
      if (this.replacedStyleTop_) {
        this.dialog_.style.top = '';
        this.replacedStyleTop_ = false;
      }

      // Optimistically clear the modal part of this <dialog>.
      this.backdrop_.removeEventListener('click', this.backdropClick_);
      if (this.backdrop_.parentElement) {
        this.backdrop_.parentElement.removeChild(this.backdrop_);
      }
      dialogPolyfill.dm.removeDialog(this);
    },

    /**
     * @param {boolean} value whether to open or close this dialog
     */
    setOpen: function(value) {
      if (value) {
        this.dialog_.hasAttribute('open') || this.dialog_.setAttribute('open', '');
      } else {
        this.dialog_.removeAttribute('open');
        this.maybeHideModal();  // nb. redundant with MutationObserver
      }
    },

    /**
     * Handles clicks on the fake .backdrop element, redirecting them as if
     * they were on the dialog itself.
     *
     * @param {!Event} e to redirect
     */
    backdropClick_: function(e) {
      var redirectedEvent = document.createEvent('MouseEvents');
      redirectedEvent.initMouseEvent(e.type, e.bubbles, e.cancelable, window,
          e.detail, e.screenX, e.screenY, e.clientX, e.clientY, e.ctrlKey,
          e.altKey, e.shiftKey, e.metaKey, e.button, e.relatedTarget);
      this.dialog_.dispatchEvent(redirectedEvent);
      e.stopPropagation();
    },

    /**
     * Sets the zIndex for the backdrop and dialog.
     *
     * @param {number} backdropZ
     * @param {number} dialogZ
     */
    updateZIndex: function(backdropZ, dialogZ) {
      this.backdrop_.style.zIndex = backdropZ;
      this.dialog_.style.zIndex = dialogZ;
    },

    /**
     * Shows the dialog. This is idempotent and will always succeed.
     */
    show: function() {
      this.setOpen(true);
    },

    /**
     * Show this dialog modally.
     */
    showModal: function() {
      if (this.dialog_.hasAttribute('open')) {
        throw new Error('Failed to execute \'showModal\' on dialog: The element is already open, and therefore cannot be opened modally.');
      }
      if (!document.body.contains(this.dialog_)) {
        throw new Error('Failed to execute \'showModal\' on dialog: The element is not in a Document.');
      }
      if (!dialogPolyfill.dm.pushDialog(this)) {
        throw new Error('Failed to execute \'showModal\' on dialog: There are too many open modal dialogs.');
      }
      this.show();
      this.openAsModal_ = true;

      // Optionally center vertically, relative to the current viewport.
      if (dialogPolyfill.needsCentering(this.dialog_)) {
        dialogPolyfill.reposition(this.dialog_);
        this.replacedStyleTop_ = true;
      } else {
        this.replacedStyleTop_ = false;
      }

      // Insert backdrop.
      this.backdrop_.addEventListener('click', this.backdropClick_);
      this.dialog_.parentNode.insertBefore(this.backdrop_,
          this.dialog_.nextSibling);

      // Find element with `autofocus` attribute or first form control.
      var target = this.dialog_.querySelector('[autofocus]:not([disabled])');
      if (!target) {
        // TODO: technically this is 'any focusable area'
        var opts = ['button', 'input', 'keygen', 'select', 'textarea'];
        var query = opts.map(function(el) {
          return el + ':not([disabled])';
        }).join(', ');
        target = this.dialog_.querySelector(query);
      }
      safeBlur(document.activeElement);
      target && target.focus();
    },

    /**
     * Closes this HTMLDialogElement. This is optional vs clearing the open
     * attribute, however this fires a 'close' event.
     *
     * @param {string=} opt_returnValue to use as the returnValue
     */
    close: function(opt_returnValue) {
      if (!this.dialog_.hasAttribute('open')) {
        throw new Error('Failed to execute \'close\' on dialog: The element does not have an \'open\' attribute, and therefore cannot be closed.');
      }
      this.setOpen(false);

      // Leave returnValue untouched in case it was set directly on the element
      if (opt_returnValue !== undefined) {
        this.dialog_.returnValue = opt_returnValue;
      }

      // Triggering "close" event for any attached listeners on the <dialog>.
      var closeEvent = new supportCustomEvent('close', {
        bubbles: false,
        cancelable: false
      });
      this.dialog_.dispatchEvent(closeEvent);
    }

  };

  var dialogPolyfill = {};

  dialogPolyfill.reposition = function(element) {
    var scrollTop = document.body.scrollTop || document.documentElement.scrollTop;
    var topValue = scrollTop + (window.innerHeight - element.offsetHeight) / 2;
    element.style.top = Math.max(scrollTop, topValue) + 'px';
  };

  dialogPolyfill.isInlinePositionSetByStylesheet = function(element) {
    for (var i = 0; i < document.styleSheets.length; ++i) {
      var styleSheet = document.styleSheets[i];
      var cssRules = null;
      // Some browsers throw on cssRules.
      try {
        cssRules = styleSheet.cssRules;
      } catch (e) {}
      if (!cssRules)
        continue;
      for (var j = 0; j < cssRules.length; ++j) {
        var rule = cssRules[j];
        var selectedNodes = null;
        // Ignore errors on invalid selector texts.
        try {
          selectedNodes = document.querySelectorAll(rule.selectorText);
        } catch(e) {}
        if (!selectedNodes || !inNodeList(selectedNodes, element))
          continue;
        var cssTop = rule.style.getPropertyValue('top');
        var cssBottom = rule.style.getPropertyValue('bottom');
        if ((cssTop && cssTop != 'auto') || (cssBottom && cssBottom != 'auto'))
          return true;
      }
    }
    return false;
  };

  dialogPolyfill.needsCentering = function(dialog) {
    var computedStyle = window.getComputedStyle(dialog);
    if (computedStyle.position != 'absolute') {
      return false;
    }

    // We must determine whether the top/bottom specified value is non-auto.  In
    // WebKit/Blink, checking computedStyle.top == 'auto' is sufficient, but
    // Firefox returns the used value. So we do this crazy thing instead: check
    // the inline style and then go through CSS rules.
    if ((dialog.style.top != 'auto' && dialog.style.top != '') ||
        (dialog.style.bottom != 'auto' && dialog.style.bottom != ''))
      return false;
    return !dialogPolyfill.isInlinePositionSetByStylesheet(dialog);
  };

  /**
   * @param {!Element} element to force upgrade
   */
  dialogPolyfill.forceRegisterDialog = function(element) {
    if (element.showModal) {
      console.warn('This browser already supports <dialog>, the polyfill ' +
          'may not work correctly', element);
    }
    if (element.nodeName.toUpperCase() != 'DIALOG') {
      throw new Error('Failed to register dialog: The element is not a dialog.');
    }
    new dialogPolyfillInfo(/** @type {!HTMLDialogElement} */ (element));
  };

  /**
   * @param {!Element} element to upgrade
   */
  dialogPolyfill.registerDialog = function(element) {
    if (element.showModal) {
      console.warn('Can\'t upgrade <dialog>: already supported', element);
    } else {
      dialogPolyfill.forceRegisterDialog(element);
    }
  };

  /**
   * @constructor
   */
  dialogPolyfill.DialogManager = function() {
    /** @type {!Array<!dialogPolyfillInfo>} */
    this.pendingDialogStack = [];

    // The overlay is used to simulate how a modal dialog blocks the document.
    // The blocking dialog is positioned on top of the overlay, and the rest of
    // the dialogs on the pending dialog stack are positioned below it. In the
    // actual implementation, the modal dialog stacking is controlled by the
    // top layer, where z-index has no effect.
    this.overlay = document.createElement('div');
    this.overlay.className = '_dialog_overlay';
    this.overlay.addEventListener('click', function(e) {
      e.stopPropagation();
    });

    this.handleKey_ = this.handleKey_.bind(this);
    this.handleFocus_ = this.handleFocus_.bind(this);
    this.handleRemove_ = this.handleRemove_.bind(this);

    this.zIndexLow_ = 100000;
    this.zIndexHigh_ = 100000 + 150;
  };

  /**
   * @return {Element} the top HTML dialog element, if any
   */
  dialogPolyfill.DialogManager.prototype.topDialogElement = function() {
    if (this.pendingDialogStack.length) {
      var t = this.pendingDialogStack[this.pendingDialogStack.length - 1];
      return t.dialog;
    }
    return null;
  };

  /**
   * Called on the first modal dialog being shown. Adds the overlay and related
   * handlers.
   */
  dialogPolyfill.DialogManager.prototype.blockDocument = function() {
    document.body.appendChild(this.overlay);
    document.body.addEventListener('focus', this.handleFocus_, true);
    document.addEventListener('keydown', this.handleKey_);
    document.addEventListener('DOMNodeRemoved', this.handleRemove_);
  };

  /**
   * Called on the first modal dialog being removed, i.e., when no more modal
   * dialogs are visible.
   */
  dialogPolyfill.DialogManager.prototype.unblockDocument = function() {
    document.body.removeChild(this.overlay);
    document.body.removeEventListener('focus', this.handleFocus_, true);
    document.removeEventListener('keydown', this.handleKey_);
    document.removeEventListener('DOMNodeRemoved', this.handleRemove_);
  };

  dialogPolyfill.DialogManager.prototype.updateStacking = function() {
    var zIndex = this.zIndexLow_;

    for (var i = 0; i < this.pendingDialogStack.length; i++) {
      if (i == this.pendingDialogStack.length - 1) {
        this.overlay.style.zIndex = zIndex++;
      }
      this.pendingDialogStack[i].updateZIndex(zIndex++, zIndex++);
    }
  };

  dialogPolyfill.DialogManager.prototype.handleFocus_ = function(event) {
    var candidate = findNearestDialog(/** @type {Element} */ (event.target));
    if (candidate != this.topDialogElement()) {
      event.preventDefault();
      event.stopPropagation();
      safeBlur(/** @type {Element} */ (event.target));
      // TODO: Focus on the browser chrome (aka document) or the dialog itself
      // depending on the tab direction.
      return false;
    }
  };

  dialogPolyfill.DialogManager.prototype.handleKey_ = function(event) {
    if (event.keyCode == 27) {
      event.preventDefault();
      event.stopPropagation();
      var cancelEvent = new supportCustomEvent('cancel', {
        bubbles: false,
        cancelable: true
      });
      var dialog = this.topDialogElement();
      if (dialog.dispatchEvent(cancelEvent)) {
        dialog.close();
      }
    }
  };

  dialogPolyfill.DialogManager.prototype.handleRemove_ = function(event) {
    if (event.target.nodeName.toUpperCase() != 'DIALOG') { return; }

    var dialog = /** @type {HTMLDialogElement} */ (event.target);
    if (!dialog.open) { return; }

    // Find a dialogPolyfillInfo which matches the removed <dialog>.
    this.pendingDialogStack.some(function(dpi) {
      if (dpi.dialog == dialog) {
        // This call will clear the dialogPolyfillInfo on this DialogManager
        // as a side effect.
        dpi.maybeHideModal();
        return true;
      }
    });
  };

  /**
   * @param {!dialogPolyfillInfo} dpi
   * @return {boolean} whether the dialog was allowed
   */
  dialogPolyfill.DialogManager.prototype.pushDialog = function(dpi) {
    var allowed = (this.zIndexHigh_ - this.zIndexLow_) / 2 - 1;
    if (this.pendingDialogStack.length >= allowed) {
      return false;
    }
    this.pendingDialogStack.push(dpi);
    if (this.pendingDialogStack.length == 1) {
      this.blockDocument();
    }
    this.updateStacking();
    return true;
  };

  /**
   * @param {dialogPolyfillInfo} dpi
   */
  dialogPolyfill.DialogManager.prototype.removeDialog = function(dpi) {
    var index = this.pendingDialogStack.indexOf(dpi);
    if (index == -1) { return; }

    this.pendingDialogStack.splice(index, 1);
    this.updateStacking();
    if (this.pendingDialogStack.length == 0) {
      this.unblockDocument();
    }
  };

  dialogPolyfill.dm = new dialogPolyfill.DialogManager();

  /**
   * Global form 'dialog' method handler. Closes a dialog correctly on submit
   * and possibly sets its return value.
   */
  document.addEventListener('submit', function(ev) {
    var target = ev.target;
    if (!target || !target.hasAttribute('method')) { return; }
    if (target.getAttribute('method').toLowerCase() != 'dialog') { return; }
    ev.preventDefault();

    var dialog = findNearestDialog(/** @type {Element} */ (ev.target));
    if (!dialog) { return; }

    // FIXME: The original event doesn't contain the element used to submit the
    // form (if any). Look in some possible places.
    var returnValue;
    var cands = [document.activeElement, ev.explicitOriginalTarget];
    var els = ['BUTTON', 'INPUT'];
    cands.some(function(cand) {
      if (cand && cand.form == ev.target && els.indexOf(cand.nodeName.toUpperCase()) != -1) {
        returnValue = cand.value;
        return true;
      }
    });
    dialog.close(returnValue);
  }, true);

  dialogPolyfill['forceRegisterDialog'] = dialogPolyfill.forceRegisterDialog;
  dialogPolyfill['registerDialog'] = dialogPolyfill.registerDialog;

  if (typeof module === 'object' && typeof module['exports'] === 'object') {
    // CommonJS support
    module['exports'] = dialogPolyfill;
  } else if (typeof define === 'function' && 'amd' in define) {
    // AMD support
    define(function() { return dialogPolyfill; });
  } else {
    // all others
    window['dialogPolyfill'] = dialogPolyfill;
  }
})();

},{}],2:[function(require,module,exports){

module.exports = function load (src, opts, cb) {
  var head = document.head || document.getElementsByTagName('head')[0]
  var script = document.createElement('script')

  if (typeof opts === 'function') {
    cb = opts
    opts = {}
  }

  opts = opts || {}
  cb = cb || function() {}

  script.type = opts.type || 'text/javascript'
  script.charset = opts.charset || 'utf8';
  script.async = 'async' in opts ? !!opts.async : true
  script.src = src

  if (opts.attrs) {
    setAttributes(script, opts.attrs)
  }

  if (opts.text) {
    script.text = '' + opts.text
  }

  var onend = 'onload' in script ? stdOnEnd : ieOnEnd
  onend(script, cb)

  // some good legacy browsers (firefox) fail the 'in' detection above
  // so as a fallback we always set onload
  // old IE will ignore this and new IE will set onload
  if (!script.onload) {
    stdOnEnd(script, cb);
  }

  head.appendChild(script)
}

function setAttributes(script, attrs) {
  for (var attr in attrs) {
    script.setAttribute(attr, attrs[attr]);
  }
}

function stdOnEnd (script, cb) {
  script.onload = function () {
    this.onerror = this.onload = null
    cb(null, script)
  }
  script.onerror = function () {
    // this.onload = null here is necessary
    // because even IE9 works not like others
    this.onerror = this.onload = null
    cb(new Error('Failed to load ' + this.src), script)
  }
}

function ieOnEnd (script, cb) {
  script.onreadystatechange = function () {
    if (this.readyState != 'complete' && this.readyState != 'loaded') return
    this.onreadystatechange = null
    cb(null, script) // there is no way to catch loading errors in IE8
  }
}

},{}],3:[function(require,module,exports){
var getNative = require('./_getNative'),
    root = require('./_root');

/* Built-in method references that are verified to be native. */
var DataView = getNative(root, 'DataView');

module.exports = DataView;

},{"./_getNative":95,"./_root":139}],4:[function(require,module,exports){
var hashClear = require('./_hashClear'),
    hashDelete = require('./_hashDelete'),
    hashGet = require('./_hashGet'),
    hashHas = require('./_hashHas'),
    hashSet = require('./_hashSet');

/**
 * Creates a hash object.
 *
 * @private
 * @constructor
 * @param {Array} [entries] The key-value pairs to cache.
 */
function Hash(entries) {
  var index = -1,
      length = entries == null ? 0 : entries.length;

  this.clear();
  while (++index < length) {
    var entry = entries[index];
    this.set(entry[0], entry[1]);
  }
}

// Add methods to `Hash`.
Hash.prototype.clear = hashClear;
Hash.prototype['delete'] = hashDelete;
Hash.prototype.get = hashGet;
Hash.prototype.has = hashHas;
Hash.prototype.set = hashSet;

module.exports = Hash;

},{"./_hashClear":103,"./_hashDelete":104,"./_hashGet":105,"./_hashHas":106,"./_hashSet":107}],5:[function(require,module,exports){
var listCacheClear = require('./_listCacheClear'),
    listCacheDelete = require('./_listCacheDelete'),
    listCacheGet = require('./_listCacheGet'),
    listCacheHas = require('./_listCacheHas'),
    listCacheSet = require('./_listCacheSet');

/**
 * Creates an list cache object.
 *
 * @private
 * @constructor
 * @param {Array} [entries] The key-value pairs to cache.
 */
function ListCache(entries) {
  var index = -1,
      length = entries == null ? 0 : entries.length;

  this.clear();
  while (++index < length) {
    var entry = entries[index];
    this.set(entry[0], entry[1]);
  }
}

// Add methods to `ListCache`.
ListCache.prototype.clear = listCacheClear;
ListCache.prototype['delete'] = listCacheDelete;
ListCache.prototype.get = listCacheGet;
ListCache.prototype.has = listCacheHas;
ListCache.prototype.set = listCacheSet;

module.exports = ListCache;

},{"./_listCacheClear":119,"./_listCacheDelete":120,"./_listCacheGet":121,"./_listCacheHas":122,"./_listCacheSet":123}],6:[function(require,module,exports){
var getNative = require('./_getNative'),
    root = require('./_root');

/* Built-in method references that are verified to be native. */
var Map = getNative(root, 'Map');

module.exports = Map;

},{"./_getNative":95,"./_root":139}],7:[function(require,module,exports){
var mapCacheClear = require('./_mapCacheClear'),
    mapCacheDelete = require('./_mapCacheDelete'),
    mapCacheGet = require('./_mapCacheGet'),
    mapCacheHas = require('./_mapCacheHas'),
    mapCacheSet = require('./_mapCacheSet');

/**
 * Creates a map cache object to store key-value pairs.
 *
 * @private
 * @constructor
 * @param {Array} [entries] The key-value pairs to cache.
 */
function MapCache(entries) {
  var index = -1,
      length = entries == null ? 0 : entries.length;

  this.clear();
  while (++index < length) {
    var entry = entries[index];
    this.set(entry[0], entry[1]);
  }
}

// Add methods to `MapCache`.
MapCache.prototype.clear = mapCacheClear;
MapCache.prototype['delete'] = mapCacheDelete;
MapCache.prototype.get = mapCacheGet;
MapCache.prototype.has = mapCacheHas;
MapCache.prototype.set = mapCacheSet;

module.exports = MapCache;

},{"./_mapCacheClear":124,"./_mapCacheDelete":125,"./_mapCacheGet":126,"./_mapCacheHas":127,"./_mapCacheSet":128}],8:[function(require,module,exports){
var getNative = require('./_getNative'),
    root = require('./_root');

/* Built-in method references that are verified to be native. */
var Promise = getNative(root, 'Promise');

module.exports = Promise;

},{"./_getNative":95,"./_root":139}],9:[function(require,module,exports){
var getNative = require('./_getNative'),
    root = require('./_root');

/* Built-in method references that are verified to be native. */
var Set = getNative(root, 'Set');

module.exports = Set;

},{"./_getNative":95,"./_root":139}],10:[function(require,module,exports){
var MapCache = require('./_MapCache'),
    setCacheAdd = require('./_setCacheAdd'),
    setCacheHas = require('./_setCacheHas');

/**
 *
 * Creates an array cache object to store unique values.
 *
 * @private
 * @constructor
 * @param {Array} [values] The values to cache.
 */
function SetCache(values) {
  var index = -1,
      length = values == null ? 0 : values.length;

  this.__data__ = new MapCache;
  while (++index < length) {
    this.add(values[index]);
  }
}

// Add methods to `SetCache`.
SetCache.prototype.add = SetCache.prototype.push = setCacheAdd;
SetCache.prototype.has = setCacheHas;

module.exports = SetCache;

},{"./_MapCache":7,"./_setCacheAdd":140,"./_setCacheHas":141}],11:[function(require,module,exports){
var ListCache = require('./_ListCache'),
    stackClear = require('./_stackClear'),
    stackDelete = require('./_stackDelete'),
    stackGet = require('./_stackGet'),
    stackHas = require('./_stackHas'),
    stackSet = require('./_stackSet');

/**
 * Creates a stack cache object to store key-value pairs.
 *
 * @private
 * @constructor
 * @param {Array} [entries] The key-value pairs to cache.
 */
function Stack(entries) {
  var data = this.__data__ = new ListCache(entries);
  this.size = data.size;
}

// Add methods to `Stack`.
Stack.prototype.clear = stackClear;
Stack.prototype['delete'] = stackDelete;
Stack.prototype.get = stackGet;
Stack.prototype.has = stackHas;
Stack.prototype.set = stackSet;

module.exports = Stack;

},{"./_ListCache":5,"./_stackClear":145,"./_stackDelete":146,"./_stackGet":147,"./_stackHas":148,"./_stackSet":149}],12:[function(require,module,exports){
var root = require('./_root');

/** Built-in value references. */
var Symbol = root.Symbol;

module.exports = Symbol;

},{"./_root":139}],13:[function(require,module,exports){
var root = require('./_root');

/** Built-in value references. */
var Uint8Array = root.Uint8Array;

module.exports = Uint8Array;

},{"./_root":139}],14:[function(require,module,exports){
var getNative = require('./_getNative'),
    root = require('./_root');

/* Built-in method references that are verified to be native. */
var WeakMap = getNative(root, 'WeakMap');

module.exports = WeakMap;

},{"./_getNative":95,"./_root":139}],15:[function(require,module,exports){
/**
 * Adds the key-value `pair` to `map`.
 *
 * @private
 * @param {Object} map The map to modify.
 * @param {Array} pair The key-value pair to add.
 * @returns {Object} Returns `map`.
 */
function addMapEntry(map, pair) {
  // Don't return `map.set` because it's not chainable in IE 11.
  map.set(pair[0], pair[1]);
  return map;
}

module.exports = addMapEntry;

},{}],16:[function(require,module,exports){
/**
 * Adds `value` to `set`.
 *
 * @private
 * @param {Object} set The set to modify.
 * @param {*} value The value to add.
 * @returns {Object} Returns `set`.
 */
function addSetEntry(set, value) {
  // Don't return `set.add` because it's not chainable in IE 11.
  set.add(value);
  return set;
}

module.exports = addSetEntry;

},{}],17:[function(require,module,exports){
/**
 * A faster alternative to `Function#apply`, this function invokes `func`
 * with the `this` binding of `thisArg` and the arguments of `args`.
 *
 * @private
 * @param {Function} func The function to invoke.
 * @param {*} thisArg The `this` binding of `func`.
 * @param {Array} args The arguments to invoke `func` with.
 * @returns {*} Returns the result of `func`.
 */
function apply(func, thisArg, args) {
  switch (args.length) {
    case 0: return func.call(thisArg);
    case 1: return func.call(thisArg, args[0]);
    case 2: return func.call(thisArg, args[0], args[1]);
    case 3: return func.call(thisArg, args[0], args[1], args[2]);
  }
  return func.apply(thisArg, args);
}

module.exports = apply;

},{}],18:[function(require,module,exports){
/**
 * A specialized version of `_.forEach` for arrays without support for
 * iteratee shorthands.
 *
 * @private
 * @param {Array} [array] The array to iterate over.
 * @param {Function} iteratee The function invoked per iteration.
 * @returns {Array} Returns `array`.
 */
function arrayEach(array, iteratee) {
  var index = -1,
      length = array == null ? 0 : array.length;

  while (++index < length) {
    if (iteratee(array[index], index, array) === false) {
      break;
    }
  }
  return array;
}

module.exports = arrayEach;

},{}],19:[function(require,module,exports){
/**
 * A specialized version of `_.filter` for arrays without support for
 * iteratee shorthands.
 *
 * @private
 * @param {Array} [array] The array to iterate over.
 * @param {Function} predicate The function invoked per iteration.
 * @returns {Array} Returns the new filtered array.
 */
function arrayFilter(array, predicate) {
  var index = -1,
      length = array == null ? 0 : array.length,
      resIndex = 0,
      result = [];

  while (++index < length) {
    var value = array[index];
    if (predicate(value, index, array)) {
      result[resIndex++] = value;
    }
  }
  return result;
}

module.exports = arrayFilter;

},{}],20:[function(require,module,exports){
var baseIndexOf = require('./_baseIndexOf');

/**
 * A specialized version of `_.includes` for arrays without support for
 * specifying an index to search from.
 *
 * @private
 * @param {Array} [array] The array to inspect.
 * @param {*} target The value to search for.
 * @returns {boolean} Returns `true` if `target` is found, else `false`.
 */
function arrayIncludes(array, value) {
  var length = array == null ? 0 : array.length;
  return !!length && baseIndexOf(array, value, 0) > -1;
}

module.exports = arrayIncludes;

},{"./_baseIndexOf":44}],21:[function(require,module,exports){
/**
 * This function is like `arrayIncludes` except that it accepts a comparator.
 *
 * @private
 * @param {Array} [array] The array to inspect.
 * @param {*} target The value to search for.
 * @param {Function} comparator The comparator invoked per element.
 * @returns {boolean} Returns `true` if `target` is found, else `false`.
 */
function arrayIncludesWith(array, value, comparator) {
  var index = -1,
      length = array == null ? 0 : array.length;

  while (++index < length) {
    if (comparator(value, array[index])) {
      return true;
    }
  }
  return false;
}

module.exports = arrayIncludesWith;

},{}],22:[function(require,module,exports){
var baseTimes = require('./_baseTimes'),
    isArguments = require('./isArguments'),
    isArray = require('./isArray'),
    isBuffer = require('./isBuffer'),
    isIndex = require('./_isIndex'),
    isTypedArray = require('./isTypedArray');

/** Used for built-in method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/**
 * Creates an array of the enumerable property names of the array-like `value`.
 *
 * @private
 * @param {*} value The value to query.
 * @param {boolean} inherited Specify returning inherited property names.
 * @returns {Array} Returns the array of property names.
 */
function arrayLikeKeys(value, inherited) {
  var isArr = isArray(value),
      isArg = !isArr && isArguments(value),
      isBuff = !isArr && !isArg && isBuffer(value),
      isType = !isArr && !isArg && !isBuff && isTypedArray(value),
      skipIndexes = isArr || isArg || isBuff || isType,
      result = skipIndexes ? baseTimes(value.length, String) : [],
      length = result.length;

  for (var key in value) {
    if ((inherited || hasOwnProperty.call(value, key)) &&
        !(skipIndexes && (
           // Safari 9 has enumerable `arguments.length` in strict mode.
           key == 'length' ||
           // Node.js 0.10 has enumerable non-index properties on buffers.
           (isBuff && (key == 'offset' || key == 'parent')) ||
           // PhantomJS 2 has enumerable non-index properties on typed arrays.
           (isType && (key == 'buffer' || key == 'byteLength' || key == 'byteOffset')) ||
           // Skip index properties.
           isIndex(key, length)
        ))) {
      result.push(key);
    }
  }
  return result;
}

module.exports = arrayLikeKeys;

},{"./_baseTimes":62,"./_isIndex":112,"./isArguments":167,"./isArray":168,"./isBuffer":171,"./isTypedArray":178}],23:[function(require,module,exports){
/**
 * A specialized version of `_.map` for arrays without support for iteratee
 * shorthands.
 *
 * @private
 * @param {Array} [array] The array to iterate over.
 * @param {Function} iteratee The function invoked per iteration.
 * @returns {Array} Returns the new mapped array.
 */
function arrayMap(array, iteratee) {
  var index = -1,
      length = array == null ? 0 : array.length,
      result = Array(length);

  while (++index < length) {
    result[index] = iteratee(array[index], index, array);
  }
  return result;
}

module.exports = arrayMap;

},{}],24:[function(require,module,exports){
/**
 * Appends the elements of `values` to `array`.
 *
 * @private
 * @param {Array} array The array to modify.
 * @param {Array} values The values to append.
 * @returns {Array} Returns `array`.
 */
function arrayPush(array, values) {
  var index = -1,
      length = values.length,
      offset = array.length;

  while (++index < length) {
    array[offset + index] = values[index];
  }
  return array;
}

module.exports = arrayPush;

},{}],25:[function(require,module,exports){
/**
 * A specialized version of `_.reduce` for arrays without support for
 * iteratee shorthands.
 *
 * @private
 * @param {Array} [array] The array to iterate over.
 * @param {Function} iteratee The function invoked per iteration.
 * @param {*} [accumulator] The initial value.
 * @param {boolean} [initAccum] Specify using the first element of `array` as
 *  the initial value.
 * @returns {*} Returns the accumulated value.
 */
function arrayReduce(array, iteratee, accumulator, initAccum) {
  var index = -1,
      length = array == null ? 0 : array.length;

  if (initAccum && length) {
    accumulator = array[++index];
  }
  while (++index < length) {
    accumulator = iteratee(accumulator, array[index], index, array);
  }
  return accumulator;
}

module.exports = arrayReduce;

},{}],26:[function(require,module,exports){
/**
 * A specialized version of `_.some` for arrays without support for iteratee
 * shorthands.
 *
 * @private
 * @param {Array} [array] The array to iterate over.
 * @param {Function} predicate The function invoked per iteration.
 * @returns {boolean} Returns `true` if any element passes the predicate check,
 *  else `false`.
 */
function arraySome(array, predicate) {
  var index = -1,
      length = array == null ? 0 : array.length;

  while (++index < length) {
    if (predicate(array[index], index, array)) {
      return true;
    }
  }
  return false;
}

module.exports = arraySome;

},{}],27:[function(require,module,exports){
var baseAssignValue = require('./_baseAssignValue'),
    eq = require('./eq');

/** Used for built-in method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/**
 * Assigns `value` to `key` of `object` if the existing value is not equivalent
 * using [`SameValueZero`](http://ecma-international.org/ecma-262/7.0/#sec-samevaluezero)
 * for equality comparisons.
 *
 * @private
 * @param {Object} object The object to modify.
 * @param {string} key The key of the property to assign.
 * @param {*} value The value to assign.
 */
function assignValue(object, key, value) {
  var objValue = object[key];
  if (!(hasOwnProperty.call(object, key) && eq(objValue, value)) ||
      (value === undefined && !(key in object))) {
    baseAssignValue(object, key, value);
  }
}

module.exports = assignValue;

},{"./_baseAssignValue":31,"./eq":160}],28:[function(require,module,exports){
var eq = require('./eq');

/**
 * Gets the index at which the `key` is found in `array` of key-value pairs.
 *
 * @private
 * @param {Array} array The array to inspect.
 * @param {*} key The key to search for.
 * @returns {number} Returns the index of the matched value, else `-1`.
 */
function assocIndexOf(array, key) {
  var length = array.length;
  while (length--) {
    if (eq(array[length][0], key)) {
      return length;
    }
  }
  return -1;
}

module.exports = assocIndexOf;

},{"./eq":160}],29:[function(require,module,exports){
var copyObject = require('./_copyObject'),
    keys = require('./keys');

/**
 * The base implementation of `_.assign` without support for multiple sources
 * or `customizer` functions.
 *
 * @private
 * @param {Object} object The destination object.
 * @param {Object} source The source object.
 * @returns {Object} Returns `object`.
 */
function baseAssign(object, source) {
  return object && copyObject(source, keys(source), object);
}

module.exports = baseAssign;

},{"./_copyObject":77,"./keys":179}],30:[function(require,module,exports){
var copyObject = require('./_copyObject'),
    keysIn = require('./keysIn');

/**
 * The base implementation of `_.assignIn` without support for multiple sources
 * or `customizer` functions.
 *
 * @private
 * @param {Object} object The destination object.
 * @param {Object} source The source object.
 * @returns {Object} Returns `object`.
 */
function baseAssignIn(object, source) {
  return object && copyObject(source, keysIn(source), object);
}

module.exports = baseAssignIn;

},{"./_copyObject":77,"./keysIn":180}],31:[function(require,module,exports){
var defineProperty = require('./_defineProperty');

/**
 * The base implementation of `assignValue` and `assignMergeValue` without
 * value checks.
 *
 * @private
 * @param {Object} object The object to modify.
 * @param {string} key The key of the property to assign.
 * @param {*} value The value to assign.
 */
function baseAssignValue(object, key, value) {
  if (key == '__proto__' && defineProperty) {
    defineProperty(object, key, {
      'configurable': true,
      'enumerable': true,
      'value': value,
      'writable': true
    });
  } else {
    object[key] = value;
  }
}

module.exports = baseAssignValue;

},{"./_defineProperty":86}],32:[function(require,module,exports){
var Stack = require('./_Stack'),
    arrayEach = require('./_arrayEach'),
    assignValue = require('./_assignValue'),
    baseAssign = require('./_baseAssign'),
    baseAssignIn = require('./_baseAssignIn'),
    cloneBuffer = require('./_cloneBuffer'),
    copyArray = require('./_copyArray'),
    copySymbols = require('./_copySymbols'),
    copySymbolsIn = require('./_copySymbolsIn'),
    getAllKeys = require('./_getAllKeys'),
    getAllKeysIn = require('./_getAllKeysIn'),
    getTag = require('./_getTag'),
    initCloneArray = require('./_initCloneArray'),
    initCloneByTag = require('./_initCloneByTag'),
    initCloneObject = require('./_initCloneObject'),
    isArray = require('./isArray'),
    isBuffer = require('./isBuffer'),
    isObject = require('./isObject'),
    keys = require('./keys');

/** Used to compose bitmasks for cloning. */
var CLONE_DEEP_FLAG = 1,
    CLONE_FLAT_FLAG = 2,
    CLONE_SYMBOLS_FLAG = 4;

/** `Object#toString` result references. */
var argsTag = '[object Arguments]',
    arrayTag = '[object Array]',
    boolTag = '[object Boolean]',
    dateTag = '[object Date]',
    errorTag = '[object Error]',
    funcTag = '[object Function]',
    genTag = '[object GeneratorFunction]',
    mapTag = '[object Map]',
    numberTag = '[object Number]',
    objectTag = '[object Object]',
    regexpTag = '[object RegExp]',
    setTag = '[object Set]',
    stringTag = '[object String]',
    symbolTag = '[object Symbol]',
    weakMapTag = '[object WeakMap]';

var arrayBufferTag = '[object ArrayBuffer]',
    dataViewTag = '[object DataView]',
    float32Tag = '[object Float32Array]',
    float64Tag = '[object Float64Array]',
    int8Tag = '[object Int8Array]',
    int16Tag = '[object Int16Array]',
    int32Tag = '[object Int32Array]',
    uint8Tag = '[object Uint8Array]',
    uint8ClampedTag = '[object Uint8ClampedArray]',
    uint16Tag = '[object Uint16Array]',
    uint32Tag = '[object Uint32Array]';

/** Used to identify `toStringTag` values supported by `_.clone`. */
var cloneableTags = {};
cloneableTags[argsTag] = cloneableTags[arrayTag] =
cloneableTags[arrayBufferTag] = cloneableTags[dataViewTag] =
cloneableTags[boolTag] = cloneableTags[dateTag] =
cloneableTags[float32Tag] = cloneableTags[float64Tag] =
cloneableTags[int8Tag] = cloneableTags[int16Tag] =
cloneableTags[int32Tag] = cloneableTags[mapTag] =
cloneableTags[numberTag] = cloneableTags[objectTag] =
cloneableTags[regexpTag] = cloneableTags[setTag] =
cloneableTags[stringTag] = cloneableTags[symbolTag] =
cloneableTags[uint8Tag] = cloneableTags[uint8ClampedTag] =
cloneableTags[uint16Tag] = cloneableTags[uint32Tag] = true;
cloneableTags[errorTag] = cloneableTags[funcTag] =
cloneableTags[weakMapTag] = false;

/**
 * The base implementation of `_.clone` and `_.cloneDeep` which tracks
 * traversed objects.
 *
 * @private
 * @param {*} value The value to clone.
 * @param {boolean} bitmask The bitmask flags.
 *  1 - Deep clone
 *  2 - Flatten inherited properties
 *  4 - Clone symbols
 * @param {Function} [customizer] The function to customize cloning.
 * @param {string} [key] The key of `value`.
 * @param {Object} [object] The parent object of `value`.
 * @param {Object} [stack] Tracks traversed objects and their clone counterparts.
 * @returns {*} Returns the cloned value.
 */
function baseClone(value, bitmask, customizer, key, object, stack) {
  var result,
      isDeep = bitmask & CLONE_DEEP_FLAG,
      isFlat = bitmask & CLONE_FLAT_FLAG,
      isFull = bitmask & CLONE_SYMBOLS_FLAG;

  if (customizer) {
    result = object ? customizer(value, key, object, stack) : customizer(value);
  }
  if (result !== undefined) {
    return result;
  }
  if (!isObject(value)) {
    return value;
  }
  var isArr = isArray(value);
  if (isArr) {
    result = initCloneArray(value);
    if (!isDeep) {
      return copyArray(value, result);
    }
  } else {
    var tag = getTag(value),
        isFunc = tag == funcTag || tag == genTag;

    if (isBuffer(value)) {
      return cloneBuffer(value, isDeep);
    }
    if (tag == objectTag || tag == argsTag || (isFunc && !object)) {
      result = (isFlat || isFunc) ? {} : initCloneObject(value);
      if (!isDeep) {
        return isFlat
          ? copySymbolsIn(value, baseAssignIn(result, value))
          : copySymbols(value, baseAssign(result, value));
      }
    } else {
      if (!cloneableTags[tag]) {
        return object ? value : {};
      }
      result = initCloneByTag(value, tag, baseClone, isDeep);
    }
  }
  // Check for circular references and return its corresponding clone.
  stack || (stack = new Stack);
  var stacked = stack.get(value);
  if (stacked) {
    return stacked;
  }
  stack.set(value, result);

  var keysFunc = isFull
    ? (isFlat ? getAllKeysIn : getAllKeys)
    : (isFlat ? keysIn : keys);

  var props = isArr ? undefined : keysFunc(value);
  arrayEach(props || value, function(subValue, key) {
    if (props) {
      key = subValue;
      subValue = value[key];
    }
    // Recursively populate clone (susceptible to call stack limits).
    assignValue(result, key, baseClone(subValue, bitmask, customizer, key, value, stack));
  });
  return result;
}

module.exports = baseClone;

},{"./_Stack":11,"./_arrayEach":18,"./_assignValue":27,"./_baseAssign":29,"./_baseAssignIn":30,"./_cloneBuffer":69,"./_copyArray":76,"./_copySymbols":78,"./_copySymbolsIn":79,"./_getAllKeys":91,"./_getAllKeysIn":92,"./_getTag":100,"./_initCloneArray":108,"./_initCloneByTag":109,"./_initCloneObject":110,"./isArray":168,"./isBuffer":171,"./isObject":175,"./keys":179}],33:[function(require,module,exports){
var isObject = require('./isObject');

/** Built-in value references. */
var objectCreate = Object.create;

/**
 * The base implementation of `_.create` without support for assigning
 * properties to the created object.
 *
 * @private
 * @param {Object} proto The object to inherit from.
 * @returns {Object} Returns the new object.
 */
var baseCreate = (function() {
  function object() {}
  return function(proto) {
    if (!isObject(proto)) {
      return {};
    }
    if (objectCreate) {
      return objectCreate(proto);
    }
    object.prototype = proto;
    var result = new object;
    object.prototype = undefined;
    return result;
  };
}());

module.exports = baseCreate;

},{"./isObject":175}],34:[function(require,module,exports){
var SetCache = require('./_SetCache'),
    arrayIncludes = require('./_arrayIncludes'),
    arrayIncludesWith = require('./_arrayIncludesWith'),
    arrayMap = require('./_arrayMap'),
    baseUnary = require('./_baseUnary'),
    cacheHas = require('./_cacheHas');

/** Used as the size to enable large array optimizations. */
var LARGE_ARRAY_SIZE = 200;

/**
 * The base implementation of methods like `_.difference` without support
 * for excluding multiple arrays or iteratee shorthands.
 *
 * @private
 * @param {Array} array The array to inspect.
 * @param {Array} values The values to exclude.
 * @param {Function} [iteratee] The iteratee invoked per element.
 * @param {Function} [comparator] The comparator invoked per element.
 * @returns {Array} Returns the new array of filtered values.
 */
function baseDifference(array, values, iteratee, comparator) {
  var index = -1,
      includes = arrayIncludes,
      isCommon = true,
      length = array.length,
      result = [],
      valuesLength = values.length;

  if (!length) {
    return result;
  }
  if (iteratee) {
    values = arrayMap(values, baseUnary(iteratee));
  }
  if (comparator) {
    includes = arrayIncludesWith;
    isCommon = false;
  }
  else if (values.length >= LARGE_ARRAY_SIZE) {
    includes = cacheHas;
    isCommon = false;
    values = new SetCache(values);
  }
  outer:
  while (++index < length) {
    var value = array[index],
        computed = iteratee == null ? value : iteratee(value);

    value = (comparator || value !== 0) ? value : 0;
    if (isCommon && computed === computed) {
      var valuesIndex = valuesLength;
      while (valuesIndex--) {
        if (values[valuesIndex] === computed) {
          continue outer;
        }
      }
      result.push(value);
    }
    else if (!includes(values, computed, comparator)) {
      result.push(value);
    }
  }
  return result;
}

module.exports = baseDifference;

},{"./_SetCache":10,"./_arrayIncludes":20,"./_arrayIncludesWith":21,"./_arrayMap":23,"./_baseUnary":64,"./_cacheHas":66}],35:[function(require,module,exports){
var baseForOwn = require('./_baseForOwn'),
    createBaseEach = require('./_createBaseEach');

/**
 * The base implementation of `_.forEach` without support for iteratee shorthands.
 *
 * @private
 * @param {Array|Object} collection The collection to iterate over.
 * @param {Function} iteratee The function invoked per iteration.
 * @returns {Array|Object} Returns `collection`.
 */
var baseEach = createBaseEach(baseForOwn);

module.exports = baseEach;

},{"./_baseForOwn":39,"./_createBaseEach":82}],36:[function(require,module,exports){
/**
 * The base implementation of `_.findIndex` and `_.findLastIndex` without
 * support for iteratee shorthands.
 *
 * @private
 * @param {Array} array The array to inspect.
 * @param {Function} predicate The function invoked per iteration.
 * @param {number} fromIndex The index to search from.
 * @param {boolean} [fromRight] Specify iterating from right to left.
 * @returns {number} Returns the index of the matched value, else `-1`.
 */
function baseFindIndex(array, predicate, fromIndex, fromRight) {
  var length = array.length,
      index = fromIndex + (fromRight ? 1 : -1);

  while ((fromRight ? index-- : ++index < length)) {
    if (predicate(array[index], index, array)) {
      return index;
    }
  }
  return -1;
}

module.exports = baseFindIndex;

},{}],37:[function(require,module,exports){
var arrayPush = require('./_arrayPush'),
    isFlattenable = require('./_isFlattenable');

/**
 * The base implementation of `_.flatten` with support for restricting flattening.
 *
 * @private
 * @param {Array} array The array to flatten.
 * @param {number} depth The maximum recursion depth.
 * @param {boolean} [predicate=isFlattenable] The function invoked per iteration.
 * @param {boolean} [isStrict] Restrict to values that pass `predicate` checks.
 * @param {Array} [result=[]] The initial result value.
 * @returns {Array} Returns the new flattened array.
 */
function baseFlatten(array, depth, predicate, isStrict, result) {
  var index = -1,
      length = array.length;

  predicate || (predicate = isFlattenable);
  result || (result = []);

  while (++index < length) {
    var value = array[index];
    if (depth > 0 && predicate(value)) {
      if (depth > 1) {
        // Recursively flatten arrays (susceptible to call stack limits).
        baseFlatten(value, depth - 1, predicate, isStrict, result);
      } else {
        arrayPush(result, value);
      }
    } else if (!isStrict) {
      result[result.length] = value;
    }
  }
  return result;
}

module.exports = baseFlatten;

},{"./_arrayPush":24,"./_isFlattenable":111}],38:[function(require,module,exports){
var createBaseFor = require('./_createBaseFor');

/**
 * The base implementation of `baseForOwn` which iterates over `object`
 * properties returned by `keysFunc` and invokes `iteratee` for each property.
 * Iteratee functions may exit iteration early by explicitly returning `false`.
 *
 * @private
 * @param {Object} object The object to iterate over.
 * @param {Function} iteratee The function invoked per iteration.
 * @param {Function} keysFunc The function to get the keys of `object`.
 * @returns {Object} Returns `object`.
 */
var baseFor = createBaseFor();

module.exports = baseFor;

},{"./_createBaseFor":83}],39:[function(require,module,exports){
var baseFor = require('./_baseFor'),
    keys = require('./keys');

/**
 * The base implementation of `_.forOwn` without support for iteratee shorthands.
 *
 * @private
 * @param {Object} object The object to iterate over.
 * @param {Function} iteratee The function invoked per iteration.
 * @returns {Object} Returns `object`.
 */
function baseForOwn(object, iteratee) {
  return object && baseFor(object, iteratee, keys);
}

module.exports = baseForOwn;

},{"./_baseFor":38,"./keys":179}],40:[function(require,module,exports){
var castPath = require('./_castPath'),
    toKey = require('./_toKey');

/**
 * The base implementation of `_.get` without support for default values.
 *
 * @private
 * @param {Object} object The object to query.
 * @param {Array|string} path The path of the property to get.
 * @returns {*} Returns the resolved value.
 */
function baseGet(object, path) {
  path = castPath(path, object);

  var index = 0,
      length = path.length;

  while (object != null && index < length) {
    object = object[toKey(path[index++])];
  }
  return (index && index == length) ? object : undefined;
}

module.exports = baseGet;

},{"./_castPath":67,"./_toKey":152}],41:[function(require,module,exports){
var arrayPush = require('./_arrayPush'),
    isArray = require('./isArray');

/**
 * The base implementation of `getAllKeys` and `getAllKeysIn` which uses
 * `keysFunc` and `symbolsFunc` to get the enumerable property names and
 * symbols of `object`.
 *
 * @private
 * @param {Object} object The object to query.
 * @param {Function} keysFunc The function to get the keys of `object`.
 * @param {Function} symbolsFunc The function to get the symbols of `object`.
 * @returns {Array} Returns the array of property names and symbols.
 */
function baseGetAllKeys(object, keysFunc, symbolsFunc) {
  var result = keysFunc(object);
  return isArray(object) ? result : arrayPush(result, symbolsFunc(object));
}

module.exports = baseGetAllKeys;

},{"./_arrayPush":24,"./isArray":168}],42:[function(require,module,exports){
var Symbol = require('./_Symbol'),
    getRawTag = require('./_getRawTag'),
    objectToString = require('./_objectToString');

/** `Object#toString` result references. */
var nullTag = '[object Null]',
    undefinedTag = '[object Undefined]';

/** Built-in value references. */
var symToStringTag = Symbol ? Symbol.toStringTag : undefined;

/**
 * The base implementation of `getTag` without fallbacks for buggy environments.
 *
 * @private
 * @param {*} value The value to query.
 * @returns {string} Returns the `toStringTag`.
 */
function baseGetTag(value) {
  if (value == null) {
    return value === undefined ? undefinedTag : nullTag;
  }
  return (symToStringTag && symToStringTag in Object(value))
    ? getRawTag(value)
    : objectToString(value);
}

module.exports = baseGetTag;

},{"./_Symbol":12,"./_getRawTag":97,"./_objectToString":136}],43:[function(require,module,exports){
/**
 * The base implementation of `_.hasIn` without support for deep paths.
 *
 * @private
 * @param {Object} [object] The object to query.
 * @param {Array|string} key The key to check.
 * @returns {boolean} Returns `true` if `key` exists, else `false`.
 */
function baseHasIn(object, key) {
  return object != null && key in Object(object);
}

module.exports = baseHasIn;

},{}],44:[function(require,module,exports){
var baseFindIndex = require('./_baseFindIndex'),
    baseIsNaN = require('./_baseIsNaN'),
    strictIndexOf = require('./_strictIndexOf');

/**
 * The base implementation of `_.indexOf` without `fromIndex` bounds checks.
 *
 * @private
 * @param {Array} array The array to inspect.
 * @param {*} value The value to search for.
 * @param {number} fromIndex The index to search from.
 * @returns {number} Returns the index of the matched value, else `-1`.
 */
function baseIndexOf(array, value, fromIndex) {
  return value === value
    ? strictIndexOf(array, value, fromIndex)
    : baseFindIndex(array, baseIsNaN, fromIndex);
}

module.exports = baseIndexOf;

},{"./_baseFindIndex":36,"./_baseIsNaN":49,"./_strictIndexOf":150}],45:[function(require,module,exports){
var baseGetTag = require('./_baseGetTag'),
    isObjectLike = require('./isObjectLike');

/** `Object#toString` result references. */
var argsTag = '[object Arguments]';

/**
 * The base implementation of `_.isArguments`.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an `arguments` object,
 */
function baseIsArguments(value) {
  return isObjectLike(value) && baseGetTag(value) == argsTag;
}

module.exports = baseIsArguments;

},{"./_baseGetTag":42,"./isObjectLike":176}],46:[function(require,module,exports){
var baseIsEqualDeep = require('./_baseIsEqualDeep'),
    isObjectLike = require('./isObjectLike');

/**
 * The base implementation of `_.isEqual` which supports partial comparisons
 * and tracks traversed objects.
 *
 * @private
 * @param {*} value The value to compare.
 * @param {*} other The other value to compare.
 * @param {boolean} bitmask The bitmask flags.
 *  1 - Unordered comparison
 *  2 - Partial comparison
 * @param {Function} [customizer] The function to customize comparisons.
 * @param {Object} [stack] Tracks traversed `value` and `other` objects.
 * @returns {boolean} Returns `true` if the values are equivalent, else `false`.
 */
function baseIsEqual(value, other, bitmask, customizer, stack) {
  if (value === other) {
    return true;
  }
  if (value == null || other == null || (!isObjectLike(value) && !isObjectLike(other))) {
    return value !== value && other !== other;
  }
  return baseIsEqualDeep(value, other, bitmask, customizer, baseIsEqual, stack);
}

module.exports = baseIsEqual;

},{"./_baseIsEqualDeep":47,"./isObjectLike":176}],47:[function(require,module,exports){
var Stack = require('./_Stack'),
    equalArrays = require('./_equalArrays'),
    equalByTag = require('./_equalByTag'),
    equalObjects = require('./_equalObjects'),
    getTag = require('./_getTag'),
    isArray = require('./isArray'),
    isBuffer = require('./isBuffer'),
    isTypedArray = require('./isTypedArray');

/** Used to compose bitmasks for value comparisons. */
var COMPARE_PARTIAL_FLAG = 1;

/** `Object#toString` result references. */
var argsTag = '[object Arguments]',
    arrayTag = '[object Array]',
    objectTag = '[object Object]';

/** Used for built-in method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/**
 * A specialized version of `baseIsEqual` for arrays and objects which performs
 * deep comparisons and tracks traversed objects enabling objects with circular
 * references to be compared.
 *
 * @private
 * @param {Object} object The object to compare.
 * @param {Object} other The other object to compare.
 * @param {number} bitmask The bitmask flags. See `baseIsEqual` for more details.
 * @param {Function} customizer The function to customize comparisons.
 * @param {Function} equalFunc The function to determine equivalents of values.
 * @param {Object} [stack] Tracks traversed `object` and `other` objects.
 * @returns {boolean} Returns `true` if the objects are equivalent, else `false`.
 */
function baseIsEqualDeep(object, other, bitmask, customizer, equalFunc, stack) {
  var objIsArr = isArray(object),
      othIsArr = isArray(other),
      objTag = objIsArr ? arrayTag : getTag(object),
      othTag = othIsArr ? arrayTag : getTag(other);

  objTag = objTag == argsTag ? objectTag : objTag;
  othTag = othTag == argsTag ? objectTag : othTag;

  var objIsObj = objTag == objectTag,
      othIsObj = othTag == objectTag,
      isSameTag = objTag == othTag;

  if (isSameTag && isBuffer(object)) {
    if (!isBuffer(other)) {
      return false;
    }
    objIsArr = true;
    objIsObj = false;
  }
  if (isSameTag && !objIsObj) {
    stack || (stack = new Stack);
    return (objIsArr || isTypedArray(object))
      ? equalArrays(object, other, bitmask, customizer, equalFunc, stack)
      : equalByTag(object, other, objTag, bitmask, customizer, equalFunc, stack);
  }
  if (!(bitmask & COMPARE_PARTIAL_FLAG)) {
    var objIsWrapped = objIsObj && hasOwnProperty.call(object, '__wrapped__'),
        othIsWrapped = othIsObj && hasOwnProperty.call(other, '__wrapped__');

    if (objIsWrapped || othIsWrapped) {
      var objUnwrapped = objIsWrapped ? object.value() : object,
          othUnwrapped = othIsWrapped ? other.value() : other;

      stack || (stack = new Stack);
      return equalFunc(objUnwrapped, othUnwrapped, bitmask, customizer, stack);
    }
  }
  if (!isSameTag) {
    return false;
  }
  stack || (stack = new Stack);
  return equalObjects(object, other, bitmask, customizer, equalFunc, stack);
}

module.exports = baseIsEqualDeep;

},{"./_Stack":11,"./_equalArrays":87,"./_equalByTag":88,"./_equalObjects":89,"./_getTag":100,"./isArray":168,"./isBuffer":171,"./isTypedArray":178}],48:[function(require,module,exports){
var Stack = require('./_Stack'),
    baseIsEqual = require('./_baseIsEqual');

/** Used to compose bitmasks for value comparisons. */
var COMPARE_PARTIAL_FLAG = 1,
    COMPARE_UNORDERED_FLAG = 2;

/**
 * The base implementation of `_.isMatch` without support for iteratee shorthands.
 *
 * @private
 * @param {Object} object The object to inspect.
 * @param {Object} source The object of property values to match.
 * @param {Array} matchData The property names, values, and compare flags to match.
 * @param {Function} [customizer] The function to customize comparisons.
 * @returns {boolean} Returns `true` if `object` is a match, else `false`.
 */
function baseIsMatch(object, source, matchData, customizer) {
  var index = matchData.length,
      length = index,
      noCustomizer = !customizer;

  if (object == null) {
    return !length;
  }
  object = Object(object);
  while (index--) {
    var data = matchData[index];
    if ((noCustomizer && data[2])
          ? data[1] !== object[data[0]]
          : !(data[0] in object)
        ) {
      return false;
    }
  }
  while (++index < length) {
    data = matchData[index];
    var key = data[0],
        objValue = object[key],
        srcValue = data[1];

    if (noCustomizer && data[2]) {
      if (objValue === undefined && !(key in object)) {
        return false;
      }
    } else {
      var stack = new Stack;
      if (customizer) {
        var result = customizer(objValue, srcValue, key, object, source, stack);
      }
      if (!(result === undefined
            ? baseIsEqual(srcValue, objValue, COMPARE_PARTIAL_FLAG | COMPARE_UNORDERED_FLAG, customizer, stack)
            : result
          )) {
        return false;
      }
    }
  }
  return true;
}

module.exports = baseIsMatch;

},{"./_Stack":11,"./_baseIsEqual":46}],49:[function(require,module,exports){
/**
 * The base implementation of `_.isNaN` without support for number objects.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is `NaN`, else `false`.
 */
function baseIsNaN(value) {
  return value !== value;
}

module.exports = baseIsNaN;

},{}],50:[function(require,module,exports){
var isFunction = require('./isFunction'),
    isMasked = require('./_isMasked'),
    isObject = require('./isObject'),
    toSource = require('./_toSource');

/**
 * Used to match `RegExp`
 * [syntax characters](http://ecma-international.org/ecma-262/7.0/#sec-patterns).
 */
var reRegExpChar = /[\\^$.*+?()[\]{}|]/g;

/** Used to detect host constructors (Safari). */
var reIsHostCtor = /^\[object .+?Constructor\]$/;

/** Used for built-in method references. */
var funcProto = Function.prototype,
    objectProto = Object.prototype;

/** Used to resolve the decompiled source of functions. */
var funcToString = funcProto.toString;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/** Used to detect if a method is native. */
var reIsNative = RegExp('^' +
  funcToString.call(hasOwnProperty).replace(reRegExpChar, '\\$&')
  .replace(/hasOwnProperty|(function).*?(?=\\\()| for .+?(?=\\\])/g, '$1.*?') + '$'
);

/**
 * The base implementation of `_.isNative` without bad shim checks.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a native function,
 *  else `false`.
 */
function baseIsNative(value) {
  if (!isObject(value) || isMasked(value)) {
    return false;
  }
  var pattern = isFunction(value) ? reIsNative : reIsHostCtor;
  return pattern.test(toSource(value));
}

module.exports = baseIsNative;

},{"./_isMasked":116,"./_toSource":153,"./isFunction":173,"./isObject":175}],51:[function(require,module,exports){
var baseGetTag = require('./_baseGetTag'),
    isLength = require('./isLength'),
    isObjectLike = require('./isObjectLike');

/** `Object#toString` result references. */
var argsTag = '[object Arguments]',
    arrayTag = '[object Array]',
    boolTag = '[object Boolean]',
    dateTag = '[object Date]',
    errorTag = '[object Error]',
    funcTag = '[object Function]',
    mapTag = '[object Map]',
    numberTag = '[object Number]',
    objectTag = '[object Object]',
    regexpTag = '[object RegExp]',
    setTag = '[object Set]',
    stringTag = '[object String]',
    weakMapTag = '[object WeakMap]';

var arrayBufferTag = '[object ArrayBuffer]',
    dataViewTag = '[object DataView]',
    float32Tag = '[object Float32Array]',
    float64Tag = '[object Float64Array]',
    int8Tag = '[object Int8Array]',
    int16Tag = '[object Int16Array]',
    int32Tag = '[object Int32Array]',
    uint8Tag = '[object Uint8Array]',
    uint8ClampedTag = '[object Uint8ClampedArray]',
    uint16Tag = '[object Uint16Array]',
    uint32Tag = '[object Uint32Array]';

/** Used to identify `toStringTag` values of typed arrays. */
var typedArrayTags = {};
typedArrayTags[float32Tag] = typedArrayTags[float64Tag] =
typedArrayTags[int8Tag] = typedArrayTags[int16Tag] =
typedArrayTags[int32Tag] = typedArrayTags[uint8Tag] =
typedArrayTags[uint8ClampedTag] = typedArrayTags[uint16Tag] =
typedArrayTags[uint32Tag] = true;
typedArrayTags[argsTag] = typedArrayTags[arrayTag] =
typedArrayTags[arrayBufferTag] = typedArrayTags[boolTag] =
typedArrayTags[dataViewTag] = typedArrayTags[dateTag] =
typedArrayTags[errorTag] = typedArrayTags[funcTag] =
typedArrayTags[mapTag] = typedArrayTags[numberTag] =
typedArrayTags[objectTag] = typedArrayTags[regexpTag] =
typedArrayTags[setTag] = typedArrayTags[stringTag] =
typedArrayTags[weakMapTag] = false;

/**
 * The base implementation of `_.isTypedArray` without Node.js optimizations.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a typed array, else `false`.
 */
function baseIsTypedArray(value) {
  return isObjectLike(value) &&
    isLength(value.length) && !!typedArrayTags[baseGetTag(value)];
}

module.exports = baseIsTypedArray;

},{"./_baseGetTag":42,"./isLength":174,"./isObjectLike":176}],52:[function(require,module,exports){
var baseMatches = require('./_baseMatches'),
    baseMatchesProperty = require('./_baseMatchesProperty'),
    identity = require('./identity'),
    isArray = require('./isArray'),
    property = require('./property');

/**
 * The base implementation of `_.iteratee`.
 *
 * @private
 * @param {*} [value=_.identity] The value to convert to an iteratee.
 * @returns {Function} Returns the iteratee.
 */
function baseIteratee(value) {
  // Don't store the `typeof` result in a variable to avoid a JIT bug in Safari 9.
  // See https://bugs.webkit.org/show_bug.cgi?id=156034 for more details.
  if (typeof value == 'function') {
    return value;
  }
  if (value == null) {
    return identity;
  }
  if (typeof value == 'object') {
    return isArray(value)
      ? baseMatchesProperty(value[0], value[1])
      : baseMatches(value);
  }
  return property(value);
}

module.exports = baseIteratee;

},{"./_baseMatches":55,"./_baseMatchesProperty":56,"./identity":166,"./isArray":168,"./property":184}],53:[function(require,module,exports){
var isPrototype = require('./_isPrototype'),
    nativeKeys = require('./_nativeKeys');

/** Used for built-in method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/**
 * The base implementation of `_.keys` which doesn't treat sparse arrays as dense.
 *
 * @private
 * @param {Object} object The object to query.
 * @returns {Array} Returns the array of property names.
 */
function baseKeys(object) {
  if (!isPrototype(object)) {
    return nativeKeys(object);
  }
  var result = [];
  for (var key in Object(object)) {
    if (hasOwnProperty.call(object, key) && key != 'constructor') {
      result.push(key);
    }
  }
  return result;
}

module.exports = baseKeys;

},{"./_isPrototype":117,"./_nativeKeys":133}],54:[function(require,module,exports){
var isObject = require('./isObject'),
    isPrototype = require('./_isPrototype'),
    nativeKeysIn = require('./_nativeKeysIn');

/** Used for built-in method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/**
 * The base implementation of `_.keysIn` which doesn't treat sparse arrays as dense.
 *
 * @private
 * @param {Object} object The object to query.
 * @returns {Array} Returns the array of property names.
 */
function baseKeysIn(object) {
  if (!isObject(object)) {
    return nativeKeysIn(object);
  }
  var isProto = isPrototype(object),
      result = [];

  for (var key in object) {
    if (!(key == 'constructor' && (isProto || !hasOwnProperty.call(object, key)))) {
      result.push(key);
    }
  }
  return result;
}

module.exports = baseKeysIn;

},{"./_isPrototype":117,"./_nativeKeysIn":134,"./isObject":175}],55:[function(require,module,exports){
var baseIsMatch = require('./_baseIsMatch'),
    getMatchData = require('./_getMatchData'),
    matchesStrictComparable = require('./_matchesStrictComparable');

/**
 * The base implementation of `_.matches` which doesn't clone `source`.
 *
 * @private
 * @param {Object} source The object of property values to match.
 * @returns {Function} Returns the new spec function.
 */
function baseMatches(source) {
  var matchData = getMatchData(source);
  if (matchData.length == 1 && matchData[0][2]) {
    return matchesStrictComparable(matchData[0][0], matchData[0][1]);
  }
  return function(object) {
    return object === source || baseIsMatch(object, source, matchData);
  };
}

module.exports = baseMatches;

},{"./_baseIsMatch":48,"./_getMatchData":94,"./_matchesStrictComparable":130}],56:[function(require,module,exports){
var baseIsEqual = require('./_baseIsEqual'),
    get = require('./get'),
    hasIn = require('./hasIn'),
    isKey = require('./_isKey'),
    isStrictComparable = require('./_isStrictComparable'),
    matchesStrictComparable = require('./_matchesStrictComparable'),
    toKey = require('./_toKey');

/** Used to compose bitmasks for value comparisons. */
var COMPARE_PARTIAL_FLAG = 1,
    COMPARE_UNORDERED_FLAG = 2;

/**
 * The base implementation of `_.matchesProperty` which doesn't clone `srcValue`.
 *
 * @private
 * @param {string} path The path of the property to get.
 * @param {*} srcValue The value to match.
 * @returns {Function} Returns the new spec function.
 */
function baseMatchesProperty(path, srcValue) {
  if (isKey(path) && isStrictComparable(srcValue)) {
    return matchesStrictComparable(toKey(path), srcValue);
  }
  return function(object) {
    var objValue = get(object, path);
    return (objValue === undefined && objValue === srcValue)
      ? hasIn(object, path)
      : baseIsEqual(srcValue, objValue, COMPARE_PARTIAL_FLAG | COMPARE_UNORDERED_FLAG);
  };
}

module.exports = baseMatchesProperty;

},{"./_baseIsEqual":46,"./_isKey":114,"./_isStrictComparable":118,"./_matchesStrictComparable":130,"./_toKey":152,"./get":164,"./hasIn":165}],57:[function(require,module,exports){
/**
 * The base implementation of `_.property` without support for deep paths.
 *
 * @private
 * @param {string} key The key of the property to get.
 * @returns {Function} Returns the new accessor function.
 */
function baseProperty(key) {
  return function(object) {
    return object == null ? undefined : object[key];
  };
}

module.exports = baseProperty;

},{}],58:[function(require,module,exports){
var baseGet = require('./_baseGet');

/**
 * A specialized version of `baseProperty` which supports deep paths.
 *
 * @private
 * @param {Array|string} path The path of the property to get.
 * @returns {Function} Returns the new accessor function.
 */
function basePropertyDeep(path) {
  return function(object) {
    return baseGet(object, path);
  };
}

module.exports = basePropertyDeep;

},{"./_baseGet":40}],59:[function(require,module,exports){
/**
 * The base implementation of `_.reduce` and `_.reduceRight`, without support
 * for iteratee shorthands, which iterates over `collection` using `eachFunc`.
 *
 * @private
 * @param {Array|Object} collection The collection to iterate over.
 * @param {Function} iteratee The function invoked per iteration.
 * @param {*} accumulator The initial value.
 * @param {boolean} initAccum Specify using the first or last element of
 *  `collection` as the initial value.
 * @param {Function} eachFunc The function to iterate over `collection`.
 * @returns {*} Returns the accumulated value.
 */
function baseReduce(collection, iteratee, accumulator, initAccum, eachFunc) {
  eachFunc(collection, function(value, index, collection) {
    accumulator = initAccum
      ? (initAccum = false, value)
      : iteratee(accumulator, value, index, collection);
  });
  return accumulator;
}

module.exports = baseReduce;

},{}],60:[function(require,module,exports){
var identity = require('./identity'),
    overRest = require('./_overRest'),
    setToString = require('./_setToString');

/**
 * The base implementation of `_.rest` which doesn't validate or coerce arguments.
 *
 * @private
 * @param {Function} func The function to apply a rest parameter to.
 * @param {number} [start=func.length-1] The start position of the rest parameter.
 * @returns {Function} Returns the new function.
 */
function baseRest(func, start) {
  return setToString(overRest(func, start, identity), func + '');
}

module.exports = baseRest;

},{"./_overRest":138,"./_setToString":143,"./identity":166}],61:[function(require,module,exports){
var constant = require('./constant'),
    defineProperty = require('./_defineProperty'),
    identity = require('./identity');

/**
 * The base implementation of `setToString` without support for hot loop shorting.
 *
 * @private
 * @param {Function} func The function to modify.
 * @param {Function} string The `toString` result.
 * @returns {Function} Returns `func`.
 */
var baseSetToString = !defineProperty ? identity : function(func, string) {
  return defineProperty(func, 'toString', {
    'configurable': true,
    'enumerable': false,
    'value': constant(string),
    'writable': true
  });
};

module.exports = baseSetToString;

},{"./_defineProperty":86,"./constant":157,"./identity":166}],62:[function(require,module,exports){
/**
 * The base implementation of `_.times` without support for iteratee shorthands
 * or max array length checks.
 *
 * @private
 * @param {number} n The number of times to invoke `iteratee`.
 * @param {Function} iteratee The function invoked per iteration.
 * @returns {Array} Returns the array of results.
 */
function baseTimes(n, iteratee) {
  var index = -1,
      result = Array(n);

  while (++index < n) {
    result[index] = iteratee(index);
  }
  return result;
}

module.exports = baseTimes;

},{}],63:[function(require,module,exports){
var Symbol = require('./_Symbol'),
    arrayMap = require('./_arrayMap'),
    isArray = require('./isArray'),
    isSymbol = require('./isSymbol');

/** Used as references for various `Number` constants. */
var INFINITY = 1 / 0;

/** Used to convert symbols to primitives and strings. */
var symbolProto = Symbol ? Symbol.prototype : undefined,
    symbolToString = symbolProto ? symbolProto.toString : undefined;

/**
 * The base implementation of `_.toString` which doesn't convert nullish
 * values to empty strings.
 *
 * @private
 * @param {*} value The value to process.
 * @returns {string} Returns the string.
 */
function baseToString(value) {
  // Exit early for strings to avoid a performance hit in some environments.
  if (typeof value == 'string') {
    return value;
  }
  if (isArray(value)) {
    // Recursively convert values (susceptible to call stack limits).
    return arrayMap(value, baseToString) + '';
  }
  if (isSymbol(value)) {
    return symbolToString ? symbolToString.call(value) : '';
  }
  var result = (value + '');
  return (result == '0' && (1 / value) == -INFINITY) ? '-0' : result;
}

module.exports = baseToString;

},{"./_Symbol":12,"./_arrayMap":23,"./isArray":168,"./isSymbol":177}],64:[function(require,module,exports){
/**
 * The base implementation of `_.unary` without support for storing metadata.
 *
 * @private
 * @param {Function} func The function to cap arguments for.
 * @returns {Function} Returns the new capped function.
 */
function baseUnary(func) {
  return function(value) {
    return func(value);
  };
}

module.exports = baseUnary;

},{}],65:[function(require,module,exports){
var SetCache = require('./_SetCache'),
    arrayIncludes = require('./_arrayIncludes'),
    arrayIncludesWith = require('./_arrayIncludesWith'),
    cacheHas = require('./_cacheHas'),
    createSet = require('./_createSet'),
    setToArray = require('./_setToArray');

/** Used as the size to enable large array optimizations. */
var LARGE_ARRAY_SIZE = 200;

/**
 * The base implementation of `_.uniqBy` without support for iteratee shorthands.
 *
 * @private
 * @param {Array} array The array to inspect.
 * @param {Function} [iteratee] The iteratee invoked per element.
 * @param {Function} [comparator] The comparator invoked per element.
 * @returns {Array} Returns the new duplicate free array.
 */
function baseUniq(array, iteratee, comparator) {
  var index = -1,
      includes = arrayIncludes,
      length = array.length,
      isCommon = true,
      result = [],
      seen = result;

  if (comparator) {
    isCommon = false;
    includes = arrayIncludesWith;
  }
  else if (length >= LARGE_ARRAY_SIZE) {
    var set = iteratee ? null : createSet(array);
    if (set) {
      return setToArray(set);
    }
    isCommon = false;
    includes = cacheHas;
    seen = new SetCache;
  }
  else {
    seen = iteratee ? [] : result;
  }
  outer:
  while (++index < length) {
    var value = array[index],
        computed = iteratee ? iteratee(value) : value;

    value = (comparator || value !== 0) ? value : 0;
    if (isCommon && computed === computed) {
      var seenIndex = seen.length;
      while (seenIndex--) {
        if (seen[seenIndex] === computed) {
          continue outer;
        }
      }
      if (iteratee) {
        seen.push(computed);
      }
      result.push(value);
    }
    else if (!includes(seen, computed, comparator)) {
      if (seen !== result) {
        seen.push(computed);
      }
      result.push(value);
    }
  }
  return result;
}

module.exports = baseUniq;

},{"./_SetCache":10,"./_arrayIncludes":20,"./_arrayIncludesWith":21,"./_cacheHas":66,"./_createSet":85,"./_setToArray":142}],66:[function(require,module,exports){
/**
 * Checks if a `cache` value for `key` exists.
 *
 * @private
 * @param {Object} cache The cache to query.
 * @param {string} key The key of the entry to check.
 * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
 */
function cacheHas(cache, key) {
  return cache.has(key);
}

module.exports = cacheHas;

},{}],67:[function(require,module,exports){
var isArray = require('./isArray'),
    isKey = require('./_isKey'),
    stringToPath = require('./_stringToPath'),
    toString = require('./toString');

/**
 * Casts `value` to a path array if it's not one.
 *
 * @private
 * @param {*} value The value to inspect.
 * @param {Object} [object] The object to query keys on.
 * @returns {Array} Returns the cast property path array.
 */
function castPath(value, object) {
  if (isArray(value)) {
    return value;
  }
  return isKey(value, object) ? [value] : stringToPath(toString(value));
}

module.exports = castPath;

},{"./_isKey":114,"./_stringToPath":151,"./isArray":168,"./toString":192}],68:[function(require,module,exports){
var Uint8Array = require('./_Uint8Array');

/**
 * Creates a clone of `arrayBuffer`.
 *
 * @private
 * @param {ArrayBuffer} arrayBuffer The array buffer to clone.
 * @returns {ArrayBuffer} Returns the cloned array buffer.
 */
function cloneArrayBuffer(arrayBuffer) {
  var result = new arrayBuffer.constructor(arrayBuffer.byteLength);
  new Uint8Array(result).set(new Uint8Array(arrayBuffer));
  return result;
}

module.exports = cloneArrayBuffer;

},{"./_Uint8Array":13}],69:[function(require,module,exports){
var root = require('./_root');

/** Detect free variable `exports`. */
var freeExports = typeof exports == 'object' && exports && !exports.nodeType && exports;

/** Detect free variable `module`. */
var freeModule = freeExports && typeof module == 'object' && module && !module.nodeType && module;

/** Detect the popular CommonJS extension `module.exports`. */
var moduleExports = freeModule && freeModule.exports === freeExports;

/** Built-in value references. */
var Buffer = moduleExports ? root.Buffer : undefined,
    allocUnsafe = Buffer ? Buffer.allocUnsafe : undefined;

/**
 * Creates a clone of  `buffer`.
 *
 * @private
 * @param {Buffer} buffer The buffer to clone.
 * @param {boolean} [isDeep] Specify a deep clone.
 * @returns {Buffer} Returns the cloned buffer.
 */
function cloneBuffer(buffer, isDeep) {
  if (isDeep) {
    return buffer.slice();
  }
  var length = buffer.length,
      result = allocUnsafe ? allocUnsafe(length) : new buffer.constructor(length);

  buffer.copy(result);
  return result;
}

module.exports = cloneBuffer;

},{"./_root":139}],70:[function(require,module,exports){
var cloneArrayBuffer = require('./_cloneArrayBuffer');

/**
 * Creates a clone of `dataView`.
 *
 * @private
 * @param {Object} dataView The data view to clone.
 * @param {boolean} [isDeep] Specify a deep clone.
 * @returns {Object} Returns the cloned data view.
 */
function cloneDataView(dataView, isDeep) {
  var buffer = isDeep ? cloneArrayBuffer(dataView.buffer) : dataView.buffer;
  return new dataView.constructor(buffer, dataView.byteOffset, dataView.byteLength);
}

module.exports = cloneDataView;

},{"./_cloneArrayBuffer":68}],71:[function(require,module,exports){
var addMapEntry = require('./_addMapEntry'),
    arrayReduce = require('./_arrayReduce'),
    mapToArray = require('./_mapToArray');

/** Used to compose bitmasks for cloning. */
var CLONE_DEEP_FLAG = 1;

/**
 * Creates a clone of `map`.
 *
 * @private
 * @param {Object} map The map to clone.
 * @param {Function} cloneFunc The function to clone values.
 * @param {boolean} [isDeep] Specify a deep clone.
 * @returns {Object} Returns the cloned map.
 */
function cloneMap(map, isDeep, cloneFunc) {
  var array = isDeep ? cloneFunc(mapToArray(map), CLONE_DEEP_FLAG) : mapToArray(map);
  return arrayReduce(array, addMapEntry, new map.constructor);
}

module.exports = cloneMap;

},{"./_addMapEntry":15,"./_arrayReduce":25,"./_mapToArray":129}],72:[function(require,module,exports){
/** Used to match `RegExp` flags from their coerced string values. */
var reFlags = /\w*$/;

/**
 * Creates a clone of `regexp`.
 *
 * @private
 * @param {Object} regexp The regexp to clone.
 * @returns {Object} Returns the cloned regexp.
 */
function cloneRegExp(regexp) {
  var result = new regexp.constructor(regexp.source, reFlags.exec(regexp));
  result.lastIndex = regexp.lastIndex;
  return result;
}

module.exports = cloneRegExp;

},{}],73:[function(require,module,exports){
var addSetEntry = require('./_addSetEntry'),
    arrayReduce = require('./_arrayReduce'),
    setToArray = require('./_setToArray');

/** Used to compose bitmasks for cloning. */
var CLONE_DEEP_FLAG = 1;

/**
 * Creates a clone of `set`.
 *
 * @private
 * @param {Object} set The set to clone.
 * @param {Function} cloneFunc The function to clone values.
 * @param {boolean} [isDeep] Specify a deep clone.
 * @returns {Object} Returns the cloned set.
 */
function cloneSet(set, isDeep, cloneFunc) {
  var array = isDeep ? cloneFunc(setToArray(set), CLONE_DEEP_FLAG) : setToArray(set);
  return arrayReduce(array, addSetEntry, new set.constructor);
}

module.exports = cloneSet;

},{"./_addSetEntry":16,"./_arrayReduce":25,"./_setToArray":142}],74:[function(require,module,exports){
var Symbol = require('./_Symbol');

/** Used to convert symbols to primitives and strings. */
var symbolProto = Symbol ? Symbol.prototype : undefined,
    symbolValueOf = symbolProto ? symbolProto.valueOf : undefined;

/**
 * Creates a clone of the `symbol` object.
 *
 * @private
 * @param {Object} symbol The symbol object to clone.
 * @returns {Object} Returns the cloned symbol object.
 */
function cloneSymbol(symbol) {
  return symbolValueOf ? Object(symbolValueOf.call(symbol)) : {};
}

module.exports = cloneSymbol;

},{"./_Symbol":12}],75:[function(require,module,exports){
var cloneArrayBuffer = require('./_cloneArrayBuffer');

/**
 * Creates a clone of `typedArray`.
 *
 * @private
 * @param {Object} typedArray The typed array to clone.
 * @param {boolean} [isDeep] Specify a deep clone.
 * @returns {Object} Returns the cloned typed array.
 */
function cloneTypedArray(typedArray, isDeep) {
  var buffer = isDeep ? cloneArrayBuffer(typedArray.buffer) : typedArray.buffer;
  return new typedArray.constructor(buffer, typedArray.byteOffset, typedArray.length);
}

module.exports = cloneTypedArray;

},{"./_cloneArrayBuffer":68}],76:[function(require,module,exports){
/**
 * Copies the values of `source` to `array`.
 *
 * @private
 * @param {Array} source The array to copy values from.
 * @param {Array} [array=[]] The array to copy values to.
 * @returns {Array} Returns `array`.
 */
function copyArray(source, array) {
  var index = -1,
      length = source.length;

  array || (array = Array(length));
  while (++index < length) {
    array[index] = source[index];
  }
  return array;
}

module.exports = copyArray;

},{}],77:[function(require,module,exports){
var assignValue = require('./_assignValue'),
    baseAssignValue = require('./_baseAssignValue');

/**
 * Copies properties of `source` to `object`.
 *
 * @private
 * @param {Object} source The object to copy properties from.
 * @param {Array} props The property identifiers to copy.
 * @param {Object} [object={}] The object to copy properties to.
 * @param {Function} [customizer] The function to customize copied values.
 * @returns {Object} Returns `object`.
 */
function copyObject(source, props, object, customizer) {
  var isNew = !object;
  object || (object = {});

  var index = -1,
      length = props.length;

  while (++index < length) {
    var key = props[index];

    var newValue = customizer
      ? customizer(object[key], source[key], key, object, source)
      : undefined;

    if (newValue === undefined) {
      newValue = source[key];
    }
    if (isNew) {
      baseAssignValue(object, key, newValue);
    } else {
      assignValue(object, key, newValue);
    }
  }
  return object;
}

module.exports = copyObject;

},{"./_assignValue":27,"./_baseAssignValue":31}],78:[function(require,module,exports){
var copyObject = require('./_copyObject'),
    getSymbols = require('./_getSymbols');

/**
 * Copies own symbols of `source` to `object`.
 *
 * @private
 * @param {Object} source The object to copy symbols from.
 * @param {Object} [object={}] The object to copy symbols to.
 * @returns {Object} Returns `object`.
 */
function copySymbols(source, object) {
  return copyObject(source, getSymbols(source), object);
}

module.exports = copySymbols;

},{"./_copyObject":77,"./_getSymbols":98}],79:[function(require,module,exports){
var copyObject = require('./_copyObject'),
    getSymbolsIn = require('./_getSymbolsIn');

/**
 * Copies own and inherited symbols of `source` to `object`.
 *
 * @private
 * @param {Object} source The object to copy symbols from.
 * @param {Object} [object={}] The object to copy symbols to.
 * @returns {Object} Returns `object`.
 */
function copySymbolsIn(source, object) {
  return copyObject(source, getSymbolsIn(source), object);
}

module.exports = copySymbolsIn;

},{"./_copyObject":77,"./_getSymbolsIn":99}],80:[function(require,module,exports){
var root = require('./_root');

/** Used to detect overreaching core-js shims. */
var coreJsData = root['__core-js_shared__'];

module.exports = coreJsData;

},{"./_root":139}],81:[function(require,module,exports){
var baseRest = require('./_baseRest'),
    isIterateeCall = require('./_isIterateeCall');

/**
 * Creates a function like `_.assign`.
 *
 * @private
 * @param {Function} assigner The function to assign values.
 * @returns {Function} Returns the new assigner function.
 */
function createAssigner(assigner) {
  return baseRest(function(object, sources) {
    var index = -1,
        length = sources.length,
        customizer = length > 1 ? sources[length - 1] : undefined,
        guard = length > 2 ? sources[2] : undefined;

    customizer = (assigner.length > 3 && typeof customizer == 'function')
      ? (length--, customizer)
      : undefined;

    if (guard && isIterateeCall(sources[0], sources[1], guard)) {
      customizer = length < 3 ? undefined : customizer;
      length = 1;
    }
    object = Object(object);
    while (++index < length) {
      var source = sources[index];
      if (source) {
        assigner(object, source, index, customizer);
      }
    }
    return object;
  });
}

module.exports = createAssigner;

},{"./_baseRest":60,"./_isIterateeCall":113}],82:[function(require,module,exports){
var isArrayLike = require('./isArrayLike');

/**
 * Creates a `baseEach` or `baseEachRight` function.
 *
 * @private
 * @param {Function} eachFunc The function to iterate over a collection.
 * @param {boolean} [fromRight] Specify iterating from right to left.
 * @returns {Function} Returns the new base function.
 */
function createBaseEach(eachFunc, fromRight) {
  return function(collection, iteratee) {
    if (collection == null) {
      return collection;
    }
    if (!isArrayLike(collection)) {
      return eachFunc(collection, iteratee);
    }
    var length = collection.length,
        index = fromRight ? length : -1,
        iterable = Object(collection);

    while ((fromRight ? index-- : ++index < length)) {
      if (iteratee(iterable[index], index, iterable) === false) {
        break;
      }
    }
    return collection;
  };
}

module.exports = createBaseEach;

},{"./isArrayLike":169}],83:[function(require,module,exports){
/**
 * Creates a base function for methods like `_.forIn` and `_.forOwn`.
 *
 * @private
 * @param {boolean} [fromRight] Specify iterating from right to left.
 * @returns {Function} Returns the new base function.
 */
function createBaseFor(fromRight) {
  return function(object, iteratee, keysFunc) {
    var index = -1,
        iterable = Object(object),
        props = keysFunc(object),
        length = props.length;

    while (length--) {
      var key = props[fromRight ? length : ++index];
      if (iteratee(iterable[key], key, iterable) === false) {
        break;
      }
    }
    return object;
  };
}

module.exports = createBaseFor;

},{}],84:[function(require,module,exports){
var baseIteratee = require('./_baseIteratee'),
    isArrayLike = require('./isArrayLike'),
    keys = require('./keys');

/**
 * Creates a `_.find` or `_.findLast` function.
 *
 * @private
 * @param {Function} findIndexFunc The function to find the collection index.
 * @returns {Function} Returns the new find function.
 */
function createFind(findIndexFunc) {
  return function(collection, predicate, fromIndex) {
    var iterable = Object(collection);
    if (!isArrayLike(collection)) {
      var iteratee = baseIteratee(predicate, 3);
      collection = keys(collection);
      predicate = function(key) { return iteratee(iterable[key], key, iterable); };
    }
    var index = findIndexFunc(collection, predicate, fromIndex);
    return index > -1 ? iterable[iteratee ? collection[index] : index] : undefined;
  };
}

module.exports = createFind;

},{"./_baseIteratee":52,"./isArrayLike":169,"./keys":179}],85:[function(require,module,exports){
var Set = require('./_Set'),
    noop = require('./noop'),
    setToArray = require('./_setToArray');

/** Used as references for various `Number` constants. */
var INFINITY = 1 / 0;

/**
 * Creates a set object of `values`.
 *
 * @private
 * @param {Array} values The values to add to the set.
 * @returns {Object} Returns the new set.
 */
var createSet = !(Set && (1 / setToArray(new Set([,-0]))[1]) == INFINITY) ? noop : function(values) {
  return new Set(values);
};

module.exports = createSet;

},{"./_Set":9,"./_setToArray":142,"./noop":182}],86:[function(require,module,exports){
var getNative = require('./_getNative');

var defineProperty = (function() {
  try {
    var func = getNative(Object, 'defineProperty');
    func({}, '', {});
    return func;
  } catch (e) {}
}());

module.exports = defineProperty;

},{"./_getNative":95}],87:[function(require,module,exports){
var SetCache = require('./_SetCache'),
    arraySome = require('./_arraySome'),
    cacheHas = require('./_cacheHas');

/** Used to compose bitmasks for value comparisons. */
var COMPARE_PARTIAL_FLAG = 1,
    COMPARE_UNORDERED_FLAG = 2;

/**
 * A specialized version of `baseIsEqualDeep` for arrays with support for
 * partial deep comparisons.
 *
 * @private
 * @param {Array} array The array to compare.
 * @param {Array} other The other array to compare.
 * @param {number} bitmask The bitmask flags. See `baseIsEqual` for more details.
 * @param {Function} customizer The function to customize comparisons.
 * @param {Function} equalFunc The function to determine equivalents of values.
 * @param {Object} stack Tracks traversed `array` and `other` objects.
 * @returns {boolean} Returns `true` if the arrays are equivalent, else `false`.
 */
function equalArrays(array, other, bitmask, customizer, equalFunc, stack) {
  var isPartial = bitmask & COMPARE_PARTIAL_FLAG,
      arrLength = array.length,
      othLength = other.length;

  if (arrLength != othLength && !(isPartial && othLength > arrLength)) {
    return false;
  }
  // Assume cyclic values are equal.
  var stacked = stack.get(array);
  if (stacked && stack.get(other)) {
    return stacked == other;
  }
  var index = -1,
      result = true,
      seen = (bitmask & COMPARE_UNORDERED_FLAG) ? new SetCache : undefined;

  stack.set(array, other);
  stack.set(other, array);

  // Ignore non-index properties.
  while (++index < arrLength) {
    var arrValue = array[index],
        othValue = other[index];

    if (customizer) {
      var compared = isPartial
        ? customizer(othValue, arrValue, index, other, array, stack)
        : customizer(arrValue, othValue, index, array, other, stack);
    }
    if (compared !== undefined) {
      if (compared) {
        continue;
      }
      result = false;
      break;
    }
    // Recursively compare arrays (susceptible to call stack limits).
    if (seen) {
      if (!arraySome(other, function(othValue, othIndex) {
            if (!cacheHas(seen, othIndex) &&
                (arrValue === othValue || equalFunc(arrValue, othValue, bitmask, customizer, stack))) {
              return seen.push(othIndex);
            }
          })) {
        result = false;
        break;
      }
    } else if (!(
          arrValue === othValue ||
            equalFunc(arrValue, othValue, bitmask, customizer, stack)
        )) {
      result = false;
      break;
    }
  }
  stack['delete'](array);
  stack['delete'](other);
  return result;
}

module.exports = equalArrays;

},{"./_SetCache":10,"./_arraySome":26,"./_cacheHas":66}],88:[function(require,module,exports){
var Symbol = require('./_Symbol'),
    Uint8Array = require('./_Uint8Array'),
    eq = require('./eq'),
    equalArrays = require('./_equalArrays'),
    mapToArray = require('./_mapToArray'),
    setToArray = require('./_setToArray');

/** Used to compose bitmasks for value comparisons. */
var COMPARE_PARTIAL_FLAG = 1,
    COMPARE_UNORDERED_FLAG = 2;

/** `Object#toString` result references. */
var boolTag = '[object Boolean]',
    dateTag = '[object Date]',
    errorTag = '[object Error]',
    mapTag = '[object Map]',
    numberTag = '[object Number]',
    regexpTag = '[object RegExp]',
    setTag = '[object Set]',
    stringTag = '[object String]',
    symbolTag = '[object Symbol]';

var arrayBufferTag = '[object ArrayBuffer]',
    dataViewTag = '[object DataView]';

/** Used to convert symbols to primitives and strings. */
var symbolProto = Symbol ? Symbol.prototype : undefined,
    symbolValueOf = symbolProto ? symbolProto.valueOf : undefined;

/**
 * A specialized version of `baseIsEqualDeep` for comparing objects of
 * the same `toStringTag`.
 *
 * **Note:** This function only supports comparing values with tags of
 * `Boolean`, `Date`, `Error`, `Number`, `RegExp`, or `String`.
 *
 * @private
 * @param {Object} object The object to compare.
 * @param {Object} other The other object to compare.
 * @param {string} tag The `toStringTag` of the objects to compare.
 * @param {number} bitmask The bitmask flags. See `baseIsEqual` for more details.
 * @param {Function} customizer The function to customize comparisons.
 * @param {Function} equalFunc The function to determine equivalents of values.
 * @param {Object} stack Tracks traversed `object` and `other` objects.
 * @returns {boolean} Returns `true` if the objects are equivalent, else `false`.
 */
function equalByTag(object, other, tag, bitmask, customizer, equalFunc, stack) {
  switch (tag) {
    case dataViewTag:
      if ((object.byteLength != other.byteLength) ||
          (object.byteOffset != other.byteOffset)) {
        return false;
      }
      object = object.buffer;
      other = other.buffer;

    case arrayBufferTag:
      if ((object.byteLength != other.byteLength) ||
          !equalFunc(new Uint8Array(object), new Uint8Array(other))) {
        return false;
      }
      return true;

    case boolTag:
    case dateTag:
    case numberTag:
      // Coerce booleans to `1` or `0` and dates to milliseconds.
      // Invalid dates are coerced to `NaN`.
      return eq(+object, +other);

    case errorTag:
      return object.name == other.name && object.message == other.message;

    case regexpTag:
    case stringTag:
      // Coerce regexes to strings and treat strings, primitives and objects,
      // as equal. See http://www.ecma-international.org/ecma-262/7.0/#sec-regexp.prototype.tostring
      // for more details.
      return object == (other + '');

    case mapTag:
      var convert = mapToArray;

    case setTag:
      var isPartial = bitmask & COMPARE_PARTIAL_FLAG;
      convert || (convert = setToArray);

      if (object.size != other.size && !isPartial) {
        return false;
      }
      // Assume cyclic values are equal.
      var stacked = stack.get(object);
      if (stacked) {
        return stacked == other;
      }
      bitmask |= COMPARE_UNORDERED_FLAG;

      // Recursively compare objects (susceptible to call stack limits).
      stack.set(object, other);
      var result = equalArrays(convert(object), convert(other), bitmask, customizer, equalFunc, stack);
      stack['delete'](object);
      return result;

    case symbolTag:
      if (symbolValueOf) {
        return symbolValueOf.call(object) == symbolValueOf.call(other);
      }
  }
  return false;
}

module.exports = equalByTag;

},{"./_Symbol":12,"./_Uint8Array":13,"./_equalArrays":87,"./_mapToArray":129,"./_setToArray":142,"./eq":160}],89:[function(require,module,exports){
var getAllKeys = require('./_getAllKeys');

/** Used to compose bitmasks for value comparisons. */
var COMPARE_PARTIAL_FLAG = 1;

/** Used for built-in method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/**
 * A specialized version of `baseIsEqualDeep` for objects with support for
 * partial deep comparisons.
 *
 * @private
 * @param {Object} object The object to compare.
 * @param {Object} other The other object to compare.
 * @param {number} bitmask The bitmask flags. See `baseIsEqual` for more details.
 * @param {Function} customizer The function to customize comparisons.
 * @param {Function} equalFunc The function to determine equivalents of values.
 * @param {Object} stack Tracks traversed `object` and `other` objects.
 * @returns {boolean} Returns `true` if the objects are equivalent, else `false`.
 */
function equalObjects(object, other, bitmask, customizer, equalFunc, stack) {
  var isPartial = bitmask & COMPARE_PARTIAL_FLAG,
      objProps = getAllKeys(object),
      objLength = objProps.length,
      othProps = getAllKeys(other),
      othLength = othProps.length;

  if (objLength != othLength && !isPartial) {
    return false;
  }
  var index = objLength;
  while (index--) {
    var key = objProps[index];
    if (!(isPartial ? key in other : hasOwnProperty.call(other, key))) {
      return false;
    }
  }
  // Assume cyclic values are equal.
  var stacked = stack.get(object);
  if (stacked && stack.get(other)) {
    return stacked == other;
  }
  var result = true;
  stack.set(object, other);
  stack.set(other, object);

  var skipCtor = isPartial;
  while (++index < objLength) {
    key = objProps[index];
    var objValue = object[key],
        othValue = other[key];

    if (customizer) {
      var compared = isPartial
        ? customizer(othValue, objValue, key, other, object, stack)
        : customizer(objValue, othValue, key, object, other, stack);
    }
    // Recursively compare objects (susceptible to call stack limits).
    if (!(compared === undefined
          ? (objValue === othValue || equalFunc(objValue, othValue, bitmask, customizer, stack))
          : compared
        )) {
      result = false;
      break;
    }
    skipCtor || (skipCtor = key == 'constructor');
  }
  if (result && !skipCtor) {
    var objCtor = object.constructor,
        othCtor = other.constructor;

    // Non `Object` object instances with different constructors are not equal.
    if (objCtor != othCtor &&
        ('constructor' in object && 'constructor' in other) &&
        !(typeof objCtor == 'function' && objCtor instanceof objCtor &&
          typeof othCtor == 'function' && othCtor instanceof othCtor)) {
      result = false;
    }
  }
  stack['delete'](object);
  stack['delete'](other);
  return result;
}

module.exports = equalObjects;

},{"./_getAllKeys":91}],90:[function(require,module,exports){
(function (global){
/** Detect free variable `global` from Node.js. */
var freeGlobal = typeof global == 'object' && global && global.Object === Object && global;

module.exports = freeGlobal;

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{}],91:[function(require,module,exports){
var baseGetAllKeys = require('./_baseGetAllKeys'),
    getSymbols = require('./_getSymbols'),
    keys = require('./keys');

/**
 * Creates an array of own enumerable property names and symbols of `object`.
 *
 * @private
 * @param {Object} object The object to query.
 * @returns {Array} Returns the array of property names and symbols.
 */
function getAllKeys(object) {
  return baseGetAllKeys(object, keys, getSymbols);
}

module.exports = getAllKeys;

},{"./_baseGetAllKeys":41,"./_getSymbols":98,"./keys":179}],92:[function(require,module,exports){
var baseGetAllKeys = require('./_baseGetAllKeys'),
    getSymbolsIn = require('./_getSymbolsIn'),
    keysIn = require('./keysIn');

/**
 * Creates an array of own and inherited enumerable property names and
 * symbols of `object`.
 *
 * @private
 * @param {Object} object The object to query.
 * @returns {Array} Returns the array of property names and symbols.
 */
function getAllKeysIn(object) {
  return baseGetAllKeys(object, keysIn, getSymbolsIn);
}

module.exports = getAllKeysIn;

},{"./_baseGetAllKeys":41,"./_getSymbolsIn":99,"./keysIn":180}],93:[function(require,module,exports){
var isKeyable = require('./_isKeyable');

/**
 * Gets the data for `map`.
 *
 * @private
 * @param {Object} map The map to query.
 * @param {string} key The reference key.
 * @returns {*} Returns the map data.
 */
function getMapData(map, key) {
  var data = map.__data__;
  return isKeyable(key)
    ? data[typeof key == 'string' ? 'string' : 'hash']
    : data.map;
}

module.exports = getMapData;

},{"./_isKeyable":115}],94:[function(require,module,exports){
var isStrictComparable = require('./_isStrictComparable'),
    keys = require('./keys');

/**
 * Gets the property names, values, and compare flags of `object`.
 *
 * @private
 * @param {Object} object The object to query.
 * @returns {Array} Returns the match data of `object`.
 */
function getMatchData(object) {
  var result = keys(object),
      length = result.length;

  while (length--) {
    var key = result[length],
        value = object[key];

    result[length] = [key, value, isStrictComparable(value)];
  }
  return result;
}

module.exports = getMatchData;

},{"./_isStrictComparable":118,"./keys":179}],95:[function(require,module,exports){
var baseIsNative = require('./_baseIsNative'),
    getValue = require('./_getValue');

/**
 * Gets the native function at `key` of `object`.
 *
 * @private
 * @param {Object} object The object to query.
 * @param {string} key The key of the method to get.
 * @returns {*} Returns the function if it's native, else `undefined`.
 */
function getNative(object, key) {
  var value = getValue(object, key);
  return baseIsNative(value) ? value : undefined;
}

module.exports = getNative;

},{"./_baseIsNative":50,"./_getValue":101}],96:[function(require,module,exports){
var overArg = require('./_overArg');

/** Built-in value references. */
var getPrototype = overArg(Object.getPrototypeOf, Object);

module.exports = getPrototype;

},{"./_overArg":137}],97:[function(require,module,exports){
var Symbol = require('./_Symbol');

/** Used for built-in method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/**
 * Used to resolve the
 * [`toStringTag`](http://ecma-international.org/ecma-262/7.0/#sec-object.prototype.tostring)
 * of values.
 */
var nativeObjectToString = objectProto.toString;

/** Built-in value references. */
var symToStringTag = Symbol ? Symbol.toStringTag : undefined;

/**
 * A specialized version of `baseGetTag` which ignores `Symbol.toStringTag` values.
 *
 * @private
 * @param {*} value The value to query.
 * @returns {string} Returns the raw `toStringTag`.
 */
function getRawTag(value) {
  var isOwn = hasOwnProperty.call(value, symToStringTag),
      tag = value[symToStringTag];

  try {
    value[symToStringTag] = undefined;
    var unmasked = true;
  } catch (e) {}

  var result = nativeObjectToString.call(value);
  if (unmasked) {
    if (isOwn) {
      value[symToStringTag] = tag;
    } else {
      delete value[symToStringTag];
    }
  }
  return result;
}

module.exports = getRawTag;

},{"./_Symbol":12}],98:[function(require,module,exports){
var arrayFilter = require('./_arrayFilter'),
    stubArray = require('./stubArray');

/** Used for built-in method references. */
var objectProto = Object.prototype;

/** Built-in value references. */
var propertyIsEnumerable = objectProto.propertyIsEnumerable;

/* Built-in method references for those with the same name as other `lodash` methods. */
var nativeGetSymbols = Object.getOwnPropertySymbols;

/**
 * Creates an array of the own enumerable symbols of `object`.
 *
 * @private
 * @param {Object} object The object to query.
 * @returns {Array} Returns the array of symbols.
 */
var getSymbols = !nativeGetSymbols ? stubArray : function(object) {
  if (object == null) {
    return [];
  }
  object = Object(object);
  return arrayFilter(nativeGetSymbols(object), function(symbol) {
    return propertyIsEnumerable.call(object, symbol);
  });
};

module.exports = getSymbols;

},{"./_arrayFilter":19,"./stubArray":186}],99:[function(require,module,exports){
var arrayPush = require('./_arrayPush'),
    getPrototype = require('./_getPrototype'),
    getSymbols = require('./_getSymbols'),
    stubArray = require('./stubArray');

/* Built-in method references for those with the same name as other `lodash` methods. */
var nativeGetSymbols = Object.getOwnPropertySymbols;

/**
 * Creates an array of the own and inherited enumerable symbols of `object`.
 *
 * @private
 * @param {Object} object The object to query.
 * @returns {Array} Returns the array of symbols.
 */
var getSymbolsIn = !nativeGetSymbols ? stubArray : function(object) {
  var result = [];
  while (object) {
    arrayPush(result, getSymbols(object));
    object = getPrototype(object);
  }
  return result;
};

module.exports = getSymbolsIn;

},{"./_arrayPush":24,"./_getPrototype":96,"./_getSymbols":98,"./stubArray":186}],100:[function(require,module,exports){
var DataView = require('./_DataView'),
    Map = require('./_Map'),
    Promise = require('./_Promise'),
    Set = require('./_Set'),
    WeakMap = require('./_WeakMap'),
    baseGetTag = require('./_baseGetTag'),
    toSource = require('./_toSource');

/** `Object#toString` result references. */
var mapTag = '[object Map]',
    objectTag = '[object Object]',
    promiseTag = '[object Promise]',
    setTag = '[object Set]',
    weakMapTag = '[object WeakMap]';

var dataViewTag = '[object DataView]';

/** Used to detect maps, sets, and weakmaps. */
var dataViewCtorString = toSource(DataView),
    mapCtorString = toSource(Map),
    promiseCtorString = toSource(Promise),
    setCtorString = toSource(Set),
    weakMapCtorString = toSource(WeakMap);

/**
 * Gets the `toStringTag` of `value`.
 *
 * @private
 * @param {*} value The value to query.
 * @returns {string} Returns the `toStringTag`.
 */
var getTag = baseGetTag;

// Fallback for data views, maps, sets, and weak maps in IE 11 and promises in Node.js < 6.
if ((DataView && getTag(new DataView(new ArrayBuffer(1))) != dataViewTag) ||
    (Map && getTag(new Map) != mapTag) ||
    (Promise && getTag(Promise.resolve()) != promiseTag) ||
    (Set && getTag(new Set) != setTag) ||
    (WeakMap && getTag(new WeakMap) != weakMapTag)) {
  getTag = function(value) {
    var result = baseGetTag(value),
        Ctor = result == objectTag ? value.constructor : undefined,
        ctorString = Ctor ? toSource(Ctor) : '';

    if (ctorString) {
      switch (ctorString) {
        case dataViewCtorString: return dataViewTag;
        case mapCtorString: return mapTag;
        case promiseCtorString: return promiseTag;
        case setCtorString: return setTag;
        case weakMapCtorString: return weakMapTag;
      }
    }
    return result;
  };
}

module.exports = getTag;

},{"./_DataView":3,"./_Map":6,"./_Promise":8,"./_Set":9,"./_WeakMap":14,"./_baseGetTag":42,"./_toSource":153}],101:[function(require,module,exports){
/**
 * Gets the value at `key` of `object`.
 *
 * @private
 * @param {Object} [object] The object to query.
 * @param {string} key The key of the property to get.
 * @returns {*} Returns the property value.
 */
function getValue(object, key) {
  return object == null ? undefined : object[key];
}

module.exports = getValue;

},{}],102:[function(require,module,exports){
var castPath = require('./_castPath'),
    isArguments = require('./isArguments'),
    isArray = require('./isArray'),
    isIndex = require('./_isIndex'),
    isLength = require('./isLength'),
    toKey = require('./_toKey');

/**
 * Checks if `path` exists on `object`.
 *
 * @private
 * @param {Object} object The object to query.
 * @param {Array|string} path The path to check.
 * @param {Function} hasFunc The function to check properties.
 * @returns {boolean} Returns `true` if `path` exists, else `false`.
 */
function hasPath(object, path, hasFunc) {
  path = castPath(path, object);

  var index = -1,
      length = path.length,
      result = false;

  while (++index < length) {
    var key = toKey(path[index]);
    if (!(result = object != null && hasFunc(object, key))) {
      break;
    }
    object = object[key];
  }
  if (result || ++index != length) {
    return result;
  }
  length = object == null ? 0 : object.length;
  return !!length && isLength(length) && isIndex(key, length) &&
    (isArray(object) || isArguments(object));
}

module.exports = hasPath;

},{"./_castPath":67,"./_isIndex":112,"./_toKey":152,"./isArguments":167,"./isArray":168,"./isLength":174}],103:[function(require,module,exports){
var nativeCreate = require('./_nativeCreate');

/**
 * Removes all key-value entries from the hash.
 *
 * @private
 * @name clear
 * @memberOf Hash
 */
function hashClear() {
  this.__data__ = nativeCreate ? nativeCreate(null) : {};
  this.size = 0;
}

module.exports = hashClear;

},{"./_nativeCreate":132}],104:[function(require,module,exports){
/**
 * Removes `key` and its value from the hash.
 *
 * @private
 * @name delete
 * @memberOf Hash
 * @param {Object} hash The hash to modify.
 * @param {string} key The key of the value to remove.
 * @returns {boolean} Returns `true` if the entry was removed, else `false`.
 */
function hashDelete(key) {
  var result = this.has(key) && delete this.__data__[key];
  this.size -= result ? 1 : 0;
  return result;
}

module.exports = hashDelete;

},{}],105:[function(require,module,exports){
var nativeCreate = require('./_nativeCreate');

/** Used to stand-in for `undefined` hash values. */
var HASH_UNDEFINED = '__lodash_hash_undefined__';

/** Used for built-in method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/**
 * Gets the hash value for `key`.
 *
 * @private
 * @name get
 * @memberOf Hash
 * @param {string} key The key of the value to get.
 * @returns {*} Returns the entry value.
 */
function hashGet(key) {
  var data = this.__data__;
  if (nativeCreate) {
    var result = data[key];
    return result === HASH_UNDEFINED ? undefined : result;
  }
  return hasOwnProperty.call(data, key) ? data[key] : undefined;
}

module.exports = hashGet;

},{"./_nativeCreate":132}],106:[function(require,module,exports){
var nativeCreate = require('./_nativeCreate');

/** Used for built-in method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/**
 * Checks if a hash value for `key` exists.
 *
 * @private
 * @name has
 * @memberOf Hash
 * @param {string} key The key of the entry to check.
 * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
 */
function hashHas(key) {
  var data = this.__data__;
  return nativeCreate ? (data[key] !== undefined) : hasOwnProperty.call(data, key);
}

module.exports = hashHas;

},{"./_nativeCreate":132}],107:[function(require,module,exports){
var nativeCreate = require('./_nativeCreate');

/** Used to stand-in for `undefined` hash values. */
var HASH_UNDEFINED = '__lodash_hash_undefined__';

/**
 * Sets the hash `key` to `value`.
 *
 * @private
 * @name set
 * @memberOf Hash
 * @param {string} key The key of the value to set.
 * @param {*} value The value to set.
 * @returns {Object} Returns the hash instance.
 */
function hashSet(key, value) {
  var data = this.__data__;
  this.size += this.has(key) ? 0 : 1;
  data[key] = (nativeCreate && value === undefined) ? HASH_UNDEFINED : value;
  return this;
}

module.exports = hashSet;

},{"./_nativeCreate":132}],108:[function(require,module,exports){
/** Used for built-in method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/**
 * Initializes an array clone.
 *
 * @private
 * @param {Array} array The array to clone.
 * @returns {Array} Returns the initialized clone.
 */
function initCloneArray(array) {
  var length = array.length,
      result = array.constructor(length);

  // Add properties assigned by `RegExp#exec`.
  if (length && typeof array[0] == 'string' && hasOwnProperty.call(array, 'index')) {
    result.index = array.index;
    result.input = array.input;
  }
  return result;
}

module.exports = initCloneArray;

},{}],109:[function(require,module,exports){
var cloneArrayBuffer = require('./_cloneArrayBuffer'),
    cloneDataView = require('./_cloneDataView'),
    cloneMap = require('./_cloneMap'),
    cloneRegExp = require('./_cloneRegExp'),
    cloneSet = require('./_cloneSet'),
    cloneSymbol = require('./_cloneSymbol'),
    cloneTypedArray = require('./_cloneTypedArray');

/** `Object#toString` result references. */
var boolTag = '[object Boolean]',
    dateTag = '[object Date]',
    mapTag = '[object Map]',
    numberTag = '[object Number]',
    regexpTag = '[object RegExp]',
    setTag = '[object Set]',
    stringTag = '[object String]',
    symbolTag = '[object Symbol]';

var arrayBufferTag = '[object ArrayBuffer]',
    dataViewTag = '[object DataView]',
    float32Tag = '[object Float32Array]',
    float64Tag = '[object Float64Array]',
    int8Tag = '[object Int8Array]',
    int16Tag = '[object Int16Array]',
    int32Tag = '[object Int32Array]',
    uint8Tag = '[object Uint8Array]',
    uint8ClampedTag = '[object Uint8ClampedArray]',
    uint16Tag = '[object Uint16Array]',
    uint32Tag = '[object Uint32Array]';

/**
 * Initializes an object clone based on its `toStringTag`.
 *
 * **Note:** This function only supports cloning values with tags of
 * `Boolean`, `Date`, `Error`, `Number`, `RegExp`, or `String`.
 *
 * @private
 * @param {Object} object The object to clone.
 * @param {string} tag The `toStringTag` of the object to clone.
 * @param {Function} cloneFunc The function to clone values.
 * @param {boolean} [isDeep] Specify a deep clone.
 * @returns {Object} Returns the initialized clone.
 */
function initCloneByTag(object, tag, cloneFunc, isDeep) {
  var Ctor = object.constructor;
  switch (tag) {
    case arrayBufferTag:
      return cloneArrayBuffer(object);

    case boolTag:
    case dateTag:
      return new Ctor(+object);

    case dataViewTag:
      return cloneDataView(object, isDeep);

    case float32Tag: case float64Tag:
    case int8Tag: case int16Tag: case int32Tag:
    case uint8Tag: case uint8ClampedTag: case uint16Tag: case uint32Tag:
      return cloneTypedArray(object, isDeep);

    case mapTag:
      return cloneMap(object, isDeep, cloneFunc);

    case numberTag:
    case stringTag:
      return new Ctor(object);

    case regexpTag:
      return cloneRegExp(object);

    case setTag:
      return cloneSet(object, isDeep, cloneFunc);

    case symbolTag:
      return cloneSymbol(object);
  }
}

module.exports = initCloneByTag;

},{"./_cloneArrayBuffer":68,"./_cloneDataView":70,"./_cloneMap":71,"./_cloneRegExp":72,"./_cloneSet":73,"./_cloneSymbol":74,"./_cloneTypedArray":75}],110:[function(require,module,exports){
var baseCreate = require('./_baseCreate'),
    getPrototype = require('./_getPrototype'),
    isPrototype = require('./_isPrototype');

/**
 * Initializes an object clone.
 *
 * @private
 * @param {Object} object The object to clone.
 * @returns {Object} Returns the initialized clone.
 */
function initCloneObject(object) {
  return (typeof object.constructor == 'function' && !isPrototype(object))
    ? baseCreate(getPrototype(object))
    : {};
}

module.exports = initCloneObject;

},{"./_baseCreate":33,"./_getPrototype":96,"./_isPrototype":117}],111:[function(require,module,exports){
var Symbol = require('./_Symbol'),
    isArguments = require('./isArguments'),
    isArray = require('./isArray');

/** Built-in value references. */
var spreadableSymbol = Symbol ? Symbol.isConcatSpreadable : undefined;

/**
 * Checks if `value` is a flattenable `arguments` object or array.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is flattenable, else `false`.
 */
function isFlattenable(value) {
  return isArray(value) || isArguments(value) ||
    !!(spreadableSymbol && value && value[spreadableSymbol]);
}

module.exports = isFlattenable;

},{"./_Symbol":12,"./isArguments":167,"./isArray":168}],112:[function(require,module,exports){
/** Used as references for various `Number` constants. */
var MAX_SAFE_INTEGER = 9007199254740991;

/** Used to detect unsigned integer values. */
var reIsUint = /^(?:0|[1-9]\d*)$/;

/**
 * Checks if `value` is a valid array-like index.
 *
 * @private
 * @param {*} value The value to check.
 * @param {number} [length=MAX_SAFE_INTEGER] The upper bounds of a valid index.
 * @returns {boolean} Returns `true` if `value` is a valid index, else `false`.
 */
function isIndex(value, length) {
  length = length == null ? MAX_SAFE_INTEGER : length;
  return !!length &&
    (typeof value == 'number' || reIsUint.test(value)) &&
    (value > -1 && value % 1 == 0 && value < length);
}

module.exports = isIndex;

},{}],113:[function(require,module,exports){
var eq = require('./eq'),
    isArrayLike = require('./isArrayLike'),
    isIndex = require('./_isIndex'),
    isObject = require('./isObject');

/**
 * Checks if the given arguments are from an iteratee call.
 *
 * @private
 * @param {*} value The potential iteratee value argument.
 * @param {*} index The potential iteratee index or key argument.
 * @param {*} object The potential iteratee object argument.
 * @returns {boolean} Returns `true` if the arguments are from an iteratee call,
 *  else `false`.
 */
function isIterateeCall(value, index, object) {
  if (!isObject(object)) {
    return false;
  }
  var type = typeof index;
  if (type == 'number'
        ? (isArrayLike(object) && isIndex(index, object.length))
        : (type == 'string' && index in object)
      ) {
    return eq(object[index], value);
  }
  return false;
}

module.exports = isIterateeCall;

},{"./_isIndex":112,"./eq":160,"./isArrayLike":169,"./isObject":175}],114:[function(require,module,exports){
var isArray = require('./isArray'),
    isSymbol = require('./isSymbol');

/** Used to match property names within property paths. */
var reIsDeepProp = /\.|\[(?:[^[\]]*|(["'])(?:(?!\1)[^\\]|\\.)*?\1)\]/,
    reIsPlainProp = /^\w*$/;

/**
 * Checks if `value` is a property name and not a property path.
 *
 * @private
 * @param {*} value The value to check.
 * @param {Object} [object] The object to query keys on.
 * @returns {boolean} Returns `true` if `value` is a property name, else `false`.
 */
function isKey(value, object) {
  if (isArray(value)) {
    return false;
  }
  var type = typeof value;
  if (type == 'number' || type == 'symbol' || type == 'boolean' ||
      value == null || isSymbol(value)) {
    return true;
  }
  return reIsPlainProp.test(value) || !reIsDeepProp.test(value) ||
    (object != null && value in Object(object));
}

module.exports = isKey;

},{"./isArray":168,"./isSymbol":177}],115:[function(require,module,exports){
/**
 * Checks if `value` is suitable for use as unique object key.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is suitable, else `false`.
 */
function isKeyable(value) {
  var type = typeof value;
  return (type == 'string' || type == 'number' || type == 'symbol' || type == 'boolean')
    ? (value !== '__proto__')
    : (value === null);
}

module.exports = isKeyable;

},{}],116:[function(require,module,exports){
var coreJsData = require('./_coreJsData');

/** Used to detect methods masquerading as native. */
var maskSrcKey = (function() {
  var uid = /[^.]+$/.exec(coreJsData && coreJsData.keys && coreJsData.keys.IE_PROTO || '');
  return uid ? ('Symbol(src)_1.' + uid) : '';
}());

/**
 * Checks if `func` has its source masked.
 *
 * @private
 * @param {Function} func The function to check.
 * @returns {boolean} Returns `true` if `func` is masked, else `false`.
 */
function isMasked(func) {
  return !!maskSrcKey && (maskSrcKey in func);
}

module.exports = isMasked;

},{"./_coreJsData":80}],117:[function(require,module,exports){
/** Used for built-in method references. */
var objectProto = Object.prototype;

/**
 * Checks if `value` is likely a prototype object.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a prototype, else `false`.
 */
function isPrototype(value) {
  var Ctor = value && value.constructor,
      proto = (typeof Ctor == 'function' && Ctor.prototype) || objectProto;

  return value === proto;
}

module.exports = isPrototype;

},{}],118:[function(require,module,exports){
var isObject = require('./isObject');

/**
 * Checks if `value` is suitable for strict equality comparisons, i.e. `===`.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` if suitable for strict
 *  equality comparisons, else `false`.
 */
function isStrictComparable(value) {
  return value === value && !isObject(value);
}

module.exports = isStrictComparable;

},{"./isObject":175}],119:[function(require,module,exports){
/**
 * Removes all key-value entries from the list cache.
 *
 * @private
 * @name clear
 * @memberOf ListCache
 */
function listCacheClear() {
  this.__data__ = [];
  this.size = 0;
}

module.exports = listCacheClear;

},{}],120:[function(require,module,exports){
var assocIndexOf = require('./_assocIndexOf');

/** Used for built-in method references. */
var arrayProto = Array.prototype;

/** Built-in value references. */
var splice = arrayProto.splice;

/**
 * Removes `key` and its value from the list cache.
 *
 * @private
 * @name delete
 * @memberOf ListCache
 * @param {string} key The key of the value to remove.
 * @returns {boolean} Returns `true` if the entry was removed, else `false`.
 */
function listCacheDelete(key) {
  var data = this.__data__,
      index = assocIndexOf(data, key);

  if (index < 0) {
    return false;
  }
  var lastIndex = data.length - 1;
  if (index == lastIndex) {
    data.pop();
  } else {
    splice.call(data, index, 1);
  }
  --this.size;
  return true;
}

module.exports = listCacheDelete;

},{"./_assocIndexOf":28}],121:[function(require,module,exports){
var assocIndexOf = require('./_assocIndexOf');

/**
 * Gets the list cache value for `key`.
 *
 * @private
 * @name get
 * @memberOf ListCache
 * @param {string} key The key of the value to get.
 * @returns {*} Returns the entry value.
 */
function listCacheGet(key) {
  var data = this.__data__,
      index = assocIndexOf(data, key);

  return index < 0 ? undefined : data[index][1];
}

module.exports = listCacheGet;

},{"./_assocIndexOf":28}],122:[function(require,module,exports){
var assocIndexOf = require('./_assocIndexOf');

/**
 * Checks if a list cache value for `key` exists.
 *
 * @private
 * @name has
 * @memberOf ListCache
 * @param {string} key The key of the entry to check.
 * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
 */
function listCacheHas(key) {
  return assocIndexOf(this.__data__, key) > -1;
}

module.exports = listCacheHas;

},{"./_assocIndexOf":28}],123:[function(require,module,exports){
var assocIndexOf = require('./_assocIndexOf');

/**
 * Sets the list cache `key` to `value`.
 *
 * @private
 * @name set
 * @memberOf ListCache
 * @param {string} key The key of the value to set.
 * @param {*} value The value to set.
 * @returns {Object} Returns the list cache instance.
 */
function listCacheSet(key, value) {
  var data = this.__data__,
      index = assocIndexOf(data, key);

  if (index < 0) {
    ++this.size;
    data.push([key, value]);
  } else {
    data[index][1] = value;
  }
  return this;
}

module.exports = listCacheSet;

},{"./_assocIndexOf":28}],124:[function(require,module,exports){
var Hash = require('./_Hash'),
    ListCache = require('./_ListCache'),
    Map = require('./_Map');

/**
 * Removes all key-value entries from the map.
 *
 * @private
 * @name clear
 * @memberOf MapCache
 */
function mapCacheClear() {
  this.size = 0;
  this.__data__ = {
    'hash': new Hash,
    'map': new (Map || ListCache),
    'string': new Hash
  };
}

module.exports = mapCacheClear;

},{"./_Hash":4,"./_ListCache":5,"./_Map":6}],125:[function(require,module,exports){
var getMapData = require('./_getMapData');

/**
 * Removes `key` and its value from the map.
 *
 * @private
 * @name delete
 * @memberOf MapCache
 * @param {string} key The key of the value to remove.
 * @returns {boolean} Returns `true` if the entry was removed, else `false`.
 */
function mapCacheDelete(key) {
  var result = getMapData(this, key)['delete'](key);
  this.size -= result ? 1 : 0;
  return result;
}

module.exports = mapCacheDelete;

},{"./_getMapData":93}],126:[function(require,module,exports){
var getMapData = require('./_getMapData');

/**
 * Gets the map value for `key`.
 *
 * @private
 * @name get
 * @memberOf MapCache
 * @param {string} key The key of the value to get.
 * @returns {*} Returns the entry value.
 */
function mapCacheGet(key) {
  return getMapData(this, key).get(key);
}

module.exports = mapCacheGet;

},{"./_getMapData":93}],127:[function(require,module,exports){
var getMapData = require('./_getMapData');

/**
 * Checks if a map value for `key` exists.
 *
 * @private
 * @name has
 * @memberOf MapCache
 * @param {string} key The key of the entry to check.
 * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
 */
function mapCacheHas(key) {
  return getMapData(this, key).has(key);
}

module.exports = mapCacheHas;

},{"./_getMapData":93}],128:[function(require,module,exports){
var getMapData = require('./_getMapData');

/**
 * Sets the map `key` to `value`.
 *
 * @private
 * @name set
 * @memberOf MapCache
 * @param {string} key The key of the value to set.
 * @param {*} value The value to set.
 * @returns {Object} Returns the map cache instance.
 */
function mapCacheSet(key, value) {
  var data = getMapData(this, key),
      size = data.size;

  data.set(key, value);
  this.size += data.size == size ? 0 : 1;
  return this;
}

module.exports = mapCacheSet;

},{"./_getMapData":93}],129:[function(require,module,exports){
/**
 * Converts `map` to its key-value pairs.
 *
 * @private
 * @param {Object} map The map to convert.
 * @returns {Array} Returns the key-value pairs.
 */
function mapToArray(map) {
  var index = -1,
      result = Array(map.size);

  map.forEach(function(value, key) {
    result[++index] = [key, value];
  });
  return result;
}

module.exports = mapToArray;

},{}],130:[function(require,module,exports){
/**
 * A specialized version of `matchesProperty` for source values suitable
 * for strict equality comparisons, i.e. `===`.
 *
 * @private
 * @param {string} key The key of the property to get.
 * @param {*} srcValue The value to match.
 * @returns {Function} Returns the new spec function.
 */
function matchesStrictComparable(key, srcValue) {
  return function(object) {
    if (object == null) {
      return false;
    }
    return object[key] === srcValue &&
      (srcValue !== undefined || (key in Object(object)));
  };
}

module.exports = matchesStrictComparable;

},{}],131:[function(require,module,exports){
var memoize = require('./memoize');

/** Used as the maximum memoize cache size. */
var MAX_MEMOIZE_SIZE = 500;

/**
 * A specialized version of `_.memoize` which clears the memoized function's
 * cache when it exceeds `MAX_MEMOIZE_SIZE`.
 *
 * @private
 * @param {Function} func The function to have its output memoized.
 * @returns {Function} Returns the new memoized function.
 */
function memoizeCapped(func) {
  var result = memoize(func, function(key) {
    if (cache.size === MAX_MEMOIZE_SIZE) {
      cache.clear();
    }
    return key;
  });

  var cache = result.cache;
  return result;
}

module.exports = memoizeCapped;

},{"./memoize":181}],132:[function(require,module,exports){
var getNative = require('./_getNative');

/* Built-in method references that are verified to be native. */
var nativeCreate = getNative(Object, 'create');

module.exports = nativeCreate;

},{"./_getNative":95}],133:[function(require,module,exports){
var overArg = require('./_overArg');

/* Built-in method references for those with the same name as other `lodash` methods. */
var nativeKeys = overArg(Object.keys, Object);

module.exports = nativeKeys;

},{"./_overArg":137}],134:[function(require,module,exports){
/**
 * This function is like
 * [`Object.keys`](http://ecma-international.org/ecma-262/7.0/#sec-object.keys)
 * except that it includes inherited enumerable properties.
 *
 * @private
 * @param {Object} object The object to query.
 * @returns {Array} Returns the array of property names.
 */
function nativeKeysIn(object) {
  var result = [];
  if (object != null) {
    for (var key in Object(object)) {
      result.push(key);
    }
  }
  return result;
}

module.exports = nativeKeysIn;

},{}],135:[function(require,module,exports){
var freeGlobal = require('./_freeGlobal');

/** Detect free variable `exports`. */
var freeExports = typeof exports == 'object' && exports && !exports.nodeType && exports;

/** Detect free variable `module`. */
var freeModule = freeExports && typeof module == 'object' && module && !module.nodeType && module;

/** Detect the popular CommonJS extension `module.exports`. */
var moduleExports = freeModule && freeModule.exports === freeExports;

/** Detect free variable `process` from Node.js. */
var freeProcess = moduleExports && freeGlobal.process;

/** Used to access faster Node.js helpers. */
var nodeUtil = (function() {
  try {
    return freeProcess && freeProcess.binding && freeProcess.binding('util');
  } catch (e) {}
}());

module.exports = nodeUtil;

},{"./_freeGlobal":90}],136:[function(require,module,exports){
/** Used for built-in method references. */
var objectProto = Object.prototype;

/**
 * Used to resolve the
 * [`toStringTag`](http://ecma-international.org/ecma-262/7.0/#sec-object.prototype.tostring)
 * of values.
 */
var nativeObjectToString = objectProto.toString;

/**
 * Converts `value` to a string using `Object.prototype.toString`.
 *
 * @private
 * @param {*} value The value to convert.
 * @returns {string} Returns the converted string.
 */
function objectToString(value) {
  return nativeObjectToString.call(value);
}

module.exports = objectToString;

},{}],137:[function(require,module,exports){
/**
 * Creates a unary function that invokes `func` with its argument transformed.
 *
 * @private
 * @param {Function} func The function to wrap.
 * @param {Function} transform The argument transform.
 * @returns {Function} Returns the new function.
 */
function overArg(func, transform) {
  return function(arg) {
    return func(transform(arg));
  };
}

module.exports = overArg;

},{}],138:[function(require,module,exports){
var apply = require('./_apply');

/* Built-in method references for those with the same name as other `lodash` methods. */
var nativeMax = Math.max;

/**
 * A specialized version of `baseRest` which transforms the rest array.
 *
 * @private
 * @param {Function} func The function to apply a rest parameter to.
 * @param {number} [start=func.length-1] The start position of the rest parameter.
 * @param {Function} transform The rest array transform.
 * @returns {Function} Returns the new function.
 */
function overRest(func, start, transform) {
  start = nativeMax(start === undefined ? (func.length - 1) : start, 0);
  return function() {
    var args = arguments,
        index = -1,
        length = nativeMax(args.length - start, 0),
        array = Array(length);

    while (++index < length) {
      array[index] = args[start + index];
    }
    index = -1;
    var otherArgs = Array(start + 1);
    while (++index < start) {
      otherArgs[index] = args[index];
    }
    otherArgs[start] = transform(array);
    return apply(func, this, otherArgs);
  };
}

module.exports = overRest;

},{"./_apply":17}],139:[function(require,module,exports){
var freeGlobal = require('./_freeGlobal');

/** Detect free variable `self`. */
var freeSelf = typeof self == 'object' && self && self.Object === Object && self;

/** Used as a reference to the global object. */
var root = freeGlobal || freeSelf || Function('return this')();

module.exports = root;

},{"./_freeGlobal":90}],140:[function(require,module,exports){
/** Used to stand-in for `undefined` hash values. */
var HASH_UNDEFINED = '__lodash_hash_undefined__';

/**
 * Adds `value` to the array cache.
 *
 * @private
 * @name add
 * @memberOf SetCache
 * @alias push
 * @param {*} value The value to cache.
 * @returns {Object} Returns the cache instance.
 */
function setCacheAdd(value) {
  this.__data__.set(value, HASH_UNDEFINED);
  return this;
}

module.exports = setCacheAdd;

},{}],141:[function(require,module,exports){
/**
 * Checks if `value` is in the array cache.
 *
 * @private
 * @name has
 * @memberOf SetCache
 * @param {*} value The value to search for.
 * @returns {number} Returns `true` if `value` is found, else `false`.
 */
function setCacheHas(value) {
  return this.__data__.has(value);
}

module.exports = setCacheHas;

},{}],142:[function(require,module,exports){
/**
 * Converts `set` to an array of its values.
 *
 * @private
 * @param {Object} set The set to convert.
 * @returns {Array} Returns the values.
 */
function setToArray(set) {
  var index = -1,
      result = Array(set.size);

  set.forEach(function(value) {
    result[++index] = value;
  });
  return result;
}

module.exports = setToArray;

},{}],143:[function(require,module,exports){
var baseSetToString = require('./_baseSetToString'),
    shortOut = require('./_shortOut');

/**
 * Sets the `toString` method of `func` to return `string`.
 *
 * @private
 * @param {Function} func The function to modify.
 * @param {Function} string The `toString` result.
 * @returns {Function} Returns `func`.
 */
var setToString = shortOut(baseSetToString);

module.exports = setToString;

},{"./_baseSetToString":61,"./_shortOut":144}],144:[function(require,module,exports){
/** Used to detect hot functions by number of calls within a span of milliseconds. */
var HOT_COUNT = 800,
    HOT_SPAN = 16;

/* Built-in method references for those with the same name as other `lodash` methods. */
var nativeNow = Date.now;

/**
 * Creates a function that'll short out and invoke `identity` instead
 * of `func` when it's called `HOT_COUNT` or more times in `HOT_SPAN`
 * milliseconds.
 *
 * @private
 * @param {Function} func The function to restrict.
 * @returns {Function} Returns the new shortable function.
 */
function shortOut(func) {
  var count = 0,
      lastCalled = 0;

  return function() {
    var stamp = nativeNow(),
        remaining = HOT_SPAN - (stamp - lastCalled);

    lastCalled = stamp;
    if (remaining > 0) {
      if (++count >= HOT_COUNT) {
        return arguments[0];
      }
    } else {
      count = 0;
    }
    return func.apply(undefined, arguments);
  };
}

module.exports = shortOut;

},{}],145:[function(require,module,exports){
var ListCache = require('./_ListCache');

/**
 * Removes all key-value entries from the stack.
 *
 * @private
 * @name clear
 * @memberOf Stack
 */
function stackClear() {
  this.__data__ = new ListCache;
  this.size = 0;
}

module.exports = stackClear;

},{"./_ListCache":5}],146:[function(require,module,exports){
/**
 * Removes `key` and its value from the stack.
 *
 * @private
 * @name delete
 * @memberOf Stack
 * @param {string} key The key of the value to remove.
 * @returns {boolean} Returns `true` if the entry was removed, else `false`.
 */
function stackDelete(key) {
  var data = this.__data__,
      result = data['delete'](key);

  this.size = data.size;
  return result;
}

module.exports = stackDelete;

},{}],147:[function(require,module,exports){
/**
 * Gets the stack value for `key`.
 *
 * @private
 * @name get
 * @memberOf Stack
 * @param {string} key The key of the value to get.
 * @returns {*} Returns the entry value.
 */
function stackGet(key) {
  return this.__data__.get(key);
}

module.exports = stackGet;

},{}],148:[function(require,module,exports){
/**
 * Checks if a stack value for `key` exists.
 *
 * @private
 * @name has
 * @memberOf Stack
 * @param {string} key The key of the entry to check.
 * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
 */
function stackHas(key) {
  return this.__data__.has(key);
}

module.exports = stackHas;

},{}],149:[function(require,module,exports){
var ListCache = require('./_ListCache'),
    Map = require('./_Map'),
    MapCache = require('./_MapCache');

/** Used as the size to enable large array optimizations. */
var LARGE_ARRAY_SIZE = 200;

/**
 * Sets the stack `key` to `value`.
 *
 * @private
 * @name set
 * @memberOf Stack
 * @param {string} key The key of the value to set.
 * @param {*} value The value to set.
 * @returns {Object} Returns the stack cache instance.
 */
function stackSet(key, value) {
  var data = this.__data__;
  if (data instanceof ListCache) {
    var pairs = data.__data__;
    if (!Map || (pairs.length < LARGE_ARRAY_SIZE - 1)) {
      pairs.push([key, value]);
      this.size = ++data.size;
      return this;
    }
    data = this.__data__ = new MapCache(pairs);
  }
  data.set(key, value);
  this.size = data.size;
  return this;
}

module.exports = stackSet;

},{"./_ListCache":5,"./_Map":6,"./_MapCache":7}],150:[function(require,module,exports){
/**
 * A specialized version of `_.indexOf` which performs strict equality
 * comparisons of values, i.e. `===`.
 *
 * @private
 * @param {Array} array The array to inspect.
 * @param {*} value The value to search for.
 * @param {number} fromIndex The index to search from.
 * @returns {number} Returns the index of the matched value, else `-1`.
 */
function strictIndexOf(array, value, fromIndex) {
  var index = fromIndex - 1,
      length = array.length;

  while (++index < length) {
    if (array[index] === value) {
      return index;
    }
  }
  return -1;
}

module.exports = strictIndexOf;

},{}],151:[function(require,module,exports){
var memoizeCapped = require('./_memoizeCapped');

/** Used to match property names within property paths. */
var reLeadingDot = /^\./,
    rePropName = /[^.[\]]+|\[(?:(-?\d+(?:\.\d+)?)|(["'])((?:(?!\2)[^\\]|\\.)*?)\2)\]|(?=(?:\.|\[\])(?:\.|\[\]|$))/g;

/** Used to match backslashes in property paths. */
var reEscapeChar = /\\(\\)?/g;

/**
 * Converts `string` to a property path array.
 *
 * @private
 * @param {string} string The string to convert.
 * @returns {Array} Returns the property path array.
 */
var stringToPath = memoizeCapped(function(string) {
  var result = [];
  if (reLeadingDot.test(string)) {
    result.push('');
  }
  string.replace(rePropName, function(match, number, quote, string) {
    result.push(quote ? string.replace(reEscapeChar, '$1') : (number || match));
  });
  return result;
});

module.exports = stringToPath;

},{"./_memoizeCapped":131}],152:[function(require,module,exports){
var isSymbol = require('./isSymbol');

/** Used as references for various `Number` constants. */
var INFINITY = 1 / 0;

/**
 * Converts `value` to a string key if it's not a string or symbol.
 *
 * @private
 * @param {*} value The value to inspect.
 * @returns {string|symbol} Returns the key.
 */
function toKey(value) {
  if (typeof value == 'string' || isSymbol(value)) {
    return value;
  }
  var result = (value + '');
  return (result == '0' && (1 / value) == -INFINITY) ? '-0' : result;
}

module.exports = toKey;

},{"./isSymbol":177}],153:[function(require,module,exports){
/** Used for built-in method references. */
var funcProto = Function.prototype;

/** Used to resolve the decompiled source of functions. */
var funcToString = funcProto.toString;

/**
 * Converts `func` to its source code.
 *
 * @private
 * @param {Function} func The function to convert.
 * @returns {string} Returns the source code.
 */
function toSource(func) {
  if (func != null) {
    try {
      return funcToString.call(func);
    } catch (e) {}
    try {
      return (func + '');
    } catch (e) {}
  }
  return '';
}

module.exports = toSource;

},{}],154:[function(require,module,exports){
var assignValue = require('./_assignValue'),
    copyObject = require('./_copyObject'),
    createAssigner = require('./_createAssigner'),
    isArrayLike = require('./isArrayLike'),
    isPrototype = require('./_isPrototype'),
    keys = require('./keys');

/** Used for built-in method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/**
 * Assigns own enumerable string keyed properties of source objects to the
 * destination object. Source objects are applied from left to right.
 * Subsequent sources overwrite property assignments of previous sources.
 *
 * **Note:** This method mutates `object` and is loosely based on
 * [`Object.assign`](https://mdn.io/Object/assign).
 *
 * @static
 * @memberOf _
 * @since 0.10.0
 * @category Object
 * @param {Object} object The destination object.
 * @param {...Object} [sources] The source objects.
 * @returns {Object} Returns `object`.
 * @see _.assignIn
 * @example
 *
 * function Foo() {
 *   this.a = 1;
 * }
 *
 * function Bar() {
 *   this.c = 3;
 * }
 *
 * Foo.prototype.b = 2;
 * Bar.prototype.d = 4;
 *
 * _.assign({ 'a': 0 }, new Foo, new Bar);
 * // => { 'a': 1, 'c': 3 }
 */
var assign = createAssigner(function(object, source) {
  if (isPrototype(source) || isArrayLike(source)) {
    copyObject(source, keys(source), object);
    return;
  }
  for (var key in source) {
    if (hasOwnProperty.call(source, key)) {
      assignValue(object, key, source[key]);
    }
  }
});

module.exports = assign;

},{"./_assignValue":27,"./_copyObject":77,"./_createAssigner":81,"./_isPrototype":117,"./isArrayLike":169,"./keys":179}],155:[function(require,module,exports){
var copyObject = require('./_copyObject'),
    createAssigner = require('./_createAssigner'),
    keysIn = require('./keysIn');

/**
 * This method is like `_.assign` except that it iterates over own and
 * inherited source properties.
 *
 * **Note:** This method mutates `object`.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @alias extend
 * @category Object
 * @param {Object} object The destination object.
 * @param {...Object} [sources] The source objects.
 * @returns {Object} Returns `object`.
 * @see _.assign
 * @example
 *
 * function Foo() {
 *   this.a = 1;
 * }
 *
 * function Bar() {
 *   this.c = 3;
 * }
 *
 * Foo.prototype.b = 2;
 * Bar.prototype.d = 4;
 *
 * _.assignIn({ 'a': 0 }, new Foo, new Bar);
 * // => { 'a': 1, 'b': 2, 'c': 3, 'd': 4 }
 */
var assignIn = createAssigner(function(object, source) {
  copyObject(source, keysIn(source), object);
});

module.exports = assignIn;

},{"./_copyObject":77,"./_createAssigner":81,"./keysIn":180}],156:[function(require,module,exports){
var baseClone = require('./_baseClone');

/** Used to compose bitmasks for cloning. */
var CLONE_SYMBOLS_FLAG = 4;

/**
 * Creates a shallow clone of `value`.
 *
 * **Note:** This method is loosely based on the
 * [structured clone algorithm](https://mdn.io/Structured_clone_algorithm)
 * and supports cloning arrays, array buffers, booleans, date objects, maps,
 * numbers, `Object` objects, regexes, sets, strings, symbols, and typed
 * arrays. The own enumerable properties of `arguments` objects are cloned
 * as plain objects. An empty object is returned for uncloneable values such
 * as error objects, functions, DOM nodes, and WeakMaps.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to clone.
 * @returns {*} Returns the cloned value.
 * @see _.cloneDeep
 * @example
 *
 * var objects = [{ 'a': 1 }, { 'b': 2 }];
 *
 * var shallow = _.clone(objects);
 * console.log(shallow[0] === objects[0]);
 * // => true
 */
function clone(value) {
  return baseClone(value, CLONE_SYMBOLS_FLAG);
}

module.exports = clone;

},{"./_baseClone":32}],157:[function(require,module,exports){
/**
 * Creates a function that returns `value`.
 *
 * @static
 * @memberOf _
 * @since 2.4.0
 * @category Util
 * @param {*} value The value to return from the new function.
 * @returns {Function} Returns the new constant function.
 * @example
 *
 * var objects = _.times(2, _.constant({ 'a': 1 }));
 *
 * console.log(objects);
 * // => [{ 'a': 1 }, { 'a': 1 }]
 *
 * console.log(objects[0] === objects[1]);
 * // => true
 */
function constant(value) {
  return function() {
    return value;
  };
}

module.exports = constant;

},{}],158:[function(require,module,exports){
var isObject = require('./isObject'),
    now = require('./now'),
    toNumber = require('./toNumber');

/** Error message constants. */
var FUNC_ERROR_TEXT = 'Expected a function';

/* Built-in method references for those with the same name as other `lodash` methods. */
var nativeMax = Math.max,
    nativeMin = Math.min;

/**
 * Creates a debounced function that delays invoking `func` until after `wait`
 * milliseconds have elapsed since the last time the debounced function was
 * invoked. The debounced function comes with a `cancel` method to cancel
 * delayed `func` invocations and a `flush` method to immediately invoke them.
 * Provide `options` to indicate whether `func` should be invoked on the
 * leading and/or trailing edge of the `wait` timeout. The `func` is invoked
 * with the last arguments provided to the debounced function. Subsequent
 * calls to the debounced function return the result of the last `func`
 * invocation.
 *
 * **Note:** If `leading` and `trailing` options are `true`, `func` is
 * invoked on the trailing edge of the timeout only if the debounced function
 * is invoked more than once during the `wait` timeout.
 *
 * If `wait` is `0` and `leading` is `false`, `func` invocation is deferred
 * until to the next tick, similar to `setTimeout` with a timeout of `0`.
 *
 * See [David Corbacho's article](https://css-tricks.com/debouncing-throttling-explained-examples/)
 * for details over the differences between `_.debounce` and `_.throttle`.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Function
 * @param {Function} func The function to debounce.
 * @param {number} [wait=0] The number of milliseconds to delay.
 * @param {Object} [options={}] The options object.
 * @param {boolean} [options.leading=false]
 *  Specify invoking on the leading edge of the timeout.
 * @param {number} [options.maxWait]
 *  The maximum time `func` is allowed to be delayed before it's invoked.
 * @param {boolean} [options.trailing=true]
 *  Specify invoking on the trailing edge of the timeout.
 * @returns {Function} Returns the new debounced function.
 * @example
 *
 * // Avoid costly calculations while the window size is in flux.
 * jQuery(window).on('resize', _.debounce(calculateLayout, 150));
 *
 * // Invoke `sendMail` when clicked, debouncing subsequent calls.
 * jQuery(element).on('click', _.debounce(sendMail, 300, {
 *   'leading': true,
 *   'trailing': false
 * }));
 *
 * // Ensure `batchLog` is invoked once after 1 second of debounced calls.
 * var debounced = _.debounce(batchLog, 250, { 'maxWait': 1000 });
 * var source = new EventSource('/stream');
 * jQuery(source).on('message', debounced);
 *
 * // Cancel the trailing debounced invocation.
 * jQuery(window).on('popstate', debounced.cancel);
 */
function debounce(func, wait, options) {
  var lastArgs,
      lastThis,
      maxWait,
      result,
      timerId,
      lastCallTime,
      lastInvokeTime = 0,
      leading = false,
      maxing = false,
      trailing = true;

  if (typeof func != 'function') {
    throw new TypeError(FUNC_ERROR_TEXT);
  }
  wait = toNumber(wait) || 0;
  if (isObject(options)) {
    leading = !!options.leading;
    maxing = 'maxWait' in options;
    maxWait = maxing ? nativeMax(toNumber(options.maxWait) || 0, wait) : maxWait;
    trailing = 'trailing' in options ? !!options.trailing : trailing;
  }

  function invokeFunc(time) {
    var args = lastArgs,
        thisArg = lastThis;

    lastArgs = lastThis = undefined;
    lastInvokeTime = time;
    result = func.apply(thisArg, args);
    return result;
  }

  function leadingEdge(time) {
    // Reset any `maxWait` timer.
    lastInvokeTime = time;
    // Start the timer for the trailing edge.
    timerId = setTimeout(timerExpired, wait);
    // Invoke the leading edge.
    return leading ? invokeFunc(time) : result;
  }

  function remainingWait(time) {
    var timeSinceLastCall = time - lastCallTime,
        timeSinceLastInvoke = time - lastInvokeTime,
        result = wait - timeSinceLastCall;

    return maxing ? nativeMin(result, maxWait - timeSinceLastInvoke) : result;
  }

  function shouldInvoke(time) {
    var timeSinceLastCall = time - lastCallTime,
        timeSinceLastInvoke = time - lastInvokeTime;

    // Either this is the first call, activity has stopped and we're at the
    // trailing edge, the system time has gone backwards and we're treating
    // it as the trailing edge, or we've hit the `maxWait` limit.
    return (lastCallTime === undefined || (timeSinceLastCall >= wait) ||
      (timeSinceLastCall < 0) || (maxing && timeSinceLastInvoke >= maxWait));
  }

  function timerExpired() {
    var time = now();
    if (shouldInvoke(time)) {
      return trailingEdge(time);
    }
    // Restart the timer.
    timerId = setTimeout(timerExpired, remainingWait(time));
  }

  function trailingEdge(time) {
    timerId = undefined;

    // Only invoke if we have `lastArgs` which means `func` has been
    // debounced at least once.
    if (trailing && lastArgs) {
      return invokeFunc(time);
    }
    lastArgs = lastThis = undefined;
    return result;
  }

  function cancel() {
    if (timerId !== undefined) {
      clearTimeout(timerId);
    }
    lastInvokeTime = 0;
    lastArgs = lastCallTime = lastThis = timerId = undefined;
  }

  function flush() {
    return timerId === undefined ? result : trailingEdge(now());
  }

  function debounced() {
    var time = now(),
        isInvoking = shouldInvoke(time);

    lastArgs = arguments;
    lastThis = this;
    lastCallTime = time;

    if (isInvoking) {
      if (timerId === undefined) {
        return leadingEdge(lastCallTime);
      }
      if (maxing) {
        // Handle invocations in a tight loop.
        timerId = setTimeout(timerExpired, wait);
        return invokeFunc(lastCallTime);
      }
    }
    if (timerId === undefined) {
      timerId = setTimeout(timerExpired, wait);
    }
    return result;
  }
  debounced.cancel = cancel;
  debounced.flush = flush;
  return debounced;
}

module.exports = debounce;

},{"./isObject":175,"./now":183,"./toNumber":191}],159:[function(require,module,exports){
var baseDifference = require('./_baseDifference'),
    baseFlatten = require('./_baseFlatten'),
    baseRest = require('./_baseRest'),
    isArrayLikeObject = require('./isArrayLikeObject');

/**
 * Creates an array of `array` values not included in the other given arrays
 * using [`SameValueZero`](http://ecma-international.org/ecma-262/7.0/#sec-samevaluezero)
 * for equality comparisons. The order and references of result values are
 * determined by the first array.
 *
 * **Note:** Unlike `_.pullAll`, this method returns a new array.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Array
 * @param {Array} array The array to inspect.
 * @param {...Array} [values] The values to exclude.
 * @returns {Array} Returns the new array of filtered values.
 * @see _.without, _.xor
 * @example
 *
 * _.difference([2, 1], [2, 3]);
 * // => [1]
 */
var difference = baseRest(function(array, values) {
  return isArrayLikeObject(array)
    ? baseDifference(array, baseFlatten(values, 1, isArrayLikeObject, true))
    : [];
});

module.exports = difference;

},{"./_baseDifference":34,"./_baseFlatten":37,"./_baseRest":60,"./isArrayLikeObject":170}],160:[function(require,module,exports){
/**
 * Performs a
 * [`SameValueZero`](http://ecma-international.org/ecma-262/7.0/#sec-samevaluezero)
 * comparison between two values to determine if they are equivalent.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to compare.
 * @param {*} other The other value to compare.
 * @returns {boolean} Returns `true` if the values are equivalent, else `false`.
 * @example
 *
 * var object = { 'a': 1 };
 * var other = { 'a': 1 };
 *
 * _.eq(object, object);
 * // => true
 *
 * _.eq(object, other);
 * // => false
 *
 * _.eq('a', 'a');
 * // => true
 *
 * _.eq('a', Object('a'));
 * // => false
 *
 * _.eq(NaN, NaN);
 * // => true
 */
function eq(value, other) {
  return value === other || (value !== value && other !== other);
}

module.exports = eq;

},{}],161:[function(require,module,exports){
module.exports = require('./assignIn');

},{"./assignIn":155}],162:[function(require,module,exports){
var createFind = require('./_createFind'),
    findIndex = require('./findIndex');

/**
 * Iterates over elements of `collection`, returning the first element
 * `predicate` returns truthy for. The predicate is invoked with three
 * arguments: (value, index|key, collection).
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Collection
 * @param {Array|Object} collection The collection to inspect.
 * @param {Function} [predicate=_.identity] The function invoked per iteration.
 * @param {number} [fromIndex=0] The index to search from.
 * @returns {*} Returns the matched element, else `undefined`.
 * @example
 *
 * var users = [
 *   { 'user': 'barney',  'age': 36, 'active': true },
 *   { 'user': 'fred',    'age': 40, 'active': false },
 *   { 'user': 'pebbles', 'age': 1,  'active': true }
 * ];
 *
 * _.find(users, function(o) { return o.age < 40; });
 * // => object for 'barney'
 *
 * // The `_.matches` iteratee shorthand.
 * _.find(users, { 'age': 1, 'active': true });
 * // => object for 'pebbles'
 *
 * // The `_.matchesProperty` iteratee shorthand.
 * _.find(users, ['active', false]);
 * // => object for 'fred'
 *
 * // The `_.property` iteratee shorthand.
 * _.find(users, 'active');
 * // => object for 'barney'
 */
var find = createFind(findIndex);

module.exports = find;

},{"./_createFind":84,"./findIndex":163}],163:[function(require,module,exports){
var baseFindIndex = require('./_baseFindIndex'),
    baseIteratee = require('./_baseIteratee'),
    toInteger = require('./toInteger');

/* Built-in method references for those with the same name as other `lodash` methods. */
var nativeMax = Math.max;

/**
 * This method is like `_.find` except that it returns the index of the first
 * element `predicate` returns truthy for instead of the element itself.
 *
 * @static
 * @memberOf _
 * @since 1.1.0
 * @category Array
 * @param {Array} array The array to inspect.
 * @param {Function} [predicate=_.identity] The function invoked per iteration.
 * @param {number} [fromIndex=0] The index to search from.
 * @returns {number} Returns the index of the found element, else `-1`.
 * @example
 *
 * var users = [
 *   { 'user': 'barney',  'active': false },
 *   { 'user': 'fred',    'active': false },
 *   { 'user': 'pebbles', 'active': true }
 * ];
 *
 * _.findIndex(users, function(o) { return o.user == 'barney'; });
 * // => 0
 *
 * // The `_.matches` iteratee shorthand.
 * _.findIndex(users, { 'user': 'fred', 'active': false });
 * // => 1
 *
 * // The `_.matchesProperty` iteratee shorthand.
 * _.findIndex(users, ['active', false]);
 * // => 0
 *
 * // The `_.property` iteratee shorthand.
 * _.findIndex(users, 'active');
 * // => 2
 */
function findIndex(array, predicate, fromIndex) {
  var length = array == null ? 0 : array.length;
  if (!length) {
    return -1;
  }
  var index = fromIndex == null ? 0 : toInteger(fromIndex);
  if (index < 0) {
    index = nativeMax(length + index, 0);
  }
  return baseFindIndex(array, baseIteratee(predicate, 3), index);
}

module.exports = findIndex;

},{"./_baseFindIndex":36,"./_baseIteratee":52,"./toInteger":190}],164:[function(require,module,exports){
var baseGet = require('./_baseGet');

/**
 * Gets the value at `path` of `object`. If the resolved value is
 * `undefined`, the `defaultValue` is returned in its place.
 *
 * @static
 * @memberOf _
 * @since 3.7.0
 * @category Object
 * @param {Object} object The object to query.
 * @param {Array|string} path The path of the property to get.
 * @param {*} [defaultValue] The value returned for `undefined` resolved values.
 * @returns {*} Returns the resolved value.
 * @example
 *
 * var object = { 'a': [{ 'b': { 'c': 3 } }] };
 *
 * _.get(object, 'a[0].b.c');
 * // => 3
 *
 * _.get(object, ['a', '0', 'b', 'c']);
 * // => 3
 *
 * _.get(object, 'a.b.c', 'default');
 * // => 'default'
 */
function get(object, path, defaultValue) {
  var result = object == null ? undefined : baseGet(object, path);
  return result === undefined ? defaultValue : result;
}

module.exports = get;

},{"./_baseGet":40}],165:[function(require,module,exports){
var baseHasIn = require('./_baseHasIn'),
    hasPath = require('./_hasPath');

/**
 * Checks if `path` is a direct or inherited property of `object`.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Object
 * @param {Object} object The object to query.
 * @param {Array|string} path The path to check.
 * @returns {boolean} Returns `true` if `path` exists, else `false`.
 * @example
 *
 * var object = _.create({ 'a': _.create({ 'b': 2 }) });
 *
 * _.hasIn(object, 'a');
 * // => true
 *
 * _.hasIn(object, 'a.b');
 * // => true
 *
 * _.hasIn(object, ['a', 'b']);
 * // => true
 *
 * _.hasIn(object, 'b');
 * // => false
 */
function hasIn(object, path) {
  return object != null && hasPath(object, path, baseHasIn);
}

module.exports = hasIn;

},{"./_baseHasIn":43,"./_hasPath":102}],166:[function(require,module,exports){
/**
 * This method returns the first argument it receives.
 *
 * @static
 * @since 0.1.0
 * @memberOf _
 * @category Util
 * @param {*} value Any value.
 * @returns {*} Returns `value`.
 * @example
 *
 * var object = { 'a': 1 };
 *
 * console.log(_.identity(object) === object);
 * // => true
 */
function identity(value) {
  return value;
}

module.exports = identity;

},{}],167:[function(require,module,exports){
var baseIsArguments = require('./_baseIsArguments'),
    isObjectLike = require('./isObjectLike');

/** Used for built-in method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/** Built-in value references. */
var propertyIsEnumerable = objectProto.propertyIsEnumerable;

/**
 * Checks if `value` is likely an `arguments` object.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an `arguments` object,
 *  else `false`.
 * @example
 *
 * _.isArguments(function() { return arguments; }());
 * // => true
 *
 * _.isArguments([1, 2, 3]);
 * // => false
 */
var isArguments = baseIsArguments(function() { return arguments; }()) ? baseIsArguments : function(value) {
  return isObjectLike(value) && hasOwnProperty.call(value, 'callee') &&
    !propertyIsEnumerable.call(value, 'callee');
};

module.exports = isArguments;

},{"./_baseIsArguments":45,"./isObjectLike":176}],168:[function(require,module,exports){
/**
 * Checks if `value` is classified as an `Array` object.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an array, else `false`.
 * @example
 *
 * _.isArray([1, 2, 3]);
 * // => true
 *
 * _.isArray(document.body.children);
 * // => false
 *
 * _.isArray('abc');
 * // => false
 *
 * _.isArray(_.noop);
 * // => false
 */
var isArray = Array.isArray;

module.exports = isArray;

},{}],169:[function(require,module,exports){
var isFunction = require('./isFunction'),
    isLength = require('./isLength');

/**
 * Checks if `value` is array-like. A value is considered array-like if it's
 * not a function and has a `value.length` that's an integer greater than or
 * equal to `0` and less than or equal to `Number.MAX_SAFE_INTEGER`.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is array-like, else `false`.
 * @example
 *
 * _.isArrayLike([1, 2, 3]);
 * // => true
 *
 * _.isArrayLike(document.body.children);
 * // => true
 *
 * _.isArrayLike('abc');
 * // => true
 *
 * _.isArrayLike(_.noop);
 * // => false
 */
function isArrayLike(value) {
  return value != null && isLength(value.length) && !isFunction(value);
}

module.exports = isArrayLike;

},{"./isFunction":173,"./isLength":174}],170:[function(require,module,exports){
var isArrayLike = require('./isArrayLike'),
    isObjectLike = require('./isObjectLike');

/**
 * This method is like `_.isArrayLike` except that it also checks if `value`
 * is an object.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an array-like object,
 *  else `false`.
 * @example
 *
 * _.isArrayLikeObject([1, 2, 3]);
 * // => true
 *
 * _.isArrayLikeObject(document.body.children);
 * // => true
 *
 * _.isArrayLikeObject('abc');
 * // => false
 *
 * _.isArrayLikeObject(_.noop);
 * // => false
 */
function isArrayLikeObject(value) {
  return isObjectLike(value) && isArrayLike(value);
}

module.exports = isArrayLikeObject;

},{"./isArrayLike":169,"./isObjectLike":176}],171:[function(require,module,exports){
var root = require('./_root'),
    stubFalse = require('./stubFalse');

/** Detect free variable `exports`. */
var freeExports = typeof exports == 'object' && exports && !exports.nodeType && exports;

/** Detect free variable `module`. */
var freeModule = freeExports && typeof module == 'object' && module && !module.nodeType && module;

/** Detect the popular CommonJS extension `module.exports`. */
var moduleExports = freeModule && freeModule.exports === freeExports;

/** Built-in value references. */
var Buffer = moduleExports ? root.Buffer : undefined;

/* Built-in method references for those with the same name as other `lodash` methods. */
var nativeIsBuffer = Buffer ? Buffer.isBuffer : undefined;

/**
 * Checks if `value` is a buffer.
 *
 * @static
 * @memberOf _
 * @since 4.3.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a buffer, else `false`.
 * @example
 *
 * _.isBuffer(new Buffer(2));
 * // => true
 *
 * _.isBuffer(new Uint8Array(2));
 * // => false
 */
var isBuffer = nativeIsBuffer || stubFalse;

module.exports = isBuffer;

},{"./_root":139,"./stubFalse":187}],172:[function(require,module,exports){
var baseIsEqual = require('./_baseIsEqual');

/**
 * Performs a deep comparison between two values to determine if they are
 * equivalent.
 *
 * **Note:** This method supports comparing arrays, array buffers, booleans,
 * date objects, error objects, maps, numbers, `Object` objects, regexes,
 * sets, strings, symbols, and typed arrays. `Object` objects are compared
 * by their own, not inherited, enumerable properties. Functions and DOM
 * nodes are compared by strict equality, i.e. `===`.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to compare.
 * @param {*} other The other value to compare.
 * @returns {boolean} Returns `true` if the values are equivalent, else `false`.
 * @example
 *
 * var object = { 'a': 1 };
 * var other = { 'a': 1 };
 *
 * _.isEqual(object, other);
 * // => true
 *
 * object === other;
 * // => false
 */
function isEqual(value, other) {
  return baseIsEqual(value, other);
}

module.exports = isEqual;

},{"./_baseIsEqual":46}],173:[function(require,module,exports){
var baseGetTag = require('./_baseGetTag'),
    isObject = require('./isObject');

/** `Object#toString` result references. */
var asyncTag = '[object AsyncFunction]',
    funcTag = '[object Function]',
    genTag = '[object GeneratorFunction]',
    proxyTag = '[object Proxy]';

/**
 * Checks if `value` is classified as a `Function` object.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a function, else `false`.
 * @example
 *
 * _.isFunction(_);
 * // => true
 *
 * _.isFunction(/abc/);
 * // => false
 */
function isFunction(value) {
  if (!isObject(value)) {
    return false;
  }
  // The use of `Object#toString` avoids issues with the `typeof` operator
  // in Safari 9 which returns 'object' for typed arrays and other constructors.
  var tag = baseGetTag(value);
  return tag == funcTag || tag == genTag || tag == asyncTag || tag == proxyTag;
}

module.exports = isFunction;

},{"./_baseGetTag":42,"./isObject":175}],174:[function(require,module,exports){
/** Used as references for various `Number` constants. */
var MAX_SAFE_INTEGER = 9007199254740991;

/**
 * Checks if `value` is a valid array-like length.
 *
 * **Note:** This method is loosely based on
 * [`ToLength`](http://ecma-international.org/ecma-262/7.0/#sec-tolength).
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a valid length, else `false`.
 * @example
 *
 * _.isLength(3);
 * // => true
 *
 * _.isLength(Number.MIN_VALUE);
 * // => false
 *
 * _.isLength(Infinity);
 * // => false
 *
 * _.isLength('3');
 * // => false
 */
function isLength(value) {
  return typeof value == 'number' &&
    value > -1 && value % 1 == 0 && value <= MAX_SAFE_INTEGER;
}

module.exports = isLength;

},{}],175:[function(require,module,exports){
/**
 * Checks if `value` is the
 * [language type](http://www.ecma-international.org/ecma-262/7.0/#sec-ecmascript-language-types)
 * of `Object`. (e.g. arrays, functions, objects, regexes, `new Number(0)`, and `new String('')`)
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an object, else `false`.
 * @example
 *
 * _.isObject({});
 * // => true
 *
 * _.isObject([1, 2, 3]);
 * // => true
 *
 * _.isObject(_.noop);
 * // => true
 *
 * _.isObject(null);
 * // => false
 */
function isObject(value) {
  var type = typeof value;
  return value != null && (type == 'object' || type == 'function');
}

module.exports = isObject;

},{}],176:[function(require,module,exports){
/**
 * Checks if `value` is object-like. A value is object-like if it's not `null`
 * and has a `typeof` result of "object".
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is object-like, else `false`.
 * @example
 *
 * _.isObjectLike({});
 * // => true
 *
 * _.isObjectLike([1, 2, 3]);
 * // => true
 *
 * _.isObjectLike(_.noop);
 * // => false
 *
 * _.isObjectLike(null);
 * // => false
 */
function isObjectLike(value) {
  return value != null && typeof value == 'object';
}

module.exports = isObjectLike;

},{}],177:[function(require,module,exports){
var baseGetTag = require('./_baseGetTag'),
    isObjectLike = require('./isObjectLike');

/** `Object#toString` result references. */
var symbolTag = '[object Symbol]';

/**
 * Checks if `value` is classified as a `Symbol` primitive or object.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a symbol, else `false`.
 * @example
 *
 * _.isSymbol(Symbol.iterator);
 * // => true
 *
 * _.isSymbol('abc');
 * // => false
 */
function isSymbol(value) {
  return typeof value == 'symbol' ||
    (isObjectLike(value) && baseGetTag(value) == symbolTag);
}

module.exports = isSymbol;

},{"./_baseGetTag":42,"./isObjectLike":176}],178:[function(require,module,exports){
var baseIsTypedArray = require('./_baseIsTypedArray'),
    baseUnary = require('./_baseUnary'),
    nodeUtil = require('./_nodeUtil');

/* Node.js helper references. */
var nodeIsTypedArray = nodeUtil && nodeUtil.isTypedArray;

/**
 * Checks if `value` is classified as a typed array.
 *
 * @static
 * @memberOf _
 * @since 3.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a typed array, else `false`.
 * @example
 *
 * _.isTypedArray(new Uint8Array);
 * // => true
 *
 * _.isTypedArray([]);
 * // => false
 */
var isTypedArray = nodeIsTypedArray ? baseUnary(nodeIsTypedArray) : baseIsTypedArray;

module.exports = isTypedArray;

},{"./_baseIsTypedArray":51,"./_baseUnary":64,"./_nodeUtil":135}],179:[function(require,module,exports){
var arrayLikeKeys = require('./_arrayLikeKeys'),
    baseKeys = require('./_baseKeys'),
    isArrayLike = require('./isArrayLike');

/**
 * Creates an array of the own enumerable property names of `object`.
 *
 * **Note:** Non-object values are coerced to objects. See the
 * [ES spec](http://ecma-international.org/ecma-262/7.0/#sec-object.keys)
 * for more details.
 *
 * @static
 * @since 0.1.0
 * @memberOf _
 * @category Object
 * @param {Object} object The object to query.
 * @returns {Array} Returns the array of property names.
 * @example
 *
 * function Foo() {
 *   this.a = 1;
 *   this.b = 2;
 * }
 *
 * Foo.prototype.c = 3;
 *
 * _.keys(new Foo);
 * // => ['a', 'b'] (iteration order is not guaranteed)
 *
 * _.keys('hi');
 * // => ['0', '1']
 */
function keys(object) {
  return isArrayLike(object) ? arrayLikeKeys(object) : baseKeys(object);
}

module.exports = keys;

},{"./_arrayLikeKeys":22,"./_baseKeys":53,"./isArrayLike":169}],180:[function(require,module,exports){
var arrayLikeKeys = require('./_arrayLikeKeys'),
    baseKeysIn = require('./_baseKeysIn'),
    isArrayLike = require('./isArrayLike');

/**
 * Creates an array of the own and inherited enumerable property names of `object`.
 *
 * **Note:** Non-object values are coerced to objects.
 *
 * @static
 * @memberOf _
 * @since 3.0.0
 * @category Object
 * @param {Object} object The object to query.
 * @returns {Array} Returns the array of property names.
 * @example
 *
 * function Foo() {
 *   this.a = 1;
 *   this.b = 2;
 * }
 *
 * Foo.prototype.c = 3;
 *
 * _.keysIn(new Foo);
 * // => ['a', 'b', 'c'] (iteration order is not guaranteed)
 */
function keysIn(object) {
  return isArrayLike(object) ? arrayLikeKeys(object, true) : baseKeysIn(object);
}

module.exports = keysIn;

},{"./_arrayLikeKeys":22,"./_baseKeysIn":54,"./isArrayLike":169}],181:[function(require,module,exports){
var MapCache = require('./_MapCache');

/** Error message constants. */
var FUNC_ERROR_TEXT = 'Expected a function';

/**
 * Creates a function that memoizes the result of `func`. If `resolver` is
 * provided, it determines the cache key for storing the result based on the
 * arguments provided to the memoized function. By default, the first argument
 * provided to the memoized function is used as the map cache key. The `func`
 * is invoked with the `this` binding of the memoized function.
 *
 * **Note:** The cache is exposed as the `cache` property on the memoized
 * function. Its creation may be customized by replacing the `_.memoize.Cache`
 * constructor with one whose instances implement the
 * [`Map`](http://ecma-international.org/ecma-262/7.0/#sec-properties-of-the-map-prototype-object)
 * method interface of `clear`, `delete`, `get`, `has`, and `set`.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Function
 * @param {Function} func The function to have its output memoized.
 * @param {Function} [resolver] The function to resolve the cache key.
 * @returns {Function} Returns the new memoized function.
 * @example
 *
 * var object = { 'a': 1, 'b': 2 };
 * var other = { 'c': 3, 'd': 4 };
 *
 * var values = _.memoize(_.values);
 * values(object);
 * // => [1, 2]
 *
 * values(other);
 * // => [3, 4]
 *
 * object.a = 2;
 * values(object);
 * // => [1, 2]
 *
 * // Modify the result cache.
 * values.cache.set(object, ['a', 'b']);
 * values(object);
 * // => ['a', 'b']
 *
 * // Replace `_.memoize.Cache`.
 * _.memoize.Cache = WeakMap;
 */
function memoize(func, resolver) {
  if (typeof func != 'function' || (resolver != null && typeof resolver != 'function')) {
    throw new TypeError(FUNC_ERROR_TEXT);
  }
  var memoized = function() {
    var args = arguments,
        key = resolver ? resolver.apply(this, args) : args[0],
        cache = memoized.cache;

    if (cache.has(key)) {
      return cache.get(key);
    }
    var result = func.apply(this, args);
    memoized.cache = cache.set(key, result) || cache;
    return result;
  };
  memoized.cache = new (memoize.Cache || MapCache);
  return memoized;
}

// Expose `MapCache`.
memoize.Cache = MapCache;

module.exports = memoize;

},{"./_MapCache":7}],182:[function(require,module,exports){
/**
 * This method returns `undefined`.
 *
 * @static
 * @memberOf _
 * @since 2.3.0
 * @category Util
 * @example
 *
 * _.times(2, _.noop);
 * // => [undefined, undefined]
 */
function noop() {
  // No operation performed.
}

module.exports = noop;

},{}],183:[function(require,module,exports){
var root = require('./_root');

/**
 * Gets the timestamp of the number of milliseconds that have elapsed since
 * the Unix epoch (1 January 1970 00:00:00 UTC).
 *
 * @static
 * @memberOf _
 * @since 2.4.0
 * @category Date
 * @returns {number} Returns the timestamp.
 * @example
 *
 * _.defer(function(stamp) {
 *   console.log(_.now() - stamp);
 * }, _.now());
 * // => Logs the number of milliseconds it took for the deferred invocation.
 */
var now = function() {
  return root.Date.now();
};

module.exports = now;

},{"./_root":139}],184:[function(require,module,exports){
var baseProperty = require('./_baseProperty'),
    basePropertyDeep = require('./_basePropertyDeep'),
    isKey = require('./_isKey'),
    toKey = require('./_toKey');

/**
 * Creates a function that returns the value at `path` of a given object.
 *
 * @static
 * @memberOf _
 * @since 2.4.0
 * @category Util
 * @param {Array|string} path The path of the property to get.
 * @returns {Function} Returns the new accessor function.
 * @example
 *
 * var objects = [
 *   { 'a': { 'b': 2 } },
 *   { 'a': { 'b': 1 } }
 * ];
 *
 * _.map(objects, _.property('a.b'));
 * // => [2, 1]
 *
 * _.map(_.sortBy(objects, _.property(['a', 'b'])), 'a.b');
 * // => [1, 2]
 */
function property(path) {
  return isKey(path) ? baseProperty(toKey(path)) : basePropertyDeep(path);
}

module.exports = property;

},{"./_baseProperty":57,"./_basePropertyDeep":58,"./_isKey":114,"./_toKey":152}],185:[function(require,module,exports){
var arrayReduce = require('./_arrayReduce'),
    baseEach = require('./_baseEach'),
    baseIteratee = require('./_baseIteratee'),
    baseReduce = require('./_baseReduce'),
    isArray = require('./isArray');

/**
 * Reduces `collection` to a value which is the accumulated result of running
 * each element in `collection` thru `iteratee`, where each successive
 * invocation is supplied the return value of the previous. If `accumulator`
 * is not given, the first element of `collection` is used as the initial
 * value. The iteratee is invoked with four arguments:
 * (accumulator, value, index|key, collection).
 *
 * Many lodash methods are guarded to work as iteratees for methods like
 * `_.reduce`, `_.reduceRight`, and `_.transform`.
 *
 * The guarded methods are:
 * `assign`, `defaults`, `defaultsDeep`, `includes`, `merge`, `orderBy`,
 * and `sortBy`
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Collection
 * @param {Array|Object} collection The collection to iterate over.
 * @param {Function} [iteratee=_.identity] The function invoked per iteration.
 * @param {*} [accumulator] The initial value.
 * @returns {*} Returns the accumulated value.
 * @see _.reduceRight
 * @example
 *
 * _.reduce([1, 2], function(sum, n) {
 *   return sum + n;
 * }, 0);
 * // => 3
 *
 * _.reduce({ 'a': 1, 'b': 2, 'c': 1 }, function(result, value, key) {
 *   (result[value] || (result[value] = [])).push(key);
 *   return result;
 * }, {});
 * // => { '1': ['a', 'c'], '2': ['b'] } (iteration order is not guaranteed)
 */
function reduce(collection, iteratee, accumulator) {
  var func = isArray(collection) ? arrayReduce : baseReduce,
      initAccum = arguments.length < 3;

  return func(collection, baseIteratee(iteratee, 4), accumulator, initAccum, baseEach);
}

module.exports = reduce;

},{"./_arrayReduce":25,"./_baseEach":35,"./_baseIteratee":52,"./_baseReduce":59,"./isArray":168}],186:[function(require,module,exports){
/**
 * This method returns a new empty array.
 *
 * @static
 * @memberOf _
 * @since 4.13.0
 * @category Util
 * @returns {Array} Returns the new empty array.
 * @example
 *
 * var arrays = _.times(2, _.stubArray);
 *
 * console.log(arrays);
 * // => [[], []]
 *
 * console.log(arrays[0] === arrays[1]);
 * // => false
 */
function stubArray() {
  return [];
}

module.exports = stubArray;

},{}],187:[function(require,module,exports){
/**
 * This method returns `false`.
 *
 * @static
 * @memberOf _
 * @since 4.13.0
 * @category Util
 * @returns {boolean} Returns `false`.
 * @example
 *
 * _.times(2, _.stubFalse);
 * // => [false, false]
 */
function stubFalse() {
  return false;
}

module.exports = stubFalse;

},{}],188:[function(require,module,exports){
var debounce = require('./debounce'),
    isObject = require('./isObject');

/** Error message constants. */
var FUNC_ERROR_TEXT = 'Expected a function';

/**
 * Creates a throttled function that only invokes `func` at most once per
 * every `wait` milliseconds. The throttled function comes with a `cancel`
 * method to cancel delayed `func` invocations and a `flush` method to
 * immediately invoke them. Provide `options` to indicate whether `func`
 * should be invoked on the leading and/or trailing edge of the `wait`
 * timeout. The `func` is invoked with the last arguments provided to the
 * throttled function. Subsequent calls to the throttled function return the
 * result of the last `func` invocation.
 *
 * **Note:** If `leading` and `trailing` options are `true`, `func` is
 * invoked on the trailing edge of the timeout only if the throttled function
 * is invoked more than once during the `wait` timeout.
 *
 * If `wait` is `0` and `leading` is `false`, `func` invocation is deferred
 * until to the next tick, similar to `setTimeout` with a timeout of `0`.
 *
 * See [David Corbacho's article](https://css-tricks.com/debouncing-throttling-explained-examples/)
 * for details over the differences between `_.throttle` and `_.debounce`.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Function
 * @param {Function} func The function to throttle.
 * @param {number} [wait=0] The number of milliseconds to throttle invocations to.
 * @param {Object} [options={}] The options object.
 * @param {boolean} [options.leading=true]
 *  Specify invoking on the leading edge of the timeout.
 * @param {boolean} [options.trailing=true]
 *  Specify invoking on the trailing edge of the timeout.
 * @returns {Function} Returns the new throttled function.
 * @example
 *
 * // Avoid excessively updating the position while scrolling.
 * jQuery(window).on('scroll', _.throttle(updatePosition, 100));
 *
 * // Invoke `renewToken` when the click event is fired, but not more than once every 5 minutes.
 * var throttled = _.throttle(renewToken, 300000, { 'trailing': false });
 * jQuery(element).on('click', throttled);
 *
 * // Cancel the trailing throttled invocation.
 * jQuery(window).on('popstate', throttled.cancel);
 */
function throttle(func, wait, options) {
  var leading = true,
      trailing = true;

  if (typeof func != 'function') {
    throw new TypeError(FUNC_ERROR_TEXT);
  }
  if (isObject(options)) {
    leading = 'leading' in options ? !!options.leading : leading;
    trailing = 'trailing' in options ? !!options.trailing : trailing;
  }
  return debounce(func, wait, {
    'leading': leading,
    'maxWait': wait,
    'trailing': trailing
  });
}

module.exports = throttle;

},{"./debounce":158,"./isObject":175}],189:[function(require,module,exports){
var toNumber = require('./toNumber');

/** Used as references for various `Number` constants. */
var INFINITY = 1 / 0,
    MAX_INTEGER = 1.7976931348623157e+308;

/**
 * Converts `value` to a finite number.
 *
 * @static
 * @memberOf _
 * @since 4.12.0
 * @category Lang
 * @param {*} value The value to convert.
 * @returns {number} Returns the converted number.
 * @example
 *
 * _.toFinite(3.2);
 * // => 3.2
 *
 * _.toFinite(Number.MIN_VALUE);
 * // => 5e-324
 *
 * _.toFinite(Infinity);
 * // => 1.7976931348623157e+308
 *
 * _.toFinite('3.2');
 * // => 3.2
 */
function toFinite(value) {
  if (!value) {
    return value === 0 ? value : 0;
  }
  value = toNumber(value);
  if (value === INFINITY || value === -INFINITY) {
    var sign = (value < 0 ? -1 : 1);
    return sign * MAX_INTEGER;
  }
  return value === value ? value : 0;
}

module.exports = toFinite;

},{"./toNumber":191}],190:[function(require,module,exports){
var toFinite = require('./toFinite');

/**
 * Converts `value` to an integer.
 *
 * **Note:** This method is loosely based on
 * [`ToInteger`](http://www.ecma-international.org/ecma-262/7.0/#sec-tointeger).
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to convert.
 * @returns {number} Returns the converted integer.
 * @example
 *
 * _.toInteger(3.2);
 * // => 3
 *
 * _.toInteger(Number.MIN_VALUE);
 * // => 0
 *
 * _.toInteger(Infinity);
 * // => 1.7976931348623157e+308
 *
 * _.toInteger('3.2');
 * // => 3
 */
function toInteger(value) {
  var result = toFinite(value),
      remainder = result % 1;

  return result === result ? (remainder ? result - remainder : result) : 0;
}

module.exports = toInteger;

},{"./toFinite":189}],191:[function(require,module,exports){
var isObject = require('./isObject'),
    isSymbol = require('./isSymbol');

/** Used as references for various `Number` constants. */
var NAN = 0 / 0;

/** Used to match leading and trailing whitespace. */
var reTrim = /^\s+|\s+$/g;

/** Used to detect bad signed hexadecimal string values. */
var reIsBadHex = /^[-+]0x[0-9a-f]+$/i;

/** Used to detect binary string values. */
var reIsBinary = /^0b[01]+$/i;

/** Used to detect octal string values. */
var reIsOctal = /^0o[0-7]+$/i;

/** Built-in method references without a dependency on `root`. */
var freeParseInt = parseInt;

/**
 * Converts `value` to a number.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to process.
 * @returns {number} Returns the number.
 * @example
 *
 * _.toNumber(3.2);
 * // => 3.2
 *
 * _.toNumber(Number.MIN_VALUE);
 * // => 5e-324
 *
 * _.toNumber(Infinity);
 * // => Infinity
 *
 * _.toNumber('3.2');
 * // => 3.2
 */
function toNumber(value) {
  if (typeof value == 'number') {
    return value;
  }
  if (isSymbol(value)) {
    return NAN;
  }
  if (isObject(value)) {
    var other = typeof value.valueOf == 'function' ? value.valueOf() : value;
    value = isObject(other) ? (other + '') : other;
  }
  if (typeof value != 'string') {
    return value === 0 ? value : +value;
  }
  value = value.replace(reTrim, '');
  var isBinary = reIsBinary.test(value);
  return (isBinary || reIsOctal.test(value))
    ? freeParseInt(value.slice(2), isBinary ? 2 : 8)
    : (reIsBadHex.test(value) ? NAN : +value);
}

module.exports = toNumber;

},{"./isObject":175,"./isSymbol":177}],192:[function(require,module,exports){
var baseToString = require('./_baseToString');

/**
 * Converts `value` to a string. An empty string is returned for `null`
 * and `undefined` values. The sign of `-0` is preserved.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to convert.
 * @returns {string} Returns the converted string.
 * @example
 *
 * _.toString(null);
 * // => ''
 *
 * _.toString(-0);
 * // => '-0'
 *
 * _.toString([1, 2, 3]);
 * // => '1,2,3'
 */
function toString(value) {
  return value == null ? '' : baseToString(value);
}

module.exports = toString;

},{"./_baseToString":63}],193:[function(require,module,exports){
var baseFlatten = require('./_baseFlatten'),
    baseRest = require('./_baseRest'),
    baseUniq = require('./_baseUniq'),
    isArrayLikeObject = require('./isArrayLikeObject');

/**
 * Creates an array of unique values, in order, from all given arrays using
 * [`SameValueZero`](http://ecma-international.org/ecma-262/7.0/#sec-samevaluezero)
 * for equality comparisons.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Array
 * @param {...Array} [arrays] The arrays to inspect.
 * @returns {Array} Returns the new array of combined values.
 * @example
 *
 * _.union([2], [1, 2]);
 * // => [2, 1]
 */
var union = baseRest(function(arrays) {
  return baseUniq(baseFlatten(arrays, 1, isArrayLikeObject, true));
});

module.exports = union;

},{"./_baseFlatten":37,"./_baseRest":60,"./_baseUniq":65,"./isArrayLikeObject":170}],194:[function(require,module,exports){
// Avoid `console` errors in browsers that lack a console.
// borrowed from H5BP: https://github.com/h5bp/html5-boilerplate/blob/master/src/js/plugins.js
'use strict';

module.exports = devConsole;

devConsole();

function devConsole() {
    var method;
    var noop = function noop() {};
    var methods = ['assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error', 'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log', 'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd', 'timeline', 'timelineEnd', 'timeStamp', 'trace', 'warn'];
    var length = methods.length;
    var console = window.console = window.console || {};

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}

},{}],195:[function(require,module,exports){
// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

// explicitly inject dependencies (alphabetically), only those needed

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

var _advertisements = require('../advertisements/advertisements');

var _advertisements2 = _interopRequireDefault(_advertisements);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _advertisements2.default;

// component configuration

var COMPONENT_LAZY_SELECTOR = '[data-advertisement-lazy]';
var LAZY_ONSCROLL_SELECTOR = '[data-advertisement-lazy-onscroll]';
var LAZY_ONEXPANDED_SELECTOR = '[data-advertisement-lazy-onscroll-onexpanded]';
var BP_MEDIUM = 750;
var EVENT_NAMESPACE_AD = '.advertisement';
var WINDOW_WIDTH = (0, _jquery2.default)(window).width();
var NO_DESKTOP = WINDOW_WIDTH < BP_MEDIUM - 1;
var COMPONENT_LAZY_ASYNC_SELECTOR = '[data-advertisement-searchresults]';

/**
 * Default lazy ad initializer behaviour.
 * @param {array} elements - list of lazy ads.
 * @param {string} classFlag - classname of each ad on the first load.
 */
_advertisements2.default.lazyInitialize = function ($elements) {
    var component = this;
    window.googletag.cmd.push(function () {
        [].forEach.call($elements, function (element) {
            var config = _advertisements2.default.getAdConfig(element);
            if (config !== false) {
                component.instances.push(element);
                element.id = config.id;
                component.createLazySlot(element, config);
                if ((0, _jquery2.default)(element).attr('data-advertisement-searchresults') != undefined) {
                    component.registerOnSlotRenderCallback('async');
                }
            }
        });
        window.googletag.pubads().collapseEmptyDivs();
        window.googletag.pubads().disableInitialLoad();
        window.googletag.enableServices();
    });
};

/**
 * Creating lazy slot.
 * @param {object} element - lazy ad.
 * @param {object} config - config of lazy ad.
 */
_advertisements2.default.createLazySlot = function (element, config) {
    var component = this;
    var cachedCategories = this.cachedCategoriesByUrl[config.categoriesUrl];

    if (cachedCategories) {
        component.pushLazyAdSlot(element, config, cachedCategories);
    } else {
        _jquery2.default.get({
            url: config.categoriesUrl,
            success: function success(categories) {
                component.pushLazyAdSlot(element, config, categories);
            },
            error: function error() {
                component.pushLazyAdSlot(element, config, {});
            }
        });
    }
};

/**
 * Pushing lazy slot to DFP.
 * @param {object} element - lazy ad.
 * @param {object} config - config of lazy ad.
 * @param {object} categories - categories of lazy ad.
 */
_advertisements2.default.pushLazyAdSlot = function (element, config, categories) {
    var alreadyShown = false;
    var ONSCROLL_ONEXPANDED_ATTR_CHECKER = (0, _jquery2.default)(element).attr('data-advertisement-lazy-onscroll-onexpanded') == '';
    var tag = window.googletag.defineSlot(config.adunitpath, config.sizedefault, config.id).defineSizeMapping(config.sizemapping).setCollapseEmptyDiv(true, true).addService(window.googletag.pubads());

    for (var key in categories) {
        if (categories.hasOwnProperty(key)) {
            tag.setTargeting(key, categories[key]);
        }
    }

    // Checker for category banners on kenmerken expanded.
    if (NO_DESKTOP && ONSCROLL_ONEXPANDED_ATTR_CHECKER) {
        _advertisements2.default.pushLazyAdSlotOnMobileOnKenmerkenExpanded(element, config, tag);
    } else {
        _advertisements2.default.pushLazyAdSlotOnDefaultBehaviour(element, config, tag, alreadyShown);
    }
};

/**
 * Pushing lazy slot on mobile, on kenmerken expanded to DFP.
 * @param {object} element - lazy ad.
 * @param {object} config - config of lazy ad.
 * @param {object} tag - lazy ad tag.
 */
_advertisements2.default.pushLazyAdSlotOnMobileOnKenmerkenExpanded = function (element, config, tag) {
    (0, _jquery2.default)(element).addClass('dfp-display-triggered');
    window.googletag.cmd.push(function () {
        window.googletag.display(config.id);
        window.googletag.pubads().refresh([tag]);
    });
    _advertisements2.default.unregisterScrollEventHandlerWhenNotNeeded();
};

/**
 * Pushing lazy slot on default behavior (listening to scroll)
 * @param {object} element - lazy ad.
 * @param {object} config - config of lazy ad.
 * @param {object} tag - lazy ad tag.
 * @param {boolean} alreadyShown - a flag to mark if an ad is already shown or not.
 */
_advertisements2.default.pushLazyAdSlotOnDefaultBehaviour = function (element, config, tag, alreadyShown) {
    if (_advertisements2.default.isOnScreen(element)) {
        alreadyShown = true;
        (0, _jquery2.default)(element).addClass('dfp-display-triggered');
        window.googletag.cmd.push(function () {
            window.googletag.display(config.id);
            window.googletag.pubads().refresh([tag]);
        });
    }
    _advertisements2.default.registerOnScrollEventHandler(element, config, tag, alreadyShown);
};

/**
 * Registering scroll event handler that checks if an ad is on the screen and visible.
 * @param {object} element - lazy ad.
 * @param {object} config - config of lazy ad.
 * @param {object} tag - ad tag.
 * @param {boolean} alreadyShown - a flag to mark if an ad is already shown or not.
 */
_advertisements2.default.registerOnScrollEventHandler = function (element, config, tag, alreadyShown) {
    (0, _jquery2.default)(window).on('scroll' + EVENT_NAMESPACE_AD, function scrollHandler() {
        if (_advertisements2.default.isOnScreen(element)) {
            if (!alreadyShown) {
                (0, _jquery2.default)(element).addClass('dfp-display-triggered');
                window.googletag.cmd.push(function () {
                    window.googletag.display(config.id);
                    window.googletag.pubads().refresh([tag]);
                });
                alreadyShown = true;
            }

            _advertisements2.default.unregisterScrollEventHandlerWhenNotNeeded(element);
        }
    });
};

_advertisements2.default.unregisterScrollEventHandlerWhenNotNeeded = function () {
    var numberOfLazyAds = (0, _jquery2.default)('[data-advertisement-lazy-onscroll-onexpanded], [data-advertisement-lazy-onscroll], [data-advertisement-searchresults]').length;
    var numberOfSuccessLazyAds = (0, _jquery2.default)('[data-advertisement-lazy-onscroll-onexpanded].dfp-display-triggered, [data-advertisement-lazy-onscroll].dfp-display-triggered, [data-advertisement-searchresults].dfp-display-triggered').length;
    if (numberOfSuccessLazyAds >= numberOfLazyAds) {
        _advertisements2.default.unregisterEvent('scroll' + EVENT_NAMESPACE_AD);
    }
};

_advertisements2.default.unregisterEvent = function (eventName) {
    (0, _jquery2.default)(window).off(eventName);
};

// START: Init the lazy ads on Object details
if ((0, _jquery2.default)(COMPONENT_LAZY_SELECTOR).length && WINDOW_WIDTH > BP_MEDIUM) {
    _advertisements2.default.lazyInitialize((0, _jquery2.default)(LAZY_ONEXPANDED_SELECTOR + ',' + LAZY_ONSCROLL_SELECTOR));
} else if ((0, _jquery2.default)(COMPONENT_LAZY_SELECTOR).length && NO_DESKTOP) {
    _advertisements2.default.lazyInitialize((0, _jquery2.default)(LAZY_ONSCROLL_SELECTOR));

    (0, _jquery2.default)(document).on('kenmerkenExpanded', function (event, data) {
        var kenmerkenExpandedSelector = document.querySelector(data.selector);
        _advertisements2.default.lazyInitialize(kenmerkenExpandedSelector.querySelectorAll(LAZY_ONEXPANDED_SELECTOR));
    });
}
// END: Init the lazy ads on Object details

// Init the lazy ads on Search Results page
if ((0, _jquery2.default)(COMPONENT_LAZY_ASYNC_SELECTOR).length) {
    // On init, get the total amount of advertisement elements on the page.
    _advertisements2.default.lazyInitialize((0, _jquery2.default)(COMPONENT_LAZY_ASYNC_SELECTOR));

    (0, _jquery2.default)(document).on('resultsUpdated', function () {
        _advertisements2.default.lazyInitialize((0, _jquery2.default)(COMPONENT_LAZY_ASYNC_SELECTOR));
    });
}

// END: Init the lazy ads on Search Results page

},{"../advertisements/advertisements":196,"jquery":"jquery"}],196:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

require('./dfp-events');

var Advertisements = {};

exports.default = Advertisements;

// component configuration

var COMPONENT_ATTR = 'data-advertisement';
var COMPONENT_SELECTOR = '[' + COMPONENT_ATTR + ']';

var LAZY_INIT_SELECTOR = '[data-advertisement-lazy]';
var REFRESH_ONSCROLL_SELECTOR = '[data-advertisement-refresh-on-scroll]';
var SIZE_MAPPING_ATTR = 'data-advertisement-size-mapping';
var SIZE_DEFAULT_ATTR = 'data-advertisement-size-default';
var ADUNIT_PATH_ATTR = 'data-advertisement-adunit-path';
var CATEGORIES_URL_ATTR = 'data-advertisement-categories-url';
var SEARCHRESULT_SELECTOR = '[data-advertisement-searchresults]';
var SEARCH_AD_SELECTOR = '.search-ad';
var SEARCH_AD_LOADED_CLASS = 'search-ad--loaded';

// consts for iOnScreen
var BP_MEDIUM = 750;
var WINDOW_WIDTH = (0, _jquery2.default)(window).width();
var NO_DESKTOP = WINDOW_WIDTH < BP_MEDIUM - 1;
var LAZY_THRESHOLD = 'data-advertisement-lazy-threshold';
var LAZY_THRESHOLD_DEFAULT = 0;

Advertisements.instances = [];
Advertisements.cachedCategoriesByUrl = {};

/**
 * Default lazy ad initializer behaviour.
 * @param {array} elements - list of lazy ads.
 * @param {string} classFlag - classname of each ad on the first load.
 */
Advertisements.initialize = function ($elements, classFlag) {
    var component = this;
    window.googletag.cmd.push(function () {
        [].forEach.call($elements, function (element) {
            var config = Advertisements.getAdConfig(element);
            if (config !== false) {
                (0, _jquery2.default)(element).addClass('dfp-display-triggered');
                component.instances.push(element);
                element.id = config.id;
                component.createSlot(config);
                component.registerOnSlotRenderCallback(classFlag);
            }
        });
        window.googletag.pubads().collapseEmptyDivs();
        window.googletag.pubads().disableInitialLoad();
        window.googletag.enableServices();
    });
};

Advertisements.getAdConfig = function (element) {
    return {
        id: element.id || 'ad-' + Advertisements.instances.length,
        adunitpath: element.getAttribute(ADUNIT_PATH_ATTR),
        sizemapping: JSON.parse(element.getAttribute(SIZE_MAPPING_ATTR)),
        sizedefault: JSON.parse(element.getAttribute(SIZE_DEFAULT_ATTR)),
        categoriesUrl: element.getAttribute(CATEGORIES_URL_ATTR)
    };
};

Advertisements.createSlot = function (config) {
    var component = this;
    var cachedCategories = this.cachedCategoriesByUrl[config.categoriesUrl];

    if (cachedCategories) {
        component.pushAdSlot(config, cachedCategories);
    } else {
        _jquery2.default.get({
            url: config.categoriesUrl,
            success: function success(categories) {
                component.pushAdSlot(config, categories);
            },
            error: function error() {
                component.pushAdSlot(config, {});
            }
        });
    }
};

Advertisements.pushAdSlot = function (config, categories) {
    var tag = window.googletag.defineSlot(config.adunitpath, config.sizedefault, config.id).defineSizeMapping(config.sizemapping).setCollapseEmptyDiv(true, true).addService(window.googletag.pubads());

    for (var key in categories) {
        if (categories.hasOwnProperty(key)) {
            tag.setTargeting(key, categories[key]);
        }
    }

    window.googletag.display(config.id);
    window.googletag.pubads().refresh([tag]);
};

/**
 * Add a class to rendered ads - remove as soon as 100% released.
 * @param classname {string} - class name to add
 */
Advertisements.registerOnSlotRenderCallback = function (classname) {
    var isClassname = classname || '';

    window.googletag.cmd.push(function () {
        window.googletag.on('gpt-slot_rendered', function (e, level, message, service, slot) {
            var slotId = slot.getSlotId();
            var $slot = (0, _jquery2.default)('#' + slotId.getDomId());
            var $injectedFromGoogle = $slot.find('div[id^=google_ads_iframe]');

            // DFP adds two iframes, one for calling scripts and one for displaying the ad. we want the one that is not hidden
            Advertisements.addClassNameIfCreativeLoaded($slot, isClassname, $injectedFromGoogle.length > 0);
        });
    });
};

Advertisements.addClassNameIfCreativeLoaded = function ($slot, classname, isAdInjectedFromGoogle) {
    if (isAdInjectedFromGoogle) {
        $slot.addClass('advertisement dfp-callback-successful slot-' + classname);
        // Needed to avoid jumping effect on search results
        if ((0, _jquery2.default)(SEARCH_AD_SELECTOR).length) {
            $slot.parents(SEARCH_AD_SELECTOR).addClass(SEARCH_AD_LOADED_CLASS);
        }
        $slot.find('iframe').contents().find('body').addClass('body-' + classname);
    }
};

/**
 * Detect if an element is on the screen.
 * @param {object} element
 * @returns {boolean} if object is on screen
 */
Advertisements.isOnScreen = function (element) {
    var reservedDistance = Advertisements.calculateReservedDistance(element);
    var rect = element.getBoundingClientRect();

    return (0, _jquery2.default)(element).is(':visible') && rect.top >= 0 && rect.left >= 0 && rect.bottom <= (0, _jquery2.default)(window).height() + reservedDistance && rect.right <= WINDOW_WIDTH;
};

/**
 * Figure out the layout on search, is there any element positioned absolute?
 * On search page, the element has ~100px difference
 * @param {object} element
 * @returns {boolean} if object is on screen
 */
Advertisements.calculateReservedDistance = function (element) {
    if ((0, _jquery2.default)(element).attr('data-advertisement-searchresults') !== undefined) {
        return 200;
    }
    return parseInt(element.getAttribute(LAZY_THRESHOLD) || LAZY_THRESHOLD_DEFAULT, 10);
};

/**
 * Check if there's any advertisement on the page
 */
if ((0, _jquery2.default)(COMPONENT_SELECTOR).length && WINDOW_WIDTH > BP_MEDIUM) {
    Advertisements.initialize((0, _jquery2.default)(COMPONENT_SELECTOR + ':not(' + LAZY_INIT_SELECTOR + ')' + ':not(' + REFRESH_ONSCROLL_SELECTOR + ')' + ':not(' + SEARCHRESULT_SELECTOR + ')'), 'funda slot-ad-desktop');
} else if ((0, _jquery2.default)(COMPONENT_SELECTOR).length && NO_DESKTOP) {
    Advertisements.initialize((0, _jquery2.default)(COMPONENT_SELECTOR + ':not(' + LAZY_INIT_SELECTOR + ')' + ':not(' + REFRESH_ONSCROLL_SELECTOR + ')' + ':not(' + SEARCHRESULT_SELECTOR + ')'), 'funda slot-ad-non-desktop');
}

},{"./dfp-events":197,"jquery":"jquery"}],197:[function(require,module,exports){
'use strict';

/*
    Copyright 2013 Michael Countis
    MIT License: http://opensource.org/licenses/MIT
*/

(function () {
    'use strict';

    window.googletag = window.googletag || {};
    window.googletag.cmd = window.googletag.cmd || [];
    googletag.cmd.push(function () {
        if (googletag.hasOwnProperty('on') || googletag.hasOwnProperty('off') || googletag.hasOwnProperty('trigger') || googletag.hasOwnProperty('events')) {
            return;
        }
        var OLD_LOG = googletag.debug_log.log;
        var events = [];
        var addEvent = function addEvent(name, id, match) {
            events.push({ 'name': name, 'id': id, 'match': match });
        };

        /*eslint-disable no-multi-spaces */
        addEvent('gpt-google_js_loaded', 8, /Google service JS loaded/ig);
        addEvent('gpt-gpt_fetch', 46, /Fetching GPT implementation/ig);
        addEvent('gpt-gpt_fetched', 48, /GPT implementation fetched\./ig);
        addEvent('gpt-page_load_complete', 1, /Page load complete/ig);
        addEvent('gpt-queue_start', 31, /^Invoked queued function/ig);
        addEvent('gpt-service_add_slot', 40, /Associated ([\w]*) service with slot ([\/\w]*)/ig);
        addEvent('gpt-service_add_targeting', 88, /Setting targeting attribute ([\w]*) with value ([\w\W]*) for service ([\w]*)/ig);
        addEvent('gpt-service_collapse_containers_enable', 78, /Enabling collapsing of containers when there is no ad content/ig);
        addEvent('gpt-service_create', 35, /Created service: ([\w]*)/ig);
        addEvent('gpt-service_single_request_mode_enable', 63, /Using single request mode to fetch ads/ig);
        addEvent('gpt-slot_create', 2, /Created slot: ([\/\w]*)/ig);
        addEvent('gpt-slot_add_targeting', 17, /Setting targeting attribute ([\w]*) with value ([\w\W]*) for slot ([\/\w]*)/ig);
        addEvent('gpt-slot_fill', 50, /Calling fillslot/ig);
        addEvent('gpt-slot_fetch', 3, /Fetching ad for slot ([\/\w]*)/ig);
        addEvent('gpt-slot_receiving', 4, /Receiving ad for slot ([\/\w]*)/ig);
        addEvent('gpt-slot_render_delay', 53, /Delaying rendering of ad slot ([\/\w]*) pending loading of the GPT implementation/ig);
        addEvent('gpt-slot_rendering', 5, /^Rendering ad for slot ([\/\w]*)/ig);
        addEvent('gpt-slot_rendered', 6, /Completed rendering ad for slot ([\/\w]*)/ig);
        /*eslint-enable no-multi-spaces */

        googletag.events = googletag.events || {};
        googletag.on = function (onEvents, OP_ARG0 /*data*/, OP_ARG1 /*callback*/) {
            if (!OP_ARG0) {
                return this;
            }
            onEvents = onEvents.split(' ');
            var data = OP_ARG1 ? OP_ARG0 : undefined;
            var callback = OP_ARG1 || OP_ARG0;
            var ei = 0;
            var e = '';

            callback.data = data;

            for (e = onEvents[ei = 0]; ei < onEvents.length; e = onEvents[++ei]) {
                (this.events[e] = this.events[e] || []).push(callback);
            }

            return this;
        };
        googletag.off = function (offEvents, handler) {
            offEvents = offEvents.split(' ');
            var ei = 0;
            var e = '';
            var fi = 0;
            var f = function f() {};

            for (e = offEvents[ei]; ei < offEvents.length; e = offEvents[++ei]) {
                if (!this.events.hasOwnProperty(e)) {
                    continue;
                }

                if (!handler) {
                    delete this.events[e];
                    continue;
                }

                fi = this.events[e].length - 1;
                for (f = this.events[e][fi]; fi >= 0; f = this.events[e][--fi]) {
                    if (f == handler) {
                        this.events[e].splice(fi, 1);
                    }
                }
                if (this.events[e].length === 0) {
                    delete this.events[e];
                }
            }

            return this;
        };
        googletag.trigger = function (event) {
            var parameters = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];

            if (!this.events[event] || this.events[event].length === 0) {
                return this;
            }

            var fi = 0;
            var f = this.events[event][fi];

            for (fi, f; fi < this.events[event].length; f = this.events[event][++fi]) {
                if (f.apply(this, [{ data: f.data }].concat(parameters)) === false) {
                    break;
                }
            }

            return this;
        };
        googletag.debug_log.log = function (level, message) {
            if (message && message.getMessageId && typeof message.getMessageId() == 'number') {
                var args = Array.prototype.slice.call(arguments);
                var e = 0;
                for (e; e < events.length; e++) {
                    if (events[e].id === message.getMessageId()) {
                        googletag.trigger(events[e].name, args);
                    }
                }
            }
            return OLD_LOG.apply(this, arguments);
        };
    });
})();

},{}],198:[function(require,module,exports){
// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

// explicitly inject dependencies (alphabetically), only those needed

var $ = require('jquery');
var events = $({});

// what does this module expose?
module.exports = ajax;

/**
 * Wrap jQuery ajax call and decorate with global success and error event emitters.
 * Other modules can listen to these events using `ajax.onError` and `ajax.onSuccess`.
 * @returns {*}
 */
function ajax() /* arguments */{
    return $.ajax.apply(this, arguments).done(function (data) {
        return events.trigger('success', data);
    }).fail(function (err) {
        if (err.statusText === 'abort') {
            events.trigger('abort', err);
        } else {
            events.trigger('error', err);
        }
    });
}

ajax.onAbort = function (listener) {
    return events.on('abort', listener);
};
ajax.onError = function (listener) {
    return events.on('error', listener);
};
ajax.onSuccess = function (listener) {
    return events.on('success', listener);
};

},{"jquery":"jquery"}],199:[function(require,module,exports){
'use strict';

// dependencies (alphabetically)

require('./../expandible/expandible');

},{"./../expandible/expandible":229}],200:[function(require,module,exports){
/* global gtmDataLayer */

'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

var _mousetrap = require('mousetrap');

var _mousetrap2 = _interopRequireDefault(_mousetrap);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Shake = require('shake.js');

exports.default = AppHotkeys;


var COMPONENT_SELECTOR = '[data-app-hotkeys]';
var VERRASME_URL = '/verras-me/een-huis';
var VIBRATE_PATTERN = [300, 100, 100];
var MEDIA_VIEWER_PHOTOS_SELECTOR = '.media-viewer .media-viewer-view.media-viewer-fotos';
var MEDIA_VIEWER_OPEN_PHOTO_HASH = '#foto-1';

function AppHotkeys() {
    var component = this;

    component.registerHotkeys();

    if ('ondevicemotion' in window) {
        component.registerShakeListener();
    }
}

AppHotkeys.prototype.registerHotkeys = function () {
    _mousetrap2.default.bind('v', this.navigateToVerrasMe);
    _mousetrap2.default.bind('f', this.openMediaViewerIfPhotos);
};

AppHotkeys.prototype.registerShakeListener = function () {
    var _this = this;

    var shakeEvent = new Shake({
        threshold: 15, // optional shake strength threshold
        timeout: 1000 // optional, determines the frequency of event generation
    });
    var supportsVibrate = 'vibrate' in navigator;

    window.addEventListener('shake', function () {
        if (supportsVibrate) {
            navigator.vibrate(VIBRATE_PATTERN);
        }
        _this.navigateToVerrasMe();
    }, false);

    shakeEvent.start();
};

AppHotkeys.prototype.navigateToVerrasMe = function () {
    var lang = document.querySelector('html').getAttribute('lang');

    window.location = lang != 'nl' ? '/' + lang + VERRASME_URL : VERRASME_URL;

    if (window.gtmDataLayer !== undefined) {
        gtmDataLayer.push({
            'event': 'userInteractionVerrasMe'
        });
    }
};

AppHotkeys.prototype.openMediaViewerIfPhotos = function () {
    var hasPhotos = document.querySelector(MEDIA_VIEWER_PHOTOS_SELECTOR);

    if (hasPhotos && window.location.hash === '') {
        window.location.hash = MEDIA_VIEWER_OPEN_PHOTO_HASH;
    }
};

// turn all elements with the default selector into components
(0, _jquery2.default)(COMPONENT_SELECTOR).each(function () {
    return new AppHotkeys();
});

},{"jquery":"jquery","mousetrap":"mousetrap","shake.js":"shake.js"}],201:[function(require,module,exports){
// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

// explicitly inject dependencies (alphabetically), only those needed

var $ = require('jquery');

// what does this module expose?
module.exports = AppSpinner;

// component configuration
var KEYFRAME_ANIMATION_SUPPORTED_CLASS = 'keyframe-animation-supported';
var VISIBLE_CLASS = 'is-visible';
var VISIBLE_DELAY = 1500;

function AppSpinner(element) {
    var component = this;

    component.$element = $(element);

    if (browserSupportsKeyframeAnimations()) {
        component.$element.addClass(KEYFRAME_ANIMATION_SUPPORTED_CLASS);
    }
}

function browserSupportsKeyframeAnimations() {
    // https://hacks.mozilla.org/2011/09/detecting-and-generating-css-animations-in-javascript/
    var supportsAnimation = false;
    var domPrefixes = ['Webkit', 'Moz', 'O', 'ms', 'Khtml'];
    var element = document.createElement('div');

    if (element.style.animationName !== undefined) {
        supportsAnimation = true;
    }

    if (supportsAnimation === false) {
        for (var i = 0; i < domPrefixes.length; i++) {
            if (element.style[domPrefixes[i] + 'AnimationName'] !== undefined) {
                supportsAnimation = true;
                break;
            }
        }
    }
    return supportsAnimation;
}

AppSpinner.prototype.show = function () {
    var component = this;

    component.spinnerDelay = window.setTimeout(function () {
        component.$element.addClass(VISIBLE_CLASS);
    }, VISIBLE_DELAY);
    return $.when();
};

AppSpinner.prototype.hide = function () {
    var component = this;

    window.clearTimeout(component.spinnerDelay);
    component.$element.removeClass(VISIBLE_CLASS);
    return $.when();
};

AppSpinner.prototype.isVisible = function () {
    var component = this;
    return component.$element.hasClass(VISIBLE_CLASS);
};

},{"jquery":"jquery"}],202:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = ArrayExtensions;


function ArrayExtensions(arrayList) {
    return _jquery2.default.extend(this, arrayList);
}

ArrayExtensions.prototype.getIndexBy = function (functionToMatch) {
    var array = this;

    for (var i = 0; i < array.length; i++) {
        if (functionToMatch(array[i])) {
            return i;
        }
    }
    return -1;
};

ArrayExtensions.prototype.getNextIndexCircular = function (index) {
    var array = this;

    if (index < 0) {
        return -1;
    }

    return (index + 1) % array.length;
};

},{"jquery":"jquery"}],203:[function(require,module,exports){
'use strict';

/**
 * Cookie utility. The code is based on http://www.w3schools.com/js/js_cookies.asp
 */

Object.defineProperty(exports, "__esModule", {
    value: true
});

exports.default = function () {
    var publics = {};

    /**
     * Cookie setter.
     *
     * @param cookieName  Cookie name.
     * @param cookieValue Cookie value.
     * @param expirationDays (optional) Number of days to expire the cookie. Default is 365 days.
     * @param path. (optional) Cookie path. Default is root '/'.
     */
    publics.setCookie = function (cookieName, cookieValue, expirationDays, path) {
        if (typeof expirationDays === 'undefined') {
            expirationDays = 365;
        }
        if (typeof path === 'undefined') {
            path = '/';
        }
        var expirationDate = new Date();
        expirationDate.setTime(expirationDate.getTime() + expirationDays * 86400000); //24*60*60*1000=86400000
        document.cookie = cookieName + '=' + cookieValue + ';expires=' + expirationDate.toUTCString() + ';path=' + path;
    };

    /**
     * Cookie getter.
     *
     * @param name Cookie name.
     * @param defaultValue (optional) If cookie is not found, returns this value. If not defined, returns undefined.
     * @returns {*} Cookie value or default value.
     */
    publics.getCookie = function (cookieName, defaultValue) {
        var cookies = decodeURIComponent(document.cookie).split(';');
        var name = cookieName + '=';
        for (var i = 0; i < cookies.length; i++) {
            var cookie = cookies[i];
            while (cookie.charAt(0) == ' ') {
                cookie = cookie.substring(1);
            }
            if (cookie.indexOf(name) == 0) {
                return cookie.substring(name.length, cookie.length);
            }
        }
        return defaultValue;
    };

    return publics;
}();

},{}],204:[function(require,module,exports){
// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

// explicitly inject dependencies (alphabetically), only those needed

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

require('../filter-count/filter-count');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

require('../filter-reset/filter-reset');

// what does this module expose?
exports.default = AppliedFilters;

// component configuration

var COMPONENT_SELECTOR = '[data-applied-filters]';
var APPLIED_FILTERS_SELECTOR = '[data-applied-filter-list]';
var APPLIED_FILTER_TEMPLATE = '[data-applied-filter-template]';
var REMOVE_APPLIED_FILTER_ATTR = 'data-remove-applied-filter';
var REMOVE_APPLIED_FILTER_SELECTOR = '[' + REMOVE_APPLIED_FILTER_ATTR + ']';
var APPLIED_FILTER_GROUP_ATTR = 'data-applied-filter-group-id';
var APPLIED_FILTER_GROUPNAME_ATTR = 'data-applied-filter-group-name';
var VISIBLE_CLASS = 'is-visible';
var CLOSING_CLASS = 'is-closing';

function AppliedFilters(element) {
    var component = this;

    component.bindToElements(element);

    (0, _jquery2.default)(window).on('sidebar-refresh', function () {
        var $myNewElement = (0, _jquery2.default)(COMPONENT_SELECTOR);
        if ($myNewElement.length == 1) {
            component.bindToElements($myNewElement);
        }
    });
}

AppliedFilters.prototype.bindToElements = function (element) {
    var component = this;

    component.$element = (0, _jquery2.default)(element);
    component.$items = component.$element.find(APPLIED_FILTERS_SELECTOR);

    component.$element.on('update', function () {
        return component.updateState();
    });
    component.$element.on('resetfilter', function () {
        component.removeAll();
    });
};

/**
 * on reset filters all applied filters will be removed
 */
AppliedFilters.prototype.removeAll = function () {
    var component = this;
    component.$items.html('');
    component.updateState();
};

/**
 * toggle view state of component
 */
AppliedFilters.prototype.updateState = function () {
    var component = this;
    var numberOfActiveFilters = component.$items.find(REMOVE_APPLIED_FILTER_SELECTOR).length;

    (0, _jquery2.default)(document).trigger('setfiltercount', { amount: numberOfActiveFilters });

    if (numberOfActiveFilters > 0) {
        component.$element.removeClass(CLOSING_CLASS).addClass(VISIBLE_CLASS);
    } else {
        component.$element.removeClass(VISIBLE_CLASS).addClass(CLOSING_CLASS);
    }
};

/**
 * get the html template and fill it with data from the selected filter
 * @param {Obj} filterData; selected data from filter
 * @returns {*|jQuery}
 */
AppliedFilters.getButton = function (filterData) {
    var html = (0, _jquery2.default)(APPLIED_FILTER_TEMPLATE).html();
    var findPattern = null;

    if (html === undefined) {
        return; // the template doesn't exist
    }

    var filterAttributes = {
        'id': { find: '{filterGroupId}', replace: filterData.filterGroupId },
        'group': { find: '{filterGroupName}', replace: filterData.filterGroupName },
        'option': { find: '{filterName}', replace: filterData.filterName },
        'label': { find: '{combinedFilterName}', replace: filterData.labelText }
    };

    for (var filter in filterAttributes) {
        if (filterAttributes[filter].replace !== undefined) {
            findPattern = new RegExp('(' + filterAttributes[filter].find + ')', 'gm');
            html = html.replace(findPattern, filterAttributes[filter].replace);
        }
    }
    return html;
};

/**
 * add a new filter by getting template/html and returns a filled template
 * @param {Obj} filterData; selected data from filter
 * @param {Function} clearFunction
 * @param {Boolean} removeOldFilter
 * @returns {*|jQuery}
 */
AppliedFilters.add = function (filterData, clearFunction, removeOldFilter) {
    // from this group filters there maybe only one visible; let's remove the previous selected first
    if (removeOldFilter) {
        AppliedFilters.remove(filterData.filterGroupId);
    }
    var $button = AppliedFilters.getButton(filterData);
    // add new filter to applied filter list
    (0, _jquery2.default)(APPLIED_FILTERS_SELECTOR).append(AppliedFilters.applyClearBinding($button, clearFunction));
    (0, _jquery2.default)(COMPONENT_SELECTOR).trigger('update');
};

/**
 * remove filter by id or by group id
 * @param {string} optionId
 */
AppliedFilters.remove = function (optionId) {
    var $filters = (0, _jquery2.default)(APPLIED_FILTERS_SELECTOR + ' [' + APPLIED_FILTER_GROUP_ATTR + '="' + optionId + '"], ' + APPLIED_FILTERS_SELECTOR + ' [' + APPLIED_FILTER_GROUPNAME_ATTR + '="' + optionId + '"]');
    var numberOfActiveFilters = $filters.length;

    if (numberOfActiveFilters > 0) {
        $filters.detach();
        (0, _jquery2.default)(COMPONENT_SELECTOR).trigger('update');
    }
};

/**
 * Applies the clear filter binding to the html
 * @param {html} htmlButton; the button with selected filter
 * @param {function} clearFunction
 * @returns {*|jQuery}
 */
AppliedFilters.applyClearBinding = function (htmlButton, clearFunction) {
    var $button = (0, _jquery2.default)(htmlButton).on('click', function (event) {
        event.preventDefault();
        // run given action from component to clear the selection
        clearFunction.apply();
        return false;
    });

    return $button;
};

// turn all elements with the default selector into components
(0, _jquery2.default)(COMPONENT_SELECTOR).each(function (index, element) {
    return new AppliedFilters(element);
});

},{"../filter-count/filter-count":231,"../filter-reset/filter-reset":234,"jquery":"jquery"}],205:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = FormHidden;


var COMPONENT_SELECTOR = '[data-form-hidden]';

var EDIT_TRIGGER = '[data-form-hidden-edit-trigger]';
var READ_TRIGGER = '[data-form-hidden-read-trigger]';
var EDIT_CONTAINER = '[data-form-hidden-edit-container]';
var READ_CONTAINER = '[data-form-hidden-read-container]';

function FormHidden(element) {
    var component = this;

    component.$element = (0, _jquery2.default)(element);
    component.editContainer = component.$element.find(EDIT_CONTAINER);
    component.readContainer = component.$element.find(READ_CONTAINER);
    component.editTrigger = component.$element.find(EDIT_TRIGGER);
    component.readTrigger = component.$element.find(READ_TRIGGER);

    component.disableFallbackLinks();
    component.bindEvents();
}

FormHidden.prototype.disableFallbackLinks = function () {
    var component = this;

    component.readTrigger.removeAttr('href');
    component.editTrigger.removeAttr('href');
};

FormHidden.prototype.bindEvents = function () {
    var component = this;

    component.editTrigger.click(function () {
        return component.toggle();
    });
    component.readTrigger.click(function () {
        return component.toggle();
    });
    component.$element.on('submit-success', function () {
        return component.toggle();
    });
};

FormHidden.prototype.toggle = function () {
    var component = this;

    component.editTrigger.toggle();
    component.readContainer.toggle();
    component.editContainer.toggle();
};

(0, _jquery2.default)(COMPONENT_SELECTOR).each(function (index, element) {
    return new FormHidden(element, index);
});

},{"jquery":"jquery"}],206:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = FormWithNotifications;


var COMPONENT_SELECTOR = '[data-form-with-notifications]';
var CANCEL_SELECTOR = '[data-form-with-notifications-cancel]';
var SUCCESS_NOTIFICATION_SELECTOR = '[data-notification="successNotification"]';
var ERROR_NOTIFICATION_SELECTOR = '[data-notification="errorNotification"]';
var READ_FIELD_DATA_ATTRIBUTE = 'data-read';
var READ_FIELD_SELECTOR = '[' + READ_FIELD_DATA_ATTRIBUTE + ']';

function FormWithNotifications(element) {
    var component = this;

    component.$element = (0, _jquery2.default)(element);
    component.$form = component.$element.find('form');
    component.$cancel = component.$element.find(CANCEL_SELECTOR);
    component.$successNotification = component.$element.find(SUCCESS_NOTIFICATION_SELECTOR);
    component.$errorNotification = component.$element.find(ERROR_NOTIFICATION_SELECTOR);
    component.$readFields = component.$element.find(READ_FIELD_SELECTOR);
    component.lastValidData = component.getInitialFormValues();

    component.bindEvents();
}

FormWithNotifications.prototype.bindEvents = function () {
    var component = this;

    component.$form.unbind('submit').bind('submit', function (event) {
        return component.submitAsync(event);
    });
    component.$cancel.click(function () {
        return component.cancel();
    });
};

FormWithNotifications.prototype.submitAsync = function (event) {
    var component = this;

    var url = component.$form.attr('action');
    var data = component.$form.serialize();

    if (typeof event !== 'undefined' && typeof event.preventDefault === 'function') {
        event.preventDefault();
    }

    _jquery2.default.post(url, data).done(function (message) {
        return component.submitSuccessHandler(message);
    }).fail(function (message) {
        return component.submitErrorHandler(message);
    });
};

FormWithNotifications.prototype.submitErrorHandler = function (message) {
    var component = this;

    component.$errorNotification.trigger('show', message);
    component.$element.trigger('submit-error', message);
};

FormWithNotifications.prototype.submitSuccessHandler = function (message) {
    var component = this;

    if (typeof message !== 'undefined' && message.success === true) {
        component.lastValidData = message.data;
        component.restoreEditValues();
        component.updateReadContent(message.data);
        component.$successNotification.trigger('show', message.NotificationMessage);
        component.$errorNotification.trigger('hide');
        component.$element.trigger('submit-success', message.data);
    } else {
        var errorMessage = (typeof message === 'undefined' ? 'undefined' : _typeof(message)) === 'object' ? message.NotificationMessage : message;
        component.$successNotification.trigger('hide');
        component.$errorNotification.trigger('show', errorMessage);
        component.$element.trigger('submit-error', errorMessage);
    }
};

FormWithNotifications.prototype.updateReadContent = function (data) {
    var component = this;

    component.$readFields.each(function (i, inputField) {
        var key = inputField.getAttribute(READ_FIELD_DATA_ATTRIBUTE);
        if (typeof data !== 'undefined' && data.hasOwnProperty(key)) {
            inputField.innerHTML = data[key];
        }
    });
};

FormWithNotifications.prototype.cancel = function () {
    var component = this;

    component.restoreEditValues();
    component.$successNotification.trigger('hide');
    component.$errorNotification.trigger('hide');
};

FormWithNotifications.prototype.getInitialFormValues = function () {
    var component = this;

    var queryData = component.$form.serialize();
    return JSON.parse('{"' + decodeURI(queryData).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g, '":"') + '"}');
};

FormWithNotifications.prototype.restoreEditValues = function () {
    var component = this;

    for (var key in component.lastValidData) {
        if (component.lastValidData.hasOwnProperty(key)) {
            var initialValue = component.lastValidData[key];
            var $inputField = component.$form.find('[name=' + key + ']');

            if ($inputField.length > 0) {
                if ($inputField.attr('type') === 'radio') {
                    $inputField.filter('[value="' + initialValue + '"]').prop('checked', true);
                } else {
                    $inputField.val(initialValue);
                }
            }
        }
    }
};

(0, _jquery2.default)(COMPONENT_SELECTOR).each(function (i, element) {
    return new FormWithNotifications(element);
});

},{"jquery":"jquery"}],207:[function(require,module,exports){
'use strict';

var $ = require('jquery');
var extend = require('lodash/extend');
var isFunction = require('lodash/isFunction');

module.exports = AsyncForm;

// component configuration
var AJAX_SUBMIT_SELECTOR = '[dialog-ajax-submit]';
var OPTIONS = {
    body: null,
    success: null,
    error: null,
    submitCallback: null
};

function AsyncForm($element, options) {
    var component = this;
    component.$element = $element.find('form');
    component.options = extend(OPTIONS, options);

    if (component.$element.length === 0) {
        return;
    }
    component.hookForm();
}

AsyncForm.prototype.hookForm = function () {
    var component = this;
    var url = component.$element.attr('action');

    // binding to specific form/action
    $('form[action="' + url + '"]').on('submit', function (event) {
        event.preventDefault();

        var submitButtons = AsyncForm.getSubmitButtons($(event.target));

        //disable all submit buttons
        for (var i = 0, length = submitButtons.length; i < length; i++) {
            $(submitButtons[i]).prop('disabled', true);
        }

        var data = $(this).find(':input').serialize();
        // post it
        $.post({
            url: url,
            data: data,
            cache: false,
            xhrFields: {
                withCredentials: true
            },
            dataType: 'json'
        }).then(function (response) {
            if (response.Result === 'OK') {

                if (response.View === null || typeof response.View === 'undefined') {
                    if (isFunction(component.options.success)) {
                        component.options.success();
                    }
                } else {
                    var $viewHtml = $(response.View);
                    component.options.body.html(response.View);
                    // bind social login
                    if (component.options.submitCallback) {
                        component.options.submitCallback(component.options.body);
                    }
                    // bind new instance to form
                    if (AsyncForm.wantsAjaxSubmit($viewHtml)) {
                        return new AsyncForm($viewHtml, {
                            success: component.options.success,
                            error: component.options.error
                        });
                    }
                }
            }
            // TODO: add handling of errors (eg. result = 'ERROR' and errorMessage != null)
        }).catch(function (reason) {
            console.log(reason); // TODO: add handling of errors (eg. result = 'ERROR' and errorMessage != null)

            //enable all submit buttons
            for (var _i = 0, _length = submitButtons.length; _i < _length; _i++) {
                $(submitButtons[_i]).prop('disabled', false);
            }
        });
    });
};

AsyncForm.wantsAjaxSubmit = function ($viewHtml) {
    var $ajaxSubmit = AsyncForm.getSubmitButtons($viewHtml);
    return $ajaxSubmit.length > 0;
};
AsyncForm.getSubmitButtons = function ($viewHtml) {
    return $viewHtml.find(AJAX_SUBMIT_SELECTOR);
};

},{"jquery":"jquery","lodash/extend":161,"lodash/isFunction":173}],208:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = loginAsync;


var LOGIN_DIALOG_SELECTOR = '[data-dialog="user-form"]';

var loginAsyncCacheIsLoggedIn = false;

/**
 * Performs an asyncronous login. The response can be handled either with a callback or a promise.
 * Once the user is logged in, the result gets cached so we don't perform more request than the scrictly required.
 * @param callback optional callback. Can cbe also handled with
 * @returns Deferred promise.
 */
function loginAsync(callback) {
    var result = _jquery2.default.Deferred(); // eslint-disable-line new-cap
    var dialog = document.querySelector(LOGIN_DIALOG_SELECTOR).dialog;

    if (loginAsyncCacheIsLoggedIn) {
        callback();
        result.resolve();
    } else if (typeof dialog !== 'undefined' && typeof dialog.isUserLoggedInUrl === 'string') {
        _jquery2.default.ajax(dialog.isUserLoggedInUrl).then(function (response) {
            if (typeof response === 'undefined') {
                result.reject('Bad response');
            } else if (response.LoggedIn === true) {
                loginAsyncCacheIsLoggedIn = true;
                callback();
                result.resolve();
            } else if (typeof response.LoginUrl === 'string') {
                dialog.onFormSuccess = function () {
                    window.location.reload();
                };
                dialog.open();
                dialog.getContent(response.LoginUrl);
            } else {
                result.reject('Bad response');
            }
        });
    } else {
        result.reject('Dialog component was not found.');
    }
    return result.promise();
}

},{"jquery":"jquery"}],209:[function(require,module,exports){
// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

// explicitly inject dependencies (alphabetically), only those needed

var _expandible = require('../expandible/expandible');

var _expandible2 = _interopRequireDefault(_expandible);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var $ = require('jquery');
var ObjectRatingForm = require('../object-rating-form/object-rating-form');
var Rating = require('../rating/rating');
var LoginDialog = require('../login-dialog/login-dialog');

var AppSpinner = require('../app-spinner/app-spinner');

module.exports = AsyncObjectRating;

// component configuration
var COMPONENT_ATTR = 'data-async-object-rating';
var COMPONENT_SELECTOR = '[' + COMPONENT_ATTR + ']';
var HANDLE_SELECTOR = '[data-async-object-rating-handle]';
var HANDLE_TEXT_ATTR = 'data-async-object-rating-handle-text';
var HANDLE_TEXT_SELECTOR = '[' + HANDLE_TEXT_ATTR + ']';
var DEFAULT_INPUT_SELECTOR = '[data-default-rating-input]';
var RESET_HANDLE_SELECTOR = '[data-object-rating-form-reset-handle]';
var CONTENT_SELECTOR = '[data-async-object-rating-content]';
var FORM_SELECTOR = '[data-object-rating-form]';
var RATING_SELECTOR = '[data-rating]';
var TEXTAREA_SELECTOR = '[data-object-rating-textarea]';
var SUBMIT_BUTTON_SELECTOR = '[data-object-rating-submit-button]';
var ERROR_MESSAGE_SELECTOR = '[data-overall-object-rating-error-message]';
var OVERALL_RATING_SELECTOR = '[data-overall-object-rating-form]';
var SAVE_OBJECT_SELECTOR = '[data-save-object]';
var LOGIN_DIALOG_SELECTOR = '[data-dialog="user-form"]';
var SPINNER_SELECTOR = '[data-async-object-rating-spinner]';
var RATED_CLASS = 'is-rated';
var LOADED_CLASS = 'is-loaded';
var EXPANDED_CLASS = 'is-expanded';
var VISIBLE_CLASS = 'is-visible';
var DOING_REQUEST_CLASS = 'is-doing-request';
var SCROLL_ATTR = 'data-expandible-scroll';
var CLOSE_HANDLE_SELECTOR = '[data-expandible-handle-scroll]';

var instances = {};

function AsyncObjectRating(element) {
    var component = this;
    component.$element = $(element);
    component.$handle = component.$element.find(HANDLE_SELECTOR);
    component.$closeHandle = component.$element.find(CLOSE_HANDLE_SELECTOR);
    component.$overallRating = component.$element.find(OVERALL_RATING_SELECTOR);
    component.$overallRatingInput = component.$overallRating.find('input');
    component.$spinner = component.$element.find(SPINNER_SELECTOR);
    component.doPageRefresh = false;
    component.isExpanded = component.$element.hasClass(EXPANDED_CLASS);
    instances[component.$element.attr(COMPONENT_ATTR)] = component;
    var $saveObject = component.$element.find($(SAVE_OBJECT_SELECTOR));
    component.spinner = new AppSpinner(component.$spinner[0]);
    component.scrollPosition = 0;

    component.$handle.on('click', function (event) {
        event.preventDefault();
        var objectRatingFormExists = component.$element.find(FORM_SELECTOR).length;
        component.isExpanded = component.$element.hasClass(EXPANDED_CLASS);

        if (!component.isExpanded && !objectRatingFormExists) {
            component.isUserLoggedIn(function onSuccessfulLogin() {
                component.getForm(component.$handle);
            });
        }

        if (component.$element[0].hasAttribute(SCROLL_ATTR)) {
            component.scrollPosition = $(window).scrollTop();
        }
    });

    component.$element.on('click', SUBMIT_BUTTON_SELECTOR, function (event) {
        var checkedOverallInputValue = component.$overallRating.find('input:checked').val();
        event.preventDefault();
        component.toggleRatedState();

        if (component.scrollPosition > 0 && checkedOverallInputValue !== '0') {
            component.scrollToObjectPosition();
        }
    });

    component.$element.on('click', RESET_HANDLE_SELECTOR, function (event) {
        event.preventDefault();
        component.resetObjectRating();

        if (component.scrollPosition > 0) {
            component.scrollToObjectPosition();
        }
    });

    component.$closeHandle.on('click', function (event) {
        event.preventDefault();

        if (component.scrollPosition > 0) {
            component.scrollToObjectPosition();
        }
    });

    component.$overallRatingInput.on('change', function () {
        component.submitOverallRating();
    });

    $saveObject.on('objectsaved', function (event, isSaved) {
        var objectRatingFormExists = component.$element.find(FORM_SELECTOR).length;
        var $handleText = component.$element.find(HANDLE_TEXT_SELECTOR);

        if (objectRatingFormExists) {
            if (isSaved === true) {
                component.getForm(component.$handle);
            } else {
                component.$element.find(TEXTAREA_SELECTOR).val('').html('');
                component.expandible.toggleExpand(false);
                $handleText.text($handleText.attr(HANDLE_TEXT_ATTR));
                component.resetRatingStyling();
            }
        }
    });
}

AsyncObjectRating.prototype.isUserLoggedIn = function (onLoggedIn) {
    var component = this;
    var dialogElement = document.querySelector(LOGIN_DIALOG_SELECTOR);
    var url = dialogElement.dialog.isUserLoggedInUrl;
    component.expandible = new _expandible2.default(component.$element[0]);
    component.spinner = new AppSpinner(component.$spinner[0]);
    component.spinner.show();

    return $.ajax({
        url: url,
        success: function success(response) {
            if (response.LoggedIn === true) {
                onLoggedIn();
            } else {
                component.userLoginStatus = new LoginDialog(response.LoginUrl, function onSuccessfulLogin() {
                    window.location.reload();
                });
                component.expandible.toggleExpand(false);
                component.spinner.hide();
            }
        },
        error: function error(response) {
            console.error('Error calling', url, response);
        }
    });
};

/**
 * Does an asynchronous request to get the form content and appends
 * it to the DOM if the request is successful
 */
AsyncObjectRating.prototype.getForm = function ($element) {
    var component = this;
    var url = $element.attr('href');
    component.$element.removeClass(LOADED_CLASS);

    return $.ajax({
        url: url,
        success: function success(response) {
            if (AsyncObjectRating.isSuccesfulResponse(response)) {
                var $content = component.$element.find(CONTENT_SELECTOR);
                $content.html('');
                $content.append(response.Html);
                component.createInstances();
            }
        },
        error: onError,
        complete: function complete() {
            component.spinner.hide();
            component.$element.addClass(LOADED_CLASS);
        }
    });

    function onError(response) {
        console.error('Error trying to rate object using', url, response);
    }
};

AsyncObjectRating.isSuccesfulResponse = function (response) {
    return response.Result === 'OK';
};

/**
 * Creates a new instance of all components if they're not instantiated yet
 */
AsyncObjectRating.prototype.createInstances = function () {
    var component = this;

    component.objectRatingForm = new ObjectRatingForm(FORM_SELECTOR);
    component.$element.find(RATING_SELECTOR).each(function (index, element) {
        return new Rating(element);
    });
    component.expandible = new _expandible2.default(component.$element[0]);
};

/**
 * Updates the text inside the handle with the value of the textarea
 */
AsyncObjectRating.prototype.updateHandleText = function () {
    var component = this;
    var note = component.$element.find(TEXTAREA_SELECTOR).val();
    var maxCharacters = 150;
    var $handleText = component.$element.find(HANDLE_TEXT_SELECTOR);

    if (note) {
        $handleText.text(note.substring(0, maxCharacters));
    } else {
        $handleText.text($handleText.attr(HANDLE_TEXT_ATTR));
    }
};

/**
 * Does an ajax request and adds the 'is-rated' class if the response is
 * successful
 */
AsyncObjectRating.prototype.toggleRatedState = function () {
    var component = this;
    component.$form = component.$element.find(FORM_SELECTOR);
    var url = component.$form.attr('action');
    var data = component.$form.find(':input').serialize();
    component.spinner.show();
    component.$element.addClass(DOING_REQUEST_CLASS);

    return $.ajax({
        method: 'POST',
        url: url,
        data: data,
        success: function onSuccess(response) {
            if (AsyncObjectRating.isSuccesfulResponse(response)) {
                component.updateHandleText();
                component.$handle.addClass(RATED_CLASS);
                component.$element.addClass(RATED_CLASS);
                component.$element.trigger('objectrated');
                component.expandible.toggleExpand(!component.expandible.isExpanded);
                component.getForm(component.$handle);
            } else if (response.Result === 'ERROR') {
                var $element = component.$element.find(CONTENT_SELECTOR);
                $element.html('');
                $element.append(response.Html);
                component.createInstances();
            }
        },
        error: function onError(error) {
            console.error('Error trying to submit form', error);
            window.location.href = url;
        },
        complete: function complete() {
            if (component.doPageRefresh === true) {
                window.location.reload();
            }
            component.spinner.hide();
            component.$element.removeClass(DOING_REQUEST_CLASS);
        }
    });
};

/**
 * Does an asynchronous request to post the overall object rating
 */
AsyncObjectRating.prototype.submitOverallRating = function () {
    var component = this;
    var url = component.$overallRating.attr('action');
    var data = component.$overallRating.serialize();

    return $.ajax({
        method: 'POST',
        url: url,
        data: data,
        error: function onError(error) {
            console.error('Error trying to submit overall rating form', error);
        }
    });
};

/**
 * Resets, clears and closes the async object rating form, calls
 * resetRatingStyling() for resetting the rating styling
 */
AsyncObjectRating.prototype.resetObjectRating = function () {
    var component = this;
    var $defaultInputs = component.$element.find(DEFAULT_INPUT_SELECTOR);

    // set all ratings to the default input (which is the hidden input
    // with value=0)
    $defaultInputs.each(function (index, element) {
        $(element).prop('checked', true);
    });

    component.resetRatingStyling();
    component.$element.find(TEXTAREA_SELECTOR).val('');
    component.updateHandleText();
    $(ERROR_MESSAGE_SELECTOR).removeClass(VISIBLE_CLASS);
    // clear the form
    component.$element.find(CONTENT_SELECTOR).html('');
    component.expandible.toggleExpand(!component.expandible.isExpanded);
};

/**
 * Resets all ratings to the default value and removes the 'is-rated' class
 * from all ratings
 */
AsyncObjectRating.prototype.resetRatingStyling = function () {
    var component = this;

    component.$overallRatingInput.removeClass(RATED_CLASS);
    component.$handle.removeClass(RATED_CLASS);
    component.$element.removeClass(RATED_CLASS);
    component.objectRatingForm.$inputs.removeClass(RATED_CLASS);
};

AsyncObjectRating.getInstances = function () {
    return instances;
};

AsyncObjectRating.prototype.scrollToObjectPosition = function () {
    var component = this;

    $(window).scrollTop(component.scrollPosition);
};

AsyncObjectRating.initialize = function () {
    // turn all elements with the default selector into components
    $(COMPONENT_SELECTOR).each(function (index, element) {
        return new AsyncObjectRating(element);
    });
};

AsyncObjectRating.initialize();

},{"../app-spinner/app-spinner":201,"../expandible/expandible":229,"../login-dialog/login-dialog":249,"../object-rating-form/object-rating-form":290,"../rating/rating":300,"jquery":"jquery"}],210:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = AsyncLabelText;


var COMPONENT_ATTR = 'data-async-object-rating-handle-text';
var COMPONENT_SELECTOR = '[' + COMPONENT_ATTR + ']';
var MAX_CHARACTERS = 150;

function AsyncLabelText(element) {
    var component = this;
    component.$element = (0, _jquery2.default)(element);

    var defaultText = component.$element.attr(COMPONENT_ATTR);

    /**
     * Label setter.
     * @param note if undefined, sets default message. Otherwise writes the first 150 characters in the element
     */
    component.setLabel = function (note) {
        note = note ? note.substring(0, MAX_CHARACTERS) : defaultText;
        component.$element.text(note);
    };
}

AsyncLabelText.getSelector = function () {
    return COMPONENT_SELECTOR;
};

},{"jquery":"jquery"}],211:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

var _asyncLogin = require('../async-login/async-login');

var _asyncLogin2 = _interopRequireDefault(_asyncLogin);

var _classObservable = require('../class-observable/class-observable');

var _classObservable2 = _interopRequireDefault(_classObservable);

var _appSpinner = require('../app-spinner/app-spinner');

var _appSpinner2 = _interopRequireDefault(_appSpinner);

var _rating = require('../rating/rating');

var _rating2 = _interopRequireDefault(_rating);

var _serviceController = require('../service-controller/service-controller');

var _serviceController2 = _interopRequireDefault(_serviceController);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = AsyncObjectRatingForm;

// component configuration

var COMPONENT_SELECTOR = '[data-async-object-rating-form]';
var RESET_HANDLE_SELECTOR = '[data-object-rating-form-reset-handle]';
var RESET_ACTION_ATTR = 'data-object-rating-form-reset-action';
var CONTAINER_SELECTOR = '[data-async-object-rating-content]';
var SPINNER_SELECTOR = '[data-async-object-rating-spinner]';
var FORM_URL_ATTR = 'url';
var TEXTAREA_SELECTOR = '[data-object-rating-textarea]';
var SUBMIT_BUTTON_SELECTOR = '[data-object-rating-submit-button]';
var DOING_REQUEST_CLASS = 'is-doing-request';
var FORM_SELECTOR = '[data-object-rating-form]';

function AsyncObjectRatingForm(element) {
    var component = this;
    component.$element = (0, _jquery2.default)(element);
    component.$container = component.$element.find(CONTAINER_SELECTOR);
    component.spinnerInstance = new _appSpinner2.default(component.$element.find(SPINNER_SELECTOR));
    component.url = component.$container.data(FORM_URL_ATTR);
    component.isComponentLoaded = false;
    component.observers = new _classObservable2.default();

    component.MESSAGES = {
        ELEMENT_LOADED: 0,
        ELEMENT_RESET: 1,
        ERROR_RESETTING: 2,
        ELEMENT_UPDATED: 3,
        ERROR_UPDATING: 4
    };

    function inject(html) {
        //Inject
        component.$container.html('');
        component.$container.append(html);
        //Create instances
        component.ratings = _serviceController2.default.getAllInstances(_rating2.default, component.$container);
        //Find selectors
        component.$textarea = component.$element.find(TEXTAREA_SELECTOR);
        component.$resetHandle = component.$element.find(RESET_HANDLE_SELECTOR);
        component.$submitButton = component.$element.find(SUBMIT_BUTTON_SELECTOR);
        component.$form = component.$element.find(FORM_SELECTOR);
        //Create watchers
        component.$resetHandle.on('click', component.reset);
        component.$submitButton.on('click', component.submit);
        //Store state and notify observers
        component.isComponentLoaded = true;
        component.observers.notify(component.MESSAGES.ELEMENT_LOADED);
    }

    /**
     * To be used by observers.  Call callbacks when form is successfully submitted.
     * @param observer callback
     */
    component.onSubmit = function (observer) {
        component.observers.listen(component.MESSAGES.ELEMENT_UPDATED, observer);
    };

    /**
     * To be used by observers.  Call callbacks when form is successfully reset.
     * @param observer callback
     */
    component.onReset = function (observer) {
        component.observers.listen(component.MESSAGES.ELEMENT_RESET, observer);
    };

    /**
     * Performs reset action
     * @param event
     * @returns {*} deferred promise.
     */
    component.reset = function (event) {
        if (typeof event !== 'undefined') {
            event.preventDefault();
        }
        if (component.isComponentLoaded) {

            for (var i = 0; i < component.ratings.length; i++) {
                component.ratings[i].selectRating(0);
            }
            component.$textarea.val('').html('');
            component.isComponentLoaded = false;
            var resetUrl = component.$form.attr(RESET_ACTION_ATTR);
            return _jquery2.default.ajax({
                method: 'POST',
                url: resetUrl
            }).done(function () {
                component.observers.notify(component.MESSAGES.ELEMENT_RESET);
            }).fail(function () {
                component.observers.notify(component.MESSAGES.ERROR_RESETTING);
            });
        }
    };

    /**
     * Performs submit action
     * @param event
     * @returns {*} deferred promise.
     */
    component.submit = function (event) {
        var _arguments = arguments;

        if (typeof event !== 'undefined') {
            event.preventDefault();
        }
        var url = component.$form.attr('action');
        var data = component.$form.find(':input').serialize();

        component.spinnerInstance.show();
        component.$element.addClass(DOING_REQUEST_CLASS);
        return _jquery2.default.ajax({
            method: 'POST',
            url: url,
            data: data
        }).done(function (response) {
            if (response.Result === 'OK') {
                component.observers.notify(component.MESSAGES.ELEMENT_UPDATED, component.$textarea.val());
            } else if (response.Result === 'ERROR') {
                inject(response.Html);
                component.observers.notify(component.MESSAGES.ERROR_UPDATING, _arguments);
            }
            return _jquery2.default.when(_arguments);
        }).fail(function () {
            component.observers.notify(component.MESSAGES.ERROR_UPDATING, _arguments);
        }).then(function () {
            component.spinnerInstance.hide();
            component.$element.removeClass(DOING_REQUEST_CLASS);
            return _jquery2.default.when(_arguments);
        });
    };

    component.performAjaxRequest = function () {
        return _jquery2.default.ajax(component.url).done(function (response) {
            var res = _jquery2.default.Deferred(); // eslint-disable-line new-cap
            if (response.Result === 'OK') {
                component.$container.append(response.Html);
                inject(response.Html);
                res.resolve();
            } else {
                res.reject(response);
            }
            return res.promise();
        });
    };

    /**
     * Shows the spinner, logs-in performs ajax request and hide the spinner.
     * @returns {*} deferred promise.
     */
    component.load = function () {
        if (component.isComponentLoaded) {
            return _jquery2.default.when();
        }
        return component.spinnerInstance.show().then(function () {
            return (0, _asyncLogin2.default)(function () {
                return component.performAjaxRequest().then(function () {
                    return component.spinnerInstance.hide();
                });
            });
        });
    };
}

AsyncObjectRatingForm.getSelector = function () {
    return COMPONENT_SELECTOR;
};

},{"../app-spinner/app-spinner":201,"../async-login/async-login":208,"../class-observable/class-observable":218,"../rating/rating":300,"../service-controller/service-controller":317,"jquery":"jquery"}],212:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

var _classObservable = require('../class-observable/class-observable');

var _classObservable2 = _interopRequireDefault(_classObservable);

var _serviceController = require('../service-controller/service-controller');

var _serviceController2 = _interopRequireDefault(_serviceController);

var _rating = require('../rating/rating');

var _rating2 = _interopRequireDefault(_rating);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = AsyncOverallRating;


var OVERALL_RATING_SELECTOR = '[data-overall-object-rating]';

function AsyncOverallRating(element) {
    var component = this;
    component.$element = (0, _jquery2.default)(element);
    component.ratingController = _serviceController2.default.getInstance(_rating2.default, element);
    component.observers = new _classObservable2.default();
    component.isEnabled = true;
    component.value = component.ratingController.selectedValue;

    /**
     *  To be used by observers.
     *  Registers a callback that will be called when the overall rating is successfully changed.
     * @param observer callback
     */
    component.onChange = function (observer) {
        component.observers.listen(observer);
    };

    /**
     * Enables ajax communication.
     */
    component.enable = function () {
        component.isEnabled = true;
    };

    /**
     * Disables ajax communication.
     */
    component.disable = function () {
        component.isEnabled = false;
    };

    /**
     * Overall rating value. Retrieved from the rating component.
     * @returns {*}
     */
    component.getValue = function () {
        component.value = component.ratingController.selectedValue;
        return component.value;
    };

    /**
     * Submits to the server the overall rating.
     * @returns {*} deferred promise.
     */
    component.submit = function () {
        var prevValue = component.value;
        var newValue = component.getValue();
        if (prevValue !== component.value) {
            component.observers.notify(newValue);

            if (component.isEnabled) {
                var url = component.$element.attr('action');
                var data = component.$element.serialize();
                return _jquery2.default.ajax({
                    method: 'POST',
                    url: url,
                    data: data
                });
            } else {
                return _jquery2.default.when();
            }
        }
    };

    //Watchers
    component.ratingController.onChange(component.submit);
}

AsyncOverallRating.getSelector = function () {
    return OVERALL_RATING_SELECTOR;
};

},{"../class-observable/class-observable":218,"../rating/rating":300,"../service-controller/service-controller":317,"jquery":"jquery"}],213:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

var _asyncLabelContainer = require('./async-label-container');

var _asyncLabelContainer2 = _interopRequireDefault(_asyncLabelContainer);

var _asyncObjectRatingForm = require('./async-object-rating-form');

var _asyncObjectRatingForm2 = _interopRequireDefault(_asyncObjectRatingForm);

var _asyncOverallRating = require('./async-overall-rating');

var _asyncOverallRating2 = _interopRequireDefault(_asyncOverallRating);

var _expandible = require('../expandible/expandible');

var _expandible2 = _interopRequireDefault(_expandible);

var _serviceController = require('../service-controller/service-controller');

var _serviceController2 = _interopRequireDefault(_serviceController);

var _hiddenHelper = require('./hidden-helper');

var _hiddenHelper2 = _interopRequireDefault(_hiddenHelper);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = AsyncRatingContainer;


var COMPONENT_SELECTOR = '[data-async-rating-container]';

function AsyncRatingContainer(element) {
    var component = this;
    component.$element = (0, _jquery2.default)(element);
    //Dependencies
    component.expandInstance = _serviceController2.default.getInstance(_expandible2.default, element);
    component.labelInstance = _serviceController2.default.getInstance(_asyncLabelContainer2.default, element);
    component.overallRatingInstance = _serviceController2.default.getInstance(_asyncOverallRating2.default, element);
    component.formInstance = _serviceController2.default.getInstance(_asyncObjectRatingForm2.default, element);

    /**
     * This function is called when the element gets expanded or collapsed.
     * If expanded, disables the ovarall rating ajax and loads the form instance.
     * If collapses, enables the overall rating ajax request.
     * @param isExpanded true is expanded. false if collapsed.
     */
    component.expandHandler = function (isExpanded) {
        if (isExpanded) {
            component.overallRatingInstance.disable();
            component.formInstance.load();
        } else {
            component.overallRatingInstance.enable();
        }
    };

    component.show = function () {
        component.expandInstance.toggleExpand(true);
        _hiddenHelper2.default.show(component.$element);
    };

    component.hide = function () {
        _hiddenHelper2.default.hide(component.$element);
    };

    //Watchers
    component.expandInstance.onChange(component.expandHandler);
    component.formInstance.onSubmit(collapseAndSetLabel);
    component.formInstance.onReset(function () {
        return collapseAndSetLabel();
    });

    function collapseAndSetLabel(label) {
        component.expandInstance.toggleExpand(false);
        component.labelInstance.setLabel(label);
    }
}

AsyncRatingContainer.getSelector = function () {
    return COMPONENT_SELECTOR;
};
_serviceController2.default.getAllInstances(AsyncRatingContainer);

},{"../expandible/expandible":229,"../service-controller/service-controller":317,"./async-label-container":210,"./async-object-rating-form":211,"./async-overall-rating":212,"./hidden-helper":215,"jquery":"jquery"}],214:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

var _classObservable = require('../class-observable/class-observable');

var _classObservable2 = _interopRequireDefault(_classObservable);

var _asyncLogin = require('../async-login/async-login');

var _asyncLogin2 = _interopRequireDefault(_asyncLogin);

var _hiddenHelper = require('./hidden-helper');

var _hiddenHelper2 = _interopRequireDefault(_hiddenHelper);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = AsyncRatingTrigger;


var COMPONENT_SELECTOR = '[data-rating-trigger]';

function AsyncRatingTrigger(element) {
    var component = this;
    component.$element = (0, _jquery2.default)(element);
    component.observers = new _classObservable2.default();
    component.visible = _hiddenHelper2.default.isShown(component.$element);

    component.onTrigger = function (observer) {
        component.observers.listen(observer);
    };

    component.clickHandler = function () {
        return (0, _asyncLogin2.default)(function () {
            component.observers.notify();
            component.hide();
        });
    };

    component.hide = function () {
        component.visible = _hiddenHelper2.default.hide(component.$element);
    };

    component.show = function () {
        component.visible = _hiddenHelper2.default.show(component.$element);
    };

    component.$element.click(component.clickHandler);
}

AsyncRatingTrigger.getSelector = function () {
    return COMPONENT_SELECTOR;
};

},{"../async-login/async-login":208,"../class-observable/class-observable":218,"./hidden-helper":215,"jquery":"jquery"}],215:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
var HIDE_CLASS = 'hide';

/**
 * Static helper. Abstracts the handling of the HIDE_CLASS class into a component.
 */
var hiddenHelper = function () {
    var component = {};

    component.isShown = function ($element) {
        return !$element.hasClass(HIDE_CLASS);
    };

    component.show = function ($element) {
        $element.removeClass(HIDE_CLASS);
        return true;
    };

    component.hide = function ($element) {
        $element.addClass(HIDE_CLASS);
        return false;
    };

    return component;
}();

exports.default = hiddenHelper;

},{}],216:[function(require,module,exports){
// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

// explicitly inject dependencies (alphabetically), only those needed

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

var _isEqual = require('lodash/isEqual');

var _isEqual2 = _interopRequireDefault(_isEqual);

var _thousandSeparator = require('../thousand-separator/thousand-separator');

var _thousandSeparator2 = _interopRequireDefault(_thousandSeparator);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// what does this module expose?
exports.default = Autocomplete;

// component configuration

var CLEAR_HANDLER_SELECTOR = '[data-autocomplete-clear]';
var DEFAULT_SELECTOR = '[data-autocomplete]';
var IS_ASYNC_ATTR = 'data-autocomplete-is-async';
var INPUT_SELECTOR = '[data-autocomplete-input]';
var DIRTY_CLASS = 'is-dirty';
var ITEM_TEMPLATE_SELECTOR = '[data-autocomplete-item-template]';
var NIVEAU_ID_ATTRIBUTE = 'data-autocomplete-niveau-id';
var PARENT_ID_ATTRIBUTE = 'data-autocomplete-parent-id';
var LIST_SELECTOR = '[data-autocomplete-list]';
var MATCH_CLASS = 'is-match';
var MATCH_SELECTOR = '[data-autocomplete-match]';
var MINLENGTH_ATTR = 'data-autocomplete-minlength';
var MINLENGTH = 3;
var OPENED_CLASS = 'is-open';
var OUTPUT_SELECTOR = '[data-autocomplete-output]';
var SELECTED_CLASS = 'is-selected';
var UPDATING_CLASS = 'is-updating';
var ALT_SUGGESTIONS_CLASS = 'has-alt-suggestions';
var NO_SUGGESTIONS_CLASS = 'has-no-suggestions';
var URL_SELECTOR = '[data-autocomplete-url]';
var ALT_URL_SELECTOR = '[data-autocomplete-alt-url]';
var SUGGESTIONS_SELECTOR = '[data-autocomplete-suggestions]';

var SEARCH_QUERY_UPDATED_EVENT = 'searchqueryupdated';
var SEARCH_QUERY_SET_EVENT = 'searchQuerySet';
var RESET_FILTER_EVENT = 'reset';
var SEARCH_INVALID_LOCATION_EVENT = 'invalidlocation';

var THOUSAND_SEPARATOR = 'data-thousand-separator';

var KEY_CODE = {
    UP: 38,
    DOWN: 40,
    ENTER: 13,
    LEFT: 37,
    RIGHT: 39,
    ESCAPE: 27,
    BACKSPACE: 8,
    PLUS: 187,
    PLUSNUMERICPAD: 107,
    SHIFT: 16,
    CTRL: 17,
    COMMAND_FIREFOX: 224,
    COMMAND: 91
};

/**
 * @param {HTMLElement} element any block level HTML element
 */
function Autocomplete(element) {
    var component = this;
    component.$element = (0, _jquery2.default)(element);
    component.$form = component.$element.closest('form');
    component.isAsync = component.$element[0].hasAttribute(IS_ASYNC_ATTR);
    component.$input = component.$element.find(INPUT_SELECTOR);
    component.$suggestions = component.$element.find(SUGGESTIONS_SELECTOR);
    component.$list = component.$element.find(LIST_SELECTOR);
    component.$output = component.$element.find(OUTPUT_SELECTOR);
    component.$clearHandle = component.$element.find(CLEAR_HANDLER_SELECTOR);
    component.itemTemplate = component.$element.find(ITEM_TEMPLATE_SELECTOR).html();
    component.minLength = component.$input.attr(MINLENGTH_ATTR) || MINLENGTH;
    component.queryObjectIndex = 0;
    component.query = '';
    component.isOpen = false;
    component.options = [];
    component.selectedIndex = -1;
    component.$autoCompleteUrl = component.$element.find(URL_SELECTOR);
    component.$autoCompleteAltUrl = component.$element.find(ALT_URL_SELECTOR);
    component.url = component.$autoCompleteUrl.val();
    component.altUrl = component.$autoCompleteAltUrl.val();
    component.activeOptions = [];
    component.request = null;
    component.requestCount = 0;
    component.firstSubmitAttempt = false;
    component.close();
    component.bindEvents();
    element.autoComplete = component;
    component.altSuggestionsIsOpen = false;
    component.noSuggestionsFoundIsOpen = false;
    component.separator = component.$element.attr(THOUSAND_SEPARATOR);
    // initialise server side set active options
    component.setActiveOptions();
}

/**
 *  Binds all necessary events for the autocomplete
 */

Autocomplete.prototype.bindEvents = function () {
    var _this = this;

    var component = this;

    // listen to submit event and determine if location is valid
    component.$form.on('submit', function (event) {
        if (!component.isValidLocation() && !component.firstSubmitAttempt) {
            // location is invalid and it's typed for the first time wrong
            event.preventDefault();
            component.searchAlternativeSuggestions();
            component.altSuggestionsIsOpen = true;
            return false;
        }
    });

    component.$form.on(SEARCH_INVALID_LOCATION_EVENT, function () {
        component.$suggestions.addClass(NO_SUGGESTIONS_CLASS);
        if (!component.isOpen) {
            component.open();
        }
    });

    component.$input.on('blur', function () {
        setTimeout(function () {
            return component.closeIfValidLocation();
        }, 400);
    });

    component.$input.on('keydown', function (e) {
        switch (e.keyCode) {
            case KEY_CODE.ENTER:
                // there is no suggestion found and this is the second attempt, submit the form
                if (component.firstSubmitAttempt) {
                    component.$element.trigger(SEARCH_QUERY_UPDATED_EVENT);
                    return false;
                }
                // if you selected an item from the list then you can search for the next location
                else if (component.selectedIndex >= 0) {
                        component.altSuggestionsIsOpen = false;
                        component.$element.trigger(SEARCH_QUERY_UPDATED_EVENT, { isOpen: component.isOpen });
                        return false;
                    }
                    // if the first item is exact then that one can be automatically selected and the user can search on the next location
                    else if (component.options.length && component.options[0].isExact && !component.altSuggestionsIsOpen) {
                            component.selectItem(0);
                            component.altSuggestionsIsOpen = false;
                            component.$element.trigger(SEARCH_QUERY_UPDATED_EVENT, { isOpen: component.isOpen });
                            return false;
                        }
                        // on a slow connection the request can be canceled by incrementing the request count so that the user can search on the next location and doesn't have to wait.
                        else if (component.request && component.request.state() === 'pending') {
                                component.requestCount++; //reset request count
                                component.altSuggestionsIsOpen = false;
                                component.$element.trigger(SEARCH_QUERY_UPDATED_EVENT);
                                return false;
                            }
                            // else do an alternative search
                            else {
                                    component.searchAlternativeSuggestions();
                                    component.altSuggestionsIsOpen = true;
                                    return false;
                                }
            case KEY_CODE.PLUS:
            case KEY_CODE.PLUSNUMERICPAD:
                var inputElement = _this;
                // if you selected an item from the list then you can search for the next location
                if (component.selectedIndex >= 0) {
                    component.altSuggestionsIsOpen = false;
                    component.addPlus(inputElement);
                    // return false because we added the plus already
                    return false;
                }
                // if the first item is exact then that one can be automatically selected and the user can search on the next location
                else if (component.options.length && component.options[0].isExact && !component.altSuggestionsIsOpen) {
                        component.selectItem(0);
                        component.altSuggestionsIsOpen = false;
                        component.addPlus(inputElement);
                        return false;
                    }
                    // on a slow connection the request can be canceled by incrementing the request count so that the user can search on the next location and doesn't have to wait.
                    else if (component.request && component.request.state() === 'pending') {
                            component.requestCount++; //reset request count
                            component.altSuggestionsIsOpen = false;
                            component.addPlus(inputElement);
                            return false;
                        }
                        // to prevent empty zbox addition of "+"
                        else if (component.$input.val().length > 0) {
                                component.altSuggestionsIsOpen = false;
                                component.addPlus(inputElement);
                                // return false because we added the plus already
                                return false;
                            }
                            // else do an alternative search
                            else {
                                    component.searchAlternativeSuggestions();
                                    component.altSuggestionsIsOpen = true;
                                    return false;
                                }
        }
    });

    component.$input.on('keyup', function (e) {
        component.setDirty();

        switch (e.keyCode) {
            case KEY_CODE.SHIFT:
                break;
            case KEY_CODE.LEFT:
                break;
            case KEY_CODE.RIGHT:
                break;
            case KEY_CODE.UP:
                if (component.isOpen) {
                    component.selectPrevItem();
                }
                break;
            case KEY_CODE.DOWN:
                if (component.isOpen) {
                    component.selectNextItem();
                }
                break;
            case KEY_CODE.BACKSPACE:
                component.altSuggestionsIsOpen = false;
                component.synchroniseActiveOptions();
                component.search();
                break;
            case KEY_CODE.ESCAPE:
                component.synchroniseActiveOptions();
                component.close();
                break;
            case KEY_CODE.ENTER:
            case KEY_CODE.PLUS:
            case KEY_CODE.PLUSNUMERICPAD:
                if (!component.altSuggestionsIsOpen) {
                    component.addNewActiveOption = true;
                    component.close();
                }
                break;
            case KEY_CODE.CTRL:
            case KEY_CODE.COMMAND:
            case KEY_CODE.COMMAND_FIREFOX:
                if (component.altSuggestionsIsOpen) {
                    // user is probably using the selecting or pasting shortcut
                    // the suggestion is visible, user is not selecting one, close the suggestions
                    component.altSuggestionsIsOpen = false;
                    component.close();
                }
                break;
            default:
                if (component.altSuggestionsIsOpen) {
                    // the suggestion is visible, user is not selecting one, close the suggestions
                    component.altSuggestionsIsOpen = false;
                    component.close();
                } else {
                    // alternate suggestion isn't open
                    component.synchroniseActiveOptions();
                    component.search();
                    component.setDummyOption();
                }
        }
    });

    component.$list.on('click', 'li', function (event) {
        var index = (0, _jquery2.default)(event.currentTarget).index();
        component.selectItem(index);
        component.$input.focus();
        component.$element.trigger(SEARCH_QUERY_UPDATED_EVENT, { isOpen: component.isOpen });
        component.close();
    });

    component.$clearHandle.on('click', function () {
        component.reset();
        component.$input.focus();
    });

    component.$input.on('focus', function () {
        return component.setDirty();
    });

    component.$element.on(RESET_FILTER_EVENT, function () {
        return component.reset();
    });

    component.$autoCompleteUrl.on('change', function () {
        component.url = component.$autoCompleteUrl.val();
    });

    component.$autoCompleteAltUrl.on('change', function () {
        component.altUrl = component.$autoCompleteAltUrl.val();
    });

    component.$element.on(SEARCH_QUERY_UPDATED_EVENT, function () {
        if (component.isAsync) {
            component.$input.blur(); // hide virtual keyboard on mobile devices
        }
    });
};

/**
 * reset the autocomplete and empty the input field
 */
Autocomplete.prototype.reset = function () {
    var component = this;
    component.$input.val('');
    component.synchroniseActiveOptions();
    component.setDirty();
    component.close();
};

/**
 * Trim spaces, add a plus and force input to end of the input
 */
Autocomplete.prototype.addPlus = function (inputElement) {
    inputElement.$input.val(inputElement.$input.val().replace(/^\s+|\s?\+?\s+$/g, '') + ' + ');

    var inputLength = inputElement.$input.val().length;
    inputElement.selectionStart = inputLength;
    inputElement.selectionEnd = inputLength;
};

/**
 * Sets component.activeOptions based on input value, hidden ids input element, parentId and niveauId data attributes
 */
Autocomplete.prototype.setActiveOptions = function () {
    var component = this;
    var parentId = component.$input.attr(PARENT_ID_ATTRIBUTE);
    var niveauId = component.$input.attr(NIVEAU_ID_ATTRIBUTE);

    var inputValue = component.$input.val();
    var strippedInputValue = inputValue.replace(/(\s+)?\+(\s+)?/g, '+');
    var splittedInput = strippedInputValue.split('+');
    var splittedOutput = component.$output.val().split(',');

    splittedInput.forEach(function (value, index) {
        component.activeOptions[index] = {
            value: value,
            id: splittedOutput[index],
            parentId: parentId,
            niveauId: niveauId
        };
    });
};

/**
 * Sets/removes the dirty state as a class in the html
 */
Autocomplete.prototype.setDirty = function () {
    var component = this;
    if (component.$input.val() === '') {
        component.$element.removeClass(DIRTY_CLASS);
    } else {
        component.$element.addClass(DIRTY_CLASS);
    }
};

/**
 * Determines the active query object based on the caret position,
 * also sets component.queryObjectIndex based on the matching of the caret position and the active options object
 * @return {Object} the active query object based on caret position
 */
Autocomplete.prototype.getQueryObjectOnPosition = function () {
    var component = this;
    var inputValue = component.$input.val();
    var splittedInput = inputValue.split('+');
    var caretPosition = component.getCaretPosition();

    var addedWordLength = 0;
    var activeQuery = void 0;
    for (var i = 0; i < splittedInput.length; i++) {
        var value = splittedInput[i];
        addedWordLength += value.length + 1;
        if (addedWordLength > caretPosition) {
            activeQuery = component.activeOptions[i];
            // store query object position
            component.queryObjectIndex = i;
            break;
        }
    }

    return activeQuery;
};

/**
 * Gets the relevant query string based on caret position
 * @return {String} query string
 */
Autocomplete.prototype.getQuery = function () {
    var component = this;
    if (component.caretPositionIsOnEnd()) {
        // would be better to do something like set query object index, because we only need component.queryObjectIndex to be set.
        // We do not need the return value
        component.getQueryObjectOnPosition();

        return component.getLastValue();
    } else {
        return component.getQueryObjectOnPosition().value;
    }
};

/**
 * Gets the caret position
 * @return {Number} the caret position
 */
Autocomplete.prototype.getCaretPosition = function () {
    var component = this;
    // assume for now we do not have something selected, because then selectionEnd should be taken into account
    return component.$input[0].selectionStart;
};

/**
 * Determines if the caret position is at the end of the input field
 * @return {Boolean} caret position is at end of input
 */
Autocomplete.prototype.caretPositionIsOnEnd = function () {
    var component = this;
    var caretPosition = component.getCaretPosition();
    var inputLength = component.$input.val().length;

    return caretPosition === inputLength;
};

/**
 * Opens the suggestion list
 */
Autocomplete.prototype.open = function () {
    var component = this;
    component.isOpen = true;
    component.$element.addClass(OPENED_CLASS);
    component.$list.addClass(OPENED_CLASS);
};

/**
 * CLoses the suggestion list
 */
Autocomplete.prototype.close = function () {
    var component = this;
    component.isOpen = false;
    component.altSuggestionsIsOpen = false;
    component.noSuggestionsFoundIsOpen = false;
    component.$element.removeClass(OPENED_CLASS);
    component.$list.removeClass(OPENED_CLASS);
};

Autocomplete.prototype.closeIfValidLocation = function () {
    var component = this;
    if (!component.noSuggestionsFoundIsOpen) {
        component.close();
    }
};

/**
 *  Synchronise activeOptions with input fields, call this function if there are bugs ¯\_(ツ)_/¯
 */
Autocomplete.prototype.synchroniseActiveOptions = function () {
    var component = this;
    var inputValue = component.$input.val();
    var strippedInputValue = inputValue.replace(/(\s+)?\+(\s+)?/g, '+');
    var splittedInput = strippedInputValue.split('+');

    // disable straal if multiple selections
    if (splittedInput.length <= 1 || splittedInput.length === 2 && splittedInput[splittedInput.length - 1] === '') {
        component.$element.trigger(SEARCH_QUERY_SET_EVENT, { multiple: false });
    } else {
        component.$element.trigger(SEARCH_QUERY_SET_EVENT, { multiple: true });
    }

    var filteredActiveOptions = component.activeOptions.map(function (option, index) {
        if (splittedInput[index] && splittedInput[index].trim() === option.value) {
            return option;
        } else {
            return {};
        }
    });

    var mappedActiveOptions = [];
    component.activeOptions.forEach(function (option, index) {
        if (filteredActiveOptions[index] === option) {
            mappedActiveOptions.push(option);
        } else if (splittedInput[index]) {
            mappedActiveOptions.push({ id: 0, value: splittedInput[index] });
        }
    });

    component.activeOptions = mappedActiveOptions;

    // should we set dummy option?
    var activeOptionsChanged = !(0, _isEqual2.default)(component.activeOptions, filteredActiveOptions);

    // first reset output value
    component.$output.val('');
    // different value is typed, reset the first submit attempt
    component.firstSubmitAttempt = false;

    mappedActiveOptions.forEach(function (option, index) {
        // update hidden input
        if (index === 0) {
            component.$output[0].value = option.id;
        } else {
            component.$output[0].value += ',' + option.id;
        }
    });

    // if last item in the splitted input array is a empty string then it means that the user has
    // used backspace untill the `+` and instead of replacing we should add a dummy option.
    if (splittedInput[splittedInput.length - 1] === '') {
        component.addNewActiveOption = true;
    }

    if (activeOptionsChanged) {
        component.setDummyOption();
    }
};

/**
 * Returns the last query string from the input
 * @return {String} last query string from the input
 */
Autocomplete.prototype.getLastValue = function () {
    var inputValue = this.$input[0].value;
    var strippedInputValue = inputValue.replace(/(\s+)?\+(\s+)?/g, '+');
    var splittedValues = strippedInputValue.split('+');
    return splittedValues[splittedValues.length - 1];
};

Autocomplete.prototype.constructQueryUrl = function (url, query) {
    var component = this;
    var firstSelectedOption = component.activeOptions[0];

    var niveauId = '';
    var parentId = '';
    if (firstSelectedOption && typeof firstSelectedOption.niveauId !== 'undefined' && typeof firstSelectedOption.parentId !== 'undefined') {
        niveauId = firstSelectedOption.niveauId;
        parentId = firstSelectedOption.parentId;
    }

    return url.replace('{queryPlaceholder}', query).replace('{niveauPlaceholder}', niveauId).replace('{parentPlaceholder}', parentId);
};

/**
 * Searches for suggestions
 */
Autocomplete.prototype.search = function () {
    var _this2 = this;

    var component = this;
    component.$suggestions.removeClass(NO_SUGGESTIONS_CLASS).removeClass(ALT_SUGGESTIONS_CLASS);
    component.query = component.getQuery();

    if (component.query.length < component.minLength) {
        component.close();
        return;
    }
    component.selectedIndex = -1;
    component.options = [];

    var url = component.constructQueryUrl(component.url, component.query);
    component.$element.addClass(UPDATING_CLASS);

    component.request = _jquery2.default.ajax({
        url: url,
        requestCount: ++component.requestCount,
        dataType: 'jsonp',
        success: function success(response) {
            if (component.requestCount !== _this2.requestCount) {
                return;
            }
            component.options = component.transformResponse(response);
            component.renderList();
            if (component.options.length) {
                component.open();
            } else {
                component.close();
            }
        },
        error: function error() {
            component.options = [];
            component.renderList();
        }
    });
};

/**
 * Searches for alternative suggestions
 */
Autocomplete.prototype.searchAlternativeSuggestions = function () {
    var _this3 = this;

    var component = this;

    if (component.query.length < component.minLength) {
        component.close();
        return;
    }

    component.selectedIndex = -1;
    component.options = [];

    var url = component.constructQueryUrl(component.altUrl, component.query);
    component.$element.addClass(UPDATING_CLASS);

    component.request = _jquery2.default.ajax({
        url: url,
        requestCount: ++component.requestCount,
        dataType: 'jsonp',
        success: function success(response) {
            if (component.requestCount !== _this3.requestCount) {
                return; // not the latest request so don't do anything
            }

            component.options = component.transformResponse(response);
            component.renderList();

            if (component.options.length) {
                component.$suggestions.addClass(ALT_SUGGESTIONS_CLASS);
            } else {
                component.firstSubmitAttempt = true; // first search submit event
                component.noSuggestionsFoundIsOpen = true;
                component.$suggestions.addClass(NO_SUGGESTIONS_CLASS);
            }
            component.open();
        },
        error: function error() {
            component.options = [];
            component.renderList();
        }
    });
};

/**
 * Creates the html for the suggestion list
 * @return {String} html for suggestion list
 */
Autocomplete.prototype.renderList = function () {
    var component = this;
    var items = (0, _jquery2.default)('<ul>');
    component.selectedIndex = -1;
    component.options.forEach(function (option, index) {
        option.id = option.id || component.$list[0].id + '-option' + index;

        // ¯\_(ツ)_/¯ ask backend
        if (option.niveauId !== 3 && option.niveauId !== 5 && option.niveauId !== 15) {
            option.parentId = '';
        }

        var item = component.renderOption(option);
        items.append(item);
    });
    var html = items.html();
    component.$list.html(html);
    component.$element.removeClass(UPDATING_CLASS);
    return html;
};

/**
 * Renders and returns an option as HTML using the item template.
 * Any option property can be used in this template using `{propName}`.
 * @param {Object} option
 * @returns {String} html
 */
Autocomplete.prototype.renderOption = function (option) {
    var component = this;

    var html = this.itemTemplate;
    // replace placeholders in template by values from option data
    for (var prop in option) {
        if (option.hasOwnProperty(prop)) {
            html = html.replace('{' + prop + '}', option[prop]);
        }
    }
    html = component.highlightMatches(html);
    return html;
};

/**
 * Select previous item in list, or last item when already at the top of the list.
 * @returns {{index, item, option}} object with index of selected item, the item itself and the related option.
 */
Autocomplete.prototype.selectPrevItem = function () {
    var component = this;
    var index = component.selectedIndex - 1;
    if (index < 0) {
        index = component.options.length - 1;
    }
    return component.selectItem(index);
};

/**
 * Select next item in list, or first item when already at the end of the list.
 * @returns {{index, item, option}} object with index of selected item, the item itself and the related option.
 */
Autocomplete.prototype.selectNextItem = function () {
    var component = this;
    var index = component.selectedIndex + 1;
    if (index > component.options.length - 1) {
        index = 0;
    }
    return component.selectItem(index);
};

/**
 * @param {Number} index
 * @returns {{index, item, option}} object with index of selected item, the item itself and the related option.
 */
Autocomplete.prototype.selectItem = function (index) {
    var component = this;
    var selectedOption = component.options[index];
    component.isDirty = !(0, _isEqual2.default)(component.selectedOption, selectedOption);
    component.selectedOption = selectedOption;
    component.selectedIndex = index;

    // set selected state in autocomplete list
    var $items = component.$list.children();
    $items.removeClass(SELECTED_CLASS);
    $items.eq(index).addClass(SELECTED_CLASS);

    var activeOption = {
        id: selectedOption.identifier,
        value: selectedOption.displayValue,
        parentId: selectedOption.parentId,
        niveauId: selectedOption.niveauId
    };

    var lastIndex = void 0;
    if (component.activeOptions.length > 0) {
        lastIndex = component.queryObjectIndex;
    } else {
        lastIndex = 0;
    }

    // set the last value to the selected value
    component.activeOptions[lastIndex] = activeOption;

    component.updateActiveOptions();
};

/**
 * Sets an active option when no match with autosuggest has been made
 */
Autocomplete.prototype.setDummyOption = function () {
    var component = this;
    var inputValue = component.$input.val();
    var strippedInputValue = inputValue.replace(/(\s+)?\+(\s+)?/g, '+');
    var inputValues = strippedInputValue.split('+');
    var lastValue = inputValues[inputValues.length - 1];

    if (inputValue.length >= component.minLength) {
        component.isDirty = true;
    }

    // lastValue can be an empty string if key combinations are used
    if (!lastValue || component.activeOptions.length && lastValue.trim() === component.activeOptions[component.activeOptions.length - 1].value) {
        return;
    }

    var activeOption = {
        id: 0,
        value: lastValue.replace('/^\s+/g', '')
    };

    if (component.addNewActiveOption) {
        component.activeOptions.push(activeOption);
        // reset addNewActiveOption
        component.addNewActiveOption = false;
    } else {
        var lastIndex = component.activeOptions.length > 0 ? component.activeOptions.length - 1 : 0;
        component.activeOptions[lastIndex] = activeOption;
    }

    component.updateActiveOptions();
};

Autocomplete.prototype.updateActiveOptions = function () {
    var component = this;
    component.activeOptions.forEach(function (option, index) {
        if (index === 0) {
            component.$input[0].value = option.value;
            component.$output[0].value = option.id;
        } else {
            component.$input[0].value += ' + ' + option.value;
            component.$output[0].value += ',' + option.id;
        }
    });
};

/**
 * Transform JSON response into manageable object
 * @param {Object} response JSON query response
 * @return {Object} transformed response
 */
Autocomplete.prototype.transformResponse = function (response) {
    var component = this;
    var options = [];

    response.Results.forEach(function (option) {
        var identifier = option.GeoIdentifier;
        var name = option.Display.Naam.replace('+', ' ');
        var niveau = option.Display.Niveau ? option.Display.Niveau + ' ' : '';
        var parent = option.Display.Parent ? ', ' + option.Display.Parent : '';
        var count = option.Aantal ? _thousandSeparator2.default.format(option.Aantal, component.separator) : '';

        var displayValue = niveau + name + parent;

        options.push({
            identifier: identifier,
            value: name,
            label: name,
            description: option.Display.NiveauLabel + parent,
            count: count,
            niveauId: option.Niveau,
            parentId: option.Parent,
            displayValue: displayValue,
            isExact: option.Exact
        });
    });

    return options;
};

/**
 * Highlights parts of html string matching the current input value.
 * @param {String} html
 * @return {String} html with highlighted matches
 */
Autocomplete.prototype.highlightMatches = function (html) {
    var component = this;
    var item = (0, _jquery2.default)('<temp>').html(html);
    var pattern = component.query.replace(/[()]/gi, '');
    pattern = new RegExp(pattern, 'ig');
    item.find(MATCH_SELECTOR).each(function (index, element) {
        var $element = (0, _jquery2.default)(element);
        var highlightedHtml = $element.html().replace(pattern, function (match) {
            return '<span class="' + MATCH_CLASS + '">' + match + '</span>';
        });
        $element.html(highlightedHtml);
    });
    return item.html();
};

Autocomplete.prototype.isValidLocation = function () {
    var component = this;
    if (component.options.length && component.options[0].isExact && !component.altSuggestionsIsOpen) {
        return true;
    }
    return component.$output.val() !== '0';
};

// automatically turn all elements with the default selector into components
(0, _jquery2.default)(DEFAULT_SELECTOR).each(function (index, element) {
    return new Autocomplete(element);
});

},{"../thousand-separator/thousand-separator":322,"jquery":"jquery","lodash/isEqual":172}],217:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

require('funda-slick-carousel');

exports.default = Carousel;


var COMPONENT_SELECTOR = '[data-carousel]';
var ITEM_LIST_SELECTOR = '[data-carousel-list]';
var NEXT_SELECTOR = '[data-carousel-next]';
var ENHANCED_CLASS = 'is-enhanced';
var BP_MEDIUM = 750; // $bp-medium
var BP_LARGE = 1020; // $bp-large

function Carousel(element) {
    var component = this;

    component.$element = (0, _jquery2.default)(element);
    component.$items = component.$element.find(ITEM_LIST_SELECTOR);
    component.$nextButton = component.$element.find(NEXT_SELECTOR);
    component.initSlick();
}

Carousel.prototype.initSlick = function () {
    var component = this;

    component.$items.slick({
        mobileFirst: true,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        nextArrow: component.$nextButton,
        prevArrow: false,
        responsive: [{
            breakpoint: BP_MEDIUM,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        }, {
            breakpoint: BP_LARGE,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3
            }
        }]
    }).addClass(ENHANCED_CLASS);
};

(0, _jquery2.default)(COMPONENT_SELECTOR).each(function (index, element) {
    return new Carousel(element);
});

},{"funda-slick-carousel":"funda-slick-carousel","jquery":"jquery"}],218:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = Observable;

/**
 * Observer pattern with optional topics.
 * See: https://en.wikipedia.org/wiki/Observer_pattern
 */

function Observable() {
    var publics = {};

    var globalObservers = [];
    var topicObservers = {};

    /**
     * Register an observer. It can
     *
     * @param p1 topic (optional)
     * @param p2 observer callback
     */
    publics.listen = listen;
    function listen(p1, p2) {
        var observer;
        var topic;
        //Only 1 parameter = Global listener
        if (typeof p2 === 'undefined') {
            observer = p1;
            globalObservers.push(observer);
        } else {
            topic = p1;
            observer = p2;
            if (typeof topicObservers[topic] === 'undefined') {
                topicObservers[topic] = [];
            }
            topicObservers[topic].push(observer);
        }
    }

    /**
     * Sends a message to the observers
     *
     * @param topic (optional)
     * @param message
     */
    publics.notify = notify;
    function notify(topic, message) {
        for (var i = 0; i < globalObservers.length; i++) {
            globalObservers[i](topic, message);
        }
        if (typeof topic !== 'undefined' && typeof topicObservers[topic] !== 'undefined') {
            for (var _i = 0; _i < topicObservers[topic].length; _i++) {
                topicObservers[topic][_i](message);
            }
        }
    }

    return publics;
}

},{}],219:[function(require,module,exports){
// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

// explicitly inject dependencies (alphabetically), only those needed

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// what does this module expose?
exports.default = ClearInput;

// component configuration

var COMPONENT_SELECTOR = '[data-clear-input]';
var INPUT_SELECTOR = '[data-dirty-input]';
var DIRTY_CLASS = 'is-dirty';
var TRIGGER_SELECTOR = '[data-clear-input-handle]';

function ClearInput(element) {
    var component = this;
    component.$element = (0, _jquery2.default)(element);
    component.$trigger = component.$element.find(TRIGGER_SELECTOR);
    component.$input = component.$element.find(INPUT_SELECTOR);
    component.bindEvents();
    if (component.$input.val() !== '') {
        component.$trigger.addClass(DIRTY_CLASS);
    }
}

ClearInput.prototype.clear = function () {
    var component = this;
    component.$input.val('');
    component.$trigger.removeClass(DIRTY_CLASS);
};

ClearInput.prototype.bindEvents = function () {
    var component = this;

    component.$input.on('keyup', function () {
        component.toggleInput();
    });

    component.$trigger.on('click', function () {
        component.clear();
    });
};

ClearInput.prototype.toggleInput = function () {
    var component = this;

    if (component.$input.val() === '') {
        component.handleEmptyInput();
    } else {
        component.handleInput();
    }
};

ClearInput.prototype.handleEmptyInput = function () {
    var component = this;
    component.$trigger.removeClass(DIRTY_CLASS);
};

ClearInput.prototype.handleInput = function () {
    var component = this;
    component.$trigger.addClass(DIRTY_CLASS);
};

// turn all elements with the default selector into components
(0, _jquery2.default)(COMPONENT_SELECTOR).each(function (index, element) {
    return new ClearInput(element);
});

},{"jquery":"jquery"}],220:[function(require,module,exports){
'use strict';

// dependencies (alphabetically)

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ArticleHeader = {};
module.exports = ArticleHeader;

// component configuration
var COMPONENT_ATTR = 'data-article-header';
var COMPONENT_SELECTOR = '[' + COMPONENT_ATTR + ']';

ArticleHeader.initialize = function ($element) {
    var _this = this;

    var navHeight = (0, _jquery2.default)('.app-header').outerHeight();
    var headerHeight = (0, _jquery2.default)(COMPONENT_SELECTOR).outerHeight() - 100;
    var minWidth = 768;
    var minHeight = 1024;
    var win = (0, _jquery2.default)(window);
    this.$element = (0, _jquery2.default)($element);

    if (win.width() > minWidth && win.height() > minHeight) {
        win.on('scroll', function () {
            var start = navHeight;
            var end = headerHeight;
            var opacity = 1 - (win.scrollTop() - start) / (end - start);

            if (opacity > 1) {
                opacity = 1;
            } else if (opacity < 0.3) {
                opacity = 0.3;
            }
            _this.$element.css('opacity', opacity);
        });
    }
};

if ((0, _jquery2.default)(COMPONENT_SELECTOR).length) {
    ArticleHeader.initialize((0, _jquery2.default)(COMPONENT_SELECTOR));
}

},{"jquery":"jquery"}],221:[function(require,module,exports){
// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

// explicitly inject dependencies (alphabetically), only those needed

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

var _thousandSeparator = require('../thousand-separator/thousand-separator');

var _thousandSeparator2 = _interopRequireDefault(_thousandSeparator);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// what does this module expose?
exports.default = ConditionalRangeFilter;

// component configuration

var DATA_ATTRIBUTE_SELECTOR_DEFAULT_VALUE = 'data-range-filter-selector-default-value';
var DATA_ATTRIBUTE_PREFIX_DATA_VAN = 'data-van';
var DATA_ATTRIBUTE_PREFIX_DATA_TOT = 'data-tot';

var PLACEHOLDER_VALUE_IGNORE_FILTER = 'ignore_filter';
var PLACEHOLDER_VALUE_CUSTOM_VALUE_FILTER = 'other';

var DISPLAY_TEMPLATE_TOKEN = '{token}';
var COMPONENT_SELECTOR = '[data-conditional-range-filter]';
var THOUSAND_SEPARATOR = '.';
var DISPLAY_TEMPLATE = '€ {token}';
var SPLIT_SYMBOL = ':';

function ConditionalRangeFilter(element) {
    var component = this;
    component.$element = (0, _jquery2.default)(element);
    component.vanRangeSelectorConfiguration = component.createRangeSelectorConditionalConfiguration(component.createElementSelector(element.id, 'min'), DATA_ATTRIBUTE_PREFIX_DATA_VAN);
    component.totRangeSelectorConfiguration = component.createRangeSelectorConditionalConfiguration(component.createElementSelector(element.id, 'max'), DATA_ATTRIBUTE_PREFIX_DATA_TOT);
    component.bindEvents();
}

ConditionalRangeFilter.prototype.bindEvents = function () {
    var component = this;
    component.$element.on('change', function (event) {
        return component.updateTargetedRangeSelectors(event.target || event.srcElement, component);
    });
};

ConditionalRangeFilter.prototype.addOptions = function (selector, newOptions) {
    return newOptions.forEach(function (optionToKeep) {
        return selector.add(optionToKeep);
    });
};
ConditionalRangeFilter.prototype.isValueToKeep = function (option, valuesToKeep) {
    return valuesToKeep.filter(function (valueToKeep) {
        return option.value === valueToKeep;
    }).length > 0;
};
ConditionalRangeFilter.prototype.clearAllOptions = function (selector) {
    return [].slice.call(selector.options).forEach(function () {
        return selector.remove(0);
    });
};
ConditionalRangeFilter.prototype.createFormattedValue = function (displayTemplate, thousandSeparator, token, value) {
    return displayTemplate.replace(token, _thousandSeparator2.default.format(value, thousandSeparator));
};
ConditionalRangeFilter.prototype.selectMatchingOption = function (optionsToKeep, filter) {
    return optionsToKeep.filter(function (optionToKeep) {
        return filter(optionToKeep.value);
    }).shift();
};
ConditionalRangeFilter.prototype.createElementSelector = function (id, postfix) {
    return '[data-conditie-filter-target="' + id + '"][data-range-filter-' + postfix + '] [data-range-filter-selector-preset-select]';
};
ConditionalRangeFilter.prototype.getIndexForMatchingOption = function (options, filter) {
    return options.reduce(function (carry, option, index) {
        return filter(option) ? index : carry;
    }, undefined);
};
ConditionalRangeFilter.prototype.addAsLastOptionIfNonEmpty = function (ignoreFilterOption, newVanRangeSelectorOptions) {
    return ignoreFilterOption && newVanRangeSelectorOptions.push(ignoreFilterOption);
};
ConditionalRangeFilter.prototype.splitToListOfTrimmedValues = function (datastring, splitSymbol) {
    return datastring.split(splitSymbol).map(function (value) {
        return value.trim();
    });
};
ConditionalRangeFilter.prototype.isPlaceholderValueForCustomValue = function (value) {
    return value === PLACEHOLDER_VALUE_CUSTOM_VALUE_FILTER;
};
ConditionalRangeFilter.prototype.isPlaceholderValueForNoMaximumValue = function (value) {
    return value === PLACEHOLDER_VALUE_IGNORE_FILTER;
};
ConditionalRangeFilter.prototype.hasPlaceholderValueForDefaultRangeSelectorValue = function (option) {
    return option.getAttribute(DATA_ATTRIBUTE_SELECTOR_DEFAULT_VALUE) === '';
};
ConditionalRangeFilter.prototype.addRangeFilterSelectorDefaultValueAttributeToOption = function (option) {
    return option.setAttribute(DATA_ATTRIBUTE_SELECTOR_DEFAULT_VALUE, '');
};

ConditionalRangeFilter.prototype.createRangeSelectorConditionalConfiguration = function (selectorTargetRangeSelector, dataAttributePrefix) {
    return {
        attributeNameDefaultValue: dataAttributePrefix + '-default-value',
        attributeNameNewRangeValue: dataAttributePrefix + '-range',
        selectorTargetRangeSelector: selectorTargetRangeSelector,
        attributeNameOptionValuesToKeep: dataAttributePrefix + '-options-to-keep',
        thousandSeparator: THOUSAND_SEPARATOR,
        displayTemplate: DISPLAY_TEMPLATE,
        splitSymbol: SPLIT_SYMBOL,
        token: DISPLAY_TEMPLATE_TOKEN
    };
};

ConditionalRangeFilter.prototype.updateTargetedRangeSelectors = function (conditionalSelector, component) {
    var selectedCondition = conditionalSelector.options[conditionalSelector.selectedIndex];

    component.updateTargetedRangeSelectorForConfigurationAndCondition(component.vanRangeSelectorConfiguration, selectedCondition);
    component.updateTargetedRangeSelectorForConfigurationAndCondition(component.totRangeSelectorConfiguration, selectedCondition);

    (0, _jquery2.default)(component.vanRangeSelectorConfiguration.selectorTargetRangeSelector).trigger('reset');
    (0, _jquery2.default)(component.totRangeSelectorConfiguration.selectorTargetRangeSelector).trigger('reset');
    (0, _jquery2.default)(component.totRangeSelectorConfiguration.selectorTargetRangeSelector).trigger('change');
};

ConditionalRangeFilter.prototype.updateTargetedRangeSelectorForConfigurationAndCondition = function (configuration, selectedCondition) {
    var component = this;
    var targetRangeSelector = document.querySelector(configuration.selectorTargetRangeSelector);
    var defaultSelectedValue = selectedCondition.getAttribute(configuration.attributeNameDefaultValue);

    var newVanRangeSelectorOptions = component.createOptionsForRangeSelector(component.splitToListOfTrimmedValues(selectedCondition.getAttribute(configuration.attributeNameNewRangeValue), configuration.splitSymbol), component.splitToListOfTrimmedValues(selectedCondition.getAttribute(configuration.attributeNameOptionValuesToKeep), configuration.splitSymbol), targetRangeSelector, defaultSelectedValue, function (value) {
        return component.createFormattedValue(configuration.displayTemplate, configuration.thousandSeparator, configuration.token, value);
    });

    component.updateRangeSelectorWithOptions(targetRangeSelector, newVanRangeSelectorOptions);
};

ConditionalRangeFilter.prototype.createOptionsForRangeSelector = function (newRangeSelectorValues, rangeSelectorValuesToKeep, targetRangeSelector, newRangeSelectorDefaultSelectedValue, formatValue) {
    var component = this;
    var optionsToKeep = [].slice.call(targetRangeSelector.options).filter(function (option) {
        return component.isValueToKeep(option, rangeSelectorValuesToKeep);
    });
    var otherOption = optionsToKeep.filter(function (optionToKeep) {
        return component.isPlaceholderValueForCustomValue(optionToKeep.value);
    }).shift();
    var newVanRangeSelectorOptions = newRangeSelectorValues.map(function (newOptionValue) {
        return new Option(formatValue(newOptionValue), newOptionValue);
    });
    newVanRangeSelectorOptions.unshift(otherOption);

    var ignoreFilterOption = component.selectMatchingOption(optionsToKeep, component.isPlaceholderValueForNoMaximumValue);
    component.addAsLastOptionIfNonEmpty(ignoreFilterOption, newVanRangeSelectorOptions);

    var selectedOption = newVanRangeSelectorOptions.filter(function (newOption) {
        return newOption.value === newRangeSelectorDefaultSelectedValue;
    }).shift();
    component.addRangeFilterSelectorDefaultValueAttributeToOption(selectedOption);

    return newVanRangeSelectorOptions;
};

ConditionalRangeFilter.prototype.syncSelectedIndexWithFlaggedDefaultOption = function (selector) {
    var component = this;
    selector.selectedIndex = component.getIndexForMatchingOption([].slice.call(selector.options), component.hasPlaceholderValueForDefaultRangeSelectorValue);
};

ConditionalRangeFilter.prototype.updateRangeSelectorWithOptions = function (targetRangeSelector, newRangeSelectorOptions) {
    var component = this;

    component.clearAllOptions(targetRangeSelector);
    component.addOptions(targetRangeSelector, newRangeSelectorOptions);
    component.syncSelectedIndexWithFlaggedDefaultOption(targetRangeSelector);
};

// turn all elements with the default selector into components
(0, _jquery2.default)(COMPONENT_SELECTOR).each(function (index, element) {
    return new ConditionalRangeFilter(element);
});

},{"../thousand-separator/thousand-separator":322,"jquery":"jquery"}],222:[function(require,module,exports){
// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = ContentFetch;

/*
  `element` is the element where you'd like to render the complete callback
  `urlFeed` is the URL API
  `onComplete` is the callback (function)
*/

function ContentFetch(element, urlFeed) {
    var onComplete = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : function () {};

    _jquery2.default.get({
        url: urlFeed,
        crossDomain: true,
        dataType: 'html',
        success: function success(data, responseText, jqXHR) {
            ContentFetch.showData(element, data);
            if (jqXHR.status == 204) {
                ContentFetch.hideWidget(element);
            }
        },
        error: function error() {
            ContentFetch.hideWidget(element);
        },
        complete: onComplete
    });
}

ContentFetch.showData = function (element, data) {
    (0, _jquery2.default)(element).html(data);
};

ContentFetch.hideWidget = function (element) {
    (0, _jquery2.default)(element).addClass('is-hidden');
};

},{"jquery":"jquery"}],223:[function(require,module,exports){
// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

// explicitly inject dependencies (alphabetically), only those needed

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// what does this module expose?
exports.default = ContentTabs;

// component configuration

var COMPONENT_SELECTOR = '[data-content-tabs]';
var COMPONENT_TRIGGER = '[data-tabs-trigger]';
var COMPONENT_TARGET = '[data-tabs-target]';

function ContentTabs(element) {
    var component = this; // Not necessary in ES2015, keep using it for consistency
    component.initTabs(element);
}

ContentTabs.prototype.initTabs = function (element) {
    var component = this;
    component.$element = (0, _jquery2.default)(element);

    component.$element.find(COMPONENT_TARGET).hide();
    component.$element.find(COMPONENT_TARGET).first().show();
    component.$element.find(COMPONENT_TRIGGER).children('li').first().addClass('active');

    component.$element.find(COMPONENT_TRIGGER).find('a:not([data-expandible-handle])').click(function (event) {
        event.preventDefault();

        (0, _jquery2.default)(COMPONENT_TRIGGER).find('li').removeClass('active');

        (0, _jquery2.default)(this).parent('li').addClass('active');

        var isCurrentTab = (0, _jquery2.default)(this).attr('href');
        (0, _jquery2.default)(COMPONENT_TARGET).hide();
        (0, _jquery2.default)(isCurrentTab).show();
    });
};

// turn all elements with the default selector into components
(0, _jquery2.default)(COMPONENT_SELECTOR).each(function (index, element) {
    return new ContentTabs(element);
});

},{"jquery":"jquery"}],224:[function(require,module,exports){
// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

// explicitly inject dependencies (alphabetically), only those needed

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// what does this module expose?
exports.default = ContentTwitterFeed;

// component configuration

var TWITTER_FEED_COMPONENT_ATTR = 'data-content-twitter-feed';
var TWITTER_FEED_COMPONENT_SELECTOR = '[' + TWITTER_FEED_COMPONENT_ATTR + ']';
var TWITTER_FEED_LIST_TEMPLATE_SELECTOR = '[data-content-twitter-feed-list]';
var TWITTER_FEED_TWEET_TEMPLATE_SELECTOR = '[data-content-twitter-feed-item-template]';
var TWITTER_FEED_TWEET_COUNT = '3';
var TWITTER_FEED_RELOAD_HANDLE = '[data-content-twitter-feed-reload-handle]';
var TWITTER_FEED_HAS_RESULTS_CLASS = 'has-results';

function ContentTwitterFeed(element) {
    var $component = this;

    $component.$element = (0, _jquery2.default)(element);
    $component.apiEndpointPage = 1;
    $component.apiEndpointURL = $component.$element.attr(TWITTER_FEED_COMPONENT_ATTR);
    $component.template = $component.$element.find(TWITTER_FEED_TWEET_TEMPLATE_SELECTOR).html();
    $component.$list = $component.$element.find(TWITTER_FEED_LIST_TEMPLATE_SELECTOR);
    $component.$reloadButton = $component.$element.find(TWITTER_FEED_RELOAD_HANDLE);

    $component.getTweets();

    $component.$reloadButton.on('click', function () {
        $component.getTweets();
    });
}

ContentTwitterFeed.prototype.getTweets = function () {
    var component = this;

    _jquery2.default.ajax({
        url: component.apiEndpointURL,
        data: {
            count: TWITTER_FEED_TWEET_COUNT,
            page: component.apiEndpointPage
        }
    }).then(function (response) {
        if (!response.length) {
            throw Error('No tweets.');
        }

        return response;
    }).then(function (response) {
        var html = component.renderTweets(component.template, response);
        component.update(html);
    }).catch(function (err) {
        return console.error('Error: ', err);
    });
};

ContentTwitterFeed.prototype.update = function (html) {
    var $component = this;

    $component.$list.append(html);
    $component.$element.addClass(TWITTER_FEED_HAS_RESULTS_CLASS);

    $component.apiEndpointPage++;
};

ContentTwitterFeed.prototype.render = function (template, data) {
    return Object.keys(data).reduce(function (html, prop) {
        var pattern = new RegExp('{' + prop + '}', 'g');

        if (prop === 'ProfileImageUrl') {
            return html.replace(pattern, data[prop].replace('_normal', ''));
        }

        return html.replace(pattern, data[prop]);
    }, template);
};

ContentTwitterFeed.prototype.renderTweets = function (template, tweets) {
    var component = this;

    return tweets.map(function (item) {
        return component.render(template, item);
    }).join('');
};

(0, _jquery2.default)(TWITTER_FEED_COMPONENT_SELECTOR).each(function (index, element) {
    return new ContentTwitterFeed(element);
});

},{"jquery":"jquery"}],225:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = CookiePolicy;

// component configuration

var COMPONENT_SELECTOR = '[data-cookie-policy]';
var VERSION_ATTR = 'data-cookie-policy-version';
var ACCEPTED_CLASS = 'is-accepted';
var AUTO_ACCEPTING_INTERACTIONS_SELECTOR = 'a:not([data-cookie-policy-ignore]), :input:not([data-cookie-policy-ignore]), .zoek-op-kaart';
var COOKIES_ACCEPTED_EVENT = 'cookiesaccepted';
var EVENT_NAMESPACE = '.cookie-policy';
var COOKIE_PREFIX = 'cookiesAccepted_';
var ENHANCED_CLASS = 'is-acceptable';

var COOKIE_TOGGLE = '[data-cookie-toggle]';
var COOKIE_TOGGLE_TARGET = '[data-cookie-target]';
var COOKIE_TOGGLE_TRIGGER = '[data-cookie-trigger]';

var $body = (0, _jquery2.default)('body');

function CookiePolicy(element) {
    var component = this;
    component.$element = (0, _jquery2.default)(element);
    component.version = element.getAttribute(VERSION_ATTR);
    component.cookieName = COOKIE_PREFIX + component.version;

    component.$element.addClass(ENHANCED_CLASS);
    component.toggleExpand(element);

    // Listen to all clickable elements and if any of them is clicked, we assume the user is accepting the cookies.
    $body.on('click' + EVENT_NAMESPACE, AUTO_ACCEPTING_INTERACTIONS_SELECTOR, function () {
        component.acceptPolicy();
    });
}

CookiePolicy.prototype.toggleExpand = function (element) {
    (0, _jquery2.default)(element).find(COOKIE_TOGGLE_TARGET).addClass('hidden');
    (0, _jquery2.default)(element).find(COOKIE_TOGGLE_TRIGGER).on('click', function (event) {
        event.preventDefault();
        (0, _jquery2.default)(this).hide().parents(COOKIE_TOGGLE).find(COOKIE_TOGGLE_TARGET).removeClass('hidden');
    });
};

CookiePolicy.prototype.acceptPolicy = function () {
    this.setCookie();
    this.$element.addClass(ACCEPTED_CLASS);
    $body.off('click' + EVENT_NAMESPACE);
    (0, _jquery2.default)(window).trigger(COOKIES_ACCEPTED_EVENT);
};

/**
 * sets cookie and return the cookie value
 * @returns {string}
 */
CookiePolicy.prototype.setCookie = function () {
    var component = this;
    var version = component.version;
    var date = new Date();
    date.setFullYear(date.getFullYear() + 1);
    var cookie = 'cookiesAccepted_' + version + '=' + version + '; expires=' + date.toGMTString() + '; path= /';

    document.cookie = cookie;
    return cookie;
};

// turn all elements with the default selector into components
(0, _jquery2.default)(COMPONENT_SELECTOR).each(function (index, element) {
    return new CookiePolicy(element);
});

},{"jquery":"jquery"}],226:[function(require,module,exports){
// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = CustomSelectBox;

// constant definitions

var COMPONENT_SELECTOR = '[data-custom-select-box]';
var DATA_URL = 'data-request-url';
var HANDLE_SELECTOR = '[data-instant-search-handle]';
var SELECTED_OPTION = '[data-selected-option]';
var OPTIONS_LIST = '[data-selectbox-list]';
var OPENED_CLASS = 'is-open';
var SELECTED_CLASS = 'is-selected';
var NO_RESULTS = 'no-results';
var DATA_PLACEHOLDER = 'data-custom-select-placeholder';
var DATA_NO_OPTIONS = 'data-custom-select-nooptions';

var optionTemplate = '<li role="option"\n    id="{Id}"\n    data-selectbox-option-display-value="{Value}"\n    class="selectbox-option">\n    <span class="selectbox-label" data-selectbox-match>{Label}</span>\n    <div class="selectbox-option-count">{ResultCount}</div>\n</li>';

function CustomSelectBox(element) {
    var component = this;
    component.$element = (0, _jquery2.default)(element);
    component.sourceUrl = component.$element.attr(DATA_URL);
    component.$handler = component.$element.find(HANDLE_SELECTOR);
    component.$selectedOption = component.$element.find(SELECTED_OPTION);
    component.$optionsList = component.$element.find(OPTIONS_LIST);
    component.isOpen = false;
    component.selectedIndex = -1;
    component.placeholder = component.$element.attr(DATA_PLACEHOLDER);
    component.noOptions = component.$element.attr(DATA_NO_OPTIONS);
    component.init();
}

CustomSelectBox.prototype.init = function () {
    var component = this;
    component.$selectedOption.html(component.placeholder);
    if (component.sourceUrl) {
        // for client-side rendering
        component.fetchOptions();
    } else {
        // for server-side rendering
        component.fetchOptionsFromPage();
    }
    component.checkOptions();
    component.bindEvents();
};

CustomSelectBox.prototype.renderOptionsList = function () {
    var component = this;
    component.$optionsList.html('');
    component.selectedIndex = -1;
    component.options.forEach(function (option) {
        var item = component.renderOption(option);
        component.$optionsList.append(item);
    });
};

CustomSelectBox.prototype.renderOption = function (option) {
    var itemTemplate = optionTemplate;

    for (var prop in option) {
        if (option.hasOwnProperty(prop)) {
            itemTemplate = itemTemplate.replace('{' + prop + '}', option[prop]);
        }
    }
    return itemTemplate;
};

CustomSelectBox.prototype.fetchOptions = function () {
    var component = this;
    _jquery2.default.ajax({
        url: component.sourceUrl,
        success: function success(response) {
            component.options = Array.isArray(response) ? response : [];
            component.renderOptionsList();
            component.handleZeroResults();
        },
        error: function error() {
            component.options = [];
            component.$selectedOption.html(component.noOptions);
        }
    });
};

CustomSelectBox.prototype.handleZeroResults = function () {
    var component = this;

    component.$optionsList.find('li').each(function (id, item) {
        var option = (0, _jquery2.default)(item);
        var resultCount = (0, _jquery2.default)(option.find('.selectbox-option-count')).html();

        if (parseInt(resultCount, 8) == 0) {
            option.addClass(NO_RESULTS);
        }
    });
};

// Getting the options list for server-side rendering
CustomSelectBox.prototype.fetchOptionsFromPage = function () {
    var component = this;
    component.options = (0, _jquery2.default)('.selectbox-list').find('li').map(function () {
        var item = {};
        item.Id = (0, _jquery2.default)(this).attr('id');
        item.Value = (0, _jquery2.default)(this).data('selectboxOptionDisplayValue');
        var children = (0, _jquery2.default)(this).children();
        if (children !== undefined) {
            if (children[0] !== undefined) {
                item.Label = (0, _jquery2.default)(this).children()[0].textContent;
            }
            if (children[1] !== undefined) {
                item.ResultCount = (0, _jquery2.default)(this).children()[1].textContent;
            }
        }
        return item;
    }) || [];
};

// If already selected item exist in url
// User can refresh the page or copy/paste the url
// This method checks the existence of selected item
// To select
CustomSelectBox.prototype.checkOptions = function () {
    var component = this;
    var selectedItem = component.$optionsList.find('.selectbox-option.' + SELECTED_CLASS);
    if (selectedItem.length) {
        component.selectOption(selectedItem.index());
    }
};

CustomSelectBox.prototype.bindEvents = function () {
    var component = this;
    component.bindSelectboxEvent();
    component.bindSelectOptionEvent();
    component.bindClickOutsideEvent();
};

CustomSelectBox.prototype.bindSelectboxEvent = function () {
    var component = this;
    component.$selectedOption.on('click', function () {
        if (component.isOpen) {
            component.closeOptions();
        } else {
            component.openOptions();
        }
    });
};

CustomSelectBox.prototype.openOptions = function () {
    var component = this;
    component.isOpen = true;
    component.$element.addClass(OPENED_CLASS);
    component.$optionsList.addClass(OPENED_CLASS);
};

CustomSelectBox.prototype.closeOptions = function () {
    var component = this;
    component.isOpen = false;
    component.$element.removeClass(OPENED_CLASS);
    component.$optionsList.removeClass(OPENED_CLASS);
};

CustomSelectBox.prototype.bindSelectOptionEvent = function () {
    var component = this;
    component.$optionsList.on('click', 'li', function (event) {
        var index = (0, _jquery2.default)(event.currentTarget).index();
        component.selectOption(index);
        component.updatePage();
        component.closeOptions();
    });
};

CustomSelectBox.prototype.selectOption = function (index) {
    var component = this;
    var selectedOption = component.options[index];
    component.selectedOption = selectedOption;
    component.selectedIndex = index;

    // set selected state in option list
    var $items = component.$optionsList.children();
    $items.removeClass(SELECTED_CLASS);
    $items.eq(index).addClass(SELECTED_CLASS);

    // select option
    component.$selectedOption.html(selectedOption.Label);
};

CustomSelectBox.prototype.updatePage = function () {
    var component = this;
    component.$handler.val(component.selectedOption.Value);
    component.$handler.trigger('change');
};

CustomSelectBox.prototype.bindClickOutsideEvent = function () {
    var component = this;
    (0, _jquery2.default)(document).on('click', function (event) {
        if ((0, _jquery2.default)(event.target).closest('.custom-select-box').length === 0) {
            component.closeOptions();
        }
    });
};

(0, _jquery2.default)(COMPONENT_SELECTOR).each(function (index, element) {
    return new CustomSelectBox(element);
});

},{"jquery":"jquery"}],227:[function(require,module,exports){
// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

// explicitly inject dependencies (alphabetically), only those needed

var $ = require('jquery');
var isFunction = require('lodash/isFunction');
var AsyncForm = require('../async-form/async-form');
var SocialLogin = require('../social-login/social-login');
var AppSpinner = require('../app-spinner/app-spinner');
var DialogPolyfill = require('dialog-polyfill');

// what does this module expose?
module.exports = Dialog;

// component configuration
var COMPONENT_ATTR = 'data-dialog';
var COMPONENT_SELECTOR = '[data-dialog]';
var CLOSE_SELECTOR = '[data-dialog-close]';
var BODY_SELECTOR = '[data-dialog-body]';
var HANDLE_ATTR = 'data-dialog-handle';
var OPEN_DIALOG_CLASS = 'has-open-dialog';
var OPEN_DIALOG_SELECTOR = 'dialog[open]';
var CANCEL_CLASS = 'cancel-button';
var AJAX_SUBMIT_SELECTOR = '[dialog-ajax-submit]';
var SOCIAL_LOGIN_SELECTOR = '[data-social-login]';
var URL_ISUSERLOGGEDIN = 'data-isuserloggedinurl';
var SPINNER_SELECTOR = '[data-dialog-spinner]';
var KEY_CODES_PREVENT_SCROLL = [32, // spacebar
33, // pageup
34, // pagedown
35, // end
36, // home
37, // left
38, // up
39, // right
40 // down
];

var supportsDialog = function () {
    var dialog = document.createElement('dialog');
    return typeof dialog.show === 'function';
}();

var $window = $(window);
var $body = $('body');

function Dialog(element, options) {
    options = options || {};
    var component = this;
    element.dialog = this;
    component.element = element;
    component.$element = $(element);
    component.id = element.getAttribute(COMPONENT_ATTR);
    component.$close = component.$element.find(CLOSE_SELECTOR);
    component.$body = component.$element.find(BODY_SELECTOR);
    component.onFormSuccess = options.onFormSuccess;
    component.isUserLoggedInUrl = element.getAttribute(URL_ISUSERLOGGEDIN);
    component.$spinner = component.$element.find(SPINNER_SELECTOR);
    component.previousDialog = null;

    if (!supportsDialog) {
        DialogPolyfill.registerDialog(element);
    }

    $body.on('click', '[' + HANDLE_ATTR + '="' + component.id + '"]', function (event) {
        event.preventDefault();
        if (!component.element.open) {
            component.open();
        }
        if (event.target.href) {
            // remove the content from the dialog body to prevent
            // showing incorrect content on reopening the dialog
            component.$body.html('');
            component.getContent(event.target.href);
        }
    });

    component.$element.on('click', CLOSE_SELECTOR, function () {
        component.close();
    });

    component.$element.on('click', 'a.' + CANCEL_CLASS, function (event) {
        event.preventDefault();
        component.close();
    });

    component.$element.on('click', function (event) {
        var target = $(event.target);
        if (target.is('dialog')) {
            component.close();
        }
    });

    // Close key (escape)
    $(document).on('keydown', function (event) {
        if (event.ctrlKey || event.shiftKey || event.altKey || event.metaKey) {
            return;
        }

        // ESC key
        if (event.keyCode === 27) {
            component.close();
        }
    });
}

Dialog.prototype.getContent = function (url) {
    var component = this;
    component.spinner = new AppSpinner(component.$spinner[0]);
    component.spinner.show();

    return $.ajax({
        url: url,
        cache: false,
        data: {
            currentUrl: window.location.pathname
        },
        xhrFields: {
            withCredentials: true
        },
        dataType: 'json'
    }).then(function (response) {
        if (response.Result === 'OK') {
            component.updateContent(response.View || response.Html);
        }
    }).catch(function (err) {
        console.error(err);
        var path = url.replace('dialog/', '');
        window.location.href = path + '?ReturnUrl=' + window.location.pathname;
    }).then(function () {
        component.spinner.hide();
    });
};

Dialog.prototype.enableSocialLogin = function (body) {
    var socialLogin = body.find(SOCIAL_LOGIN_SELECTOR);
    if (socialLogin.length === 1) {
        return new SocialLogin(socialLogin[0]);
    }
    return null;
};

Dialog.prototype.updateContent = function (content) {
    var component = this;
    component.$body.html(content);

    var containsAsyncForm = component.$body.find(AJAX_SUBMIT_SELECTOR).length > 0;
    if (containsAsyncForm) {
        component.asyncForm();
    }
    return component.enableSocialLogin(component.$body);
};

Dialog.prototype.asyncForm = function () {
    var component = this;
    return new AsyncForm(component.$body, {
        body: component.$body,
        success: function success(response) {
            if (component.onFormSuccess) {
                component.onFormSuccess(response);
            } else {
                window.location.reload();
            }
            component.close();
        },
        submitCallback: function submitCallback(body) {
            component.enableSocialLogin(body);
        }
    });
};

Dialog.prototype.hideVisibleDialogs = function () {
    var component = this;
    var $openDialog = $(OPEN_DIALOG_SELECTOR);
    if ($openDialog.length) {
        component.previousDialog = $openDialog[0];
        component.previousDialog.close();
    }
};

Dialog.prototype.restorePreviousDialogs = function () {
    var component = this;
    if (component.previousDialog != null) {
        component.previousDialog.showModal();
        component.previousDialog = null;
    }
};

Dialog.prototype.open = function () {
    var component = this;
    component.hideVisibleDialogs();
    component.element.showModal();

    // prevent scroll bar when the content of the window body
    // is larger then the dialog
    $body.addClass(OPEN_DIALOG_CLASS);
    $window.on('popstate.' + component.id, function () {
        component.close();
        component.enableScroll();
    });

    component.disableScroll();
};

Dialog.prototype.close = function () {
    var component = this;

    if (component.element.open && isFunction(component.element.close)) {
        component.element.close();
        component.enableScroll();
    }
    $body.removeClass(OPEN_DIALOG_CLASS);
    $window.off('popstate.' + component.id);
    component.enableScroll();
    component.restorePreviousDialogs();
};

/**
 * Disables scroll-events, including touch devices.
 */
Dialog.prototype.disableScroll = function () {
    if (window.addEventListener) {
        // older FF
        window.addEventListener('DOMMouseScroll', preventDefaultBehavior, false);
    }

    window.onwheel = preventDefaultBehavior; // modern standard
    window.onmousewheel = document.onmousewheel = preventDefaultBehavior; // older browsers, IE
    window.ontouchmove = preventDefaultBehavior; // mobile
    document.onkeydown = preventScrollBehavior;
};

/**
 * Enables scroll-events, including touch devices.
 */
Dialog.prototype.enableScroll = function () {
    if (window.removeEventListener) {
        // older FF
        window.removeEventListener('DOMMouseScroll', preventDefaultBehavior, false);
    }

    window.onmousewheel = document.onmousewheel = null;
    window.onwheel = null;
    window.ontouchmove = null;
    document.onkeydown = null;
};

/**
 * Prevent the default behavior.
 * If the parameter is an event, prevent its default behavior.
 * If not, the returnValue is false.
 * @param event     event that should have its default behavior prevented.
 */
function preventDefaultBehavior(event) {
    event = event || window.event;
    if (event.preventDefault) {
        event.preventDefault();
    }
    event.returnValue = false;
}

/**
 * Prevent any scrolling behavior using keyboard keys.
 * @param event     event that should have its default behavior prevented.
 */
function preventScrollBehavior(event) {
    if (KEY_CODES_PREVENT_SCROLL.indexOf(event.keyCode) !== -1) {
        event.preventDefault();
    }
}

// turn all elements with the default selector into components
$(COMPONENT_SELECTOR).each(function (index, element) {
    return new Dialog(element);
});

},{"../app-spinner/app-spinner":201,"../async-form/async-form":207,"../social-login/social-login":318,"dialog-polyfill":1,"jquery":"jquery","lodash/isFunction":173}],228:[function(require,module,exports){
// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

// explicitly inject dependencies (alphabetically), only those needed

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ajax = require('../ajax/ajax');

// what does this module expose?
exports.default = ErrorBar;

// component configuration

var COMPONENT_SELECTOR = '[data-error-bar]';
var NOTIFICATION_SELECTOR = '[data-notification]';

var IS_ACTIVE_CLASS = 'is-active';
var FLASH_ANIMATION_CLASS = 'error-bar-flash';

function ErrorBar(element) {
    var component = this;
    component.element = element;
    component.$element = (0, _jquery2.default)(element);
    component.$notifications = this.$element.find(NOTIFICATION_SELECTOR);

    ajax.onError(function () {
        return component.show();
    });
    ajax.onAbort(function () {
        return component.hide();
    });
    ajax.onSuccess(function () {
        return component.hide();
    });
    component.$notifications.on('close', function () {
        return component.hide();
    });
}

ErrorBar.prototype.hide = function () {
    var component = this;
    component.$element.removeClass(IS_ACTIVE_CLASS);
};

ErrorBar.prototype.show = function () {
    var component = this;
    if (component.$element.hasClass(IS_ACTIVE_CLASS)) {
        component.$element.removeClass(FLASH_ANIMATION_CLASS);
        // trigger element reflow
        // this enables us to restart the animation
        // source: https://css-tricks.com/restart-css-animation/
        component.element.offsetWidth;
        component.$element.addClass(FLASH_ANIMATION_CLASS);
    } else {
        component.$notifications.trigger('show');
        component.$element.addClass(IS_ACTIVE_CLASS);
    }
};

// turn all elements with the default selector into components
(0, _jquery2.default)(COMPONENT_SELECTOR).get().map(function (element) {
    return new ErrorBar(element);
});

},{"../ajax/ajax":198,"jquery":"jquery"}],229:[function(require,module,exports){
'use strict';

// dependencies (alphabetically)

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

var _classObservable = require('../class-observable/class-observable');

var _classObservable2 = _interopRequireDefault(_classObservable);

var _serviceController = require('../service-controller/service-controller');

var _serviceController2 = _interopRequireDefault(_serviceController);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

module.exports = Expandible;

// component configuration
var COMPONENT_ATTR = 'data-expandible';
var COMPONENT_SELECTOR = '[' + COMPONENT_ATTR + ']';
var HANDLE_ATTR = 'data-expandible-handle';
var HANDLE_SELECTOR = '[' + HANDLE_ATTR + ']';
var ENHANCED_CLASS = 'is-expandible';
var EXPANDED_CLASS = 'is-expanded';
var ON_BLUR_ATTR = 'data-expandible-on-blur';
var $body = (0, _jquery2.default)('body');
var OPENED_EVENT = 'expandibleopened';
var CLOSED_EVENT = 'expandibleclosed';
var TOGGLE_EVENT = 'expandibletoggle';

function Expandible(element) {
    var _this = this;

    // define relevant dom elements and initial value
    this.$element = (0, _jquery2.default)(element);
    this.expandibleId = this.$element.attr(COMPONENT_ATTR) || false;
    this.expandibleOnBlur = this.$element.attr(ON_BLUR_ATTR) || false;
    this.isExpanded = this.$element.hasClass(EXPANDED_CLASS);
    this.$handlesOutside = (0, _jquery2.default)();
    this.hasNestedExpandible = this.$element.find(COMPONENT_SELECTOR).length > 0;
    this.hasHandleOutside = !!this.expandibleId;
    this.observers = new _classObservable2.default();

    //To be called by the observers (creator functions usually)
    this.onChange = function (observer) {
        _this.observers.listen(observer);
    };

    // if component has an ID, look for handles outside the component with that ID
    if (this.expandibleId) {
        this.$handlesOutside = $body.find('[' + HANDLE_ATTR + '="' + this.expandibleId + '"]');
    }

    if (this.expandibleOnBlur) {
        $body.on('click', function (event) {
            var targetIsHandle = _this.$handlesOutside.is(event.target);
            var targetIsExpandible = _this.$element.is(event.target);
            var targetIsInsideExpandible = (0, _jquery2.default)(event.target).closest(_this.$element).length !== 0;

            if (_this.isExpanded && !targetIsHandle && !targetIsExpandible && !targetIsInsideExpandible) {
                _this.toggleExpand();
            }
        });
    }

    // enhance, set initial state, and toggle when handles are triggered
    this.$element.addClass(ENHANCED_CLASS);
    this.toggleExpand(this.isExpanded);

    // only toggle expand on click element if it doesn't have nested expandible components
    if (!this.hasNestedExpandible && !this.hasHandleOutside) {

        this.$element.on('click', HANDLE_SELECTOR, function () {
            return _this.toggleExpand();
        });
    }
    if (this.expandibleId) {
        $body.on('click', '[' + HANDLE_ATTR + '="' + this.expandibleId + '"]', function () {
            return _this.toggleExpand();
        });
    }

    this.$element.on(TOGGLE_EVENT, function (e, isExpanded) {
        return _this.toggleExpand(isExpanded);
    });
}

/**
 * Toggle (expand / collapse) the expand state by adding / removing the expanded class.
 * @param {Boolean} [isExpanded]    Set true to force component to expand (optional).
 * @returns {Boolean}               True if component is expanded.
 */
Expandible.prototype.toggleExpand = function (isExpanded) {
    this.isExpanded = isExpanded !== undefined ? isExpanded : !this.isExpanded;
    this.$element.toggleClass(EXPANDED_CLASS, this.isExpanded);
    this.$handlesOutside.toggleClass(EXPANDED_CLASS, this.isExpanded);
    this.$element.attr('aria-expanded', this.isExpanded);
    this.$element.trigger(this.isExpanded ? OPENED_EVENT : CLOSED_EVENT);
    this.observers.notify(this.isExpanded);
    return this.isExpanded;
};

Expandible.getSelector = function () {
    return COMPONENT_SELECTOR;
};
Expandible.initialize = function () {
    return _serviceController2.default.getAllInstances(Expandible);
};
Expandible.initialize();

},{"../class-observable/class-observable":218,"../service-controller/service-controller":317,"jquery":"jquery"}],230:[function(require,module,exports){
// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

// explicitly inject dependencies (alphabetically), only those needed

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// what does this module expose?
exports.default = FeaturedHomes;


var COMPONENT_SELECTOR = '[data-featured-homes]';
var UPDATING_CLASS = 'is-updating';
var HEIGHT_MARGIN = 40;

function FeaturedHomes(element) {
    var component = this;
    component.$element = (0, _jquery2.default)(element);
    component.$element.css({ 'min-height': Math.ceil(component.$element.outerHeight()) + HEIGHT_MARGIN + 'px' });

    (0, _jquery2.default)(document).on('resultsUpdating', function () {
        return component.isUpdating();
    }).on('resultsUpdated', function () {
        return component.isUpdated();
    });
}

FeaturedHomes.prototype.isUpdating = function () {
    this.$element.addClass(UPDATING_CLASS);
};

FeaturedHomes.prototype.isUpdated = function () {
    var component = this;
    setTimeout(function () {
        component.$element.removeClass(UPDATING_CLASS);
    }, 250);
};

// turn all elements with the default selector into components
(0, _jquery2.default)(COMPONENT_SELECTOR).each(function (index, element) {
    return new FeaturedHomes(element);
});

},{"jquery":"jquery"}],231:[function(require,module,exports){
// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

// explicitly inject dependencies (alphabetically), only those needed

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// what does this module expose?
exports.default = FilterCount;

// component configuration

var COMPONENT_SELECTOR = '[data-filter-count]';
var COUNT_SELECTOR = '[data-filter-count-number]';
var LABEL_SELECTOR = '[data-filter-count-label]';
var LABEL_ONE_ATTR = 'data-filter-count-label-one';
var LABEL_MULTIPLE_ATTR = 'data-filter-count-label-multiple';

function FilterCount(element) {
    var component = this;
    component.$element = (0, _jquery2.default)(element);
    component.$count = component.$element.find(COUNT_SELECTOR);
    component.initialCount = component.getInitialCount();
    component.$label = component.$element.find(LABEL_SELECTOR);
    component.labelCountOne = component.$label.attr(LABEL_ONE_ATTR);
    component.labelCountMultiple = component.$label.attr(LABEL_MULTIPLE_ATTR);
    component.$form = component.$element.closest('form');

    (0, _jquery2.default)(document).on('setfiltercount', function (event, eventArgs) {
        return component.setCount(eventArgs.amount);
    });
}

FilterCount.prototype.getLabel = function (count) {
    var component = this;
    return count === 1 ? component.labelCountOne : component.labelCountMultiple;
};

FilterCount.prototype.setCount = function (int) {
    var component = this;

    if (component.initialCount > 0) {
        // When total number of filters has been rendered, reset initial count so on subsequent renderings it will be updated correctly.
        if (component.initialCount === int) {
            component.initialCount = 0;
        }
    } else {
        component.count = int || 0;
        component.$count.text(component.count);
        component.$label.text(component.getLabel(component.count));
    }
};

FilterCount.prototype.getInitialCount = function () {
    var component = this;
    return component.$count.length > 0 ? parseInt(component.$count[0].innerText, 10) : 0;
};

// turn all elements with the default selector into components
(0, _jquery2.default)(COMPONENT_SELECTOR).each(function (index, element) {
    return new FilterCount(element);
});

},{"jquery":"jquery"}],232:[function(require,module,exports){
'use strict';

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

var _appliedFilters = require('../applied-filters/applied-filters');

var _appliedFilters2 = _interopRequireDefault(_appliedFilters);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ToggleAllInputs = require('../filter-flyout/toggle-all-inputs');

module.exports = FilterFlyout;

var COMPONENT_SELECTOR = '[data-filter-flyout]';
var SELECTED_ITEMS_SELECTOR = '[data-flyout-selected-options]';
var SELECTED_ITEM_SELECTOR = '[data-flyout-selected-option]';
var SELECTED_ITEM_TEMPLATE = '[filter-flyout-selected-option-template]';
var SELECTED_FILTER_ID = 'data-filter-id';
var SELECTED_FILTER_GROUP_ID = 'data-filter-group-id';
var SELECTED_FILTER_GROUP_NAME = 'data-filter-group-name';
var FILTER_GROUPNAME_SELECTOR = '[' + SELECTED_FILTER_GROUP_NAME + ']';
var FLYOUT_BUTTON_SELECTOR = '[data-filter-open-flyout]';
var FLYOUT_SELECTOR = '[data-filter-flyout-panel]';
var FLYOUT_PREVIEW_OPTIONS_SELECTOR = '[data-filter-flyout-preview-options]';
var FLYOUT_BACKDROP_SELECTOR = '[data-filter-flyout-backdrop]';
var FLYOUT_CLOSE_SELECTOR = '[data-filter-close-flyout]';
var FLYOUT_CLOSED_EVENT = 'filterflyoutclosed';
var FLYOUT_CLOSE_EVENT = 'filterflyoutclose';
var FLYOUT_OPENED_EVENT = 'filterflyoutopened';
var FILTER_ITEM_SELECTOR = '[data-flyout-item]';
var FLYOUT_ITEM_LABEL_ATTR = 'data-filter-flyout-option-label';
var FLYOUT_ITEM_LABEL_SELECTOR = '[' + FLYOUT_ITEM_LABEL_ATTR + ']';
var PARENT_DESELECT_EVENT = 'deselect-filter-item';
var PARENT_SELECTOR = '[data-nested-filter]';
var TOGGLE_ALL_INPUT_SELECTOR = '[data-toggle-all-inputs]';
var ZOEK_OP_KAART_SELECTOR = 'main > .zoek-op-kaart';
var ZOEK_OP_KAART_MAP_SELECTOR = '.zoek-op-kaart__map';

var RANGE_MIN = '[data-range-min]';
var RANGE_MAX = '[data-range-max]';
var RANGE_SUBMIT = '[data-range-submit]';
var RANGE_TEMPLATE = '[data-range-label-template]';
var RANGE_SELECT_EVENT = 'rangeselected';
var RANGE_DESELECT_EVENT = 'rangedeselected';
var RANGE_INITIALIZED_EVENT = 'rangeinitialized';
var CHANGE_BUURT_EVENT = 'changeBuurt';
var CHANGE_RADIUS_EVENT = 'changeRadius';

var FLYOUT_OPEN_CLASS = 'is-open';
var FLYOUT_BOTTOM_CLASS = 'is-bottom';
var FLYOUT_HAS_SELECTED_FILTER = 'has-selected-filter';
var FLYOUT_BODY_OPEN_CLASS = 'zoek-op-kaart-flyout-open';
var LABEL_CLASS = '.label-text';
var LABEL_ADD_GROUP_NAME_SELECTOR = '[data-filter-flyout-add-groupname-to-label]';
var FILTER_ADD_GROUP_NAME_SELECTOR = '[data-filter-flyout-add-groupname-to-filter]';

var KEY_CODE = {
    ENTER: 13,
    ESC: 27,
    SPACE: 32
};

var $body = (0, _jquery2.default)('body');
var $kaart = (0, _jquery2.default)(ZOEK_OP_KAART_MAP_SELECTOR);

function FilterFlyout(element, reinitiate) {
    var component = this;

    component.uniqueId = null;
    component.bindToElements(element, reinitiate);

    component.bindEvents();

    component.setInitialSelectedFilters();

    (0, _jquery2.default)(window).on('sidebar-refresh', function () {
        if (component.uniqueId == null) return;
        var $myNewElement = (0, _jquery2.default)(COMPONENT_SELECTOR + '[data-unique="' + component.uniqueId + '"]');
        component.bindToElements($myNewElement, false);
        component.bindEvents();
    });
}

FilterFlyout.prototype.bindToElements = function (element, reinitiate) {
    var component = this;

    component.$element = (0, _jquery2.default)(element);
    component.$filterOption = component.$element.find(FILTER_ITEM_SELECTOR);
    component.$filterOptionContainer = component.$element.find(FLYOUT_ITEM_LABEL_SELECTOR);
    component.$selectedOptions = component.$element.find(SELECTED_ITEMS_SELECTOR);
    component.selectedOptionsTemplate = component.$element.find(SELECTED_ITEM_TEMPLATE).html();
    component.$flyout = component.$element.find(FLYOUT_SELECTOR);
    component.$flyoutButton = component.$element.find(FLYOUT_BUTTON_SELECTOR);
    component.$flyoutBackdrop = component.$element.find(FLYOUT_BACKDROP_SELECTOR);
    component.$flyoutClose = component.$element.find(FLYOUT_CLOSE_SELECTOR);
    component.flyoutIsOpen = false;
    component.$previewOptions = component.$element.find(FLYOUT_PREVIEW_OPTIONS_SELECTOR);
    component.$parentFilter = component.$element.closest(PARENT_SELECTOR);
    component.hasParentFilter = component.$parentFilter.length > 0;
    component.$form = component.$element.closest('form');
    component.$rangeInputSubmit = component.$element.find(RANGE_SUBMIT);
    component.$rangeMin = component.$element.find(RANGE_MIN);
    component.$rangeMax = component.$element.find(RANGE_MAX);
    component.rangeTemplate = component.$element.find(RANGE_TEMPLATE).data('range-label-template');

    if (reinitiate) {
        // this filter flyout was already initiated; make sure there are no available filters left.
        var groupName = null;
        var groupList = [];

        component.$element.find(FILTER_GROUPNAME_SELECTOR).each(function () {
            var $element = (0, _jquery2.default)(this);
            groupName = $element.attr(SELECTED_FILTER_GROUP_NAME).toLowerCase();
            if (groupList.indexOf(groupName) < 0) {
                groupList.push(groupName);
                _appliedFilters2.default.remove(groupName);
            }
        });
    }

    if (component.hasParentFilter) {
        component.$parentFilter.on(PARENT_DESELECT_EVENT, function () {
            var numberOfActiveSubfilters = component.$selectedOptions.find('li').length;
            if (numberOfActiveSubfilters > 0) {
                component.$filterOption.each(function () {
                    var $filterOption = (0, _jquery2.default)(this);
                    component.removeFilterLabel($filterOption);
                });
                component.$filterOption.prop('checked', false);
                component.$selectedOptions.empty();
            }
        });
    }

    if (component.$element.find(TOGGLE_ALL_INPUT_SELECTOR)) {
        var toggleAllInputs = component.$element.find(TOGGLE_ALL_INPUT_SELECTOR)[0];
        component.toggleAllInputs = new ToggleAllInputs(toggleAllInputs);
    }

    // remember uniqueId (first time)
    if (component.uniqueId == null) {
        component.uniqueId = component.$element.data('unique');
    }
};

FilterFlyout.prototype.bindEvents = function () {
    var component = this;

    component.$rangeInputSubmit.on('click', function () {
        component.sendRangeEvent();
        component.closeFlyout();
    });
    component.$element.on('sendrangeinputvalues', function () {
        component.sendRangeEvent();
    });
    component.$element.on(RANGE_DESELECT_EVENT, function () {
        component.$rangeMin.val('');
        component.$rangeMax.val('');
    });
    component.$flyoutButton.on('click', function () {
        component.toggleFlyout();
    });
    component.$flyoutBackdrop.on('click', function () {
        component.closeFlyout();
    });
    component.$flyoutClose.on('click', function () {
        component.closeFlyout();
    });
    component.$element.on('resetfilter', function () {
        component.$filterOption.each(function (index, filterOption) {
            var $filterOption = (0, _jquery2.default)(filterOption);
            var isChecked = $filterOption[0].checked;
            if (isChecked) {
                component.removeFilterLabel($filterOption);
            }
        });
    });
    component.$element.on('change', FILTER_ITEM_SELECTOR, function () {
        var $filterOption = (0, _jquery2.default)(this);
        var isChecked = $filterOption[0].checked;
        var numFiltersSelected = null;

        if (isChecked) {
            numFiltersSelected = component.addSelectedFilterLabel($filterOption);
        } else {
            // filter isn't checked
            numFiltersSelected = component.removeSelectedFilter($filterOption);
        }
        var isBuurtFilter = $filterOption.parents('[data-instant-search-output=wijkNaamFilter]').length > 0;
        if (isBuurtFilter) {
            (0, _jquery2.default)(document).trigger(CHANGE_BUURT_EVENT, { selectedFilterCount: numFiltersSelected });
        }
    });
    component.$selectedOptions.on('click', SELECTED_ITEM_SELECTOR, function (e) {
        e.preventDefault();
        e.stopPropagation();
        var $li = (0, _jquery2.default)(this);
        var selectedOptionId = $li.attr(SELECTED_FILTER_ID);
        var $input = component.$flyout.find('input[id="' + selectedOptionId + '"]');
        $input.prop('checked', false);
        $input.trigger('change');
        $li.remove();
    });
    component.$element.on(FLYOUT_CLOSE_EVENT, function () {
        component.closeFlyout();
    });
    component.$filterOptionContainer.on('changed', function () {
        if (component.previewTimer) {
            clearTimeout(component.previewTimer);
        }
        component.previewTimer = setTimeout(function () {
            component.setPreviewOptions();
        }, 300);
    });
    (0, _jquery2.default)(document).on(CHANGE_RADIUS_EVENT, function (event, data) {
        var buurtFilterVisible = data.selectedRadius == 0;
        component.togggleBuurtFilter(buurtFilterVisible);
    });
};

FilterFlyout.prototype.sendRangeEvent = function () {
    var component = this;
    var data = {};

    data.min = component.$rangeMin.val();
    data.max = component.$rangeMax.val();

    if (data.max || data.min) {
        component.$element.trigger(RANGE_SELECT_EVENT, data);
    }
};

FilterFlyout.prototype.sendInitializedRangeEvent = function () {
    var component = this;
    var data = {};

    data.min = component.$rangeMin.val();
    data.max = component.$rangeMax.val();

    if (data.max || data.min) {
        component.$element.trigger(RANGE_INITIALIZED_EVENT, data);
    }
};

FilterFlyout.prototype.setInitialSelectedFilters = function () {
    var component = this;

    if (component.$filterOption.length > 0) {
        component.$filterOption.each(function (index, filterOption) {
            var $filterOption = (0, _jquery2.default)(filterOption);
            var isChecked = $filterOption[0].checked;

            if (isChecked) {
                component.addSelectedFilterLabel($filterOption);
            }
        });
    } else {
        setTimeout(function () {
            component.sendInitializedRangeEvent();
        }, 125);
    }
};

FilterFlyout.prototype.toggleFlyout = function () {
    var component = this;
    component.flyoutIsOpen = !component.flyoutIsOpen;
    if (component.flyoutIsOpen) {
        component.openFlyout();
    } else {
        component.closeFlyout();
    }
};

FilterFlyout.prototype.openFlyout = function () {
    var component = this;
    component.flyoutIsOpen = true;

    var zoekopkaart = document.querySelector(ZOEK_OP_KAART_SELECTOR);
    if (zoekopkaart !== null) {
        $body.addClass(FLYOUT_BODY_OPEN_CLASS);
    }

    component.$flyout.addClass(FLYOUT_OPEN_CLASS).attr('aria-expanded', component.flyoutIsOpen);
    if (component.$flyout.hasClass(FLYOUT_BOTTOM_CLASS)) {
        component.$flyout.toggleClass(FLYOUT_BOTTOM_CLASS);
    }
    var flyoutRect = component.$flyout[0].getBoundingClientRect();
    if (flyoutRect.top + flyoutRect.height > $kaart.height()) {
        component.$flyout.addClass(FLYOUT_BOTTOM_CLASS);
    }
    component.$flyoutButton.addClass(FLYOUT_OPEN_CLASS);
    component.$element.trigger(FLYOUT_OPENED_EVENT);
    component.$element.on('keydown', function (e) {
        if (e.keyCode === KEY_CODE.ENTER) {
            e.preventDefault();
            component.closeFLyoutOnEnter(e, e.keyCode);
        }
    });

    component.$element.on('keypress', function (e) {
        var pressedKey = e.keyCode || e.charCode;
        var key = String.fromCharCode(pressedKey);
        if (!/[0-9]/.test(key) && pressedKey >= KEY_CODE.SPACE) {
            e.preventDefault();
        }
    });

    (0, _jquery2.default)(document).on('keydown.closeFlyout', function (e) {
        if (e.keyCode === KEY_CODE.ESC) {
            component.closeFlyout();
        }
    });
};

FilterFlyout.prototype.closeFlyout = function () {
    $body.removeClass(FLYOUT_BODY_OPEN_CLASS);
    var component = this;
    component.flyoutIsOpen = false;
    component.$flyout.removeClass(FLYOUT_OPEN_CLASS).attr('aria-expanded', component.flyoutIsOpen);
    component.$flyoutButton.removeClass(FLYOUT_OPEN_CLASS);
    component.$element.trigger(FLYOUT_CLOSED_EVENT);
    component.$element.off('keydown');
    (0, _jquery2.default)(document).off('keydown.closeFlyout');
};

FilterFlyout.prototype.closeFLyoutOnEnter = function () {
    $body.removeClass(FLYOUT_BODY_OPEN_CLASS);
    var component = this;
    component.sendRangeEvent();
    component.$element.trigger(FLYOUT_CLOSE_EVENT);
    component.$element.off('keydown');
};

FilterFlyout.prototype.addSelectedFilterLabel = function ($filterOption) {
    var component = this;
    var isRadioGroup = component.filterIsRadioButton($filterOption);
    var filterLabeltext = $filterOption.next(LABEL_CLASS)[0].childNodes[0].nodeValue;
    var filterGroupId = $filterOption.attr(SELECTED_FILTER_GROUP_ID);
    var filterGroupName = $filterOption.attr(SELECTED_FILTER_GROUP_NAME);
    var html = component.selectedOptionsTemplate;
    var pattern = new RegExp('{id}', 'g');
    var appliedFilter = {
        filterGroupId: $filterOption[0].id,
        filterGroupName: filterGroupName.toLowerCase(),
        filterName: filterLabeltext,
        labelText: filterLabeltext
    };
    if (isRadioGroup) {
        component.removeSelectedFilter($filterOption);
        appliedFilter.filterGroupId = filterGroupId;
    }
    if ($filterOption.is(LABEL_ADD_GROUP_NAME_SELECTOR)) {
        filterLabeltext = filterLabeltext + ' (' + filterGroupName.toLowerCase() + ')';
    }
    if ($filterOption.is(FILTER_ADD_GROUP_NAME_SELECTOR)) {
        appliedFilter.labelText = appliedFilter.labelText + ' (' + appliedFilter.filterGroupName + ')';
    }

    html = html.replace(pattern, $filterOption[0].id).replace(new RegExp('({label})', 'gm'), filterLabeltext).replace(new RegExp('({group-id})', 'gm'), filterGroupId);
    component.$selectedOptions.append(html);
    var numFiltersSelected = component.determineFilterSelected();

    _appliedFilters2.default.add(appliedFilter, function () {
        component.removeFilterLabel($filterOption, component);
    }, isRadioGroup);

    return numFiltersSelected;
};

/**
 * remove the selected filter
 * @param {Obj} $filterOption
 */
FilterFlyout.prototype.removeSelectedFilter = function ($filterOption) {
    var component = this;
    var $filters = null;
    var filterOptionId = $filterOption[0].id;

    if (component.filterIsRadioButton($filterOption)) {
        filterOptionId = $filterOption.attr(SELECTED_FILTER_GROUP_ID);
        $filters = component.$selectedOptions.find('[' + SELECTED_FILTER_GROUP_ID + '="' + filterOptionId + '"]');
    } else {
        // filter is checkbox
        $filters = component.$selectedOptions.find('[' + SELECTED_FILTER_ID + '="' + filterOptionId + '"]');
    }

    if ($filters.length > 0) {
        $filters.remove();
        component.determineFilterSelected();
        _appliedFilters2.default.remove(filterOptionId);
    }
    return component.determineFilterSelected();
};

/**
 * remove the selected filter and fires the trigger
 * @param $filterOption
 * @param {component} filterFlyoutComponent; the filter flyout component it self
 */
FilterFlyout.prototype.removeFilterLabel = function ($filterOption, filterFlyoutComponent) {
    var component = filterFlyoutComponent || this;

    component.removeSelectedFilter($filterOption);
    $filterOption.prop('checked', false);
    $filterOption.trigger('change');
};

FilterFlyout.prototype.filterIsRadioButton = function ($filter) {
    return $filter.attr('type') === 'radio';
};

FilterFlyout.prototype.determineFilterSelected = function () {
    var component = this;
    var selectedFilters = component.$element.find(SELECTED_ITEM_SELECTOR).length;
    if (selectedFilters > 0) {
        component.$element.addClass(FLYOUT_HAS_SELECTED_FILTER);
    } else {
        component.$element.removeClass(FLYOUT_HAS_SELECTED_FILTER);
    }
    return selectedFilters;
};

FilterFlyout.prototype.setPreviewOptions = function () {
    var component = this;
    var optionList = [];

    component.$filterOptionContainer.each(function () {
        var $element = (0, _jquery2.default)(this);
        if (!$element.hasClass('is-disabled')) {
            optionList.push($element.attr(FLYOUT_ITEM_LABEL_ATTR));
        }
    });
    component.$previewOptions.text(optionList.join(', '));
};

FilterFlyout.prototype.togggleBuurtFilter = function (newState) {
    var $buurtFilter = $body.find('[data-instant-search-output="wijkNaamFilter"]');
    if ($buurtFilter.length == 0) return;

    if (newState) {
        (0, _jquery2.default)('.filter-flyout', $buurtFilter).removeClass('is-disabled');
    } else {
        (0, _jquery2.default)('.filter-flyout-fieldset input[type=checkbox]:checked', $buurtFilter).click();
        (0, _jquery2.default)('.filter-flyout', $buurtFilter).addClass('is-disabled');
    }
};

(0, _jquery2.default)(COMPONENT_SELECTOR).each(function (index, element) {
    return new FilterFlyout(element, false);
});

},{"../applied-filters/applied-filters":204,"../filter-flyout/toggle-all-inputs":233,"jquery":"jquery"}],233:[function(require,module,exports){
'use strict';

var $ = require('jquery');

module.exports = ToggleAllInputs;

var ALL_INPUTS_HANDLER_SELECTOR = '[data-toggle-all-inputs-handle]';

function ToggleAllInputs(element) {
    var component = this;
    component.$element = $(element);

    component.$inputs = component.$element.find('input').not(ALL_INPUTS_HANDLER_SELECTOR);
    component.$handle = component.$element.find(ALL_INPUTS_HANDLER_SELECTOR);
    component.totalAmountInputs = component.$inputs.length;

    component.$inputs.on('change', function () {
        var amountCheckedInputs = component.$element.find('input:checked').not(ALL_INPUTS_HANDLER_SELECTOR).length;
        var isChecked = amountCheckedInputs === component.totalAmountInputs;
        component.$handle.prop('checked', isChecked);
    });

    component.$handle.on('change', function () {
        var isChecked = component.$handle.is(':checked');
        component.changeCheckedState(isChecked);
    });

    component.initializeHandler();
}

ToggleAllInputs.prototype.initializeHandler = function () {
    var component = this;
    var isChecked = component.$handle.is(':checked');
    if (isChecked) {
        component.changeCheckedState(isChecked);
    }
};

ToggleAllInputs.prototype.changeCheckedState = function (isChecked) {
    var component = this;

    component.$inputs.each(function () {
        var $input = $(this);

        if (isChecked === true && !$input.is(':checked')) {
            $input.prop('checked', isChecked);
            $input.trigger('change');
        } else if (isChecked === false && $input.is(':checked')) {
            $input.prop('checked', isChecked);
            $input.trigger('change');
        }
    });
};

},{"jquery":"jquery"}],234:[function(require,module,exports){
// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

// explicitly inject dependencies (alphabetically), only those needed

var $ = require('jquery');

// what does this module expose?
module.exports = FilterReset;

// component configuration
var COMPONENT_SELECTOR = '[data-filter-reset]';
var RESETTABLE_ELEMENTS_SELECTOR = '[data-resettable]';
var IS_ENHANCED_CLASS = 'is-enhanced';

function FilterReset(element) {
    var component = this;
    component.$element = $(element);
    component.$element.addClass(IS_ENHANCED_CLASS);
    component.$form = component.$element.closest('form');
    component.$resetElements = component.$form.find(RESETTABLE_ELEMENTS_SELECTOR);

    component.$element.on('click', function (event) {
        event.preventDefault();
        component.$resetElements.trigger('resetfilter');
    });

    $(document).on('setfiltercount', function (event, eventArgs) {
        var isDisabled = true;
        if (eventArgs.amount > 0) {
            isDisabled = false;
        }
        component.$element.prop('disabled', isDisabled);
    });
}

// turn all elements with the default selector into components
$(COMPONENT_SELECTOR).each(function (index, element) {
    return new FilterReset(element);
});

},{"jquery":"jquery"}],235:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = FormAlertOnLeave;


var COMPONENT_DATA_ATTR = 'form-alert-on-leave';
var COMPONENT_SELECTOR = '[data-' + COMPONENT_DATA_ATTR + ']';

function FormAlertOnLeave(element) {
    var component = this;
    component.$element = (0, _jquery2.default)(element);
    component.message = component.$element.data(COMPONENT_DATA_ATTR);
    component.isDirty = false;

    component.bindEvents();
}

FormAlertOnLeave.prototype.bindEvents = function () {
    var component = this;

    component.$element.find('input,textarea,select').on('change', function () {
        return component.setDirty(true);
    });
    component.$element.on('submit', function () {
        return component.setDirty(false);
    });
    (0, _jquery2.default)(window).on('beforeunload', function () {
        return component.pageLeaveHandler();
    });
};

FormAlertOnLeave.prototype.setDirty = function (isDirty) {
    var component = this;

    component.isDirty = isDirty;
};

FormAlertOnLeave.prototype.pageLeaveHandler = function () {
    var component = this;

    if (component.isDirty) {
        return confirm(component.message);
    }
};

(0, _jquery2.default)(COMPONENT_SELECTOR).each(function (index, element) {
    return new FormAlertOnLeave(element);
});

},{"jquery":"jquery"}],236:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = FormMinChars;


var COMPONENT_SELECTOR = '[data-form-textarea-countdown]';
var INPUT_SELECTOR = '[data-form-input]';
var ICON_SELECTOR = '[data-icon]';
var COUNTDOWN_SELECTOR = '[data-countdown]';
var ZERO_INPUT_TEXT_DATA = 'data-zero-input-text';
var MORE_INPUT_PRE_TEXT_DATA = 'data-more-input-pre-text';
var MORE_INPUT_POST_TEXT_DATA = 'data-more-input-post-text';
var IS_CORRECT_CLASS = 'is-correct';
var IS_HIDDEN_CLASS = 'is-hidden';
var MIN_LENGTH_ATTR = 'minlength';
var EVENTS_TO_LISTEN = 'input propertychange';

function FormMinChars(element) {
    var component = this;

    component.$element = (0, _jquery2.default)(element);
    component.$textarea = component.$element.find(INPUT_SELECTOR);
    component.$countDown = component.$element.find(COUNTDOWN_SELECTOR);
    component.$icon = component.$element.find(ICON_SELECTOR);
    component.minChars = component.$textarea.attr(MIN_LENGTH_ATTR);
    component.zeroInputText = component.$textarea.attr(ZERO_INPUT_TEXT_DATA);
    component.moreInputPreText = component.$textarea.attr(MORE_INPUT_PRE_TEXT_DATA);
    component.moreInputPostText = component.$textarea.attr(MORE_INPUT_POST_TEXT_DATA);

    component.bindEvents();
    component.updateCountDown();
}

FormMinChars.prototype.bindEvents = function () {
    var component = this;

    component.$textarea.bind(EVENTS_TO_LISTEN, function () {
        return component.updateCountDown();
    });
};

FormMinChars.prototype.updateCountDown = function () {
    var component = this;

    var currentChars = component.$textarea.val().length;

    if (currentChars >= component.minChars) {
        component.$icon.removeClass(IS_HIDDEN_CLASS);
        component.$countDown.addClass(IS_CORRECT_CLASS);
        component.$countDown.html('');
    } else {
        var displayedText = currentChars > 0 ? '' + component.moreInputPreText + (component.minChars - currentChars) + component.moreInputPostText : component.zeroInputText;
        component.$icon.addClass(IS_HIDDEN_CLASS);
        component.$countDown.removeClass(IS_CORRECT_CLASS);
        component.$countDown.html(displayedText);
    }
};

(0, _jquery2.default)(COMPONENT_SELECTOR).each(function (i, element) {
    return new FormMinChars(element);
});

},{"jquery":"jquery"}],237:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = FormValidation;


var COMPONENT_SELECTOR = '[data-form-validation]';
var ERROR_INPUT_CLASS = 'input-validation-error';
var ERROR_INPUT_SELECTOR = '.' + ERROR_INPUT_CLASS;
var ERROR_MESSAGE_SELECTOR = '.form-message--error';
var EVENTS_TRIGGERS = 'keypress click';

function FormValidation(element) {
    var component = this;
    component.$element = (0, _jquery2.default)(element);
    component.$errorInputs = component.$element.find(ERROR_INPUT_SELECTOR);

    component.bindEvents();
    component.scrollToFirstError();
}

FormValidation.prototype.scrollToFirstError = function () {
    var component = this;

    if (component.$errorInputs.length) {
        component.$errorInputs[0].scrollIntoView();
    }
};

FormValidation.prototype.bindEvents = function () {
    var component = this;

    component.$errorInputs.on(EVENTS_TRIGGERS, function (e) {
        return component.errorHandler(e);
    });
};

FormValidation.prototype.rebindEvents = function () {
    var component = this;

    component.$errorInputs.off(EVENTS_TRIGGERS);
    component.$errorInputs = component.$element.find(ERROR_INPUT_SELECTOR);
    component.bindEvents();
};

FormValidation.prototype.errorHandler = function (event) {
    var component = this;
    var $eventTrigger = (0, _jquery2.default)(event.target);
    var $errorInput = FormValidation.findExpanding($eventTrigger, ERROR_INPUT_SELECTOR);
    var $errorMessage = FormValidation.findExpanding($errorInput, ERROR_MESSAGE_SELECTOR);

    $errorMessage.remove(); //remove error message
    $errorInput.removeClass(ERROR_INPUT_CLASS); //remove red border in input
    component.rebindEvents();
};

FormValidation.findExpanding = function ($element, toFind) {
    var elementToFind = $element.find(toFind).addBack(toFind);

    if (elementToFind.length) {
        return elementToFind;
    } else {
        var parent = $element.parent();
        if (parent.length) {
            return FormValidation.findExpanding(parent, toFind);
        } else {
            return parent;
        }
    }
};

(0, _jquery2.default)(COMPONENT_SELECTOR).each(function (i, element) {
    return new FormValidation(element);
});

},{"jquery":"jquery"}],238:[function(require,module,exports){
/**
 * Promisify loading the Google Maps API asynchronously.
 * This module ensures GMaps is only loaded once.
 * The `load()` method returns a promise which is resolved when GMaps is loaded.
 */
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = load;


var loadingGmaps = void 0;

/**
 * Load the Google Maps API asynchronously.
 * Returns a promise which is resolved when gmaps is loaded.
 */
function load() {
  // if we're already loading gmaps, return the promsie
  if (loadingGmaps) {
    return loadingGmaps;
  }

  var callbackName = 'onGmapsLoaded';

  /*eslint-disable new-cap */
  var loadGmaps = _jquery2.default.Deferred();
  /*eslint-enable new-cap */

  loadingGmaps = loadGmaps.promise();

  window[callbackName] = loadGmaps.resolve;

  var script = document.createElement('script');
  script.src = 'https://maps.googleapis.com/maps/api/js?client=gme-fundarealestatebv&libraries=geometry&v=3.exp&callback=' + callbackName;
  document.body.appendChild(script);

  return loadingGmaps;
}

},{"jquery":"jquery"}],239:[function(require,module,exports){
// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = Goto;

// constant definitions

var COMPONENT_SELECTOR = '[data-goto]';
var SPEED_MILLIS = 600;

function Goto(element) {
    var component = this;
    component.$element = (0, _jquery2.default)(element);

    component.bindEvents();
}

Goto.prototype.bindEvents = function () {
    var component = this;

    component.$element.click(function (e) {
        return component.goToElement(e);
    });
};

Goto.prototype.goToElement = function (event) {
    var component = this;

    event.preventDefault();

    (0, _jquery2.default)('html, body').animate({
        scrollTop: (0, _jquery2.default)(component.$element.attr('href')).offset().top
    }, SPEED_MILLIS);
};

(0, _jquery2.default)(COMPONENT_SELECTOR).each(function (index, element) {
    return new Goto(element);
});

},{"jquery":"jquery"}],240:[function(require,module,exports){
'use strict';
/* globals Highcharts */

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

var _isFunction = require('lodash/isFunction');

var _isFunction2 = _interopRequireDefault(_isFunction);

var _debounce = require('lodash/debounce');

var _debounce2 = _interopRequireDefault(_debounce);

var _loadScript = require('load-script');

var _loadScript2 = _interopRequireDefault(_loadScript);

var _thousandSeparator = require('../thousand-separator/thousand-separator');

var _thousandSeparator2 = _interopRequireDefault(_thousandSeparator);

var _appSpinner = require('../app-spinner/app-spinner');

var _appSpinner2 = _interopRequireDefault(_appSpinner);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

module.exports = Graph;

// component configuration
var COMPONENT_SELECTOR = '[data-graph]';
var ASSET_BASE_SELECTOR = 'data-graph-asset-base';
var ID_SELECTOR = 'graph-container';
var LIBRARY_URL = '/assets/vendor-highcharts.js';
var TYPE = 'area';
var TOOLTIP_SELECTOR = 'div.highcharts-tooltip'; // Specific element created by Highcharts.
var TOOLTIP_FORMAT_SELECTOR = '[data-my-house-tooltip-template]';
var TOOLTIP_PRODUCT_FORMAT_SELECTOR = '[data-my-house-tooltip-product-template]';
var TOOLTIP_PRODUCT_HANDLE = '[data-my-house-tooltip-products]';
var SPINNER_TEMPLATE_HANDLE = '[data-my-house-graph-spinner-template]';
var SPINNER_HANDLE = '[data-graph-spinner]';
var TRANSLATION_MONTHS = 'data-date-translations-months';
var TRANSLATION_SHORTMONTHS = 'data-date-translations-shortmonths';
var TRANSLATION_WEEKDAYS = 'data-date-translations-weekdays';
var TRANSLATION_FORMAT_MONTH = 'data-date-translations-month-format';
var TRANSLATION_FORMAT_DATE = 'data-date-translations-date-format';
var TRANSLATION_DAY = 'data-translations-day-label';
var THOUSAND_SEPARATOR = 'data-thousand-separator';
var YOUR_OBJECT_ID = 'YourObject';

/**
 * Constructor method, links child elements to variables for internal use
 * @param {HTMLElement} element     The HTML element to bind to.
 * @param {object} options          Received component options.
 */
function Graph(element, options) {
    var component = this;
    this.$element = (0, _jquery2.default)(element);
    var tooltipTemplate = component.$element.find(TOOLTIP_FORMAT_SELECTOR).html().trim();
    var productTemplate = component.$element.find(TOOLTIP_PRODUCT_FORMAT_SELECTOR).html().trim();
    var initialViewportWidth = (0, _jquery2.default)(window).width();
    var monthFormat = component.$element.attr(TRANSLATION_FORMAT_MONTH);
    component.componentLoaded = false;
    component.assetBaseUrl = component.$element.attr(ASSET_BASE_SELECTOR);
    component.setSpecificOptions = null;
    component.spinner = new _appSpinner2.default(SPINNER_HANDLE);
    component.seperator = component.$element.attr(THOUSAND_SEPARATOR);
    var chartOptions = {
        chart: {
            renderTo: ID_SELECTOR,
            spacing: [10, 0, 10, 0],
            type: TYPE,
            animation: {
                duration: 300
            },
            height: 330,
            backgroundColor: 'transparent',
            style: {
                fontFamily: '"Proxima Nova",sans-serif'
            }
        },
        credits: {
            enabled: false
        },
        lang: {
            thousandsSep: this.seperator,
            decimalPoint: ','
        },
        legend: {
            enabled: false
        },
        loading: {
            hideDuration: 500,
            showDuration: 500
        },
        plotOptions: {
            area: {
                animation: false,
                fillColor: 'transparent',
                lineWidth: 2.5,
                marker: {
                    fillColor: '#FFFFFF',
                    lineColor: '#3888C5',
                    lineWidth: 2,
                    radius: 3,
                    states: {
                        hover: {
                            enabled: false
                        }
                    }
                },
                trackByArea: true
            },
            allowPointSelect: false,
            series: {
                turboThreshold: 5000,
                states: {
                    hover: {
                        enabled: false
                    }
                }
            }
        },
        series: [{
            data: [],
            id: YOUR_OBJECT_ID,
            lineWidth: 3,
            color: '#3888C5',
            marker: {
                lineWidth: 2,
                radius: 3
            },
            name: YOUR_OBJECT_ID,
            zIndex: 3
        }, {
            data: [],
            id: 'flags',
            linkedTo: ':previous',
            lineWidth: 0,
            marker: {
                enabled: false
            },
            name: null,
            onSeries: 'line'
        }, {
            data: [],
            id: 'Products',
            linkedTo: ':previous',
            lineWidth: 0,
            marker: {
                enabled: false
            },
            color: '#3888C5',
            name: 'Products',
            onSeries: 'line'
        }],
        title: {
            text: ''
        },
        tooltip: {
            shadow: false,
            shared: true,
            useHTML: true,
            crosshairs: {
                color: '#A4DBFF',
                dashStyle: 'solid'
            },
            followPointer: true,
            formatter: function formatter() {
                if (!component.isLoading) {
                    return component.renderTooltip({
                        tooltip: tooltipTemplate,
                        product: productTemplate
                    }, this);
                }
            },
            hideDelay: 0,
            positioner: function positioner(labelWidth, labelHeight, point) {
                var tooltipXaxis = void 0;
                var tooltipYaxis = void 0;
                var tooltip = (0, _jquery2.default)(TOOLTIP_SELECTOR);
                var tooltipWidth = tooltip.width() / 2;
                var tooltipHeight = tooltip.height();

                // Set the tooltip horizontal position.
                if (point.plotX + tooltipWidth > component.chart.plotWidth) {
                    // If tooltip start touching the edge on the right.
                    tooltipXaxis = point.plotX + component.chart.plotLeft - 130;
                    tooltip.addClass('align-right');
                } else if (point.plotX < tooltipWidth) {
                    // If tooltip start touching the edge on the left.
                    tooltipXaxis = point.plotX + component.chart.plotLeft - 20;
                    tooltip.addClass('align-left');
                } else {
                    tooltipXaxis = point.plotX + component.chart.plotLeft - 75;
                    tooltip.removeClass('align-right');
                    tooltip.removeClass('align-left');
                }

                // Set the tooltip vertical position.
                tooltipYaxis = point.plotY + component.chart.plotTop - (tooltipHeight + 20);

                return {
                    x: tooltipXaxis,
                    y: tooltipYaxis
                };
            },
            style: {
                padding: 0
            }
        },
        xAxis: {
            dateTimeLabelFormats: {
                day: monthFormat,
                week: monthFormat,
                month: '%B'
            },
            name: 'Datum',
            labels: {
                autoRotation: [-45],
                autoRotationLimit: 120,
                align: 'center',
                style: {
                    color: '#999'
                }
            },
            lineColor: '#D7E2E5',
            lineWidth: 1,
            minPadding: 0,
            maxPadding: 0,
            tickColor: 'transparent'
        },
        yAxis: {
            gridLineColor: '#D7E2E5',
            gridLineWidth: 1,
            labels: {
                formatter: function formatter() {
                    return Highcharts.numberFormat(this.value, 0, '', component.seperator);
                },
                style: {
                    color: '#999'
                }
            },
            lineColor: '#D7E2E5',
            lineWidth: 1,
            min: 0,
            title: {
                style: {
                    color: '#999'
                },
                x: -6
            }
        }
    };

    component.overrideComponentOptions(options);

    // When main Highcharts library is loaded, init a new Highcharts chart.
    (0, _loadScript2.default)(component.getLibraryUrl(), function (err) {
        if (typeof err !== 'undefined') {

            component.translateDates();

            Highcharts.setOptions({
                global: {
                    useUTC: false
                }
            });

            component.chart = new Highcharts.Chart(chartOptions);
            Graph.redrawBasedOnViewport(component.chart, initialViewportWidth);
            component.componentLoaded = true;
        }
    });

    (0, _jquery2.default)(window).on('resize', (0, _debounce2.default)(function () {
        var resizedViewportWidth = (0, _jquery2.default)(window).width();
        Graph.redrawBasedOnViewport(component.chart, resizedViewportWidth);
    }, 100));
}

/**
 * Get options from component that is extending this component.
 * @param {string} options
 */
Graph.prototype.overrideComponentOptions = function (options) {
    var component = this;

    component.assetBaseUrl = options.assetBaseUrl || component.assetBaseUrl;
    component.setSpecificOptions = options.setSpecificOptions || component.setSpecificOptions;
};

/**
 * Set the translations to Highchart properties so the correct translations show up in the graph.
 */
Graph.prototype.translateDates = function () {
    var component = this;
    var lang = {};

    var setTranslation = function setTranslation(object) {
        var attribute = component.$element.attr(object.selector);
        if (attribute) {
            Graph[object.property] = lang[object.property] = attribute.split(',');
        }
    };

    setTranslation({ property: 'months', selector: TRANSLATION_MONTHS });
    setTranslation({ property: 'shortMonths', selector: TRANSLATION_SHORTMONTHS });
    setTranslation({ property: 'weekdays', selector: TRANSLATION_WEEKDAYS });

    Graph.dateFormat = component.$element.attr(TRANSLATION_FORMAT_DATE);

    // Set language options with correct translations.
    Highcharts.setOptions({ lang: lang });
};

/**
 * Returns url based on where Highcharts is loaded from.
 * @returns {string} library url
 */
Graph.prototype.getLibraryUrl = function () {
    var component = this;

    if (component.assetBaseUrl !== undefined && component.assetBaseUrl !== '') {
        return component.assetBaseUrl + LIBRARY_URL;
    }

    return LIBRARY_URL;
};

/**
 * Transforms the received data to the format needed for the chart.
 */
Graph.prototype.processData = function () {
    var component = this;

    component.setLoading(true);

    if (component.setSpecificOptions !== null && (0, _isFunction2.default)(component.setSpecificOptions)) {
        component.setSpecificOptions(component.data, component.chart); // Set specific options
    }

    Graph.highlightWeekends(component.chart, component.data.weekends);
    Graph.populateChart(component.chart, component.data);
    Graph.plotFlagLine(component.chart);

    component.setLoading(false);
};

/**
 * Show or hide the spinner, depanding on boolean returned by all connected components.
 * @param {boolean} value       If we're still loading data.
 */
Graph.prototype.setLoading = function (value) {
    var component = this;
    var spinnerTemplate = component.$element.find(SPINNER_TEMPLATE_HANDLE).html().trim();

    // If they're equal, return.
    if (component.isLoading == value) {
        return;
    }

    component.isLoading = value;

    if (component.isLoading) {
        component.chart.showLoading(spinnerTemplate);
        component.spinner.show();
    } else {
        component.chart.hideLoading();
        component.spinner.hide();
    }
};

/**
 * Redraw axis based on viewport size.
 * @param {Object[]} chart
 * @param {number} width
 */
Graph.redrawBasedOnViewport = function (chart, width) {
    if (width < 500) {
        chart.xAxis[0].update({
            labels: {
                y: 30
            } }, false);
        chart.yAxis[0].update({
            showLastLabel: false,
            labels: {
                align: 'left',
                x: 5,
                y: -10
            } }, false);
    } else {
        chart.xAxis[0].update({
            labels: {
                y: 20
            } }, false);
        chart.yAxis[0].update({
            showLastLabel: true,
            labels: {
                align: 'right',
                x: -8,
                y: 3
            } }, false);
    }
    chart.redraw();
    chart.reflow();
};

/**
 * Filter series to a single flag.
 * @param {Object[]} chart
 */
Graph.plotFlagLine = function (chart) {
    chart.xAxis[0].removePlotLine('flags'); // Remove all existing flag lines
    chart.series.filter(function (serie) {
        return serie.options.name !== 'products';
    }) // exclude products
    .filter(function (serie) {
        return serie.options.id === 'flags';
    }) // find all flags
    .reduce(function (out, serie) {
        return serie.data;
    }, []) // reduce to data of a single flag
    .forEach(function (point) {
        return Graph.plotFlagPoint(point, chart);
    });
};

/**
 * Adds plot lines to the graph.
 * @param {Object[]} point
 * @param {Object[]} chart
 */
Graph.plotFlagPoint = function (point, chart) {
    chart.xAxis[0].addPlotLine({
        value: point.x,
        width: 1,
        color: '#00AAE6',
        id: 'flags',
        zIndex: 1
    });
};

/**
 * Adds a plotBand for each weekend day into the graph.
 * @param {Object[]} chart
 * @param {array} weekends
 */
Graph.highlightWeekends = function (chart, weekends) {
    weekends.forEach(function (weekend) {
        var date = weekend.Date;
        var twelveHours = 60 * 60 * 24 * 1000 / 2;

        chart.xAxis[0].addPlotBand({
            from: date - twelveHours,
            to: date + twelveHours,
            color: 'rgba(245, 245, 245, 0.7)',
            zIndex: 1
        });
    });
};

/**
 * Creates a custom tooltip from the available data.
 * @param {{tooltip, product}} templates
 * @param {Object[]} data
 */
Graph.prototype.renderTooltip = function (templates, data) {
    var component = this;
    var titlePattern = new RegExp('{title}', 'g');
    var serieNameLabel = YOUR_OBJECT_ID;
    var dayLabel = (0, _jquery2.default)(COMPONENT_SELECTOR).attr(TRANSLATION_DAY);
    var title = data.x.toString().length > 2 ? Graph.formatDate(new Date(data.x)) : dayLabel.replace('{value}', data.x);
    var value = data.points.filter(function (point) {
        return point.series.name === serieNameLabel;
    })[0];
    var product = data.points.filter(function (point) {
        return point.series.name === 'Products';
    })[0];

    var tooltipHtml = templates.tooltip.replace(titlePattern, title).replace('{seriesColor}', value.color).replace('{value}', _thousandSeparator2.default.format(value.y, component.seperator));

    var $tooltip = (0, _jquery2.default)(tooltipHtml);

    if (product) {
        var titlesHtml = Graph.renderProductTitles(templates.product, product.point.title);
        var productsHandle = $tooltip.find(TOOLTIP_PRODUCT_HANDLE);
        productsHandle.append(titlesHtml);
    }

    (0, _jquery2.default)(TOOLTIP_SELECTOR).empty().append($tooltip);
};

/**
 * Lookup flag data from the flagMap and return the array of flag objects.
 * @param {string} template
 * @param {string[]} titles
 * @returns {string} rendered product-title HTML
 */
Graph.renderProductTitles = function (template, titles) {
    var pattern = new RegExp('{flagName}', 'g');

    return titles.map(function (title) {
        return template.replace(pattern, title);
    }).join('');
};

/**
 * Iterate over the chart data object and push the data to the chart options
 * in the order that the series array is defined in the options.
 * @param {string} chart
 * @param {string[]} data
 */
Graph.populateChart = function (chart, data) {
    delete data.weekends;

    Object.keys(data).forEach(function (key, index) {
        return chart.series[index].setData(data[key], true, true, false);
    });
};

/**
 * Create formatted, readable, date from received date string.
 * @param {Date} date
 * @returns {string} date as "Ma 10 januari 2000"
 */
Graph.formatDate = function (date) {
    return Highcharts.dateFormat(Graph.dateFormat, date);
};

},{"../app-spinner/app-spinner":201,"../thousand-separator/thousand-separator":322,"jquery":"jquery","load-script":2,"lodash/debounce":158,"lodash/isFunction":173}],241:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

var _contentFetch = require('../content-fetch/content-fetch');

var _contentFetch2 = _interopRequireDefault(_contentFetch);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = AdvertisementHypotheekAdvies;

// component configuration

var COMPONENT_SELECTOR = '[data-hypotheek-advies]';
var COMPONENT_SELECTOR_TARGET = '[data-hypotheek-advies-box]';
var COMPONENT_TRIGGER_SELECTOR_TARGET = '[data-hypotheek-advies-handle]';
var COMPONENT_TARGET_SELECTOR_TARGET = '[data-hypotheek-advies-target]';
var CATEGORIES_URL_ATTR = 'data-content-fetch-url';
var fetchDataCallback = function fetchDataCallback() {
    AdvertisementHypotheekAdvies.prototype.handleComplete();
};

function AdvertisementHypotheekAdvies(element) {
    var component = this;
    component.urlFeed = element.getAttribute(CATEGORIES_URL_ATTR);
    (0, _contentFetch2.default)(element, component.urlFeed, fetchDataCallback);
}

AdvertisementHypotheekAdvies.prototype.handleComplete = function () {
    var component = this;
    (0, _jquery2.default)(COMPONENT_SELECTOR_TARGET).each(function (index, element) {
        component.toggleForm((0, _jquery2.default)(element));
    });
};

AdvertisementHypotheekAdvies.prototype.toggleForm = function ($element) {
    var component = this;
    component.$trigger = $element.find(COMPONENT_TRIGGER_SELECTOR_TARGET);
    component.$target = $element.find(COMPONENT_TARGET_SELECTOR_TARGET);
    component.$inputs = component.$target.find('input');
    component.handleChange(component.$trigger, component.$inputs);
};

AdvertisementHypotheekAdvies.prototype.handleChange = function ($trigger, $inputs) {
    var component = this;
    $trigger.on('change', function () {
        if ((0, _jquery2.default)(this).is(':checked')) {
            component.checkedFeedback($inputs);
        } else {
            component.unCheckedFeedback($inputs);
        }
    });
};

AdvertisementHypotheekAdvies.prototype.checkedFeedback = function (input) {
    (0, _jquery2.default)(input).parents(COMPONENT_SELECTOR_TARGET).addClass('is-expanded');
    input.prop('required', true);
};

AdvertisementHypotheekAdvies.prototype.unCheckedFeedback = function (input) {
    (0, _jquery2.default)(input).parents(COMPONENT_SELECTOR_TARGET).removeClass('is-expanded');
    input.prop('required', false);
};

// turn all elements with the default selector into components
(0, _jquery2.default)(COMPONENT_SELECTOR).each(function (index, element) {
    return new AdvertisementHypotheekAdvies(element);
});

},{"../content-fetch/content-fetch":222,"jquery":"jquery"}],242:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = IframeHeightUpdater;


var COMPONENT_SELECTOR = '[data-iframe-height-updater]';
var MARGIN_SIZE = 50;

function IframeHeightUpdater(element) {
    var component = this;
    component.$element = (0, _jquery2.default)(element);

    component.bindEvents();
    component.updateHeight();
}

IframeHeightUpdater.prototype.bindEvents = function () {
    var component = this;

    (0, _jquery2.default)(window).on('resize click', function () {
        return component.updateHeight();
    });
    setInterval(function () {
        return component.updateHeight();
    }, 500);
};

IframeHeightUpdater.prototype.updateHeight = function () {
    var component = this;
    var currentHeight = MARGIN_SIZE + component.$element.height();

    if (currentHeight !== component.Height) {
        component.Height = currentHeight;
        window.parent.postMessage('height=' + currentHeight, '*');
    }
};

(0, _jquery2.default)(COMPONENT_SELECTOR).each(function (index, element) {
    return new IframeHeightUpdater(element);
});

},{"jquery":"jquery"}],243:[function(require,module,exports){
// require this module where needed, either in a specific page or component or generically in src/funda.js
'use strict';

// explicitly inject dependencies (alphabetically), only those needed

var $ = require('jquery');

// component configuration
var COMPONENT_ATTR = 'data-image-error-fallback';
var COMPONENT_SELECTOR = '[' + COMPONENT_ATTR + ']';

// what does this module expose?
module.exports = ImageErrorFallback;

function ImageErrorFallback(element, event) {
    var component = this;
    component.$element = $(element);
    component.fallbackUrl = element.getAttribute(COMPONENT_ATTR);

    if (component.$element.is('IMG')) {
        if (event.type === 'load') {
            component.checkImage(element);
        } else {
            element.onerror = function () {
                element.onerror = null;
                component.applyFallback(element);
            };
        }
    } else {
        component.$element.find('img').each(function (index, image) {
            component.checkImage(image);

            image.onerror = function () {
                image.onerror = null;
                component.applyFallback(image);
            };
        });
    }
}

/**
 * Check image and apply fallback if not OK
 * @param image
 */
ImageErrorFallback.prototype.checkImage = function (image) {
    if (!this.imageIsOk(image)) {
        this.applyFallback(image);
    }
};

/**
 * Set image attributes to fallback
 * @param image
 */
ImageErrorFallback.prototype.applyFallback = function (image) {
    var parent = $(image).parent();

    image.srcset = '';
    image.src = this.fallbackUrl;

    if (parent.hasClass('is-backgroundcover')) {
        parent.css('background-image', 'url(' + this.fallbackUrl + ')');
    }
};

/**
 * Fuzzy fluffy image check for images.
 * http://stackoverflow.com/questions/1977871/check-if-an-image-is-loaded-no-errors-in-javascript
 * https://stereochro.me/ideas/detecting-broken-images-js
 *
 * @param image {element} - image html element
 * @return {boolean}
 */
ImageErrorFallback.prototype.imageIsOk = function (image) {
    // this indicates that a srcset operation is currently deciding what image to load, which can lead
    // to a false positive for the error fallback. Therefore in this case we will assume the image is OK.
    if (image.currentSrc === '') {
        return true;
    }
    // During the onload event, IE correctly identifies any images that
    // weren’t downloaded as not complete. Others should too. Gecko-based
    // browsers act like NS4 in that they report this incorrectly.
    if (image.complete === false) {
        return false;
    }
    // However, they do have two very useful properties: naturalWidth and
    // naturalHeight. These give the true size of the image. If it failed
    // to load, either of these should be zero.
    if (image.naturalWidth === 0) {
        return false;
    }
    // No other way of checking: assume it’s ok.
    return true;
};

$(window).on('load resultsUpdated', function (event) {
    $(COMPONENT_SELECTOR).each(function (index, element) {
        return new ImageErrorFallback(element, event);
    });
});

},{"jquery":"jquery"}],244:[function(require,module,exports){
// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

// explicitly inject dependencies (alphabetically), only those needed

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

var _thousandSeparator = require('../thousand-separator/thousand-separator');

var _thousandSeparator2 = _interopRequireDefault(_thousandSeparator);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// what does this module expose?
exports.default = InputNumber;

// component configuration

var COMPONENT_SELECTOR = '[data-input-number]';
var INPUT_SELECTOR = '[data-input-number-field]';
var MASK_SELECTOR = '[data-input-number-mask]';
var THOUSAND_SEPARATOR = 'data-thousand-separator';
var DISPLAY_STRING = 'data-display-string';
var TOKEN = '{value}';

var KEY_CODE = {
    ENTER: 13
};

function InputNumber(element) {
    var component = this;
    component.$element = (0, _jquery2.default)(element);
    component.$input = component.$element.find(INPUT_SELECTOR);
    component.$mask = component.$element.find(MASK_SELECTOR);
    component.separator = component.$element.attr(THOUSAND_SEPARATOR);
    component.displayString = component.$element.attr(DISPLAY_STRING) || TOKEN;
    component.token = TOKEN;
    component.bindEvents();
}

InputNumber.prototype.bindEvents = function () {
    var component = this;

    component.$input.on('keypress', function (event) {
        if (event.keyCode === KEY_CODE.ENTER) {
            event.preventDefault();
            var $input = (0, _jquery2.default)(event.currentTarget);
            $input.blur();
        }
    });

    component.$input.on('blur', function () {
        component.updateMask();
    });
};

/**
 * update the mask with the data from the input field and add the thousand separator if needed
 */
InputNumber.prototype.updateMask = function () {
    var component = this;
    var number = component.$input.val();
    if (number > 0) {
        var formattedValue = _thousandSeparator2.default.format(parseInt(number, 10), component.separator);
        var displayString = component.displayString.replace(component.token, formattedValue);

        component.$mask.text(displayString);
    } else {
        component.$mask.text('');
    }
};

// turn all elements with the default selector into components
(0, _jquery2.default)(COMPONENT_SELECTOR).each(function (index, element) {
    return new InputNumber(element);
});

},{"../thousand-separator/thousand-separator":322,"jquery":"jquery"}],245:[function(require,module,exports){
// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

// explicitly inject dependencies (alphabetically), only those needed

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var InstantSearch = require('../instant-search/instant-search');

// what does this module expose?
exports.default = InstantMapSearch;

// component configuration

var COMPONENT_SELECTOR = '[data-instant-map-search]';
var MAP_SELECTOR = '[data-map]';

var CENTER_MAP_EVENT = 'center_map_on';
var SEARCH_QUERY_UPDATED_EVENT = 'zo_updated';

var SEARCH_SIDEBAR_SELECTOR = '[data-search-sidebar]';
var EXTENDED_CLASS = 'is-extended';

/**
 * @param {HTMLFormElement} form
 * @constructor
 */
function InstantMapSearch(form) {
    InstantSearch.call(this, form);

    var component = this;
    component.$form = (0, _jquery2.default)(form);
    component.$map = (0, _jquery2.default)(MAP_SELECTOR);

    component.overrideEvents();

    (0, _jquery2.default)(window).on('sidebar-refresh', function () {
        var $myNewElement = (0, _jquery2.default)(COMPONENT_SELECTOR);
        if ($myNewElement.length == 1) {
            component.bindToElements($myNewElement);
            component.bindToEvents();
        }
    });
}

InstantMapSearch.prototype = Object.create(InstantSearch.prototype);

/**
 * override events from parent class
 */
InstantMapSearch.prototype.overrideEvents = function () {
    var component = this;
    window.removeEventListener('popstate', component.onpopstate, false);
};

/**
 * Update changes in map for updated result
 */
InstantMapSearch.prototype.updateResults = function (data) {
    var component = this;
    if (data.geo) {
        component.updateCenterMap(data.geo);
    }
    data.url = data.url + window.location.search;
    component.updateHistory(data.url);
    component.updateMapTiles(data.zo);

    if (component.$form.find(SEARCH_SIDEBAR_SELECTOR).hasClass(EXTENDED_CLASS)) {
        component.asyncResult = data;
    } else {
        component.renderOutputResults(data);
    }
};

/**
 * trigger center_event
 */
InstantMapSearch.prototype.updateCenterMap = function (geo) {
    var component = this;
    component.$map.trigger(CENTER_MAP_EVENT, geo);
};

/**
 * trigger query_updated_event
 */
InstantMapSearch.prototype.updateMapTiles = function (searchQuery) {
    var component = this;
    component.$map.trigger(SEARCH_QUERY_UPDATED_EVENT, { zo: searchQuery });
};

(0, _jquery2.default)(COMPONENT_SELECTOR).each(function (index, element) {
    return new InstantMapSearch(element);
});

},{"../instant-search/instant-search":246,"jquery":"jquery"}],246:[function(require,module,exports){
/* global gtmDataLayer */
'use strict';

// explicitly inject dependencies (alphabetically), only those needed

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

var _ajax = require('../ajax/ajax');

var _ajax2 = _interopRequireDefault(_ajax);

var _debounce = require('lodash/debounce');

var _debounce2 = _interopRequireDefault(_debounce);

var _appliedFilters = require('../applied-filters/applied-filters');

var _appliedFilters2 = _interopRequireDefault(_appliedFilters);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var FilterFlyout = require('../filter-flyout/filter-flyout');

// what does this module expose?
module.exports = InstantSearch;

// component configuration
var COMPONENT_SELECTOR = '[data-instant-search]';
var HANDLE_SELECTOR = '[data-instant-search-handle]';
var RANGE_SELECTOR = '[data-instant-search-range]';
var OUTPUT_KEY_ATTR = 'data-instant-search-output';
var OUTPUT_SELECTOR = '[data-instant-search-output]';
var SEARCH_SIDEBAR_SELECTOR = '[data-search-sidebar]';
var SIDEBAR_CLOSED_EVENT = 'sidebarclosed';
var INVALID_LOCATION_EVENT = 'invalidlocation';
var AUTOCOMPLETE_SELECTOR = '[data-autocomplete]';
var RADIUS_SELECTOR = '[data-input-radius] select';
var ZOEKTYPE_SELECTOR = 'select[data-zoektype]';
var MAKELAARNAAM_SELECTOR = '[data-makelaars-name]';
var RANGE_FLYOUT_SELECTOR = '[data-filter-flyout-range]';
var COUNT_ATTR = 'data-instant-search-count';
var SOLD_OBJECTS_ANCHOR_SELECTOR = '[data-instant-search-sold-objects]';
var FOR_SALE_OBJECTS_SELECTOR = '[data-instant-search-for-sale-objects]';
var PAGINATION_SELECTOR = '[data-pagination]';
var FILTER_COUNT_NUMBER = '[data-filter-count-number]';
var UPDATING_CLASS = 'is-updating';
var EXTENDED_CLASS = 'is-extended';
var NO_RESULTS_CLASS = 'has-no-results';
var MULTILINE_CLASS = 'multiline';
var URL_SUFFIX = '?ajax=true';
var DYNAMIC_FILTER_FIELD = 'wijkNaamFilter';
var DYNAMIC_APPLIED_FILTER = 'buurt';
var DYNAMIC_FILTER_SELECTOR = '[data-dynamic-filter]';
var PUSH_STATE_MARKER = 'push_state_set_by_funda';
var MINIMUM_INTERVAL_BETWEEN_REQUESTS = 500; // ms
var TIMEOUT_SEARCH_REQUEST = 30000; // 30 sec

var PRICE_RANGE_SELECTOR = '[data-range-filter]';
var RADIO_GROUP_SELECTOR = '[data-radio-group]';

var supportsPushState = 'pushState' in window.history;
var isCapableBrowser = supportsPushState;

/**
 * @param {HTMLFormElement} form
 * @constructor
 */
function InstantSearch(form) {
    if (!isCapableBrowser) {
        return;
    }

    var component = this;
    component.bindToElements(form);
    component.bindToEvents();

    // initialize popstate
    window.history.replaceState(PUSH_STATE_MARKER, window.title);
}

InstantSearch.prototype.bindToElements = function (form) {
    var component = this;

    var $form = (0, _jquery2.default)(form);
    component.form = form;
    component.$form = $form;
    component.outputMap = {};
    component.request = null; // store pending xhr request
    component.asyncResult = {};
    component.soldFilterOption = component.$form.find(SOLD_OBJECTS_ANCHOR_SELECTOR);
    component.forSaleFilterOption = component.$form.find(FOR_SALE_OBJECTS_SELECTOR);
    component.stateObject = {};
    component.$filterCountNumber = component.$form.find(FILTER_COUNT_NUMBER);
    component.$outputs = component.getOutputs((0, _jquery2.default)('body'));
};

/**
 * Request form submit on form input changes.
 */
InstantSearch.prototype.bindToEvents = function () {
    var component = this;

    component.$form.find(SEARCH_SIDEBAR_SELECTOR).on(SIDEBAR_CLOSED_EVENT, function () {
        if (Object.keys(component.asyncResult).length > 0) {
            component.renderOutputResults(component.asyncResult);
            component.asyncResult = {}; //empty async result
        }
    });

    window.addEventListener('popstate', component.onpopstate, false);

    function debouncedRequest(forceGet) {
        return (0, _debounce2.default)(function () {
            component.doRequest(forceGet);
        }, MINIMUM_INTERVAL_BETWEEN_REQUESTS, { leading: true });
    }

    component.$form.on('pricerangereset', PRICE_RANGE_SELECTOR, debouncedRequest());
    component.$form.on('radiogroupreset', RADIO_GROUP_SELECTOR, debouncedRequest());
    component.$form.on('change', RADIUS_SELECTOR, debouncedRequest());
    component.$form.on('makelaarsnameupdated', MAKELAARNAAM_SELECTOR, debouncedRequest());
    component.$form.on('change', HANDLE_SELECTOR, debouncedRequest());
    component.$form.on('change', ZOEKTYPE_SELECTOR, debouncedRequest(true));
    component.$form.on('rangechanged', RANGE_SELECTOR, debouncedRequest());
    component.$form.on('rangeselected', RANGE_FLYOUT_SELECTOR, debouncedRequest());
    component.$form.on('pageadjusted', PAGINATION_SELECTOR, debouncedRequest());
    component.$form.on('searchqueryupdated', AUTOCOMPLETE_SELECTOR, debouncedRequest());
    // TODO: refactor all the things related below listener and its trigger
    component.$form.on('range_filter_removed', debouncedRequest());
};

// reload page on browser back only if we have set the state. Safari fires the popstate on pageload (with an empty event state), and we don't want to reload on pageload :)
InstantSearch.prototype.onpopstate = function (event) {
    if (event.state === PUSH_STATE_MARKER) {
        window.location.reload();
    }
};

InstantSearch.prototype.doRequest = function (forceGet) {
    var component = this;
    var formData = component.$form.serialize();
    var queryParams = URL_SUFFIX;
    if (forceGet !== undefined && forceGet === true) {
        queryParams += '&forceget=true';
    }

    // ensure outputs indicate they are updating
    component.$outputs.addClass(UPDATING_CLASS);
    (0, _jquery2.default)(document).trigger('resultsUpdating');

    // abort previous request if still pending
    if (component.request) {
        component.request.abort();
    }

    component.request = (0, _ajax2.default)({
        url: component.form.action + queryParams,
        method: (this.form.method || 'POST').toUpperCase(),
        data: formData,
        timeout: TIMEOUT_SEARCH_REQUEST
    });

    component.request.done(function onSuccess(data) {
        if (data.Error) {
            component.stoppedUpdating();
            component.logError(data.Error);
            component.$form.trigger(INVALID_LOCATION_EVENT);
        } else if (data.forcedRedirectUrl) {
            window.location = data.forcedRedirectUrl;
        } else {
            component.updateResults(data);
        }
    });

    component.request.fail(function onError(err) {
        if (err.statusText !== 'abort') {
            component.stoppedUpdating();
        }
    });
};

/**
 * Determine if results from search request needs to be rendered
 * @param {Object} data
 * @param {Object} data.content
 * @param {String} data.url
 */
InstantSearch.prototype.updateResults = function (data) {
    var component = this;

    // update first all sync elements
    document.title = data.title;
    component.soldFilterOption.attr('href', data.historischAanbodUrl);
    component.soldFilterOption.text(data.historischAanbodLabel);
    component.soldFilterOption.toggleClass(MULTILINE_CLASS, data.multilineHistorischAanbodLabel);
    component.forSaleFilterOption.text(data.actiefAanbodLabel);
    component.forSaleFilterOption.toggleClass(MULTILINE_CLASS, data.multilineActiefAanbodLabel);
    component.updateTotalCount(data.content.total, data.content.searchButtonTotal);
    component.updateCounts(data.counts);
    component.updateHistory(data.url);

    if (component.$form.find(SEARCH_SIDEBAR_SELECTOR).hasClass(EXTENDED_CLASS)) {
        component.asyncResult = data;
    } else {
        component.renderOutputResults(data);
    }
};

/**
 * log the error for Google Analytics
 * @param {String} errorType
 */
InstantSearch.prototype.logError = function (errorType) {
    if (window.gtmDataLayer !== undefined) {
        // Fill GtmDataLayer with data
        gtmDataLayer.push({
            'event': 'searchResultError',
            'errorType': 'invalid-' + errorType
        });
    }
};

/**
 * Render the results from the search request
 * @param {Object} resultData with keys corresponding to output identifiers and their new HTML as values.
 */
InstantSearch.prototype.renderOutputResults = function (resultData) {
    var component = this;
    var fields = resultData.content;

    for (var key in fields) {
        if (fields.hasOwnProperty(key) && component.outputMap[key] && fields[key] !== null) {
            if (component.outputMap[key].is('input')) {
                component.outputMap[key].val(fields[key]);
            } else {
                component.outputMap[key].html(fields[key]);
            }
        }
    }
    if (Object.keys(fields).length > 0) {
        component.stoppedUpdating();
        component.sendResultsUpdatedEvent(resultData.url);
    }

    component.updateBinds(fields);
};

InstantSearch.prototype.sendResultsUpdatedEvent = function (url) {
    (0, _jquery2.default)(document).trigger('resultsUpdated', { url: url });
};

/**
 * the results are updated or there is an error occured
 */
InstantSearch.prototype.stoppedUpdating = function () {
    var component = this;
    component.$outputs.removeClass(UPDATING_CLASS);
};

/**
 * @param {Object} counts;   map in which key matches count selectors (eg. `{ "my-id": "1.356" }`).
 */
InstantSearch.prototype.updateCounts = function (counts) {
    var component = this;
    for (var key in counts) {
        if (counts.hasOwnProperty(key)) {
            var $countElement = component.$form.find('[' + COUNT_ATTR + '="' + key + '"]');
            var $countWrapper = $countElement.closest('li');
            var count = counts[key];

            $countElement.text(count);

            if (count === '0') {
                $countWrapper.addClass(NO_RESULTS_CLASS);
            } else {
                $countWrapper.removeClass(NO_RESULTS_CLASS);
            }
        }
    }
};

/**
 * @param {String} totalResults;    The total number of results (eg. '650 resultaten in <location>')
 * @param {String} searchButtonTotalResults;    The total number of results within a button (eg. '650 resultaten')
 *
 * In makelaar result list, there is a difference between totalResults and searchButtonTotalResults
 */
InstantSearch.prototype.updateTotalCount = function (totalResults, searchButtonTotalResults) {
    var component = this;
    component.outputMap.total.each(function (index, element) {
        (0, _jquery2.default)(element).text(totalResults);
    });
    component.outputMap.searchButtonTotal.each(function (index, element) {
        (0, _jquery2.default)(element).text(searchButtonTotalResults);
    });
};

/**
 * Add url to browser history
 * @param {String} url
 */
InstantSearch.prototype.updateHistory = function (url) {
    window.history.pushState(PUSH_STATE_MARKER, window.title, url);
};

InstantSearch.prototype.updateBinds = function (fields) {
    var component = this;

    if (fields[DYNAMIC_FILTER_FIELD] && fields[DYNAMIC_FILTER_FIELD].length > 1) {
        var $dynamicFilter = component.$form.find(DYNAMIC_FILTER_SELECTOR);
        return new FilterFlyout($dynamicFilter, true);
    } else if (component.$form.find(DYNAMIC_FILTER_SELECTOR).length < 1) {
        /* buurtfilter not present, make sure no remaining buurten are in applied filters */
        _appliedFilters2.default.remove(DYNAMIC_APPLIED_FILTER);
    }
};

/**
 * Get the outputs for the instant-search form.
 * This needs to be a prototype because user-saved-objects-sorting module extends it to find the outputs elswhere
 * @param {Object} $form
 */
InstantSearch.prototype.getOutputs = function ($container) {
    var component = this;
    var $temp = $container.find(OUTPUT_SELECTOR);
    $temp.each(function (index, output) {
        var key = output.getAttribute(OUTPUT_KEY_ATTR);
        var $outputs = component.outputMap[key] || (0, _jquery2.default)();
        component.outputMap[key] = $outputs.add(output);
    });
    return $temp;
};

// turn all elements with the default selector into components
(0, _jquery2.default)(COMPONENT_SELECTOR).each(function (index, element) {
    return new InstantSearch(element);
});

},{"../ajax/ajax":198,"../applied-filters/applied-filters":204,"../filter-flyout/filter-flyout":232,"jquery":"jquery","lodash/debounce":158}],247:[function(require,module,exports){
'use strict';

var $ = require('jquery');

window.triggerHotspotClickEvent = function (name, referer) {
    $(window).trigger('krpanohotspotclicked', {
        name: name,
        referer: referer
    });
};

window.triggerWebVRAvailable = function () {
    $(window).trigger('webvravailable');
};

window.triggerWebVREnabled = function () {
    $(window).trigger('webvrenabled');
};

window.triggerWebVRDisabled = function () {
    $(window).trigger('webvrdisabled');
};

},{"jquery":"jquery"}],248:[function(require,module,exports){
// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

var $ = require('jquery');

module.exports = LogRequest;

// component configuration
var COMPONENT_ATTR = 'data-log-request';
var COMPONENT_SELECTOR = '[' + COMPONENT_ATTR + ']';

function LogRequest(element) {
    var component = this;
    component.$element = $(element);
    component.url = component.$element.attr(COMPONENT_ATTR);

    if (!component.url || component.url.length === 0) {
        return;
    }

    component.$element.on('click', function (event) {
        var clickedElement = $(event.target);
        if (!clickedElement.data('logged')) {
            $.ajax(component.url);
            clickedElement.data('logged', true);
        }
    });
}

// turn all elements with the default selector into components
$(COMPONENT_SELECTOR).each(function (index, element) {
    return new LogRequest(element);
});

},{"jquery":"jquery"}],249:[function(require,module,exports){
// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

// explicitly inject dependencies (alphabetically), only those needed

// what does this module expose?

module.exports = LoginDialog;

var LOGIN_DIALOG_SELECTOR = '[data-dialog="user-form"]';

function LoginDialog(contentUrl, onSuccess) {
    var component = this;
    var dialogElement = document.querySelector(LOGIN_DIALOG_SELECTOR);

    dialogElement.dialog.onFormSuccess = function () {
        onSuccess();
        // on successful login a lot of things need to update on the page,
        // so it's easier just to reload the entire page entirely.
    };

    component.dialog = dialogElement.dialog;
    component.dialog.open();
    component.dialog.getContent(contentUrl);
}

},{}],250:[function(require,module,exports){
// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

// explicitly inject dependencies (alphabetically), only those needed

var $ = require('jquery');

// what does this module expose?
module.exports = MaandlastenIndicator;

// component configuration
var COMPONENT_SELECTOR = '[data-maandlasten-indicator]';
var URL_SMALL_ATTR = 'data-maandlasten-indicator-small';
var URL_LARGE_ATTR = 'data-maandlasten-indicator-large';
var viewport = $(window).width();
var bpMortgage = 643;

function MaandlastenIndicator(element) {
    var component = this;
    component.injectIframe(element);
}

MaandlastenIndicator.prototype.injectIframe = function (element) {
    var component = this;
    component.$element = $(element);
    component.urlSmall = element.getAttribute(URL_SMALL_ATTR);
    component.urlLarge = element.getAttribute(URL_LARGE_ATTR);

    var iframeEl = $('<iframe frameborder="0" width="100%"/>');
    iframeEl.appendTo(COMPONENT_SELECTOR);
    component.iframe = component.$element.find('iframe');

    if (viewport >= bpMortgage) {
        component.iframe.attr({
            src: component.urlLarge,
            width: '100%'
        });
    } else {
        component.iframe.attr({
            src: component.urlSmall,
            width: '100%'
        });
    }
};

// turn all elements with the default selector into components
if (COMPONENT_SELECTOR.length) {
    $(COMPONENT_SELECTOR).each(function (index, element) {
        return new MaandlastenIndicator(element);
    });
}

},{"jquery":"jquery"}],251:[function(require,module,exports){
// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

// explicitly inject dependencies (alphabetically), only those needed

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// what does this module expose?
exports.default = MakelaarAds;

// component configuration

var COMPONENT_SELECTOR = '[data-makelaar-ads]';
var ITEM_SELECTOR = '[data-makelaar-ads-item]';
var ITEM_IMAGE_SELECTOR = '[data-makelaar-ads-logo]';
var ITEM_TITLE_SELECTOR = '[data-makelaar-ads-title]';
var ITEM_BODY_LINE1_SELECTOR = '[data-makelaar-ads-line1]';
var ITEM_BODY_LINE2_SELECTOR = '[data-makelaar-ads-line2]';
var ITEM_WEBSITE_SELECTOR = '[data-makelaar-ads-website]';
var MAKELAARADS_FEED_URL = 'data-makelaars-ads-feed-url';
var MAKELAARADS_CLICK_URL = 'data-makelaars-ads-click-url';
var ITEM_HEADER_SELECTOR = '[makelaar-ads-header]';
var MAKELAARADS_ASYNC_URL_SELECTOR = '[data-makelaarads-feed-async]';
var EVENT_NAMESPACE_MAKELAARAD = '.makelaar-ads-item';

function MakelaarAds(element, baseUrl) {
    var component = this;
    component.urlClick = element.getAttribute(MAKELAARADS_CLICK_URL);
    component.$element = (0, _jquery2.default)(element);
    component.$items = component.$element.find(ITEM_SELECTOR);
    component.ads = []; // init we have 0 ads

    component.resetAds();
    component.loadAds(component.visibleItems(), baseUrl);
    component.handleClickEvent();
}

MakelaarAds.prototype.resetAds = function () {

    var component = this;
    var items = component.$items;

    for (var index = 0; index < items.length; index++) {
        var item = items[index];
        (0, _jquery2.default)(item).find(ITEM_TITLE_SELECTOR).empty();
        (0, _jquery2.default)(item).find(ITEM_IMAGE_SELECTOR).attr('src', '');
        (0, _jquery2.default)(item).find(ITEM_BODY_LINE1_SELECTOR).empty();
        (0, _jquery2.default)(item).find(ITEM_BODY_LINE2_SELECTOR).empty();
        (0, _jquery2.default)(item).find(ITEM_WEBSITE_SELECTOR).html('');
        (0, _jquery2.default)(item).attr('href', null);
        (0, _jquery2.default)(item).attr('class', 'makelaar-ads-item');
        (0, _jquery2.default)(item).attr('data-globalId', '');
        (0, _jquery2.default)(item).find(ITEM_IMAGE_SELECTOR).remove();
    }

    component.$element.addClass('reset-ads');
};

MakelaarAds.prototype.visibleItems = function () {
    var component = this;
    return component.$items.filter(':visible');
};

MakelaarAds.prototype.loadAds = function (visibleAds, baseUrl) {
    var component = this;
    var adsToRequest = visibleAds.length;
    var feedUrl = baseUrl + '&count=' + adsToRequest;
    component.loadAdsAjax(feedUrl);
};

MakelaarAds.prototype.loadAdsAjax = function (feedUrl) {
    var component = this;
    _jquery2.default.get({
        url: feedUrl,
        crossDomain: true,
        dataType: 'json',
        success: function success(data) {
            component.setAds(data.MakelaarAds);
            component.togglePromoAdForMakelaarIfSpaceAvailable();
        }
    });
};

MakelaarAds.prototype.updateObjects = function (data) {
    return data;
};

MakelaarAds.prototype.setAds = function (ads) {
    var component = this;

    for (var index = 0; index < ads.length; index++) {
        var slot = component.getAvailableSlot();
        var mad = ads[index];

        //If the ad is not beeing displayed yet, render it
        if (component.isAdAlreadyBeingDisplayed(mad) === false) {
            this.renderAd(slot, mad);
        }
    }

    if (ads.length > 0) {
        component.$element.removeClass('reset-ads');
        component.$element.addClass('injected');
    }

    _jquery2.default.each(ads, function () {
        component.ads.push(this);
    }, this);
};

MakelaarAds.prototype.togglePromoAdForMakelaarIfSpaceAvailable = function () {
    var component = this;
    //If we have any ads set header and add border to each ad
    if (component.ads.length > 0) {
        component.$element.find(ITEM_HEADER_SELECTOR).css('max-height', 'inherit');
        component.$element.find(ITEM_HEADER_SELECTOR).show();
        // If there's only 1 ad but we have 2 or 3 avaiable slots, we show the ad and on the 2nd slot we show the ad promotion for makelaars
        if (component.ads.length === 1 && component.visibleItems().length > 1) {
            var promoAd = {
                GlobalId: 0,
                Titel: 'Bent u ook een NVM makelaar?',
                TekstRegels: ['Adverteer hier met uw kantoor in', 'uw werkgebied'],
                LinkText: 'Klik voor meer informatie',
                ImageUrl: 'data:image/svg+xml;charset=US-ASCII,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20xlink%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink%22%20viewBox%3D%220%200%2048%2048%22%3E%3Csymbol%20id%3D%22e%22%20viewBox%3D%220%20-48%2048%2048%22%20fill%3D%22%233888C5%22%3E%3Cpath%20fill%3D%22none%22%20d%3D%22M0%200v-48h48V0z%22%3E%3C%2Fpath%3E%3C%2Fsymbol%3E%3Csymbol%20id%3D%22f%22%20viewBox%3D%22-0.0002%20-47.9998%2048%2048%22%20fill%3D%22%233888C5%22%3E%3Cdefs%20fill%3D%22%233888C5%22%3E%3Cpath%20id%3D%22a%22%20d%3D%22M48-48H0V0h48z%22%20fill%3D%22%233888C5%22%3E%3C%2Fpath%3E%3C%2Fdefs%3E%3CclipPath%20id%3D%22b%22%20fill%3D%22%233888C5%22%3E%3Cuse%20href%3D%22%23a%22%20overflow%3D%22visible%22%20fill%3D%22%233888C5%22%3E%3C%2Fuse%3E%3Cg%20clip-path%3D%22url%28%23b%29%22%20fill%3D%22%233888C5%22%3E%3Cg%20fill%3D%22none%22%20stroke%3D%22%233888C5%22%20stroke-width%3D%22.25%22%20stroke-miterlimit%3D%2210%22%3E%3Cpath%20d%3D%22M2-48V0M4-48V0M6-48V0M8-48V0M10-48V0M12-48V0M14-48V0M16-48V0M18-48V0M20-48V0M22-48V0M24-48V0M26-48V0M28-48V0M30-48V0M32-48V0M34-48V0M36-48V0M38-48V0M40-48V0M42-48V0M44-48V0M46-48V0%22%20fill%3D%22%233888C5%22%3E%3C%2Fpath%3E%3C%2Fg%3E%3Cg%20fill%3D%22none%22%20stroke%3D%22%233888C5%22%20stroke-width%3D%22.25%22%20stroke-miterlimit%3D%2210%22%3E%3Cpath%20d%3D%22M0-2h48M0-4h48M0-6h48M0-8h48M0-10h48M0-12h48M0-14h48M0-16h48M0-18h48M0-20h48M0-22h48M0-24h48M0-26h48M0-28h48M0-30h48M0-32h48M0-34h48M0-36h48M0-38h48M0-40h48M0-42h48M0-44h48M0-46h48%22%20fill%3D%22%233888C5%22%3E%3C%2Fpath%3E%3C%2Fg%3E%3Cpath%20d%3D%22M47.75-.25v-47.5H.25v47.5h47.5M48%200H0v-48h48V0z%22%20fill%3D%22%233888C5%22%3E%3C%2Fpath%3E%3C%2Fg%3E%3C%2FclipPath%3E%3C%2Fsymbol%3E%3Csymbol%20id%3D%22g%22%20viewBox%3D%22-0.0002%20-48.0001%2048%2048%22%20fill%3D%22%233888C5%22%3E%3Cg%20opacity%3D%22.4%22%20fill%3D%22%233888C5%22%3E%3Cdefs%20fill%3D%22%233888C5%22%3E%3Cpath%20id%3D%22c%22%20opacity%3D%22.4%22%20d%3D%22M0-48V0h48v-48z%22%20fill%3D%22%233888C5%22%3E%3C%2Fpath%3E%3C%2Fdefs%3E%3CclipPath%20id%3D%22d%22%20fill%3D%22%233888C5%22%3E%3Cuse%20href%3D%22%23c%22%20overflow%3D%22visible%22%20fill%3D%22%233888C5%22%3E%3C%2Fuse%3E%3Cpath%20clip-path%3D%22url%28%23d%29%22%20fill%3D%22none%22%20stroke%3D%22%233888C5%22%20stroke-width%3D%22.25%22%20stroke-miterlimit%3D%2210%22%20d%3D%22M24%200v-48M48-24H0M48-16H0M48-32H0M32-48V0M16-48V0M47.75-.25L.25-47.75M.25-.25l47.5-47.5M24-14c-5.522%200-10-4.477-10-10%200-5.522%204.478-10%2010-10%205.523%200%2010%204.478%2010%2010%200%205.523-4.477%2010-10%2010z%22%3E%3C%2Fpath%3E%3Cpath%20d%3D%22M24-4C12.946-4%204-12.947%204-24s8.946-20%2020-20c11.053%200%2020%208.947%2020%2020S35.052-4%2024-4z%22%20clip-path%3D%22url%28%23d%29%22%20fill%3D%22none%22%20stroke%3D%22%233888C5%22%20stroke-width%3D%22.25%22%20stroke-miterlimit%3D%2210%22%3E%3C%2Fpath%3E%3Cpath%20d%3D%22M40-42H8c-1.1%200-2%20.9-2%202v32c0%201.1.9%202%202%202h32c1.1%200%202-.9%202-2v-32c0-1.1-.9-2-2-2z%22%20clip-path%3D%22url%28%23d%29%22%20fill%3D%22none%22%20stroke%3D%22%233888C5%22%20stroke-width%3D%22.25%22%20stroke-miterlimit%3D%2210%22%3E%3C%2Fpath%3E%3Cpath%20d%3D%22M38-44H10c-1.1%200-2%20.9-2%202v36c0%201.1.9%202%202%202h28c1.1%200%202-.9%202-2v-36c0-1.1-.9-2-2-2z%22%20clip-path%3D%22url%28%23d%29%22%20fill%3D%22none%22%20stroke%3D%22%233888C5%22%20stroke-width%3D%22.25%22%20stroke-miterlimit%3D%2210%22%3E%3C%2Fpath%3E%3Cg%20clip-path%3D%22url%28%23d%29%22%20fill%3D%22%233888C5%22%3E%3Cpath%20d%3D%22M47.75-.25v-47.5H.25v47.5h47.5M48%200H0v-48h48V0z%22%20fill%3D%22%233888C5%22%3E%3C%2Fpath%3E%3C%2Fg%3E%3Cpath%20d%3D%22M42-40H6c-1.1%200-2%20.9-2%202v28c0%201.1.9%202%202%202h36c1.1%200%202-.9%202-2v-28c0-1.1-.9-2-2-2z%22%20clip-path%3D%22url%28%23d%29%22%20fill%3D%22none%22%20stroke%3D%22%233888C5%22%20stroke-width%3D%22.25%22%20stroke-miterlimit%3D%2210%22%3E%3C%2Fpath%3E%3C%2FclipPath%3E%3C%2Fg%3E%3C%2Fsymbol%3E%3Cg%20fill%3D%22%233888C5%22%3E%3Cpath%20d%3D%22M9.474%2043.959v-7.755h3.068l2.887%205.37h.03v-5.37h2.12v7.755h-2.992l-2.963-5.671h-.03v5.67h-2.12zM18.616%2036.204h2.361l1.684%205.639h.03l1.895-5.64h2.24l-2.871%207.756h-2.662l-2.677-7.755zM27.804%2043.959v-7.755h3.654l1.7%205.273h.03l1.804-5.273h3.533v7.755h-2.21v-5.983h-.03l-2.09%205.983h-2.166l-1.985-5.983h-.03v5.983h-2.21zM36.4%204.042a2.11%202.11%200%200%201%202.11%202.11V32.23a1.17%201.17%200%200%201-1.17%201.17h-6.433a1.17%201.17%200%200%201-1.17-1.17v-8.955a2.409%202.409%200%200%201%202.408-2.408h.24a3.94%203.94%200%201%200-3.94-3.94l.013%2015.303a1.17%201.17%200%200%201-1.17%201.17h-6.422a1.17%201.17%200%200%201-1.17-1.17l.014-15.304a3.94%203.94%200%201%200-3.941%203.94h.24a2.409%202.409%200%200%201%202.408%202.41v8.954a1.17%201.17%200%200%201-1.17%201.17h-6.432a1.17%201.17%200%200%201-1.17-1.17V6.151a2.11%202.11%200%200%201%202.11-2.11l8.675.011a3.074%203.074%200%200%201%203.074%203.074l-.156.05a3.886%203.886%200%200%200%20.735%207.7h-.01a3.884%203.884%200%200%200%20.733-7.7l-.156-.05a3.074%203.074%200%200%201%203.074-3.074l8.676-.01z%22%20fill%3D%22%233888C5%22%3E%3C%2Fpath%3E%3C%2Fg%3E%3C%2Fsvg%3E',
                Href: '/content/default.aspx?pagina=/nl/algemene-teksten-funda-sites/fundanl/verkoop/makelaarads'
            };
            var slot = component.getAvailableSlot();
            component.renderAd(slot, promoAd);
        }
    }
};

MakelaarAds.prototype.isAdAlreadyBeingDisplayed = function (ad) {
    var component = this;
    var displayedAds = component.visibleItems();

    for (var index = 0; index < displayedAds.length; index++) {
        var slot = displayedAds[index];
        if ((0, _jquery2.default)(slot).attr('data-globalId') === ad.GlobalId) {
            return true;
        }
    }
    return false;
};

MakelaarAds.prototype.getAvailableSlot = function () {
    var component = this;
    var slots = component.visibleItems();

    for (var index = 0; index < slots.length; index++) {
        var slot = slots[index];
        if (slot.getAttribute('data-globalId') === '') {
            return slot;
        }
    }
};

MakelaarAds.prototype.renderAd = function (slot, ad) {
    (0, _jquery2.default)(slot).find('.makelaar-ads-image-wrap').prepend((0, _jquery2.default)('<img data-makelaar-ads-logo class="makelaar-ads-image" src="' + ad.ImageUrl + '">'));
    (0, _jquery2.default)(slot).attr('data-globalId', ad.GlobalId);
    (0, _jquery2.default)(slot).find(ITEM_TITLE_SELECTOR).text(ad.Titel);
    (0, _jquery2.default)(slot).find(ITEM_BODY_LINE1_SELECTOR).text(ad.TekstRegels[0]);
    (0, _jquery2.default)(slot).find(ITEM_BODY_LINE2_SELECTOR).text(ad.TekstRegels[1]);
    (0, _jquery2.default)(slot).find(ITEM_WEBSITE_SELECTOR).text(ad.LinkText);
    (0, _jquery2.default)(slot).addClass('loaded');
    (0, _jquery2.default)(slot).attr('href', ad.Href);
    (0, _jquery2.default)(slot).css('max-height', 'inherit');
    (0, _jquery2.default)(slot).attr('data-index', ad.Index).attr('data-geogebiednaam', ad.GeoGebiedNaam).attr('data-geogebiedniveau', ad.GeoGebiedNiveau).attr('data-makelaarid', ad.MakelaarId);
};

MakelaarAds.prototype.handleClickEvent = function () {
    var component = this;
    component.unregisterClickEvent('click', EVENT_NAMESPACE_MAKELAARAD);
    component.handleElementOnClick();
};

MakelaarAds.prototype.handleElementOnClick = function () {
    var component = this;
    component.$items.on('click', function () {
        var clickedAd = (0, _jquery2.default)(this);
        component.handleClick(clickedAd);
    });
};

MakelaarAds.prototype.handleClick = function (clickedAd) {
    var component = this;
    component.registerClick(clickedAd);
};

MakelaarAds.prototype.unregisterClickEvent = function (eventName) {
    var component = this;
    component.$items.off(eventName);
};

MakelaarAds.prototype.registerClick = function (clickedAd) {
    var component = this;
    var paramArray = {
        makelaarAdId: clickedAd.attr('data-globalId'),
        makelaarAdPosition: clickedAd.attr('data-index') - 1, //This is retarded for impressions the ad position has 0 index based and for recording impressions is 1 index based.
        geoGebiedNaam: clickedAd.attr('data-geogebiednaam'),
        geoGebiedNiveau: clickedAd.attr('data-geogebiedniveau'),
        makelaarId: clickedAd.attr('data-makelaarid')
    };
    console.log('idd', paramArray.makelaarAdId);
    _jquery2.default.ajax({
        url: component.urlClick,
        type: 'POST',
        data: paramArray
    });
};

MakelaarAds.prototype.setCurrentUrl = function (value) {
    return (0, _jquery2.default)(COMPONENT_SELECTOR).attr(MAKELAARADS_FEED_URL, value);
};

MakelaarAds.prototype.getCurrentUrl = function () {
    return (0, _jquery2.default)(COMPONENT_SELECTOR).attr(MAKELAARADS_FEED_URL);
};

MakelaarAds.prototype.getAsyncUrl = function () {
    return (0, _jquery2.default)(MAKELAARADS_ASYNC_URL_SELECTOR).attr('data-makelaarads-feed-async-url');
};

MakelaarAds.prototype.handleMakelaarAdsOnResultsUpdated = function () {
    var makelaarAdsAsyncUrl = MakelaarAds.prototype.getAsyncUrl();
    var makelaarAdsUrl = MakelaarAds.prototype.getCurrentUrl();

    // check if the url is updated
    if (makelaarAdsAsyncUrl != makelaarAdsUrl) {
        MakelaarAds.prototype.createMakelaarAds(makelaarAdsAsyncUrl);
    }

    MakelaarAds.prototype.setCurrentUrl(makelaarAdsAsyncUrl);
};

MakelaarAds.prototype.createMakelaarAds = function (MakelaarAdsAsyncUrl) {
    (0, _jquery2.default)(COMPONENT_SELECTOR).each(function (index, element) {
        return new MakelaarAds(element, MakelaarAdsAsyncUrl);
    });
};

MakelaarAds.initialize = function () {
    // automatically turn all elements with the default selector into components
    (0, _jquery2.default)(COMPONENT_SELECTOR).each(function (index, element) {
        return new MakelaarAds(element, element.getAttribute(MAKELAARADS_FEED_URL));
    });

    (0, _jquery2.default)(document).on('resultsUpdated', function () {
        MakelaarAds.prototype.handleMakelaarAdsOnResultsUpdated();
    });
};

MakelaarAds.initialize();

},{"jquery":"jquery"}],252:[function(require,module,exports){
'use strict';

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

require('./../../components/tooltip/tooltip');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var COMPONENT_SELECTOR = '[data-aanmeld-invoice]';
var CONTAINER_SELECTOR = '[data-aanmeld-invoice-container]';
var INVOICE_INPUT_SELECTOR = '[data-aanmeld-invoice-inputs]';

function MakelaarAanmeldToggle(element) {
    var component = this;
    component.$element = (0, _jquery2.default)(element);
    component.container = (0, _jquery2.default)(CONTAINER_SELECTOR);
    component.inputs = component.container.find(INVOICE_INPUT_SELECTOR);
    component.$element.on('change', function () {
        return component.toggleActive(!element.checked);
    });
    component.toggleActive(!element.checked);
}

MakelaarAanmeldToggle.prototype.toggleActive = function (status) {
    var component = this;
    component.container.toggleClass('is-hidden', status);
    component.inputs.attr('required', !status);
};

(0, _jquery2.default)(COMPONENT_SELECTOR).each(function (index, element) {
    return new MakelaarAanmeldToggle(element);
});

},{"./../../components/tooltip/tooltip":325,"jquery":"jquery"}],253:[function(require,module,exports){
'use strict';

require('../../components/object-description/object-description');

},{"../../components/object-description/object-description":283}],254:[function(require,module,exports){
'use strict';

require('../../components/object-description/object-description');
require('../../components/expandible/expandible');

},{"../../components/expandible/expandible":229,"../../components/object-description/object-description":283}],255:[function(require,module,exports){
'use strict';

require('../../components/log-request/log-request');

},{"../../components/log-request/log-request":248}],256:[function(require,module,exports){
// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

// explicitly inject dependencies (alphabetically), only those needed

var _expandible = require('../expandible/expandible');

var _expandible2 = _interopRequireDefault(_expandible);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var $ = require('jquery');

// what does this module expose?
module.exports = makelaarsFeatures;

// component configuration
var COMPONENT_SELECTOR = '[data-makelaars-features]';
var MIN_FEATURES = 6; // 2 * groups of dt and dd

function makelaarsFeatures(element) {
    var component = {};
    var $element = $(element);
    var numberOfChildren = $element.children().length;
    if (numberOfChildren > MIN_FEATURES) {
        $element.attr('data-expandible', 'makelaars-features');
        component.expandible = new _expandible2.default(element);
    }
    return component;
}

makelaarsFeatures($(COMPONENT_SELECTOR)[0]);

},{"../expandible/expandible":229,"jquery":"jquery"}],257:[function(require,module,exports){
'use strict';

// explicitly inject dependencies (alphabetically), only those needed

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

var _appliedFilters = require('../applied-filters/applied-filters');

var _appliedFilters2 = _interopRequireDefault(_appliedFilters);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// what does this module expose?
module.exports = MakelaarNameInput;

// component configuration
var COMPONENT_NAME = 'makelaars-name';
var MAKELAARS_NAME_ATTR = 'data-' + COMPONENT_NAME;
var COMPONENT_SELECTOR = '[' + MAKELAARS_NAME_ATTR + ']';

var CLEAR_HANDLE_SELECTOR = '[data-makelaars-name-clear-handle]';
var INPUT_SELECTOR = '[data-makelaars-name-input]';
var SUBMIT_EVENT = 'makelaarsnameupdated';
var KEY_CODE_ENTER = 13;
var DIRTY_CLASS = 'is-dirty';
var BLUR_UPDATE_INPUT_DELAY = 250; // ms

function MakelaarNameInput(element) {
    var component = this;
    component.$element = (0, _jquery2.default)(element);
    component.$input = component.$element.find(INPUT_SELECTOR);
    component.$clearHandle = component.$element.find(CLEAR_HANDLE_SELECTOR);
    component.inputValue = component.$input.val() || '';
    component.time = null;

    if (component.inputValue !== '') {
        component.addFilter(component.inputValue);
    }
    component.bindEvents();
}

MakelaarNameInput.prototype.bindEvents = function () {
    var component = this;

    component.$input.on('keydown', function (e) {
        if (e.keyCode === KEY_CODE_ENTER) {
            e.preventDefault();
            component.onUpdateInput();
        }
    });

    component.$input.on('keyup', function () {
        if (component.$input.val() === '') {
            component.clear();
        } else {
            component.$element.addClass(DIRTY_CLASS);
        }
    });

    component.$input.on('blur', function () {
        // if clear handle is clicked the input field is already been emptied; not a new filter will set
        component.time = setTimeout(function () {
            component.onUpdateInput();
        }, BLUR_UPDATE_INPUT_DELAY);
    });

    component.$element.on('resetfilter', function () {
        component.clear();
    });

    component.$clearHandle.on('click', function () {
        component.clear();
    });
};

/**
 * clears the field
 * @param {component} makelaarNameComponent; the makelaar name component it self
 */
MakelaarNameInput.prototype.clear = function (makelaarNameComponent) {
    var component = makelaarNameComponent || this;

    component.$input.val('');
    component.$element.removeClass(DIRTY_CLASS);
    _appliedFilters2.default.remove(COMPONENT_NAME);
    component.$input.trigger(SUBMIT_EVENT);
};

MakelaarNameInput.prototype.onUpdateInput = function () {
    var component = this;
    component.updateFilter();
};

MakelaarNameInput.prototype.updateFilter = function () {
    var component = this;
    var newInputValue = component.$input.val();

    if (newInputValue !== '') {
        component.inputValue = newInputValue;
        component.addFilter(component.inputValue);
    }
    component.$input.trigger(SUBMIT_EVENT);
};

MakelaarNameInput.prototype.addFilter = function (name) {
    var component = this;
    var appliedFilter = {
        filterGroupId: COMPONENT_NAME,
        filterGroupName: component.$element.attr(MAKELAARS_NAME_ATTR),
        filterName: name,
        labelText: name
    };
    _appliedFilters2.default.add(appliedFilter, function () {
        component.clear(component);
    }, true);
};

// turn all elements with the default selector into components
(0, _jquery2.default)(COMPONENT_SELECTOR).each(function (index, element) {
    return new MakelaarNameInput(element);
});

},{"../applied-filters/applied-filters":204,"jquery":"jquery"}],258:[function(require,module,exports){
'use strict';

// explicitly inject dependencies (alphabetically), only those needed

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = Twitterfeed;

// component configuration

var COMPONENT_ATTR = 'data-makelaars-twitterfeed';
var COMPONENT_SELECTOR = '[' + COMPONENT_ATTR + ']';
var LIST_TEMPLATE_SELECTOR = '[data-makelaars-twitterfeed-list]';
var TWEET_TEMPLATE_SELECTOR = '[data-makelaars-twitterfeed-item-template]';
var HAS_RESULTS_CLASS = 'has-results';

function Twitterfeed(element) {
    var $element = (0, _jquery2.default)(element);
    this.$element = $element;
    this.apiEndpoint = $element.attr(COMPONENT_ATTR);
    this.template = $element.find(TWEET_TEMPLATE_SELECTOR).html();
    this.$list = $element.find(LIST_TEMPLATE_SELECTOR);
    this.getTweets();
}

Twitterfeed.prototype.getTweets = function () {
    var _this = this;

    _jquery2.default.ajax(this.apiEndpoint).then(function (response) {
        if (!response.tweets.length) {
            throw Error('No tweets');
        }
        return response;
    }).then(function (response) {
        var template = Twitterfeed.render(_this.template, {
            user: response.user,
            profileImage: response.profileImage
        });
        var html = Twitterfeed.renderTweets(template, response.tweets);
        _this.update(html);
    }).catch(function (err) {
        return console.error(err);
    });
};

Twitterfeed.prototype.update = function (html) {
    this.$list.html(html);
    this.$element.addClass(HAS_RESULTS_CLASS);
};

Twitterfeed.render = function (template, data) {
    return Object.keys(data).reduce(function (html, prop) {
        var pattern = new RegExp('{' + prop + '}', 'g');
        return html.replace(pattern, data[prop]);
    }, template);
};

Twitterfeed.renderTweets = function (template, tweets) {
    return tweets.map(function (item) {
        return Twitterfeed.render(template, item);
    }).join('');
};

(0, _jquery2.default)(COMPONENT_SELECTOR).each(function (index, element) {
    return new Twitterfeed(element);
});

},{"jquery":"jquery"}],259:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
var VIEWED_IDS_KEY = 'viewedIds';
var MAX_VIEWED_ITEMS = 1000;

function MapLocalStorage() {
    this._hasLocalStorage = this.browserHasLocalStorage();
}

/**
 * Determine if localStorage is supported and available
 */
MapLocalStorage.prototype.browserHasLocalStorage = function () {
    try {
        var test = '__storage_test__';
        window.localStorage.setItem(test, test);
        window.localStorage.removeItem(test);
        return true;
    } catch (e) {
        this.logError(e.message);
        return false;
    }
};

/**
 * Stores the id of a viewed object
 */
MapLocalStorage.prototype.storeViewedId = function (id) {
    if (!this._hasLocalStorage) {
        return;
    }

    var viewed = this.getViewed();
    // act when not already present
    if (id in viewed) {
        return;
    }
    // delete oldest item from storage
    if (Object.keys(viewed).length >= MAX_VIEWED_ITEMS) {
        this.deleteOldestViewedItem();
    }
    viewed[id] = {
        ts: parseInt(new Date().getTime() / 1000, 10)
    };
    this.setViewed(viewed);
};

/**
 * returns true if object is present in localstorage
 */
MapLocalStorage.prototype.isViewed = function (objectId) {
    var capped = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

    if (!this._hasLocalStorage || capped) {
        return false;
    }

    var markerId = objectId;
    var singleId = objectId.indexOf('-') < 0;
    var viewed = this.getViewed();

    if (viewed.length == 0) {
        return false;
    }
    if (!singleId) {
        var markerIds = markerId.split('-');
        for (var i = 0; i < markerIds.length; i++) {
            if (!(markerIds[i] in viewed)) {
                return false;
            }
        }
        return true;
    }

    return markerId in viewed;
};

/**
 * Retrieve array from storage (where data is delimited by SPLIT_CHAR)
 */
MapLocalStorage.prototype.getViewed = function () {
    var data = window.localStorage.getItem(VIEWED_IDS_KEY);
    if (data) {
        return JSON.parse(data);
    }
    return {};
};

/**
 * Saves array to storage (where data is delimited by SPLIT_CHAR)
 */
MapLocalStorage.prototype.setViewed = function (value) {
    window.localStorage.setItem(VIEWED_IDS_KEY, JSON.stringify(value));
};

/**
 * Remove oldest viewed item from localstorage
 */
MapLocalStorage.prototype.deleteOldestViewedItem = function () {
    var current = this.getViewed();
    var max = 0;
    var maxId = null;
    for (var item in current) {
        if (current[item].ts > max) {
            max = current[item];
            maxId = item;
        }
    }
    if (maxId != null && current[maxId]) {
        delete current[maxId];
    }
    this.setViewed(current);
};

/**
 * log the error for Google Analytics
 * @param {String} errrorType
 */
MapLocalStorage.prototype.logError = function (errorType) {
    if (window.gtmDataLayer !== undefined) {
        window.gtmDataLayer.push({
            'event': 'kaartLocalStorageError',
            'errorType': 'localstorage-' + errorType
        });
    }
};

exports.default = new MapLocalStorage();

},{}],260:[function(require,module,exports){
// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _isFunction = require('lodash/isFunction');

var _isFunction2 = _interopRequireDefault(_isFunction);

var _union = require('lodash/union');

var _union2 = _interopRequireDefault(_union);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function MapEventProxy() {
    this._map = null;
    this._handlerId = 0;
    this._proxiedHandlers = {
        'click': {},
        'dblclick': {},
        'center_changed': {},
        'zoom_changed': {},
        'idle': {}
    };
    this._eventForwarding = true;
    this._enableProxyAfterIdle = false;
}

/**
 * Bind to map and proxy events
 */
MapEventProxy.prototype.setMapInstance = function (map) {
    if (map === undefined || map === null) {
        console.error('Invalid map instance');
    }
    var component = this;
    var google = window.google;

    component._map = map;
    // immediately hook all events

    var _loop = function _loop(eventName) {
        google.maps.event.addListener(component._map, eventName, function (args) {
            component._proxyEvent(eventName, args);
        });
    };

    for (var eventName in component._proxiedHandlers) {
        _loop(eventName);
    }
};

/**
 * Adds listener as target for proxied event
 */
MapEventProxy.prototype.addListener = function (eventName, handler) {
    if (!this._checkAddListenerParameters(eventName, handler)) return;

    return this._actuallyAddListener(eventName, handler);
};

/**
 * Adds listener as target for proxied event (fires once)
 */
MapEventProxy.prototype.addListenerOnce = function (eventName, handler) {
    if (!this._checkAddListenerParameters(eventName, handler)) return;

    return this._actuallyAddListener(eventName, handler, true);
};

/**
 * Verify if handler can be registered
 */
MapEventProxy.prototype._checkAddListenerParameters = function (eventName, handler) {
    if (!(eventName in this._proxiedHandlers)) {
        console.error('Cannot attach to event: \'' + eventName + '\'');
        return false;
    }
    if (!(0, _isFunction2.default)(handler)) {
        console.error('handler is not a function');
        return false;
    }
    return true;
};

/**
 * Store handler to be proxied
 */
MapEventProxy.prototype._actuallyAddListener = function (eventName, handler) {
    var runOnce = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

    this._proxiedHandlers[eventName][++this._handlerId] = {
        function: handler,
        runOnce: runOnce
    };
    return this._handlerId;
};

/**
 * Enable/disable triggering events on registered listeners
 */
MapEventProxy.prototype.forwardProxiedEvents = function () {
    var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;

    if (!state) {
        // disable takes effect immediately
        this._eventForwarding = state;
    } else {
        // enable will be triggered after idle event (so map can settle after moving to previous location)
        this._enableProxyAfterIdle = true;
    }
};

/**
 * Remove listener
 */
MapEventProxy.prototype.removeListener = function (handlerId) {
    if (!this._isHandlerRegistered(handlerId)) return;

    var eventName = this._getHandlerEventName(handlerId);
    if (eventName === null) return;

    delete this._proxiedHandlers[eventName][handlerId];
};

/**
 * Checks if handler is registered
 */
MapEventProxy.prototype._isHandlerRegistered = function (handlerId) {
    // collect all ids from proxiedHandlers
    var allIds = [];
    for (var eventName in this._proxiedHandlers) {
        allIds = (0, _union2.default)(allIds, Object.keys(this._proxiedHandlers[eventName]));
    }

    return allIds.indexOf(handlerId) >= 0;
};

/**
 * Get event name for given handlerId
 */
MapEventProxy.prototype._getHandlerEventName = function (handlerId) {
    // collect all ids from proxiedHandlers
    for (var eventName in this._proxiedHandlers) {
        if (handlerId in this._proxiedHandlers[eventName]) {
            return eventName;
        }
    }
    return null;
};

/**
 * Forward events to register listeners
 */
MapEventProxy.prototype._proxyEvent = function (eventName, args) {
    var component = this;

    // skip not forwarding or when no handlers registered
    if (!this._eventForwarding) {
        // check if re-enaling events is requested (only on force idle event)
        if (this._enableProxyAfterIdle && eventName == 'idle') {
            this._eventForwarding = true;
            this._enableProxyAfterIdle = false;
        }
        return;
    }
    // skip if no events registered
    if (Object.keys(component._proxiedHandlers[eventName]).length == 0) return;

    for (var handlerId in component._proxiedHandlers[eventName]) {
        var handlerObject = component._proxiedHandlers[eventName][handlerId];
        handlerObject.function(args);
        if (handlerObject.runOnce) {
            delete component._proxiedHandlers[eventName][handlerId];
        }
    }
};

exports.default = new MapEventProxy();

},{"lodash/isFunction":173,"lodash/union":193}],261:[function(require,module,exports){
// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _assign = require('lodash/assign');

var _assign2 = _interopRequireDefault(_assign);

var _reduce = require('lodash/reduce');

var _reduce2 = _interopRequireDefault(_reduce);

var _isEqual = require('lodash/isEqual');

var _isEqual2 = _interopRequireDefault(_isEqual);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// what does this module expose?
exports.default = MapEvents;


var HISTORY_PUSH_STATE = 'push_state_set_by_funda';
var UPDATE_HASH_DELAY = 750;

function MapEvents() {
    MapEvents.lastMapEvent = {
        center: null,
        zoom: null
    };
    MapEvents.eventHandle = null;
}

/**
 * Update the hash with new data, overwrite the old data when available
 * @param {object} mapData
 */
MapEvents.updateHash = function (currentState, newState) {
    var diffKeys = (0, _reduce2.default)(currentState, function (result, value, key) {
        return (0, _isEqual2.default)(value, newState[key]) ? result : result.concat(key);
    }, []);
    if (diffKeys.length == 0) return;

    // handle changes in id directly
    if (diffKeys.indexOf('id') >= 0) {
        MapEvents.setNewHash(newState);
    }

    if (diffKeys.indexOf('center') >= 0 || diffKeys.indexOf('zoom') >= 0) {
        // always register new location/zoom
        MapEvents.registerMapEvent(newState);
        // only trigger update of hash once per UPDATE_HASH_DELAY
        if (MapEvents.eventHandle == null) {
            MapEvents.eventHandle = window.setTimeout(function () {
                MapEvents.setNewHash(MapEvents.lastMapEvent);
                MapEvents.eventHandle = null;
            }, UPDATE_HASH_DELAY);
        }
    }
};

MapEvents.setNewHash = function (newState) {
    var newHash = MapEvents.createHash(newState);
    // set new generated hash
    if (newHash !== '') {
        var url = window.location.pathname;
        window.history.pushState(HISTORY_PUSH_STATE, window.title, url + newHash);
    }
};

MapEvents.registerMapEvent = function (newState) {
    MapEvents.lastMapEvent = {
        center: newState.center,
        zoom: newState.zoom
    };
};

/**
 * Get the hash and returns as object
 * @returns {object} hash
 */
MapEvents.getHash = function () {
    var hash = window.location.search || null;
    var hashObj = {};
    if (hash !== null) {
        hash = hash.replace('?', '');
        var hashArr = hash.split('&');

        for (var hashOption in hashArr) {
            var option = hashArr[hashOption].split('=');
            hashObj[option[0]] = option[1];
        }
    }
    return hashObj;
};

/**
 * Returns a value from the hash
 * @param {string} key
 * @returns {data}
 */
MapEvents.getValueFromHash = function (key) {
    var hashData = MapEvents.getHash();
    return hashData[key] !== undefined ? hashData[key] : null;
};

/**
 * Get current search query fom url
 */
MapEvents.getCurrentSearchFromUrl = function () {
    return window.location.pathname.replace('/kaart', '');
};

/**
 * Create a new hash from new and old map data
 * @param {object} mapData
 * @returns {string} hash
 */
MapEvents.createHash = function (newState) {
    var hashData = MapEvents.getHash();

    // update center to usable format
    if (Object.keys(newState).indexOf('center') >= 0) {
        // don't modify center objct from newState
        var _hashValues = (0, _assign2.default)({}, hashData);
        _hashValues.zoom = newState.zoom;
        _hashValues.id = newState.id;
        _hashValues.center = newState.center.lat() + ',' + newState.center.lng();
        return MapEvents.objectToQueryString(_hashValues);
    }

    var hashValues = (0, _assign2.default)({}, hashData, newState);
    return MapEvents.objectToQueryString(hashValues);
};

MapEvents.objectToQueryString = function (data) {
    // create query string (skipping invalid values)
    var newHash = '';
    for (var key in data) {
        if (data[key] !== undefined && data[key] !== null) {
            var divider = newHash === '' ? '?' : '&';
            newHash += divider + key + '=' + data[key];
        }
    }
    return newHash;
};

},{"lodash/assign":154,"lodash/isEqual":172,"lodash/reduce":185}],262:[function(require,module,exports){
// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

var _mapTileServer = require('./map-tile-server');

var _mapTileServer2 = _interopRequireDefault(_mapTileServer);

var _ajax = require('../ajax/ajax');

var _ajax2 = _interopRequireDefault(_ajax);

var _mapEventProxy = require('../map/map-event-proxy');

var _mapEventProxy2 = _interopRequireDefault(_mapEventProxy);

var _mapTileCache = require('../map/map-tile-cache');

var _mapTileCache2 = _interopRequireDefault(_mapTileCache);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// what does this module expose?
exports.default = MapMarkerOverlay;


var MAP_SELECTOR = '[data-map]';
var TILE_ID = 'tile-';
var TIMEOUT_SEARCH_REQUEST = 30000; // 30 sec
var MAP_CLICK_HANDLER_TIMEOUT = 200; // msec

var SHOW_OBJECT_INFO_EVENT = 'show_object_info';
var HIDE_OBJECT_INFO_EVENT = 'hide_object_info';
var ZOOM_CHANGED_EVENT = 'zoom_changed';
var CENTER_CHANGED_EVENT = 'center_changed';
var NO_OBJECT_EVENT = 'no_object';
var SEARCH_QUERY_UPDATED_EVENT = 'zo_updated';
var FORCE_REFRESH_MAPTILES_EVENT = 'force_maptile_refresh';
var DEFAULT_SEARCH_QUERY = 'koop/heel-nederland';

var MIN_ZOOM = 8;
var MAX_ZOOM = 19;

/**
 * @param {Object} mapData; required {map: *, config: *, tileSize: *}
 * @param {Component} marker component; to create a marker on the map
 * @constructor
 */
function MapMarkerOverlay(mapData, markerComponent) {
    var component = this;
    component.map = mapData.map;
    component.$map = (0, _jquery2.default)(MAP_SELECTOR);
    component.tileSize = mapData.tileSize;
    component.cluster = mapData.cluster;
    component.marker = markerComponent;
    component.searchQuery = window.location.pathname.replace('/kaart', '') || DEFAULT_SEARCH_QUERY;
    // event proxy
    component.mapEventProxy = _mapEventProxy2.default;
    // caching
    component.mapTileCache = _mapTileCache2.default;
    //config settings for creating the tile data url
    var config = mapData.config;
    component.tileServer = new _mapTileServer2.default(config.tileServers, config.tilePath, { min: MIN_ZOOM, max: MAX_ZOOM });
    component.singleClicked = null;
    component.clickedMarker = null;

    if (component.map !== null && component.marker !== null) {
        component.bindEvents();
    }
}
// Default GoogleMap API overlay settings
MapMarkerOverlay.prototype.minZoom = MIN_ZOOM;
MapMarkerOverlay.prototype.maxZoom = MAX_ZOOM;
MapMarkerOverlay.prototype.name = 'Tile #s';
MapMarkerOverlay.prototype.alt = 'Tile Coordinate Map Type';

/**
 * Get the tile from current x, y position; Default Google API call
 * @param {Number} zoom
 * @param {Object} coord {x: *, y, *}
 * @param {Document} ownerDocument
 * @returns {Html} The tile on the map
 */
MapMarkerOverlay.prototype.getTile = function (coord, zoom, ownerDocument) {
    var component = this;
    var tileId = TILE_ID + zoom + '-' + coord.x + '-' + coord.y;

    var tileContainer = ownerDocument.createElement('div');
    tileContainer.setAttribute('id', tileId);
    tileContainer.setAttribute('class', 'map-tile');

    if (component.tileServer.isValidTile(zoom, coord)) {
        component.loadMarker(zoom, coord, tileId, tileContainer);
    }
    return tileContainer;
};

/**
 * Refresh the tiles by updating the searchQuery; clean the cache and change the zoom level.
 * For the user it doesn't like that the zoom level is changed
 * @param {String} searchQuery
 */
MapMarkerOverlay.prototype.refreshTiles = function (searchQuery) {
    var component = this;
    component.searchQuery = searchQuery;
    // Remove stored data to get new map tiles
    component.mapTileCache.clear();

    // temporarily disable zoom limiting
    component.$map.trigger(FORCE_REFRESH_MAPTILES_EVENT, { isFinished: false });
    // zoom in and out to forec refresh of tiles
    var currentZoom = component.map.getZoom();
    component.map.setZoom(currentZoom + 1);
    // Change the zoom again and load fresh tiles
    component.map.setZoom(currentZoom);
    // re-enable zoom limiting
    component.$map.trigger(FORCE_REFRESH_MAPTILES_EVENT, { isFinished: true });
};

/**
 *eventbinding
 */
MapMarkerOverlay.prototype.bindEvents = function () {
    var component = this;
    component.mapEventProxy.addListener('click', function (event) {
        return component.mapClickHandler(event);
    });
    component.mapEventProxy.addListener('dblclick', function () {
        return component.clearMapClick();
    });
    component.mapEventProxy.addListener(CENTER_CHANGED_EVENT, function () {
        return component.hideObjectInfo(CENTER_CHANGED_EVENT);
    });
    component.mapEventProxy.addListener(ZOOM_CHANGED_EVENT, function () {
        // Empty stored markers
        component.mapTileCache.clear();
        // hide marker info
        component.hideObjectInfo(ZOOM_CHANGED_EVENT);
    });
    // filters in the sidebar are changed
    component.$map.on(SEARCH_QUERY_UPDATED_EVENT, function (event, eventArgs) {
        return component.refreshTiles(eventArgs.zo);
    });
};

/**
 * Load the markers. Determine if current marker is stored, otherwise load from server
 * @param {Number} zoom
 * @param {Object} coord {x: *, y, *}
 * @param {String} tileId
 * @param {Html} tileHtml
 */
MapMarkerOverlay.prototype.loadMarker = function (zoom, coord, tileId, tileHtml) {
    var component = this;
    var storedMarker = component.mapTileCache.getStoredTiles(tileId);

    if (Object.keys(storedMarker).length > 0) {
        // set stored marker on map
        return component.setMarker(storedMarker, tileHtml);
    } else {
        // get the marker from the server
        component.doRequest(zoom, coord, tileId, tileHtml);
    }
};

/**
 * Load the markers from server
 * @param {Number} zoom
 * @param {Object} coord {x: *, y, *}
 * @param {String} tileId
 * @param {Html} tileHtml
 */
MapMarkerOverlay.prototype.doRequest = function (zoom, coord, tileId, tileHtml) {
    var component = this;
    var url = component.tileServer.createUrl(zoom, coord, component.searchQuery, component.cluster);
    var zoomLevel = component.map.getZoom();

    component.request = (0, _ajax2.default)({
        url: url,
        method: 'GET',
        dataType: 'jsonp',
        cache: true,
        timeout: TIMEOUT_SEARCH_REQUEST
    });
    component.request.done(function (resultData) {
        if (resultData !== undefined && resultData !== null) {
            // store locally
            component.mapTileCache.storeMarker(tileId, resultData, zoomLevel);
            // set marker on map
            return component.setMarker(resultData, tileHtml);
        }
    });
};

/**
 * Set the marker in the tile
 * @param {Object} tileData
 * @param {Html} mapTile
 */
MapMarkerOverlay.prototype.setMarker = function (tileData, tileHtml) {
    var component = this;
    var objects = tileData.points;

    if (objects !== undefined && objects.length > 0) {
        for (var i = 0, n = objects.length; i < n; i++) {
            // This is the object we've found.
            var object = objects[i];
            // Create and position an HTML element.
            if (tileHtml !== undefined && object !== undefined) {
                tileHtml.appendChild(component.marker.getElement(object));
            }
        }
    }
    return tileHtml;
};

/**
 * used for getting a 'clean' click event on map
 */
MapMarkerOverlay.prototype.clearMapClick = function () {
    var component = this;
    component.singleClicked = false;
};

/**
 * handler for map click event
 */
MapMarkerOverlay.prototype.mapClickHandler = function (event) {
    var component = this;
    component.singleClicked = true;

    setTimeout(function () {
        if (component.singleClicked) {
            component.clickedOnMap(event);
        }
    }, MAP_CLICK_HANDLER_TIMEOUT);
};

/**
 * user clicked on the map, check for a close marker
 * @param {Object} event {latlng: *}
 */
MapMarkerOverlay.prototype.clickedOnMap = function (event) {
    var component = this;
    var latLng = component.fixEventOffset(event.latLng);
    var zoomLevel = component.map.getZoom();

    var nearestObjectId = _mapTileCache2.default.getNearestMarker(latLng, zoomLevel);
    if (!nearestObjectId) {
        component.hideObjectInfo(NO_OBJECT_EVENT);
        return;
    }
    var nearestObject = _mapTileCache2.default.getMarkerById(nearestObjectId);
    if (component.isPixelDistanceClose(latLng, nearestObject)) {
        component.showObjectInfo(nearestObjectId);
    } else {
        component.hideObjectInfo(NO_OBJECT_EVENT);
    }
};

/**
 * remember selected id indefinitely / trigger event
 */
MapMarkerOverlay.prototype.showObjectInfo = function (objectId) {
    var component = this;
    // add object(s) to part of cache that is not cleared on zoom_changed
    component.mapTileCache.rememberSelectObjects(objectId);
    component.$map.trigger(SHOW_OBJECT_INFO_EVENT, { objectId: objectId, initial: false });
};

/**
 * trigger events
 */
MapMarkerOverlay.prototype.hideObjectInfo = function (eventType) {
    var component = this;
    component.$map.trigger(HIDE_OBJECT_INFO_EVENT, { type: eventType });
};

MapMarkerOverlay.prototype.fixEventOffset = function (latLng) {
    var component = this;
    var scale = Math.pow(2, component.map.getZoom());
    var point = this.map.getProjection().fromLatLngToPoint(latLng);
    point.y += component.marker.pixelDistance / scale;
    return this.map.getProjection().fromPointToLatLng(point);
};

/**
 * is the object on pixel distance close
 * @param {Object} latLng {lat: *, lng: *}
 * @param {Object} objectData {x: *, y: *, id: *}
 * @returns {boolean}
 */
MapMarkerOverlay.prototype.isPixelDistanceClose = function (latLng, objectData) {
    var component = this;
    var google = window.google;
    var scale = Math.pow(2, component.map.getZoom());
    var mapProjection = component.map.getProjection();
    var objPoint = mapProjection.fromLatLngToPoint(new google.maps.LatLng(objectData.lat, objectData.lng));
    var eventPoint = mapProjection.fromLatLngToPoint(latLng);
    var objPixel = new google.maps.Point(objPoint.x * scale, objPoint.y * scale);
    var eventPixel = new google.maps.Point(eventPoint.x * scale, eventPoint.y * scale);
    var xPixelDistance = Math.abs(objPixel.x - eventPixel.x);
    var yPixelDistance = Math.abs(objPixel.y - eventPixel.y);

    // http://www.smashingmagazine.com/2012/02/21/finger-friendly-design-ideal-mobile-touchscreen-target-sizes/
    return xPixelDistance <= component.marker.pixelDistance && yPixelDistance <= component.marker.pixelDistance;
};

},{"../ajax/ajax":198,"../map/map-event-proxy":260,"../map/map-tile-cache":263,"./map-tile-server":264,"jquery":"jquery"}],263:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _find = require('lodash/find');

var _find2 = _interopRequireDefault(_find);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function MapTileCache() {
    this._storedTiles = {};
    this._storedMarkers = {};
    this._historyCache = {};
}

/**
 * Stores the tile / markers
 */
MapTileCache.prototype.storeMarker = function (tileId, tileData, zoom) {
    var objects = tileData.points;
    // store tile data
    this._storedTiles[tileId] = tileData;

    if (objects !== undefined) {
        for (var i = 0, n = objects.length; i < n; i++) {
            // This is the object we've found.
            var object = objects[i];
            this._storedMarkers[object.id] = {
                lat: object.lat,
                lng: object.lng,
                zoom: zoom
            };
        }
    }
};

/**
 * Get nearesst marker from cache
 */
MapTileCache.prototype.getNearestMarker = function (clickLatLng, zoom) {
    var google = window.google;

    var minDistance = 7199254740666;
    var nearestId = void 0;
    for (var storeId in this._storedMarkers) {
        var storedObject = this._storedMarkers[storeId];
        if (storedObject && storedObject.zoom == zoom) {
            var objLatLng = new google.maps.LatLng(storedObject.lat, storedObject.lng);
            var dist = google.maps.geometry.spherical.computeDistanceBetween(clickLatLng, objLatLng);
            if (dist < minDistance) {
                minDistance = dist;
                nearestId = storeId;
            }
        }
    }
    for (var _storeId in this._historyCache) {
        var _storedObject = this._storedMarkers[_storeId];
        if (_storedObject && _storedObject.zoom == zoom) {
            var _objLatLng = new google.maps.LatLng(_storedObject.lat, _storedObject.lng);
            var _dist = google.maps.geometry.spherical.computeDistanceBetween(clickLatLng, _objLatLng);
            if (_dist < minDistance) {
                minDistance = _dist;
                nearestId = _storeId;
            }
        }
    }
    return nearestId;
};

/**
 * Fetch marker from cache
 */
MapTileCache.prototype.getMarkerById = function (markerId) {
    return this._historyCache[markerId] || this._storedMarkers[markerId];
};

/**
 * Retrieves markerId from cache (allows patial matching)
 */
MapTileCache.prototype.getMarkerIdByPartial = function (markerId) {
    var ret = (0, _find2.default)(Object.keys(this._storedMarkers), function (m) {
        return m.indexOf(markerId) >= 0;
    });
    if (ret === undefined) {
        ret = (0, _find2.default)(Object.keys(this._historyCache), function (m) {
            return m.indexOf(markerId) >= 0;
        });
    }
    return ret;
};

/**
 * Retrieves marker from cache (allows patial matching)
 */
MapTileCache.prototype.getMarkerByPartial = function (markerId) {
    var foundMarkerId = this.getMarkerIdByPartial(markerId);
    if (foundMarkerId !== undefined) {
        return this._historyCache[foundMarkerId] || this._storedMarkers[foundMarkerId];
    }
};

/**
 * Gets a stored tile
 */
MapTileCache.prototype.getStoredTiles = function (tileId) {
    if (Object.keys(this._storedTiles).length > 0 && this._storedTiles[tileId] !== undefined) {
        return this._storedTiles[tileId];
    } else {
        return {};
    }
};

/**
 * Store given objectId indefinitely (umtil page reload)
 */
MapTileCache.prototype.rememberSelectObjects = function (objectIds) {
    var objectId = (0, _find2.default)(Object.keys(this._storedMarkers), function (m) {
        return m == objectIds;
    });
    if (objectId) {
        this._historyCache[objectIds] = this._storedMarkers[objectId];
    }
};

/**
 * clear local cache
 */
MapTileCache.prototype.clear = function () {
    this._storedMarkers = [];
    this._storedTiles = {};
};

exports.default = new MapTileCache();

},{"lodash/find":162}],264:[function(require,module,exports){
// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

// explicitly inject dependencies (alphabetically), only those needed

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// what does this module expose?
exports.default = MapTileServer;

/**
 * Initiate Map Tile Server
 * @param {Array} tileServers
 * @param {String} tilePath
 * @constructor
 */

function MapTileServer(tileServers, tilePath, zoom) {
    var component = this;
    component.tileServers = tileServers;
    component.tilePath = tilePath;
    component.zoom = zoom;
}

/**
 * Creates the tile url for getting the tile data according the coordination and zoom level
 * @param {Integer} zoom
 * @param {Object} coord {x: *, y, *}
 * @returns {string} the url
 */
MapTileServer.prototype.createUrl = function (zoom, coord, searchQuery, cluster) {
    var component = this;
    var urlAttributes = {
        zoom: { find: '{zoom}', replace: zoom },
        coordX: { find: '{coordx}', replace: coord.x },
        coordY: { find: '{coordy}', replace: coord.y },
        query: { find: '{searchQuery}', replace: encodeURIComponent(searchQuery) },
        cluster: { find: '{cluster}', replace: cluster },
        screen: { find: '{screen}', replace: (0, _jquery2.default)(document).width() < 750 ? 'm' : 'd' }
    };

    var path = component.tilePath;
    var regexPattern = void 0;
    for (var key in urlAttributes) {
        if (urlAttributes[key] !== undefined) {
            regexPattern = new RegExp('(' + urlAttributes[key].find + ')', 'gm');
            path = path.replace(regexPattern, urlAttributes[key].replace);
        }
    }
    // combine domain + path to create the complete url
    return component.getTileServer(coord) + path;
};

/**
 * Returns one domain randomized from the list
 * https://www.maxcdn.com/one/visual-glossary/domain-sharding-2/
 * @param {Object} coord {x: *, y, *}
 * @returns {String} domain
 */
MapTileServer.prototype.getTileServer = function (coord) {
    var component = this;
    var mapTileServers = component.tileServers;
    var pattern = new RegExp('([\/])$', 'g');

    if (mapTileServers.length === 0) {
        return '.';
    } else {
        var tileServer = mapTileServers[(coord.x + coord.y) % mapTileServers.length];
        if (window.location.protocol === 'https:') {
            tileServer = tileServer.replace('http://', 'https://');
        }
        return tileServer.replace(pattern, '');
    }
};

/**
 * Make sure the server load valid tiles, Netherlands only
 * @param {Integer} zoom
 * @param {Object} coord {x: *, y, *}
 */
MapTileServer.prototype.isValidTile = function (zoom, coord) {
    var component = this;
    var x = coord.x;
    var y = coord.y;
    var validTile = true;

    if (zoom < component.zoom.min || zoom > component.zoom.max) {
        validTile = false;
    } else {
        switch (zoom) {
            case 6:
                validTile = x >= 32 && x <= 33 && y >= 20 && y <= 21;
                break;
            case 7:
                validTile = x >= 65 && x <= 66 && y >= 41 && y <= 42;
                break;
            case 8:
                validTile = x >= 130 && x <= 133 && y >= 82 && y <= 85;
                break;
            case 9:
                validTile = x >= 260 && x <= 266 && y >= 165 && y <= 171;
                break;
            case 10:
                validTile = x >= 521 && x <= 532 && y >= 331 && y <= 343;
                break;
            case 11:
                validTile = x >= 1043 && x <= 1065 && y >= 662 && y <= 687;
                break;
            case 12:
                validTile = x >= 2086 && x <= 2130 && y >= 1324 && y <= 1375;
                break;
            case 13:
                validTile = x >= 4172 && x <= 4260 && y >= 2649 && y <= 2751;
                break;
            case 14:
                validTile = x >= 8344 && x <= 8520 && y >= 5298 && y <= 5503;
                break;
            case 15:
                validTile = x >= 16689 && x <= 17041 && y >= 10596 && y <= 11006;
                break;
        }
        //TODO: research boundaries for zoom 16 t/m 20
    }
    return validTile;
};

},{"jquery":"jquery"}],265:[function(require,module,exports){
// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

// explicitly inject dependencies (alphabetically), only those needed

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

var _debounce = require('lodash/debounce');

var _debounce2 = _interopRequireDefault(_debounce);

var _mapEventProxy = require('../map/map-event-proxy');

var _mapEventProxy2 = _interopRequireDefault(_mapEventProxy);

var _mapEvents = require('../map/map-events');

var _mapEvents2 = _interopRequireDefault(_mapEvents);

var _mapLocalstorage = require('../map-localstorage/map-localstorage');

var _mapLocalstorage2 = _interopRequireDefault(_mapLocalstorage);

var _gmapsAsync = require('../gmaps-async/gmaps-async');

var _gmapsAsync2 = _interopRequireDefault(_gmapsAsync);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// what does this module expose?
exports.default = Map;

// component configuration

var CONFIG_SELECTOR = '[data-map-config]';
var FULL_VIEWPORT_HANDLER = '[data-map-full-viewport]';
var CANVAS_SELECTOR = '[data-map-canvas]';
var EVENT_NAMESPACE = '.map';

var MAP_ZOOM_ZOOMIN_SELECTOR = '[data-map-zoom-zoomin]';
var MAP_ZOOM_ZOOMOUT_SELECTOR = '[data-map-zoom-zoomout]';
var MIN_ZOOM = 8;
var MAX_ZOOM = 19;

var HEADER_HEIGHT = 52; // px
var IS_LOADED_CLASS = 'is-loaded';
var IS_TOUCH_DEVICE_CLASS = 'is-touch-device';

var HISTORY_PUSH_STATE = 'push_state_set_by_funda';
var CENTER_MAP_EVENT = 'center_map_on';

var $window = (0, _jquery2.default)(window);
var MINIMUM_INTERVAL_BETWEEN_RESIZE_CANVAS = 250; // ms
var TILE_SIZE = 256;

/**
 * Will create the Google Map
 * @param {Element} element
 * @param {Function} mapLoadedHandler
 * @constructor
 */
function Map(element, mapLoadedHandler) {
    var component = this;
    component.$element = (0, _jquery2.default)(element);
    component.$canvas = component.$element.find(CANVAS_SELECTOR);
    component.config = JSON.parse(component.$element.find(CONFIG_SELECTOR).text());
    component.isFullViewport = component.$element.is(FULL_VIEWPORT_HANDLER);
    // event proxy
    component.mapEventProxy = _mapEventProxy2.default;
    // map data
    component.map = null;
    component.mapState = null;
    component.tileSize = null;
    component.mapLoadedHandler = mapLoadedHandler || null;
    // map controls
    component.isTouchDevice = false;
    component.$zoomIn = component.$element.find(MAP_ZOOM_ZOOMIN_SELECTOR);
    component.$zoomOut = component.$element.find(MAP_ZOOM_ZOOMOUT_SELECTOR);

    if (component.isFullViewport) {
        component.setCanvasHeight();
    }

    // initialize popstate
    window.history.replaceState(HISTORY_PUSH_STATE, window.title);

    component.bindEvents();
}

/**
 * Bind all events and build the map
 * All events are bounded we can load the map. For other load events we can overwrite this function
 */
Map.prototype.bindEvents = function () {
    var component = this;

    component.initiate();
};

Map.prototype.initiate = function () {
    var component = this;
    // Map will be initiated
    (0, _gmapsAsync2.default)().then(function () {
        component.create();
    });
};

/**
 * Set the height of the canvas
 */
Map.prototype.setCanvasHeight = function () {
    var component = this;
    var height = $window.height() - HEADER_HEIGHT;
    component.$element.css({ 'height': height });
};

/**
 * Create and insert map on component's canvas element.
 * @return {*} component's Google Map instance.
 */
Map.prototype.create = function () {
    var component = this;
    var google = window.google;
    // Create the Map with Street View with options
    component.map = new google.maps.Map(component.$canvas[0], component.mapSettings());
    component.tileSize = new google.maps.Size(TILE_SIZE, TILE_SIZE);
    // Map is created, we can bind now the events
    component.mapEventProxy.setMapInstance(component.map);
    component.bindMapEvents();

    var centerPosition = _mapEvents2.default.getValueFromHash('center');
    if (!component.isValidLatlng(centerPosition) && component.config.geo) {
        component.setMapToBounds(component.config.geo);
    }
};

/**
 * Bind all Map events
 */
Map.prototype.bindMapEvents = function () {
    var component = this;

    // On idle state the map is loaded
    // http://stackoverflow.com/questions/832692/how-can-i-check-whether-google-maps-is-fully-loaded
    component.mapEventProxy.addListenerOnce('idle', function () {
        component.$element.addClass(IS_LOADED_CLASS);
        if (component.mapLoadedHandler !== null) {
            component.mapLoadedHandler.apply();
        }
        // inital trigger makes sure current state is saved (it will not update the hash)
        component.recordMapState();
    });
    component.zoomLimitHandlerId = component.mapEventProxy.addListener('zoom_changed', function () {
        var currentZoom = component.map.getZoom();
        if (currentZoom > MAX_ZOOM) {
            component.map.setZoom(MAX_ZOOM);
        } else if (currentZoom < MIN_ZOOM) {
            component.map.setZoom(MIN_ZOOM);
        }
    });

    component.$zoomIn.on('click', function (event) {
        event.preventDefault();
        var currentZoomLevel = component.map.getZoom();

        if (currentZoomLevel != MAX_ZOOM) {
            component.map.setZoom(currentZoomLevel + 1);
        }
    });
    component.$zoomOut.on('click', function (event) {
        event.preventDefault();
        var currentZoomLevel = component.map.getZoom();

        if (currentZoomLevel != MIN_ZOOM) {
            component.map.setZoom(currentZoomLevel - 1);
        }
    });
    component.$element.on(CENTER_MAP_EVENT, function (event, eventArgs) {
        return component.setMapToBounds(eventArgs);
    });

    // https://css-tricks.com/debouncing-throttling-explained-examples/`
    function debouncedResizeCanvas() {
        return (0, _debounce2.default)(function () {
            if (component.isFullViewport) {
                component.setCanvasHeight();
            }
            component.updateCenterMap();
        }, MINIMUM_INTERVAL_BETWEEN_RESIZE_CANVAS, { leading: false, trailing: true });
    }

    $window.on('resize' + EVENT_NAMESPACE, debouncedResizeCanvas()).on('touchstart' + EVENT_NAMESPACE, function () {
        component.setIsTouchDevice();
    });
};

/**
 * When the viewport is resized, update the center of the map
 */
Map.prototype.updateCenterMap = function () {
    var component = this;
    var google = window.google;

    google.maps.event.trigger(component.map, 'resize');
    component.map.setCenter(component.mapState.center);
};

Map.prototype.recordMapState = function (param) {
    var component = this;
    var newState = {
        center: component.map.getCenter(),
        zoom: component.map.getZoom(),
        id: null
    };
    if (param !== undefined && 'id' in param) {
        newState.id = param.id;
        if (param.id) {
            _mapLocalstorage2.default.storeViewedId(param.id);
        }
    }
    if (component.mapState !== null) {
        _mapEvents2.default.updateHash(component.mapState, newState);
    } else {
        var idParam = _mapEvents2.default.getValueFromHash('id');
        if (idParam !== null && idParam !== '') {
            newState.id = idParam;
        }
    }
    component.mapState = newState;
};

/**
 * Determine if device is touch. on touch device the zoom buttons will be hidden
 */
Map.prototype.setIsTouchDevice = function () {
    var component = this;
    component.isTouchDevice = true;
    component.$element.addClass(IS_TOUCH_DEVICE_CLASS);
    $window.off('touchstart' + EVENT_NAMESPACE);
};

/**
 * All configuration settings for the map are set
 * @returns {{center: {lat: *, lng: *}, zoom: number}}
 */
Map.prototype.mapSettings = function () {
    var component = this;
    var config = component.config;
    var google = window.google;

    var center = null;
    var centerPosition = _mapEvents2.default.getValueFromHash('center');
    if (component.isValidLatlng(centerPosition)) {
        var latlng = centerPosition.split(',');
        center = {
            lat: parseFloat(latlng[0]),
            lng: parseFloat(latlng[1])
        };
    } else {
        center = { lat: config.lat, lng: config.lng };
    }

    return {
        center: center,
        zoom: parseInt(_mapEvents2.default.getValueFromHash('zoom'), 10) || 10,
        draggableCursor: 'default',
        draggingCursor: 'default',

        // Map control options
        disableDefaultUI: true,
        // Ignore clicking on public transport icons
        clickableIcons: false,
        zoomControlOptions: {
            style: google.maps.ZoomControlStyle.SMALL
        },

        // Remove POI
        styles: [{
            featureType: 'poi',
            elementType: 'labels',
            stylers: [{ visibility: 'off' }]
        }]
    };
};

/**
 * Determine if given string is a valid latlng
 * @param {string} latlng; Expected: 52.1179698200444,6.092906208496065 (example)
 * @returns {boolean}
 */
Map.prototype.isValidLatlng = function (latlng) {
    // there is no latlng given
    if (latlng === undefined || latlng === null) return false;
    // determine if latlng is valid
    var hasLatlngMatch = latlng.search(/(^[0-9]{1,3}\.)([\d]+)\,([0-9]{1,3}\.)([\d]+)/g);
    return hasLatlngMatch === 0;
};

/**
 * sets map to given bounds
 */
Map.prototype.setMapToBounds = function (geoLocation) {
    var component = this;
    var google = window.google;

    component.map.panTo(new google.maps.LatLng((geoLocation.MaxYWgs + geoLocation.MinYWgs) / 2.0, (geoLocation.MaxXWgs + geoLocation.MinXWgs) / 2.0));
    component.map.fitBounds(new google.maps.LatLngBounds(new google.maps.LatLng(geoLocation.MinYWgs, geoLocation.MinXWgs), new google.maps.LatLng(geoLocation.MaxYWgs, geoLocation.MaxXWgs)));
    component.recordMapState();
};

},{"../gmaps-async/gmaps-async":238,"../map-localstorage/map-localstorage":259,"../map/map-event-proxy":260,"../map/map-events":261,"jquery":"jquery","lodash/debounce":158}],266:[function(require,module,exports){
'use strict';
/* globals embedpano */

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = MediaFoto360;


var load = require('load-script');

require('../krpano-loader/krpano-loader');

var COMPONENT_SELECTOR = '[data-media-foto360]';
var KRPANO_CONTAINER_ID = 'media-foto360-krpano';
var KRPANO_BASE_URL_ATTR = 'data-media-foto360-krpano-base-url';
var FOTO_360_SOURCE = 'data-media-foto360-krpano-source';
var FOTO_360_PREFIX = '360-foto-';

function MediaFoto360(element) {
    var component = this;

    component.$element = (0, _jquery2.default)(element);
    component.baseUrl = component.$element.attr(KRPANO_BASE_URL_ATTR) || '';
    component.src = component.$element.attr(FOTO_360_SOURCE);

    this.initKrpano();

    (0, _jquery2.default)(window).on('krpanohotspotclicked', function (e, data) {
        if (data.name) {
            // targets everything after the last '/' ignoring a final '/' with nothing trailing it
            //      ____          ___
            // /foo/bar/ and /foo/bar
            var urlFotoParameterRegex = new RegExp('([^\/]+\/$)|([^\/]+$)');
            var newLocation = window.location.href.replace(urlFotoParameterRegex, FOTO_360_PREFIX + data.name);

            // See media-viewer-foto-360.js for KRPano navigation instead
            window.location.assign(newLocation);
        }
    });
}

MediaFoto360.prototype.initKrpano = function () {
    var component = this;
    var krpanoContainer = document.createElement('div');
    krpanoContainer.id = KRPANO_CONTAINER_ID;
    component.$element.append(krpanoContainer);

    var onReady = function onReady(krpano) {
        component.krpano = krpano;

        // Hack for the relative path of the stub pano's in the guide.
        // Krpano sets its basedir automatically to the path of the first loaded pano,
        // therefore the next pano's relative path becomes relative to the first one's path.
        krpano.set('basedir', '');
        // end hack
    };

    var debugging = window.location.search.indexOf('krpanodebug=true') !== -1;

    load(component.baseUrl + '/assets/vendor-krpano.js', function (error) {
        if (!error) {
            embedpano({
                html5: 'prefer',
                swf: component.baseUrl + '/assets/components/krpano-loader/krpano.swf',
                xml: component.src,
                target: KRPANO_CONTAINER_ID,
                consolelog: debugging,
                onready: onReady
            });
        }
    });
};

// turn all elements with the default selector into components
(0, _jquery2.default)(COMPONENT_SELECTOR).each(function (index, element) {
    return new MediaFoto360(element);
});

},{"../krpano-loader/krpano-loader":247,"jquery":"jquery","load-script":2}],267:[function(require,module,exports){
'use strict';
/* globals embedpano removepano */

module.exports = Foto360View;

var $ = require('jquery');
var load = require('load-script');
var Menu = require('../media-viewer-menu/media-viewer-menu');

require('../krpano-loader/krpano-loader');

var COMPONENT_SELECTOR = '[data-media-viewer-foto360]';
var FOTO_360_CONTAINER_SELECTOR = '.media-viewer-foto360-container';
var KRPANO_CONTAINER_ID = 'media-viewer-foto360-krpano';
var KRPANO_CONTAINER_OBJECT = '#krpanoSWFObject';
var KRPANO_BASE_URL_ATTR = 'data-media-viewer-foto360-krpano-base-url';
var ITEMS_SELECTOR = '[data-media-viewer-foto360-items]';
var FOTO_360_MENU_HEADER_SELECTOR = '[data-foto360-menu-header]';
var FOTO_360_MENU_FOOTER_SELECTOR = '[data-foto360-menu-footer]';
var VR_BUTTON_SELECTOR = '[data-vr-view-select]';
var HOTSPOT_CLICKED_EVENT = 'krpanohotspotclicked';
var MENU_ITEM_SELECTED_EVENT = 'mediaviewermenuitemselected';
var WEBVR_ACTIVE_CLASS = 'active';
var WEBVR_AVAILABLE_CLASS = 'available';
var WEBVR_AVAILABLE_EVENT = 'webvravailable';
var WEBVR_ENABLED_EVENT = 'webvrenabled';
var WEBVR_DISABLED_EVENT = 'webvrdisabled';
var WEBVR_OPEN_CLASS = 'krpano-webvr-enabled';

function Foto360View(element, foto360Id) {
    var view = this;

    view.$container = $(FOTO_360_CONTAINER_SELECTOR);
    view.baseUrl = view.$container.attr(KRPANO_BASE_URL_ATTR) || '';
    view.items = JSON.parse($(element).find(ITEMS_SELECTOR).text());
    view.$vrButton = $(VR_BUTTON_SELECTOR);

    if (view.baseUrl !== '') {
        view.baseUrl = this.normalizeUrl(view.baseUrl);
    }

    // if there are multiple 360 views
    if (Object.keys(view.items).length > 1) {
        var $header360Select = $(FOTO_360_MENU_HEADER_SELECTOR);
        var $footer360Select = $(FOTO_360_MENU_FOOTER_SELECTOR);
        view.menu = new Menu($header360Select, $footer360Select);
    }

    view.src = view.items[foto360Id];

    if (view.isKrpanoInitialized === true) {
        view.krpano.call('loadpano(' + view.src + ', null, null, BLEND(0.05))');
    } else {
        this.initKrpano();
        if (view.menu) {
            view.menu.transformHeader();
        }
    }

    if (view.menu) {
        view.menu.setActive(foto360Id);
    }

    view.bindEvents();
}

Foto360View.prototype.bindEvents = function () {
    var view = this;

    $(window).on(HOTSPOT_CLICKED_EVENT, function (e, data) {
        if (data.name) {
            view.gotoPano(data.name, data.referer);
            if (view.menu) {
                view.menu.setActive(data.name);
            }
        }
    });

    $(window).on(MENU_ITEM_SELECTED_EVENT, function (e, data) {
        if (data.type && data.type === 'foto360') {
            view.gotoPano(data.menuParam);
        }
    });

    $(window).on(WEBVR_AVAILABLE_EVENT, function () {
        $(VR_BUTTON_SELECTOR).addClass(WEBVR_AVAILABLE_CLASS);
    });

    view.$vrButton.on('click', function () {
        view.krpano.call('funda_enter_vr()');
    });

    $(window).on(WEBVR_ENABLED_EVENT, function () {
        $(KRPANO_CONTAINER_OBJECT).addClass(WEBVR_OPEN_CLASS);

        $(VR_BUTTON_SELECTOR).addClass(WEBVR_ACTIVE_CLASS);
    });

    $(window).on(WEBVR_DISABLED_EVENT, function () {
        $(KRPANO_CONTAINER_OBJECT).removeClass(WEBVR_OPEN_CLASS);

        $(VR_BUTTON_SELECTOR).removeClass(WEBVR_ACTIVE_CLASS);
    });
};

Foto360View.prototype.gotoPano = function (name, referer) {
    var view = this;
    var variables = referer ? 'referer=' + referer : 'null';
    view.krpano.call('loadpano(' + view.items[name] + ', ' + variables + ', null, BLEND(0.05))');
    view.updateState('#360-foto-' + name);
};

//TODO: repeated code in each view
Foto360View.prototype.updateState = function (state) {
    if ('pushState' in window.history) {
        window.history.replaceState({}, '', state);
    } else {
        window.location.replace(state);
    }
};

Foto360View.prototype.normalizeUrl = function (url) {
    var aElement = document.createElement('a');
    aElement.href = url;
    return aElement.href;
};

Foto360View.prototype.initKrpano = function () {
    var view = this;
    var debugging = window.location.search.indexOf('krpanodebug=true') !== -1;
    var krpanoContainer = document.createElement('div');

    krpanoContainer.id = KRPANO_CONTAINER_ID;
    view.$container.html(krpanoContainer);

    load(view.baseUrl + '/assets/vendor-krpano.js', function (error) {
        if (!error) {
            embedpano({
                html5: 'prefer',
                swf: view.baseUrl + '/assets/components/krpano-loader/krpano.swf',
                xml: view.src,
                target: KRPANO_CONTAINER_ID,
                consolelog: debugging,
                onready: onReady
            });
        }
    });

    function onReady(krpano) {
        view.krpano = krpano;

        // Hack for the relative path of the stub pano's in the guide.
        // Krpano sets its basedir automatically to the path of the first loaded pano,
        // therefore the next pano's relative path becomes relative to the first one's path.
        krpano.set('basedir', '');
        // end hack
    }

    view.isKrpanoInitialized = true;
};

Foto360View.prototype.dispose = function () {
    var view = this;
    removepano(KRPANO_CONTAINER_ID);
    view.isKrpanoInitialized = false;
    view.unbindEvents();
    if (view.menu) {
        view.menu.dispose();
    }
};

Foto360View.prototype.unbindEvents = function () {
    $(window).off(HOTSPOT_CLICKED_EVENT);
    $(window).off(MENU_ITEM_SELECTED_EVENT);
    $(window).off(WEBVR_ENABLED_EVENT);
    $(window).off(WEBVR_DISABLED_EVENT);
};

// turn all elements with the default selector into components
// Auto-initialize means the component should instantiate itself as normal
// Otherwise this view's constructor will likely be called from the media viewer
$(COMPONENT_SELECTOR).each(function (index, element) {
    // use jquery to get data attribute because of IE 9/10
    var autoInitialize = $(element).data('mediaViewerAutoInitialize');

    if (autoInitialize === true) {
        return new Foto360View(element, 'straat');
    }
});

},{"../krpano-loader/krpano-loader":247,"../media-viewer-menu/media-viewer-menu":269,"jquery":"jquery","load-script":2}],268:[function(require,module,exports){
'use strict';

var _advertisementsLazy = require('../advertisements-lazy/advertisements-lazy');

var _advertisementsLazy2 = _interopRequireDefault(_advertisementsLazy);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

module.exports = FotosView;

var $ = require('jquery');
require('funda-slick-carousel');


var EVENT_NAMESPACE = '.media-viewer-fotos-view';
var ITEMS_SELECTOR = '[data-media-viewer-items]';
var PREVIOUS_SELECTOR = '[data-media-viewer-previous]';
var NEXT_SELECTOR = '[data-media-viewer-next]';
var CURRENT_SELECTOR = '[data-media-viewer-current]';
var TOTAL_SELECTOR = '[data-media-viewer-total]';
var ADVERTISEMENT_SELECTOR = '[data-advertisement]';
var SLICK_SLIDE_SPEED = 250;
var SLICK_SWIPE_SENSITIVITY = 12; // 1/12th of the image width

var FOTOSTOPPER = 'data-media-viewer-fotostopper';
var REWIND_SELECTOR = '[data-media-viewer-fotostopper-rewind]';

function FotosView($elements, fotoIndex) {
    var component = this;
    component.$items = $elements.find(ITEMS_SELECTOR);
    component.$prevButton = $elements.find(PREVIOUS_SELECTOR);
    component.$nextButton = $elements.find(NEXT_SELECTOR);
    component.$currentItemCount = $elements.find(CURRENT_SELECTOR);
    component.$totalItemCount = $elements.find(TOTAL_SELECTOR);
    component.$rewindButton = $elements.find(REWIND_SELECTOR);

    component.bindEvents();
    _advertisementsLazy2.default.lazyInitialize($elements.first()[0].querySelectorAll(ADVERTISEMENT_SELECTOR), 'funda');

    component.initializeSlideshow(fotoIndex);
    component.bindGlobalEvents();
    component.setItem(fotoIndex);
}

FotosView.prototype.dispose = function () {
    var component = this;
    component.unbindEvents();
    component.unbindGlobalEvents();
    component.disableSlideshow();
};

FotosView.prototype.bindEvents = function () {
    var view = this;

    view.$nextButton.on('click', view.nextItem.bind(view));
    view.$prevButton.on('click', view.prevItem.bind(view));
    view.$rewindButton.on('click', view.setItem.bind(view, 1));

    view.$items.on('init reInit', slickInitHandler.bind(view));
    view.$items.on('beforeChange', slickBeforeChangeHandler.bind(view));
};

FotosView.prototype.unbindEvents = function () {
    var view = this;

    view.$nextButton.off('click');
    view.$prevButton.off('click');
    view.$items.off('init reInit');
    view.$items.off('beforeChange');
};

FotosView.prototype.bindGlobalEvents = function () {
    if (this.globalEventsBound) {
        return;
    }
    var view = this;
    $(document).on('keydown' + EVENT_NAMESPACE, function keyHandler(event) {
        if (event.ctrlKey || event.shiftKey || event.altKey || event.metaKey) {
            return;
        }

        switch (event.keyCode) {
            case 37:
                //arrow left
                view.prevItem();
                break;
            case 39:
                // arrow right
                view.nextItem();
                break;
        }
    });

    this.globalEventsBound = true;
};

FotosView.prototype.unbindGlobalEvents = function () {
    if (this.globalEventsBound) {
        $(document).off('keydown' + EVENT_NAMESPACE);
        this.globalEventsBound = false;
    }
};

FotosView.prototype.updateState = function (state) {
    if ('pushState' in window.history) {
        window.history.replaceState({}, '', state);
    } else {
        window.location.replace(state);
    }
};

FotosView.prototype.initializeSlideshow = function (initialSlide) {
    var component = this;

    if (component.slideshowIsInitialized) {
        return;
    }

    component.$items.slick({
        arrows: false,
        speed: SLICK_SLIDE_SPEED,
        cssEase: 'ease',
        accessibility: true,
        infinite: false,
        mobileFirst: true,
        touchThreshold: SLICK_SWIPE_SENSITIVITY,
        initialSlide: initialSlide - 1 || 0,
        useTransform: true,
        lazyLoad: 'anticipated',
        lazyLoadAhead: 3,
        lazyLoadBack: 1
    });

    component.slideshowIsInitialized = true;
};

FotosView.prototype.disableSlideshow = function () {
    this.$items.slick('unslick');
    this.slideshowIsInitialized = false;
};

FotosView.prototype.setItem = function (item) {
    this.$items.slick('slickGoTo', item - 1, true);
};

FotosView.prototype.prevItem = function () {
    // Go to previous photo, but this time without animation
    this.$items.slick('slickPrev', true);
};

FotosView.prototype.nextItem = function () {
    // Go to next photo, but this time without animation
    this.$items.slick('slickNext', true);
};

function slickInitHandler(event, slick) {
    var hasFotoStopper = slick.$slides[slick.$slides.length - 1].hasAttribute(FOTOSTOPPER);

    this.$totalItemCount.text(hasFotoStopper ? slick.slideCount - 1 : slick.slideCount);
}

function slickBeforeChangeHandler(event, slick, currentSlide, nextSlide) {
    var isGoingToBeFotoStopper = slick.$slides[nextSlide].hasAttribute(FOTOSTOPPER);
    var nextSlideNumber = parseInt(nextSlide, 10) + 1;

    if (isGoingToBeFotoStopper) {
        nextSlideNumber--;
    }

    this.$currentItemCount.text(nextSlideNumber);
    this.updateState('#foto-' + nextSlideNumber);
    this.$nextButton.attr('disabled', slick.slideCount === nextSlide + 1);
    this.$prevButton.attr('disabled', nextSlide === 0);
}

},{"../advertisements-lazy/advertisements-lazy":195,"funda-slick-carousel":"funda-slick-carousel","jquery":"jquery"}],269:[function(require,module,exports){
'use strict';

module.exports = Menu;

var $ = require('jquery');

var MEDIA_VIEWER_SELECTOR = '.media-viewer';
var MENU_TOGGLE = 'data-menu-toggle';
var MENU_TOGGLE_SELECTOR = '[' + MENU_TOGGLE + ']';
var MENU_DROPUP_TITLE_SELECTOR = '[data-dropup-title]';
var MENU_ITEMS_SELECTOR = '[data-menu-items]';
var MENU_SUB_ITEMS = 'data-menu-sub-items';
var MENU_SUB_ITEMS_SELECTOR = '[' + MENU_SUB_ITEMS + ']';
var MENU_ITEM = 'data-menu-item-param';
var MENU_ITEM_SELECTOR = '[' + MENU_ITEM + ']';
var ACTIVE_CLASS = 'active';
var ACTIVE_CLASS_SELECTOR = '.' + ACTIVE_CLASS;
var OPEN_CLASS = 'open';
var MORE_TOGGLE_TEMPLATE_SELECTOR = '[data-menu-more-toggle-template]';

function Menu($headerElement, $footerElement) {
    this.$headerElement = $headerElement;
    this.$footerElement = $footerElement;
    this.moreTemplate = $headerElement.find(MORE_TOGGLE_TEMPLATE_SELECTOR).html();

    var $footerMenuItems = $footerElement.find(MENU_ITEMS_SELECTOR);

    $(MEDIA_VIEWER_SELECTOR).on('click', function () {
        $headerElement.find(MENU_SUB_ITEMS_SELECTOR).removeClass(OPEN_CLASS);

        var subItems = $headerElement.find(MENU_SUB_ITEMS_SELECTOR);
        if (subItems.find(ACTIVE_CLASS_SELECTOR).length === 0) {
            subItems.removeClass(ACTIVE_CLASS);
        }

        $footerMenuItems.removeClass(OPEN_CLASS);
        $footerElement.find(MENU_TOGGLE_SELECTOR).removeClass(ACTIVE_CLASS);
    });

    $headerElement.find(MENU_ITEM_SELECTOR).on('click', function () {
        var $el = $(this);
        $headerElement.find(MENU_ITEM_SELECTOR).removeClass(ACTIVE_CLASS);
        $headerElement.find(MENU_TOGGLE_SELECTOR).removeClass(ACTIVE_CLASS);
        $el.addClass(ACTIVE_CLASS);

        if ($el.parents(MENU_SUB_ITEMS_SELECTOR).length > 0) {
            $headerElement.find(MENU_TOGGLE_SELECTOR).addClass(ACTIVE_CLASS);
            $headerElement.find(MENU_SUB_ITEMS_SELECTOR).removeClass(OPEN_CLASS);
        }

        $(window).trigger('mediaviewermenuitemselected', {
            type: this.getAttribute('data-view-type'),
            menuParam: this.getAttribute(MENU_ITEM)
        });
    });

    $footerElement.find(MENU_TOGGLE_SELECTOR).on('click', function (e) {
        var $el = $(this);
        $el.toggleClass(ACTIVE_CLASS);

        $footerMenuItems.toggleClass(OPEN_CLASS);
        e.stopPropagation();
    });

    $footerElement.find(MENU_ITEM_SELECTOR).on('click', function () {
        var $el = $(this);
        $footerElement.find(MENU_DROPUP_TITLE_SELECTOR).html($el.html());
        $footerMenuItems.find(MENU_ITEM_SELECTOR).removeClass(ACTIVE_CLASS);
        $el.addClass(ACTIVE_CLASS);

        $(window).trigger('mediaviewermenuitemselected', {
            type: this.getAttribute('data-view-type'),
            menuParam: this.getAttribute(MENU_ITEM)
        });
    });
}

Menu.prototype.setActive = function (id) {
    var MENU_ACTIVE_ITEM_SELECTOR = '[' + MENU_ITEM + '="' + id + '"' + ']';

    // header menu
    this.$headerElement.find(MENU_ITEM_SELECTOR).removeClass(ACTIVE_CLASS);
    this.$headerElement.find(MENU_ACTIVE_ITEM_SELECTOR).addClass(ACTIVE_CLASS);

    // footer menu
    this.$footerElement.find(MENU_ITEM_SELECTOR).removeClass(ACTIVE_CLASS);
    var $footerItem = this.$footerElement.find(MENU_ACTIVE_ITEM_SELECTOR);
    $footerItem.addClass(ACTIVE_CLASS);
    this.$footerElement.find(MENU_DROPUP_TITLE_SELECTOR).html($footerItem.html());
};

Menu.prototype.toggleMore = function (event) {
    var $el = $(this);
    $el.parent().toggleClass(ACTIVE_CLASS);
    $el.next().toggleClass(OPEN_CLASS);
    event.stopPropagation();
};

/**
 * Transforms initial menu with UL+LI structure to collapse menu with 'more' button.
 */
Menu.prototype.transformHeader = function () {
    var $menu = this.$headerElement.find(MENU_ITEMS_SELECTOR);
    var hasMenuItems = $menu.find(MENU_ITEM_SELECTOR).length > 0;
    var ulWidth = $menu.width();
    if (ulWidth === 0 || hasMenuItems === false) {
        return;
    }

    var parentWidth = this.$headerElement.parent().width();

    var floors = [];
    while ((parentWidth - ulWidth) / 2 < 220) {
        floors.push($menu.children().last().detach());
        ulWidth = $menu.width();
    }

    if (floors.length > 0) {
        floors.push($menu.children().last().detach());

        var more = $(this.moreTemplate);
        more.find(MENU_TOGGLE_SELECTOR).on('click', this.toggleMore);

        $.each(floors.reverse(), function (index, item) {
            more.find(MENU_SUB_ITEMS_SELECTOR).append(item);
        });

        $menu.append(more);
    }
};

Menu.prototype.dispose = function () {
    $(MEDIA_VIEWER_SELECTOR).off('click');
    this.$headerElement.find(MENU_ITEM_SELECTOR).off('click');
    this.$footerElement.find(MENU_TOGGLE_SELECTOR).off('click');
    this.$footerElement.find(MENU_ITEM_SELECTOR).off('click');
};

},{"jquery":"jquery"}],270:[function(require,module,exports){
'use strict';
/* globals fpViewer */

module.exports = PlattegrondView;

var $ = require('jquery');
var load = require('load-script');
var Menu = require('../media-viewer-menu/media-viewer-menu');
var AppSpinner = require('../app-spinner/app-spinner');

var COMPONENT_SELECTOR = '[data-media-viewer-plattegrond]';
var PLATTEGROND_CONTAINER_CLASS = 'media-viewer-plattegrond-container';
var PLATTEGROND_CONTAINER_SELECTOR = '.' + PLATTEGROND_CONTAINER_CLASS;
var PLATTEGROND_CONTAINER_SRC_ATTR = 'data-plattegrond-src';
var PLATTEGROND_VIEW_SELECT_RADIO_SELECTOR = '[data-view-select]';
var PLATTEGROND_2D_VIEW_SELECT_RADIO_SELECTOR = '[data-view-select][value="2d"]';
var PLATTEGROND_2D_VIEW_SELECTOR = '[data-plattegrond-2d-view]';
var PLATTEGROND_3D_VIEW_SELECTOR = '[data-plattegrond-3d-view]';
var PLATTEGROND_SPINNER_VIEW_SELECTOR = '[data-plattegrond-spinner-view]';
var PLATTEGROND_SPINNER_SELECTOR = '[data-plattegrond-spinner]';
var PLATTEGROND_MENU_HEADER_SELECTOR = '[data-plattegrond-menu-header]';
var PLATTEGROND_MENU_FOOTER_SELECTOR = '[data-plattegrond-menu-footer]';
var PLATTEGROND_MENU_ITEM_TEMPLATE_SELECTOR = '[data-plattegrond-menu-item-template]';
var PLATTEGROND_ZOOM_IN_SELECTOR = '[data-plattegrond-zoom-in]';
var PLATTEGROND_ZOOM_OUT_SELECTOR = '[data-plattegrond-zoom-out]';
var BASE_URL_ATTR = 'data-base-url';
var MENU_ITEMS_SELECTOR = '[data-menu-items]';
var FLOORPLANNER_ACTIVE_CLASS = 'active';
var SHOW_CLASS = 'show';

function PlattegrondView() {
    var view = this;
    view.$container = $(PLATTEGROND_CONTAINER_SELECTOR);
    view.$2dView = view.$container.find(PLATTEGROND_2D_VIEW_SELECTOR);
    view.$3dView = view.$container.find(PLATTEGROND_3D_VIEW_SELECTOR);

    view.src = view.$container.attr(PLATTEGROND_CONTAINER_SRC_ATTR);
    view.baseUrl = view.$container.attr(BASE_URL_ATTR);

    view.$zoomIn = $(PLATTEGROND_ZOOM_IN_SELECTOR);
    view.$zoomOut = $(PLATTEGROND_ZOOM_OUT_SELECTOR);
    view.$zoomIn.on('click', function () {
        return view.zoomInHandler();
    });
    view.$zoomOut.on('click', function () {
        return view.zoomOutHandler();
    });

    view.menuItemTemplate = $(PLATTEGROND_MENU_ITEM_TEMPLATE_SELECTOR).html();

    view.loaded = false;
    view.designId = 0;
    view.floors = {};
    view.viewType = '2d';
    // make sure 2d is the initial state of the radio select
    $(PLATTEGROND_2D_VIEW_SELECT_RADIO_SELECTOR).prop('checked', true);

    if (view.loaded) {
        return;
    }

    view.$spinnerView = $(PLATTEGROND_SPINNER_VIEW_SELECTOR);
    view.spinner = new AppSpinner($(PLATTEGROND_SPINNER_SELECTOR).get(0));
    view.spinner.show();

    $(PLATTEGROND_VIEW_SELECT_RADIO_SELECTOR).on('change', function () {
        view.viewType = this.value === '2d' ? '2d' : '3d';
        view.display();
    });

    $(window).on('mediaviewermenuitemselected', function (e, data) {
        if (data.type && data.type === 'plattegrond') {
            view.designId = data.menuParam;
            view.display();
        }
    });

    var theme = {
        ui: {
            initView: '2d', // or '3d'
            rotate3d: true // automatically rotate the 3d view
        },
        engine: {
            wallSectionHeight: null,
            wallTopColor: 0x7F7F7F, // top colour of walls in 3D
            showGrid: false,
            showDims: true, // visibility of dimension lines (both auto and manual)
            engineAutoDims: false, // generate and show automatic dimensions
            showObjects: true,
            showLabels: true, // visibility for all labels, including room names and dims
            useMetric: true // false means Imperial system, affects dimensions
        }
    };

    load(view.baseUrl + '/assets/vendor-floorplanner.js', function (err) {
        if (typeof err !== 'undefined') {
            fpViewer.init({
                projectId: null,
                box2d: view.$2dView.get(0),
                box3d: view.$3dView.get(0),
                theme: theme,
                fmlUrl: view.src,
                callback: function callback(project) {
                    project.floors.forEach(function (floor) {
                        view.floors[floor.id] = {
                            id: floor.id,
                            name: floor.name,
                            design: floor.designs[0]
                        };
                    });
                    view.designId = project.floors[0].id;
                    view.setupMenu();
                    view.display();
                    view.$spinnerView.removeClass(FLOORPLANNER_ACTIVE_CLASS);
                    view.spinner.hide();
                    view.loaded = true;
                },
                jquery: $
            });
        }
    });
}

PlattegrondView.prototype.dispose = function () {
    var view = this;
    fpViewer.cleanup();

    var $headerFloorSelect = $(PLATTEGROND_MENU_HEADER_SELECTOR);
    var $footerFloorSelect = $(PLATTEGROND_MENU_FOOTER_SELECTOR);

    var headerMenu = $headerFloorSelect.find(MENU_ITEMS_SELECTOR);
    var footerMenu = $footerFloorSelect.find(MENU_ITEMS_SELECTOR);
    headerMenu.empty();
    footerMenu.empty();

    view.loaded = false;

    // detach event listeners
    $(PLATTEGROND_VIEW_SELECT_RADIO_SELECTOR).off('change');
    $(window).off('mediaviewermenuitemselected');

    if (view.menu) {
        view.menu.dispose();
    }
};

PlattegrondView.prototype.setupMenu = function () {
    var view = this;

    var $headerFloorSelect = $(PLATTEGROND_MENU_HEADER_SELECTOR);
    var $footerFloorSelect = $(PLATTEGROND_MENU_FOOTER_SELECTOR);

    var headerMenu = $headerFloorSelect.find(MENU_ITEMS_SELECTOR);
    var footerMenu = $footerFloorSelect.find(MENU_ITEMS_SELECTOR);

    Object.keys(view.floors).forEach(function (key) {
        var floor = view.floors[key];
        var menuItem = view.renderMenuItem({ id: floor.id, name: floor.name });
        headerMenu.append(menuItem);
        footerMenu.append(menuItem);
    });

    view.menu = new Menu($headerFloorSelect, $footerFloorSelect);

    if (Object.keys(view.floors).length > 1) {
        $headerFloorSelect.addClass(SHOW_CLASS);
        $footerFloorSelect.addClass(SHOW_CLASS);
    }

    view.menu.transformHeader();
    view.menu.setActive(view.designId);
};

PlattegrondView.prototype.display = function () {
    var view = this;

    view.$2dView.removeClass(FLOORPLANNER_ACTIVE_CLASS);
    view.$3dView.removeClass(FLOORPLANNER_ACTIVE_CLASS);

    var floor = view.floors[view.designId];

    if (view.viewType === '2d') {
        view.$2dView.addClass(FLOORPLANNER_ACTIVE_CLASS);
        fpViewer.display2D(floor.design);
    } else {
        view.$3dView.addClass(FLOORPLANNER_ACTIVE_CLASS);
        fpViewer.display3D(floor.design);
    }
};

PlattegrondView.prototype.renderMenuItem = function (item) {
    var html = this.menuItemTemplate;
    // replace placeholders in template by values from option data
    for (var prop in item) {
        if (item.hasOwnProperty(prop)) {
            var pattern = new RegExp('{' + prop + '}', 'g');
            html = html.replace(pattern, item[prop]);
        }
    }
    return html;
};

PlattegrondView.prototype.zoomInHandler = function () {
    var view = this;

    if (view.loaded) {
        fpViewer.zoomIn();
    }
};

PlattegrondView.prototype.zoomOutHandler = function () {
    var view = this;

    if (view.loaded) {
        fpViewer.zoomOut();
    }
};

// turn all elements with the default selector into components
$(COMPONENT_SELECTOR).each(function (index, element) {
    // use jquery to get data attribute because of IE 9/10
    var autoInitialize = $(element).data('mediaViewerAutoInitialize');
    if (autoInitialize === 'true') {
        return new PlattegrondView(element);
    }
});

},{"../app-spinner/app-spinner":201,"../media-viewer-menu/media-viewer-menu":269,"jquery":"jquery","load-script":2}],271:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = VideoView;

var quadia = require('funda-quadia');

var COMPONENT_SELECTOR = '[data-media-viewer-video]';
var COOKIES_ACCEPTED_EVENT = 'cookiesaccepted';
var COOKIES_ACCEPTED_ATTR = 'data-cookies-accepted';
var VIDEO_CONTAINER_ID = 'media-viewer-quadia-container';
var VIDEO_CONTAINER_SELECTOR = '#' + VIDEO_CONTAINER_ID;
var VIDEO_CONTAINER_SRC_ATTR = 'data-media-viewer-video-src';

function VideoView(element) {
    var view = this;
    view.$videoContainer = (0, _jquery2.default)(VIDEO_CONTAINER_SELECTOR);
    view.src = this.$videoContainer.attr(VIDEO_CONTAINER_SRC_ATTR);

    var cookiesAccepted = element.getAttribute(COOKIES_ACCEPTED_ATTR);

    if (cookiesAccepted === 'false') {
        // There must be a cookie notification somewhere on the page, so wait
        // until accepted.
        (0, _jquery2.default)(window).on(COOKIES_ACCEPTED_EVENT, function cookiesAcceptedHandlerWithViewLoaded() {
            (0, _jquery2.default)(window).off(COOKIES_ACCEPTED_EVENT);
            view.init();
        });
    } else {
        view.init();
    }
}

/**
 * Initializes the Quadia video player, adds markup to the container, including
 * an iframe that hosts the video player
 */
VideoView.prototype.init = function () {
    var view = this;
    quadia.iframe(view.src, {
        container: '' + VIDEO_CONTAINER_ID + '',
        methods: ['fullscreen', 'fullbrowser'],
        style: 'bottom_right(4px,16px,44px,44px)'
    });
};

VideoView.prototype.dispose = function () {
    this.$videoContainer.html('');
};

// turn all elements with the default selector into components
// Auto-initialize means the component should instantiate itself and handle the cookie policy in the constructor
// Otherwise this view's constructor will likely be called from the media viewer
// todo: move init logic to constructor. Differentiate between player "init" and "play".
(0, _jquery2.default)(COMPONENT_SELECTOR).each(function (index, element) {
    // use jquery to get data attribute because of IE 9/10
    var autoInitialize = (0, _jquery2.default)(element).data('mediaViewerAutoInitialize');

    if (autoInitialize === true) {
        return new VideoView(element);
    } else {
        // Listen for accepted cookies and update the attribute so that video can
        // start normally when the component is constructed.
        (0, _jquery2.default)(window).on(COOKIES_ACCEPTED_EVENT, function cookiesAcceptedHandlerWithViewNotLoaded() {
            (0, _jquery2.default)(window).off(COOKIES_ACCEPTED_EVENT);
            element.setAttribute(COOKIES_ACCEPTED_ATTR, true);
        });
    }
});

},{"funda-quadia":"funda-quadia","jquery":"jquery"}],272:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

var _mediaViewerVideo = require('../media-viewer-video/media-viewer-video');

var _mediaViewerVideo2 = _interopRequireDefault(_mediaViewerVideo);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var FotosView = require('../media-viewer-fotos/media-viewer-fotos');

var Foto360View = require('../media-viewer-foto360/media-viewer-foto360');
var PlattegrondView = require('../media-viewer-plattegrond/media-viewer-plattegrond');

// monkey-patch 2 functions that are used by Slick but deprecated in jQuery 3
_jquery2.default.prototype.load = function (callback) {
    return _jquery2.default.fn.on.apply(undefined, ['load', callback]);
};
_jquery2.default.prototype.error = function (callback) {
    return _jquery2.default.fn.on.apply(undefined, ['error', callback]);
};

exports.default = MediaViewer;


var COMPONENT_SELECTOR = '[data-media-viewer]';
var FOTO_VIEWER_SELECTOR = '[data-media-viewer-fotos]';
var FOTO_HASH = '#foto-';
var VIDEO_VIEWER_SELECTOR = '[data-media-viewer-video]';
var VIDEO_HASH = '#video';
var FOTO360_VIEWER_SELECTOR = '[data-media-viewer-foto360]';
var FOTO360_HASH = '#360-foto-';
var PLATTEGROND_VIEWER_SELECTOR = '[data-media-viewer-plattegrond]';
var PLATTEGROND_HASH = '#plattegrond';
var MEDIA_VIEWER_HASHES = [FOTO_HASH, FOTO360_HASH, VIDEO_HASH, PLATTEGROND_HASH];
var HIDDEN_CLASS = 'hidden';
var OPEN_MEDIA_VIEWER_EVENT = 'openmediaviewer';
var EVENT_NAMESPACE = '.media-viewer';
var BODY_SELECTOR = 'body';
var BODY_OPEN_CLASS = 'media-viewer-open';
var OPEN_CLASS = 'is-open';
var CLOSING_CLASS = 'is-closing';
var CLOSE_SELECTOR = '[data-media-viewer-close]';
var OBJECT_MEDIA_OVERLAY_LINK = 'data-media-viewer-overlay';
var OBJECT_MEDIA_OVERLAY_LINK_SELECTOR = '[' + OBJECT_MEDIA_OVERLAY_LINK + ']';

var OBJECT_MEDIA_OVERLAY_SWITCH_LINK = 'data-media-viewer-overlay-switch';
var OBJECT_MEDIA_OVERLAY_SWITCH_SELECTOR = '[data-media-viewer-overlay-switch]';

var ANIMATION_END_EVENTS = 'webkitAnimationEnd oAnimationEnd msAnimationEnd animationend';
var scrollPosition = void 0;
var $body = (0, _jquery2.default)(BODY_SELECTOR);

function MediaViewer(element) {
    var component = this;

    component.animationsSupported = browserSupportsAnimations();

    component.$element = (0, _jquery2.default)(element);
    component.$closeButton = component.$element.find(CLOSE_SELECTOR);

    if (component.hasMediaViewerHashInUrl()) {
        component.isMediaViewerAccessedDirectly = true;
        component.open();
        component.refreshView();
    }

    component.bindEvents();
}

MediaViewer.prototype.bindEvents = function () {
    var component = this;

    // Close button
    component.$closeButton.on('click', function closeHandler() {
        component.close();
    });

    // Close key (escape)
    (0, _jquery2.default)(document).on('keydown' + EVENT_NAMESPACE, function keyHandler(event) {
        if (event.ctrlKey || event.shiftKey || event.altKey || event.metaKey) {
            return;
        }

        if (event.keyCode === 27 /*ESC*/) {
                component.close();
            }
    });

    // Link to media clicked, media viewer should be opened
    (0, _jquery2.default)(OBJECT_MEDIA_OVERLAY_LINK_SELECTOR).on('click', function (e) {
        e.preventDefault(); // prevent hashchange event
        (0, _jquery2.default)(window).trigger(OPEN_MEDIA_VIEWER_EVENT, [this.getAttribute(OBJECT_MEDIA_OVERLAY_LINK)]);
    });

    // Switch to different view while media viewer is open
    (0, _jquery2.default)(OBJECT_MEDIA_OVERLAY_SWITCH_SELECTOR).on('click', function () {
        component.replaceState([this.getAttribute(OBJECT_MEDIA_OVERLAY_SWITCH_LINK)]);
        component.refreshView();
    });

    // Open Media Viewer event (custom event)
    (0, _jquery2.default)(window).on(OPEN_MEDIA_VIEWER_EVENT, function (e, overlay) {
        // Change the url and the hashchange event will take care of the rest
        // (don't use history.pushState because the hashchange event won't be fired)
        window.location.href = window.location.pathname + overlay;
    });

    // Hashchange (url altered, possibly because of a history event)
    window.addEventListener('hashchange', function hashChangeHandler() {
        if (component.hasMediaViewerHashInUrl()) {
            component.open();
            component.refreshView();
        } else {
            component.close();
        }
    });
};

/*
 * Returns true if the current location url contains a valid media viewer hash
 */
MediaViewer.prototype.hasMediaViewerHashInUrl = function () {
    return MEDIA_VIEWER_HASHES.some(function (hash) {
        return window.location.hash.indexOf(hash) === 0;
    });
};

/*
 * Opens the media viewer and shows the view
 */
MediaViewer.prototype.open = function () {
    var component = this;
    var onAnimationEndHandler = function onAnimationEndHandler() {
        component.$element.attr('aria-hidden', 'false');
    };

    if (component.isOpen()) {
        return;
    }

    if (component.animationsSupported) {
        component.$element.one(ANIMATION_END_EVENTS, onAnimationEndHandler);
    } else {
        onAnimationEndHandler();
    }

    // Open Media Viewer and disable scrolling
    scrollPosition = (0, _jquery2.default)(window).scrollTop();

    $body.css('top', -1 * scrollPosition);
    $body.addClass(BODY_OPEN_CLASS);

    component.$element.addClass(OPEN_CLASS);
};

/*
 * Closes the media viewer and hides any view that may be visible
 */
MediaViewer.prototype.close = function () {
    var component = this;
    var onAnimationEndHandler = void 0;

    if (component.isOpen()) {
        component.$element.removeClass(OPEN_CLASS).addClass(CLOSING_CLASS);

        // After closing Media Viewer enable scrolling
        $body.css('top', '');
        $body.removeClass(BODY_OPEN_CLASS);

        // Set scroll position, to prevent page flicking
        (0, _jquery2.default)(window).scrollTop(scrollPosition);

        onAnimationEndHandler = function onAnimationEndHandler() {
            component.clearViews();
            component.$element.removeClass(CLOSING_CLASS);
            component.$element.attr('aria-hidden', 'true');
        };

        if (component.animationsSupported) {
            component.$element.one(ANIMATION_END_EVENTS, onAnimationEndHandler);
        } else {
            onAnimationEndHandler();
        }

        if (window.location.hash) {
            if (component.isMediaViewerAccessedDirectly === true) {
                // User entered the media viewer directly (e.g. from external source), expected behaviour is to go to the detail page
                component.isMediaViewerAccessedDirectly = false;
                component.replaceState(window.location.pathname.split(window.location.hash)[0]);
            } else {
                // In any other case, just go back in history
                window.history.go(-1);
            }
        }
    }
};

/*
 * Replaces the current browser state with newState in the best way possible, with browser support in mind
 */
MediaViewer.prototype.replaceState = function (newState) {
    if ('replaceState' in window.history) {
        // Use of history.replaceState is preferred because it allows the animation to finish and seems to be faster
        window.history.replaceState({}, '', newState);
    } else {
        // IE 9 fallback
        // Doesn't work as nice as history.replaceState (url in browser is not updated, history navigation is messed up) but it's something
        window.location.replace(newState);
    }
};

MediaViewer.prototype.refreshView = function () {
    var component = this;
    var $elements = void 0;
    var locationHash = window.location.hash;
    component.clearViews();

    if (locationHash.indexOf(FOTO_HASH) === 0) {
        var fotoIndex = locationHash.split(FOTO_HASH)[1];
        $elements = (0, _jquery2.default)(FOTO_VIEWER_SELECTOR);
        $elements.removeClass(HIDDEN_CLASS);
        component.fotosView = new FotosView($elements, fotoIndex);
    } else if (locationHash.indexOf(VIDEO_HASH) === 0) {
        $elements = (0, _jquery2.default)(VIDEO_VIEWER_SELECTOR); // for video a single element
        $elements.removeClass(HIDDEN_CLASS);
        component.videoView = new _mediaViewerVideo2.default($elements[0]);
    } else if (locationHash.indexOf(FOTO360_HASH) === 0) {
        var foto360Id = locationHash.split(FOTO360_HASH)[1];
        $elements = (0, _jquery2.default)(FOTO360_VIEWER_SELECTOR); // for 360 also  a single element
        $elements.removeClass(HIDDEN_CLASS);
        component.foto360View = new Foto360View($elements[0], foto360Id);
    } else if (locationHash.indexOf(PLATTEGROND_HASH) === 0) {
        var plattegrondId = locationHash.split(PLATTEGROND_HASH)[1];
        $elements = (0, _jquery2.default)(PLATTEGROND_VIEWER_SELECTOR);
        $elements.removeClass(HIDDEN_CLASS);
        component.plattegrondView = new PlattegrondView($elements, plattegrondId);
    }
};

MediaViewer.prototype.clearViews = function () {
    var component = this;
    var $allViewElements = (0, _jquery2.default)(FOTO_VIEWER_SELECTOR + ', ' + VIDEO_VIEWER_SELECTOR + ', ' + FOTO360_VIEWER_SELECTOR + ', ' + PLATTEGROND_VIEWER_SELECTOR);
    $allViewElements.addClass(HIDDEN_CLASS);
    if (_typeof(component.fotosView) === 'object') {
        component.fotosView.dispose();
    }
    if (_typeof(component.videoView) === 'object') {
        component.videoView.dispose();
    }
    if (_typeof(component.foto360View) === 'object') {
        component.foto360View.dispose();
    }
    if (_typeof(component.plattegrondView) === 'object') {
        component.plattegrondView.dispose();
    }
};

/*
 * Returns true if the media viewer is open.
 */
MediaViewer.prototype.isOpen = function () {
    return this.$element.hasClass(OPEN_CLASS);
};

/*
 * Returns true if the user's browser supports CSS animations
 * https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Animations/Detecting_CSS_animation_support
 *
 * @returns {boolean}
 */
function browserSupportsAnimations() {
    var domPrefixes = 'Webkit Moz O ms Khtml'.split(' ');
    var elm = document.createElement('div');

    if (elm.style.animationName !== undefined) {
        return true;
    }

    for (var i = 0; i < domPrefixes.length; i++) {
        if (elm.style[domPrefixes[i] + 'AnimationName'] !== undefined) {
            return true;
        }
    }

    return false;
}

// turn all elements with the default selector into components
(0, _jquery2.default)(COMPONENT_SELECTOR).each(function (index, element) {
    return new MediaViewer(element);
});

},{"../media-viewer-foto360/media-viewer-foto360":267,"../media-viewer-fotos/media-viewer-fotos":268,"../media-viewer-plattegrond/media-viewer-plattegrond":270,"../media-viewer-video/media-viewer-video":271,"jquery":"jquery"}],273:[function(require,module,exports){
'use strict';

require('../../components/expandible/expandible');

},{"../../components/expandible/expandible":229}],274:[function(require,module,exports){
// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

var _debounce = require('lodash/debounce');

var _debounce2 = _interopRequireDefault(_debounce);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = NieuweHuisChecklist;


var COMPONENT_SELECTOR = '[data-nieuwe-huis-checklist]';
var CHECKLIST_SELECTOR = '[data-nieuwe-huis-checklist-checkbox]';
var DEBOUNCE_DELAY = 100;

function NieuweHuisChecklist(element) {
    var component = this;
    component.$element = (0, _jquery2.default)(element);
    component.checkbox = component.$element.find(CHECKLIST_SELECTOR);
    component.registerChangeOnCheckbox();
}

NieuweHuisChecklist.prototype.registerChangeOnCheckbox = function () {
    var component = this;
    var url = component.$element.attr('action');
    var method = component.$element.attr('method');

    component.checkbox.on('change', (0, _debounce2.default)(function () {
        var $this = (0, _jquery2.default)(this);
        var $checkboxLabel = $this.parents('label');
        var data = {
            taskId: $this.attr('data-task-id'),
            value: $this.is(':checked')
        };
        return _jquery2.default.ajax({
            url: url,
            type: method,
            data: data,
            dataType: 'json',
            success: function success() {
                component.onSuccess($checkboxLabel);
            },
            error: function error() {
                component.onError($checkboxLabel);
            }
        });
    }, DEBOUNCE_DELAY));
};

NieuweHuisChecklist.prototype.onSuccess = function ($checkboxLabel) {
    $checkboxLabel.removeClass('error').addClass('success');
};

NieuweHuisChecklist.prototype.onError = function ($checkboxLabel) {
    $checkboxLabel.removeClass('success').addClass('error');
};

// turn all elements with the default selector into components
(0, _jquery2.default)(COMPONENT_SELECTOR).each(function (index, element) {
    return new NieuweHuisChecklist(element);
});

},{"jquery":"jquery","lodash/debounce":158}],275:[function(require,module,exports){
// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

var _throttle = require('lodash/throttle');

var _throttle2 = _interopRequireDefault(_throttle);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = NieuweHuisKoppelenSticky;

// component configuration

var COMPONENT_SELECTOR = '[data-nieuwe-huis-koppelen]';
var CTA_HEADER = '[data-cta-header]';
var CTA_FOOTER = '[data-cta-footer]';
var CTA_STICKY = '[data-nieuwe-huis-koppelen-sticky]';
var THRESHOLD_FOOTER = 10;
var THROTTLE_DELAY = 150;

function NieuweHuisKoppelenSticky(element) {
    var component = this;
    component.registerScroll((0, _jquery2.default)(element));
}

NieuweHuisKoppelenSticky.prototype.registerScroll = function (NieuweHuisKoppelenEl) {
    var component = this;
    component.$element = (0, _jquery2.default)(NieuweHuisKoppelenEl);
    component.ctaHeaderSelector = component.$element.find(CTA_HEADER);
    component.ctaFooterSelector = component.$element.find(CTA_FOOTER);
    component.ctaSticky = component.$element.find(CTA_STICKY);

    component.registerOnScrollEventHandler();
};

NieuweHuisKoppelenSticky.prototype.registerOnScrollEventHandler = function () {
    var component = this;

    (0, _jquery2.default)(window).on('scroll', (0, _throttle2.default)(function () {
        if (component.isVisible(component.ctaFooterSelector[0], THRESHOLD_FOOTER) || !component.isVisible(component.ctaHeaderSelector[0], 0, 'above')) {
            component.hideStickyCTA();
        } else {
            component.showStickyCTA();
        }
    }, THROTTLE_DELAY));
};

NieuweHuisKoppelenSticky.prototype.hideStickyCTA = function () {
    var component = this;
    component.ctaSticky.removeClass('visible');
};

NieuweHuisKoppelenSticky.prototype.showStickyCTA = function () {
    var component = this;
    component.ctaSticky.addClass('visible');
};

NieuweHuisKoppelenSticky.prototype.isVisible = function (element) {
    var threshold = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
    var mode = arguments[2];

    if ((typeof element === 'undefined' ? 'undefined' : _typeof(element)) !== 'object') {
        return false;
    }
    var viewHeight = Math.max(document.documentElement.clientHeight, window.innerHeight);
    var isRect = element.getBoundingClientRect();
    var isAbove = isRect.bottom - threshold < 0;
    var isBelow = isRect.top - viewHeight + threshold >= 0;

    switch (mode) {
        case 'above':
            return isAbove;
        case 'below':
            return isBelow;
        default:
            return !isAbove && !isBelow;
    }
};

(0, _jquery2.default)(COMPONENT_SELECTOR).each(function (index, element) {
    if ((0, _jquery2.default)(element).is(':visible')) {
        return new NieuweHuisKoppelenSticky(element);
    }
});

},{"jquery":"jquery","lodash/throttle":188}],276:[function(require,module,exports){
// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

var _contentFetch = require('../content-fetch/content-fetch');

var _contentFetch2 = _interopRequireDefault(_contentFetch);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = NieuweHuisWidgetContent;

// component configuration

var CATEGORIES_URL_ATTR = 'data-content-fetch-url';
var COMPONENT_SELECTOR = '[data-nieuwe-huis-widget-related-articles]';

function NieuweHuisWidgetContent(element) {
    var component = this;
    component.$element = (0, _jquery2.default)(element);
    component.urlFeed = element.getAttribute(CATEGORIES_URL_ATTR);
    (0, _contentFetch2.default)(element, component.urlFeed);
}

// turn all elements with the default selector into components
(0, _jquery2.default)(COMPONENT_SELECTOR).each(function (index, element) {
    return new NieuweHuisWidgetContent(element);
});

},{"../content-fetch/content-fetch":222,"jquery":"jquery"}],277:[function(require,module,exports){
// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

var _mediaViewerPlattegrond = require('../media-viewer-plattegrond/media-viewer-plattegrond');

var _mediaViewerPlattegrond2 = _interopRequireDefault(_mediaViewerPlattegrond);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = NieuweHuisWidget;


// component configuration
var COMPONENT_SELECTOR = '[data-bricks]';
var BRICK_SELECTOR = '[data-brick]';
var BRICK_ODD_SELECTOR = '[data-bricks-odd]';
var BRICK_EVEN_SELECTOR = '[data-bricks-even]';
var COLUMN_CLASS = '.bricks-column';
var BP_MULTIPLE_COLUMNS = 1020;
var $window = (0, _jquery2.default)(window);

function NieuweHuisWidget(element) {
    var component = this;
    component.$element = (0, _jquery2.default)(element);
    component.$children = component.$element.find(BRICK_SELECTOR);
    component.scanMq(component.$element);
}

/*
    Method to create 2 columns on large BP
    `bricks` is an object `[data-bricks]`
    `mq` is a string, defined on styling.
*/
NieuweHuisWidget.prototype.prependBricksOnColumns = function (bricks, mq) {
    (0, _jquery2.default)(bricks).prepend('<div class=' + mq + ' data-bricks-even></div>');
    (0, _jquery2.default)(bricks).prepend('<div class=' + mq + ' data-bricks-odd></div>');
};

NieuweHuisWidget.prototype.prependBricks = function (bricks, mq) {
    (0, _jquery2.default)(bricks).prepend('<div class=' + mq + '></div>');
};

NieuweHuisWidget.prototype.appendDataOnMultipleColumns = function () {
    var component = this;
    component.$children.each(function (i, el) {
        if (i % 2 === 0) {
            (0, _jquery2.default)(el).appendTo(BRICK_ODD_SELECTOR);
        } else {
            (0, _jquery2.default)(el).appendTo(BRICK_EVEN_SELECTOR);
        }
    });
};

NieuweHuisWidget.prototype.appendDataOnSingleColumn = function () {
    var component = this;
    component.$children.each(function (i, el) {
        (0, _jquery2.default)(el).appendTo(COLUMN_CLASS);
    });
};

var PLATTEGROND_SELECTOR = '[data-media-viewer-plattegrond]';

NieuweHuisWidget.prototype.handleMqLarge = function (bricks, mq) {
    var component = this;
    component.prependBricksOnColumns(bricks, mq);
    component.appendDataOnMultipleColumns();

    (0, _jquery2.default)(PLATTEGROND_SELECTOR).each(function (index, element) {
        return new _mediaViewerPlattegrond2.default(element);
    });
};

NieuweHuisWidget.prototype.handleMqDefault = function (bricks, mq) {
    var component = this;
    component.prependBricks(bricks, mq);
    component.appendDataOnSingleColumn();
};

NieuweHuisWidget.prototype.scanMq = function ($el) {
    var component = this;
    $el.each(function () {
        component.detectMqVersion(this, BP_MULTIPLE_COLUMNS);
    });
};

/*
    Method to create 2 columns on large BP
    `bricks` is an object `[data-bricks]`
    `setBpForMultipleColumns` is a number (px) to define when to toggle the multiple columns layout.
*/
NieuweHuisWidget.prototype.detectMqVersion = function (bricks, setBpForMultipleColumns) {
    var component = this;
    var mediaquery = window.getComputedStyle(bricks, ':before').content;

    if ($window.width() >= setBpForMultipleColumns) {
        component.loadMultipleColumnsBricks(bricks, mediaquery);
    } else if ((0, _jquery2.default)(bricks).hasClass('loaded-multiple-column') && $window.width() >= setBpForMultipleColumns || (0, _jquery2.default)(bricks).hasClass('loaded-single-column') && $window.width() <= setBpForMultipleColumns) {
        return;
    } else {
        component.loadSingleColumnBricks(bricks, mediaquery);
    }
};

NieuweHuisWidget.prototype.loadMultipleColumnsBricks = function (bricks, mediaquery) {
    var component = this;
    (0, _jquery2.default)(bricks).addClass('loaded-multiple-column');
    component.handleMqLarge(bricks, mediaquery);
};

NieuweHuisWidget.prototype.loadSingleColumnBricks = function (bricks, mediaquery) {
    var component = this;
    (0, _jquery2.default)(bricks).addClass('loaded-single-column');
    component.handleMqDefault(bricks, mediaquery);
};

// turn all elements with the default selector into components
(0, _jquery2.default)(COMPONENT_SELECTOR).each(function (index, element) {
    return new NieuweHuisWidget(element);
});

},{"../media-viewer-plattegrond/media-viewer-plattegrond":270,"jquery":"jquery"}],278:[function(require,module,exports){
// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

// explicitly inject dependencies (alphabetically), only those needed

var $ = require('jquery');

// what does this module expose?
module.exports = NotificationCookie;

// component configuration
var COMPONENT_SELECTOR = '[data-notification-cookie]';
var HANDLE_SELECTOR = '[data-notification-cookie-close]';
var VERSION_ATTR = 'data-notification-cookie-version';
var SOORTAANBOD_ATTR = 'data-notification-cookie-soortaanbod';
var EVENT_NAMESPACE = '.notification-cookie';
var COOKIE_PREFIX = 'notificationAcknowledged_';
var ACKNOWLEDGED_CLASS = 'is-acknowledged';
var READ_MORE_SELECTOR = '[data-read-more-link]';

function NotificationCookie(element) {
    var component = this;
    component.$element = $(element);
    component.version = element.getAttribute(VERSION_ATTR);
    component.soortAanbod = element.getAttribute(SOORTAANBOD_ATTR);
    component.cookieName = COOKIE_PREFIX + component.soortAanbod + '_' + component.version;

    component.$element.on('click' + EVENT_NAMESPACE, HANDLE_SELECTOR, function (event) {
        event.preventDefault();
        component.acknowledgeNotification();
        component.$element.addClass(ACKNOWLEDGED_CLASS);
    });
    component.$element.find(READ_MORE_SELECTOR).on('click', function () {
        component.acknowledgeNotification();
        component.$element.addClass(ACKNOWLEDGED_CLASS);
    });
}

NotificationCookie.prototype.acknowledgeNotification = function () {
    this.setCookie();
};

/**
 * sets cookie and return the cookie value
 * @returns {string}
 */
NotificationCookie.prototype.setCookie = function () {
    var component = this;
    var date = new Date();
    date.setFullYear(date.getFullYear() + 1);
    var cookie = component.cookieName + '=' + component.version + '; expires=' + date.toGMTString() + '; path= /';

    document.cookie = cookie;
    return cookie;
};

// turn all elements with the default selector into components
$(COMPONENT_SELECTOR).each(function (index, element) {
    return new NotificationCookie(element);
});

},{"jquery":"jquery"}],279:[function(require,module,exports){
// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

// explicitly inject dependencies (alphabetically), only those needed

var $ = require('jquery');

// what does this module expose?
module.exports = NotificationStatus;

// component configuration
var COMPONENT_SELECTOR = '[data-notification-status]';
var STATUS_DOMAIN = 'data-funda-status-domain';
var CONFIG_URL_RANDOM = '.js?rnd=' + Math.random().toString();
var STATUS_AREA = 'data-notification-status-area';
var STATUS_NOTIFICATION_HTML = 'data-notification-status-html';
var STATUS_INSERT_TOP = 'data-notification-status-insert-top';

function NotificationStatus(element) {
    var component = this;
    var area = element.getAttribute(STATUS_AREA);
    var html = element.getAttribute(STATUS_NOTIFICATION_HTML);
    var insertTop = element.getAttribute(STATUS_INSERT_TOP);
    component.statusDomain = element.getAttribute(STATUS_DOMAIN);
    // 'window.statusScriptSettings' is a dependency of the script that gets attached to the DOM.
    window.statusScriptSettings = {
        options: {},
        site: 'funda'
    };
    window.statusScriptSettings.options[area] = true;
    if (html) {
        window.statusScriptSettings.options.html = html;
    }
    if (insertTop && insertTop == 'true') {
        window.statusScriptSettings.options.insertTop = true;
    }
    component.loadStatus();
}

NotificationStatus.prototype.loadStatus = function () {
    var component = this;
    var statusScript = document.createElement('script');
    var anchor = document.body.appendChild(statusScript);
    var statusScriptSettings = '/js/' + window.statusScriptSettings.site + CONFIG_URL_RANDOM;
    statusScript.setAttribute('defer', 'defer');
    statusScript.setAttribute('src', document.location.protocol + '//' + component.statusDomain + statusScriptSettings);
    anchor.parentNode.insertBefore(statusScript, anchor);
};

// turn all elements with the default selector into components
if (COMPONENT_SELECTOR.length) {
    $(COMPONENT_SELECTOR).each(function (index, element) {
        return new NotificationStatus(element);
    });
}

},{"jquery":"jquery"}],280:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = Notification;


var COMPONENT_SELECTOR = '[data-notification]';
var CLOSE_SELECTOR = '[data-notification-close]';
var TEXT_CONTAINER_SELECTOR = '[data-text-container]';
var HIDDEN_CLASS = 'is-hidden';

function Notification(element) {
    var component = this;

    component.$element = (0, _jquery2.default)(element);
    component.$close = component.$element.find(CLOSE_SELECTOR);
    component.$textContainer = component.$element.find(TEXT_CONTAINER_SELECTOR);

    component.bindEvents();
}

Notification.prototype.bindEvents = function () {
    var component = this;

    component.$element.on('show', function (event, message) {
        return component.show(event, message);
    });
    component.$element.on('hide', function () {
        return component.close();
    });
    component.$close.on('click', function (event) {
        return component.close(event);
    });
};

Notification.prototype.close = function (event) {
    var component = this;

    if (typeof event !== 'undefined' && typeof event.preventDefault === 'function') {
        event.preventDefault();
    }

    component.$element.addClass(HIDDEN_CLASS);
    component.$element.trigger('close');
};

Notification.prototype.show = function (event, message) {
    var component = this;

    if (typeof message === 'string') {
        component.$textContainer.html(message);
    }

    component.$element.removeClass(HIDDEN_CLASS);
};

(0, _jquery2.default)(COMPONENT_SELECTOR).get().map(function (element) {
    return new Notification(element);
});

},{"jquery":"jquery"}],281:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

var _asyncRatingContainer = require('../async-rating-container/async-rating-container');

var _asyncRatingContainer2 = _interopRequireDefault(_asyncRatingContainer);

var _asyncRatingTrigger = require('../async-rating-container/async-rating-trigger');

var _asyncRatingTrigger2 = _interopRequireDefault(_asyncRatingTrigger);

var _userSaveObject = require('../user-save-object/user-save-object');

var _userSaveObject2 = _interopRequireDefault(_userSaveObject);

var _serviceController = require('../service-controller/service-controller');

var _serviceController2 = _interopRequireDefault(_serviceController);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = ObjectActions;


var COMPONENT_SELECTOR = '[data-object-actions]';

function ObjectActions(element) {
    var component = this;
    component.$element = (0, _jquery2.default)(element);

    //If the async-rating container is not present, this class should not be loaded.
    if (!component.$element.find(_asyncRatingContainer2.default.getSelector()).length) {
        return;
    }
    component.ratingContainer = _serviceController2.default.getInstance(_asyncRatingContainer2.default, element);
    component.notitieTrigger = _serviceController2.default.getInstance(_asyncRatingTrigger2.default, element);
    component.saveButton = _serviceController2.default.getInstance(_userSaveObject2.default, element);

    component.saveButton.onSaved(function (isSaved) {
        if (!isSaved) {
            component.ratingContainer.formInstance.reset();
        }
    });

    component.getRating = function () {
        return component.ratingContainer.overallRatingInstance.getValue();
    };

    component.ratingContainer.expandInstance.onChange(function (isExpanded) {
        if (!isExpanded && component.getRating() == 0) {
            component.hide();
        }
    });

    component.ratingContainer.formInstance.onSubmit(function () {
        component.saveButton.save(true);
    });

    component.ratingContainer.formInstance.onReset(function () {
        component.hide();
    });

    component.notitieTrigger.onTrigger(function () {
        component.show();
    });

    component.show = function () {
        component.ratingContainer.show();
        component.notitieTrigger.hide();
    };

    component.hide = function () {
        component.ratingContainer.hide();
        component.notitieTrigger.show();
    };
}

ObjectActions.getSelector = function () {
    return COMPONENT_SELECTOR;
};
_serviceController2.default.getAllInstances(ObjectActions);

},{"../async-rating-container/async-rating-container":213,"../async-rating-container/async-rating-trigger":214,"../service-controller/service-controller":317,"../user-save-object/user-save-object":337,"jquery":"jquery"}],282:[function(require,module,exports){
// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

// explicitly inject dependencies (alphabetically), only those needed

var $ = require('jquery');
require('../../components/log-request/log-request');
//todo: use expandable component for clicked class if safari allows it

// what does this module expose?
module.exports = ObjectContact;

// Component configuration
var COMPONENT_ATTR = 'data-object-contact';
var COMPONENT_SELECTOR = '[' + COMPONENT_ATTR + ']';
var TELEPHONE_HANDLE_ATTR = 'data-object-contact-phone-handle';
var TELEPHONE_HANDLE_SELECTOR = '[' + TELEPHONE_HANDLE_ATTR + ']';
var CLICKED_CLASS = 'is-clicked';
var ENHANCED_CLASS = 'is-enhanced';

function ObjectContact(element) {
    var component = this;
    component.$element = $(element);
    component.$element.addClass(ENHANCED_CLASS);

    component.$element.find(TELEPHONE_HANDLE_SELECTOR).each(function (index, phoneElement) {
        var $handle = $(phoneElement);
        var $button = $handle.find('button');
        var $link = $handle.find('a');

        $button.on('click', function () {
            $handle.addClass(CLICKED_CLASS);

            // ADA-53: Create delay + separate targeting as workaround for Safari drawing bug
            $link.addClass(CLICKED_CLASS);
            setTimeout(function () {
                $button.addClass(CLICKED_CLASS);
            }, 0);
        });
    });
}

// turn all elements with the default selector into components
$(COMPONENT_SELECTOR).each(function (index, element) {
    return new ObjectContact(element);
});

},{"../../components/log-request/log-request":248,"jquery":"jquery"}],283:[function(require,module,exports){
// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

// dependencies (alphabetically)

var $ = require('jquery');
var debounce = require('lodash/debounce');

module.exports = ObjectDescription;

// Component configuration
var COMPONENT_ATTR = 'data-object-description';
var COMPONENT_SELECTOR = '[' + COMPONENT_ATTR + ']';
var BODY_SELECTOR = '[data-object-description-body]';
var CONFIG_SELECTOR = '[data-object-description-config]';
var CONTENT_TYPE_SELECTOR = '[data-object-description-strip-markup]';
var ENHANCED_CLASS = 'is-expandable';
var EXPANDED_CLASS = 'is-expanded';
var DEFAULT_CONFIG = { '0px': 350 };

function ObjectDescription(element) {
    var component = this;

    // Enhance the element to trigger styles for the enhanced UI elements
    component.$element = $(element);
    component.$element.addClass(ENHANCED_CLASS);

    component.$body = component.$element.find(BODY_SELECTOR);
    component.htmlContent = component.$body.html();
    component.textContent = component.$body.text();

    // If there is no config on the template
    if (component.$element.find(CONFIG_SELECTOR).length > 0) {
        component.config = JSON.parse(component.$element.find(CONFIG_SELECTOR).text());
    } else {
        component.config = DEFAULT_CONFIG;
    }

    // Collapse the component only if it has more than 300 characters of content
    if (component.textContent.length < 300) {
        component.$element.addClass(EXPANDED_CLASS);
    } else {
        component.checkBreakpoints(component.config);
        component.bindEvents();
    }
}

ObjectDescription.prototype.bindEvents = function () {
    var component = this;

    // Wire-up the event handler to a handle element nested inside the component
    component.$element.on('click', function () {
        component.expand();
    });

    // Attach event on resize for breakpoints
    $(window).on('resize', debounce(function () {
        component.checkBreakpoints(component.config);
    }, 100));
};

ObjectDescription.prototype.checkBreakpoints = function (breakpoints) {
    var component = this;
    var windowSize = $(window).width();
    var matchedBreakpointCharacterCount;

    Object.getOwnPropertyNames(breakpoints).forEach(function (value) {
        // Remove px from values
        var size = value.substr(0, value.length - 2);
        if (windowSize > size) {
            matchedBreakpointCharacterCount = breakpoints[value];
        }
    });

    // Only collapse if there is a matching breaking point on the config
    if (!component.$element.hasClass(EXPANDED_CLASS) && typeof matchedBreakpointCharacterCount !== 'undefined') {
        component.collapse(matchedBreakpointCharacterCount);
    } else {
        component.expand();
    }
};

ObjectDescription.prototype.getCorrectOriginalContent = function () {
    var component = this;

    // Only show original version of the text if it was set as a config option.
    if (component.$element.find(CONTENT_TYPE_SELECTOR).length > 0) {
        return component.textContent;
    } else {
        return component.htmlContent;
    }
};

ObjectDescription.prototype.collapse = function (maxTextLength) {
    var component = this;

    // With the chosen content, create a shortened version of it
    var text = component.getCorrectOriginalContent();
    var shortenedText = text.substr(0, maxTextLength) + ' …';
    component.$body.html(shortenedText);
    component.$element.attr('aria-expanded', 'false');
};

ObjectDescription.prototype.expand = function () {
    var component = this;

    if (component.$element.hasClass(EXPANDED_CLASS)) {
        return;
    }

    // Restore the original content
    component.$body.html(component.htmlContent);
    component.$element.addClass(EXPANDED_CLASS);
    component.$element.attr('aria-expanded', 'true');
    $.event.trigger({ type: 'desciptionExpanded' });
};

$(COMPONENT_SELECTOR).each(function (index, element) {
    return new ObjectDescription(element);
});

},{"jquery":"jquery","lodash/debounce":158}],284:[function(require,module,exports){
// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

// explicitly inject dependencies (alphabetically), only those needed

var $ = require('jquery');

// what does this module expose?
module.exports = ObjectDetailInteraction;

// component configuration
var COMPONENT_SELECTOR = '[data-interaction-logging]';
var SCROLL_WATCH_SELECTOR = '[data-scroll-watch]';
var SWIPE_SELECTOR = '[data-interaction-swipe]';
var CLICK_SELECTOR = '[data-interaction-click]';

var LOGGING_ATTR = 'data-interaction';
var URL_LOGGING_ATTR = 'data-interaction-url';
var TINYID_ATTR = 'data-interaction-tinyid';
var OVERRIDE_ATTR = 'data-interaction-override';

var $body = $('body');

function ObjectDetailInteraction(element) {
    var _this = this;

    var component = this;

    component.$element = $(element);
    component.loggingUrl = element.getAttribute(URL_LOGGING_ATTR);
    component.tinyId = element.getAttribute(TINYID_ATTR);
    component.loggedActions = [];

    $body.on('click', '*' + CLICK_SELECTOR, function (event) {
        return _this.registerInteractionHandler(event);
    });

    $body.on('swipe', '*' + SWIPE_SELECTOR, function (event) {
        return _this.registerInteractionHandler(event);
    });

    registerInteractionWatchers(component);
}

ObjectDetailInteraction.prototype.registerInteractionHandler = function (event) {
    // when override selector is found prevent default action and axecute when post returns
    if (event.currentTarget.hasAttribute(OVERRIDE_ATTR)) {
        event.preventDefault();
    }
    this.logInteraction(event.currentTarget);
};

function registerInteractionWatchers(component) {
    // handle scroll interactions
    var scrollWatchers = $(SCROLL_WATCH_SELECTOR);
    if (scrollWatchers.length > 0) {
        // add watched items (currently not in viewport) to array
        component.watchedScrollElements = [];
        for (var watchIdX = 0; watchIdX < scrollWatchers.length; watchIdX++) {
            if (!component.isElementInViewport(scrollWatchers[watchIdX])) {
                component.watchedScrollElements.push(scrollWatchers[watchIdX]);
            }
        }
        // if any items in array bind to scroll event
        if (component.watchedScrollElements.length > 0) {
            $(window).on('scroll', component.debounce(function () {
                for (var watchIdY = 0; watchIdY < component.watchedScrollElements.length; watchIdY++) {
                    var checkElement = component.watchedScrollElements[watchIdY];
                    if (component.isElementInViewport(checkElement)) {
                        component.logInteraction(checkElement);
                    }
                }
            }, 100));
        }
    }
}

ObjectDetailInteraction.prototype.logInteraction = function (element) {
    var component = this;
    var logAction = $(element).attr(LOGGING_ATTR);
    // only send item once
    if (component.loggedActions.indexOf(logAction) === -1) {
        component.loggedActions.push(logAction);
        $.ajax({
            type: 'POST',
            url: component.loggingUrl,
            data: { tinyId: component.tinyId, logAction: logAction }
        }).always(function () {
            // act when override selector is found
            if (element.hasAttribute(OVERRIDE_ATTR)) {
                var href = element.getAttribute('href');
                if (href !== null && href !== '') {
                    document.location = href;
                }
            }
        });
    }
};

ObjectDetailInteraction.prototype.debounce = function (func, wait, immediate) {
    var timeout;
    return function () {
        var context = this;
        var args = arguments;
        var later = function later() {
            timeout = null;
            if (!immediate) {
                func.apply(context, args);
            }
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) {
            func.apply(context, args);
        }
    };
};

ObjectDetailInteraction.prototype.isElementInViewport = function (element) {
    var checkElement = element;
    // get DOM element from jquery
    if (typeof $ === 'function' && element instanceof $) {
        checkElement = element[0];
    }
    var rect = checkElement.getBoundingClientRect();

    return rect.top >= 0 && rect.left >= 0 && rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) && rect.right <= (window.innerWidth || document.documentElement.clientWidth);
};

// turn all elements with the default selector into components
$(COMPONENT_SELECTOR).each(function (index, element) {
    return new ObjectDetailInteraction(element);
});

},{"jquery":"jquery"}],285:[function(require,module,exports){
'use strict';

// dependencies (alphabetically)

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = ObjectKenmerken;

// component configuration

var COMPONENT_ATTR = 'data-object-kenmerken';
var COMPONENT_SELECTOR = '[' + COMPONENT_ATTR + ']';
var HANDLE_ATTR = 'data-object-kenmerken-handle';
var HANDLE_SELECTOR = '[' + HANDLE_ATTR + ']';
var ENHANCED_CLASS = 'is-expandible';
var EXPANDED_CLASS = 'is-expanded';
var CONTENT_ATTR = 'data-object-kenmerken-body';
var CONTENT_SELECTOR = '[' + CONTENT_ATTR + ']';

function ObjectKenmerken(element) {
    var component = this;
    component.$element = (0, _jquery2.default)(element);

    if (window.matchMedia) {
        var mq = '(max-width: 750px)';
        // collapsed by default on smaller screens
        if (window.matchMedia(mq).matches) {
            component.$element.removeClass(EXPANDED_CLASS);
        }

        // add listener for when screen is resized
        matchMedia(mq).addListener(function (mql) {
            if (mql.matches) {
                // screen became small
                component.collapsedHeight = component.$content.children().eq(0).outerHeight() + component.$content.children().eq(1).outerHeight() + component.$handle.outerHeight();

                component.toggleExpand(false);
            } else {
                component.toggleExpand(true);
            }
        });
    }

    // define relevant dom elements and initial value
    component.expandibleId = element.getAttribute(COMPONENT_ATTR) || false;
    component.isExpanded = component.$element.hasClass(EXPANDED_CLASS);
    component.$handle = component.$element.find(HANDLE_SELECTOR);
    component.$handlesOutside = (0, _jquery2.default)();

    // if component has an ID, look for handles outside the component with that ID
    if (component.expandibleId) {
        component.$handlesOutside = (0, _jquery2.default)('body').find('[' + HANDLE_ATTR + '="' + component.expandibleId + '"]');
    }

    // get full height before the collapsing
    component.$content = component.$element.find(CONTENT_SELECTOR).first();
    component.fullHeight = component.$content.outerHeight(true);

    // enhance, set initial state, and toggle when handles are triggered
    component.$element.addClass(ENHANCED_CLASS);

    // and one more time after collapsing
    component.collapsedHeight = component.$content.outerHeight(true);

    component.toggleExpand(component.isExpanded); //init once

    component.$element.on('click', HANDLE_SELECTOR, function () {
        component.toggleExpand();
    });

    component.$handlesOutside.on('click', function () {
        component.toggleExpand();
    });
}

/**
 * Toggle (expand / collapse) the expand state by adding / removing the expanded class.
 * @param {Boolean} [isExpanded]    Set true to force component to expand (optional).
 * @returns {Boolean}               True if component is expanded.
 */
ObjectKenmerken.prototype.toggleExpand = function (isExpanded) {
    var component = this;
    component.isExpanded = isExpanded !== undefined ? isExpanded : !component.isExpanded;
    component.$element.toggleClass(EXPANDED_CLASS, component.isExpanded);
    component.$handlesOutside.toggleClass(EXPANDED_CLASS, component.isExpanded);
    component.$element.attr('aria-expanded', component.isExpanded);

    if (component.isExpanded) {
        component.$content.css('height', 'auto');
        ObjectKenmerken.sendKenmerkenExpandedEvent();
    } else {
        component.$content.css('height', component.collapsedHeight + 'px');
    }

    return component.isExpanded;
};

ObjectKenmerken.sendKenmerkenExpandedEvent = function () {
    (0, _jquery2.default)(document).trigger('kenmerkenExpanded', { selector: '[data-object-kenmerken]' });
};

// turn all elements with the default selector into components
(0, _jquery2.default)(COMPONENT_SELECTOR).each(function (index, element) {
    return new ObjectKenmerken(element);
});

},{"jquery":"jquery"}],286:[function(require,module,exports){
/* global google */

'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.getKadasterLayer = getKadasterLayer;
exports.getKadasterBaseLayer = getKadasterBaseLayer;


function getKadasterBaseLayer() {
    var styles = [{
        'featureType': 'all',
        'elementType': 'labels.text.fill',
        'stylers': [{ 'saturation': 36 }, { 'color': '#333333' }, { 'lightness': 40 }]
    }, {
        'featureType': 'all',
        'elementType': 'labels.text.stroke',
        'stylers': [{ 'visibility': 'on' }, { 'color': '#ffffff' }, { 'lightness': 16 }]
    }, {
        'featureType': 'all',
        'elementType': 'labels.icon',
        'stylers': [{ 'visibility': 'off' }]
    }, {
        'featureType': 'administrative',
        'elementType': 'geometry.fill',
        'stylers': [{ 'color': '#fefefe' }, { 'lightness': 20 }]
    }, {
        'featureType': 'administrative',
        'elementType': 'geometry.stroke',
        'stylers': [{ 'color': '#fefefe' }, { 'lightness': 17 }, { 'weight': 1.2 }]
    }, {
        'featureType': 'landscape',
        'elementType': 'geometry',
        'stylers': [{ 'color': '#f5f5f5' }, { 'lightness': 20 }]
    }, {
        'featureType': 'poi',
        'elementType': 'geometry',
        'stylers': [{ 'color': '#f5f5f5' }, { 'lightness': 21 }]
    }, {
        'featureType': 'poi.park',
        'elementType': 'geometry',
        'stylers': [{ 'color': '#cce69f' }, { 'lightness': 21 }]
    }, {
        'featureType': 'road.highway',
        'elementType': 'geometry.fill',
        'stylers': [{ 'color': '#ffffff' }, { 'lightness': 17 }]
    }, {
        'featureType': 'road.highway',
        'elementType': 'geometry.stroke',
        'stylers': [{ 'color': '#ffffff' }, { 'lightness': 29 }, { 'weight': 0.2 }]
    }, {
        'featureType': 'road.arterial',
        'elementType': 'geometry',
        'stylers': [{ 'color': '#ffffff' }, { 'lightness': 18 }]
    }, {
        'featureType': 'road.local',
        'elementType': 'geometry',
        'stylers': [{ 'color': '#ffffff' }, { 'lightness': 16 }]
    }, {
        'featureType': 'transit',
        'elementType': 'geometry',
        'stylers': [{ 'lightness': '21' }, { 'color': '#e6e6e6' }, { 'visibility': 'on' }]
    }, {
        'featureType': 'transit.station.airport',
        'elementType': 'geometry.fill',
        'stylers': [{ 'color': '#f2f2f2' }, { 'lightness': '-3' }]
    }, {
        'featureType': 'water',
        'elementType': 'geometry',
        'stylers': [{ 'color': '#5f94ff' }, { 'gamma': '6' }, { 'lightness': '-5' }]
    }];

    return new google.maps.StyledMapType(styles, {
        name: 'Kadastraal Base Layer',
        maxZoom: 23,
        minZoom: 17
    });
}

function getKadasterLayer(map, baseKadasterUrl) {
    return new google.maps.ImageMapType({
        getTileUrl: function getTileUrl(coord, zoom) {
            var zfactor = Math.pow(2, zoom);
            var projection = map.getProjection();
            var top = projection.fromPointToLatLng(new google.maps.Point(coord.x * 256 / zfactor, coord.y * 256 / zfactor));
            var bot = projection.fromPointToLatLng(new google.maps.Point((coord.x + 1) * 256 / zfactor, (coord.y + 1) * 256 / zfactor));
            var bbox = top.lng() + ',' + bot.lat() + ',' + bot.lng() + ',' + top.lat();
            return baseKadasterUrl.replace('{bbox}', bbox);
        },
        tileSize: new google.maps.Size(256, 256),
        isPng: true,
        name: 'Bebouwing',
        maxZoom: 21,
        minZoom: 17
    });
}

},{}],287:[function(require,module,exports){
/* global google, gtmDataLayer */
'use strict';

// dependencies (alphabetically)

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

var _throttle = require('lodash/throttle');

var _throttle2 = _interopRequireDefault(_throttle);

var _debounce = require('lodash/debounce');

var _debounce2 = _interopRequireDefault(_debounce);

var _gmapsAsync = require('../gmaps-async/gmaps-async');

var _gmapsAsync2 = _interopRequireDefault(_gmapsAsync);

var _gmapsKadastraalLayers = require('./gmaps-kadastraal-layers');

var kadastraalLayers = _interopRequireWildcard(_gmapsKadastraalLayers);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = ObjectMap;

// component configuration

var COMPONENT_SELECTOR = '[data-object-map]';
var CANVAS_SELECTOR = '[data-object-map-canvas]';
var CONFIG_SELECTOR = '[data-object-map-config]';
var ENHANCED_CLASS = 'is-interactive-map';
var EVENT_NAMESPACE = '.object-map';
var LOADED_CLASS = 'is-loaded';
var INIT_TYPE_ATTR = 'data-object-map-init-type';

var MAP_LEGEND_SELECTOR = '[data-object-map-legend]';
var MAP_TYPE_SELECT_SELECTOR = '[data-object-map-type-select]';
var MAP_TYPE_SELECT_DEFAULT_SELECTOR = '.view-default';
var MAP_TYPE_SELECT_STREET_VIEW_SELECTOR = '.view-streetview';
var MAP_TYPE_SELECT_SATELLITE_VIEW_SELECTOR = '.view-satellite';
var MAP_TYPE_SELECT_KADASTRAAL_SELECTOR = '.view-kadastraal';
var MAP_ZOOM_CONTROLS_SELECTOR = '.object-map-zoom-controls';
var MAP_ZOOM_ZOOMIN_SELECTOR = '[data-map-zoom-zoomin]';
var MAP_ZOOM_ZOOMOUT_SELECTOR = '[data-map-zoom-zoomout]';
var DEFAULT_ZOOM_LEVEL = 14;
var MIN_ZOOM_LEVEL = 0;
var MAX_ZOOM_LEVEL = 21;
var MIN_ZOOM_LEVEL_KADASTRAAL = 19;

var IS_VISIBLE_CLASS = 'is-visible';
var IS_HIDDEN_CLASS = 'is-hidden';

var $window = (0, _jquery2.default)(window);

function ObjectMap(element) {
    var component = this;

    component.$element = (0, _jquery2.default)(element);
    component.$canvas = component.$element.find(CANVAS_SELECTOR);
    component.$mapLegend = component.$element.find(MAP_LEGEND_SELECTOR);
    component.$mapZoomControls = component.$element.find(MAP_ZOOM_CONTROLS_SELECTOR);
    component.percelenLayer = null;

    if (component.$element.find(CONFIG_SELECTOR).text().length) {
        component.config = JSON.parse(component.$element.find(CONFIG_SELECTOR).text());
    } else {
        component.config = JSON.parse(component.$element.find('[data-object-map-config-injected]').text());
    }

    component.$element.addClass(ENHANCED_CLASS);

    // Set configured map type as checked
    var mapTypeSelectSelector = void 0;
    switch (component.config.defaultMapType) {
        case 'satellite':
            mapTypeSelectSelector = MAP_TYPE_SELECT_SATELLITE_VIEW_SELECTOR;
            break;
        case 'kadastraal':
            mapTypeSelectSelector = MAP_TYPE_SELECT_KADASTRAAL_SELECTOR;
            break;
        case 'roadmap':
        default:
            mapTypeSelectSelector = MAP_TYPE_SELECT_DEFAULT_SELECTOR;
            break;
    }
    (0, _jquery2.default)(mapTypeSelectSelector).prop('checked', true);

    component.$element.on('change', MAP_TYPE_SELECT_SELECTOR, function (event) {
        component.switchMapType(event.currentTarget.value);
    });
    component.$element.on('click', MAP_ZOOM_ZOOMIN_SELECTOR, function (event) {
        component.zoomIn(event);
    });
    component.$element.on('click', MAP_ZOOM_ZOOMOUT_SELECTOR, function (event) {
        component.zoomOut(event);
    });

    var initType = component.$element.attr(INIT_TYPE_ATTR);
    if (initType === 'lazy') {
        $window.on('scroll' + EVENT_NAMESPACE, (0, _throttle2.default)(function () {
            component.checkPosition();
        }, 50)).on('resize' + EVENT_NAMESPACE, (0, _debounce2.default)(function () {
            component.checkPosition();
        }, 200));

        // check once to see if page was (re)loaded with the map already in view.
        component.checkPosition();
    } else {
        component.loadMap();
    }
}

ObjectMap.prototype.switchMapType = function (mapType) {
    var component = this;

    component.map.overlayMapTypes.clear();
    component.$mapLegend.removeClass(IS_VISIBLE_CLASS);

    switch (mapType) {
        case 'default':
            component.panorama.setVisible(false);
            component.map.setMapTypeId(google.maps.MapTypeId.ROADMAP);
            break;

        case 'satellite':
            component.map.setMapTypeId(google.maps.MapTypeId.HYBRID);
            component.panorama.setVisible(false);
            break;

        case 'kadastraal':
            if (component.map.getZoom() < MIN_ZOOM_LEVEL_KADASTRAAL) {
                component.map.setZoom(MIN_ZOOM_LEVEL_KADASTRAAL);
            }
            component.map.overlayMapTypes.insertAt(0, component.percelenLayer);
            component.map.overlayMapTypes.insertAt(0, component.pandenLayer);
            component.map.setMapTypeId('kadastraal');
            component.panorama.setVisible(false);
            component.$mapLegend.addClass(IS_VISIBLE_CLASS);
            break;

        case 'streetview':
            component.panorama.setVisible(true);
            break;
    }
    component.updateGTMMapType(mapType);
};

/**
 * Checks whether the map is inside the view and if so, creates te map
 */
ObjectMap.prototype.checkPosition = function () {
    var component = this;
    var mapOffset = component.$canvas.offset();
    var mapHeight = component.$canvas.height() / 4; // Load when map quarter scrolled in
    var isInView = $window.scrollTop() + $window.height() > mapOffset.top + mapHeight;

    if (isInView) {
        $window.off('scroll' + EVENT_NAMESPACE);
        $window.off('resize' + EVENT_NAMESPACE);
        component.loadMap();
    }
};

ObjectMap.prototype.loadMap = function () {
    var component = this;

    if (component.config.useStubbedMapsApi) {
        // assuming window.google is a stubbed Maps API
        component.createMap();
        return;
    }

    (0, _gmapsAsync2.default)().then(function () {
        component.createMap();

        setTimeout(function () {
            component.$element.addClass(LOADED_CLASS);
            component.switchMapType(component.config.defaultMapType);
        }, 1500); //allow some time for tiles to be loaded
    });
};

/**
 * Create and insert map on component's canvas element.
 * @return {*} component's Google Map instance.
 */
ObjectMap.prototype.createMap = function () {
    var component = this;
    var config = component.config;
    var google = window.google;
    var latLng = new google.maps.LatLng(config.lat, config.lng);

    component.map = new google.maps.Map(component.$canvas[0], component.getMapOptions());
    component.panorama = component.map.getStreetView();

    component.panorama.setOptions(component.getPanoramaOptions());
    component.panorama.setPosition(latLng);

    // Plot marker to the Map
    var marker = {
        url: config.markerUrl,
        anchor: new google.maps.Point(30, 53),
        scaledSize: new google.maps.Size(60, 66)
    };

    component.marker = new google.maps.Marker({
        position: latLng,
        map: component.map,
        title: config.markerTitle,
        icon: marker
    });

    // prepare 2 kadaster layers
    component.map.mapTypes.set('kadastraal', kadastraalLayers.getKadasterBaseLayer());
    component.percelenLayer = kadastraalLayers.getKadasterLayer(component.map, config.baseKadasterUrl);
    component.pandenLayer = kadastraalLayers.getKadasterLayer(component.map, config.basePandenUrl);

    // Set correct point of view for Steet View
    var service = new google.maps.StreetViewService();

    service.getPanoramaByLocation(component.panorama.getPosition(), 50, function (panoramaData) {
        if (panoramaData !== null) {
            (0, _jquery2.default)(MAP_TYPE_SELECT_STREET_VIEW_SELECTOR).addClass(IS_VISIBLE_CLASS);

            component.panorama.setPano(panoramaData.location.pano);

            var panoramaCenter = panoramaData.location.latLng;
            var heading = google.maps.geometry.spherical.computeHeading(panoramaCenter, latLng);

            var pov = component.panorama.getPov();
            pov.heading = heading;
            component.panorama.setPov(pov);
        }
    });

    // Only show the marker and zoom controls when we're not in Street View
    google.maps.event.addListener(component.map.getStreetView(), 'visible_changed', function () {
        if (this.getVisible()) {
            component.marker.setMap(null);
            component.$mapZoomControls.addClass(IS_HIDDEN_CLASS);
        } else {
            component.marker.setMap(component.map);
            component.$mapZoomControls.removeClass(IS_HIDDEN_CLASS);
        }
    });

    return component.map;
};

/**
 * Create and return map options based on component's configuration.
 * @returns {Object} map options
 */
ObjectMap.prototype.getMapOptions = function () {
    var config = this.config;
    var google = window.google;

    return {
        zoom: config.zoom || DEFAULT_ZOOM_LEVEL,
        center: new google.maps.LatLng(config.lat, config.lng),
        scrollwheel: false,
        gestureHandling: 'cooperative',
        // Map control options
        disableDefaultUI: true,
        mapTypeControlOptions: {
            mapTypeIds: [google.maps.MapTypeId.ROADMAP, google.maps.MapTypeId.SATELLITE, 'kadastraal']
        },
        mapTypeId: config.defaultMapType,

        scaleControl: true,
        overviewMapControl: true,
        zoomControlOptions: {
            style: google.maps.ZoomControlStyle.SMALL
        },

        // Remove POI
        styles: [{
            featureType: 'poi',
            elementType: 'labels',
            stylers: [{ visibility: 'off' }]
        }]
    };
};

ObjectMap.prototype.updateGTMMapType = function (mapTypeName) {
    if (window.gtmDataLayer !== undefined) {
        gtmDataLayer.push({
            'event': 'mapTypeChanged',
            'mapTypeName': mapTypeName
        });
    }
};

ObjectMap.prototype.getPanoramaOptions = function () {
    return {
        addressControl: false,
        linksControl: true,
        panControl: true,
        zoomControl: true,
        fullscreenControl: true,
        enableCloseButton: false
    };
};

ObjectMap.prototype.zoomIn = function (event) {
    var component = this;
    event.preventDefault();
    var currentZoomLevel = component.map.getZoom();

    if (currentZoomLevel !== MAX_ZOOM_LEVEL) {
        component.map.setZoom(currentZoomLevel + 1);
    }
};

ObjectMap.prototype.zoomOut = function (event) {
    var component = this;
    event.preventDefault();
    var currentZoomLevel = component.map.getZoom();

    if (currentZoomLevel !== MIN_ZOOM_LEVEL) {
        component.map.setZoom(currentZoomLevel - 1);
    }
};

// turn all elements with the default selector into components
(0, _jquery2.default)(COMPONENT_SELECTOR).each(function (index, element) {
    return new ObjectMap(element);
});

},{"../gmaps-async/gmaps-async":238,"./gmaps-kadastraal-layers":286,"jquery":"jquery","lodash/debounce":158,"lodash/throttle":188}],288:[function(require,module,exports){
// Require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

// Explicitly inject dependencies (alphabetically), only those needed

var $ = require('jquery');
require('funda-slick-carousel');

// What does this module expose?
module.exports = ObjectMedia;

// Component configuration
var EVENT_NAMESPACE = '.object-media';
var COMPONENT_SELECTOR = '[data-object-media]';
var ITEMS_SELECTOR = '[data-object-media-fotos]';
var SLICK_SLIDE_SPEED = 250;
var SLICK_SWIPE_SENSITIVITY = 12; // 1/12th of the image width

function ObjectMedia(element) {
    var component = this;
    var mediaQuery;

    component.$element = $(element);
    component.$items = component.$element.find(ITEMS_SELECTOR);
    component.slickWidth = 1020; // object-media.scss

    if (window.matchMedia) {
        mediaQuery = window.matchMedia('(max-width: ' + component.slickWidth + 'px)');
        mediaQuery.addListener(function (mediaQueryList) {
            if (mediaQueryList.matches) {
                // Became narrower than breakpoint
                component.initSlick();
            } else {
                component.unbindEvents();
                component.killSlick();
            }
        });

        if (mediaQuery.matches) {
            component.initSlick();
        }
    }
    // Only IE9 does not support matchMedia() and that desktop browser does not need Slick anyway
}

ObjectMedia.prototype.unbindEvents = function () {
    $(document).off('keydown' + EVENT_NAMESPACE);
};

ObjectMedia.prototype.initSlick = function () {
    var component = this;
    component.$items.slick({
        arrows: false,
        infinite: false,
        speed: SLICK_SLIDE_SPEED,
        cssEase: 'ease',
        mobileFirst: true,
        touchThreshold: SLICK_SWIPE_SENSITIVITY,
        useTransform: true,
        lazyLoad: 'anticipated',
        lazyLoadAhead: 3
    });
};

ObjectMedia.prototype.killSlick = function () {
    this.$items.slick('unslick');
};

// Turn all elements with the default selector into components
$(COMPONENT_SELECTOR).each(function (index, element) {
    return new ObjectMedia(element);
});

},{"funda-slick-carousel":"funda-slick-carousel","jquery":"jquery"}],289:[function(require,module,exports){
// import this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

// explicitly inject dependencies (alphabetically), only those needed

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// what does this module expose?
exports.default = ObjectMeldEenFoutForm;

// component configuration

var CLASS_HIDDEN = 'is-hidden';
var CLASS_INPUT_ERROR = 'input-validation-error';
var COMPONENT_SELECTOR = '[data-object-meld-een-fout-form]';
var CONFIG_SELECTOR = '[data-object-meld-een-fout-config]';
var ERROR_MESSAGE_SELECTOR = '[data-meld-een-fout-error-message]';
var FORM_SELECTOR = '[data-object-meld-een-fout-form-form]';
var INPUT_SELECTOR = '[data-meld-een-fout-input]';
var INPUT_SUBQUESTION_SELECTOR = '[data-meld-een-fout-input-subquestion]';
var INPUT_TEXTAREA_LABEL_SELECTOR = '[data-meld-een-fout-textarea-label="Foutmelding"]';
var INPUT_TEXTAREA_SELECTOR = '[data-meld-een-fout-input-message="textarea"]';
var INPUT_TEXTAREA_SUBQUESTION_LABEL_SELECTOR = '[data-meld-een-fout-textarea-label="FoutieveInformatie"]';
var INPUT_TEXTAREA_SUBQUESTION_SELECTOR = '[data-meld-een-fout-input-subquestion="textarea"]';
var SUBQUESTIONS_SELECTOR = '[data-meld-een-fout-subquestions]';
var TYPE_RADIO = 'radio';
var TYPE_TEXTAREA = 'textarea';

/**
 * Constructor method, links child elements to variables for internal use
 *
 * @param {HTMLElement} element The Html element to bind to
 */
function ObjectMeldEenFoutForm(element) {
    var component = this; // Not necessary in ES2015, keep using it for consistency
    component.config = {};
    component.$element = (0, _jquery2.default)(element);
    component.$errorMessage = (0, _jquery2.default)(ERROR_MESSAGE_SELECTOR);
    component.$form = (0, _jquery2.default)(FORM_SELECTOR);
    component.$inputs = (0, _jquery2.default)(INPUT_SELECTOR);
    component.$inputsSubquestion = (0, _jquery2.default)(INPUT_SUBQUESTION_SELECTOR);
    component.$subquestions = (0, _jquery2.default)(SUBQUESTIONS_SELECTOR);
    component.$textAreaMessage = (0, _jquery2.default)(INPUT_TEXTAREA_SELECTOR);
    component.$textAreaMessageLabel = (0, _jquery2.default)(INPUT_TEXTAREA_LABEL_SELECTOR);
    component.$textAreaSubquestion = (0, _jquery2.default)(INPUT_TEXTAREA_SUBQUESTION_SELECTOR);
    component.$textAreaSubquestionLabel = (0, _jquery2.default)(INPUT_TEXTAREA_SUBQUESTION_LABEL_SELECTOR);

    if (component.$element.find(CONFIG_SELECTOR).length > 0) {
        component.config = JSON.parse(component.$element.find(CONFIG_SELECTOR).text());
    }

    component.$form.attr('novalidate', true);

    component.attachEventListeners();
    component.setFormState();
}

/**
 * Registers event listeners
 */
ObjectMeldEenFoutForm.prototype.attachEventListeners = function attachEventListeners() {
    var component = this;
    component.$inputs.on('change', function () {
        return component.onChangeHandler();
    });
    component.$form.on('submit', function (event) {
        return component.onSubmitHandler(event);
    });
};

/**
 * Parses the form data and constructs an object with state properties.
 * Properties:
 * - isOtherMessage     - The other option is selected in the main options list
 * - isOtherWrongInfo   - The other option is selected in the subquestions list
 * - isWrongInfo        - The subquestions should be shown.
 * - messageOtherText   - Is the messages text area filled
 * - messageSet         - Has the Foutmelding radio group a value
 * - wrongInfoOptionSet - Has the FoutieveInformatie radio group a value
 * - wrongInfoOtherText - Is the wrong info text area filled
 *
 * @return {Object} The form state object
 */
ObjectMeldEenFoutForm.prototype.getFormState = function getFormState() {
    var component = this;
    var serializedForm = serializeForm(component.$form);
    var isOtherMessage = !!(serializedForm.Foutmelding === component.config.foutmeldingOtherRadioValue);
    var isOtherWrongInfo = !!(serializedForm.FoutieveInformatie === component.config.foutieveInformatieOtherRadioValue);
    var isWrongInfo = !!(serializedForm.Foutmelding === component.config.foutmeldingWrongInfoRadioValue);
    var messageOtherText = !!serializedForm.FoutmeldingText;
    var messageSet = !!serializedForm.Foutmelding;
    var wrongInfoOptionSet = !!serializedForm.FoutieveInformatie;
    var wrongInfoOtherText = !!serializedForm.FoutieveInformatieText;

    return { isOtherMessage: isOtherMessage, isOtherWrongInfo: isOtherWrongInfo, isWrongInfo: isWrongInfo, messageOtherText: messageOtherText, messageSet: messageSet, wrongInfoOptionSet: wrongInfoOptionSet, wrongInfoOtherText: wrongInfoOtherText };
};

ObjectMeldEenFoutForm.prototype.onChangeHandler = function onChangeHandler() {
    var component = this;
    component.setFormState();
    hideSections(component.$errorMessage);
    removeElementErrorState(component.$textAreaMessageLabel, component.$textAreaSubquestionLabel);
};

ObjectMeldEenFoutForm.prototype.onSubmitHandler = function onSubmitHandler(event) {
    var component = this;
    if (!component.validateForm()) {
        event.preventDefault();
    }
};

/**
 * Hides and shows form elements based on the form values
 */
ObjectMeldEenFoutForm.prototype.setFormState = function setFormState() {
    var component = this;
    var formSate = component.getFormState();
    var $textAreaMessageLabel = component.$textAreaMessageLabel;
    var $inputsSubquestion = component.$inputsSubquestion;
    var $subquestions = component.$subquestions;

    formSate.isOtherMessage ? showSections($textAreaMessageLabel) : hideSections($textAreaMessageLabel);
    formSate.isWrongInfo ? showSections($subquestions, $inputsSubquestion) : hideSections($inputsSubquestion, $subquestions);
};

/**
 * Validates the form before submitting. It returns a boolean value indicating
 * if the form is valid or not. When it is not, it sets the correct error
 * error messages
 * @return {Boolean} Is the form valid or not
 */
ObjectMeldEenFoutForm.prototype.validateForm = function validateForm() {
    var component = this;
    var formState = component.getFormState();

    if (!formState.messageSet) {
        showSections(component.$errorMessage);
        return false;
    }

    if (formState.isWrongInfo && !formState.wrongInfoOptionSet) {
        showSections(component.$errorMessage);
        return false;
    }

    if (formState.isWrongInfo && formState.wrongInfoOptionSet && formState.isOtherWrongInfo && !formState.wrongInfoOtherText) {
        setElementErrorState(component.$textAreaSubquestion);
        showSections(component.$errorMessage);
        return false;
    }

    if (formState.isOtherMessage && !formState.messageOtherText) {
        showSections(component.$errorMessage);
        setElementErrorState(component.$textAreaMessage);
        return false;
    }

    hideSections(component.$errorMessage);
    removeElementErrorState(component.$textAreaMessage, component.$textAreaSubquestion);
    return true;
};

/**
 * Clears the input value for an element.
 * First it checks the type of the element, based on that it clears a checked
 * property (radio buttons) or sets the value to null (textareas)
 *
 * @param  {HTMLElement} element The element to clear
 */
function clearInputValue(element) {
    var $element = (0, _jquery2.default)(element).get(0);

    if ($element.type === TYPE_RADIO) {
        (0, _jquery2.default)(element).prop('checked', false);
    }

    if ($element.type === TYPE_TEXTAREA) {
        (0, _jquery2.default)(element).val(null);
    }
}

/**
 * Hides one or more elements
 * It loops over the provided elements, for every element it clears the value
 * and it adds a hidden class to the elemenet
 * @param  {...HTMLElement} sections A jQuery element collection
 */
function hideSections() {
    for (var _len = arguments.length, sections = Array(_len), _key = 0; _key < _len; _key++) {
        sections[_key] = arguments[_key];
    }

    sections.forEach(function (section) {
        section.each(function (index, element) {
            clearInputValue(element);
            (0, _jquery2.default)(element).addClass(CLASS_HIDDEN);
        });
    });
}

/**
 * Shows one or more elements
 * It loops over the provided elements, for every element it removes a
 * hidden class
 * @param  {...HTMLElement} sections A jQuery element collection
 */
function showSections() {
    for (var _len2 = arguments.length, sections = Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
        sections[_key2] = arguments[_key2];
    }

    sections.forEach(function (section) {
        section.each(function (index, element) {
            (0, _jquery2.default)(element).removeClass(CLASS_HIDDEN);
        });
    });
}

/**
 * Sets an error class on the provided elements
 * It loops over the provided elements, for every element it adds a error class
 * @param  {...HTMLElement} sections A jQuery element collection
 */
function setElementErrorState() {
    for (var _len3 = arguments.length, elements = Array(_len3), _key3 = 0; _key3 < _len3; _key3++) {
        elements[_key3] = arguments[_key3];
    }

    elements.forEach(function (element) {
        element.each(function (index, elem) {
            (0, _jquery2.default)(elem).addClass(CLASS_INPUT_ERROR);
        });
    });
}

/**
 * Removes an error class on the provided elements
 * It loops over the provided elements, for every element it removes error class
 * @param  {...HTMLElement} sections A jQuery element collection
 */
function removeElementErrorState() {
    for (var _len4 = arguments.length, elements = Array(_len4), _key4 = 0; _key4 < _len4; _key4++) {
        elements[_key4] = arguments[_key4];
    }

    elements.forEach(function (element) {
        element.each(function (index, elem) {
            (0, _jquery2.default)(elem).removeClass(CLASS_INPUT_ERROR);
        });
    });
}

/**
 * Serializes the values of a form element into an object. It expects an jQuery
 * object with a form. Via $.serialize() it parses the key/values string. That
 * string is mapped into an object where the keys are the property keys.
 *
 * Object example:
 * {
 *     'Foutmelding': 2
 * }
 *
 * @param  {jQuery Object} $form The jQuery object containing a form element
 * @return {Object}              The serialized object
 */
function serializeForm($form) {
    return $form.serialize().split('&').map(function (stringvalue) {
        return stringvalue.split('=');
    }).map(function (arr) {
        var obj = {};
        obj[arr[0]] = arr[1];
        return obj;
    }).reduce(function (curr, next) {
        return Object.assign(curr, next);
    });
}

// turn all elements with the default selector into components
(0, _jquery2.default)(COMPONENT_SELECTOR).each(function createNewComponent(index, element) {
    return new ObjectMeldEenFoutForm(element);
});

},{"jquery":"jquery"}],290:[function(require,module,exports){
'use strict';

var $ = require('jquery');

module.exports = ObjectRatingForm;

var COMPONENT_SELECTOR = '[data-object-rating-form]';
var RESET_HANDLE_SELECTOR = '[data-object-rating-form-reset-handle]';
var RESET_ACTION_ATTR = 'data-object-rating-form-reset-action';
var OVERALL_RATING_SELECTOR = '[data-overall-object-rating]';
var RATING_SELECTOR = '[data-rating]';
var TEXTAREA_SELECTOR = '[data-object-rating-textarea]';
var RATED_CLASS = 'is-rated';

function ObjectRatingForm(element) {
    var component = this;
    component.element = element;
    component.$element = $(element);
    component.$overallRating = component.$element.find(OVERALL_RATING_SELECTOR);
    component.$overallRatingInputs = component.$overallRating.find('input');
    component.$resetHandle = component.$element.find(RESET_HANDLE_SELECTOR);
    component.$ratings = component.$element.find(RATING_SELECTOR);
    component.$inputs = component.$ratings.find('input');
    component.$textarea = component.$element.find(TEXTAREA_SELECTOR);
    //Watchers
    component.$resetHandle.on('click', function () {
        return component.resetRating();
    });
}

ObjectRatingForm.prototype.resetRating = function () {
    var component = this;
    component.$inputs.removeClass(RATED_CLASS);
    var resetUrl = component.$element.attr(RESET_ACTION_ATTR);
    return $.ajax({
        method: 'POST',
        url: resetUrl
    });
};

$(COMPONENT_SELECTOR).each(function (index, element) {
    return new ObjectRatingForm(element);
});

},{"jquery":"jquery"}],291:[function(require,module,exports){
// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

// explicitly inject dependencies (alphabetically), only those needed

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

require('../radio-group/radio-group');

// what does this module expose?
exports.default = ObjectTypeFilter;

// component configuration

var COMPONENT_SELECTOR = '[data-object-type-filter]';
var OPTION_ATTR = 'data-object-type-non-relevant-filters';
var OPTION_SELECTOR = '[' + OPTION_ATTR + ']';
var TARGET_ATTR = 'data-object-type-filter-target';
var TARGET_SELECTOR = '[' + TARGET_ATTR + ']';
var DISABLED_CLASS = 'is-disabled';

/**
 * @param {HTMLElement} element    containing all options (handles)
 * @constructor
 */
function ObjectTypeFilter(element) {
    var component = this;
    component.$element = (0, _jquery2.default)(element);
    component.$targets = (0, _jquery2.default)(TARGET_SELECTOR);

    component.$element.on('change', OPTION_SELECTOR, function (event) {
        var allFilters = [];
        // enable filters for current filter
        component.$element.find(OPTION_SELECTOR).each(function (index, option) {
            var filters = (0, _jquery2.default)(option).attr(OPTION_ATTR).split(' ');
            component.enableAllFilters(filters);
            allFilters = allFilters.concat(filters);
        });

        // disable filters for current option
        var $input = (0, _jquery2.default)(event.currentTarget);
        if ($input.is(':checked')) {
            var disabledFilters = $input.attr(OPTION_ATTR).split(' ');
            component.disableFilters(allFilters, disabledFilters);
        }
    });
}

/**
 * @param {String[]} disabledFilters
 */
ObjectTypeFilter.prototype.disableFilters = function (allFilters, disabledFilters) {
    var component = this;
    component.$targets.each(function (index, element) {
        var name = element.getAttribute(TARGET_ATTR);
        if (allFilters.indexOf(name) > -1) {
            // uncheck filters that are hidden (by checking the 'geen' option)
            // filters that are hidden are invalid in combination with the selected filter, that's why they need to be deselected
            // for example: aantal kamers needs to be deselected and hidden when parkeergelegenheid is selected
            var option = (0, _jquery2.default)(element).find('input[value$="-geen"]');
            if (!(0, _jquery2.default)(option).is(':checked')) {
                (0, _jquery2.default)(element).find('label[for$="-geen"]').click();
            }
            (0, _jquery2.default)(element).find('.filter-flyout-selected-option').each(function (j, selected) {
                (0, _jquery2.default)(selected).click();
            });
            var isDisabled = disabledFilters.indexOf(name) > -1;
            (0, _jquery2.default)(element).toggleClass(DISABLED_CLASS, isDisabled).trigger('changed');
        }
    });
};

ObjectTypeFilter.prototype.enableAllFilters = function (allFilters) {
    var component = this;
    component.$targets.each(function (index, element) {
        var name = element.getAttribute(TARGET_ATTR);
        if (allFilters.indexOf(name) > -1) {
            (0, _jquery2.default)(element).removeClass(DISABLED_CLASS);
        }
    });
};

// turn all elements with the default selector into components
(0, _jquery2.default)(COMPONENT_SELECTOR).each(function (index, element) {
    return new ObjectTypeFilter(element);
});

},{"../radio-group/radio-group":296,"jquery":"jquery"}],292:[function(require,module,exports){
// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

// explicitly inject dependencies (alphabetically), only those needed

var $ = require('jquery');

// what does this module expose?
module.exports = OptIn;

// component configuration
var COMPONENT_SELECTOR = '[data-opt-in]';
var OPTION_EXPERIMENT_ID_ATTRIBUTE = 'data-opt-in-experiment-id';
var OPTION_EXTERNAL_ATTRIBUTE = 'data-opt-in-is-external';

function OptIn(element) {
    var component = this;
    component.$element = $(element);
    component.experimentId = parseInt(element.getAttribute(OPTION_EXPERIMENT_ID_ATTRIBUTE), 10);
    component.isExternal = element.getAttribute(OPTION_EXTERNAL_ATTRIBUTE) === 'true';

    component.$optInLink = component.$element.find('a');
    component.$optOutForm = component.$element.find('form');

    component.$optInLink.on('click', function optIn() {
        component.optIn();
    });
    component.$optOutForm.on('submit', function optOut(e) {
        component.optOut(e, this);
    });

    // Track as an Optimizely 'experiment' (see http://developers.optimizely.com/javascript/reference/#activate)
    window.optimizely = window.optimizely || [];
    window.optimizely.push(['activate', component.experimentId]);

    // Old website also tracks with an event: _gaq.push(['_trackEvent', 'Intercept', 'Getoond', 'Tevredenheidsonderzoek']);
}

OptIn.prototype.optIn = function () {
    var component = this;

    if (component.isExternal) {
        // We opened a URL in a new tab/window, so we need to close the opt-in in the old tab/window
        component.close();
    }

    // Old website also tracks with an event: _gaq.push(['_trackEvent', 'Intercept', 'Call to action', 'Tevredenheidsonderzoek']);
};

OptIn.prototype.optOut = function (e, form) {
    var component = this;

    var settings = {
        method: form.method,
        data: $(form).serialize(),
        success: function success() {
            component.close();
            // Old website also tracks with an event: _gaq.push(['_trackEvent', 'Intercept', 'Sluiten', 'Tevredenheidsonderzoek']);
        },
        error: function error(jqXHR, textStatus, errorThrown) {
            if (window.console) {
                console.log(jqXHR, textStatus, errorThrown);
            }
        }
    };
    $.ajax(form.action, settings);

    e.preventDefault();
};

OptIn.prototype.close = function () {
    var component = this;
    component.$element.addClass('is-removed');
};

// turn all elements with the default selector into components
$(COMPONENT_SELECTOR).each(function (index, element) {
    return new OptIn(element);
});

},{"jquery":"jquery"}],293:[function(require,module,exports){
'use strict';

// dependencies (alphabetically)

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = Pagination;


module.exports = Pagination;

// component configuration
var COMPONENT_SELECTOR = '[data-pagination]';
var PAGELINK_ATTR = 'data-pagination-page';
var PAGELINK_SELECTOR = '[' + PAGELINK_ATTR + ']';
var INPUT_SELECTOR = '[data-pagination-input]';
var PAGE_ADJUSTED_EVENT = 'pageadjusted';

function Pagination(element) {
    var _this = this;

    this.$element = (0, _jquery2.default)(element);
    this.$pageNumber = (0, _jquery2.default)();

    this.$element.on('click', PAGELINK_SELECTOR, function (event) {
        event.preventDefault();

        var clickedPage = (0, _jquery2.default)(event.currentTarget).attr(PAGELINK_ATTR);
        _this.setPage(clickedPage);
    }).bind(this);
}

/**
 * Scroll to top and trigger instant-search update with new page number
 * @param {number} new page number
 */
Pagination.prototype.setPage = function (number) {
    window.scrollTo(0, 0);
    this.$pageNumber = this.$element.find(INPUT_SELECTOR);
    this.$pageNumber.val(number);
    this.$element.trigger(PAGE_ADJUSTED_EVENT);
};

Pagination.initialize = function () {
    // turn all elements with the default selector into components
    (0, _jquery2.default)(COMPONENT_SELECTOR).each(function (index, element) {
        return new Pagination(element);
    });
};

Pagination.initialize();

},{"jquery":"jquery"}],294:[function(require,module,exports){
// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

// explicitly inject dependencies (alphabetically), only those needed

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _debounce = require('lodash/debounce');

var _debounce2 = _interopRequireDefault(_debounce);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var $ = require('jquery');
var ajax = require('../ajax/ajax');


// what does this module expose?
exports.default = PartialPageUpdate;

// component configuration

var COMPONENT_NAME = 'data-partial-page-update';
var COMPONENT_SELECTOR = '[data-partial-page-update]';
var HANDLE_SELECTOR = '[data-partial-page-update-handle]';
var OUTPUT_SELECTOR = '[data-partial-page-update-output]';
var OUTPUT_KEY_ATTR = 'data-partial-page-update-output';

var URL_SUFFIX = '?ajax=true';
var PUSH_STATE_MARKER = 'push_state_set_by_funda';
var MINIMUM_INTERVAL_BETWEEN_REQUESTS = 500; // ms
var TIMEOUT_SEARCH_REQUEST = 30000; // 30 sec

function PartialPageUpdate(form) {
    var component = this;
    component.$form = $(form);
    component.$handle = component.$form.find(HANDLE_SELECTOR);
    component.url = component.$form.attr(COMPONENT_NAME);
    component.$outputs = $(OUTPUT_SELECTOR);

    component.outputMap = {};
    component.$outputs.each(function (index, output) {
        var key = output.getAttribute(OUTPUT_KEY_ATTR);
        var $outputs = component.outputMap[key] || $();
        component.outputMap[key] = $outputs.add(output);
    });

    component.bindEvents();

    // initialize popstate
    window.history.replaceState(PUSH_STATE_MARKER, window.title);
}

PartialPageUpdate.prototype.bindEvents = function () {
    var component = this;

    function debouncedRequest() {
        return (0, _debounce2.default)(function () {
            component.doRequest();
        }, MINIMUM_INTERVAL_BETWEEN_REQUESTS, { leading: true });
    }

    component.$handle.on('change', debouncedRequest());
};

PartialPageUpdate.prototype.doRequest = function () {
    var component = this;
    var formData = component.$form.serialize();
    // ensure outputs indicate they are updating
    $(document).trigger('resultsUpdating');

    // abort previous request if still pending
    if (component.request) {
        component.request.abort();
    }

    component.request = ajax({
        url: component.url + URL_SUFFIX,
        method: 'GET',
        data: formData,
        timeout: TIMEOUT_SEARCH_REQUEST
    });

    component.request.done(function (data) {
        return component.updatePartialPage(data);
    });
};

/**
 * @param {Object} data
 * @param {Object} data.content
 * @param {String} data.url
 */
PartialPageUpdate.prototype.updatePartialPage = function (data) {
    var component = this;
    // update first all sync elements
    document.title = data.title;

    component.updateHistory(data.url);
    component.renderOutputResults(data);
    // content is been updated. run image-error-fallback
    $(document).trigger('resultsUpdated');
};

/**
 * Render the results from the search request
 * @param {Object} resultData with keys corresponding to output identifiers and their new HTML as values.
 */
PartialPageUpdate.prototype.renderOutputResults = function (resultData) {
    var component = this;
    var fields = resultData.content;

    for (var key in fields) {
        if (fields.hasOwnProperty(key) && component.outputMap[key] && fields[key] !== null) {
            if (component.outputMap[key].is('input')) {
                component.outputMap[key].val(fields[key]).trigger('change');
            } else {
                component.outputMap[key].html(fields[key]);
            }
        }
    }
};

/**
 * Add url to browser history
 * @param {String} url
 */
PartialPageUpdate.prototype.updateHistory = function (url) {
    window.history.pushState(PUSH_STATE_MARKER, window.title, url);
};

// turn all elements with the default selector into components
$(COMPONENT_SELECTOR).each(function (index, element) {
    return new PartialPageUpdate(element);
});

},{"../ajax/ajax":198,"jquery":"jquery","lodash/debounce":158}],295:[function(require,module,exports){
'use strict';

// explicitly inject dependencies (alphabetically), only those needed

var $ = require('jquery');

module.exports = PlaceholderFallback;

// component configuration
var COMPONENT_SELECTOR = '[data-placeholder-fallback]';
var VISIBILITY_CLASS = 'a11y-sr-only';
var SUPPORTS_PLACEHOLDER = $('html').hasClass('supports-placeholder');

function PlaceholderFallback(element) {
    if (SUPPORTS_PLACEHOLDER) {
        $(element).addClass(VISIBILITY_CLASS);
    }
}

$(COMPONENT_SELECTOR).each(function (index, element) {
    return new PlaceholderFallback(element);
});

},{"jquery":"jquery"}],296:[function(require,module,exports){
'use strict';

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

var _expandible = require('../expandible/expandible');

var _expandible2 = _interopRequireDefault(_expandible);

var _appliedFilters = require('../applied-filters/applied-filters');

var _appliedFilters2 = _interopRequireDefault(_appliedFilters);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

require('../filter-flyout/filter-flyout');
var Separator = require('../thousand-separator/thousand-separator');

module.exports = RadioGroup;

var COMPONENT_GROUP_NAME_ATTR = 'data-radio-group';
var COMPONENT_SELECTOR = '[' + COMPONENT_GROUP_NAME_ATTR + ']';
var GROUP_NAME_LABEL_ATTR = 'data-radio-group-label';
var ITEMS_SELECTOR = '[data-radio-group-items]';
var ITEM_SELECTOR = '[data-radio-group-item]';
var INPUT_SELECTOR = '[data-radio-group-input]';
var DEFAULT_SELECTOR = '[data-radio-group-default]';
var IS_EXPANDIBLE = '[data-is-expandible]';
var HAS_CHECKED_CLASS = 'has-checked';
var IS_CHECKED_CLASS = 'is-checked';
var DESELECT_EVENT = 'deselect-filter-item';
var NESTED_SELECTOR = '[data-nested-filter]';
var FILTER_ADD_GROUP_NAME_SELECTOR = '[data-radio-group-add-groupname-to-filter]';
var NUMBER_VISIBLE_FILTERS = 2; // amount of visible filters, in this case: Woonhuis + Appartment
var RADIO_GROUP_NAME_ATTR = 'data-radio-group-option-name';
// for not applying the filter in the applied filter list
var NOT_IN_APPLIED_FILTERS_SELECTOR = '[data-not-in-applied-filters]';

var RANGE_SELECTOR = '[data-filter-flyout-range]';
var RANGE_ITEM_SELECTOR = '[data-radio-group-range]';
var RANGE_TEMPLATE = '[data-range-template]';
var RANGE_UNIT_TYPE = 'data-range-unit-type';
var RANGE_SELECT_EVENT = 'rangeselected';
var RANGE_DESELECT_EVENT = 'rangedeselected';
var RANGE_INITIALIZED_EVENT = 'rangeinitialized';
var RANGE_RESET = 'radiogroupreset';
var FORM_SELECTOR = '[data-instant-search]';

var THOUSAND_SEPARATOR = 'data-thousand-separator';

function RadioGroup(element) {
    var component = this;

    component.groupName = null;
    component.bindToElements(element);

    component.groupSettings = {
        filterGroupId: component.$element.attr(COMPONENT_GROUP_NAME_ATTR),
        filterGroupName: component.$element.attr(GROUP_NAME_LABEL_ATTR).toLowerCase()
    };
    component.initialCheckState();

    component.bindEvents();

    (0, _jquery2.default)(window).on('sidebar-refresh', function () {
        if (component.groupName == null) return;
        var $myNewElement = (0, _jquery2.default)(COMPONENT_SELECTOR + '[' + COMPONENT_GROUP_NAME_ATTR + '=' + component.groupName + ']');
        component.bindToElements($myNewElement);
        component.initialCheckState();
        component.bindEvents();
    });
}

RadioGroup.prototype.bindToElements = function (element) {
    var component = this;

    component.$element = (0, _jquery2.default)(element);
    component.$item = component.$element.find(ITEM_SELECTOR);
    component.$items = component.$element.find(ITEMS_SELECTOR);
    component.$inputs = component.$element.find(INPUT_SELECTOR);
    component.isNestedFilter = component.$element.is(NESTED_SELECTOR);
    component.$rangeFlyout = component.$element.find(RANGE_SELECTOR);
    component.rangeTemplate = component.$element.find(RANGE_TEMPLATE);
    component.isExpandible = component.$element.is(IS_EXPANDIBLE);
    component.$rangeItem = component.$element.find(RANGE_ITEM_SELECTOR);
    component.separator = component.rangeTemplate.attr(THOUSAND_SEPARATOR);
    component.unitType = component.rangeTemplate.attr(RANGE_UNIT_TYPE);
    component.notInAppliedFilters = component.$element.is(NOT_IN_APPLIED_FILTERS_SELECTOR);
    // remember group name (first time)
    if (component.groupName == null) {
        component.groupName = component.$element.attr(COMPONENT_GROUP_NAME_ATTR);
    }
};

RadioGroup.prototype.bindEvents = function () {
    var component = this;

    component.$element.on('change', INPUT_SELECTOR, function () {
        var $input = (0, _jquery2.default)(this);
        component.toggleCheckedState($input);
    });
    component.$element.on('reset', function () {
        component.$items.find(RANGE_ITEM_SELECTOR).remove();
        component.$element.removeClass(HAS_CHECKED_CLASS);
        component.$item.removeClass(IS_CHECKED_CLASS);
    });
    component.$element.on('resetfilter', function () {
        component.resetCheckState();
    });
    component.$rangeFlyout.on(RANGE_SELECT_EVENT, function (event, data) {
        component.addActiveRangeLabel(data);
    });
    component.$rangeFlyout.on(RANGE_INITIALIZED_EVENT, function (event, data) {
        component.addActiveRangeLabel(data);
    });
};

RadioGroup.prototype.addActiveRangeLabel = function (data) {
    var component = this;
    var html = component.rangeTemplate.html();
    var appliedFilter = component.groupSettings;

    component.$items.find(RANGE_ITEM_SELECTOR).remove();

    if (!data.min && data.max) {
        data.min = '0';
    }

    var filterName = '';
    if (data.min && !data.max) {
        data.min = data.min + '+';
        html = html.replace('- ', '');
        filterName = Separator.format(data.min, component.separator);
    } else {
        filterName = Separator.format(data.min, component.separator) + ' - ' + Separator.format(data.max, component.separator);
    }

    html = html.replace('{min}', Separator.format(data.min, component.separator)).replace('{max}', Separator.format(data.max, component.separator));

    appliedFilter.labelText = filterName + ' ' + component.unitType;
    appliedFilter.filterName = appliedFilter.labelText.replace('(' + appliedFilter.filterGroupName + ')', '');
    component.applyOption(appliedFilter);

    component.$items.append(html);
    component.$element.addClass(HAS_CHECKED_CLASS);
    component.onRemoveActiveRange();
};

RadioGroup.prototype.onRemoveActiveRange = function () {
    var component = this;
    component.$rangeItem = component.$element.find(RANGE_ITEM_SELECTOR);
    component.$rangeItem.on('click', function () {
        component.removeCheckedState();
        component.$rangeItem.remove();
        component.$rangeFlyout.trigger(RANGE_DESELECT_EVENT);
        // Due to the lack of communication between components, we have to interfere the dom item directly
        (0, _jquery2.default)(FORM_SELECTOR).trigger('range_filter_removed');
    });
};

RadioGroup.prototype.initialCheckState = function () {
    var component = this;
    var $checkedInput = component.$inputs.filter(':checked');
    var checkedInputIndex = component.$inputs.index($checkedInput);
    component.onRemoveActiveRange();

    if (component.isExpandible) {
        component.$element.attr('data-expandible', '');
        component.expandible = new _expandible2.default(component.$element[0]);
    }

    if (component.isExpandible && checkedInputIndex > NUMBER_VISIBLE_FILTERS) {
        component.expandible.toggleExpand(true);
    }

    if ($checkedInput.length > 0) {
        component.toggleCheckedState($checkedInput.first());
    }
};

RadioGroup.prototype.toggleCheckedState = function ($input) {
    var component = this;
    var $item = $input.closest(ITEM_SELECTOR);
    var isDefault = $input.is(DEFAULT_SELECTOR);

    if (isDefault) {
        component.removeCheckedState();
    } else {
        component.addCheckedState($item, $input);
    }
};

RadioGroup.prototype.resetCheckState = function (radioGroupComponent) {
    var component = radioGroupComponent || this;
    var $checkedInput = component.$inputs.filter(':checked');
    // SRCH-3139: we need to trigger the default option so other filters are properly shown/hidden
    var isDefault = $checkedInput.is(DEFAULT_SELECTOR);
    var groupFilterIsSet = $checkedInput.val() === '' ? false : true;

    if (groupFilterIsSet && $checkedInput.length > 0) {
        if (isDefault) {
            $checkedInput.prop('checked', false);
            $checkedInput.trigger('change');
        } else {
            var $defaultInput = component.$inputs.filter(DEFAULT_SELECTOR);
            $defaultInput.prop('checked', true);
            $defaultInput.trigger('change');
        }
        component.$items.find(RANGE_ITEM_SELECTOR).remove();
        component.removeCheckedState();
        component.$element.trigger(RANGE_RESET);
    }
    if (component.$rangeItem.length > 0) {
        component.$rangeItem.remove();
        component.$rangeFlyout.trigger(RANGE_DESELECT_EVENT);
        component.removeCheckedState();
        component.$element.trigger(RANGE_RESET);
    }
};

RadioGroup.prototype.removeCheckedState = function () {
    var component = this;
    component.$element.removeClass(HAS_CHECKED_CLASS);
    component.$item.removeClass(IS_CHECKED_CLASS);
    _appliedFilters2.default.remove(component.groupSettings.filterGroupId);
    if (component.isNestedFilter) {
        component.$element.trigger(DESELECT_EVENT);
    }
};

RadioGroup.prototype.addCheckedState = function ($item, $input) {
    var component = this;
    var appliedFilter = component.groupSettings;

    component.$element.addClass(HAS_CHECKED_CLASS);
    component.$item.not($item).removeClass(IS_CHECKED_CLASS);
    $item.addClass(IS_CHECKED_CLASS);

    appliedFilter.filterName = $input.attr(RADIO_GROUP_NAME_ATTR);
    appliedFilter.labelText = appliedFilter.filterName;

    if ($input.is(FILTER_ADD_GROUP_NAME_SELECTOR)) {
        appliedFilter.labelText = appliedFilter.filterName + ' (' + appliedFilter.filterGroupName + ')';
    }
    component.applyOption(appliedFilter);
};

RadioGroup.prototype.applyOption = function (data) {
    var component = this;
    if (component.notInAppliedFilters) {
        return;
    }
    _appliedFilters2.default.add(data, function () {
        // reset radio group
        component.resetCheckState(component);
    });
};

(0, _jquery2.default)(COMPONENT_SELECTOR).each(function (index, element) {
    return new RadioGroup(element);
});

},{"../applied-filters/applied-filters":204,"../expandible/expandible":229,"../filter-flyout/filter-flyout":232,"../thousand-separator/thousand-separator":322,"jquery":"jquery"}],297:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = RadiusFilter;


var COMPONENT_SELECTOR = '[data-input-radius]';
var AUTOCOMPLETE_SELECTOR = '[data-autocomplete]';
var IS_DISABLED_CLASS = 'is-disabled';
var SEARCH_QUERY_SET_EVENT = 'searchQuerySet';
var RESET_FILTER_EVENT = 'reset';
var CHANGE_BUURT_EVENT = 'changeBuurt';
var CHANGE_RADIUS_EVENT = 'changeRadius';

function RadiusFilter(element) {
    var component = this;
    component.$element = (0, _jquery2.default)(element);
    component.$select = component.$element.find('select');
    component.$form = component.$element.closest('form');
    component.$autocomplete = component.$form.find(AUTOCOMPLETE_SELECTOR);

    component.$autocomplete.on(RESET_FILTER_EVENT, function () {
        component.$select[0].selectedIndex = 0;
    });

    component.$select.on('change', function () {
        var radiusValue = +component.$select.val();
        console.log(CHANGE_RADIUS_EVENT, { selectedRadius: radiusValue });
        (0, _jquery2.default)(document).trigger(CHANGE_RADIUS_EVENT, { selectedRadius: radiusValue });
    });

    (0, _jquery2.default)(document).on(CHANGE_BUURT_EVENT, function (event, data) {
        component.setDisabled(data.selectedFilterCount > 1);
    });

    component.$autocomplete.on(SEARCH_QUERY_SET_EVENT, function (event, data) {
        component.setDisabled(data.multiple);
    });
}

RadiusFilter.prototype.setDisabled = function (newState) {
    var component = this;

    if (newState) {
        component.$select.attr('disabled', true);
        component.$element.addClass(IS_DISABLED_CLASS);
        component.$select[0].selectedIndex = 0;
    } else {
        component.$select.removeAttr('disabled');
        component.$element.removeClass(IS_DISABLED_CLASS);
    }
};

(0, _jquery2.default)(COMPONENT_SELECTOR).each(function (index, element) {
    return new RadiusFilter(element);
});

},{"jquery":"jquery"}],298:[function(require,module,exports){
// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

// explicitly inject dependencies (alphabetically), only those needed

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

require('../input-number/input-number');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// what does this module expose?
exports.default = RangeFilterSelector;

// component configuration

var RANGE_FILTER_SELECTOR_PRESET = '[data-range-filter-selector-preset]';
var RANGE_FILTER_SELECTOR_CUSTOM = '[data-range-filter-selector-custom]';
var CLEAR_CUSTOM_RANGE_FILTER_SELECTOR = '[data-range-filter-selector-clear-custom-value]';

var RANGE_FILTER_SELECTOR_SELECT = '[data-range-filter-selector-preset-select]';
var PRESET_DEFAULT_NUMBER_SELECTOR = '[data-range-filter-selector-default-value]';

var RANGE_FILTER_SELECTOR_INPUT = '[data-input-number-field]';

var VALUE_CHANGED_TRIGGER = 'numberchanged';
var CUSTOM_VALUE = 'other';
var INACTIVE_CLASS = 'is-inactive';
var INPUT_FOCUS_DELAY = 125;
var INVALID_CLASS = 'is-invalid';

function RangeFilterSelector(element, $parent) {
    var component = this;
    component.$element = (0, _jquery2.default)(element);
    component.$parent = $parent;
    component.$preset = component.$element.find(RANGE_FILTER_SELECTOR_PRESET);
    component.$custom = component.$element.find(RANGE_FILTER_SELECTOR_CUSTOM);
    component.$select = component.$element.find(RANGE_FILTER_SELECTOR_SELECT);
    component.$input = component.$element.find(RANGE_FILTER_SELECTOR_INPUT);
    component.$clearCustomValue = component.$element.find(CLEAR_CUSTOM_RANGE_FILTER_SELECTOR);
    component.defaultValue = component.$select.find(PRESET_DEFAULT_NUMBER_SELECTOR).val();
    component.defaultIndex = component.$select.find(PRESET_DEFAULT_NUMBER_SELECTOR).index();
    component.selectedIndex = component.$select[0].selectedIndex;
    component.valueIsDefault = component.isDefault();
    component.time = null;

    component.bindEvents();
}

RangeFilterSelector.prototype.bindEvents = function () {
    var component = this;

    component.$input.on('blur', function () {
        return component.updateCustomValueInput();
    });
    component.$select.on('reset', function () {
        return component.setDefault();
    });
    component.$select.on('change', function (event) {
        return component.changedPresetValue(event);
    });
    component.$clearCustomValue.on('click', function () {
        return component.setDefault();
    });
};

/**
 * determine if component is set to default
 * @returns {boolean}
 */
RangeFilterSelector.prototype.isDefault = function () {
    var component = this;
    return component.defaultIndex === component.selectedIndex;
};

/**
 * set component to it's default; remove changed value and classes
 */
RangeFilterSelector.prototype.setDefault = function () {
    var component = this;
    component.removeValidation(); // default value can't be invalid
    component.setPresetSelect(component.defaultValue);
    component.$input.val('');
    component.$custom.addClass(INACTIVE_CLASS);
    component.$preset.removeClass(INACTIVE_CLASS);
    component.$parent.trigger(VALUE_CHANGED_TRIGGER);
};

/**
 * when the select is changed; we also changed the selectedIndex
 * @param {String} number
 */
RangeFilterSelector.prototype.setPresetSelect = function (number) {
    var component = this;
    component.$select.val(number);
    component.selectedIndex = component.$select[0].selectedIndex;
};

/**
 * get the number
 */
RangeFilterSelector.prototype.getValue = function () {
    var component = this;
    var number = component.$select.val() === CUSTOM_VALUE ? component.$input.val() : component.$select.val();

    if (number > 0) {
        return parseInt(number, 10);
    }
    return number;
};

/**
 * set a new value
 * @param {Integer} number
 */
RangeFilterSelector.prototype.setValue = function (number) {
    var component = this;
    component.setPresetSelect(CUSTOM_VALUE);
    component.$input.val(number);
    component.$input.blur(); // blur to set new mask
    component.showCustomValue();
    component.$parent.trigger(VALUE_CHANGED_TRIGGER);
};

/**
 * set the difference between numbers
 * @param {Integer} number
 */
RangeFilterSelector.prototype.setValueDifference = function (number) {
    var component = this;
    var $filtered = component.$select.find('option').filter(function () {
        return parseInt((0, _jquery2.default)(this).val(), 10) > number;
    });

    if ($filtered.length > 0) {
        component.setPresetSelect($filtered.first().val());
    } else {
        component.setPresetSelect(component.$select.find('option:last').val());
    }
    component.$parent.trigger(VALUE_CHANGED_TRIGGER);
};

/**
 * preset select is changed
 * @param {Object} event
 */
RangeFilterSelector.prototype.changedPresetValue = function (event) {
    var component = this;
    var $select = (0, _jquery2.default)(event.currentTarget);
    component.selectedIndex = $select[0].selectedIndex;
    if ($select.val() === CUSTOM_VALUE) {
        event.stopPropagation();
        // prob. Chrome fix
        $select.find('option[value="' + CUSTOM_VALUE + '"]').prop('selected', true);
        component.showCustomValue();
        return; // stop trigger
    } else {
        component.$parent.trigger(VALUE_CHANGED_TRIGGER);
    }
};

/**
 * hides the preset value and shows the custom value
 */
RangeFilterSelector.prototype.showCustomValue = function () {
    var component = this;
    component.$preset.addClass(INACTIVE_CLASS);
    component.$custom.removeClass(INACTIVE_CLASS);
    component.time = setTimeout(function () {
        // set focus on input field after a small delay
        component.$input.focus();
    }, INPUT_FOCUS_DELAY);
};

/**
 * input field is changed, let the form know
 */
RangeFilterSelector.prototype.updateCustomValueInput = function () {
    var component = this;
    if (component.$input.val() === '') {
        component.setDefault();
    } else {
        component.$parent.trigger(VALUE_CHANGED_TRIGGER);
    }
};

/**
 * the value is invalid
 */
RangeFilterSelector.prototype.setInvalid = function () {
    var component = this;
    component.$element.addClass(INVALID_CLASS);
};

/**
 * determine if the value is invalid
 */
RangeFilterSelector.prototype.isInvalid = function () {
    var component = this;
    return component.$element.hasClass(INVALID_CLASS);
};

/**
 * the value is not invalid
 */
RangeFilterSelector.prototype.removeValidation = function () {
    var component = this;
    if (component.isInvalid()) {
        component.$element.removeClass(INVALID_CLASS);
    }
};

},{"../input-number/input-number":244,"jquery":"jquery"}],299:[function(require,module,exports){
// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

// explicitly inject dependencies (alphabetically), only those needed

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

var _appliedFilters = require('../applied-filters/applied-filters');

var _appliedFilters2 = _interopRequireDefault(_appliedFilters);

var _rangeFilterSelector = require('../range-filter-selector/range-filter-selector');

var _rangeFilterSelector2 = _interopRequireDefault(_rangeFilterSelector);

var _thousandSeparator = require('../thousand-separator/thousand-separator');

var _thousandSeparator2 = _interopRequireDefault(_thousandSeparator);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// what does this module expose?
exports.default = RangeFilter;

// component configuration

var COMPONENT_ATTR = 'range-filter';
var COMPONENT_SELECTOR = '[data-' + COMPONENT_ATTR + ']';
var RANGE_FILTER_SELECTOR_COMPONENT = '[data-range-filter-selector]';
var RANGE_FILTER_MIN_SELECTOR = '[data-range-filter-min] ' + RANGE_FILTER_SELECTOR_COMPONENT;
var RANGE_FILTER_MAX_SELECTOR = '[data-range-filter-max] ' + RANGE_FILTER_SELECTOR_COMPONENT;
var RANGE_FILTER_NAME = 'data-filter-name';
var THOUSAND_SEPARATOR = 'data-thousand-separator';

var DISPLAY_STRING = 'data-display-string';
var TOKEN = '{value}';

var EXTERNAL_TRIGGER = 'change.external';
var RANGE_CHANGED_TRIGGER = 'rangechanged';
var VALUE_CHANGED_TRIGGER = 'numberchanged';
var IGNORE_FILTER = 'ignore_filter';

function RangeFilter(element) {
    var component = this;
    component.$element = (0, _jquery2.default)(element);
    component.$form = (0, _jquery2.default)('form');
    component.$minContainer = component.$element.find(RANGE_FILTER_MIN_SELECTOR);
    component.$maxContainer = component.$element.find(RANGE_FILTER_MAX_SELECTOR);
    component.min = new _rangeFilterSelector2.default(component.$minContainer, component.$element);
    component.max = new _rangeFilterSelector2.default(component.$maxContainer, component.$element);
    component.filterGroupId = component.$element.attr(RANGE_FILTER_NAME);
    component.filterGroupName = component.$element.attr(RANGE_FILTER_NAME);
    component.separator = component.$element.attr(THOUSAND_SEPARATOR);
    component.displayString = component.$element.attr(DISPLAY_STRING) || '&euro; ' + TOKEN;
    component.token = TOKEN;
    component.currentRange = null;
    component.isInitiated = false;

    component.bindEvents();

    component.adjustFilter();
}

RangeFilter.prototype.bindEvents = function () {
    var component = this;

    component.$element.on('resetfilter', function () {
        component.reset();
    });

    component.$element.on(VALUE_CHANGED_TRIGGER, function () {
        component.adjustFilter();
    });

    component.isInitiated = true;
};

/**
 * add a token to the given number
 * @param {String} number
 * @param {Bool} numberIsFormatted
 * @returns {string}
 */
RangeFilter.prototype.addToken = function (number, numberIsFormatted) {
    var component = this;

    if (numberIsFormatted) {
        return component.displayString.replace(component.token, number);
    } else {
        var formattedValue = _thousandSeparator2.default.format(number, component.separator);
        return component.displayString.replace(component.token, formattedValue);
    }
};

/**
 * add the range to the applied filters
 * @param {amount} range
 */
RangeFilter.prototype.addFilter = function (range) {
    var component = this;
    var rangeAttributes = {
        filterGroupId: component.filterGroupId,
        filterGroupName: component.filterGroupName,
        filterName: range,
        labelText: range
    };

    _appliedFilters2.default.add(rangeAttributes, function () {
        component.reset(component);
    }, true);
};

/**
 * Adjust the filter depending if value is default or not
 */
RangeFilter.prototype.adjustFilter = function () {
    var component = this;

    if (!component.isValidRange()) {
        return;
    }
    var range = 0;
    var minimumNumber = component.min.getValue();
    var maximumNumber = component.max.getValue();
    var formattedMinValue = _thousandSeparator2.default.format(minimumNumber || '', component.separator);

    if (maximumNumber === IGNORE_FILTER && minimumNumber > 0) {
        // only minimum number is set
        range = component.addToken(formattedMinValue + '+', true);
    } else if (maximumNumber !== IGNORE_FILTER && maximumNumber > 0) {
        // maximum number is set
        var formattedMaxValue = _thousandSeparator2.default.format(maximumNumber, component.separator);
        range = component.addToken(formattedMinValue, true) + ' - ' + component.addToken(formattedMaxValue, true);
    }

    if (component.isInitiated && component.currentRange !== range) {
        // make sure there is a change
        component.$element.trigger(RANGE_CHANGED_TRIGGER);
    }

    // only apply filter when range is larger then 0
    if (range !== 0 && range !== '0') {
        component.addFilter(range);
    } else {
        _appliedFilters2.default.remove(component.filterGroupId);
    }
    component.currentRange = range;
};

/**
 * set a range difference between minimum and maximum number
 */
RangeFilter.prototype.setRangeDifference = function () {
    var component = this;
    var minimumNumber = component.min.getValue();
    component.max.setValueDifference(minimumNumber);
};

/**
 * validate current range and change value if needed or show validation
 */
RangeFilter.prototype.isValidRange = function () {
    var component = this;
    var minimumNumber = component.min.getValue();
    var maximumNumber = component.max.getValue();
    if (minimumNumber > maximumNumber) {
        component.max.setInvalid();
        component.bindFormChangeEvent();
        return false;
    }
    component.max.removeValidation();
    component.unbindFormEvent();
    return true;
};

/**
 * reset complete component to default
 * @param {component} RangeFilterComponent; the range-filter component it self
 */
RangeFilter.prototype.reset = function (RangeFilterComponent) {
    var component = RangeFilterComponent || this;
    component.min.setDefault();
    component.max.setDefault();
};

RangeFilter.prototype.unbindFormEvent = function () {
    var component = this;
    component.$form.off(EXTERNAL_TRIGGER);
};

/**
 * only trigger this request when change triggered outside the custom value
 */
RangeFilter.prototype.bindFormChangeEvent = function () {
    var component = this;

    component.$form.on(EXTERNAL_TRIGGER, function (event) {
        if ((0, _jquery2.default)(event.target).closest(COMPONENT_SELECTOR).length === 1) {
            return;
        }
        component.formChanged();
    });
};

/**
 * there is a 'change' within the form
 */
RangeFilter.prototype.formChanged = function () {
    var component = this;
    var minimumNumber = component.min.getValue();
    var maximumNumber = component.max.getValue();

    if (minimumNumber > maximumNumber && component.max.isInvalid()) {
        component.setRangeDifference();
    }
};

/**
 * determine if current element is visible
 * @returns {boolean}
 */
RangeFilter.prototype.isVisible = function () {
    var component = this;
    return component.$element.is(':visible');
};

// turn all elements with the default selector into components
(0, _jquery2.default)(COMPONENT_SELECTOR).each(function (index, element) {
    return new RangeFilter(element);
});

},{"../applied-filters/applied-filters":204,"../range-filter-selector/range-filter-selector":298,"../thousand-separator/thousand-separator":322,"jquery":"jquery"}],300:[function(require,module,exports){
'use strict';

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

var _classObservable = require('../class-observable/class-observable');

var _classObservable2 = _interopRequireDefault(_classObservable);

var _serviceController = require('../service-controller/service-controller');

var _serviceController2 = _interopRequireDefault(_serviceController);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

module.exports = Rating;

// component configuration
var COMPONENT_ATTR = 'data-rating';
var COMPONENT_SELECTOR = '[data-rating]';
var HOVER_CLASS = 'is-hovered';
var RATED_CLASS = 'is-rated';

//This is a static instance of observable, so ratings can observe themselves
var ratingService = function () {
    return new _classObservable2.default();
}();

function Rating(element) {
    var component = this;
    component.$element = (0, _jquery2.default)(element);
    component.ratingId = component.$element.attr(COMPONENT_ATTR) || false;
    component.$inputs = component.$element.find('input');
    component.selectedValue = 0;

    component.run = function () {
        setWatchers();
        setRatingFromRadioButton();
    };

    //region Observer facade
    component.observers = new _classObservable2.default();
    component.TOPICS = {
        CHANGE: 0,
        HOVER_IN: 1,
        HOVER_OUT: 2
    };

    component.onChange = function (observer) {
        component.observers.listen(component.TOPICS.CHANGE, observer);
    };
    component.onHoverIn = function (observer) {
        component.observers.listen(component.TOPICS.HOVER_IN, observer);
    };
    component.onHoverOut = function (observer) {
        component.observers.listen(component.TOPICS.HOVER_OUT, observer);
    };

    function notify(topic, newValue) {
        ratingService.notify(component.ratingId, {
            topic: topic,
            value: newValue
        });
        component.observers.notify(topic, newValue);
    }

    function notifyChange(newValue) {
        notify(component.TOPICS.CHANGE, newValue);
    }

    function notifyHoverIn(newValue) {
        notify(component.TOPICS.HOVER_IN, newValue);
    }

    function notifyHoverOut(newValue) {
        notify(component.TOPICS.HOVER_OUT, newValue);
    }

    //endregion

    //region Handlers
    component.clickHandler = function (value, skipSync) {
        var newValue = parseInt(value, 10);
        //Update checked attribute in the inputs
        (0, _jquery2.default)(component.$inputs[component.selectedValue]).prop('checked', false).removeAttr('checked');
        (0, _jquery2.default)(component.$inputs[newValue]).prop('checked', true).attr('checked', 'checked');
        // Update the rated-class. Add rated class all until the selected value, and remove the next ones.
        component.$inputs.slice(0, newValue + 1).addClass(RATED_CLASS);
        component.$inputs.slice(newValue + 1).removeClass(RATED_CLASS);
        //Cache value
        component.selectedValue = newValue;
        //Triggers and notifies
        if (typeof skipSync === 'undefined' || !skipSync) {
            notifyChange(newValue);
            if (component.ratingId) {
                component.$element.trigger('selectrating', { score: newValue });
            }
        }
    };

    component.hoverInHandler = function (value, skipSync) {
        var val = parseInt(value, 10);
        component.$inputs.slice(0, val + 1).addClass(HOVER_CLASS);
        component.hoverValue = val;
        if (typeof skipSync === 'undefined' || !skipSync) {
            notifyHoverIn(val);
        }
    };

    component.hoverOutHandler = function (value, skipSync) {
        var val = parseInt(value, 10);
        component.$inputs.removeClass(HOVER_CLASS);
        component.hoverValue = 0;
        if (typeof skipSync === 'undefined' || !skipSync) {
            notifyHoverOut(val);
        }
    };

    component.messageHandler = function (message) {
        var value = message.value;
        switch (message.topic) {
            case component.TOPICS.CHANGE:
                component.clickHandler(value, true);
                break;
            case component.TOPICS.HOVER_IN:
                component.hoverInHandler(value, true);
                break;
            case component.TOPICS.HOVER_OUT:
                component.hoverOutHandler(value, true);
                break;
        }
    };
    //endregion

    function setWatchers() {
        //Observe ratings with same ID and link
        if (component.ratingId) {
            ratingService.listen(component.ratingId, component.messageHandler);
        }

        // store the label's index in memory and set watchers
        component.$inputs.each(function (index, elem) {
            var $elem = (0, _jquery2.default)(elem);
            var $label = (0, _jquery2.default)('label[for=' + $elem.attr('id') + ']');
            $label.data('index', index);
            $label.on('mouseenter', function () {
                return component.hoverInHandler(index);
            });
            $label.on('mouseleave', function () {
                return component.hoverOutHandler(index);
            });
            $elem.on('change', function () {
                return component.clickHandler(index);
            });
        });
    }

    function setRatingFromRadioButton() {
        component.$inputs.each(function (idx, input) {
            var $input = (0, _jquery2.default)(input);
            if ($input.is(':checked')) {
                component.clickHandler($input.val());
            }
        });
    }

    /**
     * Alternative name to click Handler. For backwards compatibility.
     * @type {(function())|*} see clickHandler
     */
    component.selectRating = component.clickHandler;
    component.run();
}

Rating.getSelector = function () {
    return COMPONENT_SELECTOR;
};
Rating.initialize = function () {
    return _serviceController2.default.getAllInstances(Rating);
};
Rating.initialize();

},{"../class-observable/class-observable":218,"../service-controller/service-controller":317,"jquery":"jquery"}],301:[function(require,module,exports){
'use strict';

require('../carousel/carousel');

},{"../carousel/carousel":217}],302:[function(require,module,exports){
// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

// explicitly inject dependencies (alphabetically), only those needed

var _expandible = require('../expandible/expandible');

var _expandible2 = _interopRequireDefault(_expandible);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var $ = require('jquery');

var LoginDialog = require('../login-dialog/login-dialog');
var AppSpinner = require('../app-spinner/app-spinner');

// what does this module expose?
module.exports = UserSaveSearch;

// component configuration
var COMPONENT_ATTR = 'data-save-search';
var COMPONENT_SELECTOR = '[' + COMPONENT_ATTR + ']';
var HANDLE_ATTR = 'data-save-search-handle';
var HANDLE_SELECTOR = '[' + HANDLE_ATTR + ']';
var SUBMIT_BUTTON_SELECTOR = '[data-save-search-submit-button]';
var SAVE_SEARCH_DIALOG_SELECTOR = '[data-dialog="save-search-form"]';
var DIALOG_SUBMIT_BUTTON_SELECTOR = SAVE_SEARCH_DIALOG_SELECTOR + ' ' + SUBMIT_BUTTON_SELECTOR;
var FLYOUT_FORM_SELECTOR = '[data-save-search="flyout"]';
var EXPANDIBLE_SELECTOR = '[data-expandible]';
var LOGIN_DIALOG_SELECTOR = '[data-dialog="user-form"]';
var CONTAINER_ATTR = 'data-save-search-container';
var SPINNER_SELECTOR = '[data-save-search-spinner]';
var LOADED_CLASS = 'is-loaded';
var EXPANDED_CLASS = 'is-expanded';
var OPEN_CLASS = 'is-open';
var FLYOUT_CLOSE = '.search-close-flyout';
var HISTORISCH_RADIO_GROUP_SELECTOR = '[data-radio-group="IndHistorisch"] [data-radio-group-input]';
var SAVE_SEARCH_TOGGLE_SELECTOR = '.search-sidebar .search-save';
var HISTORICFILTER_SELECTOR = '[name="filter_IndHistorisch"][value="True"]';
var RESULTLIST_SELECTOR = '.search-main';
var HISTORIC_CLASS = 'historic';

var $body = $('body');

function UserSaveSearch(element) {
    var component = this;
    component.element = element;
    component.$element = $(element);
    component.saveSearchId = element.getAttribute(COMPONENT_ATTR) || false;
    component.$saveSearchHandle = $body.find(HANDLE_SELECTOR);
    component.$container = $body.find('[' + CONTAINER_ATTR + '="' + component.saveSearchId + '"]');

    $body.on('click', '[' + HANDLE_ATTR + '="' + component.saveSearchId + '"]', function (event) {
        event.preventDefault();

        if (component.saveSearchId == 'dialog') {
            var dialogElement = document.querySelector('[data-dialog="save-search-form"]');
            dialogElement.dialog.onFormSuccess = function () {
                // on successful login a lot of things need to update on the page,
                // so it's easier just to reload the entire page entirely.
            };

            component.dialog = dialogElement.dialog;
            component.dialog.open();
            component.dialog.getContent($(this).attr('href'));
        } else {
            var isVisible = $(this).hasClass(EXPANDED_CLASS) || $(this).hasClass(OPEN_CLASS);

            component.$container.removeClass(LOADED_CLASS);
            component.$element.html('');

            if (isVisible) {
                component.getForm(this);
            }
        }
    });

    $(document).on('resultsUpdating', function () {
        if ($(HISTORICFILTER_SELECTOR).is(':checked')) {
            $(RESULTLIST_SELECTOR).addClass(HISTORIC_CLASS);
        } else {
            $(RESULTLIST_SELECTOR).removeClass(HISTORIC_CLASS);
        }
    });

    component.$element.on('click', SUBMIT_BUTTON_SELECTOR, function (event) {
        if (component.saveSearchId == 'flyout') {
            $(FLYOUT_CLOSE).click();
        }
        component.submitForm(event);
    });

    if (component.saveSearchId == 'dialog') {
        $body.on('click', DIALOG_SUBMIT_BUTTON_SELECTOR, function (event) {
            component.submitForm(event);
        });
    }
    $body.on('change', HISTORISCH_RADIO_GROUP_SELECTOR, function (event) {
        var isDefault = $(event.currentTarget).is('[data-radio-group-default]');
        if (!isDefault) {
            $(SAVE_SEARCH_TOGGLE_SELECTOR).addClass('is-hidden');
        } else {
            $(SAVE_SEARCH_TOGGLE_SELECTOR).removeClass('is-hidden');
        }
    });
}

UserSaveSearch.prototype.submitForm = function (event) {
    var component = this;
    event.preventDefault();
    component.isUserLoggedIn(function onSuccessfulLogin(loginPerformed) {
        if (loginPerformed) {
            $('.search-map-content__filter-button').click();
        }
        component.doRequest(component.$saveSearchHandle.attr('href'));
    });
};

UserSaveSearch.prototype.getForm = function (element) {
    var component = this;
    var url = element.href;
    component.spinner = new AppSpinner(SPINNER_SELECTOR);
    component.spinner.show();

    return $.ajax({
        url: url,
        data: {
            currentUrl: window.location.pathname
        },
        success: function success(response) {
            if (response.Result === 'OK') {
                component.$element.append(response.Html);
                //extra expandible for mobile - toon oude opdracht
                var extraExpandibleElement = component.$element.find(EXPANDIBLE_SELECTOR);
                var hasExtraExpandibleElement = extraExpandibleElement.length;
                if (hasExtraExpandibleElement) {
                    component.extraExpandible = new _expandible2.default(extraExpandibleElement[0]);
                }
            }
        },
        error: function error(response) {
            console.error('Error trying to open save search form', url, response);
        },
        complete: function complete() {
            component.spinner.hide();
            component.$container.addClass(LOADED_CLASS);
        }
    });
};

UserSaveSearch.prototype.isUserLoggedIn = function (onLoggedIn) {
    var component = this;
    var dialogElement = document.querySelector(LOGIN_DIALOG_SELECTOR);

    var url = dialogElement.dialog.isUserLoggedInUrl;

    return $.ajax({
        url: url,
        success: function success(response) {
            if (response.LoggedIn === true) {
                onLoggedIn(false);
            } else {
                component.userLoginStatus = new LoginDialog(response.LoginUrl, function onSuccessfulLogin() {
                    onLoggedIn(true);
                });
            }
        },
        error: function error(response) {
            console.error('Error calling', url, response);
        }
    });
};

UserSaveSearch.prototype.doRequest = function (url) {
    var component = this;
    var data = component.$element.find(':input').serialize();
    var isKaartPage = component.$element.parents('.search-save.kaart-page').length > 0;
    // rewrite requests from /mijn/savedsearch/kaart/koop/ to /mijn/savedsearch/koop/
    url = url.replace('/kaart', '');
    if (component.saveSearchId == 'dialog') {
        data = $(SAVE_SEARCH_DIALOG_SELECTOR).find(':input').serialize();
    } else if (component.saveSearchId == 'flyout') {
        data = $(FLYOUT_FORM_SELECTOR).find(':input').serialize();
    }

    return $.ajax({
        method: 'POST',
        url: url,
        data: data,
        success: function onSuccess(response) {
            if (response.Result !== 'OK') {
                console.error('Error trying to save search', response);
            } else if (component.saveSearchId == 'dialog') {
                $('.current-query-kaart').html(response.Html);
                $('.overwrite-alert').remove();
                $('.save-search-actions *:not(.map-dialog-visible)').remove();
                $('.notification .notification-buttons').remove();
                $('.save-search-actions .map-dialog-visible').show();
                $('.save-search-actions .map-dialog-visible').click(function () {
                    var $this = $(this);
                    var $dlgContent = $this.parents('.dialog-content');
                    $dlgContent.find('[data-dialog-close]').click();
                });
            } else if (component.saveSearchId == 'flyout' && isKaartPage) {
                $('.search-sidebar .filter-reset').before(response.Html);
                $('.notification .notification-buttons').remove();
                setTimeout(function () {
                    $('.search-sidebar .notification').remove();
                }, 3000);
            }
        },
        error: function onError(response) {
            console.error('Error trying to save search', response);
        },
        complete: function complete() {
            if (component.saveSearchId == 'flyout' && !isKaartPage || component.saveSearchId == 'expandible') {
                window.location.reload();
            }
        }
    });
};

// turn all elements with the default selector into components
$(COMPONENT_SELECTOR).each(function (index, element) {
    return new UserSaveSearch(element);
});

},{"../app-spinner/app-spinner":201,"../expandible/expandible":229,"../login-dialog/login-dialog":249,"jquery":"jquery"}],303:[function(require,module,exports){
'use strict';

// explicitly inject dependencies (alphabetically), only those needed

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

require('../../components/partial-page-update/partial-page-update');

require('../../components/radius-filter/radius-filter');

require('../../components/custom-select-box/custom-select-box');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// what does this module expose?
exports.default = SearchBlock;

// component configuration

var COMPONENT_SELECTOR = '[data-search-block]';
var AUTOCOMPLETE_SELECTOR = '[data-autocomplete]';

var FORM_SELECTOR = '[data-search-block-form]';
var SOORT_AANBOD_SELECTOR = '[data-search-block-soort-aanbod-selector]';
var LOCATION_FILTERS_SELECTOR = '[data-search-block-location-filters]';
var HORECA_REGIONS_SELECTOR = '[data-search-block-horeca-regions]';
var HORECA_REGIONS_SELECT = '[data-search-block-horeca-regions-handle]';
var SOORT_AANBOD_HORECA = 'horeca';

var SEARCH_QUERY_UPDATED_EVENT = 'searchqueryupdated';
var IS_HIDDEN_CLASS = 'is-hidden';

function SearchBlock(element) {
    var component = this;
    component.$element = (0, _jquery2.default)(element);
    component.$form = component.$element.find(FORM_SELECTOR);
    component.$autocomplete = component.$element.find(AUTOCOMPLETE_SELECTOR);
    component.$soortAanbodSelect = component.$form.find(SOORT_AANBOD_SELECTOR);

    component.bindEvents();
    component.initializeSoortAanbodHandle();
    component.toggleHorecaRegions();
}

SearchBlock.prototype.bindEvents = function () {
    var component = this;

    component.$autocomplete.on(SEARCH_QUERY_UPDATED_EVENT, function (e, data) {
        if (data === undefined || !data.isOpen) {
            component.$form.submit();
        }
    });
};

SearchBlock.prototype.initializeSoortAanbodHandle = function () {
    var component = this;

    if (component.$soortAanbodSelect.length > 0) {
        component.$locationFilters = component.$element.find(LOCATION_FILTERS_SELECTOR);
        component.$horecaRegions = component.$element.find(HORECA_REGIONS_SELECTOR);
        component.$horecaRegionsSelect = component.$element.find(HORECA_REGIONS_SELECT);

        component.$soortAanbodSelect.on('change', function () {
            return component.toggleHorecaRegions();
        });
    }
};

/**
 * Show or hide horeca select
 */
SearchBlock.prototype.toggleHorecaRegions = function () {
    var component = this;
    var soortAanbod = component.$soortAanbodSelect.val();

    if (!soortAanbod) {
        return;
    }

    // soortAanbod does not necessarily represent SoortAanbod.Horeca; it may also be beleggingtype horeca with SoortAanbod.Belegging.
    // Both have the same underlying value (horeca) and both need this same show/hide functionality.
    if (soortAanbod.toLowerCase() === SOORT_AANBOD_HORECA) {
        component.showHorecaRegions();
    } else {
        component.hideHorecaRegions();
    }
};

/**
 * show the horeca regions; hide the auto complete
 */
SearchBlock.prototype.showHorecaRegions = function () {
    var component = this;
    component.$locationFilters.addClass(IS_HIDDEN_CLASS);
    component.$horecaRegions.removeClass(IS_HIDDEN_CLASS);
    // clear the location in the auto complete and radius
    component.$autocomplete.trigger('reset');
};

/**
 * show the auto complete; hide the horeca regions
 */
SearchBlock.prototype.hideHorecaRegions = function () {
    var component = this;
    component.$horecaRegions.addClass(IS_HIDDEN_CLASS);
    component.$locationFilters.removeClass(IS_HIDDEN_CLASS);
    // set the horeca regions to the default (empty) value
    component.$horecaRegionsSelect.val('');
};

(0, _jquery2.default)(COMPONENT_SELECTOR).each(function (index, element) {
    return new SearchBlock(element);
});

},{"../../components/custom-select-box/custom-select-box":226,"../../components/partial-page-update/partial-page-update":294,"../../components/radius-filter/radius-filter":297,"jquery":"jquery"}],304:[function(require,module,exports){
'use strict';

require('../search-results-loading/search-results-loading');
require('../search-scroll-notify/search-scroll-notify');

},{"../search-results-loading/search-results-loading":313,"../search-scroll-notify/search-scroll-notify":315}],305:[function(require,module,exports){
// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

// explicitly inject dependencies (alphabetically), only those needed

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// what does this module expose?
exports.default = SearchElementInViewport;


var COMPONENT_POSITION_TOP = 0.4; // 40% height of viewport

function SearchElementInViewport(element, defaultTopPosition) {
    var component = this;
    component.element = element;
    component.$element = (0, _jquery2.default)(element);
    component.defaultTop = defaultTopPosition;
}

/**
 * Update vertical position to make component visible in the viewport
 */
SearchElementInViewport.prototype.updateVerticalPosition = function () {
    var component = this;
    component.$element.removeAttr('style');

    if (isScrollPositionAtTopOfPage()) {
        // place at top
        component.$element.css({ 'margin-top': component.defaultTop });
    } else {
        // center in viewport
        // offsets are relative to the current viewport
        var currentOffsetTop = component.element.getBoundingClientRect().top;
        var newOffsetTop = window.innerHeight * COMPONENT_POSITION_TOP;
        var marginTop = newOffsetTop - currentOffsetTop;

        component.$element.css({ 'margin-top': marginTop + 'px' });
    }
};

/**
 * Check if scroll position of document is at the top
 */
function isScrollPositionAtTopOfPage() {
    return document.documentElement.getBoundingClientRect().top === 0;
}

},{"jquery":"jquery"}],306:[function(require,module,exports){
'use strict';

// explicitly inject dependencies (alphabetically), only those needed

require('../autocomplete/autocomplete');
require('../radius-filter/radius-filter');

},{"../autocomplete/autocomplete":216,"../radius-filter/radius-filter":297}],307:[function(require,module,exports){
// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

// explicitly inject dependencies (alphabetically), only those needed

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = SearchMapContent;

// component configuration

var COMPONENT_SELECTOR = '[data-search-map-content]';
var MAP_SELECTOR = '[data-map]';
var URL_ATTR = 'data-search-map-content-url';
var AUTOCOMPLETE_SELECTOR = '[data-autocomplete]';
var SUBMIT_LOCATION_SELECTOR = '[data-search-map-content-submit-location]';
var GEO_INDENTIFIER_SELECTOR = '[data-instant-search-output="zoekboxHiddenValue"]';
var FORM_SELECTOR = '[data-instant-map-search]';
var GEO_SELECTOR = '#filter_Geo';
var NEW_GEO_SELECTOR = '#filter_NewGeo';

var SEARCH_QUERY_UPDATED_EVENT = 'searchqueryupdated';

function SearchMapContent(element) {
    var component = this;

    component.bindToElements(element);
    component.bindEvents();

    (0, _jquery2.default)(window).on('sidebar-refresh', function () {
        var $myNewElement = (0, _jquery2.default)(COMPONENT_SELECTOR);
        if ($myNewElement.length == 1) {
            component.bindToElements($myNewElement);
            component.bindEvents();
        }
    });
}

SearchMapContent.prototype.bindToElements = function (element) {
    var component = this;

    component.$element = (0, _jquery2.default)(element);
    component.$map = (0, _jquery2.default)(MAP_SELECTOR);
    component.url = component.$element.attr(URL_ATTR);
    component.$autocomplete = component.$element.find(AUTOCOMPLETE_SELECTOR);
    component.$submitButton = component.$element.find(SUBMIT_LOCATION_SELECTOR);
    component.$geoIdentifier = component.$element.find(GEO_INDENTIFIER_SELECTOR);
    component.$geo = (0, _jquery2.default)(FORM_SELECTOR).find(GEO_SELECTOR);
    component.$newGeo = (0, _jquery2.default)(FORM_SELECTOR).find(NEW_GEO_SELECTOR);
};

SearchMapContent.prototype.bindEvents = function () {
    var component = this;

    component.$autocomplete.on(SEARCH_QUERY_UPDATED_EVENT, function () {
        return component.submitLocation();
    });
    component.$submitButton.on('click', function () {
        return component.submitLocation();
    });

    (0, _jquery2.default)(document).on('resultsUpdated', function () {
        return component.$newGeo.val('');
    });
};

SearchMapContent.prototype.submitLocation = function () {
    var component = this;
    var locationId = component.$geoIdentifier.val();

    if (locationId !== '' && locationId !== '0') {
        component.changeCenterMap(locationId);
    }
};

SearchMapContent.prototype.changeCenterMap = function (locationId) {
    var component = this;

    component.$geo.val(locationId);
    component.$newGeo.val(locationId);
    component.$newGeo.trigger('change');
};

// turn all elements with the default selector into components
(0, _jquery2.default)(COMPONENT_SELECTOR).each(function (index, element) {
    return new SearchMapContent(element);
});

},{"jquery":"jquery"}],308:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

var _ajax = require('../ajax/ajax');

var _ajax2 = _interopRequireDefault(_ajax);

var _serviceController = require('../service-controller/service-controller');

var _serviceController2 = _interopRequireDefault(_serviceController);

var _userSaveObject = require('../user-save-object/user-save-object');

var _userSaveObject2 = _interopRequireDefault(_userSaveObject);

var _mapTileCache = require('../map/map-tile-cache');

var _mapTileCache2 = _interopRequireDefault(_mapTileCache);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

require('funda-slick-carousel');

// what does this module expose?
exports.default = SearchMapInfoWindow;

// component configuration

var OBJECT_LIST_SELECTOR = '[data-search-map-infowindow-list]';
var OBJECT_INFO_URL_ATTR = 'data-search-map-infowindow-object-url';
var MAP_SELECTOR = '[data-map]';
var TIMEOUT_SEARCH_REQUEST = 30000; // 30 sec

var POSITION_OBJECT_SELECTOR = '[data-search-map-infowindow-position]';
var POSITION_SEPARATOR_ATTR = 'data-search-map-infowindow-separator';
var NAVIGATION_SELECTOR = '[data-search-map-infowindow-navigation]';
var PREVIOUS_SELECTOR = '[data-search-map-infowindow-previous]';
var NEXT_SELECTOR = '[data-search-map-infowindow-next]';

var IS_VISIBLE = 'is-visible';
var IS_UPDATING = 'is-updating';
var MEASURE = 'measure';
var SHOW_OBJECT_INFO_EVENT = 'show_object_info';
var HIDE_OBJECT_INFO_EVENT = 'hide_object_info';

var SLICK_SLIDE_SPEED = 250;
var SLICK_SWIPE_SENSITIVITY = 12; // 1/12th of the image width

var EVENT_NAMESPACE = '.keyupBinding';
var SLIDE_RESET = 0;
var KEY_CODE = {
    ESCAPE: 27,
    LEFT: 37,
    RIGHT: 39
};

function SearchMapInfoWindow(element, parent) {
    var component = this;
    component.$element = (0, _jquery2.default)(element);
    component.$map = (0, _jquery2.default)(MAP_SELECTOR);
    component.objects = [];
    component.objectsAreVisible = false;
    component.objectUrl = component.$element.attr(OBJECT_INFO_URL_ATTR);
    component.$objectList = component.$element.find(OBJECT_LIST_SELECTOR);
    //navigation buttons
    component.$navigation = component.$element.find(NAVIGATION_SELECTOR);
    component.$previousButton = component.$navigation.find(PREVIOUS_SELECTOR);
    component.$nextButton = component.$navigation.find(NEXT_SELECTOR);

    component.slideshowIsInitialized = false;
    component.currentSlide = 0;
    component.$positionObject = component.$navigation.find(POSITION_OBJECT_SELECTOR);
    component.positionSeparator = component.$positionObject.attr(POSITION_SEPARATOR_ATTR);
    // caching
    component.mapTileCache = _mapTileCache2.default;
    component.parent = parent;
    component.skipUpdate = false;
    component.selectedObjectScreenPos = null;

    component.bindEvents();
}

SearchMapInfoWindow.prototype.bindEvents = function () {
    var component = this;

    component.$map.on(SHOW_OBJECT_INFO_EVENT, function (event, eventArgs) {
        return component.processEvent(eventArgs);
    });
    component.$map.on(HIDE_OBJECT_INFO_EVENT, function () {
        return component.hide();
    });
    component.$objectList.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
        return component.updatePosition(nextSlide);
    });
};

SearchMapInfoWindow.prototype.processEvent = function (eventArgs) {
    var component = this;

    var toBeLoadedObjectIds = null;
    component.selectedObjectScreenPos = null;
    // default selected slide
    component.currentSlide = SLIDE_RESET;
    if (eventArgs.initial) {
        var storedId = _mapTileCache2.default.getMarkerIdByPartial(eventArgs.objectId);
        toBeLoadedObjectIds = storedId.split('-');
        if (toBeLoadedObjectIds.length > 1) {
            component.currentSlide = toBeLoadedObjectIds.indexOf(eventArgs.objectId);
        }
        component.skipUpdate = true;
    } else {
        toBeLoadedObjectIds = eventArgs.objectId.split('-');
    }
    // is info-window going to obscure the marker?
    var selectedObjectId = _mapTileCache2.default.getMarkerByPartial(toBeLoadedObjectIds[0]);
    if (selectedObjectId) {
        component.selectedObjectScreenPos = component.getSelectedScreenCoordinates(selectedObjectId);
    }
    component.loadObjectInfo(toBeLoadedObjectIds);
};

SearchMapInfoWindow.prototype.getSelectedScreenCoordinates = function (selectedObjectId) {
    var google = window.google;

    var topRight = this.parent.map.getProjection().fromLatLngToPoint(this.parent.map.getBounds().getNorthEast());
    var bottomLeft = this.parent.map.getProjection().fromLatLngToPoint(this.parent.map.getBounds().getSouthWest());
    var latLng = new google.maps.LatLng(selectedObjectId.lat, selectedObjectId.lng);
    var scale = Math.pow(2, this.parent.map.getZoom());
    var latLngPoint = this.parent.map.getProjection().fromLatLngToPoint(latLng);
    var point2 = new google.maps.Point((latLngPoint.x - bottomLeft.x) * scale, (latLngPoint.y - topRight.y) * scale);
    // add offset to map
    var mapPos = this.parent.$element.offset();
    point2.x += mapPos.left;
    point2.y += mapPos.top;
    // modify for marker image
    var s = { w: 22, h: 28 };
    if (this.parent.mapMarkerManager.pixelDistance == 14) {
        point2.x -= 11;
        point2.y -= 28;
    } else {
        point2.x -= 9;
        point2.y -= 22;
        s = { w: 18, h: 22 };
    }
    return {
        x1: point2.x,
        y1: point2.y,
        x2: point2.x + s.w,
        y2: point2.y + s.h
    };
};

SearchMapInfoWindow.prototype.show = function () {
    var component = this;
    component.$element.removeClass(MEASURE);
    if (!component.objectsAreVisible) {
        component.$element.addClass(IS_VISIBLE);
        component.objectsAreVisible = true;
    }
};

SearchMapInfoWindow.prototype.measure = function () {
    var component = this;
    var google = window.google;

    component.$element.addClass(MEASURE);
    var $ele = component.$element.find('.search-map-infowindow__list');
    var point = $ele.offset();
    var size = { w: $ele.width(), h: $ele.height() };
    // add padding
    point.left -= 10;point.top -= 69;size.w += 20;size.h += 89;
    if (component.selectedObjectScreenPos !== null) {
        var markerPosition = component.selectedObjectScreenPos;
        // is marker within region of info-window
        if (markerPosition.x1 >= point.left && markerPosition.x1 <= point.left + size.w && markerPosition.y1 >= point.top && markerPosition.y1 <= point.top + size.h) {
            var yOffset = markerPosition.y2 - point.top - 20;
            google.maps.event.addListenerOnce(this.parent.map, 'idle', function () {
                component.show();
            });
            this.parent.mapEventProxy.forwardProxiedEvents(false);
            this.parent.map.panBy(0, yOffset);
            this.parent.mapEventProxy.forwardProxiedEvents(true);
        } else {
            component.show();
        }
    }
};

SearchMapInfoWindow.prototype.hide = function () {
    var component = this;
    component.$element.removeClass(IS_VISIBLE);
    component.objectsAreVisible = false;
    (0, _jquery2.default)(document).off('keydown' + EVENT_NAMESPACE);
};

/**
 * Load the object information from server
 * @param {Integer} tinyId
 */
SearchMapInfoWindow.prototype.loadObjectInfo = function (objectIds) {
    var component = this;
    component.objects = component.getObjectList(objectIds);
    component.$objectList.addClass(IS_UPDATING);

    var objectIdsQuery = component.objects.join('-');
    var currentSearchQuery = encodeURIComponent(component.parent.currentSearch);

    var url = component.objectUrl.replace('{objectId}', objectIdsQuery).replace('{searchQuery}', currentSearchQuery);

    // abort previous request if still pending
    if (component.request) {
        component.request.abort();
    }
    component.request = (0, _ajax2.default)({
        url: url,
        method: 'GET',
        dataType: 'JSON',
        timeout: TIMEOUT_SEARCH_REQUEST
    });
    component.request.done(function (data) {
        if (data !== undefined && data !== null) {
            component.updateResults(data);
        }
    });

    component.request.fail(function (err) {
        if (err.statusText !== 'abort') {
            component.isUpdated();
        }
    });
};

SearchMapInfoWindow.prototype.isUpdated = function () {
    var component = this;
    setTimeout(function () {
        component.$objectList.removeClass(IS_UPDATING);
    }, 250);
    (0, _jquery2.default)(document).trigger('resultsUpdated');
};

/**
 * Set new results on the page
 * @param {Object} data
 */
SearchMapInfoWindow.prototype.updateResults = function (data) {
    var component = this;
    var content = data.content;
    if (content !== '') {
        component.removeSlickBinding();
        component.$objectList.html(content);
        _serviceController2.default.getAllInstances(_userSaveObject2.default);
        if (component.objects.length > 1) {
            component.addSlickBinding();
        }
        (0, _jquery2.default)(document).on('keyup' + EVENT_NAMESPACE, function (e) {
            if (e.keyCode == KEY_CODE.ESCAPE) {
                component.parent.recordMapState({ id: null });
                component.parent.markerOverlay.hideObjectInfo();
            }
        });

        component.measure();
    }
    component.isUpdated();
};

/**
 * Initialize slick and determine current slide
 * @param {Number} currentSlide
 */
SearchMapInfoWindow.prototype.addSlickBinding = function () {
    var component = this;
    // start by a given point
    component.updatePosition(component.currentSlide);
    // bind carousel to results
    component.$objectList.slick({
        speed: SLICK_SLIDE_SPEED,
        cssEase: 'ease',
        mobileFirst: true,
        infinite: false,
        useTransform: true,
        touchThreshold: SLICK_SWIPE_SENSITIVITY,
        initialSlide: component.currentSlide || 0,
        slidesToShow: 1,
        slidesToScroll: 1,
        nextArrow: component.$nextButton,
        prevArrow: component.$previousButton
    });
    component.slideshowIsInitialized = true;
    // add key bindings for easy navigating through the results
    (0, _jquery2.default)(document).on('keyup' + EVENT_NAMESPACE, function (e) {
        if (e.keyCode == KEY_CODE.LEFT && !e.altKey) {
            component.$objectList.slick('slickPrev', false);
        } else if (e.keyCode == KEY_CODE.RIGHT && !e.altKey) {
            component.$objectList.slick('slickNext', false);
        }
    });
    // show navigation
    component.$navigation.addClass(IS_VISIBLE);
};

SearchMapInfoWindow.prototype.removeSlickBinding = function () {
    var component = this;
    // hide navigation and undo key binding
    component.$navigation.removeClass(IS_VISIBLE);
    (0, _jquery2.default)(document).off('keydown' + EVENT_NAMESPACE);

    if (component.slideshowIsInitialized) {
        component.$objectList.slick('unslick');
        component.slideshowIsInitialized = false;
    }
};

/**
 * update position result
 * @param {Number} nextSlide; zero index based number
 */
SearchMapInfoWindow.prototype.updatePosition = function (nextSlide) {
    var component = this;
    if (nextSlide < 0) {
        nextSlide = 0;
    }
    var currentSlide = parseInt(nextSlide, 10) + 1;
    var amountObjects = component.objects.length;
    var positionDescription = currentSlide + ' ' + component.positionSeparator + amountObjects;

    //history.pushState(null, null, window.location.search.replace(/id=.*(&?)/, 'id=' + component.objects[currentSlide - 1]));
    if (!component.skipUpdate) {
        component.parent.recordMapState({ id: component.objects[currentSlide - 1] });
    } else {
        component.skipUpdate = false;
    }

    component.$positionObject.text(positionDescription);
};

/**
 * Set string to array
 * @param {string} objects
 * @returns {*|Array}
 */
SearchMapInfoWindow.prototype.getObjectList = function (objectIds) {
    if (typeof objectIds == 'string') {
        return objectIds.split('-');
    }
    return objectIds;
};

// turn all elements with the default selector into components
//$(COMPONENT_SELECTOR).each((index, element) => new SearchMapInfoWindow(element));

},{"../ajax/ajax":198,"../map/map-tile-cache":263,"../service-controller/service-controller":317,"../user-save-object/user-save-object":337,"funda-slick-carousel":"funda-slick-carousel","jquery":"jquery"}],309:[function(require,module,exports){
// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

// explicitly inject dependencies (alphabetically), only those needed

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

var _debounce = require('lodash/debounce');

var _debounce2 = _interopRequireDefault(_debounce);

var _mapEvents = require('../map/map-events');

var _mapEvents2 = _interopRequireDefault(_mapEvents);

var _mapTileCache = require('../map/map-tile-cache');

var _mapTileCache2 = _interopRequireDefault(_mapTileCache);

var _mapLocalstorage = require('../map-localstorage/map-localstorage');

var _mapLocalstorage2 = _interopRequireDefault(_mapLocalstorage);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// what does this module expose?
exports.default = SearchMapMarkerManager;

// component configuration

var MARKER_ATTR = 'data-marker';
var MARKER_CLASS = 'search-map-marker';
var MARKER_DEFAULT_CLASS = ' search-map-marker__default';
var MARKER_MULTIPLE_OBJECTS_CLASS = ' search-map-marker__multiple-objects';
var IS_ACTIVE = 'is-active';
var IS_CAPPED = 'capped';
var IS_VIEWED = 'viewed';
var SHOW_OBJECT_INFO_EVENT = 'show_object_info';
var HIDE_OBJECT_INFO_EVENT = 'hide_object_info';
// pending on the size of the viewport the marker will be displayed at certain size. Small viewport -> large icon
var MARKER_SIZE = { small: 14, medium: 13 }; // px
var MINIMUM_INTERVAL_BETWEEN_RESIZE_VIEWPORT = 250; // ms
var MEDIUM_VIEWPORT_SIZE = 750; //$bp-medium

function SearchMapMarkerManager($map, config) {
    var component = this;
    var $window = (0, _jquery2.default)(window);

    component.$map = $map;
    component.config = config;
    component.selectedMarker = _mapEvents2.default.getValueFromHash('id') || null;
    component.pixelDistance = $window.width() >= MEDIUM_VIEWPORT_SIZE ? MARKER_SIZE.medium : MARKER_SIZE.small;
    component.isInitialized = false;
    // caching
    component.mapTileCache = _mapTileCache2.default;

    component.bindEvents();
}

SearchMapMarkerManager.prototype.bindEvents = function () {
    var component = this;
    var $window = (0, _jquery2.default)(window);
    component.$map.on(SHOW_OBJECT_INFO_EVENT, function (event, eventArgs) {
        if (eventArgs.objectId !== undefined && eventArgs.objectId !== null) {
            component.selectMarker(eventArgs.objectId);
        }
    });
    component.$map.on(HIDE_OBJECT_INFO_EVENT, function () {
        return component.unselectMarker();
    });

    function debouncedResizeViewport() {
        return (0, _debounce2.default)(function () {
            if ($window.width() >= MEDIUM_VIEWPORT_SIZE) {
                component.pixelDistance = MARKER_SIZE.medium;
            } else {
                component.pixelDistance = MARKER_SIZE.small;
            }
        }, MINIMUM_INTERVAL_BETWEEN_RESIZE_VIEWPORT, { leading: false, trailing: true });
    }
    $window.on('resize', debouncedResizeViewport());
};

/**
 * Create a html marker and returns html
 * @param {Object} object {id: *, lat: *, lng: *, sub: []}
 * @returns {Element}
 */
SearchMapMarkerManager.prototype.getElement = function (object) {
    var component = this;
    var markerId = object.id;

    var isViewed = _mapLocalstorage2.default.isViewed(object.id, object.capped);
    var marker = document.createElement('div');
    marker.setAttribute(MARKER_ATTR, markerId);

    if (object.capped) {
        marker.setAttribute('class', component.createClassName(markerId, MARKER_MULTIPLE_OBJECTS_CLASS, object.capped, isViewed));
        marker.innerHTML = '•';
    } else if (object.count > 1) {
        marker.setAttribute('class', component.createClassName(markerId, MARKER_MULTIPLE_OBJECTS_CLASS, object.capped, isViewed));
        marker.innerHTML = object.count;
    } else {
        marker.setAttribute('class', component.createClassName(markerId, MARKER_DEFAULT_CLASS, object.capped, isViewed));
    }
    // trigger event only once (!initialized)
    if (!component.isInitialized && component.isActive(markerId)) {
        component.$map.trigger(SHOW_OBJECT_INFO_EVENT, { objectId: component.selectedMarker, initial: true });
        component.isInitialized = true;
    }

    marker.style.left = object.x + 'px';
    marker.style.top = object.y + 'px';
    // return marker (in this case to component map-marker-overlay)
    return marker;
};

/**
 * Set selected marker to active state
 * @param {string} objectId
 */
SearchMapMarkerManager.prototype.selectMarker = function (objectId) {
    var component = this;

    component.unselectMarker();

    component.selectedMarker = objectId;
    (0, _jquery2.default)('[' + MARKER_ATTR + '*="' + objectId + '"]').addClass(IS_ACTIVE).addClass(IS_ACTIVE);
};

/**
 * Unselect the marker and remove the active state
 */
SearchMapMarkerManager.prototype.unselectMarker = function () {
    var component = this;
    var $selectedMarker = (0, _jquery2.default)('[' + MARKER_ATTR + '*="' + component.selectedMarker + '"]');

    if (component.selectedMarker && !$selectedMarker.hasClass('capped')) {
        if (_mapLocalstorage2.default.isViewed(component.selectedMarker)) {
            $selectedMarker.addClass(IS_VIEWED);
        }
    }

    component.selectedMarker = null;
    $selectedMarker.removeClass(IS_ACTIVE);
};

/**
 * Unselect the marker and remove the active state
 */
SearchMapMarkerManager.prototype.unselectAllMarkers = function () {
    var component = this;
    var $selectedMarker = (0, _jquery2.default)('[' + MARKER_ATTR + ']');

    $selectedMarker.removeClass(IS_ACTIVE);
    component.selectedMarker = null;
};

/**
 * Create class name for marker
 * @param markerId
 * @param typeMarker
 * @returns {string}
 */
SearchMapMarkerManager.prototype.createClassName = function (markerId, typeMarker, capped, isViewed) {
    var component = this;

    var domainSpecificType = typeMarker + ' ' + component.config.markerDomainClass;
    var isActive = component.isActive(markerId);
    var classes = [MARKER_CLASS, domainSpecificType];

    if (isActive) {
        classes.push(IS_ACTIVE);
    }
    if (capped) {
        classes.push(IS_CAPPED);
    }
    if (isViewed) {
        classes.push(IS_VIEWED);
    }

    return classes.join(' ');
};

/**
 * Determine if marker is selected
 * @param {string} markerId
 * @returns {boolean}
 */
SearchMapMarkerManager.prototype.isActive = function (markerId) {
    var component = this;
    return markerId.indexOf(component.selectedMarker) > -1 ? true : false;
};

},{"../map-localstorage/map-localstorage":259,"../map/map-events":261,"../map/map-tile-cache":263,"jquery":"jquery","lodash/debounce":158}],310:[function(require,module,exports){
// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

// explicitly inject dependencies (alphabetically), only those needed

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

var _map = require('../map/map');

var _map2 = _interopRequireDefault(_map);

var _mapEventProxy = require('../map/map-event-proxy');

var _mapEventProxy2 = _interopRequireDefault(_mapEventProxy);

var _mapEvents = require('../map/map-events');

var _mapEvents2 = _interopRequireDefault(_mapEvents);

var _mapMarkerOverlay = require('../map/map-marker-overlay');

var _mapMarkerOverlay2 = _interopRequireDefault(_mapMarkerOverlay);

var _searchMapMarkerManager = require('../search-map-marker-manager/search-map-marker-manager');

var _searchMapMarkerManager2 = _interopRequireDefault(_searchMapMarkerManager);

var _searchMapInfowindow = require('../../components/search-map-infowindow/search-map-infowindow');

var _searchMapInfowindow2 = _interopRequireDefault(_searchMapInfowindow);

require('../search-map-content/search-map-content');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = SearchMap;

// component configuration

var COMPONENT_SELECTOR = '[data-search-map]';
var INFO_WINDOW_COMPONENT_SELECTOR = '[data-search-map-infowindow]';
var SHOW_OBJECT_INFO_EVENT = 'show_object_info';
var HIDE_OBJECT_INFO_EVENT = 'hide_object_info';
var NO_OBJECT_EVENT_ARG = 'no_object';
var FORCE_REFRESH_MAPTILES_EVENT = 'force_maptile_refresh';
var SEARCH_QUERY_UPDATED_EVENT = 'zo_updated';
var PUSH_STATE_MARKER = 'push_state_set_by_funda';
var ASYNC_SIDEBAR_URL = '/kaartsidebar';
var MIN_ZOOM = 8;
var MAX_ZOOM = 19;

function SearchMap(element) {
    var component = this;
    component.doNotEnforceZoomLevel = false;
    // event proxy
    component.mapEventProxy = _mapEventProxy2.default;
    component.mapEvents = _mapEvents2.default;
    component.currentSearch = component.mapEvents.getCurrentSearchFromUrl();

    _map2.default.call(this, element, function mapLoadedHandler() {
        // The Google Map API idle event will create the overlay
        component.createOverlay();

        component.mapEventProxy.removeListener(component.zoomLimitHandlerId);
        component.mapEventProxy.addListener('zoom_changed', function () {
            component.verifyMapZoomLimit(component.map);
        });
        component.mapEventProxy.addListener('idle', function () {
            component.recordMapState();
        });

        component.$element.on(FORCE_REFRESH_MAPTILES_EVENT, function (event, eventArgs) {
            component.doNotEnforceZoomLevel = !eventArgs.isfinshed;
        });
        window.addEventListener('popstate', component.onMapPopState.bind(component), false);
        (0, _jquery2.default)(INFO_WINDOW_COMPONENT_SELECTOR).each(function (index, ele) {
            component.infoWindow = new _searchMapInfowindow2.default(ele, component);
        });
        component.$element.on(SEARCH_QUERY_UPDATED_EVENT, function (event, eventArgs) {
            component.currentSearch = eventArgs.zo;
        });
    });
}

SearchMap.prototype = Object.create(_map2.default.prototype);

/**
 *  Create marker overlay with custom markers
 */
SearchMap.prototype.createOverlay = function () {
    var component = this;
    component.mapMarkerManager = new _searchMapMarkerManager2.default(component.$element, component.config);
    var mapData = {
        map: component.map,
        config: component.config,
        tileSize: component.tileSize,
        cluster: _mapEvents2.default.getValueFromHash('c') || 0
    };
    component.markerOverlay = new _mapMarkerOverlay2.default(mapData, component.mapMarkerManager);
    // Use this map type as overlay.
    component.map.overlayMapTypes.insertAt(0, component.markerOverlay);
    // map changed
    component.$element.on(SHOW_OBJECT_INFO_EVENT, function (event, eventArgs) {
        var id = eventArgs.objectId;
        // we could get an id consisting of multiple parts
        var idParts = id.split('-');
        if (idParts.length > 1) {
            id = idParts[0];
        }
        component.recordMapState({ id: id });
    });
    component.$element.on(HIDE_OBJECT_INFO_EVENT, function (event, eventArgs) {
        if (eventArgs.type === NO_OBJECT_EVENT_ARG) {
            component.recordMapState({ id: null });
        }
    });
};

/**
 *  implementation that checks zoomlevel limits that can be switched off (used when forcing google map to reload tiles)
 */
SearchMap.prototype.verifyMapZoomLimit = function (map) {
    var component = this;
    if (component.doNotEnforceZoomLevel) return;

    var currentZoom = map.getZoom();
    if (currentZoom > MAX_ZOOM) {
        map.setZoom(MAX_ZOOM);
    } else if (currentZoom < MIN_ZOOM) {
        map.setZoom(MIN_ZOOM);
    }
};

/**
 * handles popstate for map
 */
SearchMap.prototype.onMapPopState = function (event) {
    var component = this;
    var google = window.google;

    if (event && event.state === PUSH_STATE_MARKER) {
        // handle map stuff
        component.mapEventProxy.forwardProxiedEvents(false);
        var prevLocation = component.mapEvents.getValueFromHash('center');
        var prevZoom = component.mapEvents.getValueFromHash('zoom');
        if (prevLocation && prevZoom) {
            var latlng = component.mapEvents.getValueFromHash('center').split(',');
            var weirdPoint = new google.maps.LatLng(parseFloat(latlng[0]) + .5, parseFloat(latlng[1]) + .5);
            component.map.setCenter(weirdPoint);
            var point = new google.maps.LatLng(parseFloat(latlng[0]), parseFloat(latlng[1]));
            component.map.setCenter(point);
            component.map.setZoom(parseInt(component.mapEvents.getValueFromHash('zoom'), 10));
        }
        // do tiles need refreshing
        var newSearchFromUrl = component.mapEvents.getCurrentSearchFromUrl();
        if (newSearchFromUrl != component.currentSearch) {
            component.markerOverlay.refreshTiles(newSearchFromUrl);
            component.currentSearch = newSearchFromUrl;
            component.refreshSidebar(newSearchFromUrl);
        }
        // marker / infowindow
        var rawId = component.mapEvents.getValueFromHash('id');
        if (rawId !== null && rawId !== '') {
            component.mapMarkerManager.selectMarker(rawId);
            component.infoWindow.processEvent({ initial: true, objectId: rawId });
        } else {
            component.mapMarkerManager.unselectAllMarkers();
            component.infoWindow.hide();
        }
        component.mapEventProxy.forwardProxiedEvents(true);
    }
};

SearchMap.prototype.refreshSidebar = function (newSearch) {
    _jquery2.default.ajax({
        type: 'GET',
        url: ASYNC_SIDEBAR_URL + newSearch,
        dataType: 'text',
        success: function success(data) {
            (0, _jquery2.default)('form').html((0, _jquery2.default)(data));
            (0, _jquery2.default)(window).trigger('sidebar-refresh');
        }
    });
};

// turn all elements with the default selector into components
(0, _jquery2.default)(COMPONENT_SELECTOR).each(function (index, element) {
    return new SearchMap(element);
});

},{"../../components/search-map-infowindow/search-map-infowindow":308,"../map/map":265,"../map/map-event-proxy":260,"../map/map-events":261,"../map/map-marker-overlay":262,"../search-map-content/search-map-content":307,"../search-map-marker-manager/search-map-marker-manager":309,"jquery":"jquery"}],311:[function(require,module,exports){
// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

// explicitly inject dependencies (alphabetically), only those needed

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

var _searchElementInViewport = require('../search-element-in-viewport/search-element-in-viewport');

var _searchElementInViewport2 = _interopRequireDefault(_searchElementInViewport);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// what does this module expose?
exports.default = SearchNoResults;

// component configuration

var COMPONENT_SELECTOR = '[data-search-no-results]';
var COMPONENT_POSITION_TOP = 0.4; // 40% height of viewport
var WARNING_CLASS = 'search-no-results-warning';
var RESULTS_UPDATED_EVENT = 'resultsUpdated';

function SearchNoResults(element) {
    var component = this;
    component.element = element;
    component.$element = (0, _jquery2.default)(component.element);
    component.noResults = new _searchElementInViewport2.default(component.element, COMPONENT_POSITION_TOP);

    component.updateState();
}

/**
 * Center the component inside the viewport to show the user there are 0 results
 */
SearchNoResults.prototype.updateState = function () {
    var component = this;
    component.noResults.updateVerticalPosition();

    // class may have been added already by clicking another filter, so remove it first
    component.$element.removeClass(WARNING_CLASS).addClass(WARNING_CLASS);
};

SearchNoResults.enhance = function () {
    var element = (0, _jquery2.default)(COMPONENT_SELECTOR)[0];

    if (element !== undefined) {
        return new SearchNoResults(element);
    }
};

(0, _jquery2.default)(document).on(RESULTS_UPDATED_EVENT, SearchNoResults.enhance);
SearchNoResults.enhance();

},{"../search-element-in-viewport/search-element-in-viewport":305,"jquery":"jquery"}],312:[function(require,module,exports){
// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

// explicitly inject dependencies (alphabetically), only those needed

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

require('../../components/conditional-range-filter/conditional-range-filter');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// what does this module expose?
exports.default = SearchPriceToggle;

// component configuration

var COMPONENT_SELECTOR = '[data-search-price-toggle]';
var PRICE_TYPE_INPUT_SELECTOR = '[data-search-price-toggle-input]';
var PRICE_TYPE_TARGET_ATTR = 'data-search-price-toggle-target';
var PRICE_TYPE_TARGET_SELECTOR = '[' + PRICE_TYPE_TARGET_ATTR + ']';
var PRICE_RANGE_SELECTOR = '[data-range-filter]';
var MAP_CONDITION_SELECTOR = '[data-display-range-filter]';
var MAP_PRICE_PAIR_ITEM_ATTR = 'data-display-range-value';
var MAP_PRICE_PAIR_ITEM_SELECTOR = '[' + MAP_PRICE_PAIR_ITEM_ATTR + ']';
var ACTIVE_CLASS = 'is-active';

function SearchPriceToggle(element) {
    var component = this; // Not necessary in ES2015, keep using it for consistency
    component.$element = (0, _jquery2.default)(element);

    component.$targets = component.$element.find(PRICE_TYPE_TARGET_SELECTOR);
    component.$currentActiveTarget = component.$element.find(PRICE_TYPE_TARGET_SELECTOR + '.' + ACTIVE_CLASS);

    component.$targetsDisplay = component.$element.find(MAP_PRICE_PAIR_ITEM_SELECTOR);
    component.$currentActiveTargetDisplay = component.$element.find(MAP_PRICE_PAIR_ITEM_SELECTOR + '.' + ACTIVE_CLASS);

    component.bindEvents();
}

SearchPriceToggle.prototype.bindEvents = function () {
    var component = this;

    component.$element.on('change', PRICE_TYPE_INPUT_SELECTOR, function (event) {
        var $input = (0, _jquery2.default)(event.currentTarget);
        var targetValue = $input.val();
        component.showTarget(targetValue);
    });
    component.$element.on('change', MAP_CONDITION_SELECTOR, function (event) {
        var $input = (0, _jquery2.default)(event.currentTarget);
        var targetValue = $input.val();
        component.showTargetDisplay(targetValue);
    });
};

SearchPriceToggle.prototype.showTarget = function (targetValue) {
    var component = this;

    component.$currentActiveTarget.find(PRICE_RANGE_SELECTOR).trigger('resetfilter');
    component.$targets.removeClass(ACTIVE_CLASS);

    component.$currentActiveTarget = component.$element.find('[' + PRICE_TYPE_TARGET_ATTR + '="' + targetValue + '"]');
    component.$currentActiveTarget.addClass(ACTIVE_CLASS);
};

SearchPriceToggle.prototype.showTargetDisplay = function (targetValue) {
    var component = this;

    component.$currentActiveTargetDisplay.trigger('resetfilter');
    component.$targetsDisplay.removeClass(ACTIVE_CLASS);

    component.$currentActiveTargetDisplay = component.$element.find('[' + MAP_PRICE_PAIR_ITEM_ATTR + '="' + targetValue + '"]');
    component.$currentActiveTargetDisplay.addClass(ACTIVE_CLASS);
};

// turn all elements with the default selector into components
(0, _jquery2.default)(COMPONENT_SELECTOR).each(function (index, element) {
    return new SearchPriceToggle(element);
});

},{"../../components/conditional-range-filter/conditional-range-filter":221,"jquery":"jquery"}],313:[function(require,module,exports){
// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

// explicitly inject dependencies (alphabetically), only those needed

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

var _searchElementInViewport = require('../search-element-in-viewport/search-element-in-viewport');

var _searchElementInViewport2 = _interopRequireDefault(_searchElementInViewport);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ajax = require('../ajax/ajax');
var AppSpinner = require('../app-spinner/app-spinner');

// what does this module expose?
exports.default = SearchResultsLoading;

// component configuration

var COMPONENT_SELECTOR = '[data-search-results-loading]';
var COMPONENT_SPINNER_SELECTOR = '[data-search-results-spinner]';
var COMPONENT_POSITION_TOP = '48px';
var IS_ACTIVE_CLASS = 'is-active';

function SearchResultsLoading(element) {
    var component = this;
    component.element = element;
    component.$element = (0, _jquery2.default)(element);

    // if appSpinner isn't called the loader won't be shown
    component.spinner = new AppSpinner(COMPONENT_SPINNER_SELECTOR);
    component.resultsLoader = new _searchElementInViewport2.default(component.element, COMPONENT_POSITION_TOP);

    (0, _jquery2.default)(document).on('resultsUpdating', function () {
        component.show();
    });

    ajax.onError(function () {
        return component.hide();
    });
    ajax.onSuccess(function () {
        return component.hide();
    });
}

/**
 * Show loading indicator
 */
SearchResultsLoading.prototype.show = function () {
    var component = this;

    component.$element.addClass(IS_ACTIVE_CLASS);
    component.resultsLoader.updateVerticalPosition();

    if (!component.spinner.isVisible()) {
        component.spinner.show();
    }
};

/**
 * Hide loading indicator
 */
SearchResultsLoading.prototype.hide = function () {
    var component = this;

    component.spinner.hide();
    component.$element.removeClass(IS_ACTIVE_CLASS);
};

// turn all elements with the default selector into components
(0, _jquery2.default)(COMPONENT_SELECTOR).each(function (index, element) {
    return new SearchResultsLoading(element);
});

},{"../ajax/ajax":198,"../app-spinner/app-spinner":201,"../search-element-in-viewport/search-element-in-viewport":305,"jquery":"jquery"}],314:[function(require,module,exports){
'use strict';

require('../../components/app-spinner/app-spinner');
require('../../components/expandible/expandible');

},{"../../components/app-spinner/app-spinner":201,"../../components/expandible/expandible":229}],315:[function(require,module,exports){
// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

// explicitly inject dependencies (alphabetically), only those needed

var _searchElementInViewport = require('../search-element-in-viewport/search-element-in-viewport');

var _searchElementInViewport2 = _interopRequireDefault(_searchElementInViewport);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var $ = require('jquery');


// what does this module expose?
module.exports = SearchScrollNotify;

// component configuration
var COMPONENT_SELECTOR = '[data-search-scroll-notify]';
var LAST_SEARCH_RESULT_SELECTOR = '[data-search-scroll-notify-target]';
var NOTIFICATION_SELECTOR = '[data-search-scroll-notification]';
var COMPONENT_POSITION_TOP = 0.4; // 40% of the viewport
var SCROLL_UP_ANCHOR_SELECTOR = '[data-search-scroll-notify-anchor]';
var NOTIFICATION_ACTIVE_CLASS = 'is-active';
var RESULTS_UPDATED_EVENT = 'resultsUpdated';

function SearchScrollNotify(element) {
    var component = this;
    component.element = element;
    component.$element = $(element);
    component.$notification = component.$element.find(NOTIFICATION_SELECTOR);
    component.notification = new _searchElementInViewport2.default(component.element, COMPONENT_POSITION_TOP);

    component.checkLastSearchResultVisibility();
    component.bindToEvents();
}

/**
 * Toggle notification when search results are updated
 * Scroll to top when anchor is clicked
 */
SearchScrollNotify.prototype.bindToEvents = function () {
    var component = this;

    $(document).on(RESULTS_UPDATED_EVENT, function () {
        component.checkLastSearchResultVisibility();
    });

    component.$element.on('click', SCROLL_UP_ANCHOR_SELECTOR, function () {
        component.hideNotification();
    });
};

/**
 * Check if the last search result in the list is visible for the user
 * Show scroll up notification if the last search result is outside the viewport
 */
SearchScrollNotify.prototype.checkLastSearchResultVisibility = function () {
    var component = this;
    var lastSearchResult = $(LAST_SEARCH_RESULT_SELECTOR)[0];

    if (!lastSearchResult || isElementBelowViewportTop(lastSearchResult)) {
        component.hideNotification();
    } else {
        component.showNotification();
    }
};

/**
 * Show notification
 */
SearchScrollNotify.prototype.showNotification = function () {
    var component = this;
    // offsets are relative to the current viewport
    component.$notification.addClass(NOTIFICATION_ACTIVE_CLASS);
    component.notification.updateVerticalPosition();
};

/**
 * Hide notification
 */
SearchScrollNotify.prototype.hideNotification = function () {
    var component = this;
    component.$notification.removeClass(NOTIFICATION_ACTIVE_CLASS);
    component.$element.removeAttr('style');
};

/**
 * Check if top of element is inside or below the viewport
 * Left, right and bottom position of element are ignored
 */
function isElementBelowViewportTop(element) {
    return element.getBoundingClientRect().top > 0;
}

// turn all elements with the default selector into components
$(COMPONENT_SELECTOR).each(function (index, element) {
    return new SearchScrollNotify(element);
});

},{"../search-element-in-viewport/search-element-in-viewport":305,"jquery":"jquery"}],316:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

var _debounce = require('lodash/debounce');

var _debounce2 = _interopRequireDefault(_debounce);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = SearchSidebar;


var COMPONENT_SELECTOR = '[data-search-sidebar]';
var FLYOUT_SELECTOR = '[data-filter-flyout]';
var SIDEBAR_HANDLE = '[data-sidebar-handle]';
var CLOSE_FLYOUT_SELECTOR = '[data-search-close-flyout]';
var OPEN_FLYOUT_CLASS = 'has-open-flyout';
var FLYOUT_CLOSE_EVENT = 'filterflyoutclose';
var FLYOUT_CLOSED_EVENT = 'filterflyoutclosed';
var FLYOUT_OPEN_EVENT = 'filterflyoutopened';
var IS_EXTENDABLE_CLASS = 'is-extendable';
var IS_EXTENDED_CLASS = 'is-extended';
var SIDEBAR_IS_EXTENDED = 'sidebar-is-extended';
var SIDEBAR_CLOSED_EVENT = 'sidebarclosed';
var ERROR_BAR_SELECTOR = '[data-error-bar]';

var FLYOUT_HAS_FILTER_SELECTOR = '[data-has-preview-filter]';
var FLYOUT_FILTER_OPEN_CLASS = 'has-preview-filter';

function SearchSidebar(element) {
    var component = this;
    var $html = (0, _jquery2.default)('html');

    component.scrollPosition = 0;
    component.isExtended = false;

    component.bindToElements(element);

    component.bindEvents();
    component.watchFlyouts();
    component.toggleFlyoutStickyButton();
    component.$element.addClass(IS_EXTENDABLE_CLASS);

    $html.on('click', function (event) {
        var $target = (0, _jquery2.default)(event.target);
        var targetSidebarHandle = $target.is(SIDEBAR_HANDLE);
        var targetInsideHandle = $target.closest(SIDEBAR_HANDLE).length > 0;
        var targetInsideSidebar = $target.closest(COMPONENT_SELECTOR).length > 0 && !targetSidebarHandle;
        var targetInsideErrorBar = $target.closest(ERROR_BAR_SELECTOR).length > 0;
        var targetFlyoutHandle = $target.is(CLOSE_FLYOUT_SELECTOR);

        if (!targetSidebarHandle && !targetInsideHandle && !targetInsideSidebar && !targetInsideErrorBar && !targetFlyoutHandle && component.isExtended) {
            component.close();
        }
    });
    $html.on('click', SIDEBAR_HANDLE, function (event) {
        event.preventDefault();

        if (component.isExtended) {
            component.close();
        } else {
            component.open();
        }
    });
    // horizontal scroll on mobile triggers resize event.
    var windowWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
    (0, _jquery2.default)(window).on('resize', (0, _debounce2.default)(function () {
        var newWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        // Chrome 48 on iOS9.2.1 (incorrectly) reports a different window width when a select input has focus (ADA-288).
        // this should not trigger the sidebar to close.
        var widthDifferenceOnSelectFocus = 10;
        if (component.isExtended && Math.abs(windowWidth - newWidth) > widthDifferenceOnSelectFocus) {
            component.close();
        }
    }, 100));

    (0, _jquery2.default)(window).on('sidebar-refresh', function () {
        var $myNewElement = (0, _jquery2.default)(COMPONENT_SELECTOR);
        if ($myNewElement.length == 1) {
            component.bindToElements($myNewElement);
            component.bindEvents();
            component.watchFlyouts();
            component.toggleFlyoutStickyButton();
            component.$element.addClass(IS_EXTENDABLE_CLASS);
            (0, _jquery2.default)(document).scrollTop(0);
        }
    });
}

SearchSidebar.prototype.bindToElements = function (element) {
    var component = this;

    component.$element = (0, _jquery2.default)(element);
    component.$flyouts = component.$element.find(FLYOUT_SELECTOR);
    component.$form = component.$element.closest('form');
    component.$flyoutWithFilter = component.$element.find(FLYOUT_HAS_FILTER_SELECTOR);
    component.$closeFlyoutButton = (0, _jquery2.default)(CLOSE_FLYOUT_SELECTOR);
    component.$sideBarHandles = (0, _jquery2.default)(SIDEBAR_HANDLE);
};

SearchSidebar.prototype.bindEvents = function () {
    var component = this;

    component.$closeFlyoutButton.on('click', function () {
        component.$element.find(FLYOUT_SELECTOR).trigger(FLYOUT_CLOSE_EVENT);
    });
};

SearchSidebar.prototype.open = function () {
    var component = this;
    component.isExtended = true;
    component.$element.addClass(IS_EXTENDED_CLASS);
    (0, _jquery2.default)('html').addClass(SIDEBAR_IS_EXTENDED);
};

SearchSidebar.prototype.close = function () {
    var component = this;
    window.scrollTo(0, 0);
    component.isExtended = false;
    component.$element.removeClass(IS_EXTENDED_CLASS);
    component.$element.trigger(SIDEBAR_CLOSED_EVENT);
    (0, _jquery2.default)('html').removeClass(SIDEBAR_IS_EXTENDED);
};

SearchSidebar.prototype.watchFlyouts = function () {
    var component = this;

    component.$form.on(FLYOUT_OPEN_EVENT, FLYOUT_SELECTOR, function () {
        component.$element.addClass(OPEN_FLYOUT_CLASS);
        component.scrollPosition = component.$element.scrollTop();
        component.$element.animate({ scrollTop: 0 }, 0);
    });

    component.$form.on(FLYOUT_CLOSED_EVENT, FLYOUT_SELECTOR, function () {
        component.$element.removeClass(OPEN_FLYOUT_CLASS);
        component.$element.animate({ scrollTop: component.scrollPosition }, 0);
    });
};

SearchSidebar.prototype.toggleFlyoutStickyButton = function () {
    var component = this;

    component.$form.on('click', FLYOUT_HAS_FILTER_SELECTOR, function () {
        component.$element.toggleClass(FLYOUT_FILTER_OPEN_CLASS);
    });

    component.$form.on(FLYOUT_CLOSED_EVENT, FLYOUT_SELECTOR, function () {
        component.$element.removeClass(FLYOUT_FILTER_OPEN_CLASS);
    });
};

// turn all elements with the default selector into components
(0, _jquery2.default)(COMPONENT_SELECTOR).each(function (index, element) {
    return new SearchSidebar(element);
});

},{"jquery":"jquery","lodash/debounce":158}],317:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var controllerService = function () {
    var publics = {};
    var SERVICE_DATA = 'controller-uid';
    var controllers = [];

    var uid = 0;

    function getUid() {
        return uid++;
    }

    function get$element(element, ControllerClass) {
        return typeof ControllerClass.getSelector === 'function' ? (0, _jquery2.default)(element).find(ControllerClass.getSelector()) : (0, _jquery2.default)(element);
    }

    function getControllerBy$element($element, ControllerClass) {
        if ($element.length !== 1) {
            console.error('For binding a controller we need to have exactly one html, but we have ' + $element.length);
            console.error($element, ControllerClass.getSelector());
        }

        var id = $element.data(SERVICE_DATA);
        if (typeof id === 'undefined') {
            id = getUid();
            controllers[id] = new ControllerClass($element);
            $element.attr('data-' + SERVICE_DATA, id);
        }
        return controllers[id];
    }

    /**
     * Features (a.k.a. limtitations) one html attribute can have at most one controller!
     * @param element
     * @param ControllerClass
     * @param useSelector
     * @returns {*}
     */
    publics.getInstance = function (ControllerClass, element) {
        var $element = get$element(element, ControllerClass);
        return getControllerBy$element($element, ControllerClass);
    };

    /**
     *
     * @param ControllerClass
     * @param scope
     * @returns {Array}
     */
    publics.getAllInstances = function (ControllerClass, scope) {
        var componentSelector = ControllerClass.getSelector();
        var results = [];
        var $scope = typeof scope === 'undefined' ? (0, _jquery2.default)(componentSelector) : (0, _jquery2.default)(scope).find(componentSelector);

        $scope.each(function (i, element) {
            return results.push(getControllerBy$element((0, _jquery2.default)(element), ControllerClass));
        });

        return results;
    };

    return publics;
}();

exports.default = controllerService;

},{"jquery":"jquery"}],318:[function(require,module,exports){
// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

// explicitly inject dependencies (alphabetically), only those needed

var $ = require('jquery');

// what does this module expose?
module.exports = SocialLogin;

// component configuration
var COMPONENT_SELECTOR = '[data-social-login]';
var PROVIDER_ATTR = 'data-social-login-provider';
var ROOT_URL_ATTR = 'data-social-login-root-url';
var LOGIN_DIALOG_SELECTOR = '[data-dialog="user-form"]';

function SocialLogin(element) {
    var component = this;

    component.$element = $(element);
    component.provider = element.getAttribute(PROVIDER_ATTR);
    component.state = {
        reauthenticate: false,
        callback: null,
        notify: null,
        notifySelector: null,
        notifyParent: null,
        returnUrl: null,
        rootUrl: element.getAttribute(ROOT_URL_ATTR)
    };

    // Act only when provider is set
    if (component.provider !== undefined) {
        component.state.returnUrl = component.getReturnUrl();

        // Bind handler
        component.$element.on('click', function () {
            component.doAuth();
        });
    }
}

SocialLogin.prototype.doAuth = function () {
    var component = this;
    // write handler to window
    component.writeHandler();

    // initiate login
    component.invokeLogin();

    // find (this) existing form and remove it
    var baseUrl = component.state.rootUrl.slice(0, -1);
    var authForm = $('<form/>').attr('method', 'POST').attr('target', 'login-popup').attr('action', baseUrl + '/mijn/sociallogin/externallogin/').hide();
    authForm.append($('<input>').attr('type', 'hidden').attr('name', 'provider').attr('value', component.provider));

    if (!component.state.returnUrl) {
        component.state.returnUrl = component.state.rootUrl;
    }
    authForm.append($('<input>').attr('type', 'hidden').attr('name', 'returnUrl').attr('value', component.state.returnUrl));
    authForm.append($('<input>').attr('type', 'hidden').attr('name', 'currentUrl').attr('value', document.location.pathname));
    authForm.append($('<input>').attr('type', 'hidden').attr('name', 'rerequest').attr('value', component.state.reauthenticate ? '1' : '0'));
    $('body').append(authForm);
    authForm.submit();
};

SocialLogin.prototype.invokeLogin = function () {
    var chrome = 100;
    var width = 500;
    var height = 500;
    var left = (screen.width - width) / 2;
    var top = (screen.height - height - chrome) / 2;
    var options = 'status=0,toolbar=0,location=1,resizable=1,scrollbars=1,left=' + left + ',top=' + top + ',width=' + width + ',height=' + height;
    window.open('about:blank', 'login-popup', options);
};

SocialLogin.prototype.postLoginHandler = function (success, returnUrl) {
    if (returnUrl) {
        setTimeout(function () {
            window.location.href = returnUrl;
        }, 160);
    }
};

SocialLogin.prototype.writeHandler = function () {
    var component = this;
    // set callback on global scope
    window.socialCall = function (social) {
        return function (success, returnUrl) {
            if (success) {
                var dialogElement = document.querySelector(LOGIN_DIALOG_SELECTOR);
                if (dialogElement !== null) {
                    dialogElement.dialog.onFormSuccess();
                } else {
                    social.postLoginHandler(success, returnUrl);
                }
            }

            // remove myself
            window.socialCall = undefined;
        };
    }(component);
};

SocialLogin.prototype.getReturnUrl = function () {
    var qs = window.location.search.substr(1).split('&');
    var b = null;
    for (var i = 0; i < qs.length; ++i) {
        var p = qs[i].split('=');
        if (p.length !== 2 || p[0] !== 'ReturnUrl') {
            continue;
        }
        b = decodeURIComponent(p[1].replace(/\+/g, ' '));
    }
    return b;
};

// turn all elements with the default selector into components
$(COMPONENT_SELECTOR).each(function (index, element) {
    return new SocialLogin(element);
});

},{"jquery":"jquery"}],319:[function(require,module,exports){
// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

// explicitly inject dependencies (alphabetically), only those needed

var $ = require('jquery');

// what does this module expose?
module.exports = SoortNotificationCookie;

// component configuration
var COMPONENT_SELECTOR = '[data-soort-notification-cookie]';
var HANDLE_SELECTOR = '[data-soort-notification-cookie-close]';
var VERSION_ATTR = 'data-notification-cookie-version';
var SOORTAANBOD_ATTR = 'data-notification-cookie-soortaanbod';
var EVENT_NAMESPACE = '.notification-cookie';
var COOKIE_PREFIX = 'notificationAcknowledged_sa';
var ACKNOWLEDGED_CLASS = 'is-acknowledged';

function SoortNotificationCookie(element) {
    var component = this;
    component.$element = $(element);
    component.version = element.getAttribute(VERSION_ATTR);
    component.soortAanbod = element.getAttribute(SOORTAANBOD_ATTR);
    component.cookieName = COOKIE_PREFIX + '_' + component.version;

    component.$element.on('click' + EVENT_NAMESPACE, HANDLE_SELECTOR, function (event) {
        event.preventDefault();
        component.acknowledgeNotification();
        component.$element.addClass(ACKNOWLEDGED_CLASS);
    });
}

SoortNotificationCookie.prototype.acknowledgeNotification = function () {
    this.setCookie();
};

/**
 * sets cookie and return the cookie value
 * @returns {string}
 */
SoortNotificationCookie.prototype.setCookie = function () {
    var component = this;
    var date = new Date();
    date.setFullYear(date.getFullYear() + 1);
    var cookie = component.cookieName + '=' + component.version + '; expires=' + date.toGMTString() + '; path= /';

    document.cookie = cookie;
    return cookie;
};

// turn all elements with the default selector into components
$(COMPONENT_SELECTOR).each(function (index, element) {
    return new SoortNotificationCookie(element);
});

},{"jquery":"jquery"}],320:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

var _throttle = require('lodash/throttle');

var _throttle2 = _interopRequireDefault(_throttle);

var _debounce = require('lodash/debounce');

var _debounce2 = _interopRequireDefault(_debounce);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// what does this module expose?
exports.default = StickyNavigation;

// component configuration

var COMPONENT_WRAPPER_SELECTOR = '[data-sticky-navigation-wrapper]';
var COMPONENT_SELECTOR = '[data-sticky-navigation]';
var LINKS_SELECTOR = '[data-sticky-navigation-links]';
var $window = (0, _jquery2.default)(window);
var THROTTLE_DELAY = 100;
var DEBOUNCE_DELAY = 150;

function StickyNavigation(element) {
    var component = this;
    component.$element = (0, _jquery2.default)(element);
    component.$links = component.$element.find(LINKS_SELECTOR);
    component.$navWrapper = component.$element.parents(COMPONENT_WRAPPER_SELECTOR);
    component.setStickyWidth();
    component.registerOnScroll();
    component.registeronResize();
}

StickyNavigation.prototype.setStickyWidth = function () {
    var component = this;
    var widthBase = component.$element.width();
    var heightBase = component.$links.height();
    component.$links.width(widthBase);
    component.$element.height(heightBase);
};

StickyNavigation.prototype.registeronResize = function () {
    var component = this;
    $window.on('resize', (0, _debounce2.default)(function () {
        component.setStickyWidth();
        component.registerOnScroll();
    }, DEBOUNCE_DELAY));
};

StickyNavigation.prototype.registerOnScroll = function () {
    var component = this;
    var stickyTop = component.$element.offset().top;

    $window.on('scroll', (0, _throttle2.default)(function () {
        var windowTop = $window.scrollTop();
        var stickyHeight = component.$element.height();
        var navWrapperHeight = component.$navWrapper.height();
        var navWrapperTop = component.$navWrapper.offset().top;

        if (stickyTop < windowTop && navWrapperHeight + navWrapperTop - stickyHeight > windowTop) {
            component.sticky();
        } else {
            component.noSticky();
        }
    }, THROTTLE_DELAY));
};

StickyNavigation.prototype.sticky = function () {
    var component = this;
    component.$links.addClass('sticky');
};

StickyNavigation.prototype.noSticky = function () {
    var component = this;
    component.$links.removeClass('sticky');
};

// turn all elements with the default selector into components
(0, _jquery2.default)(COMPONENT_SELECTOR).each(function (index, element) {
    return new StickyNavigation(element);
});

},{"jquery":"jquery","lodash/debounce":158,"lodash/throttle":188}],321:[function(require,module,exports){
// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

var _debounce = require('lodash/debounce');

var _debounce2 = _interopRequireDefault(_debounce);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// what does this module expose?
exports.default = Tabs;

// component configuration

var COMPONENT_SELECTOR = '[data-tabs]';
var TABS_TARGET_SELECTOR = '[data-tabs-target]';
var TABS_HANDLE_SELECTOR = '[data-tabs-handle]';
var $HTML_BODY = (0, _jquery2.default)('html, body');
var $window = (0, _jquery2.default)(window);
var DEBOUNCE_DELAY = 150;
var ANIMATE_DELAY = 500;

function Tabs(element) {
    var component = this;
    component.$element = (0, _jquery2.default)(element);
    component.$handle = component.$element.find(TABS_HANDLE_SELECTOR);
    component.$target = component.$element.find(TABS_TARGET_SELECTOR);
    component.registerOnClick();
    component.registerOnResize();
}

Tabs.prototype.registerOnClick = function () {
    var component = this;
    component.$handle.on('click', function (e) {
        e.preventDefault();
        var $this = (0, _jquery2.default)(this);
        var targetId = $this.attr('href');
        component.responseOnClick($this, targetId);
        component.scrollView();
    });
};

Tabs.prototype.responseOnClick = function ($this, targetId) {
    var component = this;
    component.$handle.removeClass('active');
    component.$target.removeClass('active');
    $this.addClass('active');
    (0, _jquery2.default)(targetId).addClass('active');
};

Tabs.prototype.scrollView = function () {
    var component = this;
    var topPosition = component.$element.offset().top;
    $HTML_BODY.animate({
        scrollTop: topPosition
    }, ANIMATE_DELAY);
};

Tabs.prototype.registerOnResize = function () {
    var component = this;
    var windowWidth = $window.width();
    var windowHeight = $window.height();

    $window.on('resize', (0, _debounce2.default)(function () {
        component.checkWindowSize(windowWidth, windowHeight);
    }, DEBOUNCE_DELAY));
};

Tabs.prototype.checkWindowSize = function (windowWidth, windowHeight) {
    var component = this;
    if ($window.width() != windowWidth && $window.height() != windowHeight) {
        component.scrollView();
    }
};

// turn all elements with the default selector into components
(0, _jquery2.default)(COMPONENT_SELECTOR).each(function (index, element) {
    return new Tabs(element);
});

},{"jquery":"jquery","lodash/debounce":158}],322:[function(require,module,exports){
// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

// what does this module expose?

module.exports = ThousandSeparator;

function ThousandSeparator() {}

/**
 * Returns a 1000 => 1.000
 *
 * @param {Integer} num
 * @param {String} separator (period or comma)
 * @returns {String}
 */
ThousandSeparator.format = function (num, separator) {
    var data = num.toString();

    if (data.length > 3) {
        data = data.replace(/(\d)(?=(?:[0-9]{3})+\b)/gm, '$1' + separator);
    }
    return data;
};

/**
 * Returns a 1.000 => 1000
 * @param {Integer, String} num
 * @returns {Number}
 */
ThousandSeparator.parse = function (num) {
    var data = num.toString();

    if (data.length > 3) {
        data = data.replace(/[.,]/gm, '');
    }
    return parseInt(data, 10);
};

},{}],323:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

var _appUtilsArrayExtensions = require('../app-utils/app-utils-array-extensions');

var _appUtilsArrayExtensions2 = _interopRequireDefault(_appUtilsArrayExtensions);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = ToggleVisibility;

// constant definitions

var COMPONENT_SELECTOR = '[data-toggle-visibility]';
var CONTAINER_DATA = 'toggle-visibility-container';
var CONTAINERS_SELECTOR = '[data-' + CONTAINER_DATA + ']';
var TRIGGER_DATA = 'toggle-visibility-trigger';
var TRIGGER_SELECTOR = '[data-' + TRIGGER_DATA + ']';
var IS_HIDDEN_CLASS = 'is-hidden';

function ToggleVisibility(element) {
    var component = this;
    component.$element = (0, _jquery2.default)(element);

    component.$$containers = new _appUtilsArrayExtensions2.default(component.$element.find(CONTAINERS_SELECTOR));
    component.iActiveContainer = component.$$containers.getIndexBy(function (c) {
        return !(0, _jquery2.default)(c).hasClass(IS_HIDDEN_CLASS);
    });
    component.$triggers = component.$element.find(TRIGGER_SELECTOR);

    component.bindEvents();
}

ToggleVisibility.prototype.bindEvents = function () {
    var component = this;

    component.$triggers.on('click', function (event) {
        return component.toggle(event);
    });
};

ToggleVisibility.prototype.toggle = function (event) {
    var component = this;
    var $trigger = (0, _jquery2.default)(event.target);
    var $triggerThisContainer = $trigger.data(TRIGGER_DATA);

    var index = $triggerThisContainer ? //Do I have to trigger a specific container?
    //YES! Find the container with that name
    component.$$containers.getIndexBy(function (c) {
        return (0, _jquery2.default)(c).data(CONTAINER_DATA) === $triggerThisContainer;
    }) :
    //NOPE! Give me the next element (circular, so after last comes first again)
    component.$$containers.getNextIndexCircular(component.iActiveContainer);

    component.showByIndex(index);
};

ToggleVisibility.prototype.showByIndex = function (index) {
    var component = this;
    if (index >= 0 && index < component.$$containers.length) {
        component.$$containers.addClass(IS_HIDDEN_CLASS);
        (0, _jquery2.default)(component.$$containers[index]).removeClass(IS_HIDDEN_CLASS);
        component.iActiveContainer = index;
    }
};

(0, _jquery2.default)(COMPONENT_SELECTOR).each(function (index, element) {
    return new ToggleVisibility(element);
});

},{"../app-utils/app-utils-array-extensions":202,"jquery":"jquery"}],324:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = TooltipInline;

require('../expandible/expandible');

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var COMPONENT_ATTR = 'data-tooltip-inline';
var COMPONENT_SELECTOR = '[' + COMPONENT_ATTR + ']';
var HANDLE = '[data-tooltip-handle]';
var VISIBLE_CLASS = 'is-expanded';

function TooltipInline(element) {
    var component = this;
    component.$element = (0, _jquery2.default)(element);
    component.$handler = this.$element.find(HANDLE);
    this.$handler.on('click', function (event) {
        return component.open(event);
    });
    component.content = this.$element.attr('title');
    component.createTooltip();
}

TooltipInline.prototype.open = function (event) {
    var component = this;
    //prevent event bubbling
    event.preventDefault();
    event.stopPropagation();
    component.$element.addClass(VISIBLE_CLASS);
    this.$handler.off('click');
    (0, _jquery2.default)(window).on('click', function (closeEvent) {
        return component.close(closeEvent);
    });
};

TooltipInline.prototype.close = function (event) {
    var component = this;
    event.stopPropagation();
    component.$element.removeClass(VISIBLE_CLASS);
    this.$handler.on('click', function (openEvent) {
        return component.open(openEvent);
    });
};

TooltipInline.prototype.createTooltip = function () {
    var component = this;
    component.$tooltip = (0, _jquery2.default)('<div/>');
    component.$tooltip.addClass('tooltip__content');
    component.$tooltip.html(component.content);
    component.$element.append(component.$tooltip);
};

(0, _jquery2.default)(COMPONENT_SELECTOR).each(function (index, element) {
    return new TooltipInline(element);
});

},{"../expandible/expandible":229,"jquery":"jquery"}],325:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = Tooltip;


var COMPONENT_SELECTOR = '[data-tooltip]';
var IS_VISIBLE_CLASS = 'is-expanded';
var TITLE_ATTR = 'title';
var TOOLTIP_TEXT_ATTR = 'data-title';
var EVENTS_TO_LISTEN = 'resize click';
var EXPAND_TO_BOTTOM_CLASS = 'bottom';
var EXPAND_TO_LEFT_CLASS = 'reverse';
var SMALL_BREAKPOINT = 500;

function Tooltip(element) {
    var component = this;

    component.$element = (0, _jquery2.default)(element);
    component.initialInfo = undefined;

    component.removeTitle(); //Disables the tooltip on hover

    component.bindEvents();
}

Tooltip.prototype.bindEvents = function () {
    var component = this;

    //Since the expandable component closes when anything else is clicked,
    // window needs to be bind instead of the component.
    (0, _jquery2.default)(window).on(EVENTS_TO_LISTEN, function () {
        return component.tooltipHandler();
    });
};

Tooltip.prototype.tooltipHandler = function () {
    var component = this;

    //Act only when tooltip is visible.
    if (component.$element.hasClass(IS_VISIBLE_CLASS)) {
        var tooltip = component.getUpdateDescriptor();
        component.updateTooltip(tooltip);
    }
};

/**
 * Returns a description of what needs to be updated in the tooltip.
 * @returns {*} A descriptor object or undefined if no update needed.
 */
Tooltip.prototype.getUpdateDescriptor = function () {
    var component = this;
    var tooltip = void 0;

    if ((0, _jquery2.default)('body').outerWidth() < SMALL_BREAKPOINT) {
        return;
    }

    if (component.doesInitialTooltipFitInCurrentWindow()) {
        tooltip = {
            isBottom: component.initialInfo.isBottom,
            isReversed: component.initialInfo.isReversed
        };
    } else {
        tooltip = component.getCurrentTooltipDescriptor();
        var tooltipPos = component.getTooltipAbsolutePosition(tooltip);
        if (tooltipPos.bottom < 0 && tooltip.isBottom) {
            tooltip.isBottom = false;
        } else if (tooltipPos.top < 0 && !tooltip.isBottom) {
            tooltip.isBottom = true;
        }

        if (tooltipPos.left < 0 && tooltip.isReversed) {
            tooltip.isReversed = false;
        } else if (tooltipPos.right < 0 && !tooltip.isReversed) {
            tooltip.isReversed = true;
        }
    }

    return tooltip;
};

/**
 * Using the descriptor of the initial tooltip, checks if it fits in the current window.
 * If there is no initial descriptor, it stores the current one.
 * @returns {boolean} true if the initial tooltip fits.
 */
Tooltip.prototype.doesInitialTooltipFitInCurrentWindow = function () {
    var component = this;

    if (!component.initialInfo) {
        component.initialInfo = component.getCurrentTooltipDescriptor();
    }

    //How the initial tooltip would look in current window.
    var initialPos = component.getTooltipAbsolutePosition(component.initialInfo);

    return initialPos.top > 0 && initialPos.bottom > 0 && initialPos.left > 0 && initialPos.right > 0;
};

/**
 * Returns an object describing the relevant information of the current tooltip displayed.
 * The information is relative to the parent component, not to the window.
 * This way it can be used to compare it in different windows.
 * @returns {{isReversed: *, isBottom: *, width: Number, height: Number, topFromParent: Number, leftFromParent: Number}}
 */
Tooltip.prototype.getCurrentTooltipDescriptor = function () {
    var component = this;
    var tooltipStyle = window.getComputedStyle(component.$element[0], ':after');

    return {
        isReversed: component.$element.hasClass(EXPAND_TO_LEFT_CLASS),
        isBottom: component.$element.hasClass(EXPAND_TO_BOTTOM_CLASS),
        width: parseInt(tooltipStyle.width, 10),
        height: parseInt(tooltipStyle.height, 10),
        topFromParent: parseInt(tooltipStyle.top, 10),
        leftFromParent: parseInt(tooltipStyle.left, 10)
    };
};

/**
 * Given a tooltip descriptor, returns the boundaries of it in the current document.
 * @param tooltip descriptor.
 * @returns {{top: *, left: *, right: number, bottom: number}}
 */
Tooltip.prototype.getTooltipAbsolutePosition = function (tooltip) {
    var component = this;
    var $body = (0, _jquery2.default)('body');
    var top = component.$element.offset().top + tooltip.topFromParent;
    var left = component.$element.offset().left + tooltip.leftFromParent;
    var right = $body.width() - tooltip.width - left;
    var bottom = $body.height() - tooltip.height - top;

    return {
        top: top,
        left: left,
        right: right,
        bottom: bottom
    };
};

/**
 * Given a tooltip descriptor, updates the displayed tooltip.
 * @param tooltip descriptor.
 */
Tooltip.prototype.updateTooltip = function (tooltip) {
    var component = this;

    if ((typeof tooltip === 'undefined' ? 'undefined' : _typeof(tooltip)) !== 'object' || typeof tooltip.isBottom === 'undefined' || typeof tooltip.isReversed === 'undefined') {
        return;
    }

    if (tooltip.isBottom) {
        component.$element.addClass(EXPAND_TO_BOTTOM_CLASS);
    } else {
        component.$element.removeClass(EXPAND_TO_BOTTOM_CLASS);
    }

    if (tooltip.isReversed) {
        component.$element.addClass(EXPAND_TO_LEFT_CLASS);
    } else {
        component.$element.removeClass(EXPAND_TO_LEFT_CLASS);
    }
};

/**
 * Disables the on hover title default functionality (non-javascript fallback)
 */
Tooltip.prototype.removeTitle = function () {
    var component = this;

    var text = component.$element.attr(TITLE_ATTR);

    component.$element.attr(TITLE_ATTR, '');
    component.$element.attr(TOOLTIP_TEXT_ATTR, text);
};

(0, _jquery2.default)(COMPONENT_SELECTOR).each(function (i, element) {
    return new Tooltip(element);
});

},{"jquery":"jquery"}],326:[function(require,module,exports){
'use strict';

var $ = require('jquery');
var debounce = require('lodash/debounce');
var difference = require('lodash/difference');

module.exports = TopPosition;

var COMPONENT_ATTR = 'data-top-position';
var COMPONENT_SELECTOR = '[' + COMPONENT_ATTR + ']';
var CLICK_ATTR = 'data-top-position-click';
var CLICK_ATTR_SELECTOR = '[' + CLICK_ATTR + ']';
var ITEM_ATTR = 'data-top-position-item';
var ITEM_SELECTOR = '[' + ITEM_ATTR + ']';

function TopPosition(element) {
    var component = this;

    $(document).on('resultsUpdated', function () {
        $(COMPONENT_SELECTOR).each(function (index, elementOnUpdate) {
            component.init(elementOnUpdate);
        });
    });

    component.init(element);
}

TopPosition.prototype.init = function (element) {
    var component = this;
    component.element = element;
    component.$element = $(element);
    component.loggedEndpoints = [];
    component.endpoints = TopPosition.getEndPoints(element);
    component.logTracking(component.endpoints);

    component.bindEvents();
};

TopPosition.prototype.logTracking = function (endpoints) {
    var component = this;
    return endpoints.map(function (url) {
        component.loggedEndpoints.push(url);
        return $.ajax(url);
    });
};

TopPosition.prototype.bindEvents = function () {
    var component = this;

    $(CLICK_ATTR_SELECTOR).on('click', function (event) {
        var element = this;
        var url = element.getAttribute('href');
        event.preventDefault();
        $.ajax(element.getAttribute(CLICK_ATTR)).always(function () {
            return window.location = url;
        });
    });

    $(window).on('resize', debounce(function () {

        var endpoints = TopPosition.getEndPoints(component.element);
        var notLogged = difference(endpoints, component.loggedEndpoints);

        if (notLogged.length > 0) {
            component.logTracking(notLogged);
        }
    }, 200));
};

TopPosition.getValidElements = function (element) {
    return element.find(ITEM_SELECTOR + ':visible');
};

TopPosition.getEndPoints = function (element) {
    var $items = TopPosition.getValidElements($(element));
    var endpoints = [];

    $items.each(function (index, topPositionElement) {
        endpoints.push($(topPositionElement).attr(ITEM_ATTR));
    });
    return endpoints;
};

$(COMPONENT_SELECTOR).each(function (index, element) {
    return new TopPosition(element);
});

},{"jquery":"jquery","lodash/debounce":158,"lodash/difference":159}],327:[function(require,module,exports){
// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

// explicitly inject dependencies (alphabetically), only those needed

var $ = require('jquery');

// what does this module expose?
module.exports = UserLogin;

// component configuration
var COMPONENT_SELECTOR = '[data-user-login]';
var SUMBIT_FORM_SELECTOR = '[data-login-submit-form]';
var SUBMIT_BUTTON_SELECTOR = '[data-login-submit-button]';

function UserLogin(element) {

    var component = this;
    component.$element = $(element);

    //disable the submit button so doubleclicking if won't generate a anti-forgery exception
    $(SUMBIT_FORM_SELECTOR).submit(component.disableAfterAction);
}

UserLogin.prototype.disableAfterAction = function () {
    $(this).find(SUBMIT_BUTTON_SELECTOR).prop('disabled', true);
};

// turn all elements with the default selector into components
// note that this will only work for the login page and not the login dialog
// because the login dialog is loaded into the dom at a later point
// this is the intended behaviour
$(COMPONENT_SELECTOR).each(function (index, element) {
    return new UserLogin(element);
});

},{"jquery":"jquery"}],328:[function(require,module,exports){
'use strict';

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

var _appUtilsCookies = require('../app-utils/app-utils-cookies');

var _appUtilsCookies2 = _interopRequireDefault(_appUtilsCookies);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

module.exports = UserMyHouseDatasource;

// component configuration
var COMPONENT_SELECTOR = '[data-my-house-datasource]';
var REQUEST_URL = 'data-datasource-request-url';
var JWT_KEY = 'data-datasource-jwt';
var PERIOD_DROPDOWN_SELECTOR = '[data-my-house-datasource-dropdown]';
var FORM_SELECTOR = '[data-my-house-datasource-form]';
var GLOBALID = 'data-datasource-globalid';
var SLASH = 'data-datasource-remove-slash';
var ERROR_MESSAGE_CLASS = '.graph-no-data';

var PREFERRED_PERIOD_COOKIE_NAME = 'mijn_PreferredStatsPeriod';

UserMyHouseDatasource.instance = null;

/**
 * Constructor method, links child elements to variables for internal use
 * @param {HTMLElement} element     The HTML element to bind to.
 */
function UserMyHouseDatasource(element) {
    var component = this;
    component.$element = (0, _jquery2.default)(element);
    component.$selectGraphDataForm = component.$element.find(FORM_SELECTOR);
    component.dataObservers = [];
    component.requestUrl = component.$element.attr(REQUEST_URL);
    component.issuedJwt = component.$element.attr(JWT_KEY);
    component.globalId = component.$element.attr(GLOBALID);
    component.trailingSlash = component.$element[0].hasAttribute(SLASH);
    component.selectedPeriod = null;

    var preferredPeriod = component.getValidPeriodCookie();
    if (preferredPeriod) {
        component.$element.find('select').val(preferredPeriod);
    }

    component.$selectGraphDataForm.on('change', function () {
        (0, _jquery2.default)(ERROR_MESSAGE_CLASS).remove();
        component.selectedInputChanged(component.$selectGraphDataForm);
    });

    (0, _jquery2.default)(document).ready(function () {
        return component.loadData();
    });
}

/**
 * Check if all observers are loaded. If so, start loading data, if not, retry in 100ms.
 */
UserMyHouseDatasource.prototype.loadData = function () {
    var component = this;
    var allObserversLoaded = true;

    component.foreachObservers(function (observer) {
        return function () {
            if (!observer.isLoaded.apply(this)) {
                allObserversLoaded = false;
            }
        };
    });

    if (!allObserversLoaded) {
        setTimeout(component.loadData.bind(component), 100);
    } else {
        // load the periods dropdown
        component.requestPeriodsForDropdown()
        // then trigger select of default option
        .then(function () {
            return component.selectedInputChanged(component.$selectGraphDataForm);
        });
    }
};

/**
 * Get the selected input from the form.
 * @param {HTMLElement} form
 */
UserMyHouseDatasource.prototype.selectedInputChanged = function (form) {
    var component = this;
    var $form = (0, _jquery2.default)(form);

    component.selectedPeriod = $form.find(':selected');
    component.selectedType = $form.find(':checked').val();

    _appUtilsCookies2.default.setCookie(PREFERRED_PERIOD_COOKIE_NAME, component.selectedPeriod.val());

    component.requestStatisticsData();
};

/**
 * Load dropdown periods data using a request with authorization.
 * @returns {*}     Select (dropdown) element.
 */
UserMyHouseDatasource.prototype.requestPeriodsForDropdown = function () {
    var component = this;
    var requestUrl = component.createDropdownRequestUrl(component.requestUrl, component.globalId, component.trailingSlash);

    var requestOptions = {
        url: requestUrl,
        headers: {
            Authorization: 'Bearer ' + component.issuedJwt
        },
        xhrFields: {
            withCredentials: true
        },
        dataType: 'json'
    };

    component.foreachObservers(function (observer) {
        return observer.onDataLoading;
    });

    return _jquery2.default.ajax(requestOptions).done(function (response, statusText, xhr) {
        if (UserMyHouseDatasource.isTokenExpired(xhr)) {
            component.refreshToken(component.requestPeriodsForDropdown);
        } else if (UserMyHouseDatasource.isSuccesfulResponse(response)) {
            component.createOptionsForDropdown(response);
        } else {
            onFail(response);
        }
    }).fail(function (response) {
        return onFail(response);
    });

    function onFail(response) {
        component.foreachObservers(function (observer) {
            return observer.onDataLoadError;
        }, response);
    }
};

/**
 * Create the options and insert them in the dropdown element.
 * @param {Object[]} response - Request response.
 */
UserMyHouseDatasource.prototype.createOptionsForDropdown = function (response) {
    var component = this;
    var $dropdown = (0, _jquery2.default)(PERIOD_DROPDOWN_SELECTOR);

    Object.keys(response).forEach(function (key) {
        var $element = $dropdown.find('[value=\'' + response[key].Label + '\']');

        if (!$element.length) {
            $element = (0, _jquery2.default)('<option></option>').text(response[key].Label);
            $dropdown.find('option').eq(key).before($element);
        }

        $element = $element.data('dateFrom', response[key].DateFrom).data('dateTo', response[key].DateTo).data('format', response[key].Format).data('interval', response[key].Interval);

        if (!component.getValidPeriodCookie() && response[key].IsSelected) {
            $element.attr('selected', 1);
        }
    });

    //remove options that existed in funda.website, but where not provided by the statistics api
    $dropdown.find('option').each(function (key, value) {
        if (!(0, _jquery2.default)(value).data('dateFrom')) {
            (0, _jquery2.default)(value).remove();
        }
    });
};

/**
 * Load statistics data using a request with authorization.
 * @returns {*}     Statistics request
 */
UserMyHouseDatasource.prototype.requestStatisticsData = function () {
    var component = this;
    var requestUrl = component.createStatisticsRequestUrl(component.requestUrl, component.selectedType, component.globalId, component.trailingSlash);
    var requestOptions = {
        url: requestUrl,
        dataType: 'json'
    };

    if (component.issuedJwt) {
        requestOptions.headers = {
            Authorization: 'Bearer ' + component.issuedJwt
        };
        requestOptions.xhrFields = {
            withCredentials: true
        };
    }

    component.foreachObservers(function (observer) {
        return observer.onDataLoading;
    });

    return _jquery2.default.ajax(requestOptions).done(function (response, statusText, xhr) {
        if (UserMyHouseDatasource.isTokenExpired(xhr)) {
            component.refreshToken(component.requestStatisticsData);
        } else if (UserMyHouseDatasource.isSuccesfulResponse(response)) {
            component.foreachObservers(function (observer) {
                return observer.onDataLoaded;
            }, response, component.selectedPeriod, component.selectedType);
        } else {
            onFail(response);
        }
        component.isLoading = false;
    }).fail(function (response) {
        return onFail(response);
    });

    function onFail(response) {
        component.foreachObservers(function (observer) {
            return observer.onDataLoadError;
        }, response);
    }
};

/**
 * Create the request URL based on the selected period.
 * @returns {string}    Request URL
 */
UserMyHouseDatasource.prototype.createStatisticsRequestUrl = function (requestUrl, selectedType, globalId, trailingSlash) {
    var component = this;
    var fromDate = UserMyHouseDatasource.formatDate(component.selectedPeriod.data('dateFrom'));
    var toDate = UserMyHouseDatasource.formatDate(component.selectedPeriod.data('dateTo'));
    var partialUrl = [requestUrl, selectedType, globalId, fromDate, toDate].join('/');

    return [partialUrl, trailingSlash === false ? '/' : ''].join('');
};

/**
 * Create the URL to request the correct data.
 * @param {string} requestUrl
 * @param {number} globalId
 * @param {boolean} trailingSlash
 * @returns {string}
 */
UserMyHouseDatasource.prototype.createDropdownRequestUrl = function (requestUrl, globalId, trailingSlash) {
    return [requestUrl, '/getgraphperiods/', globalId, trailingSlash === false ? '/' : ''].join('');
};

/**
 * Loop through each observer and call the {@link action} parameter with any provided {@link arguments}
 * @param {function} action - the action to call on the observer
 * @param {*} [arguments] - arguments passed to the {@link action} function
 */
UserMyHouseDatasource.prototype.foreachObservers = function (action) {
    var component = this;
    var parameters = [].slice.call(arguments).splice(1);

    Object.keys(this.dataObservers).forEach(function (observer) {
        action(component.dataObservers[observer]).apply(component.dataObservers[observer].context, parameters);
    });
};

/**
 * Register an observer for the dataChanges events
 * @param {Object} observer - Object with observed function objects.
 */
UserMyHouseDatasource.prototype.registerDataChanged = function (observer) {
    this.dataObservers.push(observer);
};

/**
 * Refresh the users' access-token when expired.
 * @param callback      requestStatisticsData function
 * @returns {*}         Refresh token request
 */
UserMyHouseDatasource.prototype.refreshToken = function (callback) {
    var component = this;

    return _jquery2.default.ajax(component.refreshJwtUrl).done(function (response) {
        if ('Token' in response && response.Token !== '') {
            component.issuedJwt = response.Token;
            callback();
        }
    });
};

/**
 * Gets preferred period from cookie and performs a validation.
 * @returns The
 */
UserMyHouseDatasource.prototype.getValidPeriodCookie = function () {
    var component = this;
    var cookieValue = _appUtilsCookies2.default.getCookie(PREFERRED_PERIOD_COOKIE_NAME);

    //Check if the cookie value is a valid option.
    var optionElements = component.$element.find('option');
    for (var i = 0; i < optionElements.length; i++) {
        if (optionElements[i].value === cookieValue) {
            return cookieValue;
        }
    }
};

/**
 * Format a date to the correct format for the request URL.
 * @param date
 * @returns {string}
 */
UserMyHouseDatasource.formatDate = function (date) {
    var rawDate = new Date(date);
    var format = function zeroFormat(number) {
        return number < 10 ? '0' + number : number;
    };

    return [rawDate.getFullYear(), format(rawDate.getMonth() + 1), format(rawDate.getDate())].join('');
};

/**
 * Check if token is expired
 * @param {object} xhr - The request.
 * @returns {boolean}   Is token expired boolean
 */
UserMyHouseDatasource.isTokenExpired = function (xhr) {
    return xhr.status === 498;
};

/**
 * Check if response is successful.
 * @param {object[]} response - Request response.
 * @returns {boolean}   Is request successful boolean
 */
UserMyHouseDatasource.isSuccesfulResponse = function (response) {
    return response.Result !== 'undefined';
};

// turn all elements with the default selector into components
(0, _jquery2.default)(COMPONENT_SELECTOR).each(function (index, element) {
    if (UserMyHouseDatasource.instance) {
        throw 'UserMyHouseDatasource.instance has already been set.';
    }
    UserMyHouseDatasource.instance = new UserMyHouseDatasource(element);
});

},{"../app-utils/app-utils-cookies":203,"jquery":"jquery"}],329:[function(require,module,exports){
'use strict';

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

var _graph = require('../graph/graph');

var _graph2 = _interopRequireDefault(_graph);

var _userMyHouseDatasource = require('../user-my-house-datasource/user-my-house-datasource');

var _userMyHouseDatasource2 = _interopRequireDefault(_userMyHouseDatasource);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

module.exports = UserMyHouseGraph;

// component configuration
var COMPONENT_SELECTOR = '[data-my-house-graph]';
var GRAPH_SELECTOR = '[data-graph]';
var CHART_ASSET_BASE = 'data-graph-asset-base';
var YAXIS_LABEL_SELECTOR = 'data-y-axis-label-';
var HIGHCHARTS_CONTAINER = '.highcharts-container';
var ERROR_MESSAGE_CLASS = '.graph-no-data';
var ERROR_MESSAGE_SELECTOR = '[data-graph-error-message-template]';
var TRANSLATION_DAY_LABEL = 'data-translation-day-label';

/**
 * Constructor method, links child elements to variables for internal use
 * @param {HTMLElement} element     The HTML element to bind to.
 */
function UserMyHouseGraph(element) {
    var component = this;
    component.$element = (0, _jquery2.default)(element);
    var $graphElement = component.$element.find(GRAPH_SELECTOR);
    var $assetBaseUrl = $graphElement.attr(CHART_ASSET_BASE);
    var graphOptions = {
        setData: function setData(targetData, receivedData) {
            return this.setData(targetData, receivedData);
        }
    };

    if ($graphElement.length !== 1) {
        return;
    }

    if ($assetBaseUrl) {
        graphOptions.assetBaseUrl = this.assetBaseUrl;
    } else {
        return;
    }

    this.graph = new _graph2.default($graphElement, graphOptions);

    // Register loading events at datasource observer
    _userMyHouseDatasource2.default.instance.registerDataChanged({
        onDataLoading: UserMyHouseGraph.loadingData,
        onDataLoaded: UserMyHouseGraph.loadData,
        onDataLoadError: UserMyHouseGraph.loadError,
        isLoaded: UserMyHouseGraph.isLoaded,
        context: this
    });
}

/**
 * return if this component is loaded (used by user-my-house-datasource)
 * @returns {*|boolean}     returns graph object, If component is loaded.
 */
UserMyHouseGraph.isLoaded = function () {
    return this.graph && this.graph.componentLoaded;
};

/**
 * Using the selected option format, set the data to the correct format and update the graph.
 * @param {object[]} rawData - Data received from the statistics API.
 * @param {array[]} period - Selected option from the dropdown.
 * @param {string} type - Type of axis label.
 */
UserMyHouseGraph.loadData = function (rawData, period, type) {
    var component = this;
    var selectedOptionFormat = period.data('format');

    if (selectedOptionFormat === 'Date') {
        component.setToDates(rawData.GraphData);
    } else if (selectedOptionFormat === 'DayNumber') {
        component.setToIndex(rawData.GraphData);
    }

    component.updateAxisInterval(period);
    component.updateAxisFormat(period, type);

    component.graph.processData();
};

/**
 * Transform raw data into the correct Highcharts format using the index number as the 'x' values.
 * @param {string[]} rawData
 * @returns {{Viewed: [], Flags: [], Products: []}}
 */
UserMyHouseGraph.prototype.setToIndex = function (rawData) {
    var getDay = function getDay(date) {
        return new Date(date).getDate();
    };

    this.graph.data = {
        viewed: rawData.Viewed.map(function (item) {
            return [rawData.Viewed.indexOf(item) + 1, item.Value];
        }),
        flags: rawData.Flags.map(function (item) {
            return {
                x: item.Date = getDay(item.Date),
                y: 0, title: 'flags'
            };
        }),
        products: rawData.Products.map(function (item) {
            return {
                x: rawData.Products.indexOf(item) + 1,
                y: 0, title: item.Value, name: 'products'
            };
        }),
        weekends: rawData.Weekends
    };
};

/**
 * Transform raw data into the correct Highcharts format using dates as the 'x' values.
 * @param {string[]} rawData
 * @returns {{Viewed: [], Flags: [], Products: []}}
 */
UserMyHouseGraph.prototype.setToDates = function (rawData) {
    var isoDate = function isoDate(date) {
        return new Date(date).getTime();
    };

    // Transform all date formats.
    Object.keys(rawData).forEach(function (segment) {
        rawData[segment].forEach(function (item) {
            return item.Date = isoDate(item.Date);
        });
    });

    this.graph.data = {
        viewed: rawData.Viewed.map(function (item) {
            return [item.Date, item.Value];
        }),
        flags: rawData.Flags.map(function (item) {
            return {
                x: item.Date,
                y: 0, title: 'flags'
            };
        }),
        products: rawData.Products.map(function (item) {
            return {
                x: item.Date,
                y: 0,
                title: item.Value,
                name: 'products'
            };
        }),
        weekends: rawData.Weekends
    };
};

/**
 * Return the correct axis label when switching between statistics types.
 * @param {string} type     Type of statistics selected.
 * @returns {string}        Axis label string.
 */
UserMyHouseGraph.prototype.setAxisLabel = function (type) {
    var component = this;
    var axisLabels = {};

    _jquery2.default.each(component.$element.get(0).attributes, function (i, attr) {
        var attributeName = attr.name;
        var labelName = attr.value;

        if (attributeName.indexOf(YAXIS_LABEL_SELECTOR) === 0) {
            attributeName = attributeName.substring(attributeName.lastIndexOf('-') + 1);
            axisLabels[attributeName] = labelName;
        }
    });

    return axisLabels[type];
};

/**
 * Set the format for the x-axis based on the selected option.
 * @param {array} period        Selected option from the period dropdown.
 * @param {string} type         Type of statistics selected.
 */
UserMyHouseGraph.prototype.updateAxisFormat = function (period, type) {
    var component = this;
    var selectedOptionFormat = period.data('format');

    // Update the Y-axis label when switching type of statistics.
    component.graph.chart.yAxis[0].update({
        title: {
            text: component.setAxisLabel(type)
        }
    });

    if (selectedOptionFormat === 'Date') {
        component.graph.chart.xAxis[0].update({
            type: 'datetime',
            labels: {
                format: undefined
            }
        }, false);
    } else if (selectedOptionFormat === 'DayNumber') {
        component.graph.chart.xAxis[0].update({
            tickInterval: 7,
            type: 'linear',
            labels: {
                format: component.$element.attr(TRANSLATION_DAY_LABEL)
            }
        }, false);
    }
};

/**
 *  Set the interval for the x-axis based on the selected option.
 * @param {array} period        Selected option from the period dropdown.
 */
UserMyHouseGraph.prototype.updateAxisInterval = function (period) {
    var component = this;
    var dayTick = 24 * 3600 * 1000;
    var selectedOptionFormat = period.data('interval');
    var interval = void 0;

    if (selectedOptionFormat === 'Daily') {
        interval = dayTick;
    } else if (selectedOptionFormat === 'Weekly') {
        interval = 7 * dayTick;
    } else if (selectedOptionFormat === 'Monthly') {
        interval = 30 * dayTick;
    } else if (selectedOptionFormat === 'Yearly') {
        interval = 365 * dayTick;
    }

    component.graph.chart.xAxis[0].update({
        tickInterval: interval
    }, false);
};

/**
 * Tell all related components data is being loaded.
 */
UserMyHouseGraph.loadingData = function () {
    this.graph.setLoading(true);
};

/**
 * Show user something went wrong.
 */
UserMyHouseGraph.loadError = function () {
    var component = this;
    var $errorElement = (0, _jquery2.default)(ERROR_MESSAGE_CLASS);
    var $errorElementTemplate = (0, _jquery2.default)(ERROR_MESSAGE_SELECTOR).html().trim();

    // Don't append if it already exists.
    if ($errorElement.length === 0) {
        (0, _jquery2.default)(HIGHCHARTS_CONTAINER).append($errorElementTemplate);
    }

    // Always stop showing the spinner.
    component.graph.setLoading(false);
};

// turn all elements with the default selector into components
(0, _jquery2.default)(COMPONENT_SELECTOR).each(function (index, element) {
    return new UserMyHouseGraph(element);
});

},{"../graph/graph":240,"../user-my-house-datasource/user-my-house-datasource":328,"jquery":"jquery"}],330:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = UserMyHouseHeader;

// component configuration

var COMPONENT_SELECTOR = '[data-my-house-header]';
var DROPDOWN_SELECTOR = '[data-my-house-header-dropdown]';
var URL_PATH_BASE_ATTR = 'data-my-house-base-url';

/**
 * Constructor method, links child elements to variables for internal use
 * @param {HTMLElement} element     The HTML element to bind to.
 */
function UserMyHouseHeader(element) {
    var component = this;
    component.$element = (0, _jquery2.default)(element);
    component.$dropdown = component.$element.find(DROPDOWN_SELECTOR);
    component.urlPathBase = component.$dropdown.attr(URL_PATH_BASE_ATTR);
    component.selectedOptionValue = component.getSelectedOptionValue();

    component.bindEvents();
}

/**
 * Bind component events.
 * Listen for changes on the dropdown.
 */
UserMyHouseHeader.prototype.bindEvents = function () {
    var component = this;

    component.$dropdown.on('change', function () {
        component.onChange();
    });
};

/**
 * Gets path to redirect to and redirects to that path
 */
UserMyHouseHeader.prototype.onChange = function () {
    var component = this;

    window.location.pathname = component.getPathToRedirect();
};

/**
 * Returns the value from the selected option in the dropdown.
 * @returns {string}        Value of the selected option. Should be global id.
 */
UserMyHouseHeader.prototype.getSelectedOptionValue = function () {
    var component = this;
    component.selectedOptionValue = component.$dropdown.val();

    return component.selectedOptionValue;
};

/**
 * Returns the complete path to redirect to.
 * @returns {string}        Should be a complete URL.
 */
UserMyHouseHeader.prototype.getPathToRedirect = function () {
    var component = this;

    return component.urlPathBase + component.getSelectedOptionValue() + '/';
};

// turn all elements with the default selector into components
(0, _jquery2.default)(COMPONENT_SELECTOR).each(function (index, element) {
    return new UserMyHouseHeader(element);
});

},{"jquery":"jquery"}],331:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = UserMyHouseOptions;

// component configuration

var COMPONENT_SELECTOR = '[data-my-house-options]';
var URL_STRING_ATTR = 'data-url-string';
var URL_STRING_SELECTOR = '[' + URL_STRING_ATTR + ']';
var REPORT_HOUSE_DIALOG_SELECTOR = '[data-dialog-handle]';

/**
 * Constructor method, links child elements to variables for internal use
 *
 * @param {HTMLElement} element The Html element to bind to
 */
function UserMyHouseOptions(element) {
    var component = this;
    component.$element = (0, _jquery2.default)(element);
    component.urlAttribute = component.$element.find(URL_STRING_SELECTOR);
    component.urlValue = component.urlAttribute[0].getAttribute(URL_STRING_ATTR);
    component.$link = (0, _jquery2.default)(REPORT_HOUSE_DIALOG_SELECTOR);

    // Check if url comes from an email link.
    if (component.checkUrl()) {

        // Trigger the click event for the user. This shows the dialog tof the user.
        component.$link.trigger('click');
    }
}

/**
 * Check if the url contains the correct string.
 * @returns {boolean}
 */
UserMyHouseOptions.prototype.checkUrl = function () {
    var component = this;
    var url = window.location.href;

    return url.indexOf(component.urlValue) > -1;
};

(0, _jquery2.default)(COMPONENT_SELECTOR).each(function (index, element) {
    return new UserMyHouseOptions(element);
});

},{"jquery":"jquery"}],332:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

var _userMyHouseDatasource = require('../user-my-house-datasource/user-my-house-datasource');

var _userMyHouseDatasource2 = _interopRequireDefault(_userMyHouseDatasource);

var _thousandSeparator = require('../thousand-separator/thousand-separator');

var _thousandSeparator2 = _interopRequireDefault(_thousandSeparator);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = UserMyHouseStatistics;

// component configuration

var COMPONENT_SELECTOR = '[data-my-house-statistics]';
var CELL_SELECTOR = 'data-my-house-cell-data';
var CELL_SELECTOR_ATTR = '[' + CELL_SELECTOR + ']';
var CELL_TOTAAL_SELECTOR = '#user-my-house-table-header-total';
var THOUSAND_SEPARATOR = 'data-thousand-separator';
var NO_DATA_CLASS = 'no-data';

function UserMyHouseStatistics(element) {
    var component = this;
    this.$element = (0, _jquery2.default)(element);
    this.loaded = false;
    this.$cells = this.$element.find(CELL_SELECTOR_ATTR);
    this.$cellTotal = this.$element.find(CELL_TOTAAL_SELECTOR);
    this.thousandSeperator = this.$element.attr(THOUSAND_SEPARATOR);

    // Observe the data loading events
    _userMyHouseDatasource2.default.instance.registerDataChanged({
        onDataLoading: component.loadingTableData,
        onDataLoaded: component.populateTable,
        onDataLoadError: component.loadError,
        isLoaded: component.isLoaded,
        context: component
    });

    (0, _jquery2.default)(document).ready(function () {
        component.loaded = true;
    });
}

/**
 * return if this component is loaded (used by user-my-house-datasource)
 * @returns {boolean}       If component is loaded.
 */
UserMyHouseStatistics.prototype.isLoaded = function () {
    return this.loaded;
};

/**
 * Populate the table with the received data.
 * @param {object} rawData          Raw data received from the API.
 * @param {object} selectedPeriod   Selected option from the period dropdown.
 */
UserMyHouseStatistics.prototype.populateTable = function (rawData, selectedPeriod) {
    var receivedData = rawData.TableData;
    var component = this;

    // Remove NO_DATA_CLASS if it exists.
    if (this.$element.hasClass(NO_DATA_CLASS)) {
        this.$element.removeClass(NO_DATA_CLASS);
    }

    this.$cellTotal.html((0, _jquery2.default)(selectedPeriod).html());

    if (receivedData.hasOwnProperty) {
        this.$cells.each(function (index, element) {
            var data = element.getAttribute(CELL_SELECTOR).split('.');
            if (data[1] == 'Conversion') {
                element.innerHTML = receivedData[data[0]][data[1]] + '%';
            } else {
                element.innerHTML = _thousandSeparator2.default.format(receivedData[data[0]][data[1]], component.thousandSeperator);
            }
        });
    }
};

/**
 * When loading data resulted in error
 */
UserMyHouseStatistics.prototype.loadError = function () {
    // Hide table when error occurred
    this.$element.addClass(NO_DATA_CLASS);
};

/**
 * Tell all related components data is being loaded.
 */
UserMyHouseStatistics.prototype.loadingTableData = function () {};

// turn all elements with the default selector into components
(0, _jquery2.default)(COMPONENT_SELECTOR).each(function (index, element) {
    return new UserMyHouseStatistics(element);
});

},{"../thousand-separator/thousand-separator":322,"../user-my-house-datasource/user-my-house-datasource":328,"jquery":"jquery"}],333:[function(require,module,exports){
// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

// explicitly inject dependencies (alphabetically), only those needed

var $ = require('jquery');

// what does this module expose?
module.exports = UserMyLoggedInAs;

// component configuration
var COMPONENT_SELECTOR = '[data-user-my-logged-in-as]';
var BUTTON_SELECTOR = 'button';
var HAS_WRITE_ACCESS_CLASS = 'has-write-access';
var HAS_READONLY_ACCESS_CLASS = 'has-readonly-access';
var NOTIFY_CLASS = '.notify';
var REQUEST_WRITE_CLASS = 'request-write';
var DISABLE_WRITE_URL = '/mijn/fundamanagerlogin/declinewrite/';
var ENABLE_WRITE_URL = '/mijn/fundamanagerlogin/requestwrite/';

function UserMyLoggedInAs(element) {
    var component = this;
    var $element = $(element);
    component.$element = $element;
    component.button = $element.find(BUTTON_SELECTOR);
    component.hasWriteAccess = $element.hasClass(HAS_WRITE_ACCESS_CLASS);

    component.$element.on('click', BUTTON_SELECTOR, function () {
        var requestWrite = $(this).parents(NOTIFY_CLASS).hasClass(REQUEST_WRITE_CLASS);
        component.grantUsersWishes(requestWrite);
    });

    // annoy logged in user
    if (component.hasWriteAccess) {
        var result = confirm('Wil je de schrijfrechten behouden voor dit account?\n\nActies en veranderingen aan dit account worden wel opgeslagen.');
        if (!result) {
            component.doRequest(DISABLE_WRITE_URL, function () {
                component.switchWriteAccess(false);
            });
        }
    }
}

UserMyLoggedInAs.prototype.grantUsersWishes = function (isRequestForWriteAccess) {
    var component = this;

    if (isRequestForWriteAccess) {
        component.doRequest(ENABLE_WRITE_URL, function () {
            component.switchWriteAccess(true);
        }, component.getLapText(true));
    } else {
        component.doRequest(DISABLE_WRITE_URL, function () {
            component.switchWriteAccess(false);
        }, component.getLapText(false));
    }
};

UserMyLoggedInAs.prototype.switchWriteAccess = function (hasWriteAccess) {
    var component = this;

    component.hasWriteAccess = hasWriteAccess;
    if (hasWriteAccess) {
        component.$element.removeClass(HAS_READONLY_ACCESS_CLASS).addClass(HAS_WRITE_ACCESS_CLASS);
    } else {
        component.$element.removeClass(HAS_WRITE_ACCESS_CLASS).addClass(HAS_READONLY_ACCESS_CLASS);
    }
};

UserMyLoggedInAs.prototype.doRequest = function (url, callback, requestText) {
    if (requestText !== undefined && requestText !== null) {
        var result = confirm(requestText);
        if (!result) {
            return false;
        }
    }
    $.post(url, function () {
        if (callback !== undefined) {
            callback();
        }
    });
};

UserMyLoggedInAs.prototype.getLapText = function (isForSure) {
    var returnValue = '';
    if (isForSure) {
        returnValue = 'Weet je zeker dat je de schrijfrechten wilt inschakelen voor dit account?\n\nLet op! ';
        returnValue += 'Acties en veranderingen (zoals bekeken woningen) worden echt opgeslagen en zijn dus zichtbaar voor de mijn funda gebruiker! ';
        returnValue += 'Deze functionaliteit is alleen toegestaan in opdracht van de eigenaar van het “Mijn funda” account. ';
        returnValue += 'In het geval een verzoek tot wijziging niet afkomstig is van de eigenaar zelf (maar bijv. van de makelaar) ';
        returnValue += 'dan altijd checken of hij/zij hiervoor toestemming heeft van de eigenaar.\n\nBij twijfel altijd eerst overleggen met je manager!';
    } else {
        returnValue = 'Wil je de schrijfrechten uitschakelen voor dit account?\n\nActies en veranderingen worden dan niet meer opgeslagen.';
    }
    return returnValue;
};

// turn all elements with the default selector into components
$(COMPONENT_SELECTOR).each(function (index, element) {
    return new UserMyLoggedInAs(element);
});

},{"jquery":"jquery"}],334:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = UserReportForm;

// component configuration

var COMPONENT_SELECTOR = '[data-user-report-form]';
var ERROR_MESSAGE_SELECTOR = '[data-user-report-form-error-message]';
var INPUT_SELECTOR = '[data-user-report-form-input]';
var INPUT_ERROR_CLASS = 'input-validation-error';
var TEXTAREA_SELECTOR = '[data-user-report-form-input-message]';
var FORM_SELECTOR = '[data-user-report-form-form]';
var CHECKBOX_OTHER_ATTR = 'data-user-report-form-other';
var HIDDEN_CLASS = 'is-hidden';
var INPUT_TYPE = '[type="checkbox"]';

/**
 * Constructor method, links child elements to variables for internal use
 * @param {HTMLElement} element     The HTML element to bind to.
 */
function UserReportForm(element) {
    var component = this;
    component.$element = (0, _jquery2.default)(element);
    component.$errorMessage = (0, _jquery2.default)(ERROR_MESSAGE_SELECTOR);
    component.$inputs = component.$element.find(INPUT_SELECTOR);
    component.$form = component.$element.find(FORM_SELECTOR);
    component.$textArea = component.$element.find(TEXTAREA_SELECTOR);
    component.$checkboxes = [].slice.call(document.querySelectorAll(INPUT_TYPE));
    component.checkboxOther = component.$element.attr(CHECKBOX_OTHER_ATTR);

    component.$inputs.on('change', function () {
        return component.onChangeHandler();
    });
    component.$textArea.on('keypress', function () {
        return component.onChangeHandler();
    });
    component.$form.on('submit', function (event) {
        return component.onSubmitHandler(event);
    });
}

/**
 * Handle changes in the form.
 */
UserReportForm.prototype.onChangeHandler = function () {
    var component = this;

    component.$errorMessage.addClass(HIDDEN_CLASS);
    component.$textArea.removeClass(INPUT_ERROR_CLASS);

    component.setFormState();
};

/**
 * Check form validation on submit.
 * @param {event} event     Submit click event.
 */
UserReportForm.prototype.onSubmitHandler = function (event) {
    var component = this;

    if (!component.validateForm()) {
        event.preventDefault();
    }
};

/**
 * Set the form state depending on the option selected.
 */
UserReportForm.prototype.setFormState = function () {
    var component = this;
    var formState = component.getFormState();

    formState.ReportOther ? component.$textArea.removeClass(HIDDEN_CLASS) : component.$textArea.addClass(HIDDEN_CLASS);
};

/**
 * Get the serialized form state.
 */
UserReportForm.prototype.getFormState = function () {
    var component = this;
    var serializedForm = component.serializeCheckboxes(component.$checkboxes);

    return serializedForm;
};

/**
 * Validate the form on submit.
 *
 * We only check:
 *  - if at least 1 checkbox was checked.
 *  - if the last checkbox is check AND a message was written in the textarea.
 *
 *  Other, more complex, validation is done by BE.
 *
 * @returns {boolean}       Does the form validate?
 */
UserReportForm.prototype.validateForm = function () {
    var component = this;
    var formState = component.getFormState();
    var checkboxes = component.$checkboxes;
    var textAreaMessage = component.$textArea.val();
    var atLeastOneChecked = checkboxes.some(function (checkbox) {
        return checkbox.checked;
    });

    // None are checked.
    if (!atLeastOneChecked) {
        component.$errorMessage.removeClass(HIDDEN_CLASS);
        return false;
    }

    // Last option is checked but there's no text.
    if (formState.ReportOther && textAreaMessage.length === 0) {
        component.$errorMessage.removeClass(HIDDEN_CLASS);
        component.$textArea.addClass(INPUT_ERROR_CLASS);
        return false;
    }

    component.$errorMessage.addClass(HIDDEN_CLASS);
    return true;
};

/**
 * Serializes the checked state and name of each checkbox element into an object.
 *
 * Object example:
 * {
 *     'TaalgebruikMelding': true
 * }
 *
 * @param  {object} checkboxes      All checkboxes within the form.
 * @return {Object}                 The serialized object
 */
UserReportForm.prototype.serializeCheckboxes = function (checkboxes) {
    return checkboxes.reduce(function (mapping, checkbox) {
        mapping[checkbox.name] = checkbox.checked;
        return mapping;
    }, {});
};

// turn all elements with the default selector into components
(0, _jquery2.default)(COMPONENT_SELECTOR).each(function (index, element) {
    return new UserReportForm(element);
});

},{"jquery":"jquery"}],335:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

var _ajax = require('../ajax/ajax');

var _ajax2 = _interopRequireDefault(_ajax);

var _clone = require('lodash/clone');

var _clone2 = _interopRequireDefault(_clone);

var _debounce = require('lodash/debounce');

var _debounce2 = _interopRequireDefault(_debounce);

var _appSpinner = require('../app-spinner/app-spinner');

var _appSpinner2 = _interopRequireDefault(_appSpinner);

var _expandible = require('../expandible/expandible');

var _expandible2 = _interopRequireDefault(_expandible);

var _userReview = require('../user-review/user-review');

var _userReview2 = _interopRequireDefault(_userReview);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = UserReviewPaging;

// component configuration

var COMPONENT_SELECTOR = '[data-user-review-paging]';
var PAGINATION_SELECTOR = '[data-pagination]';
var OUTPUT_KEY_ATTR = 'data-instant-search-output';
var OUTPUT_SELECTOR = '[data-instant-search-output]';
var PAGINATION_INPUT_SELECTOR = '[data-pagination-input-next]';
var SPINNER_SELECTOR = '[data-user-reviews-spinner]';
var UPDATING_CLASS = 'is-updating';
var MINIMUM_INTERVAL_BETWEEN_REQUESTS = 250; // ms
var PUSH_STATE_MARKER = 'pust_state_set_by_funda';

/**
 * Constructor method, links child elements to variables for internal use
 * @param {HTMLElement} element     The HTML element to bind to.
 */
function UserReviewPaging(element) {
    var ajaxDependency = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : _ajax2.default;

    var component = this;
    var $form = (0, _jquery2.default)(element);
    component.form = element;
    component.$form = $form;
    component.asyncResult = {};
    component.$currentPage = component.$form.find(PAGINATION_INPUT_SELECTOR);
    component.formData = {};
    component.outputMap = {};
    component.$outputs = UserReviewPaging.getOutputs(component.$form);
    component.spinner = new _appSpinner2.default(SPINNER_SELECTOR);
    component.ajax = ajaxDependency;

    // Create our outputs object.
    component.$outputs.each(function (index, output) {
        var key = output.getAttribute(OUTPUT_KEY_ATTR);
        var $outputs = component.outputMap[key] || (0, _jquery2.default)();
        component.outputMap[key] = $outputs.add(output);
    });

    (0, _jquery2.default)(document).on('isUpdatingReviews', function () {
        component.$outputs.addClass(UPDATING_CLASS);

        if (!component.spinner.isVisible()) {
            component.spinner.show();
        }
    });

    (0, _jquery2.default)(document).on('resultsUpdated resultsError', function () {
        component.$outputs.removeClass(UPDATING_CLASS);
        component.spinner.hide();
    });

    // Bind all events.
    component.bindToEvents();

    component.formData = component.$form.serializeArray();

    // initialize popstate
    if ('replaceState' in window.history) {
        window.history.replaceState(PUSH_STATE_MARKER, window.title);
    }
}

/**
 * Request form submit on form input changes.
 */
UserReviewPaging.prototype.bindToEvents = function () {
    var component = this;

    window.addEventListener('popstate', onpopstate, false);

    // reload page on browser back only if we have set the state. Safari fires the popstate on pageload (with an empty event state), and we don't want to reload on pageload :)
    function onpopstate(event) {
        if (event.state === PUSH_STATE_MARKER) {
            window.location.reload();
        }
    }

    function debouncedRequest() {
        return (0, _debounce2.default)(function () {
            component.doRequest();
        }, MINIMUM_INTERVAL_BETWEEN_REQUESTS, { leading: true });
    }

    component.$form.on('pageadjusted', PAGINATION_SELECTOR, debouncedRequest());
};

/**
 * Requests new reviews.
 */
UserReviewPaging.prototype.doRequest = function () {
    var component = this;
    (0, _jquery2.default)(document).trigger('isUpdatingReviews');

    // Abort current request if it's still pending.
    if (component.request) {
        component.request.abort();
    }

    component.request = component.ajax({
        url: component.$form.attr('action'),
        method: (component.$form.attr('method') || 'POST').toUpperCase(),
        data: component.$form.serialize(),
        success: function success(data) {
            return component.successHandler(data);
        },
        error: function error(_error) {
            return component.errorHandler(_error);
        }
    });
};

UserReviewPaging.prototype.successHandler = function (data) {
    var component = this;
    data.formData = (0, _clone2.default)(component.$form.serializeArray());
    component.update(data);
    component.formData = component.$form.serializeArray();
};

UserReviewPaging.prototype.errorHandler = function (error) {
    var component = this;
    if (error.statusText !== 'abort') {
        component.triggerResultsError();
    }
};

UserReviewPaging.prototype.triggerResultsError = function () {
    (0, _jquery2.default)(document).trigger('resultsError');
};

/**
 * Update outputs on the page with the received data.
 * @param {object} data     Received data.
 */
UserReviewPaging.prototype.update = function (data) {
    var component = this;

    component.updateHistory(data.url); //change url
    component.updateOutputs(data.content); // Update outputs on page.
};

/**
 * @param {String} url
 */
UserReviewPaging.prototype.updateHistory = function (url) {
    if ('pushState' in window.history) {
        window.history.pushState(PUSH_STATE_MARKER, window.title, url);
    }
};

/**
 * Get the outputs identifiers from the form.
 * @param {Object} $form
 */
UserReviewPaging.getOutputs = function ($form) {
    return $form.find(OUTPUT_SELECTOR);
};

/**
 * Use the outputMap object to map the new, received data, to the correct ouput identifiers on the page.
 * @param {Object} fields - data with keys corresponding to output identifiers.
 */
UserReviewPaging.prototype.updateOutputs = function (fields) {
    var component = this;

    // Map keys with their corresponding output identifiers.
    for (var key in fields) {
        if (fields.hasOwnProperty(key) && component.outputMap[key] && fields[key] !== null) {
            component.outputMap[key].html(fields[key]);
        }
    }

    if (Object.keys(fields).length) {
        (0, _jquery2.default)(document).trigger('resultsUpdated');
    }

    // Re-initialize the Expandible and UserReview components.
    _expandible2.default.initialize();
    _userReview2.default.initialize();
};

// turn all elements with the default selector into components
(0, _jquery2.default)(COMPONENT_SELECTOR).each(function (index, element) {
    return new UserReviewPaging(element);
});

},{"../ajax/ajax":198,"../app-spinner/app-spinner":201,"../expandible/expandible":229,"../user-review/user-review":336,"jquery":"jquery","lodash/clone":156,"lodash/debounce":158}],336:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

var _expandible = require('../expandible/expandible');

var _expandible2 = _interopRequireDefault(_expandible);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = UserReview;

// component configuration

var COMPONENT_SELECTOR = '[data-user-review]';
var SCROLL_ATTR = 'data-user-review-scroll';
var OPEN_HANDLE_SELECTOR = '[data-user-review-open]';
var CLOSE_HANDLE_SELECTOR = '[data-user-review-close]';
var DESCRIPTION_SELECTOR = '[data-user-review-description]';
var CONFIG_SELECTOR = '[data-user-review-config]';
var TOGGLE_BUTTONS_SELECTOR = '[data-user-review-footer]';
var FIRST_ELEMENT_SELECTOR = '.user-reviews .user-review:first-of-type';
var ENHANCED_CLASS = 'is-expandible';
var REPLY_ATTR = 'data-user-reply';
var HIDE_BUTTONS_CLASS = 'hide-buttons';
var DEFAULT_CONFIG = { '0px': { 'wordCount': 150, 'height': 200 } };

/**
 * Constructor method, links child elements to variables for internal use
 *
 * @param {HTMLElement} element The Html element to bind to.
 */
function UserReview(element) {
    var component = this;
    component.$element = (0, _jquery2.default)(element);
    component.$toggleButtons = component.$element.find(TOGGLE_BUTTONS_SELECTOR);
    component.$description = component.$element.find(DESCRIPTION_SELECTOR);
    component.expandible = new _expandible2.default(component.$element[0]);
    component.textContent = component.$description.text();
    component.htmlContent = component.$description.html();
    component.originalHeight = component.$description.height();

    // If there is no config on the template, use the default.
    if (component.$element.find(CONFIG_SELECTOR).length > 0) {
        component.config = JSON.parse(component.$element.find(CONFIG_SELECTOR).text());
    } else {
        component.config = DEFAULT_CONFIG;
    }

    component.getBreakpoints(component.config);
    component.bindEvents();

    // Expand the first review by default.
    if (component.$element.parent().is(FIRST_ELEMENT_SELECTOR)) {
        component.expandDescription();
        component.expandible.toggleExpand(true);
    }
}

/**
 * Binds all necessary events for the review.
 */
UserReview.prototype.bindEvents = function () {
    var component = this;
    component.scrollPosition = 0;

    component.$element.on('click', OPEN_HANDLE_SELECTOR, function () {
        component.expandDescription();

        // Set current position on expand.
        if (component.$element[0].hasAttribute(SCROLL_ATTR)) {
            component.scrollPosition = (0, _jquery2.default)(window).scrollTop();
        }
    });

    component.$element.on('click', CLOSE_HANDLE_SELECTOR, function () {
        component.getBreakpoints(component.config);

        // Return to position on collapse.
        if (component.scrollPosition > 0) {
            (0, _jquery2.default)(window).scrollTop(component.scrollPosition);
        }
    });
};

/**
 * Use the supplied breakpoints from the config to see if there's a match.
 * If there's a match, use it to collapse the description.
 * @param breakpoints
 */
UserReview.prototype.getBreakpoints = function (breakpoints) {
    var component = this;
    var windowSize = (0, _jquery2.default)(window).width();
    var matchedBreakpoint = void 0;

    Object.getOwnPropertyNames(breakpoints).forEach(function (index) {
        var size = index.substr(0, index.length - 2); // Remove 'px' from values.

        // If we have a matched breakpoint.
        if (windowSize > size) {
            matchedBreakpoint = breakpoints[index];
        }
    });

    // Collapse the description if we're dealing with a expandible $element
    // and breakpoint isn't undefined.
    if (component.$element.hasClass(ENHANCED_CLASS) && typeof matchedBreakpoint !== 'undefined') {
        component.collapseDescription(matchedBreakpoint);
    }
};

/**
 * Expand the description.
 */
UserReview.prototype.expandDescription = function () {
    var component = this;

    // Serve original text, reset $element height, toggle expand.
    component.$description.html(component.htmlContent);
    component.$element.css('height', 'auto');
    component.expandible.toggleExpand(true);
};

/**
 * Collapse the description.
 * Only trim the text if it's (too) long. If so, ecalculate the height to properly close the $element.
 * Depending on the length of the text the toggle buttons are shown/hidden as well.
 * @param breakpoint    matched breakpoint from the config object.
 */
UserReview.prototype.collapseDescription = function (breakpoint) {
    var component = this;
    var parentElement = component.$element.parent();
    var textLength = breakpoint.wordCount;
    var componentHeight = breakpoint.height;
    var reviewText = component.textContent.substr(0, textLength) + ' …';
    var replyText = component.textContent.substr(0, textLength - 25) + ' …';

    // Check if we're dealing with a long description text.
    if (component.originalHeight > 100) {
        // First, serve the shortened text.
        if (parentElement[0].hasAttribute(REPLY_ATTR)) {
            component.$description.html(replyText);
        } else {
            component.$description.html(reviewText);
        }

        // Then, calculate the height again.
        var descriptionHeight = component.$description.height();

        // The, if $element is a reply, use a smaller height calculation.
        if (parentElement[0].hasAttribute(REPLY_ATTR)) {
            component.$element.height(componentHeight + descriptionHeight - 60);
        } else {
            component.$element.height(componentHeight + descriptionHeight);
        }

        // Show the toggle buttons and collapse it.
        component.$toggleButtons.removeClass(HIDE_BUTTONS_CLASS);
        component.expandible.toggleExpand(false);
    } else {
        // Hide $element toggle buttons.
        component.$toggleButtons.addClass(HIDE_BUTTONS_CLASS);
    }
};

UserReview.initialize = function () {
    // turn all elements with the default selector into components
    (0, _jquery2.default)(COMPONENT_SELECTOR).each(function (index, element) {
        return new UserReview(element);
    });
};

UserReview.initialize();

},{"../expandible/expandible":229,"jquery":"jquery"}],337:[function(require,module,exports){
// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

// explicitly inject dependencies (alphabetically), only those needed

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _serviceController = require('../service-controller/service-controller');

var _serviceController2 = _interopRequireDefault(_serviceController);

var _classObservable = require('../class-observable/class-observable');

var _classObservable2 = _interopRequireDefault(_classObservable);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var $ = require('jquery');
var LoginDialog = require('../login-dialog/login-dialog');
exports.default = SaveObject;

// component configuration

var COMPONENT_SELECTOR = '[data-save-object]';
var HANDLE_SELECTOR = '[data-save-object-handle]';
var IS_SAVED_CLASS = 'is-saved';
var LOGIN_DIALOG_SELECTOR = '[data-dialog="user-form"]';
var ASYNC_OBJECT_RATING_SELECTOR = '[data-async-object-rating]';
var MULTIPLE_SAVE_COMPONENT_SELECTOR = '[data-connect-save-object-instances]';

function SaveObject(element) {
    var component = this;
    var $element = $(element);
    component.$element = $element;
    component.$saveHandle = $element.find(HANDLE_SELECTOR);
    component.$rating = $element.closest(ASYNC_OBJECT_RATING_SELECTOR);
    component.doPageRefresh = false;
    component.isSaved = component.$element.hasClass(IS_SAVED_CLASS);
    component.observers = new _classObservable2.default();
    component.saveUrl = component.$saveHandle.attr('href');

    //To be used by observers!
    component.onSaved = function (observer) {
        component.observers.listen(observer);
    };

    component.$element.on('click', HANDLE_SELECTOR, function (event) {
        event.preventDefault();
        component.event = event !== undefined ? event : !component.event;
        component.saveIfLoggedIn(this.href, component.event);
    });

    // event is triggered in the async object rating component
    // when an object is rated, the object is saved
    component.$rating.on('objectrated', function () {
        component.toggleSavedState(component.$saveHandle.attr('href'), true);
    });
}

SaveObject.prototype.saveIfLoggedIn = function (url, isSaved) {
    var component = this;
    component.isSaved = isSaved !== undefined ? isSaved : !component.isSaved;
    component.isUserLoggedIn(function onSuccessfulLogin() {
        component.toggleSavedState(url, component.isSaved);
    });
};

SaveObject.prototype.isUserLoggedIn = function (onLoggedIn) {
    var component = this;
    var dialogElement = document.querySelector(LOGIN_DIALOG_SELECTOR);
    var url = dialogElement.dialog.isUserLoggedInUrl;

    return $.ajax({
        url: url,
        success: function success(response) {
            if (response.LoggedIn === true) {
                onLoggedIn();
            } else {
                component.userLoginStatus = new LoginDialog(response.LoginUrl, function onSuccessfulLogin() {
                    component.doPageRefresh = true;
                    onLoggedIn();
                });
            }
        },
        error: function error(response) {
            console.error('Error calling', url, response);
        }
    });
};

/**
 * Does an ajax request and toggles the 'is-saved' class if the response is
 * successful
 */
SaveObject.prototype.toggleSavedState = function (url, isSaved) {
    var component = this;
    component.isSaved = isSaved !== undefined ? isSaved : !component.isSaved;
    component.multipleInstances = component.$element.find(MULTIPLE_SAVE_COMPONENT_SELECTOR).length;

    return $.ajax({
        url: url,
        success: function success(response) {
            if (SaveObject.isSuccesfulResponse(response)) {
                if (component.multipleInstances) {
                    $(COMPONENT_SELECTOR).each(function () {
                        $(this).toggleClass(IS_SAVED_CLASS, component.isSaved);
                    });
                } else {
                    component.$element.toggleClass(IS_SAVED_CLASS, component.isSaved);
                }
                /* component.isSaved gives me an event, this is clearly wrong, but the whole component is dirty */
                component.observers.notify(component.$element.hasClass(IS_SAVED_CLASS));
                component.$element.trigger('objectsaved', component.isSaved);
            } else {
                onError(response);
            }
        },
        error: onError,
        complete: function complete() {
            if (component.doPageRefresh === true) {
                window.location.reload();
            }
        }
    });

    function onError(response) {
        console.error('Error trying to save object using', component.url, response);
    }
};

SaveObject.prototype.save = function (isSaved) {
    var component = this;
    return component.toggleSavedState(component.saveUrl, isSaved);
};

SaveObject.isSuccesfulResponse = function (response) {
    return response.Result === 'OK';
};

// turn all elements with the default selector into components
// $(COMPONENT_SELECTOR).each(function(index, element) {
//     return new SaveObject(element);
// });
SaveObject.getSelector = function () {
    return COMPONENT_SELECTOR;
};
_serviceController2.default.getAllInstances(SaveObject);

},{"../class-observable/class-observable":218,"../login-dialog/login-dialog":249,"../service-controller/service-controller":317,"jquery":"jquery"}],338:[function(require,module,exports){
// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

// explicitly inject dependencies (alphabetically), only those needed

var _rating = require('../rating/rating');

var _rating2 = _interopRequireDefault(_rating);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var $ = require('jquery');
var InstantSearch = require('../instant-search/instant-search');
var AsyncObjectRating = require('../async-object-rating/async-object-rating');
var Expandible = require('../expandible/expandible');

// what does this module expose?
module.exports = UserSavedObjectsSorting;

// component configuration
var COMPONENT_SELECTOR = '[data-user-saved-objects-sorting]';
var PAGINATION_SELECTOR = '[data-pagination]';
var PAGINATION_VALUE_SELECTOR = 'input[data-pagination-input]';

/**
 * @param {HTMLFormElement} form
 * @constructor
 */
function UserSavedObjectsSorting(form) {
    InstantSearch.call(this, form);
    this.getOutputs($(form).parent());
}

UserSavedObjectsSorting.prototype = Object.create(InstantSearch.prototype);

UserSavedObjectsSorting.prototype.sendResultsUpdatedEvent = function () {
    //saved-items page shouldn't send result update event
};

UserSavedObjectsSorting.prototype.updateResults = function (data) {
    var component = this;
    component.renderOutputResults(data);
    component.soldFilterOption.attr('href', data.historischAanbodUrl);
    component.updateHistory(data.url);

    //reinitialize the AsyncObjectRating and Expandible modules
    AsyncObjectRating.initialize();
    Expandible.initialize();
    _rating2.default.initialize();
};

UserSavedObjectsSorting.prototype.bindToEvents = function () {
    //call base first
    InstantSearch.prototype.bindToEvents.call(this);

    var component = this;

    //and do some own wiring
    component.$form.parent().on('pageadjusted', PAGINATION_SELECTOR, function () {

        //get the page from the pagination module
        var page = $(this).find(PAGINATION_VALUE_SELECTOR).val();

        //and set it's value in the form that handles the instant search module
        component.$form.find(PAGINATION_VALUE_SELECTOR).val(page);

        if (!component.isRestoring) {
            component.doRequest();
        }
    });
};

$(COMPONENT_SELECTOR).each(function (index, element) {
    return new UserSavedObjectsSorting(element);
});

},{"../async-object-rating/async-object-rating":209,"../expandible/expandible":229,"../instant-search/instant-search":246,"../rating/rating":300,"jquery":"jquery"}],339:[function(require,module,exports){
'use strict';

/**
 * App
 * include all views, which in turn include all their components
 */

// prevent console errors in legacy browsers (needs to be included first)
require('./components/_dev-console/dev-console');

// validation library used in forms
require('jquery-validation-unobtrusive');

// views (alphabetically)
require('./views/home/home');
require('./views/beoordelingen-widget/beoordelingen-widget');
require('./views/content-funda-huis/content-funda-huis');
require('./views/content-page/content-page');
require('./views/cms-article/cms-article');
require('./views/contact-form-brochure/contact-form-brochure');
require('./views/contact-form-makelaar/contact-form-makelaar');
require('./views/contact-form-object/contact-form-object');
require('./views/contact-form-plan-viewing/contact-form-plan-viewing');
require('./views/fib-home/fib-home');
require('./views/fib-makelaars-landing/fib-makelaars-landing');
require('./views/fib-search/fib-search');
require('./views/maandlasten-indicator-landing/maandlasten-indicator-landing');
require('./views/makelaars-aanmeld/makelaars-aanmeld');
require('./views/makelaars-kantoor/makelaars-kantoor');
require('./views/makelaars-landing/makelaars-landing');
require('./views/makelaars-collaborators/makelaars-collaborators');
require('./views/nieuwbouwproject-detail/nieuwbouwproject-detail');
require('./views/nieuwe-huis-checklist/nieuwe-huis-checklist');
require('./views/nieuwe-huis-koppelen/nieuwe-huis-koppelen');
require('./views/nieuwe-huis-overzicht/nieuwe-huis-overzicht');
require('./views/makelaars-search-results/makelaars-search-results');
require('./views/object-detail/object-detail');
require('./views/object-detail-print/object-detail-print');
require('./views/object-meld-een-fout/object-meld-een-fout');
require('./views/object-share-email/object-share-email');
require('./views/search/search');
require('./views/search-no-results/search-no-results');
require('./views/user-cadeauflow/user-cadeauflow');
require('./views/user-claimmyhouse/user-claimmyhouse');
require('./views/user-login/user-login');
require('./views/user-my-house/user-my-house');
require('./views/user-object-rating/user-object-rating');
require('./views/user-profile/user-profile');
require('./views/user-recovery/user-recovery');
require('./views/user-register/user-register');
require('./views/user-report-abuse/user-report-abuse');
require('./views/user-reviews/user-reviews');
require('./views/user-reviews-write/user-reviews-write');
require('./views/user-saved-objects/user-saved-objects');
require('./views/cookie-policy-page/cookie-policy-page');
require('./views/zoek-op-kaart/zoek-op-kaart');

require('./components/media-foto360/media-foto360');
require('./components/custom-select-box/custom-select-box');

},{"./components/_dev-console/dev-console":194,"./components/custom-select-box/custom-select-box":226,"./components/media-foto360/media-foto360":266,"./views/beoordelingen-widget/beoordelingen-widget":341,"./views/cms-article/cms-article":342,"./views/contact-form-brochure/contact-form-brochure":343,"./views/contact-form-makelaar/contact-form-makelaar":344,"./views/contact-form-object/contact-form-object":345,"./views/contact-form-plan-viewing/contact-form-plan-viewing":346,"./views/content-funda-huis/content-funda-huis":347,"./views/content-page/content-page":348,"./views/cookie-policy-page/cookie-policy-page":349,"./views/fib-home/fib-home":350,"./views/fib-makelaars-landing/fib-makelaars-landing":351,"./views/fib-search/fib-search":352,"./views/home/home":353,"./views/maandlasten-indicator-landing/maandlasten-indicator-landing":354,"./views/makelaars-aanmeld/makelaars-aanmeld":355,"./views/makelaars-collaborators/makelaars-collaborators":356,"./views/makelaars-kantoor/makelaars-kantoor":357,"./views/makelaars-landing/makelaars-landing":358,"./views/makelaars-search-results/makelaars-search-results":359,"./views/nieuwbouwproject-detail/nieuwbouwproject-detail":360,"./views/nieuwe-huis-checklist/nieuwe-huis-checklist":361,"./views/nieuwe-huis-koppelen/nieuwe-huis-koppelen":362,"./views/nieuwe-huis-overzicht/nieuwe-huis-overzicht":363,"./views/object-detail-print/object-detail-print":364,"./views/object-detail/object-detail":365,"./views/object-meld-een-fout/object-meld-een-fout":366,"./views/object-share-email/object-share-email":367,"./views/search-no-results/search-no-results":368,"./views/search/search":369,"./views/user-cadeauflow/user-cadeauflow":370,"./views/user-claimmyhouse/user-claimmyhouse":371,"./views/user-login/user-login":372,"./views/user-my-house/user-my-house":373,"./views/user-object-rating/user-object-rating":374,"./views/user-profile/user-profile":375,"./views/user-recovery/user-recovery":376,"./views/user-register/user-register":377,"./views/user-report-abuse/user-report-abuse":378,"./views/user-reviews-write/user-reviews-write":379,"./views/user-reviews/user-reviews":380,"./views/user-saved-objects/user-saved-objects":381,"./views/zoek-op-kaart/zoek-op-kaart":382,"jquery-validation-unobtrusive":"jquery-validation-unobtrusive"}],340:[function(require,module,exports){
'use strict';

/**
 * Base view
 * Foundation for all views, bootstraps app-wide components
 */

require('../../components/error-bar/error-bar');
require('../../components/app-header/app-header');
require('../../components/app-hotkeys/app-hotkeys');
require('../../components/cookie-policy/cookie-policy');

},{"../../components/app-header/app-header":199,"../../components/app-hotkeys/app-hotkeys":200,"../../components/cookie-policy/cookie-policy":225,"../../components/error-bar/error-bar":228}],341:[function(require,module,exports){
'use strict';

require('../../components/iframe-height-updater/iframe-height-updater');

},{"../../components/iframe-height-updater/iframe-height-updater":242}],342:[function(require,module,exports){
'use strict';

require('../../components/cms-article-header-bg/cms-article-header-bg');

},{"../../components/cms-article-header-bg/cms-article-header-bg":220}],343:[function(require,module,exports){
'use strict';

require('./../_base-view/base-view');
require('../../components/object-contact/object-contact');

},{"../../components/object-contact/object-contact":282,"./../_base-view/base-view":340}],344:[function(require,module,exports){
'use strict';

require('./../_base-view/base-view');
require('../../components/object-contact/object-contact');

},{"../../components/object-contact/object-contact":282,"./../_base-view/base-view":340}],345:[function(require,module,exports){
'use strict';

require('./../_base-view/base-view');
require('../../components/object-contact/object-contact');
require('./../../components/hypotheek-advies/hypotheek-advies');

},{"../../components/object-contact/object-contact":282,"./../../components/hypotheek-advies/hypotheek-advies":241,"./../_base-view/base-view":340}],346:[function(require,module,exports){
'use strict';

require('./../_base-view/base-view');
require('../../components/object-contact/object-contact');

},{"../../components/object-contact/object-contact":282,"./../_base-view/base-view":340}],347:[function(require,module,exports){
'use strict';

/**
 * funda-huis view
 */

require('./../_base-view/base-view');
require('./../../components/content-twitter-feed/content-twitter-feed');

// jquery.twentytwenty component is included inline

},{"./../../components/content-twitter-feed/content-twitter-feed":224,"./../_base-view/base-view":340}],348:[function(require,module,exports){
'use strict';

require('../../components/carousel/carousel');

},{"../../components/carousel/carousel":217}],349:[function(require,module,exports){
'use strict';

require('../../components/content-tabs/content-tabs');

},{"../../components/content-tabs/content-tabs":223}],350:[function(require,module,exports){
'use strict';

/**
 * funda in business - home koop / default view
 */

require('./../_base-view/base-view');
require('../../components/featured-homes/featured-homes');

},{"../../components/featured-homes/featured-homes":230,"./../_base-view/base-view":340}],351:[function(require,module,exports){
'use strict';

require('../../components/search-block/search-block');
require('../../components/clear-input/clear-input');

},{"../../components/clear-input/clear-input":219,"../../components/search-block/search-block":303}],352:[function(require,module,exports){
'use strict';

require('./../_base-view/base-view');
require('../../components/advertisements/advertisements');
require('../../components/search-content/search-content');
require('../../components/dialog/dialog');
require('../../components/filter-flyout/filter-flyout');
require('../../components/instant-search/instant-search');
require('../../components/makelaar-ads/makelaar-ads');
require('../../components/object-type-filter/object-type-filter');
require('../../components/pagination/pagination');
require('../../components/radio-group/radio-group');
require('../../components/search-header/search-header');
require('../../components/search-save/search-save');
require('../../components/search-sidebar/search-sidebar');
require('../../components/search-price-toggle/search-price-toggle');
require('../../components/top-position/top-position');
require('../../components/applied-filters/applied-filters');

},{"../../components/advertisements/advertisements":196,"../../components/applied-filters/applied-filters":204,"../../components/dialog/dialog":227,"../../components/filter-flyout/filter-flyout":232,"../../components/instant-search/instant-search":246,"../../components/makelaar-ads/makelaar-ads":251,"../../components/object-type-filter/object-type-filter":291,"../../components/pagination/pagination":293,"../../components/radio-group/radio-group":296,"../../components/search-content/search-content":304,"../../components/search-header/search-header":306,"../../components/search-price-toggle/search-price-toggle":312,"../../components/search-save/search-save":314,"../../components/search-sidebar/search-sidebar":316,"../../components/top-position/top-position":326,"./../_base-view/base-view":340}],353:[function(require,module,exports){
'use strict';

/**
 * home koop / default view
 */

require('./../_base-view/base-view');
require('../../components/autocomplete/autocomplete');
require('../../components/notification-cookie/notification-cookie');
require('../../components/soort-notification-cookie/soort-notification-cookie');
require('../../components/notification-status/notification-status');
require('../../components/range-filter/range-filter');
require('../../components/expandible/expandible');
require('../../components/search-block/search-block');

},{"../../components/autocomplete/autocomplete":216,"../../components/expandible/expandible":229,"../../components/notification-cookie/notification-cookie":278,"../../components/notification-status/notification-status":279,"../../components/range-filter/range-filter":299,"../../components/search-block/search-block":303,"../../components/soort-notification-cookie/soort-notification-cookie":319,"./../_base-view/base-view":340}],354:[function(require,module,exports){
'use strict';

require('./../_base-view/base-view');
require('./../../components/maandlasten-indicator/maandlasten-indicator');

},{"./../../components/maandlasten-indicator/maandlasten-indicator":250,"./../_base-view/base-view":340}],355:[function(require,module,exports){
'use strict';

require('./../../components/makelaars-aanmeld-toggle/makelaars-aanmeld-toggle');
require('./../../components/tooltip-inline/tooltip-inline');

},{"./../../components/makelaars-aanmeld-toggle/makelaars-aanmeld-toggle":252,"./../../components/tooltip-inline/tooltip-inline":324}],356:[function(require,module,exports){
'use strict';

/**
 * makelaars medewerkers view
 */

require('../../components/makelaars-collaborators-list/makelaars-collaborators-list');
require('../../components/object-contact/object-contact');

},{"../../components/makelaars-collaborators-list/makelaars-collaborators-list":254,"../../components/object-contact/object-contact":282}],357:[function(require,module,exports){
'use strict';

/**
 * makelaars-kantoor view
 * [description]
 */

require('../../components/makelaars-about/makelaars-about');
require('../../components/object-contact/object-contact');
require('../../components/makelaars-contact/makelaars-contact');
require('../../components/makelaars-features/makelaars-features');
require('./../../components/related-objects/related-objects');
require('../../components/makelaars-twitterfeed/makelaars-twitterfeed');
require('./../../components/media-viewer/media-viewer');

},{"../../components/makelaars-about/makelaars-about":253,"../../components/makelaars-contact/makelaars-contact":255,"../../components/makelaars-features/makelaars-features":256,"../../components/makelaars-twitterfeed/makelaars-twitterfeed":258,"../../components/object-contact/object-contact":282,"./../../components/media-viewer/media-viewer":272,"./../../components/related-objects/related-objects":301}],358:[function(require,module,exports){
'use strict';

// we need the js from search-block component to submit on enter
require('../../components/search-block/search-block');
require('../../components/clear-input/clear-input');

},{"../../components/clear-input/clear-input":219,"../../components/search-block/search-block":303}],359:[function(require,module,exports){
'use strict';

require('./../_base-view/base-view');

require('../../components/dialog/dialog');
require('../../components/instant-search/instant-search');
require('../../components/filter-reset/filter-reset');
require('../../components/makelaars-name/makelaars-name');
require('../../components/notification/notification');
require('../../components/pagination/pagination');
require('../../components/radio-group/radio-group');
require('../../components/search-header/search-header');
require('../../components/search-sidebar/search-sidebar');

},{"../../components/dialog/dialog":227,"../../components/filter-reset/filter-reset":234,"../../components/instant-search/instant-search":246,"../../components/makelaars-name/makelaars-name":257,"../../components/notification/notification":280,"../../components/pagination/pagination":293,"../../components/radio-group/radio-group":296,"../../components/search-header/search-header":306,"../../components/search-sidebar/search-sidebar":316,"./../_base-view/base-view":340}],360:[function(require,module,exports){
'use strict';

/**
 * nieuwbouwproject-detail view
 */
require('./../_base-view/base-view');
require('./../../components/object-description/object-description');
require('./../../components/nieuwbouwproject-woningtypen/nieuwbouwproject-woningtypen');
require('./../../components/related-objects/related-objects');
require('./../../components/object-actions/object-actions');

},{"./../../components/nieuwbouwproject-woningtypen/nieuwbouwproject-woningtypen":273,"./../../components/object-actions/object-actions":281,"./../../components/object-description/object-description":283,"./../../components/related-objects/related-objects":301,"./../_base-view/base-view":340}],361:[function(require,module,exports){
'use strict';

require('./../../components/sticky-navigation/sticky-navigation');
require('./../../components/tabs/tabs');
require('./../../components/nieuwe-huis-checklist/nieuwe-huis-checklist');

},{"./../../components/nieuwe-huis-checklist/nieuwe-huis-checklist":274,"./../../components/sticky-navigation/sticky-navigation":320,"./../../components/tabs/tabs":321}],362:[function(require,module,exports){
'use strict';

require('./../../components/nieuwe-huis-koppelen-sticky/nieuwe-huis-koppelen-sticky');

},{"./../../components/nieuwe-huis-koppelen-sticky/nieuwe-huis-koppelen-sticky":275}],363:[function(require,module,exports){
'use strict';

require('./../../components/nieuwe-huis-widget/nieuwe-huis-widget');
require('./../../components/nieuwe-huis-widget-related-articles/nieuwe-huis-widget-related-articles');

},{"./../../components/nieuwe-huis-widget-related-articles/nieuwe-huis-widget-related-articles":276,"./../../components/nieuwe-huis-widget/nieuwe-huis-widget":277}],364:[function(require,module,exports){
'use strict';

/**
 * object-detail Print-friendly view
 */

require('./../_base-view/base-view');

var PRINT_PAGE_SELECTOR = '.object-detail-print';
var PRINT_DELAY = 1000;

if (document.querySelector(PRINT_PAGE_SELECTOR)) {
    window.onload = function () {
        setTimeout(function () {
            return window.print();
        }, PRINT_DELAY);
    };
}

},{"./../_base-view/base-view":340}],365:[function(require,module,exports){
'use strict';

/**
 * object-detail view
 */

require('./../_base-view/base-view');
require('./../../components/image-error-fallback/image-error-fallback');
require('./../../components/object-description/object-description');
require('./../../components/object-kenmerken/object-kenmerken');
require('./../../components/object-map/object-map');
require('./../../components/media-viewer/media-viewer');
require('./../../components/object-media/object-media');
require('./../../components/object-contact/object-contact');
require('./../../components/log-request/log-request');
require('./../../components/opt-in/opt-in');
require('./../../components/tooltip/tooltip');
require('./../../components/user-save-object/user-save-object');
require('./../../components/object-detail-interaction/object-detail-interaction');
require('./../../components/advertisements-lazy/advertisements-lazy');
require('./../../components/related-objects/related-objects');

},{"./../../components/advertisements-lazy/advertisements-lazy":195,"./../../components/image-error-fallback/image-error-fallback":243,"./../../components/log-request/log-request":248,"./../../components/media-viewer/media-viewer":272,"./../../components/object-contact/object-contact":282,"./../../components/object-description/object-description":283,"./../../components/object-detail-interaction/object-detail-interaction":284,"./../../components/object-kenmerken/object-kenmerken":285,"./../../components/object-map/object-map":287,"./../../components/object-media/object-media":288,"./../../components/opt-in/opt-in":292,"./../../components/related-objects/related-objects":301,"./../../components/tooltip/tooltip":325,"./../../components/user-save-object/user-save-object":337,"./../_base-view/base-view":340}],366:[function(require,module,exports){
'use strict';

require('../../components/object-meld-een-fout-form/object-meld-een-fout-form');

},{"../../components/object-meld-een-fout-form/object-meld-een-fout-form":289}],367:[function(require,module,exports){
'use strict';

/**
 * object-share-email view A.K.A. Doorsturen
 */

require('./../_base-view/base-view');

},{"./../_base-view/base-view":340}],368:[function(require,module,exports){
/**
 * search-no-results view
 * displays message when applying a filter set with no matches
 */

'use strict';

require('./../_base-view/base-view');
require('../../components/expandible/expandible');
require('../../components/instant-search/instant-search');
require('../../components/makelaar-ads/makelaar-ads');
require('../../components/save-search/save-search');
require('../../components/search-header/search-header');
require('../../components/search-sidebar/search-sidebar');
require('../../components/search-no-results/search-no-results');
require('../../components/radius-filter/radius-filter');
require('../../components/user-save-object/user-save-object');

},{"../../components/expandible/expandible":229,"../../components/instant-search/instant-search":246,"../../components/makelaar-ads/makelaar-ads":251,"../../components/radius-filter/radius-filter":297,"../../components/save-search/save-search":302,"../../components/search-header/search-header":306,"../../components/search-no-results/search-no-results":311,"../../components/search-sidebar/search-sidebar":316,"../../components/user-save-object/user-save-object":337,"./../_base-view/base-view":340}],369:[function(require,module,exports){
'use strict';

require('./../_base-view/base-view');
require('../../components/advertisements/advertisements');
require('../../components/advertisements-lazy/advertisements-lazy');
require('../../components/search-content/search-content');
require('../../components/dialog/dialog');
require('../../components/filter-flyout/filter-flyout');
require('../../components/instant-search/instant-search');
require('../../components/makelaar-ads/makelaar-ads');
require('../../components/object-type-filter/object-type-filter');
require('../../components/pagination/pagination');
require('../../components/radio-group/radio-group');
require('../../components/search-header/search-header');
require('../../components/search-save/search-save');
require('../../components/search-sidebar/search-sidebar');
require('../../components/search-price-toggle/search-price-toggle');
require('../../components/top-position/top-position');
require('../../components/applied-filters/applied-filters');

},{"../../components/advertisements-lazy/advertisements-lazy":195,"../../components/advertisements/advertisements":196,"../../components/applied-filters/applied-filters":204,"../../components/dialog/dialog":227,"../../components/filter-flyout/filter-flyout":232,"../../components/instant-search/instant-search":246,"../../components/makelaar-ads/makelaar-ads":251,"../../components/object-type-filter/object-type-filter":291,"../../components/pagination/pagination":293,"../../components/radio-group/radio-group":296,"../../components/search-content/search-content":304,"../../components/search-header/search-header":306,"../../components/search-price-toggle/search-price-toggle":312,"../../components/search-save/search-save":314,"../../components/search-sidebar/search-sidebar":316,"../../components/top-position/top-position":326,"./../_base-view/base-view":340}],370:[function(require,module,exports){
'use strict';

require('./../../components/form-validation/form-validation');
require('./../../components/goto/goto');

},{"./../../components/form-validation/form-validation":237,"./../../components/goto/goto":239}],371:[function(require,module,exports){
'use strict';

require('./../../components/form-validation/form-validation');

},{"./../../components/form-validation/form-validation":237}],372:[function(require,module,exports){
'use strict';

/**
 * user-login view
 * [description]
 */

require('../../components/dialog/dialog');
require('../../components/notification/notification');
require('../../components/placeholder-fallback/placeholder-fallback');
require('../../components/social-login/social-login');
require('../../components/user-login/user-login.js');

},{"../../components/dialog/dialog":227,"../../components/notification/notification":280,"../../components/placeholder-fallback/placeholder-fallback":295,"../../components/social-login/social-login":318,"../../components/user-login/user-login.js":327}],373:[function(require,module,exports){
'use strict';

/**
 * user-my-house view
 * View for the users to view their house on funda, and how it performs.
 */

require('./../_base-view/base-view');
require('../../components/dialog/dialog');
require('../../components/graph/graph');
require('../../components/user-my-house-datasource/user-my-house-datasource');
require('../../components/user-my-house-graph/user-my-house-graph');
require('../../components/user-my-house-header/user-my-house-header');
require('../../components/user-my-house-options/user-my-house-options');
require('../../components/user-my-house-statistics/user-my-house-statistics');

},{"../../components/dialog/dialog":227,"../../components/graph/graph":240,"../../components/user-my-house-datasource/user-my-house-datasource":328,"../../components/user-my-house-graph/user-my-house-graph":329,"../../components/user-my-house-header/user-my-house-header":330,"../../components/user-my-house-options/user-my-house-options":331,"../../components/user-my-house-statistics/user-my-house-statistics":332,"./../_base-view/base-view":340}],374:[function(require,module,exports){
'use strict';

/**
 * user-object-rating view
 * [description]
 */

require('./../_base-view/base-view');
require('../../components/rating/rating');
require('../../components/object-rating-form/object-rating-form');

},{"../../components/object-rating-form/object-rating-form":290,"../../components/rating/rating":300,"./../_base-view/base-view":340}],375:[function(require,module,exports){
'use strict';

require('../../components/async-form-with-read-container/form-with-notifications');
require('../../components/async-form-with-read-container/form-hidden');
require('../../components/form-alert-on-leave/form-alert-on-leave');

},{"../../components/async-form-with-read-container/form-hidden":205,"../../components/async-form-with-read-container/form-with-notifications":206,"../../components/form-alert-on-leave/form-alert-on-leave":235}],376:[function(require,module,exports){
'use strict';

/**
 * user-recovery view
 * [description]
 */

require('./../_base-view/base-view');
require('../../components/placeholder-fallback/placeholder-fallback');

},{"../../components/placeholder-fallback/placeholder-fallback":295,"./../_base-view/base-view":340}],377:[function(require,module,exports){
'use strict';

/**
 * user-register view
 * [description]
 */

require('./../_base-view/base-view');
require('../../components/placeholder-fallback/placeholder-fallback');

},{"../../components/placeholder-fallback/placeholder-fallback":295,"./../_base-view/base-view":340}],378:[function(require,module,exports){
'use strict';

/**
 * user-report-abuse view
 * View with a form to report abuse regarding reviews.
 */

require('./../_base-view/base-view');
require('../../components/user-report-form/user-report-form');

},{"../../components/user-report-form/user-report-form":334,"./../_base-view/base-view":340}],379:[function(require,module,exports){
'use strict';

require('./../../components/form-textarea-countdown/form-textarea-countdown');
require('./../../components/form-validation/form-validation');
require('./../../components/toggle-visibility/toggle-visibility');

},{"./../../components/form-textarea-countdown/form-textarea-countdown":236,"./../../components/form-validation/form-validation":237,"./../../components/toggle-visibility/toggle-visibility":323}],380:[function(require,module,exports){
'use strict';

/**
 * user-reviews view
 * View containing all reviews (and sorting options) for a makelaar.
 */

require('../../components/user-review/user-review');
require('../../components/user-review-paging/user-review-paging');

},{"../../components/user-review-paging/user-review-paging":335,"../../components/user-review/user-review":336}],381:[function(require,module,exports){
'use strict';

/**
 * object-detail view
 * [description]
 */

require('./../_base-view/base-view');
require('../../components/async-object-rating/async-object-rating');
require('../../components/object-rating-form/object-rating-form');
require('../../components/user-saved-objects-sorting/user-saved-objects-sorting');
require('../../components/user-my-logged-in-as/user-my-logged-in-as');

},{"../../components/async-object-rating/async-object-rating":209,"../../components/object-rating-form/object-rating-form":290,"../../components/user-my-logged-in-as/user-my-logged-in-as":333,"../../components/user-saved-objects-sorting/user-saved-objects-sorting":338,"./../_base-view/base-view":340}],382:[function(require,module,exports){
'use strict';

require('./../_base-view/base-view');
require('../../components/instant-map-search/instant-map-search');
require('../../components/search-map/search-map');

},{"../../components/instant-map-search/instant-map-search":245,"../../components/search-map/search-map":310,"./../_base-view/base-view":340}]},{},[339])

//# sourceMappingURL=index.js.map
