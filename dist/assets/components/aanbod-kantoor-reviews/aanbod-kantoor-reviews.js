// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

// explicitly inject dependencies (alphabetically), only those needed
import $ from 'jquery';

// what does this module expose?
export default AanbodKantoorReviews;

// component configuration
const COMPONENT_SELECTOR = '[data-aanbod-kantoor-reviews]';

function AanbodKantoorReviews(element) {
    const component = this; // Not necessary in ES2015, keep using it for consistency
    component.$element = $(element);
}

// turn all elements with the default selector into components
$(COMPONENT_SELECTOR).each(function(index, element) {
    return new AanbodKantoorReviews(element);
});
