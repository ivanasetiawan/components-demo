/*
    Copyright 2013 Michael Countis
    MIT License: http://opensource.org/licenses/MIT
*/

(function() {
    'use strict';
    window.googletag = window.googletag || {};
    window.googletag.cmd = window.googletag.cmd || [];
    googletag.cmd.push(function() {
        if (googletag.hasOwnProperty('on') || googletag.hasOwnProperty('off') || googletag.hasOwnProperty('trigger') || googletag.hasOwnProperty('events')) {
            return;
        }
        var OLD_LOG = googletag.debug_log.log;
        var events = [];
        var addEvent = function(name, id, match) {
            events.push({'name': name, 'id': id, 'match': match});
        };

        /*eslint-disable no-multi-spaces */
        addEvent('gpt-google_js_loaded',                     8, /Google service JS loaded/ig);
        addEvent('gpt-gpt_fetch',                           46, /Fetching GPT implementation/ig);
        addEvent('gpt-gpt_fetched',                         48, /GPT implementation fetched\./ig);
        addEvent('gpt-page_load_complete',                   1, /Page load complete/ig);
        addEvent('gpt-queue_start',                         31, /^Invoked queued function/ig);
        addEvent('gpt-service_add_slot',                    40, /Associated ([\w]*) service with slot ([\/\w]*)/ig);
        addEvent('gpt-service_add_targeting',               88, /Setting targeting attribute ([\w]*) with value ([\w\W]*) for service ([\w]*)/ig);
        addEvent('gpt-service_collapse_containers_enable',  78, /Enabling collapsing of containers when there is no ad content/ig);
        addEvent('gpt-service_create',                      35, /Created service: ([\w]*)/ig);
        addEvent('gpt-service_single_request_mode_enable',  63, /Using single request mode to fetch ads/ig);
        addEvent('gpt-slot_create',                          2, /Created slot: ([\/\w]*)/ig);
        addEvent('gpt-slot_add_targeting',                  17, /Setting targeting attribute ([\w]*) with value ([\w\W]*) for slot ([\/\w]*)/ig);
        addEvent('gpt-slot_fill',                           50, /Calling fillslot/ig);
        addEvent('gpt-slot_fetch',                           3, /Fetching ad for slot ([\/\w]*)/ig);
        addEvent('gpt-slot_receiving',                       4, /Receiving ad for slot ([\/\w]*)/ig);
        addEvent('gpt-slot_render_delay',                   53, /Delaying rendering of ad slot ([\/\w]*) pending loading of the GPT implementation/ig);
        addEvent('gpt-slot_rendering',                       5, /^Rendering ad for slot ([\/\w]*)/ig);
        addEvent('gpt-slot_rendered',                        6, /Completed rendering ad for slot ([\/\w]*)/ig);
        /*eslint-enable no-multi-spaces */

        googletag.events = googletag.events || {};
        googletag.on = function(onEvents, OP_ARG0/*data*/, OP_ARG1/*callback*/) {
            if (!OP_ARG0) {
                return this;
            }
            onEvents = onEvents.split(' ');
            var data = OP_ARG1 ? OP_ARG0 : undefined;
            var callback = OP_ARG1 || OP_ARG0;
            var ei = 0;
            var e = '';

            callback.data = data;

            for (e = onEvents[ei = 0]; ei < onEvents.length; e = onEvents[++ei]) {
                (this.events[e] = this.events[e] || []).push(callback);
            }

            return this;
        };
        googletag.off = function(offEvents, handler) {
            offEvents = offEvents.split(' ');
            var ei = 0;
            var e = '';
            var fi = 0;
            var f = function() {};

            for (e = offEvents[ei]; ei < offEvents.length; e = offEvents[++ei]) {
                if (!this.events.hasOwnProperty(e)) {
                    continue;
                }

                if (!handler) {
                    delete this.events[e];
                    continue;
                }

                fi = this.events[e].length - 1;
                for (f = this.events[e][fi]; fi >= 0; f = this.events[e][--fi]) {
                    if (f == handler) {
                        this.events[e].splice(fi, 1);
                    }
                }
                if (this.events[e].length === 0) {
                    delete this.events[e];
                }
            }

            return this;
        };
        googletag.trigger = function(event, parameters = []) {
            if (!this.events[event] || this.events[event].length === 0) {
                return this;
            }

            var fi = 0;
            var f = this.events[event][fi];

            for (fi, f ; fi < this.events[event].length; f = this.events[event][++fi]) {
                if (f.apply(this, [{data: f.data}].concat(parameters)) === false) {
                    break;
                }
            }

            return this;
        };
        googletag.debug_log.log = function(level, message) {
            if (message && message.getMessageId && typeof (message.getMessageId()) == 'number') {
                var args = Array.prototype.slice.call(arguments);
                var e = 0;
                for (e; e < events.length; e++) {
                    if (events[e].id === message.getMessageId()) {
                        googletag.trigger(events[e].name, args);
                    }
                }
            }
            return OLD_LOG.apply(this, arguments);
        };
    });
})();
