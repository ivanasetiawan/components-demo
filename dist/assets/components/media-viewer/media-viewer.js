'use strict';

import $ from 'jquery';
const FotosView = require('../media-viewer-fotos/media-viewer-fotos');
import VideoView from '../media-viewer-video/media-viewer-video';
const Foto360View = require('../media-viewer-foto360/media-viewer-foto360');
const PlattegrondView = require('../media-viewer-plattegrond/media-viewer-plattegrond');

// monkey-patch 2 functions that are used by Slick but deprecated in jQuery 3
$.prototype.load = (callback) => $.fn.on.apply(this, ['load', callback]);
$.prototype.error = (callback) => $.fn.on.apply(this, ['error', callback]);

export default MediaViewer;

const COMPONENT_SELECTOR = '[data-media-viewer]';
const FOTO_VIEWER_SELECTOR = '[data-media-viewer-fotos]';
const FOTO_HASH = '#foto-';
const VIDEO_VIEWER_SELECTOR = '[data-media-viewer-video]';
const VIDEO_HASH = '#video';
const FOTO360_VIEWER_SELECTOR = '[data-media-viewer-foto360]';
const FOTO360_HASH = '#360-foto-';
const PLATTEGROND_VIEWER_SELECTOR = '[data-media-viewer-plattegrond]';
const PLATTEGROND_HASH = '#plattegrond';
const MEDIA_VIEWER_HASHES = [FOTO_HASH, FOTO360_HASH, VIDEO_HASH, PLATTEGROND_HASH];
const HIDDEN_CLASS = 'hidden';
const OPEN_MEDIA_VIEWER_EVENT = 'openmediaviewer';
const EVENT_NAMESPACE = '.media-viewer';
const BODY_SELECTOR = 'body';
const BODY_OPEN_CLASS = 'media-viewer-open';
const OPEN_CLASS = 'is-open';
const CLOSING_CLASS = 'is-closing';
const CLOSE_SELECTOR = '[data-media-viewer-close]';
const OBJECT_MEDIA_OVERLAY_LINK = 'data-media-viewer-overlay';
const OBJECT_MEDIA_OVERLAY_LINK_SELECTOR = '[' + OBJECT_MEDIA_OVERLAY_LINK + ']';

const OBJECT_MEDIA_OVERLAY_SWITCH_LINK = 'data-media-viewer-overlay-switch';
const OBJECT_MEDIA_OVERLAY_SWITCH_SELECTOR = '[data-media-viewer-overlay-switch]';

const ANIMATION_END_EVENTS = 'webkitAnimationEnd oAnimationEnd msAnimationEnd animationend';
let scrollPosition;
const $body = $(BODY_SELECTOR);

function MediaViewer(element) {
    const component = this;

    component.animationsSupported = browserSupportsAnimations();

    component.$element = $(element);
    component.$closeButton = component.$element.find(CLOSE_SELECTOR);

    if (component.hasMediaViewerHashInUrl()) {
        component.isMediaViewerAccessedDirectly = true;
        component.open();
        component.refreshView();
    }

    component.bindEvents();
}

MediaViewer.prototype.bindEvents = function() {
    const component = this;

    // Close button
    component.$closeButton.on('click', function closeHandler() {
        component.close();
    });

    // Close key (escape)
    $(document).on('keydown' + EVENT_NAMESPACE, function keyHandler(event) {
        if (event.ctrlKey || event.shiftKey || event.altKey || event.metaKey) {
            return;
        }

        if (event.keyCode === 27 /*ESC*/) {
            component.close();
        }
    });

    // Link to media clicked, media viewer should be opened
    $(OBJECT_MEDIA_OVERLAY_LINK_SELECTOR).on('click', function(e) {
        e.preventDefault(); // prevent hashchange event
        $(window).trigger(OPEN_MEDIA_VIEWER_EVENT, [this.getAttribute(OBJECT_MEDIA_OVERLAY_LINK)]);
    });

    // Switch to different view while media viewer is open
    $(OBJECT_MEDIA_OVERLAY_SWITCH_SELECTOR).on('click', function() {
        component.replaceState([this.getAttribute(OBJECT_MEDIA_OVERLAY_SWITCH_LINK)]);
        component.refreshView();
    });

    // Open Media Viewer event (custom event)
    $(window).on(OPEN_MEDIA_VIEWER_EVENT, function(e, overlay) {
        // Change the url and the hashchange event will take care of the rest
        // (don't use history.pushState because the hashchange event won't be fired)
        window.location.href = window.location.pathname + overlay;
    });

    // Hashchange (url altered, possibly because of a history event)
    window.addEventListener('hashchange', function hashChangeHandler() {
        if (component.hasMediaViewerHashInUrl()) {
            component.open();
            component.refreshView();
        } else {
            component.close();
        }
    });
};

/*
 * Returns true if the current location url contains a valid media viewer hash
 */
MediaViewer.prototype.hasMediaViewerHashInUrl = function() {
    return MEDIA_VIEWER_HASHES.some(hash => window.location.hash.indexOf(hash) === 0);
};

/*
 * Opens the media viewer and shows the view
 */
MediaViewer.prototype.open = function() {
    const component = this;
    const onAnimationEndHandler = function() {
        component.$element.attr('aria-hidden', 'false');
    };

    if (component.isOpen()) {
        return;
    }

    if (component.animationsSupported) {
        component.$element.one(ANIMATION_END_EVENTS, onAnimationEndHandler);
    } else {
        onAnimationEndHandler();
    }

    // Open Media Viewer and disable scrolling
    scrollPosition = $(window).scrollTop();

    $body.css('top', -1 * scrollPosition);
    $body.addClass(BODY_OPEN_CLASS);

    component.$element.addClass(OPEN_CLASS);
};

/*
 * Closes the media viewer and hides any view that may be visible
 */
MediaViewer.prototype.close = function() {
    const component = this;
    let onAnimationEndHandler;

    if (component.isOpen()) {
        component.$element.removeClass(OPEN_CLASS).addClass(CLOSING_CLASS);

        // After closing Media Viewer enable scrolling
        $body.css('top', '');
        $body.removeClass(BODY_OPEN_CLASS);

        // Set scroll position, to prevent page flicking
        $(window).scrollTop(scrollPosition);

        onAnimationEndHandler = function() {
            component.clearViews();
            component.$element.removeClass(CLOSING_CLASS);
            component.$element.attr('aria-hidden', 'true');
        };

        if (component.animationsSupported) {
            component.$element.one(ANIMATION_END_EVENTS, onAnimationEndHandler);
        } else {
            onAnimationEndHandler();
        }

        if (window.location.hash) {
            if (component.isMediaViewerAccessedDirectly === true) {
                // User entered the media viewer directly (e.g. from external source), expected behaviour is to go to the detail page
                component.isMediaViewerAccessedDirectly = false;
                component.replaceState(window.location.pathname.split(window.location.hash)[0]);
            } else {
                // In any other case, just go back in history
                window.history.go(-1);
            }
        }
    }
};

/*
 * Replaces the current browser state with newState in the best way possible, with browser support in mind
 */
MediaViewer.prototype.replaceState = function(newState) {
    if ('replaceState' in window.history) {
        // Use of history.replaceState is preferred because it allows the animation to finish and seems to be faster
        window.history.replaceState({}, '', newState);
    } else {
        // IE 9 fallback
        // Doesn't work as nice as history.replaceState (url in browser is not updated, history navigation is messed up) but it's something
        window.location.replace(newState);
    }
};

MediaViewer.prototype.refreshView = function() {
    const component = this;
    let $elements;
    const locationHash = window.location.hash;
    component.clearViews();

    if (locationHash.indexOf(FOTO_HASH) === 0) {
        const fotoIndex = locationHash.split(FOTO_HASH)[1];
        $elements = $(FOTO_VIEWER_SELECTOR);
        $elements.removeClass(HIDDEN_CLASS);
        component.fotosView = new FotosView($elements, fotoIndex);
    } else if (locationHash.indexOf(VIDEO_HASH) === 0) {
        $elements = $(VIDEO_VIEWER_SELECTOR); // for video a single element
        $elements.removeClass(HIDDEN_CLASS);
        component.videoView = new VideoView($elements[0]);
    } else if (locationHash.indexOf(FOTO360_HASH) === 0) {
        const foto360Id = locationHash.split(FOTO360_HASH)[1];
        $elements = $(FOTO360_VIEWER_SELECTOR);  // for 360 also  a single element
        $elements.removeClass(HIDDEN_CLASS);
        component.foto360View = new Foto360View($elements[0], foto360Id);
    } else if (locationHash.indexOf(PLATTEGROND_HASH) === 0) {
        const plattegrondId = locationHash.split(PLATTEGROND_HASH)[1];
        $elements = $(PLATTEGROND_VIEWER_SELECTOR);
        $elements.removeClass(HIDDEN_CLASS);
        component.plattegrondView = new PlattegrondView($elements, plattegrondId);
    }
};

MediaViewer.prototype.clearViews = function() {
    const component = this;
    const $allViewElements = $(FOTO_VIEWER_SELECTOR + ', ' + VIDEO_VIEWER_SELECTOR + ', ' + FOTO360_VIEWER_SELECTOR + ', ' + PLATTEGROND_VIEWER_SELECTOR);
    $allViewElements.addClass(HIDDEN_CLASS);
    if (typeof component.fotosView === 'object') {
        component.fotosView.dispose();
    }
    if (typeof component.videoView === 'object') {
        component.videoView.dispose();
    }
    if (typeof component.foto360View === 'object') {
        component.foto360View.dispose();
    }
    if (typeof component.plattegrondView === 'object') {
        component.plattegrondView.dispose();
    }
};

/*
 * Returns true if the media viewer is open.
 */
MediaViewer.prototype.isOpen = function() {
    return this.$element.hasClass(OPEN_CLASS);
};

/*
 * Returns true if the user's browser supports CSS animations
 * https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Animations/Detecting_CSS_animation_support
 *
 * @returns {boolean}
 */
function browserSupportsAnimations() {
    const domPrefixes = 'Webkit Moz O ms Khtml'.split(' ');
    const elm = document.createElement('div');

    if (elm.style.animationName !== undefined) {
        return true;
    }

    for (let i = 0; i < domPrefixes.length; i++) {
        if (elm.style[domPrefixes[i] + 'AnimationName'] !== undefined) {
            return true;
        }
    }

    return false;
}

// turn all elements with the default selector into components
$(COMPONENT_SELECTOR).each((index, element) => new MediaViewer(element));
