'use strict';

module.exports = FotosView;

var $ = require('jquery');
require('funda-slick-carousel');
import Advertisements from '../advertisements-lazy/advertisements-lazy';

var EVENT_NAMESPACE = '.media-viewer-fotos-view';
var ITEMS_SELECTOR = '[data-media-viewer-items]';
var PREVIOUS_SELECTOR = '[data-media-viewer-previous]';
var NEXT_SELECTOR = '[data-media-viewer-next]';
var CURRENT_SELECTOR = '[data-media-viewer-current]';
var TOTAL_SELECTOR = '[data-media-viewer-total]';
var ADVERTISEMENT_SELECTOR = '[data-advertisement]';
var SLICK_SLIDE_SPEED = 250;
var SLICK_SWIPE_SENSITIVITY = 12; // 1/12th of the image width

var FOTOSTOPPER = 'data-media-viewer-fotostopper';
var REWIND_SELECTOR = '[data-media-viewer-fotostopper-rewind]';

function FotosView($elements, fotoIndex) {
    var component = this;
    component.$items = $elements.find(ITEMS_SELECTOR);
    component.$prevButton = $elements.find(PREVIOUS_SELECTOR);
    component.$nextButton = $elements.find(NEXT_SELECTOR);
    component.$currentItemCount = $elements.find(CURRENT_SELECTOR);
    component.$totalItemCount = $elements.find(TOTAL_SELECTOR);
    component.$rewindButton = $elements.find(REWIND_SELECTOR);

    component.bindEvents();
    Advertisements.lazyInitialize($elements.first()[0].querySelectorAll(ADVERTISEMENT_SELECTOR), 'funda');

    component.initializeSlideshow(fotoIndex);
    component.bindGlobalEvents();
    component.setItem(fotoIndex);
}

FotosView.prototype.dispose = function() {
    var component = this;
    component.unbindEvents();
    component.unbindGlobalEvents();
    component.disableSlideshow();
};

FotosView.prototype.bindEvents = function() {
    var view = this;

    view.$nextButton.on('click', view.nextItem.bind(view));
    view.$prevButton.on('click', view.prevItem.bind(view));
    view.$rewindButton.on('click', view.setItem.bind(view, 1));

    view.$items.on('init reInit', slickInitHandler.bind(view));
    view.$items.on('beforeChange', slickBeforeChangeHandler.bind(view));
};

FotosView.prototype.unbindEvents = function() {
    var view = this;

    view.$nextButton.off('click');
    view.$prevButton.off('click');
    view.$items.off('init reInit');
    view.$items.off('beforeChange');
};

FotosView.prototype.bindGlobalEvents = function() {
    if (this.globalEventsBound) {
        return;
    }
    var view = this;
    $(document).on('keydown' + EVENT_NAMESPACE, function keyHandler(event) {
        if (event.ctrlKey || event.shiftKey || event.altKey || event.metaKey) {
            return;
        }

        switch (event.keyCode) {
            case 37: //arrow left
                view.prevItem();
                break;
            case 39: // arrow right
                view.nextItem();
                break;
        }
    });

    this.globalEventsBound = true;
};

FotosView.prototype.unbindGlobalEvents = function() {
    if (this.globalEventsBound) {
        $(document).off('keydown' + EVENT_NAMESPACE);
        this.globalEventsBound = false;
    }
};

FotosView.prototype.updateState = function(state) {
    if ('pushState' in window.history) {
        window.history.replaceState({}, '', state);
    } else {
        window.location.replace(state);
    }
};

FotosView.prototype.initializeSlideshow = function(initialSlide) {
    var component = this;

    if (component.slideshowIsInitialized) {
        return;
    }

    component.$items.slick({
        arrows: false,
        speed: SLICK_SLIDE_SPEED,
        cssEase: 'ease',
        accessibility: true,
        infinite: false,
        mobileFirst: true,
        touchThreshold: SLICK_SWIPE_SENSITIVITY,
        initialSlide: initialSlide - 1 || 0,
        useTransform: true,
        lazyLoad: 'anticipated',
        lazyLoadAhead: 3,
        lazyLoadBack: 1
    });

    component.slideshowIsInitialized = true;
};

FotosView.prototype.disableSlideshow = function() {
    this.$items.slick('unslick');
    this.slideshowIsInitialized = false;
};

FotosView.prototype.setItem = function(item) {
    this.$items.slick('slickGoTo', (item - 1), true);
};

FotosView.prototype.prevItem = function() {
    // Go to previous photo, but this time without animation
    this.$items.slick('slickPrev', true);
};

FotosView.prototype.nextItem = function() {
    // Go to next photo, but this time without animation
    this.$items.slick('slickNext', true);
};

function slickInitHandler(event, slick) {
    var hasFotoStopper = slick.$slides[slick.$slides.length - 1].hasAttribute(FOTOSTOPPER);

    this.$totalItemCount.text(hasFotoStopper ? slick.slideCount - 1 : slick.slideCount);
}

function slickBeforeChangeHandler(event, slick, currentSlide, nextSlide) {
    var isGoingToBeFotoStopper = slick.$slides[nextSlide].hasAttribute(FOTOSTOPPER);
    var nextSlideNumber = parseInt(nextSlide, 10) + 1;

    if (isGoingToBeFotoStopper) {
        nextSlideNumber--;
    }

    this.$currentItemCount.text(nextSlideNumber);
    this.updateState('#foto-' + nextSlideNumber);
    this.$nextButton.attr('disabled', slick.slideCount === (nextSlide + 1));
    this.$prevButton.attr('disabled', nextSlide === 0);
}
