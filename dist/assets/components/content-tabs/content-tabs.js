// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

// explicitly inject dependencies (alphabetically), only those needed
import $ from 'jquery';

// what does this module expose?
export default ContentTabs;

// component configuration
const COMPONENT_SELECTOR = '[data-content-tabs]';
const COMPONENT_TRIGGER = '[data-tabs-trigger]';
const COMPONENT_TARGET = '[data-tabs-target]';

function ContentTabs(element) {
    const component = this; // Not necessary in ES2015, keep using it for consistency
    component.initTabs(element);
}

ContentTabs.prototype.initTabs = function(element) {
    const component = this;
    component.$element = $(element);

    component.$element.find(COMPONENT_TARGET).hide();
    component.$element.find(COMPONENT_TARGET).first().show();
    component.$element.find(COMPONENT_TRIGGER).children('li').first().addClass('active');

    component.$element.find(COMPONENT_TRIGGER)
        .find('a:not([data-expandible-handle])').click(function(event) {
            event.preventDefault();

            $(COMPONENT_TRIGGER)
                .find('li').removeClass('active');

            $(this).parent('li').addClass('active');

            const isCurrentTab = $(this).attr('href');
            $(COMPONENT_TARGET).hide();
            $(isCurrentTab).show();
        });
};

// turn all elements with the default selector into components
$(COMPONENT_SELECTOR).each(function(index, element) {
    return new ContentTabs(element);
});
