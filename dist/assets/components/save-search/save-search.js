// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

// explicitly inject dependencies (alphabetically), only those needed
var $ = require('jquery');
import Expandible from '../expandible/expandible';
var LoginDialog = require('../login-dialog/login-dialog');
var AppSpinner = require('../app-spinner/app-spinner');

// what does this module expose?
module.exports = UserSaveSearch;

// component configuration
var COMPONENT_ATTR = 'data-save-search';
var COMPONENT_SELECTOR = '[' + COMPONENT_ATTR + ']';
var HANDLE_ATTR = 'data-save-search-handle';
var HANDLE_SELECTOR = '[' + HANDLE_ATTR + ']';
var SUBMIT_BUTTON_SELECTOR = '[data-save-search-submit-button]';
var SAVE_SEARCH_DIALOG_SELECTOR = '[data-dialog="save-search-form"]';
var DIALOG_SUBMIT_BUTTON_SELECTOR = SAVE_SEARCH_DIALOG_SELECTOR + ' ' + SUBMIT_BUTTON_SELECTOR;
var FLYOUT_FORM_SELECTOR = '[data-save-search="flyout"]';
var EXPANDIBLE_SELECTOR = '[data-expandible]';
var LOGIN_DIALOG_SELECTOR = '[data-dialog="user-form"]';
var CONTAINER_ATTR = 'data-save-search-container';
var SPINNER_SELECTOR = '[data-save-search-spinner]';
var LOADED_CLASS = 'is-loaded';
var EXPANDED_CLASS = 'is-expanded';
var OPEN_CLASS = 'is-open';
var FLYOUT_CLOSE = '.search-close-flyout';
var HISTORISCH_RADIO_GROUP_SELECTOR = '[data-radio-group="IndHistorisch"] [data-radio-group-input]';
var SAVE_SEARCH_TOGGLE_SELECTOR = '.search-sidebar .search-save';
var HISTORICFILTER_SELECTOR = '[name="filter_IndHistorisch"][value="True"]';
var RESULTLIST_SELECTOR = '.search-main';
var HISTORIC_CLASS = 'historic';

var $body = $('body');

function UserSaveSearch(element) {
    var component = this;
    component.element = element;
    component.$element = $(element);
    component.saveSearchId = element.getAttribute(COMPONENT_ATTR) || false;
    component.$saveSearchHandle = $body.find(HANDLE_SELECTOR);
    component.$container = $body.find('[' + CONTAINER_ATTR + '="' + component.saveSearchId + '"]');

    $body.on('click', '[' + HANDLE_ATTR + '="' + component.saveSearchId + '"]', function(event) {
        event.preventDefault();

        if (component.saveSearchId == 'dialog') {
            var dialogElement = document.querySelector('[data-dialog="save-search-form"]');
            dialogElement.dialog.onFormSuccess = function() {
                // on successful login a lot of things need to update on the page,
                // so it's easier just to reload the entire page entirely.
            };

            component.dialog = dialogElement.dialog;
            component.dialog.open();
            component.dialog.getContent($(this).attr('href'));

        } else {
            var isVisible = $(this).hasClass(EXPANDED_CLASS) || $(this).hasClass(OPEN_CLASS);

            component.$container.removeClass(LOADED_CLASS);
            component.$element.html('');

            if (isVisible) {
                component.getForm(this);
            }
        }
    });

    $(document).on('resultsUpdating', function() {
        if ($(HISTORICFILTER_SELECTOR).is(':checked')) {
            $(RESULTLIST_SELECTOR).addClass(HISTORIC_CLASS);
        } else {
            $(RESULTLIST_SELECTOR).removeClass(HISTORIC_CLASS);
        }
    });

    component.$element.on('click', SUBMIT_BUTTON_SELECTOR, function(event) {
        if (component.saveSearchId == 'flyout') {
            $(FLYOUT_CLOSE).click();
        }
        component.submitForm(event);
    });

    if (component.saveSearchId == 'dialog') {
        $body.on('click', DIALOG_SUBMIT_BUTTON_SELECTOR, function(event) {
            component.submitForm(event);
        });
    }
    $body.on('change', HISTORISCH_RADIO_GROUP_SELECTOR, function(event) {
        var isDefault = $(event.currentTarget).is('[data-radio-group-default]');
        if (!isDefault) {
            $(SAVE_SEARCH_TOGGLE_SELECTOR).addClass('is-hidden');
        } else {
            $(SAVE_SEARCH_TOGGLE_SELECTOR).removeClass('is-hidden');
        }
    });
}

UserSaveSearch.prototype.submitForm = function(event) {
    var component = this;
    event.preventDefault();
    component.isUserLoggedIn(function onSuccessfulLogin(loginPerformed) {
        if (loginPerformed) {
            $('.search-map-content__filter-button').click();
        }
        component.doRequest(component.$saveSearchHandle.attr('href'));
    });
};

UserSaveSearch.prototype.getForm = function(element) {
    var component = this;
    var url = element.href;
    component.spinner = new AppSpinner(SPINNER_SELECTOR);
    component.spinner.show();

    return $.ajax({
        url: url,
        data: {
            currentUrl: window.location.pathname
        },
        success: function(response) {
            if (response.Result === 'OK') {
                component.$element.append(response.Html);
                //extra expandible for mobile - toon oude opdracht
                var extraExpandibleElement = component.$element.find(EXPANDIBLE_SELECTOR);
                var hasExtraExpandibleElement = extraExpandibleElement.length;
                if (hasExtraExpandibleElement) {
                    component.extraExpandible = new Expandible(extraExpandibleElement[0]);
                }
            }
        },
        error: function(response) {
            console.error('Error trying to open save search form', url, response);
        },
        complete: function() {
            component.spinner.hide();
            component.$container.addClass(LOADED_CLASS);
        }
    });
};

UserSaveSearch.prototype.isUserLoggedIn = function(onLoggedIn) {
    var component = this;
    var dialogElement = document.querySelector(LOGIN_DIALOG_SELECTOR);

    var url = dialogElement.dialog.isUserLoggedInUrl;

    return $.ajax({
        url: url,
        success: function(response) {
            if (response.LoggedIn === true) {
                onLoggedIn(false);
            } else {
                component.userLoginStatus = new LoginDialog(response.LoginUrl, function onSuccessfulLogin() {
                    onLoggedIn(true);
                });
            }
        },
        error: function(response) {
            console.error('Error calling', url, response);
        }
    });
};

UserSaveSearch.prototype.doRequest = function(url) {
    var component = this;
    var data = component.$element.find(':input').serialize();
    var isKaartPage = component.$element.parents('.search-save.kaart-page').length > 0;
    // rewrite requests from /mijn/savedsearch/kaart/koop/ to /mijn/savedsearch/koop/
    url = url.replace('/kaart', '');
    if (component.saveSearchId == 'dialog') {
        data = $(SAVE_SEARCH_DIALOG_SELECTOR).find(':input').serialize();
    } else if (component.saveSearchId == 'flyout') {
        data = $(FLYOUT_FORM_SELECTOR).find(':input').serialize();
    }

    return $.ajax({
        method: 'POST',
        url: url,
        data: data,
        success: function onSuccess(response) {
            if (response.Result !== 'OK') {
                console.error('Error trying to save search', response);
            } else if (component.saveSearchId == 'dialog') {
                $('.current-query-kaart').html(response.Html);
                $('.overwrite-alert').remove();
                $('.save-search-actions *:not(.map-dialog-visible)').remove();
                $('.notification .notification-buttons').remove();
                $('.save-search-actions .map-dialog-visible').show();
                $('.save-search-actions .map-dialog-visible').click(function() {
                    let $this = $(this);
                    let $dlgContent = $this.parents('.dialog-content');
                    $dlgContent.find('[data-dialog-close]').click();
                });
            } else if (component.saveSearchId == 'flyout' && isKaartPage) {
                $('.search-sidebar .filter-reset').before(response.Html);
                $('.notification .notification-buttons').remove();
                setTimeout(function() {
                    $('.search-sidebar .notification').remove();
                }, 3000);
            }
        },
        error: function onError(response) {
            console.error('Error trying to save search', response);
        },
        complete: function() {
            if ((component.saveSearchId == 'flyout' && !isKaartPage) || component.saveSearchId == 'expandible') {
                window.location.reload();
            }
        }
    });
};

// turn all elements with the default selector into components
$(COMPONENT_SELECTOR).each(function(index, element) {
    return new UserSaveSearch(element);
});
