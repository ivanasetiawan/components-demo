// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

// explicitly inject dependencies (alphabetically), only those needed
var $ = require('jquery');

// what does this module expose?
module.exports = Range;

// component configuration
var COMPONENT_SELECTOR = '[data-range]';
var RANGE_FROM_SELECTOR = '[data-range-min]';
var RANGE_TO_SELECTOR = '[data-range-max]';

function Range(element) {
    var component = this;
    component.$element = $(element);
    component.$min = $(element).find(RANGE_FROM_SELECTOR);
    component.$max = $(element).find(RANGE_TO_SELECTOR);
    component.minValue = 0;
    component.maxValue = false;
    component.fromType = '';
    //check if element for min value and dependable element exists otherwise dont start component
    if (component.$min.length > 0 && component.$max.length > 0) {
        component.bindEvents();
    } else {
        return false;
    }
}

Range.prototype.bindEvents = function() {
    var component = this;

    component.$min.on('change', function() {
        component.minInputUpdated(component.$min[0].type);
        component.$element.trigger('rangechanged');
    });

    component.$max.on('change', function() {
        component.maxInputUpdated(component.$max[0].type);
        component.$element.trigger('rangechanged');
    });
};

/**
 * minInputUpdated
 * @param  {String} elementType Element type that trigged this function call
 * @return {Object}             Returns the updated object, in this case a jQuery object
 */
Range.prototype.minInputUpdated = function(elementType) {

    var component = this;

    var minInputValue = parseFloat(component.$min.val());
    var maxInputValue = parseFloat(component.$max.val());

    if (minInputValue > maxInputValue) {
        if (elementType === 'number' || elementType === 'text') {
            component.$max.val(minInputValue);
        } else {
            component.$max.val(component.getSelectableOption(minInputValue, component.$max[0]));
        }
    }

    return component.$max;
};

/**
 * maxInputUpdated
 * @param  {String} elementType Element type that trigged this function call
 * @return {Object}             Returns the updated object, in this case a jQuery object
 */
Range.prototype.maxInputUpdated = function(elementType) {

    var component = this;

    var minInputValue = parseFloat(component.$min.val());
    var maxInputValue = parseFloat(component.$max.val());

    if (maxInputValue < minInputValue) {
        if (elementType === 'number' || elementType === 'text') {
            component.$min.val(maxInputValue);
        } else {
            component.$min.val(component.getSelectableOption(maxInputValue, component.$min[0]));
        }
    }

    return component.$min;
};

/**
 * getSelectableOption
 * Returns a sanitized value that exists on the target element, for select with different options
 * @param  {Float} value    Value that you want check if exists as a option on the target element
 * @param  {Object} element Target element to check for values
 * @return {Float}          Valid selectable value
 */
Range.prototype.getSelectableOption = function(value, element) {

    var selectableOption = null;
    var availableOptions = [];

    //check if there is a matching value on the available option values
    for (var i = element.options.length - 1; i >= 0; i--) {
        availableOptions.push(parseFloat(element.options[i].value));
        if (parseFloat(element.options[i].value) === value) {
            selectableOption = value;
        }
    }

    if (selectableOption === null) {
        // gets the closest value from the available options
        selectableOption = availableOptions.reduce(function(prev, curr) {
            return (Math.abs(curr - value) < Math.abs(prev - value) ? curr : prev);
        });
    }
    return selectableOption;
};

// turn all elements with the default selector into components
$(COMPONENT_SELECTOR).each(function(index, element) {
    return new Range(element);
});
