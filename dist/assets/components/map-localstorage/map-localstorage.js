'use strict';

const VIEWED_IDS_KEY = 'viewedIds';
const MAX_VIEWED_ITEMS = 1000;


function MapLocalStorage() {
    this._hasLocalStorage = this.browserHasLocalStorage();
}

/**
 * Determine if localStorage is supported and available
 */
MapLocalStorage.prototype.browserHasLocalStorage = function() {
    try {
        let test = '__storage_test__';
        window.localStorage.setItem(test, test);
        window.localStorage.removeItem(test);
        return true;
    } catch (e) {
        this.logError(e.message);
        return false;
    }
};

/**
 * Stores the id of a viewed object
 */
MapLocalStorage.prototype.storeViewedId = function(id) {
    if (!this._hasLocalStorage) {
        return;
    }

    let viewed = this.getViewed();
    // act when not already present
    if (id in viewed) {
        return;
    }
    // delete oldest item from storage
    if (Object.keys(viewed).length >= MAX_VIEWED_ITEMS) {
        this.deleteOldestViewedItem();
    }
    viewed[id] = {
        ts: parseInt(new Date().getTime() / 1000, 10)
    };
    this.setViewed(viewed);
};

/**
 * returns true if object is present in localstorage
 */
MapLocalStorage.prototype.isViewed = function(objectId, capped=false) {
    if (!this._hasLocalStorage || capped) {
        return false;
    }

    let markerId = objectId;
    let singleId = objectId.indexOf('-') < 0;
    let viewed = this.getViewed();

    if (viewed.length == 0) {
        return false;
    }
    if (!singleId) {
        let markerIds = markerId.split('-');
        for (let i = 0; i < markerIds.length; i++) {
            if (!(markerIds[i] in viewed)) {
                return false;
            }
        }
        return true;
    }

    return markerId in viewed;
};

/**
 * Retrieve array from storage (where data is delimited by SPLIT_CHAR)
 */
MapLocalStorage.prototype.getViewed = function() {
    var data = window.localStorage.getItem(VIEWED_IDS_KEY);
    if (data) {
        return JSON.parse(data);
    }
    return {};
};

/**
 * Saves array to storage (where data is delimited by SPLIT_CHAR)
 */
MapLocalStorage.prototype.setViewed = function(value) {
    window.localStorage.setItem(VIEWED_IDS_KEY, JSON.stringify(value));
};

/**
 * Remove oldest viewed item from localstorage
 */
MapLocalStorage.prototype.deleteOldestViewedItem = function() {
    let current = this.getViewed();
    let max = 0;
    let maxId = null;
    for (let item in current) {
        if (current[item].ts > max) {
            max = current[item];
            maxId = item;
        }
    }
    if (maxId != null && current[maxId]) {
        delete current[maxId];
    }
    this.setViewed(current);
};

/**
 * log the error for Google Analytics
 * @param {String} errrorType
 */
MapLocalStorage.prototype.logError = function(errorType) {
    if (window.gtmDataLayer !== undefined) {
        window.gtmDataLayer.push({
            'event': 'kaartLocalStorageError',
            'errorType': 'localstorage-' + errorType
        });
    }
};

export default new MapLocalStorage();
