// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

// explicitly inject dependencies (alphabetically), only those needed
import $ from 'jquery';

export default SearchMapContent;

// component configuration
const COMPONENT_SELECTOR = '[data-search-map-content]';
const MAP_SELECTOR = '[data-map]';
const URL_ATTR = 'data-search-map-content-url';
const AUTOCOMPLETE_SELECTOR = '[data-autocomplete]';
const SUBMIT_LOCATION_SELECTOR = '[data-search-map-content-submit-location]';
const GEO_INDENTIFIER_SELECTOR = '[data-instant-search-output="zoekboxHiddenValue"]';
const FORM_SELECTOR = '[data-instant-map-search]';
const GEO_SELECTOR = '#filter_Geo';
const NEW_GEO_SELECTOR = '#filter_NewGeo';

const SEARCH_QUERY_UPDATED_EVENT = 'searchqueryupdated';

function SearchMapContent(element) {
    const component = this;

    component.bindToElements(element);
    component.bindEvents();

    $(window).on('sidebar-refresh', function() {
        var $myNewElement = $(COMPONENT_SELECTOR);
        if ($myNewElement.length == 1) {
            component.bindToElements($myNewElement);
            component.bindEvents();
        }
    });
}

SearchMapContent.prototype.bindToElements = function (element) {
    const component = this;

    component.$element = $(element);
    component.$map = $(MAP_SELECTOR);
    component.url = component.$element.attr(URL_ATTR);
    component.$autocomplete = component.$element.find(AUTOCOMPLETE_SELECTOR);
    component.$submitButton = component.$element.find(SUBMIT_LOCATION_SELECTOR);
    component.$geoIdentifier = component.$element.find(GEO_INDENTIFIER_SELECTOR);
    component.$geo = $(FORM_SELECTOR).find(GEO_SELECTOR);
    component.$newGeo = $(FORM_SELECTOR).find(NEW_GEO_SELECTOR);
};

SearchMapContent.prototype.bindEvents = function() {
    const component = this;

    component.$autocomplete.on(SEARCH_QUERY_UPDATED_EVENT, () => component.submitLocation());
    component.$submitButton.on('click', () => component.submitLocation());

    $(document).on('resultsUpdated', () => component.$newGeo.val(''));
};

SearchMapContent.prototype.submitLocation = function() {
    const component = this;
    const locationId = component.$geoIdentifier.val();

    if (locationId !== '' && locationId !== '0') {
        component.changeCenterMap(locationId);
    }
};

SearchMapContent.prototype.changeCenterMap = function(locationId) {
    const component = this;

    component.$geo.val(locationId);
    component.$newGeo.val(locationId);
    component.$newGeo.trigger('change');
};

// turn all elements with the default selector into components
$(COMPONENT_SELECTOR).each((index, element) => new SearchMapContent(element));
