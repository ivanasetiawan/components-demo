// Require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

// Explicitly inject dependencies (alphabetically), only those needed
var $ = require('jquery');
require('funda-slick-carousel');

// What does this module expose?
module.exports = ObjectMedia;

// Component configuration
var EVENT_NAMESPACE = '.object-media';
var COMPONENT_SELECTOR = '[data-object-media]';
var ITEMS_SELECTOR = '[data-object-media-fotos]';
var SLICK_SLIDE_SPEED = 250;
var SLICK_SWIPE_SENSITIVITY = 12; // 1/12th of the image width

function ObjectMedia(element) {
    var component = this;
    var mediaQuery;

    component.$element = $(element);
    component.$items = component.$element.find(ITEMS_SELECTOR);
    component.slickWidth = 1020; // object-media.scss

    if (window.matchMedia) {
        mediaQuery = window.matchMedia('(max-width: ' + component.slickWidth + 'px)');
        mediaQuery.addListener(function(mediaQueryList) {
            if ((mediaQueryList.matches)) {
                // Became narrower than breakpoint
                component.initSlick();
            } else {
                component.unbindEvents();
                component.killSlick();
            }
        });

        if ((mediaQuery.matches)) {
            component.initSlick();
        }
    }
    // Only IE9 does not support matchMedia() and that desktop browser does not need Slick anyway
}

ObjectMedia.prototype.unbindEvents = function() {
    $(document).off('keydown' + EVENT_NAMESPACE);
};

ObjectMedia.prototype.initSlick = function() {
    var component = this;
    component.$items.slick(
        {
            arrows: false,
            infinite: false,
            speed: SLICK_SLIDE_SPEED,
            cssEase: 'ease',
            mobileFirst: true,
            touchThreshold: SLICK_SWIPE_SENSITIVITY,
            useTransform: true,
            lazyLoad: 'anticipated',
            lazyLoadAhead: 3
        }
    );
};

ObjectMedia.prototype.killSlick = function() {
    this.$items.slick('unslick');
};

// Turn all elements with the default selector into components
$(COMPONENT_SELECTOR).each(function(index, element) {
    return new ObjectMedia(element);
});
