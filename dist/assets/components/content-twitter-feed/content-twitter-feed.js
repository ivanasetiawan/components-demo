// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

// explicitly inject dependencies (alphabetically), only those needed
import $ from 'jquery';

// what does this module expose?
export default ContentTwitterFeed;

// component configuration
const TWITTER_FEED_COMPONENT_ATTR = 'data-content-twitter-feed';
const TWITTER_FEED_COMPONENT_SELECTOR = '[' + TWITTER_FEED_COMPONENT_ATTR + ']';
const TWITTER_FEED_LIST_TEMPLATE_SELECTOR = '[data-content-twitter-feed-list]';
const TWITTER_FEED_TWEET_TEMPLATE_SELECTOR = '[data-content-twitter-feed-item-template]';
const TWITTER_FEED_TWEET_COUNT = '3';
const TWITTER_FEED_RELOAD_HANDLE = '[data-content-twitter-feed-reload-handle]';
const TWITTER_FEED_HAS_RESULTS_CLASS = 'has-results';

function ContentTwitterFeed(element) {
    const $component = this;

    $component.$element = $(element);
    $component.apiEndpointPage = 1;
    $component.apiEndpointURL = $component.$element.attr(TWITTER_FEED_COMPONENT_ATTR);
    $component.template = $component.$element.find(TWITTER_FEED_TWEET_TEMPLATE_SELECTOR).html();
    $component.$list = $component.$element.find(TWITTER_FEED_LIST_TEMPLATE_SELECTOR);
    $component.$reloadButton = $component.$element.find(TWITTER_FEED_RELOAD_HANDLE);

    $component.getTweets();

    $component.$reloadButton.on('click', function() {
        $component.getTweets();
    });
}

ContentTwitterFeed.prototype.getTweets = function() {
    const component = this;

    $.ajax({
        url: component.apiEndpointURL,
        data: {
            count: TWITTER_FEED_TWEET_COUNT,
            page: component.apiEndpointPage
        }
    })

    .then(response => {
        if (!response.length) {
            throw Error('No tweets.');
        }

        return response;
    })

    .then(response => {
        const html = component.renderTweets(component.template, response);
        component.update(html);
    })

    .catch(
        (err) => console.error('Error: ', err)
    );
};

ContentTwitterFeed.prototype.update = function(html) {
    const $component = this;

    $component.$list.append(html);
    $component.$element.addClass(TWITTER_FEED_HAS_RESULTS_CLASS);

    $component.apiEndpointPage++;
};

ContentTwitterFeed.prototype.render = function(template, data) {
    return Object.keys(data).reduce((html, prop) => {
        var pattern = new RegExp('{' + prop + '}', 'g');

        if (prop === 'ProfileImageUrl') {
            return html.replace(pattern, data[prop].replace('_normal', ''));
        }

        return html.replace(pattern, data[prop]);
    }, template);
};

ContentTwitterFeed.prototype.renderTweets = function(template, tweets) {
    const component = this;

    return tweets.map(item => component.render(template, item)).join('');
};

$(TWITTER_FEED_COMPONENT_SELECTOR).each((index, element) => new ContentTwitterFeed(element));
