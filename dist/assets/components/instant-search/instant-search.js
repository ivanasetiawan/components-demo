/* global gtmDataLayer */
'use strict';

// explicitly inject dependencies (alphabetically), only those needed
import $ from 'jquery';
import ajax from '../ajax/ajax';
import debounce from 'lodash/debounce';
import AppliedFilters from '../applied-filters/applied-filters';
var FilterFlyout = require('../filter-flyout/filter-flyout');

// what does this module expose?
module.exports = InstantSearch;

// component configuration
var COMPONENT_SELECTOR = '[data-instant-search]';
var HANDLE_SELECTOR = '[data-instant-search-handle]';
var RANGE_SELECTOR = '[data-instant-search-range]';
var OUTPUT_KEY_ATTR = 'data-instant-search-output';
var OUTPUT_SELECTOR = '[data-instant-search-output]';
var SEARCH_SIDEBAR_SELECTOR = '[data-search-sidebar]';
var SIDEBAR_CLOSED_EVENT = 'sidebarclosed';
var INVALID_LOCATION_EVENT = 'invalidlocation';
var AUTOCOMPLETE_SELECTOR = '[data-autocomplete]';
var RADIUS_SELECTOR = '[data-input-radius] select';
var ZOEKTYPE_SELECTOR = 'select[data-zoektype]';
var MAKELAARNAAM_SELECTOR = '[data-makelaars-name]';
var RANGE_FLYOUT_SELECTOR = '[data-filter-flyout-range]';
var COUNT_ATTR = 'data-instant-search-count';
var SOLD_OBJECTS_ANCHOR_SELECTOR = '[data-instant-search-sold-objects]';
var FOR_SALE_OBJECTS_SELECTOR = '[data-instant-search-for-sale-objects]';
var PAGINATION_SELECTOR = '[data-pagination]';
var FILTER_COUNT_NUMBER = '[data-filter-count-number]';
var UPDATING_CLASS = 'is-updating';
var EXTENDED_CLASS = 'is-extended';
var NO_RESULTS_CLASS = 'has-no-results';
var MULTILINE_CLASS = 'multiline';
var URL_SUFFIX = '?ajax=true';
var DYNAMIC_FILTER_FIELD = 'wijkNaamFilter';
var DYNAMIC_APPLIED_FILTER = 'buurt';
var DYNAMIC_FILTER_SELECTOR = '[data-dynamic-filter]';
var PUSH_STATE_MARKER = 'push_state_set_by_funda';
var MINIMUM_INTERVAL_BETWEEN_REQUESTS = 500; // ms
var TIMEOUT_SEARCH_REQUEST = 30000; // 30 sec

var PRICE_RANGE_SELECTOR = '[data-range-filter]';
var RADIO_GROUP_SELECTOR = '[data-radio-group]';

var supportsPushState = ('pushState' in window.history);
var isCapableBrowser = (supportsPushState);

/**
 * @param {HTMLFormElement} form
 * @constructor
 */
function InstantSearch(form) {
    if (!isCapableBrowser) {
        return;
    }

    var component = this;
    component.bindToElements(form);
    component.bindToEvents();

    // initialize popstate
    window.history.replaceState(PUSH_STATE_MARKER, window.title);
}

InstantSearch.prototype.bindToElements = function(form) {
    var component = this;

    var $form = $(form);
    component.form = form;
    component.$form = $form;
    component.outputMap = {};
    component.request = null; // store pending xhr request
    component.asyncResult = {};
    component.soldFilterOption = component.$form.find(SOLD_OBJECTS_ANCHOR_SELECTOR);
    component.forSaleFilterOption = component.$form.find(FOR_SALE_OBJECTS_SELECTOR);
    component.stateObject = {};
    component.$filterCountNumber = component.$form.find(FILTER_COUNT_NUMBER);
    component.$outputs = component.getOutputs($('body'));
};

/**
 * Request form submit on form input changes.
 */
InstantSearch.prototype.bindToEvents = function() {
    var component = this;

    component.$form.find(SEARCH_SIDEBAR_SELECTOR).on(SIDEBAR_CLOSED_EVENT, function() {
        if (Object.keys(component.asyncResult).length > 0) {
            component.renderOutputResults(component.asyncResult);
            component.asyncResult = {}; //empty async result
        }
    });

    window.addEventListener('popstate', component.onpopstate, false);

    function debouncedRequest(forceGet) {
        return debounce(function() {
            component.doRequest(forceGet);
        }, MINIMUM_INTERVAL_BETWEEN_REQUESTS, { leading: true });
    }

    component.$form.on('pricerangereset', PRICE_RANGE_SELECTOR, debouncedRequest());
    component.$form.on('radiogroupreset', RADIO_GROUP_SELECTOR, debouncedRequest());
    component.$form.on('change', RADIUS_SELECTOR, debouncedRequest());
    component.$form.on('makelaarsnameupdated', MAKELAARNAAM_SELECTOR, debouncedRequest());
    component.$form.on('change', HANDLE_SELECTOR, debouncedRequest());
    component.$form.on('change', ZOEKTYPE_SELECTOR, debouncedRequest(true));
    component.$form.on('rangechanged', RANGE_SELECTOR, debouncedRequest());
    component.$form.on('rangeselected', RANGE_FLYOUT_SELECTOR, debouncedRequest());
    component.$form.on('pageadjusted', PAGINATION_SELECTOR, debouncedRequest());
    component.$form.on('searchqueryupdated', AUTOCOMPLETE_SELECTOR, debouncedRequest());
    // TODO: refactor all the things related below listener and its trigger
    component.$form.on('range_filter_removed', debouncedRequest());
};

// reload page on browser back only if we have set the state. Safari fires the popstate on pageload (with an empty event state), and we don't want to reload on pageload :)
InstantSearch.prototype.onpopstate = function(event) {
    if (event.state === PUSH_STATE_MARKER) {
        window.location.reload();
    }
};

InstantSearch.prototype.doRequest = function(forceGet) {
    var component = this;
    var formData = component.$form.serialize();
    var queryParams = URL_SUFFIX;
    if (forceGet !== undefined && forceGet === true) {
        queryParams += '&forceget=true';
    }

    // ensure outputs indicate they are updating
    component.$outputs.addClass(UPDATING_CLASS);
    $(document).trigger('resultsUpdating');

    // abort previous request if still pending
    if (component.request) {
        component.request.abort();
    }

    component.request = ajax({
        url: component.form.action + queryParams,
        method: (this.form.method || 'POST').toUpperCase(),
        data: formData,
        timeout: TIMEOUT_SEARCH_REQUEST
    });

    component.request.done(function onSuccess(data) {
        if (data.Error) {
            component.stoppedUpdating();
            component.logError(data.Error);
            component.$form.trigger(INVALID_LOCATION_EVENT);
        } else if (data.forcedRedirectUrl) {
            window.location = data.forcedRedirectUrl;
        } else {
            component.updateResults(data);
        }
    });

    component.request.fail(function onError(err) {
        if (err.statusText !== 'abort') {
            component.stoppedUpdating();
        }
    });
};

/**
 * Determine if results from search request needs to be rendered
 * @param {Object} data
 * @param {Object} data.content
 * @param {String} data.url
 */
InstantSearch.prototype.updateResults = function(data) {
    var component = this;

    // update first all sync elements
    document.title = data.title;
    component.soldFilterOption.attr('href', data.historischAanbodUrl);
    component.soldFilterOption.text(data.historischAanbodLabel);
    component.soldFilterOption.toggleClass(MULTILINE_CLASS, data.multilineHistorischAanbodLabel);
    component.forSaleFilterOption.text(data.actiefAanbodLabel);
    component.forSaleFilterOption.toggleClass(MULTILINE_CLASS, data.multilineActiefAanbodLabel);
    component.updateTotalCount(data.content.total, data.content.searchButtonTotal);
    component.updateCounts(data.counts);
    component.updateHistory(data.url);

    if (component.$form.find(SEARCH_SIDEBAR_SELECTOR).hasClass(EXTENDED_CLASS)) {
        component.asyncResult = data;
    } else {
        component.renderOutputResults(data);
    }
};

/**
 * log the error for Google Analytics
 * @param {String} errorType
 */
InstantSearch.prototype.logError = function(errorType) {
    if (window.gtmDataLayer !== undefined) {
        // Fill GtmDataLayer with data
        gtmDataLayer.push({
            'event': 'searchResultError',
            'errorType': 'invalid-' + errorType
        });
    }
};

/**
 * Render the results from the search request
 * @param {Object} resultData with keys corresponding to output identifiers and their new HTML as values.
 */
InstantSearch.prototype.renderOutputResults = function(resultData) {
    var component = this;
    var fields = resultData.content;

    for (let key in fields) {
        if (fields.hasOwnProperty(key) && component.outputMap[key] && fields[key] !== null) {
            if (component.outputMap[key].is('input')) {
                component.outputMap[key].val(fields[key]);
            } else {
                component.outputMap[key].html(fields[key]);
            }
        }
    }
    if (Object.keys(fields).length > 0) {
        component.stoppedUpdating();
        component.sendResultsUpdatedEvent(resultData.url);
    }

    component.updateBinds(fields);
};

InstantSearch.prototype.sendResultsUpdatedEvent = function(url) {
    $(document).trigger('resultsUpdated', {url: url});
};

/**
 * the results are updated or there is an error occured
 */
InstantSearch.prototype.stoppedUpdating = function() {
    var component = this;
    component.$outputs.removeClass(UPDATING_CLASS);
};

/**
 * @param {Object} counts;   map in which key matches count selectors (eg. `{ "my-id": "1.356" }`).
 */
InstantSearch.prototype.updateCounts = function(counts) {
    var component = this;
    for (var key in counts) {
        if (counts.hasOwnProperty(key)) {
            var $countElement = component.$form.find('[' + COUNT_ATTR + '="' + key + '"]');
            var $countWrapper = $countElement.closest('li');
            var count = counts[key];

            $countElement.text(count);

            if (count === '0') {
                $countWrapper.addClass(NO_RESULTS_CLASS);
            } else {
                $countWrapper.removeClass(NO_RESULTS_CLASS);
            }
        }
    }
};

/**
 * @param {String} totalResults;    The total number of results (eg. '650 resultaten in <location>')
 * @param {String} searchButtonTotalResults;    The total number of results within a button (eg. '650 resultaten')
 *
 * In makelaar result list, there is a difference between totalResults and searchButtonTotalResults
 */
InstantSearch.prototype.updateTotalCount = function(totalResults, searchButtonTotalResults) {
    var component = this;
    component.outputMap.total.each(function(index, element) {
        $(element).text(totalResults);
    });
    component.outputMap.searchButtonTotal.each(function(index, element) {
        $(element).text(searchButtonTotalResults);
    });
};

/**
 * Add url to browser history
 * @param {String} url
 */
InstantSearch.prototype.updateHistory = function(url) {
    window.history.pushState(PUSH_STATE_MARKER, window.title, url);
};

InstantSearch.prototype.updateBinds = function(fields) {
    var component = this;

    if (fields[DYNAMIC_FILTER_FIELD] && fields[DYNAMIC_FILTER_FIELD].length > 1) {
        var $dynamicFilter = component.$form.find(DYNAMIC_FILTER_SELECTOR);
        return new FilterFlyout($dynamicFilter, true);
    } else if (component.$form.find(DYNAMIC_FILTER_SELECTOR).length < 1) {
        /* buurtfilter not present, make sure no remaining buurten are in applied filters */
        AppliedFilters.remove(DYNAMIC_APPLIED_FILTER);
    }
};

/**
 * Get the outputs for the instant-search form.
 * This needs to be a prototype because user-saved-objects-sorting module extends it to find the outputs elswhere
 * @param {Object} $form
 */
InstantSearch.prototype.getOutputs = function($container) {
    var component = this;
    var $temp = $container.find(OUTPUT_SELECTOR);
    $temp.each(function(index, output) {
        var key = output.getAttribute(OUTPUT_KEY_ATTR);
        var $outputs = component.outputMap[key] || $();
        component.outputMap[key] = $outputs.add(output);
    });
    return $temp;
};

// turn all elements with the default selector into components
$(COMPONENT_SELECTOR).each(function(index, element) {
    return new InstantSearch(element);
});
