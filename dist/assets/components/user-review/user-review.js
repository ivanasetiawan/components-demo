'use strict';

import $ from 'jquery';
import Expandible from '../expandible/expandible';

export default UserReview;

// component configuration
const COMPONENT_SELECTOR = '[data-user-review]';
const SCROLL_ATTR = 'data-user-review-scroll';
const OPEN_HANDLE_SELECTOR = '[data-user-review-open]';
const CLOSE_HANDLE_SELECTOR = '[data-user-review-close]';
const DESCRIPTION_SELECTOR = '[data-user-review-description]';
const CONFIG_SELECTOR = '[data-user-review-config]';
const TOGGLE_BUTTONS_SELECTOR = '[data-user-review-footer]';
const FIRST_ELEMENT_SELECTOR = '.user-reviews .user-review:first-of-type';
const ENHANCED_CLASS = 'is-expandible';
const REPLY_ATTR = 'data-user-reply';
const HIDE_BUTTONS_CLASS = 'hide-buttons';
const DEFAULT_CONFIG = {'0px': {'wordCount': 150, 'height': 200}};

/**
 * Constructor method, links child elements to variables for internal use
 *
 * @param {HTMLElement} element The Html element to bind to.
 */
function UserReview(element) {
    const component = this;
    component.$element = $(element);
    component.$toggleButtons = component.$element.find(TOGGLE_BUTTONS_SELECTOR);
    component.$description = component.$element.find(DESCRIPTION_SELECTOR);
    component.expandible = new Expandible(component.$element[0]);
    component.textContent = component.$description.text();
    component.htmlContent = component.$description.html();
    component.originalHeight = component.$description.height();

    // If there is no config on the template, use the default.
    if (component.$element.find(CONFIG_SELECTOR).length > 0) {
        component.config = JSON.parse(component.$element.find(CONFIG_SELECTOR).text());
    } else {
        component.config = DEFAULT_CONFIG;
    }

    component.getBreakpoints(component.config);
    component.bindEvents();

    // Expand the first review by default.
    if (component.$element.parent().is(FIRST_ELEMENT_SELECTOR)) {
        component.expandDescription();
        component.expandible.toggleExpand(true);
    }
}

/**
 * Binds all necessary events for the review.
 */
UserReview.prototype.bindEvents = function() {
    const component = this;
    component.scrollPosition = 0;

    component.$element.on('click', OPEN_HANDLE_SELECTOR, () => {
        component.expandDescription();

        // Set current position on expand.
        if (component.$element[0].hasAttribute(SCROLL_ATTR)) {
            component.scrollPosition = $(window).scrollTop();
        }
    });

    component.$element.on('click', CLOSE_HANDLE_SELECTOR, () => {
        component.getBreakpoints(component.config);

        // Return to position on collapse.
        if (component.scrollPosition > 0) {
            $(window).scrollTop(component.scrollPosition);
        }
    });
};

/**
 * Use the supplied breakpoints from the config to see if there's a match.
 * If there's a match, use it to collapse the description.
 * @param breakpoints
 */
UserReview.prototype.getBreakpoints = function (breakpoints) {
    const component = this;
    const windowSize = $(window).width();
    let matchedBreakpoint;

    Object.getOwnPropertyNames(breakpoints).forEach(function(index) {
        const size = index.substr(0, index.length - 2); // Remove 'px' from values.

        // If we have a matched breakpoint.
        if (windowSize > size) {
            matchedBreakpoint = breakpoints[index];
        }
    });

    // Collapse the description if we're dealing with a expandible $element
    // and breakpoint isn't undefined.
    if (component.$element.hasClass(ENHANCED_CLASS) && typeof matchedBreakpoint !== 'undefined') {
        component.collapseDescription(matchedBreakpoint);
    }
};

/**
 * Expand the description.
 */
UserReview.prototype.expandDescription = function() {
    const component = this;

    // Serve original text, reset $element height, toggle expand.
    component.$description.html(component.htmlContent);
    component.$element.css('height', 'auto');
    component.expandible.toggleExpand(true);
};

/**
 * Collapse the description.
 * Only trim the text if it's (too) long. If so, ecalculate the height to properly close the $element.
 * Depending on the length of the text the toggle buttons are shown/hidden as well.
 * @param breakpoint    matched breakpoint from the config object.
 */
UserReview.prototype.collapseDescription = function(breakpoint) {
    const component = this;
    const parentElement = component.$element.parent();
    const textLength = breakpoint.wordCount;
    const componentHeight = breakpoint.height;
    const reviewText = component.textContent.substr(0, textLength) + ' …';
    const replyText = component.textContent.substr(0, (textLength - 25)) + ' …';

    // Check if we're dealing with a long description text.
    if (component.originalHeight > 100) {
        // First, serve the shortened text.
        if (parentElement[0].hasAttribute(REPLY_ATTR)) {
            component.$description.html(replyText);
        } else {
            component.$description.html(reviewText);
        }

        // Then, calculate the height again.
        const descriptionHeight = component.$description.height();

        // The, if $element is a reply, use a smaller height calculation.
        if (parentElement[0].hasAttribute(REPLY_ATTR)) {
            component.$element.height(componentHeight + descriptionHeight - 60);
        } else {
            component.$element.height(componentHeight + descriptionHeight);
        }

        // Show the toggle buttons and collapse it.
        component.$toggleButtons.removeClass(HIDE_BUTTONS_CLASS);
        component.expandible.toggleExpand(false);
    } else {
        // Hide $element toggle buttons.
        component.$toggleButtons.addClass(HIDE_BUTTONS_CLASS);
    }
};

UserReview.initialize = function() {
    // turn all elements with the default selector into components
    $(COMPONENT_SELECTOR).each((index, element) => new UserReview(element));
};

UserReview.initialize();
