// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

// explicitly inject dependencies (alphabetically), only those needed
var $ = require('jquery');

// what does this module expose?
module.exports = NotificationCookie;

// component configuration
var COMPONENT_SELECTOR = '[data-notification-cookie]';
var HANDLE_SELECTOR = '[data-notification-cookie-close]';
var VERSION_ATTR = 'data-notification-cookie-version';
var SOORTAANBOD_ATTR = 'data-notification-cookie-soortaanbod';
var EVENT_NAMESPACE = '.notification-cookie';
var COOKIE_PREFIX = 'notificationAcknowledged_';
var ACKNOWLEDGED_CLASS = 'is-acknowledged';
var READ_MORE_SELECTOR = '[data-read-more-link]';

function NotificationCookie(element) {
    var component = this;
    component.$element = $(element);
    component.version = element.getAttribute(VERSION_ATTR);
    component.soortAanbod = element.getAttribute(SOORTAANBOD_ATTR);
    component.cookieName = COOKIE_PREFIX + component.soortAanbod + '_' + component.version;

    component.$element.on('click' + EVENT_NAMESPACE, HANDLE_SELECTOR, function(event) {
        event.preventDefault();
        component.acknowledgeNotification();
        component.$element.addClass(ACKNOWLEDGED_CLASS);
    });
    component.$element.find(READ_MORE_SELECTOR).on('click', function() {
        component.acknowledgeNotification();
        component.$element.addClass(ACKNOWLEDGED_CLASS);
    });
}

NotificationCookie.prototype.acknowledgeNotification = function() {
    this.setCookie();
};

/**
 * sets cookie and return the cookie value
 * @returns {string}
 */
NotificationCookie.prototype.setCookie = function() {
    var component = this;
    var date = new Date();
    date.setFullYear(date.getFullYear() + 1);
    var cookie = component.cookieName + '=' + component.version + '; expires=' + date.toGMTString() + '; path= /';

    document.cookie = cookie;
    return cookie;
};

// turn all elements with the default selector into components
$(COMPONENT_SELECTOR).each(function(index, element) {
    return new NotificationCookie(element);
});
