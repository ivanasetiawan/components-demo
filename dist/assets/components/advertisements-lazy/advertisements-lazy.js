// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

// explicitly inject dependencies (alphabetically), only those needed
import $ from 'jquery';

import AdvertisementsLazy from '../advertisements/advertisements';

export default AdvertisementsLazy;

// component configuration
const COMPONENT_LAZY_SELECTOR = '[data-advertisement-lazy]';
const LAZY_ONSCROLL_SELECTOR = '[data-advertisement-lazy-onscroll]';
const LAZY_ONEXPANDED_SELECTOR = '[data-advertisement-lazy-onscroll-onexpanded]';
const BP_MEDIUM = 750;
const EVENT_NAMESPACE_AD = '.advertisement';
const WINDOW_WIDTH = $(window).width();
const NO_DESKTOP = WINDOW_WIDTH < BP_MEDIUM - 1;
const COMPONENT_LAZY_ASYNC_SELECTOR = '[data-advertisement-searchresults]';

/**
 * Default lazy ad initializer behaviour.
 * @param {array} elements - list of lazy ads.
 * @param {string} classFlag - classname of each ad on the first load.
 */
AdvertisementsLazy.lazyInitialize = function($elements) {
    const component = this;
    window.googletag.cmd.push(function() {
        [].forEach.call($elements, (element) => {
            const config = AdvertisementsLazy.getAdConfig(element);
            if (config !== false) {
                component.instances.push(element);
                element.id = config.id;
                component.createLazySlot(element, config);
                if ($(element).attr('data-advertisement-searchresults') != undefined) {
                    component.registerOnSlotRenderCallback('async');
                }
            }
        });
        window.googletag.pubads().collapseEmptyDivs();
        window.googletag.pubads().disableInitialLoad();
        window.googletag.enableServices();
    });
};

/**
 * Creating lazy slot.
 * @param {object} element - lazy ad.
 * @param {object} config - config of lazy ad.
 */
AdvertisementsLazy.createLazySlot = function(element, config) {
    const component = this;
    const cachedCategories = this.cachedCategoriesByUrl[config.categoriesUrl];

    if (cachedCategories) {
        component.pushLazyAdSlot(element, config, cachedCategories);
    } else {
        $.get({
            url: config.categoriesUrl,
            success:
                function(categories) {
                    component.pushLazyAdSlot(element, config, categories);
                },
            error:
                function() {
                    component.pushLazyAdSlot(element, config, {});
                }
        });
    }
};

/**
 * Pushing lazy slot to DFP.
 * @param {object} element - lazy ad.
 * @param {object} config - config of lazy ad.
 * @param {object} categories - categories of lazy ad.
 */
AdvertisementsLazy.pushLazyAdSlot = function(element, config, categories) {
    var alreadyShown = false;
    const ONSCROLL_ONEXPANDED_ATTR_CHECKER= $(element).attr('data-advertisement-lazy-onscroll-onexpanded') == '';
    const tag = window.googletag
        .defineSlot(config.adunitpath, config.sizedefault, config.id)
        .defineSizeMapping(config.sizemapping)
        .setCollapseEmptyDiv(true, true)
        .addService(window.googletag.pubads());

    for (let key in categories) {
        if (categories.hasOwnProperty(key)) {
            tag.setTargeting(key, categories[key]);
        }
    }

    // Checker for category banners on kenmerken expanded.
    if (NO_DESKTOP && ONSCROLL_ONEXPANDED_ATTR_CHECKER) {
        AdvertisementsLazy.pushLazyAdSlotOnMobileOnKenmerkenExpanded(element, config, tag);
    } else {
        AdvertisementsLazy.pushLazyAdSlotOnDefaultBehaviour(element, config, tag, alreadyShown);
    }
};

/**
 * Pushing lazy slot on mobile, on kenmerken expanded to DFP.
 * @param {object} element - lazy ad.
 * @param {object} config - config of lazy ad.
 * @param {object} tag - lazy ad tag.
 */
AdvertisementsLazy.pushLazyAdSlotOnMobileOnKenmerkenExpanded = function(element, config, tag) {
    $(element).addClass('dfp-display-triggered');
    window.googletag.cmd.push(() => {
        window.googletag.display(config.id);
        window.googletag.pubads().refresh([tag]);
    });
    AdvertisementsLazy.unregisterScrollEventHandlerWhenNotNeeded();
};

/**
 * Pushing lazy slot on default behavior (listening to scroll)
 * @param {object} element - lazy ad.
 * @param {object} config - config of lazy ad.
 * @param {object} tag - lazy ad tag.
 * @param {boolean} alreadyShown - a flag to mark if an ad is already shown or not.
 */
AdvertisementsLazy.pushLazyAdSlotOnDefaultBehaviour = function(element, config, tag, alreadyShown) {
    if (AdvertisementsLazy.isOnScreen(element)) {
        alreadyShown = true;
        $(element).addClass('dfp-display-triggered');
        window.googletag.cmd.push(() => {
            window.googletag.display(config.id);
            window.googletag.pubads().refresh([tag]);
        });
    }
    AdvertisementsLazy.registerOnScrollEventHandler(element, config, tag, alreadyShown);
};

/**
 * Registering scroll event handler that checks if an ad is on the screen and visible.
 * @param {object} element - lazy ad.
 * @param {object} config - config of lazy ad.
 * @param {object} tag - ad tag.
 * @param {boolean} alreadyShown - a flag to mark if an ad is already shown or not.
 */
AdvertisementsLazy.registerOnScrollEventHandler = function(element, config, tag, alreadyShown) {
    $(window).on('scroll' + EVENT_NAMESPACE_AD, function scrollHandler() {
        if ((AdvertisementsLazy.isOnScreen(element))) {
            if (!alreadyShown) {
                $(element).addClass('dfp-display-triggered');
                window.googletag.cmd.push(() => {
                    window.googletag.display(config.id);
                    window.googletag.pubads().refresh([tag]);
                });
                alreadyShown = true;
            }

            AdvertisementsLazy.unregisterScrollEventHandlerWhenNotNeeded(element);
        }
    });
};


AdvertisementsLazy.unregisterScrollEventHandlerWhenNotNeeded = function() {
    const numberOfLazyAds = $('[data-advertisement-lazy-onscroll-onexpanded], [data-advertisement-lazy-onscroll], [data-advertisement-searchresults]').length;
    const numberOfSuccessLazyAds = $('[data-advertisement-lazy-onscroll-onexpanded].dfp-display-triggered, [data-advertisement-lazy-onscroll].dfp-display-triggered, [data-advertisement-searchresults].dfp-display-triggered').length;
    if (numberOfSuccessLazyAds >= numberOfLazyAds) {
        AdvertisementsLazy.unregisterEvent('scroll' + EVENT_NAMESPACE_AD);
    }
};

AdvertisementsLazy.unregisterEvent = function(eventName) {
    $(window).off(eventName);
};

// START: Init the lazy ads on Object details
if ($(COMPONENT_LAZY_SELECTOR).length && WINDOW_WIDTH > BP_MEDIUM) {
    AdvertisementsLazy.lazyInitialize(
        $(LAZY_ONEXPANDED_SELECTOR + ',' + LAZY_ONSCROLL_SELECTOR)
    );
} else if ($(COMPONENT_LAZY_SELECTOR).length && NO_DESKTOP) {
    AdvertisementsLazy.lazyInitialize(
        $(LAZY_ONSCROLL_SELECTOR)
    );

    $(document).on('kenmerkenExpanded', (event, data) => {
        const kenmerkenExpandedSelector = document.querySelector(data.selector);
        AdvertisementsLazy.lazyInitialize(
            kenmerkenExpandedSelector.querySelectorAll(LAZY_ONEXPANDED_SELECTOR)
        );
    });
}
// END: Init the lazy ads on Object details

// Init the lazy ads on Search Results page
if ($(COMPONENT_LAZY_ASYNC_SELECTOR).length) {
    // On init, get the total amount of advertisement elements on the page.
    AdvertisementsLazy.lazyInitialize($(COMPONENT_LAZY_ASYNC_SELECTOR));

    $(document).on('resultsUpdated', function() {
        AdvertisementsLazy.lazyInitialize($(COMPONENT_LAZY_ASYNC_SELECTOR));
    });
}

// END: Init the lazy ads on Search Results page