// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

// explicitly inject dependencies (alphabetically), only those needed

// what does this module expose?
module.exports = LoginDialog;

var LOGIN_DIALOG_SELECTOR = '[data-dialog="user-form"]';

function LoginDialog(contentUrl, onSuccess) {
    var component = this;
    var dialogElement = document.querySelector(LOGIN_DIALOG_SELECTOR);

    dialogElement.dialog.onFormSuccess = function() {
        onSuccess();
        // on successful login a lot of things need to update on the page,
        // so it's easier just to reload the entire page entirely.
    };

    component.dialog = dialogElement.dialog;
    component.dialog.open();
    component.dialog.getContent(contentUrl);
}
