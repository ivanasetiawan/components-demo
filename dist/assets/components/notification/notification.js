'use strict';

import $ from 'jquery';

export default Notification;

const COMPONENT_SELECTOR = '[data-notification]';
const CLOSE_SELECTOR = '[data-notification-close]';
const TEXT_CONTAINER_SELECTOR = '[data-text-container]';
const HIDDEN_CLASS = 'is-hidden';

function Notification(element) {
    const component = this;

    component.$element = $(element);
    component.$close = component.$element.find(CLOSE_SELECTOR);
    component.$textContainer = component.$element.find(TEXT_CONTAINER_SELECTOR);

    component.bindEvents();
}

Notification.prototype.bindEvents = function () {
    const component = this;

    component.$element.on('show', (event, message) => component.show(event, message));
    component.$element.on('hide', () => component.close());
    component.$close.on('click', event => component.close(event));
};

Notification.prototype.close = function (event) {
    const component = this;

    if (typeof event !== 'undefined' && typeof event.preventDefault === 'function') {
        event.preventDefault();
    }

    component.$element.addClass(HIDDEN_CLASS);
    component.$element.trigger('close');
};

Notification.prototype.show = function (event, message) {
    const component = this;

    if (typeof message === 'string') {
        component.$textContainer.html(message);
    }

    component.$element.removeClass(HIDDEN_CLASS);
};

$(COMPONENT_SELECTOR).get().map(element => new Notification(element));
