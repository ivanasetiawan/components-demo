'use strict';

var $ = require('jquery');
var extend = require('lodash/extend');
var isFunction = require('lodash/isFunction');

module.exports = AsyncForm;

// component configuration
var AJAX_SUBMIT_SELECTOR = '[dialog-ajax-submit]';
var OPTIONS = {
    body: null,
    success: null,
    error: null,
    submitCallback: null
};

function AsyncForm($element, options) {
    var component = this;
    component.$element = $element.find('form');
    component.options = extend(OPTIONS, options);

    if (component.$element.length === 0) {
        return;
    }
    component.hookForm();
}

AsyncForm.prototype.hookForm = function() {
    var component = this;
    var url = component.$element.attr('action');

    // binding to specific form/action
    $('form[action="' + url + '"]').on('submit', function(event) {
        event.preventDefault();

        var submitButtons = AsyncForm.getSubmitButtons($(event.target));

        //disable all submit buttons
        for (let i = 0, length = submitButtons.length; i < length; i++) {
            $(submitButtons[i]).prop('disabled', true);
        }

        var data = $(this).find(':input').serialize();
        // post it
        $.post({
            url: url,
            data: data,
            cache: false,
            xhrFields: {
                withCredentials: true
            },
            dataType: 'json'
        })
            .then(function(response) {
                if (response.Result === 'OK') {

                    if (response.View === null || typeof response.View === 'undefined') {
                        if (isFunction(component.options.success)) {
                            component.options.success();
                        }
                    } else {
                        var $viewHtml = $(response.View);
                        component.options.body.html(response.View);
                        // bind social login
                        if (component.options.submitCallback) {
                            component.options.submitCallback(component.options.body);
                        }
                        // bind new instance to form
                        if (AsyncForm.wantsAjaxSubmit($viewHtml)) {
                            return new AsyncForm($viewHtml, {
                                success: component.options.success,
                                error: component.options.error
                            });
                        }
                    }
                }
                // TODO: add handling of errors (eg. result = 'ERROR' and errorMessage != null)
            })
            .catch(function(reason) {
                console.log(reason); // TODO: add handling of errors (eg. result = 'ERROR' and errorMessage != null)

                //enable all submit buttons
                for (let i = 0, length = submitButtons.length; i < length; i++) {
                    $(submitButtons[i]).prop('disabled', false);
                }
            });
    });
};

AsyncForm.wantsAjaxSubmit = function($viewHtml) {
    var $ajaxSubmit = AsyncForm.getSubmitButtons($viewHtml);
    return $ajaxSubmit.length > 0;
};
AsyncForm.getSubmitButtons = function($viewHtml) {
    return $viewHtml.find(AJAX_SUBMIT_SELECTOR);
};
