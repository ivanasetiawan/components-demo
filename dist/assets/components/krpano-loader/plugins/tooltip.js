var TOOLTIP_TEMPLATE_SELECTOR = '[data-media-viewer-foto360-tooltip-template]';
var TOOLTIP_OPEN_CLASS = 'media-viewer-foto360-tooltip-open';

var krpanoplugin = function()
{
    var local = this;

    var krpano = null;
    var plugin = null;

    var plugincanvas = null;
    var template = null;

    local.registerplugin = function(krpanointerface, pluginpath, pluginobject)
    {
        krpano = krpanointerface;
        plugin = pluginobject;

        plugin.show = local.show;
        plugin.hide = local.hide;
        plugin.open = local.open;

        plugin.registerattribute("tooltip.shown", false);
        plugin.registerattribute("tooltip.open", false);

        template = document.querySelector(TOOLTIP_TEMPLATE_SELECTOR).innerHTML;
    };

    local.unloadplugin = function()
    {
        plugin = null;
        krpano = null;
    };

    local.show = function(type, title, description, image) {
        // reset tooltip
        local.hide();

        var el = document.createElement('div');

        var items = {
            title: title,
            type: type,
            description: description,
            image: image
        };

        el.innerHTML = local.renderTemplate(items);

        plugin.sprite.appendChild(el.firstElementChild);

        var width = (krpano.device.touch && krpano.device.pixelratio > 2) ? 566 : 283;
        plugin.registercontentsize(width, 86);

        krpano.set('tooltip.shown', true);
    };

    local.open = function() {
        plugin.sprite.firstElementChild.className = plugin.sprite.firstElementChild.className + ' ' + TOOLTIP_OPEN_CLASS;
        krpano.set('tooltip.open', true);
    };

    local.close = function() {
        plugin.sprite.firstElementChild.className.replace(TOOLTIP_OPEN_CLASS, '');
        krpano.set('tooltip.open', false);
    };

    local.hide = function() {
        while (plugin.sprite.firstChild) {
            plugin.sprite.removeChild(plugin.sprite.firstChild);
        }

        plugin.registercontentsize(0,0);

        krpano.set('tooltip.shown', false);
        krpano.set('tooltip.open', false);
    };

    local.renderTemplate = function(item) {
        var html = template;
        var pattern;

        // replace {if}{/if} sections if property exists
        for (var prop in item) {
            if (item.hasOwnProperty(prop) && item[prop]) {
                pattern = new RegExp('{if ' + prop + '}(.*?){\/if}', 'gm');
                html = html.replace(pattern, '$1');
            }
        }

        // remove {if}{/if} sections that have no value
        pattern = new RegExp('{if .+}(.*?){\/if}', 'gm');
        html = html.replace(pattern, '');

        // replace placeholders in template by values from option data
        for (prop in item) {
            if (item.hasOwnProperty(prop)) {
                pattern = new RegExp('{' + prop + '}', 'g');
                html = html.replace(pattern, item[prop]);
            }
        }
        return html;
    };
};
