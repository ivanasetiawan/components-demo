// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

// explicitly inject dependencies (alphabetically), only those needed
import $ from 'jquery';

// what does this module expose?
export default FeaturedHomes;

const COMPONENT_SELECTOR = '[data-featured-homes]';
const UPDATING_CLASS = 'is-updating';
const HEIGHT_MARGIN = 40;

function FeaturedHomes(element) {
    const component = this;
    component.$element = $(element);
    component.$element.css({'min-height': (Math.ceil(component.$element.outerHeight()) + HEIGHT_MARGIN) + 'px'});

    $(document)
        .on('resultsUpdating', () => component.isUpdating())
        .on('resultsUpdated', () => component.isUpdated());
}

FeaturedHomes.prototype.isUpdating = function () {
    this.$element.addClass(UPDATING_CLASS);
};

FeaturedHomes.prototype.isUpdated = function () {
    const component = this;
    setTimeout(() => {
        component.$element.removeClass(UPDATING_CLASS);
    }, 250);
};

// turn all elements with the default selector into components
$(COMPONENT_SELECTOR).each((index, element) => new FeaturedHomes(element));
