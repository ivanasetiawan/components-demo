// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

// explicitly inject dependencies (alphabetically), only those needed
var $ = require('jquery');

// what does this module expose?
module.exports = OptIn;

// component configuration
var COMPONENT_SELECTOR = '[data-opt-in]';
var OPTION_EXPERIMENT_ID_ATTRIBUTE = 'data-opt-in-experiment-id';
var OPTION_EXTERNAL_ATTRIBUTE = 'data-opt-in-is-external';

function OptIn(element) {
    var component = this;
    component.$element = $(element);
    component.experimentId = parseInt(element.getAttribute(OPTION_EXPERIMENT_ID_ATTRIBUTE), 10);
    component.isExternal = element.getAttribute(OPTION_EXTERNAL_ATTRIBUTE) === 'true';

    component.$optInLink = component.$element.find('a');
    component.$optOutForm = component.$element.find('form');

    component.$optInLink.on('click', function optIn() { component.optIn(); });
    component.$optOutForm.on('submit', function optOut(e) { component.optOut(e, this); });

    // Track as an Optimizely 'experiment' (see http://developers.optimizely.com/javascript/reference/#activate)
    window.optimizely = window.optimizely || [];
    window.optimizely.push(['activate', component.experimentId]);

    // Old website also tracks with an event: _gaq.push(['_trackEvent', 'Intercept', 'Getoond', 'Tevredenheidsonderzoek']);
}

OptIn.prototype.optIn = function() {
    var component = this;

    if (component.isExternal) {
        // We opened a URL in a new tab/window, so we need to close the opt-in in the old tab/window
        component.close();
    }

    // Old website also tracks with an event: _gaq.push(['_trackEvent', 'Intercept', 'Call to action', 'Tevredenheidsonderzoek']);
};

OptIn.prototype.optOut = function(e, form) {
    var component = this;

    var settings = {
        method: form.method,
        data: $(form).serialize(),
        success: function() {
            component.close();
            // Old website also tracks with an event: _gaq.push(['_trackEvent', 'Intercept', 'Sluiten', 'Tevredenheidsonderzoek']);
        },
        error: function(jqXHR, textStatus, errorThrown) {
            if (window.console) {
                console.log(jqXHR, textStatus, errorThrown);
            }
        }
    };
    $.ajax(form.action, settings);

    e.preventDefault();
};

OptIn.prototype.close = function() {
    var component = this;
    component.$element.addClass('is-removed');
};

// turn all elements with the default selector into components
$(COMPONENT_SELECTOR).each(function(index, element) {
    return new OptIn(element);
});
