// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

// explicitly inject dependencies (alphabetically), only those needed
import $ from 'jquery';
require('../radio-group/radio-group');

// what does this module expose?
export default ObjectTypeFilter;

// component configuration
const COMPONENT_SELECTOR = '[data-object-type-filter]';
const OPTION_ATTR = 'data-object-type-non-relevant-filters';
const OPTION_SELECTOR = '[' + OPTION_ATTR + ']';
const TARGET_ATTR = 'data-object-type-filter-target';
const TARGET_SELECTOR = '[' + TARGET_ATTR + ']';
const DISABLED_CLASS = 'is-disabled';

/**
 * @param {HTMLElement} element    containing all options (handles)
 * @constructor
 */
function ObjectTypeFilter(element) {
    const component = this;
    component.$element = $(element);
    component.$targets = $(TARGET_SELECTOR);

    component.$element.on('change', OPTION_SELECTOR, (event) => {
        let allFilters = [];
        // enable filters for current filter
        component.$element.find(OPTION_SELECTOR).each(function(index, option) {
            const filters = $(option).attr(OPTION_ATTR).split(' ');
            component.enableAllFilters(filters);
            allFilters = allFilters.concat(filters);
        });

        // disable filters for current option
        const $input = $(event.currentTarget);
        if ($input.is(':checked')) {
            const disabledFilters = $input.attr(OPTION_ATTR).split(' ');
            component.disableFilters(allFilters, disabledFilters);
        }
    });
}

/**
 * @param {String[]} disabledFilters
 */
ObjectTypeFilter.prototype.disableFilters = function(allFilters, disabledFilters) {
    const component = this;
    component.$targets.each(function(index, element) {
        const name = element.getAttribute(TARGET_ATTR);
        if (allFilters.indexOf(name) > -1) {
            // uncheck filters that are hidden (by checking the 'geen' option)
            // filters that are hidden are invalid in combination with the selected filter, that's why they need to be deselected
            // for example: aantal kamers needs to be deselected and hidden when parkeergelegenheid is selected
            const option = $(element).find('input[value$="-geen"]');
            if (!$(option).is(':checked')) {
                $(element).find('label[for$="-geen"]').click();
            }
            $(element).find('.filter-flyout-selected-option').each(function(j, selected) {
                $(selected).click();
            });
            const isDisabled = (disabledFilters.indexOf(name) > -1);
            $(element)
                .toggleClass(DISABLED_CLASS, isDisabled)
                .trigger('changed');
        }
    });
};

ObjectTypeFilter.prototype.enableAllFilters = function(allFilters) {
    const component = this;
    component.$targets.each(function(index, element) {
        const name = element.getAttribute(TARGET_ATTR);
        if (allFilters.indexOf(name) > -1) {
            $(element).removeClass(DISABLED_CLASS);
        }
    });
};

// turn all elements with the default selector into components
$(COMPONENT_SELECTOR).each((index, element) => new ObjectTypeFilter(element));
