'use strict';

import $ from 'jquery';
import debounce from 'lodash/debounce';

export default SearchSidebar;

const COMPONENT_SELECTOR = '[data-search-sidebar]';
const FLYOUT_SELECTOR = '[data-filter-flyout]';
const SIDEBAR_HANDLE = '[data-sidebar-handle]';
const CLOSE_FLYOUT_SELECTOR = '[data-search-close-flyout]';
const OPEN_FLYOUT_CLASS = 'has-open-flyout';
const FLYOUT_CLOSE_EVENT = 'filterflyoutclose';
const FLYOUT_CLOSED_EVENT = 'filterflyoutclosed';
const FLYOUT_OPEN_EVENT = 'filterflyoutopened';
const IS_EXTENDABLE_CLASS = 'is-extendable';
const IS_EXTENDED_CLASS = 'is-extended';
const SIDEBAR_IS_EXTENDED = 'sidebar-is-extended';
const SIDEBAR_CLOSED_EVENT = 'sidebarclosed';
const ERROR_BAR_SELECTOR = '[data-error-bar]';

const FLYOUT_HAS_FILTER_SELECTOR = '[data-has-preview-filter]';
const FLYOUT_FILTER_OPEN_CLASS = 'has-preview-filter';

function SearchSidebar(element) {
    const component = this;
    const $html = $('html');

    component.scrollPosition = 0;
    component.isExtended = false;

    component.bindToElements(element);

    component.bindEvents();
    component.watchFlyouts();
    component.toggleFlyoutStickyButton();
    component.$element.addClass(IS_EXTENDABLE_CLASS);

    $html.on('click', (event) => {
        const $target = $(event.target);
        const targetSidebarHandle = $target.is(SIDEBAR_HANDLE);
        const targetInsideHandle = $target.closest(SIDEBAR_HANDLE).length > 0;
        const targetInsideSidebar = $target.closest(COMPONENT_SELECTOR).length > 0 && !targetSidebarHandle;
        const targetInsideErrorBar = $target.closest(ERROR_BAR_SELECTOR).length > 0;
        const targetFlyoutHandle = $target.is(CLOSE_FLYOUT_SELECTOR);

        if (!targetSidebarHandle && !targetInsideHandle && !targetInsideSidebar &&
            !targetInsideErrorBar && !targetFlyoutHandle && component.isExtended) {
            component.close();
        }
    });
    $html.on('click', SIDEBAR_HANDLE, (event) => {
        event.preventDefault();

        if (component.isExtended) {
            component.close();
        } else {
            component.open();
        }
    });
    // horizontal scroll on mobile triggers resize event.
    const windowWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
    $(window).on('resize', debounce(() => {
        const newWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        // Chrome 48 on iOS9.2.1 (incorrectly) reports a different window width when a select input has focus (ADA-288).
        // this should not trigger the sidebar to close.
        const widthDifferenceOnSelectFocus = 10;
        if (component.isExtended && (Math.abs(windowWidth - newWidth) > widthDifferenceOnSelectFocus)) {
            component.close();
        }
    }, 100));

    $(window).on('sidebar-refresh', function() {
        var $myNewElement = $(COMPONENT_SELECTOR);
        if ($myNewElement.length == 1) {
            component.bindToElements($myNewElement);
            component.bindEvents();
            component.watchFlyouts();
            component.toggleFlyoutStickyButton();
            component.$element.addClass(IS_EXTENDABLE_CLASS);
            $(document).scrollTop(0);
        }
    });
}

SearchSidebar.prototype.bindToElements = function(element) {
    const component = this;

    component.$element = $(element);
    component.$flyouts = component.$element.find(FLYOUT_SELECTOR);
    component.$form = component.$element.closest('form');
    component.$flyoutWithFilter = component.$element.find(FLYOUT_HAS_FILTER_SELECTOR);
    component.$closeFlyoutButton = $(CLOSE_FLYOUT_SELECTOR);
    component.$sideBarHandles = $(SIDEBAR_HANDLE);
};

SearchSidebar.prototype.bindEvents = function() {
    const component = this;

    component.$closeFlyoutButton.on('click', function() {
        component.$element.find(FLYOUT_SELECTOR).trigger(FLYOUT_CLOSE_EVENT);
    });
};

SearchSidebar.prototype.open = function() {
    const component = this;
    component.isExtended = true;
    component.$element.addClass(IS_EXTENDED_CLASS);
    $('html').addClass(SIDEBAR_IS_EXTENDED);
};

SearchSidebar.prototype.close = function() {
    const component = this;
    window.scrollTo(0, 0);
    component.isExtended = false;
    component.$element.removeClass(IS_EXTENDED_CLASS);
    component.$element.trigger(SIDEBAR_CLOSED_EVENT);
    $('html').removeClass(SIDEBAR_IS_EXTENDED);
};

SearchSidebar.prototype.watchFlyouts = function() {
    const component = this;

    component.$form.on(FLYOUT_OPEN_EVENT, FLYOUT_SELECTOR, () => {
        component.$element.addClass(OPEN_FLYOUT_CLASS);
        component.scrollPosition = component.$element.scrollTop();
        component.$element.animate({scrollTop: 0}, 0);
    });

    component.$form.on(FLYOUT_CLOSED_EVENT, FLYOUT_SELECTOR, () => {
        component.$element.removeClass(OPEN_FLYOUT_CLASS);
        component.$element.animate({scrollTop: component.scrollPosition}, 0);
    });
};

SearchSidebar.prototype.toggleFlyoutStickyButton = function() {
    const component = this;

    component.$form.on('click', FLYOUT_HAS_FILTER_SELECTOR, () => {
        component.$element.toggleClass(FLYOUT_FILTER_OPEN_CLASS);
    });

    component.$form.on(FLYOUT_CLOSED_EVENT, FLYOUT_SELECTOR, () => {
        component.$element.removeClass(FLYOUT_FILTER_OPEN_CLASS);
    });
};

// turn all elements with the default selector into components
$(COMPONENT_SELECTOR).each((index, element) => new SearchSidebar(element));
