// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

// explicitly inject dependencies (alphabetically), only those needed
import $ from 'jquery';
import Map from '../map/map';
import MapEventProxy from '../map/map-event-proxy';
import MapEvents from '../map/map-events';
import MapMarkerOverlay from '../map/map-marker-overlay';
import SearchMapMarkerManager from '../search-map-marker-manager/search-map-marker-manager';
import SearchMapInfoWindow from '../../components/search-map-infowindow/search-map-infowindow';
import '../search-map-content/search-map-content';

export default SearchMap;

// component configuration
const COMPONENT_SELECTOR = '[data-search-map]';
const INFO_WINDOW_COMPONENT_SELECTOR = '[data-search-map-infowindow]';
const SHOW_OBJECT_INFO_EVENT = 'show_object_info';
const HIDE_OBJECT_INFO_EVENT = 'hide_object_info';
const NO_OBJECT_EVENT_ARG = 'no_object';
const FORCE_REFRESH_MAPTILES_EVENT = 'force_maptile_refresh';
const SEARCH_QUERY_UPDATED_EVENT = 'zo_updated';
const PUSH_STATE_MARKER = 'push_state_set_by_funda';
const ASYNC_SIDEBAR_URL = '/kaartsidebar';
const MIN_ZOOM = 8;
const MAX_ZOOM = 19;


function SearchMap(element) {
    const component = this;
    component.doNotEnforceZoomLevel = false;
    // event proxy
    component.mapEventProxy = MapEventProxy;
    component.mapEvents = MapEvents;
    component.currentSearch = component.mapEvents.getCurrentSearchFromUrl();

    Map.call(this, element, function mapLoadedHandler() {
        // The Google Map API idle event will create the overlay
        component.createOverlay();

        component.mapEventProxy.removeListener(component.zoomLimitHandlerId);
        component.mapEventProxy.addListener('zoom_changed', () => {
            component.verifyMapZoomLimit(component.map);
        });
        component.mapEventProxy.addListener('idle', () => {
            component.recordMapState();
        });

        component.$element.on(FORCE_REFRESH_MAPTILES_EVENT, (event, eventArgs) => {
            component.doNotEnforceZoomLevel = !eventArgs.isfinshed;
        });
        window.addEventListener('popstate', component.onMapPopState.bind(component), false);
        $(INFO_WINDOW_COMPONENT_SELECTOR).each((index, ele) => {
            component.infoWindow = new SearchMapInfoWindow(ele, component);
        });
        component.$element.on(SEARCH_QUERY_UPDATED_EVENT, (event, eventArgs) => {
            component.currentSearch = eventArgs.zo;
        });
    });
}

SearchMap.prototype = Object.create(Map.prototype);

/**
 *  Create marker overlay with custom markers
 */
SearchMap.prototype.createOverlay = function() {
    const component = this;
    component.mapMarkerManager = new SearchMapMarkerManager(component.$element, component.config);
    const mapData = {
        map: component.map,
        config: component.config,
        tileSize: component.tileSize,
        cluster: MapEvents.getValueFromHash('c') || 0
    };
    component.markerOverlay = new MapMarkerOverlay(mapData, component.mapMarkerManager);
    // Use this map type as overlay.
    component.map.overlayMapTypes.insertAt(0, component.markerOverlay);
    // map changed
    component.$element.on(SHOW_OBJECT_INFO_EVENT, (event, eventArgs) => {
        let id = eventArgs.objectId;
        // we could get an id consisting of multiple parts
        var idParts = id.split('-');
        if (idParts.length > 1) {
            id = idParts[0];
        }
        component.recordMapState({id: id});
    });
    component.$element.on(HIDE_OBJECT_INFO_EVENT, (event, eventArgs) => {
        if (eventArgs.type === NO_OBJECT_EVENT_ARG) {
            component.recordMapState({id: null});
        }
    });
};

/**
 *  implementation that checks zoomlevel limits that can be switched off (used when forcing google map to reload tiles)
 */
SearchMap.prototype.verifyMapZoomLimit = function (map) {
    const component = this;
    if (component.doNotEnforceZoomLevel) return;

    const currentZoom = map.getZoom();
    if (currentZoom > MAX_ZOOM) {
        map.setZoom(MAX_ZOOM);
    }
    else if (currentZoom < MIN_ZOOM) {
        map.setZoom(MIN_ZOOM);
    }
};

/**
 * handles popstate for map
 */
SearchMap.prototype.onMapPopState = function(event) {
    const component = this;
    const google = window.google;

    if (event && event.state === PUSH_STATE_MARKER) {
        // handle map stuff
        component.mapEventProxy.forwardProxiedEvents(false);
        let prevLocation = component.mapEvents.getValueFromHash('center');
        let prevZoom = component.mapEvents.getValueFromHash('zoom');
        if (prevLocation && prevZoom) {
            const latlng = component.mapEvents.getValueFromHash('center').split(',');
            const weirdPoint = new google.maps.LatLng(parseFloat(latlng[0])+.5, parseFloat(latlng[1])+.5);
            component.map.setCenter(weirdPoint);
            const point = new google.maps.LatLng(parseFloat(latlng[0]), parseFloat(latlng[1]));
            component.map.setCenter(point);
            component.map.setZoom(parseInt(component.mapEvents.getValueFromHash('zoom'), 10));
        }
        // do tiles need refreshing
        var newSearchFromUrl = component.mapEvents.getCurrentSearchFromUrl();
        if (newSearchFromUrl != component.currentSearch) {
            component.markerOverlay.refreshTiles(newSearchFromUrl);
            component.currentSearch = newSearchFromUrl;
            component.refreshSidebar(newSearchFromUrl);
        }
        // marker / infowindow
        const rawId = component.mapEvents.getValueFromHash('id');
        if (rawId !== null && rawId !== '') {
            component.mapMarkerManager.selectMarker(rawId);
            component.infoWindow.processEvent({initial: true, objectId: rawId});
        } else {
            component.mapMarkerManager.unselectAllMarkers();
            component.infoWindow.hide();
        }
        component.mapEventProxy.forwardProxiedEvents(true);
    }
};

SearchMap.prototype.refreshSidebar = function(newSearch) {
    $.ajax({
        type: 'GET',
        url: ASYNC_SIDEBAR_URL + newSearch,
        dataType: 'text',
        success: function(data) {
            $('form').html($(data));
            $(window).trigger('sidebar-refresh');
        }
    });

};

// turn all elements with the default selector into components
$(COMPONENT_SELECTOR).each((index, element) => new SearchMap(element));
