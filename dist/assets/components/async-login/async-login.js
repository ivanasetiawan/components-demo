'use strict';

import $ from 'jquery';
export default loginAsync;

const LOGIN_DIALOG_SELECTOR = '[data-dialog="user-form"]';

let loginAsyncCacheIsLoggedIn = false;

/**
 * Performs an asyncronous login. The response can be handled either with a callback or a promise.
 * Once the user is logged in, the result gets cached so we don't perform more request than the scrictly required.
 * @param callback optional callback. Can cbe also handled with
 * @returns Deferred promise.
 */
function loginAsync(callback) {
    const result = $.Deferred(); // eslint-disable-line new-cap
    const dialog = document.querySelector(LOGIN_DIALOG_SELECTOR).dialog;

    if (loginAsyncCacheIsLoggedIn) {
        callback();
        result.resolve();
    } else if (typeof dialog !== 'undefined' && typeof dialog.isUserLoggedInUrl === 'string') {
        $.ajax(dialog.isUserLoggedInUrl).then((response)=> {
            if (typeof response === 'undefined') {
                result.reject('Bad response');
            } else if (response.LoggedIn === true) {
                loginAsyncCacheIsLoggedIn = true;
                callback();
                result.resolve();
            } else if (typeof response.LoginUrl === 'string') {
                dialog.onFormSuccess = () => {
                    window.location.reload();
                };
                dialog.open();
                dialog.getContent(response.LoginUrl);
            } else {
                result.reject('Bad response');
            }
        });
    } else {
        result.reject('Dialog component was not found.');
    }
    return result.promise();
}
