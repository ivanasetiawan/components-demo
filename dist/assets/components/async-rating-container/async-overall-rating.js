'use strict';

import $ from 'jquery';
import Observable from '../class-observable/class-observable';
import controllerService from '../service-controller/service-controller';
import Rating from '../rating/rating';

export default AsyncOverallRating;

const OVERALL_RATING_SELECTOR = '[data-overall-object-rating]';

function AsyncOverallRating(element) {
    const component = this;
    component.$element = $(element);
    component.ratingController = controllerService.getInstance(Rating, element);
    component.observers = new Observable();
    component.isEnabled = true;
    component.value = component.ratingController.selectedValue;

    /**
     *  To be used by observers.
     *  Registers a callback that will be called when the overall rating is successfully changed.
     * @param observer callback
     */
    component.onChange = function (observer) {
        component.observers.listen(observer);
    };

    /**
     * Enables ajax communication.
     */
    component.enable = function () {
        component.isEnabled = true;
    };

    /**
     * Disables ajax communication.
     */
    component.disable = function () {
        component.isEnabled = false;
    };

    /**
     * Overall rating value. Retrieved from the rating component.
     * @returns {*}
     */
    component.getValue = function () {
        component.value = component.ratingController.selectedValue;
        return component.value;
    };

    /**
     * Submits to the server the overall rating.
     * @returns {*} deferred promise.
     */
    component.submit = function () {
        var prevValue = component.value;
        var newValue = component.getValue();
        if (prevValue !== component.value) {
            component.observers.notify(newValue);

            if (component.isEnabled) {
                var url = component.$element.attr('action');
                var data = component.$element.serialize();
                return $.ajax({
                    method: 'POST',
                    url: url,
                    data: data
                });
            } else {
                return $.when();
            }
        }
    };

    //Watchers
    component.ratingController.onChange(component.submit);
}

AsyncOverallRating.getSelector = () => OVERALL_RATING_SELECTOR;
