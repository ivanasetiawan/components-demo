'use strict';

import $ from 'jquery';
import loginAsync from '../async-login/async-login';
import Observable from '../class-observable/class-observable';
import AppSpinner from '../app-spinner/app-spinner';
import Rating from '../rating/rating';
import controllerService from '../service-controller/service-controller';

export default AsyncObjectRatingForm;

// component configuration
const COMPONENT_SELECTOR = '[data-async-object-rating-form]';
const RESET_HANDLE_SELECTOR = '[data-object-rating-form-reset-handle]';
const RESET_ACTION_ATTR = 'data-object-rating-form-reset-action';
const CONTAINER_SELECTOR = '[data-async-object-rating-content]';
const SPINNER_SELECTOR = '[data-async-object-rating-spinner]';
const FORM_URL_ATTR = 'url';
const TEXTAREA_SELECTOR = '[data-object-rating-textarea]';
const SUBMIT_BUTTON_SELECTOR = '[data-object-rating-submit-button]';
const DOING_REQUEST_CLASS = 'is-doing-request';
const FORM_SELECTOR = '[data-object-rating-form]';

function AsyncObjectRatingForm(element) {
    const component = this;
    component.$element = $(element);
    component.$container = component.$element.find(CONTAINER_SELECTOR);
    component.spinnerInstance = new AppSpinner(component.$element.find(SPINNER_SELECTOR));
    component.url = component.$container.data(FORM_URL_ATTR);
    component.isComponentLoaded = false;
    component.observers = new Observable();

    component.MESSAGES = {
        ELEMENT_LOADED: 0,
        ELEMENT_RESET: 1,
        ERROR_RESETTING: 2,
        ELEMENT_UPDATED: 3,
        ERROR_UPDATING: 4
    };

    function inject(html) {
        //Inject
        component.$container.html('');
        component.$container.append(html);
        //Create instances
        component.ratings = controllerService.getAllInstances(Rating, component.$container);
        //Find selectors
        component.$textarea = component.$element.find(TEXTAREA_SELECTOR);
        component.$resetHandle = component.$element.find(RESET_HANDLE_SELECTOR);
        component.$submitButton = component.$element.find(SUBMIT_BUTTON_SELECTOR);
        component.$form = component.$element.find(FORM_SELECTOR);
        //Create watchers
        component.$resetHandle.on('click', component.reset);
        component.$submitButton.on('click', component.submit);
        //Store state and notify observers
        component.isComponentLoaded = true;
        component.observers.notify(component.MESSAGES.ELEMENT_LOADED);
    }

    /**
     * To be used by observers.  Call callbacks when form is successfully submitted.
     * @param observer callback
     */
    component.onSubmit = (observer) => {
        component.observers.listen(component.MESSAGES.ELEMENT_UPDATED, observer);
    };

    /**
     * To be used by observers.  Call callbacks when form is successfully reset.
     * @param observer callback
     */
    component.onReset = (observer) => {
        component.observers.listen(component.MESSAGES.ELEMENT_RESET, observer);
    };

    /**
     * Performs reset action
     * @param event
     * @returns {*} deferred promise.
     */
    component.reset = function (event) {
        if (typeof event !== 'undefined') {
            event.preventDefault();
        }
        if (component.isComponentLoaded) {

            for (let i = 0; i < component.ratings.length; i++) {
                component.ratings[i].selectRating(0);
            }
            component.$textarea.val('').html('');
            component.isComponentLoaded = false;
            const resetUrl = component.$form.attr(RESET_ACTION_ATTR);
            return $.ajax({
                method: 'POST',
                url: resetUrl
            }).done(() => {
                component.observers.notify(component.MESSAGES.ELEMENT_RESET);
            }).fail(() => {
                component.observers.notify(component.MESSAGES.ERROR_RESETTING);
            });
        }
    };

    /**
     * Performs submit action
     * @param event
     * @returns {*} deferred promise.
     */
    component.submit = function (event) {
        if (typeof event !== 'undefined') {
            event.preventDefault();
        }
        const url = component.$form.attr('action');
        const data = component.$form.find(':input').serialize();

        component.spinnerInstance.show();
        component.$element.addClass(DOING_REQUEST_CLASS);
        return $.ajax({
            method: 'POST',
            url: url,
            data: data
        }).done((response) => {
            if (response.Result === 'OK') {
                component.observers.notify(component.MESSAGES.ELEMENT_UPDATED, component.$textarea.val());
            } else if (response.Result === 'ERROR') {
                inject(response.Html);
                component.observers.notify(component.MESSAGES.ERROR_UPDATING, arguments);
            }
            return $.when(arguments);
        }).fail(() => {
            component.observers.notify(component.MESSAGES.ERROR_UPDATING, arguments);
        }).then(() => {
            component.spinnerInstance.hide();
            component.$element.removeClass(DOING_REQUEST_CLASS);
            return $.when(arguments);
        });
    };

    component.performAjaxRequest = function () {
        return $.ajax(component.url)
            .done((response) => {
                const res = $.Deferred(); // eslint-disable-line new-cap
                if (response.Result === 'OK') {
                    component.$container.append(response.Html);
                    inject(response.Html);
                    res.resolve();
                } else {
                    res.reject(response);
                }
                return res.promise();
            });
    };

    /**
     * Shows the spinner, logs-in performs ajax request and hide the spinner.
     * @returns {*} deferred promise.
     */
    component.load = function () {
        if (component.isComponentLoaded) {
            return $.when();
        }
        return component.spinnerInstance.show().then(() => loginAsync(() =>
            component.performAjaxRequest().then(() => component.spinnerInstance.hide()))
        );
    };
}

AsyncObjectRatingForm.getSelector = () => COMPONENT_SELECTOR;

