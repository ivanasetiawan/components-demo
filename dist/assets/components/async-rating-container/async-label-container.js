'use strict';

import $ from 'jquery';

export default AsyncLabelText;

const COMPONENT_ATTR = 'data-async-object-rating-handle-text';
const COMPONENT_SELECTOR = '[' + COMPONENT_ATTR + ']';
const MAX_CHARACTERS = 150;

function AsyncLabelText(element) {
    const component = this;
    component.$element = $(element);

    const defaultText = component.$element.attr(COMPONENT_ATTR);

    /**
     * Label setter.
     * @param note if undefined, sets default message. Otherwise writes the first 150 characters in the element
     */
    component.setLabel = function (note) {
        note = note ? note.substring(0, MAX_CHARACTERS) : defaultText;
        component.$element.text(note);
    };
}

AsyncLabelText.getSelector = () => COMPONENT_SELECTOR;
