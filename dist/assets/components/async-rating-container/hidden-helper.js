'use strict';

const HIDE_CLASS = 'hide';

/**
 * Static helper. Abstracts the handling of the HIDE_CLASS class into a component.
 */
const hiddenHelper = (() => {
    var component = {};

    component.isShown = ($element) => {
        return !$element.hasClass(HIDE_CLASS);
    };

    component.show = ($element) => {
        $element.removeClass(HIDE_CLASS);
        return true;
    };

    component.hide = ($element) => {
        $element.addClass(HIDE_CLASS);
        return false;
    };

    return component;
})();

export default hiddenHelper;
