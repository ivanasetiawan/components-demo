'use strict';

import $ from 'jquery';
import AsyncLabelText from './async-label-container';
import AsyncObjectRatingForm from './async-object-rating-form';
import AsyncOverallRating from './async-overall-rating';
import Expandible from '../expandible/expandible';
import controllerService from '../service-controller/service-controller';
import hiddenHelper from './hidden-helper';

export default AsyncRatingContainer;

const COMPONENT_SELECTOR = '[data-async-rating-container]';

function AsyncRatingContainer(element) {
    const component = this;
    component.$element = $(element);
    //Dependencies
    component.expandInstance = controllerService.getInstance(Expandible, element);
    component.labelInstance = controllerService.getInstance(AsyncLabelText, element);
    component.overallRatingInstance = controllerService.getInstance(AsyncOverallRating, element);
    component.formInstance = controllerService.getInstance(AsyncObjectRatingForm, element);

    /**
     * This function is called when the element gets expanded or collapsed.
     * If expanded, disables the ovarall rating ajax and loads the form instance.
     * If collapses, enables the overall rating ajax request.
     * @param isExpanded true is expanded. false if collapsed.
     */
    component.expandHandler = (isExpanded) => {
        if (isExpanded) {
            component.overallRatingInstance.disable();
            component.formInstance.load();
        } else {
            component.overallRatingInstance.enable();
        }
    };

    component.show = () => {
        component.expandInstance.toggleExpand(true);
        hiddenHelper.show(component.$element);
    };

    component.hide = () => {
        hiddenHelper.hide(component.$element);
    };

    //Watchers
    component.expandInstance.onChange(component.expandHandler);
    component.formInstance.onSubmit(collapseAndSetLabel);
    component.formInstance.onReset(() => collapseAndSetLabel());

    function collapseAndSetLabel(label) {
        component.expandInstance.toggleExpand(false);
        component.labelInstance.setLabel(label);
    }
}

AsyncRatingContainer.getSelector = () => COMPONENT_SELECTOR;
controllerService.getAllInstances(AsyncRatingContainer);
