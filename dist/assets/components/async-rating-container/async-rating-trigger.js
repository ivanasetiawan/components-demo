'use strict';

import $ from 'jquery';
import Observable from '../class-observable/class-observable';
import loginAsync from '../async-login/async-login';
import hiddenHelper from './hidden-helper';

export default AsyncRatingTrigger;

const COMPONENT_SELECTOR = '[data-rating-trigger]';

function AsyncRatingTrigger(element) {
    const component = this;
    component.$element = $(element);
    component.observers = new Observable();
    component.visible = hiddenHelper.isShown(component.$element);

    component.onTrigger = (observer) => {
        component.observers.listen(observer);
    };

    component.clickHandler = () => {
        return loginAsync(() => {
            component.observers.notify();
            component.hide();
        });
    };

    component.hide = () => {
        component.visible = hiddenHelper.hide(component.$element);
    };

    component.show = () => {
        component.visible = hiddenHelper.show(component.$element);
    };

    component.$element.click(component.clickHandler);
}

AsyncRatingTrigger.getSelector = () => COMPONENT_SELECTOR;
