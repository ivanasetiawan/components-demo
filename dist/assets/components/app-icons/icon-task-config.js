var componentDir = 'src/components/app-icons/';

module.exports = {
    grunticon: {
        default: {
            files: [
                {
                    expand: true,
                    cwd: 'tmp',
                    src: ['*.svg'],
                    dest: componentDir + 'assets'
                }
            ],
            options: {
                loadersnippet: 'grunticon.loader.js',
                pngpath: 'icons',
                pngfolder: 'icons',
                enhanceSVG: true,
                corsEmbed: true,
                colors: {
                    black: '#000000',
                    blue: '#0071B3',
                    blueLighter: '#E6F2F7',
                    blueBrand: '#60C5F8',
                    greyDark: '#666666',
                    grey: '#999999',
                    greyLight: '#CCCCCC',
                    orange: '#F7A100',
                    pink: '#BA3486',
                    red: '#F03C30',
                    yellow: '#F8CE00',
                    green: '#22AB34',
                    white: '#FFFFFF',
                    brightBlue: '#5BC4F8'
                },
                // added custom classes just for icons on map not updating
                customselectors: {
                    'list-blue': ['.search-content-toggle__view-type:hover .icon-list-grey'],
                    'location-blue': ['.search-content-toggle__view-type:hover .icon-location-grey'],
                },
                defaultWidth: '300px',
                defaultHeight: '300px'
            }
        }
    },
    svgmin: {
        default: {
            files: [
                {
                    expand: true,
                    src: ['tmp/*.svg']
                }
            ]
        }
    },
    copy: {
        temp: {
            files: [
                {
                    expand: true,
                    flatten: true,
                    filter: 'isFile',
                    src: [componentDir + 'icons/*.svg'],
                    dest: 'tmp/'
                }
            ]
        },
        loader: {
            src: componentDir + 'assets/grunticon.loader.js',
            dest: componentDir + 'app-icons.min.js'
        }
    },
    clean: {
        default: [
            'tmp',
            componentDir + 'assets/preview.html',
            componentDir + 'assets/grunticon.loader.js'
        ]
    }
};
