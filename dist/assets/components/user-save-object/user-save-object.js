// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

// explicitly inject dependencies (alphabetically), only those needed
var $ = require('jquery');
var LoginDialog = require('../login-dialog/login-dialog');
import controllerService from '../service-controller/service-controller';
import Observable from '../class-observable/class-observable';

export default SaveObject;

// component configuration
var COMPONENT_SELECTOR = '[data-save-object]';
var HANDLE_SELECTOR = '[data-save-object-handle]';
var IS_SAVED_CLASS = 'is-saved';
var LOGIN_DIALOG_SELECTOR = '[data-dialog="user-form"]';
var ASYNC_OBJECT_RATING_SELECTOR = '[data-async-object-rating]';
var MULTIPLE_SAVE_COMPONENT_SELECTOR = '[data-connect-save-object-instances]';

function SaveObject(element) {
    var component = this;
    var $element = $(element);
    component.$element = $element;
    component.$saveHandle = $element.find(HANDLE_SELECTOR);
    component.$rating = $element.closest(ASYNC_OBJECT_RATING_SELECTOR);
    component.doPageRefresh = false;
    component.isSaved = component.$element.hasClass(IS_SAVED_CLASS);
    component.observers = new Observable();
    component.saveUrl = component.$saveHandle.attr('href');

    //To be used by observers!
    component.onSaved = function (observer) {
        component.observers.listen(observer);
    };

    component.$element.on('click', HANDLE_SELECTOR, function (event) {
        event.preventDefault();
        component.event = (event !== undefined) ? event : !component.event;
        component.saveIfLoggedIn(this.href, component.event);
    });

    // event is triggered in the async object rating component
    // when an object is rated, the object is saved
    component.$rating.on('objectrated', function () {
        component.toggleSavedState(component.$saveHandle.attr('href'), true);
    });
}

SaveObject.prototype.saveIfLoggedIn = function (url, isSaved) {
    var component = this;
    component.isSaved = (isSaved !== undefined) ? isSaved : !component.isSaved;
    component.isUserLoggedIn(function onSuccessfulLogin() {
        component.toggleSavedState(url, component.isSaved);
    });
};

SaveObject.prototype.isUserLoggedIn = function (onLoggedIn) {
    var component = this;
    var dialogElement = document.querySelector(LOGIN_DIALOG_SELECTOR);
    var url = dialogElement.dialog.isUserLoggedInUrl;

    return $.ajax({
        url: url,
        success: function (response) {
            if (response.LoggedIn === true) {
                onLoggedIn();
            } else {
                component.userLoginStatus = new LoginDialog(response.LoginUrl, function onSuccessfulLogin() {
                    component.doPageRefresh = true;
                    onLoggedIn();
                });
            }
        },
        error: function (response) {
            console.error('Error calling', url, response);
        }
    });
};

/**
 * Does an ajax request and toggles the 'is-saved' class if the response is
 * successful
 */
SaveObject.prototype.toggleSavedState = function (url, isSaved) {
    var component = this;
    component.isSaved = (isSaved !== undefined) ? isSaved : !component.isSaved;
    component.multipleInstances = component.$element.find(MULTIPLE_SAVE_COMPONENT_SELECTOR).length;

    return $.ajax({
        url: url,
        success: function (response) {
            if (SaveObject.isSuccesfulResponse(response)) {
                if (component.multipleInstances) {
                    $(COMPONENT_SELECTOR).each(function () {
                        $(this).toggleClass(IS_SAVED_CLASS, component.isSaved);
                    });
                } else {
                    component.$element.toggleClass(IS_SAVED_CLASS, component.isSaved);
                }
                /* component.isSaved gives me an event, this is clearly wrong, but the whole component is dirty */
                component.observers.notify(component.$element.hasClass(IS_SAVED_CLASS));
                component.$element.trigger('objectsaved', component.isSaved);
            } else {
                onError(response);
            }
        },
        error: onError,
        complete: function () {
            if (component.doPageRefresh === true) {
                window.location.reload();
            }
        }
    });

    function onError(response) {
        console.error('Error trying to save object using', component.url, response);
    }
};

SaveObject.prototype.save = function (isSaved) {
    var component = this;
    return component.toggleSavedState(component.saveUrl, isSaved);
};

SaveObject.isSuccesfulResponse = function (response) {
    return response.Result === 'OK';
};

// turn all elements with the default selector into components
// $(COMPONENT_SELECTOR).each(function(index, element) {
//     return new SaveObject(element);
// });
SaveObject.getSelector = () => COMPONENT_SELECTOR;
controllerService.getAllInstances(SaveObject);