'use strict';

// dependencies (alphabetically)
import $ from 'jquery';

var ArticleHeader = {};
module.exports = ArticleHeader;

// component configuration
const COMPONENT_ATTR = 'data-article-header';
const COMPONENT_SELECTOR = '[' + COMPONENT_ATTR + ']';

ArticleHeader.initialize = function($element) {
    const navHeight = $('.app-header').outerHeight();
    const headerHeight = $(COMPONENT_SELECTOR).outerHeight() - 100;
    const minWidth = 768;
    const minHeight = 1024;
    const win = $(window);
    this.$element = $($element);

    if (win.width() > minWidth && win.height() > minHeight) {
        win.on('scroll', () => {
            const start = navHeight;
            const end = headerHeight;
            let opacity = 1 - (win.scrollTop() - start) / (end - start);

            if (opacity > 1) {
                opacity = 1;
            } else if (opacity < 0.3) {
                opacity = 0.3;
            }
            this.$element.css('opacity', opacity);
        });
    }
};

if ($(COMPONENT_SELECTOR).length) {
    ArticleHeader.initialize(
        $(COMPONENT_SELECTOR)
    );
}