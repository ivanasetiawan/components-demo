'use strict';

import $ from 'jquery';
import throttle from 'lodash/throttle';
import debounce from 'lodash/debounce';

// what does this module expose?
export default StickyNavigation;

// component configuration
const COMPONENT_WRAPPER_SELECTOR = '[data-sticky-navigation-wrapper]';
const COMPONENT_SELECTOR = '[data-sticky-navigation]';
const LINKS_SELECTOR = '[data-sticky-navigation-links]';
const $window = $(window);
const THROTTLE_DELAY = 100;
const DEBOUNCE_DELAY = 150;

function StickyNavigation(element) {
    const component = this;
    component.$element = $(element);
    component.$links = component.$element.find(LINKS_SELECTOR);
    component.$navWrapper = component.$element.parents(COMPONENT_WRAPPER_SELECTOR);
    component.setStickyWidth();
    component.registerOnScroll();
    component.registeronResize();
}

StickyNavigation.prototype.setStickyWidth = function() {
    const component = this;
    const widthBase = component.$element.width();
    const heightBase = component.$links.height();
    component.$links.width(widthBase);
    component.$element.height(heightBase);
};

StickyNavigation.prototype.registeronResize = function() {
    const component = this;
    $window.on('resize', debounce(function () {
        component.setStickyWidth();
        component.registerOnScroll();
    }, DEBOUNCE_DELAY));
};

StickyNavigation.prototype.registerOnScroll = function() {
    const component = this;
    const stickyTop = component.$element.offset().top;

    $window.on('scroll', throttle(function () {
        const windowTop = $window.scrollTop();
        const stickyHeight = component.$element.height();
        const navWrapperHeight = component.$navWrapper.height();
        const navWrapperTop = component.$navWrapper.offset().top;

        if (stickyTop < windowTop && navWrapperHeight + navWrapperTop - stickyHeight > windowTop) {
            component.sticky();
        } else {
            component.noSticky();
        }
    }, THROTTLE_DELAY));
};

StickyNavigation.prototype.sticky = function() {
    const component = this;
    component.$links.addClass('sticky');
};

StickyNavigation.prototype.noSticky = function() {
    const component = this;
    component.$links.removeClass('sticky');
};

// turn all elements with the default selector into components
$(COMPONENT_SELECTOR).each(function(index, element) {
    return new StickyNavigation(element);
});
