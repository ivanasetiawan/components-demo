'use strict';

import $ from 'jquery';
import AsyncRatingContainer from '../async-rating-container/async-rating-container';
import AsyncRatingTrigger from '../async-rating-container/async-rating-trigger';
import SaveObject from '../user-save-object/user-save-object';
import controllerService from '../service-controller/service-controller';

export default ObjectActions;

const COMPONENT_SELECTOR = '[data-object-actions]';

function ObjectActions(element) {
    const component = this;
    component.$element = $(element);

    //If the async-rating container is not present, this class should not be loaded.
    if (!component.$element.find(AsyncRatingContainer.getSelector()).length) {
        return;
    }
    component.ratingContainer = controllerService.getInstance(AsyncRatingContainer, element);
    component.notitieTrigger = controllerService.getInstance(AsyncRatingTrigger, element);
    component.saveButton = controllerService.getInstance(SaveObject, element);

    component.saveButton.onSaved((isSaved) => {
        if (!isSaved) {
            component.ratingContainer.formInstance.reset();
        }
    });

    component.getRating = () => {
        return component.ratingContainer.overallRatingInstance.getValue();
    };

    component.ratingContainer.expandInstance.onChange((isExpanded) => {
        if (!isExpanded && component.getRating() == 0) {
            component.hide();
        }
    });

    component.ratingContainer.formInstance.onSubmit(() => {
        component.saveButton.save(true);
    });

    component.ratingContainer.formInstance.onReset(() => {
        component.hide();
    });

    component.notitieTrigger.onTrigger(() => {
        component.show();
    });

    component.show = () => {
        component.ratingContainer.show();
        component.notitieTrigger.hide();
    };

    component.hide = () => {
        component.ratingContainer.hide();
        component.notitieTrigger.show();
    };
}

ObjectActions.getSelector = () => COMPONENT_SELECTOR;
controllerService.getAllInstances(ObjectActions);
