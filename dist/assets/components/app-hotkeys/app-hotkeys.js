/* global gtmDataLayer */

'use strict';

import $ from 'jquery';
import mousetrap from 'mousetrap';

const Shake = require('shake.js');

export default AppHotkeys;

const COMPONENT_SELECTOR = '[data-app-hotkeys]';
const VERRASME_URL = '/verras-me/een-huis';
const VIBRATE_PATTERN = [300, 100, 100];
const MEDIA_VIEWER_PHOTOS_SELECTOR = '.media-viewer .media-viewer-view.media-viewer-fotos';
const MEDIA_VIEWER_OPEN_PHOTO_HASH = '#foto-1';

function AppHotkeys() {
    const component = this;

    component.registerHotkeys();

    if ('ondevicemotion' in window) {
        component.registerShakeListener();
    }
}

AppHotkeys.prototype.registerHotkeys = function () {
    mousetrap.bind('v', this.navigateToVerrasMe);
    mousetrap.bind('f', this.openMediaViewerIfPhotos);
};

AppHotkeys.prototype.registerShakeListener = function() {
    const shakeEvent = new Shake({
        threshold: 15,   // optional shake strength threshold
        timeout: 1000    // optional, determines the frequency of event generation
    });
    const supportsVibrate = 'vibrate' in navigator;

    window.addEventListener('shake', () => {
        if (supportsVibrate) {
            navigator.vibrate(VIBRATE_PATTERN);
        }
        this.navigateToVerrasMe();
    }, false);

    shakeEvent.start();
};

AppHotkeys.prototype.navigateToVerrasMe = function () {
    const lang = document.querySelector('html').getAttribute('lang');

    window.location = (lang != 'nl') ? '/' + lang + VERRASME_URL : VERRASME_URL;

    if (window.gtmDataLayer !== undefined) {
        gtmDataLayer.push({
            'event': 'userInteractionVerrasMe'
        });
    }
};

AppHotkeys.prototype.openMediaViewerIfPhotos = function () {
    const hasPhotos = document.querySelector(MEDIA_VIEWER_PHOTOS_SELECTOR);

    if (hasPhotos && window.location.hash === '') {
        window.location.hash = MEDIA_VIEWER_OPEN_PHOTO_HASH;
    }
};

// turn all elements with the default selector into components
$(COMPONENT_SELECTOR).each(() => new AppHotkeys());
