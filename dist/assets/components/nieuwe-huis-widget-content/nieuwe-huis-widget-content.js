// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

import $ from 'jquery';
import contentFetch from '../content-fetch/content-fetch';
export default NieuweHuisWidgetContent;

// component configuration
const CATEGORIES_URL_ATTR = 'data-content-fetch-url';
const COMPONENT_SELECTOR = '[data-nieuwe-huis-widget-content]';
// const handleComplete = function() { };

function NieuweHuisWidgetContent(element) {
    const component = this;
    component.$element = $(element);
    component.urlFeed = element.getAttribute(CATEGORIES_URL_ATTR);
    contentFetch(element, component.urlFeed);
}

// turn all elements with the default selector into components
$(COMPONENT_SELECTOR).each(function(index, element) {
    return new NieuweHuisWidgetContent(element);
});
