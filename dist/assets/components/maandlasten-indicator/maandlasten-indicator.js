// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

// explicitly inject dependencies (alphabetically), only those needed
var $ = require('jquery');

// what does this module expose?
module.exports = MaandlastenIndicator;

// component configuration
var COMPONENT_SELECTOR = '[data-maandlasten-indicator]';
var URL_SMALL_ATTR = 'data-maandlasten-indicator-small';
var URL_LARGE_ATTR = 'data-maandlasten-indicator-large';
var viewport = $(window).width();
var bpMortgage = 643;

function MaandlastenIndicator(element) {
    var component = this;
    component.injectIframe(element);
}

MaandlastenIndicator.prototype.injectIframe = function(element) {
    var component = this;
    component.$element = $(element);
    component.urlSmall = element.getAttribute(URL_SMALL_ATTR);
    component.urlLarge = element.getAttribute(URL_LARGE_ATTR);

    var iframeEl = $('<iframe frameborder="0" width="100%"/>');
    iframeEl.appendTo(COMPONENT_SELECTOR);
    component.iframe = component.$element.find('iframe');

    if (viewport >= bpMortgage) {
        component.iframe.attr({
            src: component.urlLarge,
            width: '100%',
        });
    } else {
        component.iframe.attr({
            src: component.urlSmall,
            width: '100%',
        });
    }
};

// turn all elements with the default selector into components
if (COMPONENT_SELECTOR.length) {
    $(COMPONENT_SELECTOR).each(function(index, element) {
        return new MaandlastenIndicator(element);
    });
}
