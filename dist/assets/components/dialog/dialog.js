// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

// explicitly inject dependencies (alphabetically), only those needed
var $ = require('jquery');
var isFunction = require('lodash/isFunction');
var AsyncForm = require('../async-form/async-form');
var SocialLogin = require('../social-login/social-login');
var AppSpinner = require('../app-spinner/app-spinner');
var DialogPolyfill = require('dialog-polyfill');

// what does this module expose?
module.exports = Dialog;

// component configuration
var COMPONENT_ATTR = 'data-dialog';
var COMPONENT_SELECTOR = '[data-dialog]';
var CLOSE_SELECTOR = '[data-dialog-close]';
var BODY_SELECTOR = '[data-dialog-body]';
var HANDLE_ATTR = 'data-dialog-handle';
var OPEN_DIALOG_CLASS = 'has-open-dialog';
var OPEN_DIALOG_SELECTOR = 'dialog[open]';
var CANCEL_CLASS = 'cancel-button';
var AJAX_SUBMIT_SELECTOR = '[dialog-ajax-submit]';
var SOCIAL_LOGIN_SELECTOR = '[data-social-login]';
var URL_ISUSERLOGGEDIN = 'data-isuserloggedinurl';
var SPINNER_SELECTOR = '[data-dialog-spinner]';
var KEY_CODES_PREVENT_SCROLL = [
    32, // spacebar
    33, // pageup
    34, // pagedown
    35, // end
    36, // home
    37, // left
    38, // up
    39, // right
    40  // down
];

var supportsDialog = (function () {
    var dialog = document.createElement('dialog');
    return (typeof dialog.show === 'function');
}());

var $window = $(window);
var $body = $('body');

function Dialog(element, options) {
    options = options || {};
    var component = this;
    element.dialog = this;
    component.element = element;
    component.$element = $(element);
    component.id = element.getAttribute(COMPONENT_ATTR);
    component.$close = component.$element.find(CLOSE_SELECTOR);
    component.$body = component.$element.find(BODY_SELECTOR);
    component.onFormSuccess = options.onFormSuccess;
    component.isUserLoggedInUrl = element.getAttribute(URL_ISUSERLOGGEDIN);
    component.$spinner = component.$element.find(SPINNER_SELECTOR);
    component.previousDialog = null;

    if (!supportsDialog) {
        DialogPolyfill.registerDialog(element);
    }

    $body.on('click', '[' + HANDLE_ATTR + '="' + component.id + '"]', function (event) {
        event.preventDefault();
        if (!component.element.open) {
            component.open();
        }
        if (event.target.href) {
            // remove the content from the dialog body to prevent
            // showing incorrect content on reopening the dialog
            component.$body.html('');
            component.getContent(event.target.href);
        }
    });

    component.$element.on('click', CLOSE_SELECTOR, function () {
        component.close();
    });

    component.$element.on('click', 'a.' + CANCEL_CLASS, function (event) {
        event.preventDefault();
        component.close();
    });

    component.$element.on('click', function (event) {
        var target = $(event.target);
        if (target.is('dialog')) {
            component.close();
        }
    });

    // Close key (escape)
    $(document).on('keydown', function (event) {
        if (event.ctrlKey || event.shiftKey || event.altKey || event.metaKey) {
            return;
        }

        // ESC key
        if (event.keyCode === 27) {
            component.close();
        }
    });
}

Dialog.prototype.getContent = function (url) {
    var component = this;
    component.spinner = new AppSpinner(component.$spinner[0]);
    component.spinner.show();

    return $.ajax({
        url: url,
        cache: false,
        data: {
            currentUrl: window.location.pathname
        },
        xhrFields: {
            withCredentials: true
        },
        dataType: 'json'
    })
        .then(function (response) {
            if (response.Result === 'OK') {
                component.updateContent(response.View || response.Html);
            }
        })
        .catch(function (err) {
            console.error(err);
            var path = url.replace('dialog/', '');
            window.location.href = path + '?ReturnUrl=' + window.location.pathname;
        })
        .then(function () {
            component.spinner.hide();
        });
};

Dialog.prototype.enableSocialLogin = function (body) {
    var socialLogin = body.find(SOCIAL_LOGIN_SELECTOR);
    if (socialLogin.length === 1) {
        return new SocialLogin(socialLogin[0]);
    }
    return null;
};

Dialog.prototype.updateContent = function (content) {
    var component = this;
    component.$body.html(content);

    var containsAsyncForm = (component.$body.find(AJAX_SUBMIT_SELECTOR).length > 0);
    if (containsAsyncForm) {
        component.asyncForm();
    }
    return component.enableSocialLogin(component.$body);
};

Dialog.prototype.asyncForm = function () {
    var component = this;
    return new AsyncForm(component.$body, {
        body: component.$body,
        success: function (response) {
            if (component.onFormSuccess) {
                component.onFormSuccess(response);
            } else {
                window.location.reload();
            }
            component.close();
        },
        submitCallback: function (body) {
            component.enableSocialLogin(body);
        }
    });
};

Dialog.prototype.hideVisibleDialogs = function () {
    var component = this;
    var $openDialog = $(OPEN_DIALOG_SELECTOR);
    if ($openDialog.length) {
        component.previousDialog = $openDialog[0];
        component.previousDialog.close();
    }
};

Dialog.prototype.restorePreviousDialogs = function () {
    var component = this;
    if (component.previousDialog != null) {
        component.previousDialog.showModal();
        component.previousDialog = null;
    }
};

Dialog.prototype.open = function () {
    var component = this;
    component.hideVisibleDialogs();
    component.element.showModal();

    // prevent scroll bar when the content of the window body
    // is larger then the dialog
    $body.addClass(OPEN_DIALOG_CLASS);
    $window.on('popstate.' + component.id, function () {
        component.close();
        component.enableScroll();
    });

    component.disableScroll();
};

Dialog.prototype.close = function () {
    var component = this;

    if (component.element.open && isFunction(component.element.close)) {
        component.element.close();
        component.enableScroll();
    }
    $body.removeClass(OPEN_DIALOG_CLASS);
    $window.off('popstate.' + component.id);
    component.enableScroll();
    component.restorePreviousDialogs();
};

/**
 * Disables scroll-events, including touch devices.
 */
Dialog.prototype.disableScroll = function () {
    if (window.addEventListener) { // older FF
        window.addEventListener('DOMMouseScroll', preventDefaultBehavior, false);
    }

    window.onwheel = preventDefaultBehavior; // modern standard
    window.onmousewheel = document.onmousewheel = preventDefaultBehavior; // older browsers, IE
    window.ontouchmove = preventDefaultBehavior; // mobile
    document.onkeydown = preventScrollBehavior;
};

/**
 * Enables scroll-events, including touch devices.
 */
Dialog.prototype.enableScroll = function () {
    if (window.removeEventListener) { // older FF
        window.removeEventListener('DOMMouseScroll', preventDefaultBehavior, false);
    }

    window.onmousewheel = document.onmousewheel = null;
    window.onwheel = null;
    window.ontouchmove = null;
    document.onkeydown = null;
};

/**
 * Prevent the default behavior.
 * If the parameter is an event, prevent its default behavior.
 * If not, the returnValue is false.
 * @param event     event that should have its default behavior prevented.
 */
function preventDefaultBehavior(event) {
    event = event || window.event;
    if (event.preventDefault) {
        event.preventDefault();
    }
    event.returnValue = false;
}

/**
 * Prevent any scrolling behavior using keyboard keys.
 * @param event     event that should have its default behavior prevented.
 */
function preventScrollBehavior(event) {
    if (KEY_CODES_PREVENT_SCROLL.indexOf(event.keyCode) !== -1) {
        event.preventDefault();
    }
}

// turn all elements with the default selector into components
$(COMPONENT_SELECTOR).each(function (index, element) {
    return new Dialog(element);
});
