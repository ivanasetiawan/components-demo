// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

// explicitly inject dependencies (alphabetically), only those needed
var $ = require('jquery');

// what does this module expose?
module.exports = SocialLogin;

// component configuration
var COMPONENT_SELECTOR = '[data-social-login]';
var PROVIDER_ATTR = 'data-social-login-provider';
var ROOT_URL_ATTR = 'data-social-login-root-url';
var LOGIN_DIALOG_SELECTOR = '[data-dialog="user-form"]';

function SocialLogin(element) {
    var component = this;

    component.$element = $(element);
    component.provider = element.getAttribute(PROVIDER_ATTR);
    component.state = {
        reauthenticate: false,
        callback: null,
        notify: null,
        notifySelector: null,
        notifyParent: null,
        returnUrl: null,
        rootUrl: element.getAttribute(ROOT_URL_ATTR)
    };

    // Act only when provider is set
    if (component.provider !== undefined) {
        component.state.returnUrl = component.getReturnUrl();

        // Bind handler
        component.$element.on('click', function() {
            component.doAuth();
        });
    }
}

SocialLogin.prototype.doAuth = function() {
    var component = this;
    // write handler to window
    component.writeHandler();

    // initiate login
    component.invokeLogin();

    // find (this) existing form and remove it
    var baseUrl = component.state.rootUrl.slice(0, -1);
    var authForm = $('<form/>').attr('method', 'POST').attr('target', 'login-popup').attr('action', baseUrl + '/mijn/sociallogin/externallogin/').hide();
    authForm.append($('<input>').attr('type', 'hidden').attr('name', 'provider').attr('value', component.provider));

    if (!component.state.returnUrl) {
        component.state.returnUrl = component.state.rootUrl;
    }
    authForm.append($('<input>').attr('type', 'hidden').attr('name', 'returnUrl').attr('value', component.state.returnUrl));
    authForm.append($('<input>').attr('type', 'hidden').attr('name', 'currentUrl').attr('value', document.location.pathname));
    authForm.append($('<input>').attr('type', 'hidden').attr('name', 'rerequest').attr('value', component.state.reauthenticate ? '1' : '0'));
    $('body').append(authForm);
    authForm.submit();
};

SocialLogin.prototype.invokeLogin = function() {
    var chrome = 100;
    var width = 500;
    var height = 500;
    var left = (screen.width - width) / 2;
    var top = (screen.height - height - chrome) / 2;
    var options = 'status=0,toolbar=0,location=1,resizable=1,scrollbars=1,left=' + left + ',top=' + top + ',width=' + width + ',height=' + height;
    window.open('about:blank', 'login-popup', options);
};

SocialLogin.prototype.postLoginHandler = function(success, returnUrl) {
    if (returnUrl) {
        setTimeout(function() {
            window.location.href = returnUrl;
        }, 160);
    }
};

SocialLogin.prototype.writeHandler = function() {
    var component = this;
    // set callback on global scope
    window.socialCall = (function(social) {
        return function(success, returnUrl) {
            if (success) {
                var dialogElement = document.querySelector(LOGIN_DIALOG_SELECTOR);
                if (dialogElement !== null) {
                    dialogElement.dialog.onFormSuccess();
                } else {
                    social.postLoginHandler(success, returnUrl);
                }
            }

            // remove myself
            window.socialCall = undefined;
        };
    })(component);
};

SocialLogin.prototype.getReturnUrl = function() {
    var qs = window.location.search.substr(1).split('&');
    var b = null;
    for (var i = 0; i < qs.length; ++i) {
        var p = qs[i].split('=');
        if (p.length !== 2 || p[0] !== 'ReturnUrl') {
            continue;
        }
        b = decodeURIComponent(p[1].replace(/\+/g, ' '));
    }
    return b;
};

// turn all elements with the default selector into components
$(COMPONENT_SELECTOR).each(function(index, element) {
    return new SocialLogin(element);
});
