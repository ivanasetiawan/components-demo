/* global google */

'use strict';

export {
    getKadasterLayer,
    getKadasterBaseLayer
};

function getKadasterBaseLayer() {
    const styles =
        [
            {
                'featureType': 'all',
                'elementType': 'labels.text.fill',
                'stylers': [
                    { 'saturation': 36 },
                    { 'color': '#333333' },
                    { 'lightness': 40 }
                ]
            },
            {
                'featureType': 'all',
                'elementType': 'labels.text.stroke',
                'stylers': [
                    { 'visibility': 'on' },
                    { 'color': '#ffffff' },
                    { 'lightness': 16 }
                ]
            },
            {
                'featureType': 'all',
                'elementType': 'labels.icon',
                'stylers': [
                    { 'visibility': 'off' }
                ]
            },
            {
                'featureType': 'administrative',
                'elementType': 'geometry.fill',
                'stylers': [
                    { 'color': '#fefefe' },
                    { 'lightness': 20 }
                ]
            },
            {
                'featureType': 'administrative',
                'elementType': 'geometry.stroke',
                'stylers': [
                    { 'color': '#fefefe' },
                    { 'lightness': 17 },
                    { 'weight': 1.2 }
                ]
            },
            {
                'featureType': 'landscape',
                'elementType': 'geometry',
                'stylers': [
                    { 'color': '#f5f5f5' },
                    { 'lightness': 20 }
                ]
            },
            {
                'featureType': 'poi',
                'elementType': 'geometry',
                'stylers': [
                    { 'color': '#f5f5f5' },
                    { 'lightness': 21 }
                ]
            },
            {
                'featureType': 'poi.park',
                'elementType': 'geometry',
                'stylers': [
                    { 'color': '#cce69f'},
                    { 'lightness': 21 }
                ]
            },
            {
                'featureType': 'road.highway',
                'elementType': 'geometry.fill',
                'stylers': [
                    { 'color': '#ffffff' },
                    { 'lightness': 17 }
                ]
            },
            {
                'featureType': 'road.highway',
                'elementType': 'geometry.stroke',
                'stylers': [
                    { 'color': '#ffffff' },
                    { 'lightness': 29 },
                    { 'weight': 0.2 }
                ]
            },
            {
                'featureType': 'road.arterial',
                'elementType': 'geometry',
                'stylers': [
                    { 'color': '#ffffff' },
                    { 'lightness': 18 }
                ]
            },
            {
                'featureType': 'road.local',
                'elementType': 'geometry',
                'stylers': [
                    { 'color': '#ffffff' },
                    { 'lightness': 16 }
                ]
            },
            {
                'featureType': 'transit',
                'elementType': 'geometry',
                'stylers': [
                    { 'lightness': '21' },
                    { 'color': '#e6e6e6' },
                    { 'visibility': 'on' }
                ]
            },
            {
                'featureType': 'transit.station.airport',
                'elementType': 'geometry.fill',
                'stylers': [
                    { 'color': '#f2f2f2' },
                    { 'lightness': '-3' }
                ]
            },
            {
                'featureType': 'water',
                'elementType': 'geometry',
                'stylers': [
                    { 'color': '#5f94ff' },
                    { 'gamma': '6' },
                    { 'lightness': '-5' }
                ]
            }
        ];

    return new google.maps.StyledMapType(styles, {
        name: 'Kadastraal Base Layer',
        maxZoom: 23,
        minZoom: 17
    });
}

function getKadasterLayer(map, baseKadasterUrl) {
    return new google.maps.ImageMapType({
        getTileUrl: function (coord, zoom) {
            const zfactor = Math.pow(2, zoom);
            const projection = map.getProjection();
            const top = projection.fromPointToLatLng(new google.maps.Point(coord.x * 256 / zfactor, coord.y * 256 / zfactor));
            const bot = projection.fromPointToLatLng(new google.maps.Point((coord.x + 1) * 256 / zfactor, (coord.y + 1) * 256 / zfactor));
            const bbox = top.lng() + ',' + bot.lat() + ',' + bot.lng() + ',' + top.lat();
            return baseKadasterUrl.replace('{bbox}', bbox);
        },
        tileSize: new google.maps.Size(256, 256),
        isPng: true,
        name: 'Bebouwing',
        maxZoom: 21,
        minZoom: 17
    });
}
