/* global google, gtmDataLayer */
'use strict';

// dependencies (alphabetically)
import $ from 'jquery';
import throttle from 'lodash/throttle';
import debounce from 'lodash/debounce';
import gmapsAsyncLoad from '../gmaps-async/gmaps-async';
import * as kadastraalLayers from './gmaps-kadastraal-layers';

export default ObjectMap;

// component configuration
const COMPONENT_SELECTOR = '[data-object-map]';
const CANVAS_SELECTOR = '[data-object-map-canvas]';
const CONFIG_SELECTOR = '[data-object-map-config]';
const ENHANCED_CLASS = 'is-interactive-map';
const EVENT_NAMESPACE = '.object-map';
const LOADED_CLASS = 'is-loaded';
const INIT_TYPE_ATTR = 'data-object-map-init-type';

const MAP_LEGEND_SELECTOR = '[data-object-map-legend]';
const MAP_TYPE_SELECT_SELECTOR = '[data-object-map-type-select]';
const MAP_TYPE_SELECT_DEFAULT_SELECTOR = '.view-default';
const MAP_TYPE_SELECT_STREET_VIEW_SELECTOR = '.view-streetview';
const MAP_TYPE_SELECT_SATELLITE_VIEW_SELECTOR = '.view-satellite';
const MAP_TYPE_SELECT_KADASTRAAL_SELECTOR = '.view-kadastraal';
const MAP_ZOOM_CONTROLS_SELECTOR = '.object-map-zoom-controls';
const MAP_ZOOM_ZOOMIN_SELECTOR = '[data-map-zoom-zoomin]';
const MAP_ZOOM_ZOOMOUT_SELECTOR = '[data-map-zoom-zoomout]';
const DEFAULT_ZOOM_LEVEL = 14;
const MIN_ZOOM_LEVEL = 0;
const MAX_ZOOM_LEVEL = 21;
const MIN_ZOOM_LEVEL_KADASTRAAL = 19;

const IS_VISIBLE_CLASS = 'is-visible';
const IS_HIDDEN_CLASS = 'is-hidden';

const $window = $(window);

function ObjectMap(element) {
    const component = this;

    component.$element = $(element);
    component.$canvas = component.$element.find(CANVAS_SELECTOR);
    component.$mapLegend = component.$element.find(MAP_LEGEND_SELECTOR);
    component.$mapZoomControls = component.$element.find(MAP_ZOOM_CONTROLS_SELECTOR);
    component.percelenLayer = null;

    if (component.$element.find(CONFIG_SELECTOR).text().length) {
        component.config = JSON.parse(component.$element.find(CONFIG_SELECTOR).text());
    } else {
        component.config = JSON.parse(component.$element.find('[data-object-map-config-injected]').text());
    }

    component.$element.addClass(ENHANCED_CLASS);

    // Set configured map type as checked
    let mapTypeSelectSelector;
    switch (component.config.defaultMapType) {
        case 'satellite':
            mapTypeSelectSelector = MAP_TYPE_SELECT_SATELLITE_VIEW_SELECTOR;
            break;
        case 'kadastraal':
            mapTypeSelectSelector = MAP_TYPE_SELECT_KADASTRAAL_SELECTOR;
            break;
        case 'roadmap':
        default:
            mapTypeSelectSelector = MAP_TYPE_SELECT_DEFAULT_SELECTOR;
            break;
    }
    $(mapTypeSelectSelector).prop('checked', true);

    component.$element.on('change', MAP_TYPE_SELECT_SELECTOR, (event) => { component.switchMapType(event.currentTarget.value); });
    component.$element.on('click', MAP_ZOOM_ZOOMIN_SELECTOR, (event) => { component.zoomIn(event); });
    component.$element.on('click', MAP_ZOOM_ZOOMOUT_SELECTOR, (event) => { component.zoomOut(event); });

    const initType = component.$element.attr(INIT_TYPE_ATTR);
    if (initType === 'lazy') {
        $window
            .on('scroll' + EVENT_NAMESPACE, throttle(() => { component.checkPosition(); }, 50))
            .on('resize' + EVENT_NAMESPACE, debounce(() => { component.checkPosition(); }, 200));

        // check once to see if page was (re)loaded with the map already in view.
        component.checkPosition();
    } else {
        component.loadMap();
    }
}

ObjectMap.prototype.switchMapType = function (mapType) {
    const component = this;

    component.map.overlayMapTypes.clear();
    component.$mapLegend.removeClass(IS_VISIBLE_CLASS);

    switch (mapType) {
        case 'default':
            component.panorama.setVisible(false);
            component.map.setMapTypeId(google.maps.MapTypeId.ROADMAP);
            break;

        case 'satellite':
            component.map.setMapTypeId(google.maps.MapTypeId.HYBRID);
            component.panorama.setVisible(false);
            break;

        case 'kadastraal':
            if (component.map.getZoom() < MIN_ZOOM_LEVEL_KADASTRAAL) {
                component.map.setZoom(MIN_ZOOM_LEVEL_KADASTRAAL);
            }
            component.map.overlayMapTypes.insertAt(0, component.percelenLayer);
            component.map.overlayMapTypes.insertAt(0, component.pandenLayer);
            component.map.setMapTypeId('kadastraal');
            component.panorama.setVisible(false);
            component.$mapLegend.addClass(IS_VISIBLE_CLASS);
            break;

        case 'streetview':
            component.panorama.setVisible(true);
            break;
    }
    component.updateGTMMapType(mapType);
};

/**
 * Checks whether the map is inside the view and if so, creates te map
 */
ObjectMap.prototype.checkPosition = function () {
    const component = this;
    const mapOffset = component.$canvas.offset();
    const mapHeight = component.$canvas.height() / 4; // Load when map quarter scrolled in
    const isInView = $window.scrollTop() + $window.height() > mapOffset.top + mapHeight;

    if (isInView) {
        $window.off('scroll' + EVENT_NAMESPACE);
        $window.off('resize' + EVENT_NAMESPACE);
        component.loadMap();
    }
};

ObjectMap.prototype.loadMap = function () {
    const component = this;

    if (component.config.useStubbedMapsApi) {
        // assuming window.google is a stubbed Maps API
        component.createMap();
        return;
    }

    gmapsAsyncLoad().then(function () {
        component.createMap();

        setTimeout(function () {
            component.$element.addClass(LOADED_CLASS);
            component.switchMapType(component.config.defaultMapType);
        }, 1500); //allow some time for tiles to be loaded
    });
};

/**
 * Create and insert map on component's canvas element.
 * @return {*} component's Google Map instance.
 */
ObjectMap.prototype.createMap = function () {
    const component = this;
    const config = component.config;
    const google = window.google;
    const latLng = new google.maps.LatLng(config.lat, config.lng);

    component.map = new google.maps.Map(component.$canvas[0], component.getMapOptions());
    component.panorama = component.map.getStreetView();

    component.panorama.setOptions(component.getPanoramaOptions());
    component.panorama.setPosition(latLng);

    // Plot marker to the Map
    const marker = {
        url: config.markerUrl,
        anchor: new google.maps.Point(30, 53),
        scaledSize: new google.maps.Size(60, 66)
    };

    component.marker = new google.maps.Marker({
        position: latLng,
        map: component.map,
        title: config.markerTitle,
        icon: marker
    });

    // prepare 2 kadaster layers
    component.map.mapTypes.set('kadastraal', kadastraalLayers.getKadasterBaseLayer());
    component.percelenLayer = kadastraalLayers.getKadasterLayer(component.map, config.baseKadasterUrl);
    component.pandenLayer = kadastraalLayers.getKadasterLayer(component.map, config.basePandenUrl);

    // Set correct point of view for Steet View
    const service = new google.maps.StreetViewService;

    service.getPanoramaByLocation(component.panorama.getPosition(), 50, function (panoramaData) {
        if (panoramaData !== null) {
            $(MAP_TYPE_SELECT_STREET_VIEW_SELECTOR).addClass(IS_VISIBLE_CLASS);

            component.panorama.setPano(panoramaData.location.pano);

            const panoramaCenter = panoramaData.location.latLng;
            const heading = google.maps.geometry.spherical.computeHeading(panoramaCenter, latLng);

            const pov = component.panorama.getPov();
            pov.heading = heading;
            component.panorama.setPov(pov);
        }
    });

    // Only show the marker and zoom controls when we're not in Street View
    google.maps.event.addListener(component.map.getStreetView(), 'visible_changed', function () {
        if (this.getVisible()) {
            component.marker.setMap(null);
            component.$mapZoomControls.addClass(IS_HIDDEN_CLASS);
        } else {
            component.marker.setMap(component.map);
            component.$mapZoomControls.removeClass(IS_HIDDEN_CLASS);
        }
    });

    return component.map;
};

/**
 * Create and return map options based on component's configuration.
 * @returns {Object} map options
 */
ObjectMap.prototype.getMapOptions = function () {
    var config = this.config;
    var google = window.google;

    return {
        zoom: config.zoom || DEFAULT_ZOOM_LEVEL,
        center: new google.maps.LatLng(config.lat, config.lng),
        scrollwheel: false,
        gestureHandling: 'cooperative',
        // Map control options
        disableDefaultUI: true,
        mapTypeControlOptions: {
            mapTypeIds: [
                google.maps.MapTypeId.ROADMAP,
                google.maps.MapTypeId.SATELLITE,
                'kadastraal'
            ]
        },
        mapTypeId: config.defaultMapType,

        scaleControl: true,
        overviewMapControl: true,
        zoomControlOptions: {
            style: google.maps.ZoomControlStyle.SMALL
        },

        // Remove POI
        styles: [
            {
                featureType: 'poi',
                elementType: 'labels',
                stylers: [
                    { visibility: 'off' }
                ]
            }
        ]
    };
};

ObjectMap.prototype.updateGTMMapType = function (mapTypeName) {
    if (window.gtmDataLayer !== undefined) {
        gtmDataLayer.push({
            'event': 'mapTypeChanged',
            'mapTypeName': mapTypeName
        });
    }
};

ObjectMap.prototype.getPanoramaOptions = function () {
    return {
        addressControl: false,
        linksControl: true,
        panControl: true,
        zoomControl: true,
        fullscreenControl: true,
        enableCloseButton: false
    };
};

ObjectMap.prototype.zoomIn = function (event) {
    const component = this;
    event.preventDefault();
    const currentZoomLevel = component.map.getZoom();

    if (currentZoomLevel !== MAX_ZOOM_LEVEL) {
        component.map.setZoom(currentZoomLevel + 1);
    }
};


ObjectMap.prototype.zoomOut = function (event) {
    const component = this;
    event.preventDefault();
    const currentZoomLevel = component.map.getZoom();

    if (currentZoomLevel !== MIN_ZOOM_LEVEL) {
        component.map.setZoom(currentZoomLevel - 1);
    }
};

// turn all elements with the default selector into components
$(COMPONENT_SELECTOR).each((index, element) => new ObjectMap(element));
