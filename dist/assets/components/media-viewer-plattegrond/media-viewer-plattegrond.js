'use strict';
/* globals fpViewer */
module.exports = PlattegrondView;

var $ = require('jquery');
var load = require('load-script');
var Menu = require('../media-viewer-menu/media-viewer-menu');
var AppSpinner = require('../app-spinner/app-spinner');

var COMPONENT_SELECTOR = '[data-media-viewer-plattegrond]';
var PLATTEGROND_CONTAINER_CLASS = 'media-viewer-plattegrond-container';
var PLATTEGROND_CONTAINER_SELECTOR = '.' + PLATTEGROND_CONTAINER_CLASS;
var PLATTEGROND_CONTAINER_SRC_ATTR = 'data-plattegrond-src';
var PLATTEGROND_VIEW_SELECT_RADIO_SELECTOR = '[data-view-select]';
var PLATTEGROND_2D_VIEW_SELECT_RADIO_SELECTOR = '[data-view-select][value="2d"]';
var PLATTEGROND_2D_VIEW_SELECTOR = '[data-plattegrond-2d-view]';
var PLATTEGROND_3D_VIEW_SELECTOR = '[data-plattegrond-3d-view]';
var PLATTEGROND_SPINNER_VIEW_SELECTOR = '[data-plattegrond-spinner-view]';
var PLATTEGROND_SPINNER_SELECTOR = '[data-plattegrond-spinner]';
var PLATTEGROND_MENU_HEADER_SELECTOR = '[data-plattegrond-menu-header]';
var PLATTEGROND_MENU_FOOTER_SELECTOR = '[data-plattegrond-menu-footer]';
var PLATTEGROND_MENU_ITEM_TEMPLATE_SELECTOR = '[data-plattegrond-menu-item-template]';
var PLATTEGROND_ZOOM_IN_SELECTOR = '[data-plattegrond-zoom-in]';
var PLATTEGROND_ZOOM_OUT_SELECTOR = '[data-plattegrond-zoom-out]';
var BASE_URL_ATTR = 'data-base-url';
var MENU_ITEMS_SELECTOR = '[data-menu-items]';
var FLOORPLANNER_ACTIVE_CLASS = 'active';
var SHOW_CLASS = 'show';

function PlattegrondView() {
    var view = this;
    view.$container = $(PLATTEGROND_CONTAINER_SELECTOR);
    view.$2dView = view.$container.find(PLATTEGROND_2D_VIEW_SELECTOR);
    view.$3dView = view.$container.find(PLATTEGROND_3D_VIEW_SELECTOR);

    view.src = view.$container.attr(PLATTEGROND_CONTAINER_SRC_ATTR);
    view.baseUrl = view.$container.attr(BASE_URL_ATTR);

    view.$zoomIn = $(PLATTEGROND_ZOOM_IN_SELECTOR);
    view.$zoomOut = $(PLATTEGROND_ZOOM_OUT_SELECTOR);
    view.$zoomIn.on('click', () => view.zoomInHandler());
    view.$zoomOut.on('click', () => view.zoomOutHandler());

    view.menuItemTemplate = $(PLATTEGROND_MENU_ITEM_TEMPLATE_SELECTOR).html();

    view.loaded = false;
    view.designId = 0;
    view.floors = {};
    view.viewType = '2d';
    // make sure 2d is the initial state of the radio select
    $(PLATTEGROND_2D_VIEW_SELECT_RADIO_SELECTOR).prop('checked', true);

    if (view.loaded) {
        return;
    }

    view.$spinnerView = $(PLATTEGROND_SPINNER_VIEW_SELECTOR);
    view.spinner = new AppSpinner($(PLATTEGROND_SPINNER_SELECTOR).get(0));
    view.spinner.show();

    $(PLATTEGROND_VIEW_SELECT_RADIO_SELECTOR).on('change', function() {
        view.viewType = (this.value === '2d') ? '2d' : '3d';
        view.display();
    });

    $(window).on('mediaviewermenuitemselected', function(e, data) {
        if (data.type && data.type === 'plattegrond') {
            view.designId = data.menuParam;
            view.display();
        }
    });

    var theme = {
        ui: {
            initView: '2d', // or '3d'
            rotate3d: true // automatically rotate the 3d view
        },
        engine: {
            wallSectionHeight: null,
            wallTopColor: 0x7F7F7F, // top colour of walls in 3D
            showGrid: false,
            showDims: true, // visibility of dimension lines (both auto and manual)
            engineAutoDims: false, // generate and show automatic dimensions
            showObjects: true,
            showLabels: true, // visibility for all labels, including room names and dims
            useMetric: true// false means Imperial system, affects dimensions
        }
    };

    load(view.baseUrl + '/assets/vendor-floorplanner.js', function(err) {
        if (typeof err !== 'undefined') {
            fpViewer.init({
                projectId: null,
                box2d: view.$2dView.get(0),
                box3d: view.$3dView.get(0),
                theme: theme,
                fmlUrl: view.src,
                callback: function(project) {
                    project.floors.forEach(function(floor) {
                        view.floors[floor.id] = {
                            id: floor.id,
                            name: floor.name,
                            design: floor.designs[0]
                        };
                    });
                    view.designId = project.floors[0].id;
                    view.setupMenu();
                    view.display();
                    view.$spinnerView.removeClass(FLOORPLANNER_ACTIVE_CLASS);
                    view.spinner.hide();
                    view.loaded = true;
                },
                jquery: $
            });
        }
    });
}

PlattegrondView.prototype.dispose = function() {
    var view = this;
    fpViewer.cleanup();

    var $headerFloorSelect = $(PLATTEGROND_MENU_HEADER_SELECTOR);
    var $footerFloorSelect = $(PLATTEGROND_MENU_FOOTER_SELECTOR);

    var headerMenu = $headerFloorSelect.find(MENU_ITEMS_SELECTOR);
    var footerMenu = $footerFloorSelect.find(MENU_ITEMS_SELECTOR);
    headerMenu.empty();
    footerMenu.empty();

    view.loaded = false;

    // detach event listeners
    $(PLATTEGROND_VIEW_SELECT_RADIO_SELECTOR).off('change');
    $(window).off('mediaviewermenuitemselected');

    if (view.menu) {
        view.menu.dispose();
    }
};

PlattegrondView.prototype.setupMenu = function() {
    var view = this;

    var $headerFloorSelect = $(PLATTEGROND_MENU_HEADER_SELECTOR);
    var $footerFloorSelect = $(PLATTEGROND_MENU_FOOTER_SELECTOR);

    var headerMenu = $headerFloorSelect.find(MENU_ITEMS_SELECTOR);
    var footerMenu = $footerFloorSelect.find(MENU_ITEMS_SELECTOR);

    Object.keys(view.floors).forEach(function(key) {
        var floor = view.floors[key];
        var menuItem = view.renderMenuItem({id: floor.id, name: floor.name});
        headerMenu.append(menuItem);
        footerMenu.append(menuItem);
    });

    view.menu = new Menu($headerFloorSelect, $footerFloorSelect);

    if (Object.keys(view.floors).length > 1) {
        $headerFloorSelect.addClass(SHOW_CLASS);
        $footerFloorSelect.addClass(SHOW_CLASS);
    }

    view.menu.transformHeader();
    view.menu.setActive(view.designId);
};

PlattegrondView.prototype.display = function() {
    var view = this;

    view.$2dView.removeClass(FLOORPLANNER_ACTIVE_CLASS);
    view.$3dView.removeClass(FLOORPLANNER_ACTIVE_CLASS);

    var floor = view.floors[view.designId];

    if (view.viewType === '2d') {
        view.$2dView.addClass(FLOORPLANNER_ACTIVE_CLASS);
        fpViewer.display2D(floor.design);
    } else {
        view.$3dView.addClass(FLOORPLANNER_ACTIVE_CLASS);
        fpViewer.display3D(floor.design);
    }
};

PlattegrondView.prototype.renderMenuItem = function(item) {
    var html = this.menuItemTemplate;
    // replace placeholders in template by values from option data
    for (var prop in item) {
        if (item.hasOwnProperty(prop)) {
            var pattern = new RegExp('{' + prop + '}', 'g');
            html = html.replace(pattern, item[prop]);
        }
    }
    return html;
};

PlattegrondView.prototype.zoomInHandler = function() {
    var view = this;

    if (view.loaded) {
        fpViewer.zoomIn();
    }
};

PlattegrondView.prototype.zoomOutHandler = function() {
    var view = this;

    if (view.loaded) {
        fpViewer.zoomOut();
    }
};

// turn all elements with the default selector into components
$(COMPONENT_SELECTOR).each(function(index, element) {
    // use jquery to get data attribute because of IE 9/10
    var autoInitialize = $(element).data('mediaViewerAutoInitialize');
    if (autoInitialize === 'true') {
        return new PlattegrondView(element);
    }
});
