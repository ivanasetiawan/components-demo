# {client} Static

This project contains all static front-end files organised in unique views and reusable components.

**Please read the Authoring documentation (located at the bottom of this document) before making any changes to the code.**


## Module based development

This project encourages module based development and separation of concerns.
Each module - view or component - has its own directory. HTML is used for content structure and semantics.
(S)CSS is used for appearance. (Common)JS is used for behaviour. In addition a test and documentation (README)
is bundled per module.

## Kick start

**Clone**

	git clone ...

**Install all dependencies**

You may want to update your Node.js installation if you haven't already. Download it here https://nodejs.org/download/

	$ npm install

Integration servers have issues when trying to install Grunticon, so install it separately:

	$ npm run install-grunticon

For Windows (if and when we are using npm packages written in C++)

    $ npm install --msvs_version=2013

**Build and watch for devs**

    $ npm run guide
	$ npm run develop

**Build dist and guide (for CI):**

	$ npm run guide

**Build and preview locally**

	$ npm run guide
	$ npm start

**Create a new module (view or component)**

    $ npm run create

**Start a watcher on test files**

	$npm run test-watch

If your component includes a .css or .js file, then you need to require the component in the .css/.js file of a
view, otherwise these files will not be included in the build of index.css/.js.

**Local configuration**

By default, BrowserSync will reload the page when any html, css or js file is updated. You can only override the
BrowserSync behavior by creating a `config.local.js` in the project root folder. Example:

    module.exports = {
        browserSyncFiles: [
            'dist/assets/index.css',
            'dist/**/search.html',
            'dist/assets/*.js'
        ]
    };

In the example above BrowserSync will only reload when there are changes in the `search.html` view, and not in other
views. This will prevent unnecessary reloads (or 'flickering') when making changes.

## Read the documentation

* [Authoring assets](docs/authoring-assets.md)
* [Authoring templates (HTML)](docs/authoring-templates.md)
* [Authoring styles (SCSS/CSS)](docs/authoring-styles.md)
* [Authoring scripts (JS)](docs/authoring-scripts.md)
* [Creating modules](docs/module-crud.md)
* [Using version control](docs/using-version-control.md)
