// dependencies
const fs = require('fs');
const gulp = require('gulp');
const runSequence = require('run-sequence').use(gulp);

// task dependencies
const browserSyncTask = require('./tasks/browserSync');
const bundleTasks = require('./tasks/bundle');
const cleanTask = require('./tasks/clean-dist');
const copyTasks = require('./tasks/copy-files');
const eslintTask = require('./tasks/eslint');
const htmlTasks = require('./tasks/html');
const imageminTask = require('./tasks/run-imagemin');
const minifyInitialTask = require('./tasks/minify-initial');
const moduleTasks = require('./tasks/module');
const scssTask = require('./tasks/scss');
const testTasks = require('./tasks/test');
const zipDistTask = require('./tasks/zip-dist');

// One Rule to Config Them All
global.config = require('./tasks/config');
global.paths = global.config.paths;
global.isWatching = false;

// override main config with optional local settings
if (fs.existsSync('./config.local.js')) {
    const localConfig = require('./config.local');
    global.config = Object.assign({}, global.config, localConfig);
}

// tasks
gulp.task('build',
    [
        'build_html',
        'build_js',
        'build_scss_{client}',
        'build_scss_{client}inbusiness',
        'copy_assets',
        'copy_floorplanner',
        'copy_highcharts',
        'copy_krpano',
        'copy_other',
        'copy_stubs'
    ]
);
gulp.task('build_clean', runSequenceTask('clean_dist', 'build'));
gulp.task('build_dist', runSequenceTask('build_clean', 'build_previews')); // Bamboo uses this task to build
gulp.task('build_guide', runSequenceTask('build_clean', 'build_previews'));
gulp.task('build_html', htmlTasks.buildHtml);
gulp.task('build_js', runSequenceTask('eslint', 'build_js_app', 'build_js_vendor'));
gulp.task('build_js_app', bundleTasks.buildJsApp);
gulp.task('build_js_vendor', bundleTasks.buildJsVendor);
gulp.task('build_previews', htmlTasks.buildPreviews);
gulp.task('build_scss_{client}inbusiness', scssTask('index-{client}inbusiness'));
gulp.task('build_scss_{client}', scssTask('index-{client}'));
gulp.task('clean_dist', cleanTask);
gulp.task('default', ['build_guide']);
gulp.task('develop_fib', ['serve', 'watch_fib']);
gulp.task('develop_{client}', ['serve', 'watch_{client}']);
gulp.task('copy_assets', copyTasks.assets);
gulp.task('copy_floorplanner', copyTasks.floorplanner);
gulp.task('copy_highcharts', copyTasks.highcharts);
gulp.task('copy_krpano', copyTasks.krpano);
gulp.task('copy_other', copyTasks.otherFiles);
gulp.task('copy_stubs', copyTasks.stubs);
gulp.task('create_module', moduleTasks.createModule);
gulp.task('eslint', eslintTask);
gulp.task('imagemin', imageminTask);
gulp.task('minify_initial', minifyInitialTask);
gulp.task('serve', browserSyncTask);
gulp.task('test_run', testTasks.runTest);
gulp.task('test_watch', testTasks.watchTest);
gulp.task('watch_fib', watchTask(['build_scss_{client}', 'build_scss_{client}inbusiness']));
gulp.task('watch_{client}', watchTask(['build_scss_{client}']));
gulp.task('zip_dist', zipDistTask);

/**
 * @return {Function} - a runSequence function with callback at the end, so that the overall time the sequence took
 * gets logged.
 */
function runSequenceTask() {
    const args = Array.prototype.slice.call(arguments);
    return function(cb) {
        if (typeof args[args.length - 1] !== 'function') {
            args[args.length] = cb;
        }
        runSequence.apply(this, args);
    };
}

/**
 * Start watchers on all the things we want watched.
 *
 * @param {Array} scssTasks - the tasks to use for compiling SCSS, eg. ['build_scss_{client}', 'build_scss_{client}inbusiness']
 */
function watchTask(scssTasks) {
    return function() {
        global.isWatching = true;
        gulp.watch(global.paths.assetFiles, {interval: global.config.fileWatchInterval}, ['copy_assets']);
        gulp.watch(global.paths.htmlFiles, {interval: global.config.fileWatchInterval}, ['build_html', 'build_previews']);
        gulp.watch(global.paths.scssFiles, {interval: global.config.fileWatchInterval}, scssTasks);
        gulp.watch(global.paths.jsFiles, {interval: global.config.fileWatchInterval}, ['eslint']);
        bundleTasks.watchJs(); // use watchify instead of gulp.watch for faster rebundling
        testTasks.watchTest();
    }
}
