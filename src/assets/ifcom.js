/**
 * This library is used by iframes in external websites. Do not remove.
 */
var IFCom = (function () {
    // Options
    var options = {
        host : {
            iframeId : 'client-widget'
        },
        content : {
            parentProxyId : 'client-widget-proxy-host'
        }
    };

    // Parse message parameters
    var parseParameters = function (message) {
        var parameters = {};
        var pairs = message.split(/&/);

        for (var i = 0; i < pairs.length; i++) {
            var val = pairs[i].split(/=/);
            parameters[val[0]] = val[1];
        }
        return parameters;
    };

    // Set message in location url
    var setMessageInSrc = function (src, message) {
        var pos = src.indexOf('#');
        if (pos > -1) {
            src = src.substr(0, src.indexOf('#') + 1) + message;
        } else {
            src = src + '#' + message;
        }
        return src;
    };

    // Get message from hash
    var getMessage = function (hash) {
        return hash.substr(hash.indexOf('#') + 1);
    };

    // Test for HTML5 postMessage support
    var supportsPostMessage = function () {
        return window.postMessage ? true : false;
    };

    // Find position of an element
    var findPosition = function (obj) {
        var curleft = 0,
            curtop = 0;

        if (obj.offsetParent) {
            // continually traverse the DOM to get the position of the element
            do {
                curleft += obj.offsetLeft;
                curtop += obj.offsetTop;
                obj = obj.offsetParent;
            } while (!!obj); // once it reaches the top, it will return undefined and convert to false

            return { 'left' :  curleft, 'top' : curtop };
        }
    };

    // Get window scroll position
    var getScrollPosition = function () {
        var doc = document.documentElement,
            body = document.body,
            curleft = (doc && doc.scrollLeft || body && body.scrollLeft || 0),
            curtop = (doc && doc.scrollTop  || body && body.scrollTop  || 0);

        return { 'left' :  curleft, 'top' : curtop };
    };

    // Public
    return function (type, isProxy) {
        var ifcom = this;

        // Test for HTML5 postMessage support
        ifcom.usePostMessage = supportsPostMessage();

        // Host methods
        ifcom.host = {
            // Track first page load
            firstPageLoad : true,

            // Forward message to host
            forwardMessage : function () {
                var message = getMessage(document.location.hash);
                if (message.length > 0) {
                    // Send to global variable in host
                    top.ifcom.host.onMessageReceived(message);
                }
            },

            // Process received message
            onMessageReceived: function (message) {
                var messageToParse = ifcom.usePostMessage ? message.data : message;
                if (typeof messageToParse !== 'string') {
                    return;
                }
                var parameters = parseParameters(messageToParse);

                // Get iframe reference
                if (!ifcom.iframe) {
                    ifcom.iframe = document.getElementById(options.host.iframeId);
                }

                // Set height
                if ('height' in parameters) {
                    ifcom.iframe.height = parseInt(parameters.height, 10);

                    //Start of hack for MKLR-941

                    if (message.origin.indexOf("webshop.clientdesk.nl") > -1) {
                        if (ifcom.iframe.height < 1000) {
                            ifcom.iframe.height = 2000;
                        }
                    }
                    //End of hack for MKLR-941

                    // Scroll to top of widget
                    if (!ifcom.host.firstPageLoad) {
                        // Find position of widget
                        var pos = findPosition(ifcom.iframe);

                        // Get current scroll position
                        var scroll = getScrollPosition();

                        // Scroll host frame
                        window.scrollTo(scroll.left, pos.top - 80);
                    }
                }

                // Set as ready
                if ('ready' in parameters) {
                    ifcom.host.firstPageLoad = false;
                }
            }
        };

        // Content methods
        ifcom.content = {
            // Send message from content
            sendMessage : function (message) {
                if (ifcom.usePostMessage) {
                    // Use HTML5 postMessage to top directly
                    window.parent.postMessage(message, '*');
                } else {
                    // Get proxy reference
                    if (!ifcom.parentProxy) {
                        ifcom.parentProxy = document.getElementById(options.content.parentProxyId);
                    }

                    // Use hash in iframe urls
                    if (ifcom.parentProxy) {
                        // Set message as location
                        ifcom.parentProxy.contentWindow.location = setMessageInSrc(ifcom.parentProxy.src, message);

                        // Trigger resize event in proxy
                        ifcom.parentProxy.width = ifcom.parentProxy.width == 50 ?  100 : 50;
                    }
                }
            }
        };

        // Initialize
        if (window.addEventListener) { // Modern browsers
            if (type === 'host') {
                if (isProxy) {
                    window.addEventListener('resize', ifcom.host.forwardMessage, false);
                } else {
                    if (ifcom.usePostMessage) {
                        window.addEventListener('message', ifcom.host.onMessageReceived, false);
                    }
                }
            }
        } else if (window.attachEvent) { // IE
            if (type === 'host') {
                if (isProxy) {
                    window.attachEvent('onresize', ifcom.host.forwardMessage);
                } else {
                    if (ifcom.usePostMessage) {
                        window.attachEvent('onmessage', ifcom.host.onMessageReceived);
                    }
                }
            }
        }
    };
} ());

var ZoekWidget = {
    init: function (url) {
        // Add host to url
        var host = encodeURIComponent(document.location.href);

        // Build url from:
        // - Given url (optional query string)
        // - Host (? or & seperator depending on presence of existing query string in url)
        var qs = url.indexOf('?') < 0 ? '?' : '&';

        // Concat url
        url += qs + 'host=' + host;

        // Load iframe
        document.write('<iframe width="100%" height="250" frameborder="0" scrolling="no" id="client-widget" src="' + url + '"></iframe>');
    }
};
