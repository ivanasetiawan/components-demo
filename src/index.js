/**
 * App
 * include all views, which in turn include all their components
 */

// prevent console errors in legacy browsers (needs to be included first)
require('./components/_dev-console/dev-console');

// validation library used in forms
require('jquery-validation-unobtrusive');

// views (alphabetically)
require('./views/home/home');
require('./views/beoordelingen-widget/beoordelingen-widget');
require('./views/content-{client}-huis/content-{client}-huis');
require('./views/content-page/content-page');
require('./views/cms-article/cms-article');
require('./views/contact-form-brochure/contact-form-brochure');
require('./views/contact-form-makelaar/contact-form-makelaar');
require('./views/contact-form-object/contact-form-object');
require('./views/contact-form-plan-viewing/contact-form-plan-viewing');
require('./views/fib-home/fib-home');
require('./views/fib-makelaars-landing/fib-makelaars-landing');
require('./views/fib-search/fib-search');
require('./views/maandlasten-indicator-landing/maandlasten-indicator-landing');
require('./views/makelaars-aanmeld/makelaars-aanmeld');
require('./views/makelaars-kantoor/makelaars-kantoor');
require('./views/makelaars-landing/makelaars-landing');
require('./views/makelaars-collaborators/makelaars-collaborators');
require('./views/nieuwbouwproject-detail/nieuwbouwproject-detail');
require('./views/nieuwe-huis-checklist/nieuwe-huis-checklist');
require('./views/nieuwe-huis-koppelen/nieuwe-huis-koppelen');
require('./views/nieuwe-huis-overzicht/nieuwe-huis-overzicht');
require('./views/makelaars-search-results/makelaars-search-results');
require('./views/object-detail/object-detail');
require('./views/object-detail-print/object-detail-print');
require('./views/object-meld-een-fout/object-meld-een-fout');
require('./views/object-share-email/object-share-email');
require('./views/search/search');
require('./views/search-no-results/search-no-results');
require('./views/user-cadeauflow/user-cadeauflow');
require('./views/user-claimmyhouse/user-claimmyhouse');
require('./views/user-login/user-login');
require('./views/user-my-house/user-my-house');
require('./views/user-object-rating/user-object-rating');
require('./views/user-profile/user-profile');
require('./views/user-recovery/user-recovery');
require('./views/user-register/user-register');
require('./views/user-report-abuse/user-report-abuse');
require('./views/user-reviews/user-reviews');
require('./views/user-reviews-write/user-reviews-write');
require('./views/user-saved-objects/user-saved-objects');
require('./views/cookie-policy-page/cookie-policy-page');
require('./views/zoek-op-kaart/zoek-op-kaart');

require('./components/media-foto360/media-foto360');
require('./components/custom-select-box/custom-select-box');

