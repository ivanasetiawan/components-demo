// // Unit test for cms-featured-articles component
// // Mocha and Chai are always included
'use strict';

import MakelaarAds from './makelaar-ads';
import $ from 'jquery';
import sinonTest from 'sinon-test';
sinon.test = sinonTest.configureTest(sinon);

let instance;
const fixture = getFixture();

describe('Makelaar Ads component', function () {
    beforeEach('create instance', function () {
        instance = new MakelaarAds(fixture);
    });

    it('should have an `resetAds` method', function () {
        expect(instance.resetAds).to.be.a('function');
    });

    it('should have an `loadAds` method', function () {
        expect(instance.loadAds).to.be.a('function');
    });

    it('should have an `handleClickEvent` method', function () {
        expect(instance.unregisterClickEvent).to.be.a('function');
    });
});

describe('Makelaar Ads component', function () {
    beforeEach('create instance', function () {
        instance = new MakelaarAds(fixture);
        instance.resetAds();
    });

    it('should reset the makelaar ad element and add a class `reset-ads`', function () {
        expect(instance.$element.attr('class')).to.have.string('reset-ads');
        expect(instance.$element.find('.makelaar-ads-item').attr('class')).to.not.contain('loaded');
    });
});

describe('Makelaar Ads component', function () {
    beforeEach('create instance', function () {
        instance = new MakelaarAds(fixture);
        instance.visibleItems();
    });

    it('should find visible element(s) on the DOM', function () {
        expect(instance.$element.length > 0).to.be.true;
    });
});

describe('Makelaar Ads component', function () {
    // arrange
    const loadAdsAjaxStub = sinon.stub(MakelaarAds.prototype, 'loadAdsAjax');
    const visibleAdsSpy = sinon.spy();

    it('should show makelaar ads on init state', sinon.test(function() {
        // act
        MakelaarAds.prototype.loadAds(visibleAdsSpy);

        // assert
        assert(loadAdsAjaxStub.called);
    }));

    it('should get the ads from the server', sinon.test(function() {
        // arrange
        const feedUrl = 'https://dondraper.{client}.io/api/makelaarads/getmakelaarads/?makelaaradunitid={client}-wonen&searchArea=Land Nederland&geogebiedtype=8&geogebiedtypenaam=land';
        const ajaxStub = sinon.stub($, 'get').yieldsTo('success');
        const setAdsStub = sinon.stub(MakelaarAds.prototype, 'setAds');
        const togglePromoAdForMakelaarIfSpaceAvailableStub = sinon.stub(MakelaarAds.prototype, 'togglePromoAdForMakelaarIfSpaceAvailable');

        // act
        MakelaarAds.prototype.loadAdsAjax(feedUrl);

        // assert
        expect(setAdsStub).to.not.throw(ReferenceError);
        expect(togglePromoAdForMakelaarIfSpaceAvailableStub).to.not.throw(ReferenceError);

        // restore stubs
        ajaxStub.restore();
        setAdsStub.restore();
        togglePromoAdForMakelaarIfSpaceAvailableStub.restore();

    }));

    it('should handle that the location is changed by the user', sinon.test(function() {
        // arrange
        const handleMakelaarAdsOnResultsUpdatedStub = sinon.stub(MakelaarAds.prototype, 'handleMakelaarAdsOnResultsUpdated');

        // act
        $(document).trigger('resultsUpdated', {url: '/koop/utrecht/0-200000/'});

        // assert
        assert(handleMakelaarAdsOnResultsUpdatedStub.called);

        // restore `handleMakelaarAdsOnResultsUpdatedStub`
        handleMakelaarAdsOnResultsUpdatedStub.restore();
    }));

    it('should update the makelaarads only when the user searches for a new location', sinon.test(function() {
        // arrange
        const createMakelaarAdsStub = sinon.stub(MakelaarAds.prototype, 'createMakelaarAds');
        const getCurrentUrlStub = sinon.stub(MakelaarAds.prototype, 'getCurrentUrl').returns('utrecht');
        const setCurrentUrlStub = sinon.stub(MakelaarAds.prototype, 'setCurrentUrl');
        const getAsyncUrlStub = sinon.stub(MakelaarAds.prototype, 'getAsyncUrl').returns('amsterdam');

        // act
        MakelaarAds.prototype.handleMakelaarAdsOnResultsUpdated();

        // assert
        assert(createMakelaarAdsStub.called);
        assert(setCurrentUrlStub.calledWith('amsterdam'));

        // restore stubs
        createMakelaarAdsStub.restore();
        getCurrentUrlStub.restore();
        setCurrentUrlStub.restore();
        getAsyncUrlStub.restore();
    }));

    it('should not update the makelaarads when the user searches for the same location', sinon.test(function() {
        // arrange
        const createMakelaarAdsStub = sinon.stub(MakelaarAds.prototype, 'createMakelaarAds');
        const getCurrentUrlStub = sinon.stub(MakelaarAds.prototype, 'getCurrentUrl').returns('utrecht');
        const getAsyncUrlStub = sinon.stub(MakelaarAds.prototype, 'getAsyncUrl').returns('utrecht');

        // act
        MakelaarAds.prototype.handleMakelaarAdsOnResultsUpdated();

        // assert
        assert(createMakelaarAdsStub.notCalled);

        // restore stubs
        createMakelaarAdsStub.restore();
        getCurrentUrlStub.restore();
        getAsyncUrlStub.restore();
    }));

    it('should register the ad data only once', sinon.test(function() {
        // arrange
        const registerClickStub = sinon.stub(MakelaarAds.prototype, 'registerClick');

        // act
        MakelaarAds.prototype.handleClick();

        // assert
        assert(registerClickStub.calledOnce);

        // restore stubs
        registerClickStub.restore();
    }));

    it('should remove the event handler after it has registered the click', sinon.test(function() {
        // arrange
        const unregisterClickEventStub = sinon.stub(MakelaarAds.prototype, 'unregisterClickEvent');
        const handleElementOnClickStub = sinon.stub(MakelaarAds.prototype, 'handleElementOnClick');

        // act
        MakelaarAds.prototype.handleClickEvent();

        // assert
        assert(unregisterClickEventStub.calledOnce);

        // restore stubs
        unregisterClickEventStub.restore();
        handleElementOnClickStub.restore();
    }));
});

function getFixture() {
    let element = document.createElement('div');
    element.setAttribute('data-makelaar-ads', '');
    element.setAttribute('data-makelaars-ads-feed-url', '/stubs/components/makelaar-ads/getMakelaarAds.json');

    let makelaarAdItem = document.createElement('a');
    makelaarAdItem.setAttribute('data-makelaar-ads-item', '');

    let makelaarHeader = document.createElement('h4');
    makelaarHeader.setAttribute('makelaar-ads-header', '');

    // Makelaar's image
    let makelaarImgWrap = document.createElement('div');
    makelaarImgWrap.setAttribute('class', 'makelaar-ads-image-wrap');

    let makelaarImg = document.createElement('img');
    makelaarImg.setAttribute('data-makelaar-ads-logo', '');

    makelaarImgWrap.appendChild(makelaarImg);

    // Makelaar's text
    let makelaarTextWrap = document.createElement('div');
    makelaarTextWrap.setAttribute('class','makelaar-ads-text');

    let makelaarTitle = document.createElement('h4');
    makelaarTitle.setAttribute('data-makelaar-ads-title', '');

    let makelaarTextBody = document.createElement('p');
    makelaarTextBody.setAttribute('data-makelaar-ads-body', '');

    let makelaarTextBodyLine1 = document.createElement('span');
    makelaarTextBodyLine1.setAttribute('data-makelaar-ads-line1', '');

    let makelaarTextBodyLine2 = document.createElement('span');
    makelaarTextBodyLine2.setAttribute('data-makelaar-ads-line2', '');

    let makelaarTextWebsite = document.createElement('p');
    makelaarTextWebsite.setAttribute('data-makelaar-ads-website', '');

    makelaarTextBody.appendChild(makelaarTextBodyLine1);
    makelaarTextBody.appendChild(makelaarTextBodyLine2);

    makelaarTextWrap.appendChild(makelaarTitle);
    makelaarTextWrap.appendChild(makelaarTextBody);
    makelaarTextWrap.appendChild(makelaarTextWebsite);

    makelaarAdItem.appendChild(makelaarImgWrap);
    makelaarAdItem.appendChild(makelaarTextWrap);

    element.appendChild(makelaarAdItem);
    element.appendChild(makelaarHeader);

    let makelaaradsFeedAsync = document.createElement('div');
    makelaaradsFeedAsync.setAttribute('data-makelaars-ads-feed-async', '');
    makelaaradsFeedAsync.setAttribute('data-makelaars-ads-feed-async-url','http://dondraper.{client}.localhost/api/makelaarads/getmakelaarads/?makelaaradunitid={client}-wonen&searchArea=Amsterdam&geogebiedtype=3&geogebiedtypenaam=plaats&count=3');
    element.appendChild(makelaaradsFeedAsync);

    return element;
}
