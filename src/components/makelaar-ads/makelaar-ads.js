// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

// explicitly inject dependencies (alphabetically), only those needed
import $ from 'jquery';

// what does this module expose?
export default MakelaarAds;

// component configuration
const COMPONENT_SELECTOR = '[data-makelaar-ads]';
const ITEM_SELECTOR = '[data-makelaar-ads-item]';
const ITEM_IMAGE_SELECTOR = '[data-makelaar-ads-logo]';
const ITEM_TITLE_SELECTOR = '[data-makelaar-ads-title]';
const ITEM_BODY_LINE1_SELECTOR = '[data-makelaar-ads-line1]';
const ITEM_BODY_LINE2_SELECTOR = '[data-makelaar-ads-line2]';
const ITEM_WEBSITE_SELECTOR = '[data-makelaar-ads-website]';
const MAKELAARADS_FEED_URL = 'data-makelaars-ads-feed-url';
const MAKELAARADS_CLICK_URL = 'data-makelaars-ads-click-url';
const ITEM_HEADER_SELECTOR = '[makelaar-ads-header]';
const MAKELAARADS_ASYNC_URL_SELECTOR = '[data-makelaarads-feed-async]';
const EVENT_NAMESPACE_MAKELAARAD = '.makelaar-ads-item';

function MakelaarAds(element, baseUrl) {
    const component = this;
    component.urlClick = element.getAttribute(MAKELAARADS_CLICK_URL);
    component.$element = $(element);
    component.$items = component.$element.find(ITEM_SELECTOR);
    component.ads = []; // init we have 0 ads

    component.resetAds();
    component.loadAds(component.visibleItems(), baseUrl);
    component.handleClickEvent();
}

MakelaarAds.prototype.resetAds = function() {

    const component = this;
    const items = component.$items;

    for (let index = 0; index < items.length; index++) {
        let item = items[index];
        $(item).find(ITEM_TITLE_SELECTOR).empty();
        $(item).find(ITEM_IMAGE_SELECTOR).attr('src', '');
        $(item).find(ITEM_BODY_LINE1_SELECTOR).empty();
        $(item).find(ITEM_BODY_LINE2_SELECTOR).empty();
        $(item).find(ITEM_WEBSITE_SELECTOR).html('');
        $(item).attr('href', null);
        $(item).attr('class', 'makelaar-ads-item');
        $(item).attr('data-globalId', '');
        $(item).find(ITEM_IMAGE_SELECTOR).remove();
    }

    component.$element.addClass('reset-ads');
};

MakelaarAds.prototype.visibleItems = function() {
    const component = this;
    return component.$items.filter(':visible');
};

MakelaarAds.prototype.loadAds = function(visibleAds, baseUrl) {
    const component = this;
    const adsToRequest = visibleAds.length;
    const feedUrl = baseUrl + '&count=' + adsToRequest;
    component.loadAdsAjax(feedUrl);
};

MakelaarAds.prototype.loadAdsAjax = function(feedUrl) {
    const component = this;
    $.get({
        url: feedUrl,
        crossDomain: true,
        dataType: 'json',
        success: function(data) {
            component.setAds(data.MakelaarAds);
            component.togglePromoAdForMakelaarIfSpaceAvailable();
        }
    });
};

MakelaarAds.prototype.updateObjects = function(data) {
    return data;
};

MakelaarAds.prototype.setAds = function(ads) {
    const component = this;

    for (let index = 0; index < ads.length; index++) {
        let slot = component.getAvailableSlot();
        let mad = ads[index];

        //If the ad is not beeing displayed yet, render it
        if (component.isAdAlreadyBeingDisplayed(mad) === false) {
            this.renderAd(slot, mad);
        }

    }

    if (ads.length > 0) {
        component.$element.removeClass('reset-ads');
        component.$element.addClass('injected');
    }

    $.each(ads, function() {
        component.ads.push(this);
    }, this);
};

MakelaarAds.prototype.togglePromoAdForMakelaarIfSpaceAvailable = function() {
    const component = this;
    //If we have any ads set header and add border to each ad
    if (component.ads.length > 0) {
        component.$element.find(ITEM_HEADER_SELECTOR).css('max-height', 'inherit');
        component.$element.find(ITEM_HEADER_SELECTOR).show();
        // If there's only 1 ad but we have 2 or 3 avaiable slots, we show the ad and on the 2nd slot we show the ad promotion for makelaars
        if (component.ads.length === 1 && component.visibleItems().length > 1) {
            const promoAd = {
                GlobalId: 0,
                Titel: 'Bent u ook een NVM makelaar?',
                TekstRegels: ['Adverteer hier met uw kantoor in', 'uw werkgebied'],
                LinkText: 'Klik voor meer informatie',
                ImageUrl: 'data:image/svg+xml;charset=US-ASCII,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20xlink%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink%22%20viewBox%3D%220%200%2048%2048%22%3E%3Csymbol%20id%3D%22e%22%20viewBox%3D%220%20-48%2048%2048%22%20fill%3D%22%233888C5%22%3E%3Cpath%20fill%3D%22none%22%20d%3D%22M0%200v-48h48V0z%22%3E%3C%2Fpath%3E%3C%2Fsymbol%3E%3Csymbol%20id%3D%22f%22%20viewBox%3D%22-0.0002%20-47.9998%2048%2048%22%20fill%3D%22%233888C5%22%3E%3Cdefs%20fill%3D%22%233888C5%22%3E%3Cpath%20id%3D%22a%22%20d%3D%22M48-48H0V0h48z%22%20fill%3D%22%233888C5%22%3E%3C%2Fpath%3E%3C%2Fdefs%3E%3CclipPath%20id%3D%22b%22%20fill%3D%22%233888C5%22%3E%3Cuse%20href%3D%22%23a%22%20overflow%3D%22visible%22%20fill%3D%22%233888C5%22%3E%3C%2Fuse%3E%3Cg%20clip-path%3D%22url%28%23b%29%22%20fill%3D%22%233888C5%22%3E%3Cg%20fill%3D%22none%22%20stroke%3D%22%233888C5%22%20stroke-width%3D%22.25%22%20stroke-miterlimit%3D%2210%22%3E%3Cpath%20d%3D%22M2-48V0M4-48V0M6-48V0M8-48V0M10-48V0M12-48V0M14-48V0M16-48V0M18-48V0M20-48V0M22-48V0M24-48V0M26-48V0M28-48V0M30-48V0M32-48V0M34-48V0M36-48V0M38-48V0M40-48V0M42-48V0M44-48V0M46-48V0%22%20fill%3D%22%233888C5%22%3E%3C%2Fpath%3E%3C%2Fg%3E%3Cg%20fill%3D%22none%22%20stroke%3D%22%233888C5%22%20stroke-width%3D%22.25%22%20stroke-miterlimit%3D%2210%22%3E%3Cpath%20d%3D%22M0-2h48M0-4h48M0-6h48M0-8h48M0-10h48M0-12h48M0-14h48M0-16h48M0-18h48M0-20h48M0-22h48M0-24h48M0-26h48M0-28h48M0-30h48M0-32h48M0-34h48M0-36h48M0-38h48M0-40h48M0-42h48M0-44h48M0-46h48%22%20fill%3D%22%233888C5%22%3E%3C%2Fpath%3E%3C%2Fg%3E%3Cpath%20d%3D%22M47.75-.25v-47.5H.25v47.5h47.5M48%200H0v-48h48V0z%22%20fill%3D%22%233888C5%22%3E%3C%2Fpath%3E%3C%2Fg%3E%3C%2FclipPath%3E%3C%2Fsymbol%3E%3Csymbol%20id%3D%22g%22%20viewBox%3D%22-0.0002%20-48.0001%2048%2048%22%20fill%3D%22%233888C5%22%3E%3Cg%20opacity%3D%22.4%22%20fill%3D%22%233888C5%22%3E%3Cdefs%20fill%3D%22%233888C5%22%3E%3Cpath%20id%3D%22c%22%20opacity%3D%22.4%22%20d%3D%22M0-48V0h48v-48z%22%20fill%3D%22%233888C5%22%3E%3C%2Fpath%3E%3C%2Fdefs%3E%3CclipPath%20id%3D%22d%22%20fill%3D%22%233888C5%22%3E%3Cuse%20href%3D%22%23c%22%20overflow%3D%22visible%22%20fill%3D%22%233888C5%22%3E%3C%2Fuse%3E%3Cpath%20clip-path%3D%22url%28%23d%29%22%20fill%3D%22none%22%20stroke%3D%22%233888C5%22%20stroke-width%3D%22.25%22%20stroke-miterlimit%3D%2210%22%20d%3D%22M24%200v-48M48-24H0M48-16H0M48-32H0M32-48V0M16-48V0M47.75-.25L.25-47.75M.25-.25l47.5-47.5M24-14c-5.522%200-10-4.477-10-10%200-5.522%204.478-10%2010-10%205.523%200%2010%204.478%2010%2010%200%205.523-4.477%2010-10%2010z%22%3E%3C%2Fpath%3E%3Cpath%20d%3D%22M24-4C12.946-4%204-12.947%204-24s8.946-20%2020-20c11.053%200%2020%208.947%2020%2020S35.052-4%2024-4z%22%20clip-path%3D%22url%28%23d%29%22%20fill%3D%22none%22%20stroke%3D%22%233888C5%22%20stroke-width%3D%22.25%22%20stroke-miterlimit%3D%2210%22%3E%3C%2Fpath%3E%3Cpath%20d%3D%22M40-42H8c-1.1%200-2%20.9-2%202v32c0%201.1.9%202%202%202h32c1.1%200%202-.9%202-2v-32c0-1.1-.9-2-2-2z%22%20clip-path%3D%22url%28%23d%29%22%20fill%3D%22none%22%20stroke%3D%22%233888C5%22%20stroke-width%3D%22.25%22%20stroke-miterlimit%3D%2210%22%3E%3C%2Fpath%3E%3Cpath%20d%3D%22M38-44H10c-1.1%200-2%20.9-2%202v36c0%201.1.9%202%202%202h28c1.1%200%202-.9%202-2v-36c0-1.1-.9-2-2-2z%22%20clip-path%3D%22url%28%23d%29%22%20fill%3D%22none%22%20stroke%3D%22%233888C5%22%20stroke-width%3D%22.25%22%20stroke-miterlimit%3D%2210%22%3E%3C%2Fpath%3E%3Cg%20clip-path%3D%22url%28%23d%29%22%20fill%3D%22%233888C5%22%3E%3Cpath%20d%3D%22M47.75-.25v-47.5H.25v47.5h47.5M48%200H0v-48h48V0z%22%20fill%3D%22%233888C5%22%3E%3C%2Fpath%3E%3C%2Fg%3E%3Cpath%20d%3D%22M42-40H6c-1.1%200-2%20.9-2%202v28c0%201.1.9%202%202%202h36c1.1%200%202-.9%202-2v-28c0-1.1-.9-2-2-2z%22%20clip-path%3D%22url%28%23d%29%22%20fill%3D%22none%22%20stroke%3D%22%233888C5%22%20stroke-width%3D%22.25%22%20stroke-miterlimit%3D%2210%22%3E%3C%2Fpath%3E%3C%2FclipPath%3E%3C%2Fg%3E%3C%2Fsymbol%3E%3Cg%20fill%3D%22%233888C5%22%3E%3Cpath%20d%3D%22M9.474%2043.959v-7.755h3.068l2.887%205.37h.03v-5.37h2.12v7.755h-2.992l-2.963-5.671h-.03v5.67h-2.12zM18.616%2036.204h2.361l1.684%205.639h.03l1.895-5.64h2.24l-2.871%207.756h-2.662l-2.677-7.755zM27.804%2043.959v-7.755h3.654l1.7%205.273h.03l1.804-5.273h3.533v7.755h-2.21v-5.983h-.03l-2.09%205.983h-2.166l-1.985-5.983h-.03v5.983h-2.21zM36.4%204.042a2.11%202.11%200%200%201%202.11%202.11V32.23a1.17%201.17%200%200%201-1.17%201.17h-6.433a1.17%201.17%200%200%201-1.17-1.17v-8.955a2.409%202.409%200%200%201%202.408-2.408h.24a3.94%203.94%200%201%200-3.94-3.94l.013%2015.303a1.17%201.17%200%200%201-1.17%201.17h-6.422a1.17%201.17%200%200%201-1.17-1.17l.014-15.304a3.94%203.94%200%201%200-3.941%203.94h.24a2.409%202.409%200%200%201%202.408%202.41v8.954a1.17%201.17%200%200%201-1.17%201.17h-6.432a1.17%201.17%200%200%201-1.17-1.17V6.151a2.11%202.11%200%200%201%202.11-2.11l8.675.011a3.074%203.074%200%200%201%203.074%203.074l-.156.05a3.886%203.886%200%200%200%20.735%207.7h-.01a3.884%203.884%200%200%200%20.733-7.7l-.156-.05a3.074%203.074%200%200%201%203.074-3.074l8.676-.01z%22%20fill%3D%22%233888C5%22%3E%3C%2Fpath%3E%3C%2Fg%3E%3C%2Fsvg%3E',
                Href: '/content/default.aspx?pagina=/nl/algemene-teksten-{client}-sites/{client}nl/verkoop/makelaarads'
            };
            const slot = component.getAvailableSlot();
            component.renderAd(slot, promoAd);
        }
    }
};

MakelaarAds.prototype.isAdAlreadyBeingDisplayed = function(ad) {
    const component = this;
    const displayedAds = component.visibleItems();

    for (let index = 0; index < displayedAds.length; index++) {
        let slot = displayedAds[index];
        if ($(slot).attr('data-globalId') === ad.GlobalId) {
            return true;
        }

    }
    return false;
};

MakelaarAds.prototype.getAvailableSlot = function() {
    const component = this;
    const slots = component.visibleItems();

    for (let index = 0; index < slots.length; index++) {
        let slot = slots[index];
        if (slot.getAttribute('data-globalId') === '') {
            return slot;
        }
    }
};

MakelaarAds.prototype.renderAd = function(slot, ad) {
    $(slot).find('.makelaar-ads-image-wrap').prepend($('<img data-makelaar-ads-logo class="makelaar-ads-image" src="' + ad.ImageUrl + '">'));
    $(slot).attr('data-globalId', ad.GlobalId);
    $(slot).find(ITEM_TITLE_SELECTOR).text(ad.Titel);
    $(slot).find(ITEM_BODY_LINE1_SELECTOR).text(ad.TekstRegels[0]);
    $(slot).find(ITEM_BODY_LINE2_SELECTOR).text(ad.TekstRegels[1]);
    $(slot).find(ITEM_WEBSITE_SELECTOR).text(ad.LinkText);
    $(slot).addClass('loaded');
    $(slot).attr('href', ad.Href);
    $(slot).css('max-height', 'inherit');
    $(slot)
        .attr('data-index', ad.Index)
        .attr('data-geogebiednaam', ad.GeoGebiedNaam)
        .attr('data-geogebiedniveau', ad.GeoGebiedNiveau)
        .attr('data-makelaarid', ad.MakelaarId);
};

MakelaarAds.prototype.handleClickEvent = function() {
    const component = this;
    component.unregisterClickEvent('click', EVENT_NAMESPACE_MAKELAARAD);
    component.handleElementOnClick();
};

MakelaarAds.prototype.handleElementOnClick = function() {
    const component = this;
    component.$items.on('click', function() {
        const clickedAd = $(this);
        component.handleClick(clickedAd);
    });
};

MakelaarAds.prototype.handleClick = function(clickedAd) {
    const component = this;
    component.registerClick(clickedAd);
};

MakelaarAds.prototype.unregisterClickEvent = function(eventName) {
    const component = this;
    component.$items.off(eventName);
};

MakelaarAds.prototype.registerClick = function(clickedAd) {
    const component = this;
    const paramArray = {
        makelaarAdId: clickedAd.attr('data-globalId'),
        makelaarAdPosition: clickedAd.attr('data-index')-1, //This is retarded for impressions the ad position has 0 index based and for recording impressions is 1 index based.
        geoGebiedNaam: clickedAd.attr('data-geogebiednaam'),
        geoGebiedNiveau: clickedAd.attr('data-geogebiedniveau'),
        makelaarId: clickedAd.attr('data-makelaarid')
    };
    console.log('idd', paramArray.makelaarAdId);
    $.ajax({
        url: component.urlClick,
        type: 'POST',
        data: paramArray
    });
};

MakelaarAds.prototype.setCurrentUrl = function(value) {
    return $(COMPONENT_SELECTOR).attr(MAKELAARADS_FEED_URL, value);
};

MakelaarAds.prototype.getCurrentUrl = function() {
    return $(COMPONENT_SELECTOR).attr(MAKELAARADS_FEED_URL);
};

MakelaarAds.prototype.getAsyncUrl = function() {
    return $(MAKELAARADS_ASYNC_URL_SELECTOR).attr('data-makelaarads-feed-async-url');
};

MakelaarAds.prototype.handleMakelaarAdsOnResultsUpdated = function() {
    let makelaarAdsAsyncUrl = MakelaarAds.prototype.getAsyncUrl();
    let makelaarAdsUrl = MakelaarAds.prototype.getCurrentUrl();

    // check if the url is updated
    if (makelaarAdsAsyncUrl != makelaarAdsUrl) {
        MakelaarAds.prototype.createMakelaarAds(makelaarAdsAsyncUrl);
    }

    MakelaarAds.prototype.setCurrentUrl(makelaarAdsAsyncUrl);
};

MakelaarAds.prototype.createMakelaarAds = function(MakelaarAdsAsyncUrl) {
    $(COMPONENT_SELECTOR).each((index, element) => new MakelaarAds(element, MakelaarAdsAsyncUrl));
};

MakelaarAds.initialize = function() {
    // automatically turn all elements with the default selector into components
    $(COMPONENT_SELECTOR).each((index, element) => new MakelaarAds(element, element.getAttribute(MAKELAARADS_FEED_URL)));

    $(document).on('resultsUpdated', function() {
        MakelaarAds.prototype.handleMakelaarAdsOnResultsUpdated();
    });
};

MakelaarAds.initialize();