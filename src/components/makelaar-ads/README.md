# Makelaar Ads component

## Functionality

The makelaar ads component taregets makelaar ads on search results page. The component initiated itself when `data-makelaar-ads` exists on the view.

## Usage

For testing the component locally, update the legacyFeedUrl to a local json:
	legacyFeedUrl = '/stubs/components/makelaar-ads/getMakelaarAds.json';

Add `data-makelaar-ads` attribute on the element to trigger the functionality.

Necessary data attributes are:
- `data-makelaar-ads-item` is for each slot
- `data-makelaar-ads-logo` is for the logo of each makelaar
- `data-makelaar-ads-title` is for the makelaar's title
- `data-makelaar-ads-line1` is for the first line of the body
- `data-makelaar-ads-line2` is for the second line of the body
- `data-makelaar-ads-website` is for the makelaar's website