# Ajax component

## Functionality

Wrapped jQuery ajax method which other modules can listen to.
 
## Usage

### Do request

```javascript
const ajax = require('../ajax/ajax');
ajax('url', options); // same signature as $.ajax
```

### Listen to responses

Other modules can listen to these events using `ajax.onError` and `ajax.onSuccess`.

```javascript
const ajax = require('../ajax/ajax');
ajax.onError(err => console.log('ajax error', err));
ajax.onSuccess(data => console.log('ajax success', data));
```
