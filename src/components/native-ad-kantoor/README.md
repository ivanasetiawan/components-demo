# Native Kantoor Ad component

## Functionality

To show how the native ad look (example: list item on search results list).

## Usage

Native format and styles can be found on DFP. (https://www.google.com/dfp/6835#delivery/ListNativeStyles)

For this ad, the format we're using:
	- [%MainImage%] => 467x312
	- [%Title%] => 40 characters.
	- [%Subtitle%] => 50 characters. On smaller screens, go to 2 lines (empty row otherwise) - cut off after 2 lines
	- [%Location%] => 32 characters
	- [%CalltoAction%] => fixed 'Meer informatie over deze makelaar'
	- %%DEST_URL%% is click through URL

## HTML template to be pasted on DFP creative templates (https://www.google.com/dfp/6835#delivery/ListCreativeTemplates)

	<div class="native-kantoor-ad">
		<figure class="native-kantoor-ad__main-img">
			<a href="%%CLICK_URL_ESC%%%%DEST_URL%%" target="_blank"><img src="[%MainImage%]" alt="[%Title%]"></a>
		</figure>

		<div class="native-kantoor-ad__main-content">
			<h3 class="native-kantoor-ad__title"><a href="%%CLICK_URL_ESC%%%%DEST_URL%%" target="_blank">[%Title%]</a></h3>

			<div class="native-kantoor-ad__description">
				<p class="native-kantoor-ad__subtitle">[%Subtitle%]</p>
				<p class="native-kantoor-ad__location">[%Location%]</p>
			</div>
			<p class="native-kantoor-ad__cta"><a href="%%CLICK_URL_ESC%%%%DEST_URL%%" target="_blank">Meer informatie over deze makelaar</a></p>
	  	</div>
		<span class="native-kantoor-ad__label-adv">Adv</span>
	</div>