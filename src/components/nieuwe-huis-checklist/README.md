# Nieuwe Huis Checklist component

## Functionality

Checklist component on the dashboard of Nieuwe Huis Overzicht.

## Usage

Include the component when needed.
{% include "components/nieuwe-huis-checklist/nieuwe-huis-checklist.html" %}
