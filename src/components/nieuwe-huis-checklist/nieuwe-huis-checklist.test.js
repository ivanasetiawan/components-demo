'use strict';

import NieuweHuisChecklist from './nieuwe-huis-checklist';
import $ from 'jquery';
import sinonTest from 'sinon-test';
sinon.test = sinonTest.configureTest(sinon);

describe('nieuwe-huis-checklist component', function () {
    let component;
    let fixture;

    it('should register change event', sinon.test(function () {
        fixture = $(`
        <form data-nieuwe-huis-checklist action="/dummy" method="POST">
            <div class="notification-success is-removed"></div>
            <div class="notification-error is-removed"></div>
            <label>
                <input type="checkbox" data-nieuwe-huis-checklist-checkbox>
            </label>
        </form>
        `);
        // Arrange
        const registerChangeOnCheckboxSpy = sinon.spy(NieuweHuisChecklist.prototype, 'registerChangeOnCheckbox');
        // Act
        component = new NieuweHuisChecklist(fixture);
        // Assert
        assert(registerChangeOnCheckboxSpy.calledOnce);

        const ajaxStub = sinon.stub($, 'post').yieldsTo('success');
    }));

    it('should show error feedback', function () {
        fixture = $(`
        <form data-nieuwe-huis-checklist action="/dummy" method="POST">
            <label>
                <input type="checkbox" data-nieuwe-huis-checklist-checkbox>
            </label>
        </form>
        `);
        // Arrange
        const $checkboxLabel = $(fixture).find('label');
        component = new NieuweHuisChecklist(fixture);
        // Act
        component.onSuccess($checkboxLabel);
        // Assert
        expect($checkboxLabel.attr('class')).to.have.string('success');
        // Act
        component.onError($checkboxLabel);
        // Assert
        expect($checkboxLabel.attr('class')).to.have.string('error');
    });
});