// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

import $ from 'jquery';
import debounce from 'lodash/debounce';
export default NieuweHuisChecklist;

const COMPONENT_SELECTOR = '[data-nieuwe-huis-checklist]';
const CHECKLIST_SELECTOR = '[data-nieuwe-huis-checklist-checkbox]';
const DEBOUNCE_DELAY = 100;

function NieuweHuisChecklist(element) {
    const component = this;
    component.$element = $(element);
    component.checkbox = component.$element.find(CHECKLIST_SELECTOR);
    component.registerChangeOnCheckbox();
}

NieuweHuisChecklist.prototype.registerChangeOnCheckbox = function() {
    const component = this;
    const url = component.$element.attr('action');
    const method = component.$element.attr('method');

    component.checkbox.on('change', debounce(function() {
        const $this = $(this);
        const $checkboxLabel = $this.parents('label');
        var data = {
            taskId: $this.attr('data-task-id'),
            value: $this.is(':checked')
        };
        return $.ajax({
            url: url,
            type: method,
            data: data,
            dataType: 'json',
            success: function() {
                component.onSuccess($checkboxLabel);
            },
            error: function() {
                component.onError($checkboxLabel);
            }
        });
    }, DEBOUNCE_DELAY));
};

NieuweHuisChecklist.prototype.onSuccess = function($checkboxLabel) {
    $checkboxLabel.removeClass('error').addClass('success');
};

NieuweHuisChecklist.prototype.onError = function($checkboxLabel) {
    $checkboxLabel.removeClass('success').addClass('error');
};

// turn all elements with the default selector into components
$(COMPONENT_SELECTOR).each(function(index, element) {
    return new NieuweHuisChecklist(element);
});
