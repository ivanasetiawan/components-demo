# App-icons

## Functionality

Load optimised icons based on browser support. Modern browsers use vector (SVG) icons where older browsers use bitmap (PNG) icons.
On the first visit the support is detected and the correct icons are loaded. A cookie is set so on subsequent visits the
appropriate icons are directly included.

## Usage

### Icon loader

* Include `app-icons-loader.html` in the document's `<head>`.
* Configure the paths to the icon stylesheets (.css files).
* When no cookie, include the icon-loader.
* When `icons-loaded` cookie, use the cookie value as href in a `<link rel="stylesheet" href="{{cookieValue}}">` tag.

## Workflow

A typical workflow to add a new icon would be:

* Add an SVG icon file to components/app-icons/icons
* `npm run install-grunticon` (this will install all the grunt dependencies that you need locally. Only needed once.)
* `npm run icons`
* Use the icon as you wish

## Authoring icons

See [Authoring assets > Icons](https://git.{client}.nl/projects/WEBAPPLICATIONS/repos/{client}/browse/{client}.Static/docs/authoring-assets.md#icons) section.

## To do

* Set `icons-loaded` cookie to value of path used by loader so on next visit we can use this value without the need for the loader.

## Issue

The current version of grunticon uses fg-loadcss v0.1.6 which does not yet support relative paths to parent directories.
We need that for the paths to our icon stylesheets. In fg-loadcss v0.1.7 this issue is patched.
When creating a new `grunticon.loader.js` make sure you manually update fg-loadcss within `/node_modules/grunt-grunticon/node_modules/grunticon-lib/node_modules/fg-loadcss`.

See also: https://github.com/filamentgroup/grunticon-lib/issues/5
