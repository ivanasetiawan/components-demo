# App Spinner component

## Functionality

Spinnin', Spinnin', Spinnin'

Sometimes it takes some time to load things. To give the user feedback while we're busy, we'll show a smoothly animated spinner. 

Example use case: use the spinner while loading the plattegrond.

Important: 

- You're not allowed to use the spinner whenever you want, only when it's necessary for loading something that takes time. 
- The spinner will show up 1,5 seconds after show() is called, to prevent flashing spinners. 
- Browsers that do not support CSS keyframe animations get a .gif fallback spinner instead.

Interesting read: http://www.nngroup.com/articles/progress-indicators/

## Guide usage
In the guide, always include the spinner in your component template via the spinner's macro.

The `spinnerName` argument should match whatever is used to select it in the parent component. 'plattegrond' becomes `data-plattegrond-spinner`

Do not supply a `typeOverride` argument in other components.

Do not supply a `visibilityOverride` argument in other components.

## JS usage
This component will not self-initiate (why would we want a spinner to do that). Instead, always initiate it from it's parent component.

In your component: 
```
var AppSpinner = require ('../app-spinner/app-spinner)
component.spinner = new AppSpinner($(YOUR_SPINNER_SELECTOR).get(0));
...
//loadSomethingThatTakesTime
component.spinner.show();
...
//doneLoading
component.spinner.hide();
```
## Razor usage 
Do not include the `keyframe-animation-supported` and `is-visible` classes in Razor, even though they're used in the guide HTML

A data-attribute is needed that matches its parent component's selector, e.g. `data-plattegrond-spinner` if `'[data-plattegrond-spinner]'` is used in the parent component's JavaScript. See *guide usage* above.
