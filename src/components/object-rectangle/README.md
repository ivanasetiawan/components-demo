# Object Rectangle component

## Functionality

The Object Rectangle component targets all ad slot(s) on a view. The component initiated itself once and depending on elements with ads being present will serve advertisments from DFP.

Object rectangle is a lazy ad component.

## Usage
Add `data-advertisement` attribute on the element to trigger the functionality.

Necessary data attributes are:
- ID (Unique ID - automatically generated)
- `data-advertisement-lok` (Lok ID - check doc, if unsure - double check with the traffic team)
- `data-advertisement-adunit-path` (Local test use: "/6835/{client}.koop/resultaatlijst")
- `data-advertisement-size-mapping` (Responsive ad unit)
- `data-advertisement-size-default` (Default slot size)
- `data-advertisement-categories-url` This attribute contains the api end point needed to call to get the current targeting categories. This url is generated in the backend(encrypted).
- `data-advertisement-lazy data-advertisement-lazy-onscroll` This attributes refers to one of advertisements' feature behaviours of lazy ad onscroll.
