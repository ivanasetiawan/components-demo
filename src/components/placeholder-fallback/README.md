# Placeholder Fallback component

## Functionality

The design requires placeholders inside form inputs to be used as labels.
This is a trick which only works in modern browsers which support placeholders.
For browsers that do not support the `placeholder` attribute, 
you can use the `placeholder-fallback` component to show an alternative.

## Usage

You can add the placeholder fallback behaviour by adding the `data-placeholder-fallback` attribute to the label.
Add the class name 'is-hidden' to the label to visually hide the label in supporting browsers.

	<label for="emailAddress" data-placeholder-fallback>E-mailadres</label>
