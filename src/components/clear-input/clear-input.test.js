'use strict';
import $ from 'jquery';
import ClearInput from './clear-input';

const fixture = getFixture();
const $fixture = $(fixture);
let instance;

describe('clear-input component', function() {
    const $input = $fixture.find('[data-dirty-input]');
    const $triggerElement = $fixture.find('button');

    beforeEach('create instance', function() {
        instance = new ClearInput(fixture);
    });

    it('should clear input after button is clicked', function() {
        expect($input.prop('value')).to.contain('inputValue');

        instance.clear();

        expect($input.prop('value')).to.be.empty;
    });

    it('remove is-dirty class when input value is empty', function() {
        const handleEmptyInputStub = sinon.stub(ClearInput.prototype, 'handleEmptyInput');

        instance.toggleInput();

        assert.isTrue(handleEmptyInputStub.called);
        handleEmptyInputStub.restore();
    });

    it('add is-dirty class when input value is non-empty', function() {
        const handleInputStub = sinon.stub(ClearInput.prototype, 'handleInput');
        $input.val('some text');

        instance.toggleInput();

        assert.isTrue(handleInputStub.called);
        handleInputStub.restore();
    });

    it('Can add is-dirty class', function() {
        instance.handleInput();

        expect($triggerElement.hasClass('is-dirty')).to.be.true;
    });

    it('Can remove is-dirty class', function() {
        instance.handleEmptyInput();

        expect($triggerElement.hasClass('is-dirty')).to.be.false;
    });
});

function getFixture() {
    const wrapper = document.createElement('div');
    wrapper.setAttribute('data-clear-input', 'inputName');

    const element = document.createElement('button');
    element.setAttribute('data-clear-input-handle');

    const input = document.createElement('input');
    input.setAttribute('data-dirty-input', 'inputName');
    input.setAttribute('value', 'inputValue');

    wrapper.appendChild(input);
    wrapper.appendChild(element);
    return wrapper;
}
