# Clear Input component

## Functionality

Shows X on the right side a input text field when there is text.
Clears it by pressing the cross icon.

## Usage

Create a wrapper element and add `data-clear-input` attribute. (or add to an existing one)
Add an input with matching value for `data-dirty-input`.

    <div class="clear-input-wrapper" data-clear-input="search-header">
        <input data-dirty-input="search-header">
        <button type="button" class="clear-input button-clean icon-delete-greyLight"></button>
    </div>
