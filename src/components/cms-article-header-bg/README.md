# Cms Article Header component

## Functionality

To show the Header background on CMS pages. The header has a gracefully fade out when an user scrolling down to read the article. 

## Usage

Include in Umbraco razor template.