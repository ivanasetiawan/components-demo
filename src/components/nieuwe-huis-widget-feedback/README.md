# Nieuwe Huis Widget Feedback component

## Functionality

Widget feedback component on the dashboard of Nieuwe Huis Overzicht. The form is embedded from Wufoo.

## Usage

Include the CSS on the umbraco 4. As for now the path is https://cms.{client}.nl/media/10776208/{client}-wufoo-feedback.css
(Note: The CSS should not be part of F.Static)