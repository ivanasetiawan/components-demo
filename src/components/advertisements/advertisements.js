'use strict';

import $ from 'jquery';

require('./dfp-events');

const Advertisements = {};

export default Advertisements;

// component configuration
const COMPONENT_ATTR = 'data-advertisement';
const COMPONENT_SELECTOR = '[' + COMPONENT_ATTR + ']';

const LAZY_INIT_SELECTOR = '[data-advertisement-lazy]';
const REFRESH_ONSCROLL_SELECTOR = '[data-advertisement-refresh-on-scroll]';
const SIZE_MAPPING_ATTR = 'data-advertisement-size-mapping';
const SIZE_DEFAULT_ATTR = 'data-advertisement-size-default';
const ADUNIT_PATH_ATTR = 'data-advertisement-adunit-path';
const CATEGORIES_URL_ATTR = 'data-advertisement-categories-url';
const SEARCHRESULT_SELECTOR = '[data-advertisement-searchresults]';
const SEARCH_AD_SELECTOR = '.search-ad';
const SEARCH_AD_LOADED_CLASS = 'search-ad--loaded';

// consts for iOnScreen
const BP_MEDIUM = 750;
const WINDOW_WIDTH = $(window).width();
const NO_DESKTOP = WINDOW_WIDTH < BP_MEDIUM - 1;
const LAZY_THRESHOLD = 'data-advertisement-lazy-threshold';
const LAZY_THRESHOLD_DEFAULT = 0;

Advertisements.instances = [];
Advertisements.cachedCategoriesByUrl = {};

/**
 * Default lazy ad initializer behaviour.
 * @param {array} elements - list of lazy ads.
 * @param {string} classFlag - classname of each ad on the first load.
 */
Advertisements.initialize = function($elements, classFlag) {
    const component = this;
    window.googletag.cmd.push(function() {
        [].forEach.call($elements, (element) => {
            const config = Advertisements.getAdConfig(element);
            if (config !== false) {
                $(element).addClass('dfp-display-triggered');
                component.instances.push(element);
                element.id = config.id;
                component.createSlot(config);
                component.registerOnSlotRenderCallback(classFlag);
            }
        });
        window.googletag.pubads().collapseEmptyDivs();
        window.googletag.pubads().disableInitialLoad();
        window.googletag.enableServices();
    });
};

Advertisements.getAdConfig = function(element) {
    return {
        id: element.id || 'ad-' + Advertisements.instances.length,
        adunitpath: element.getAttribute(ADUNIT_PATH_ATTR),
        sizemapping: JSON.parse(element.getAttribute(SIZE_MAPPING_ATTR)),
        sizedefault: JSON.parse(element.getAttribute(SIZE_DEFAULT_ATTR)),
        categoriesUrl: element.getAttribute(CATEGORIES_URL_ATTR)
    };
};

Advertisements.createSlot = function(config) {
    const component = this;
    const cachedCategories = this.cachedCategoriesByUrl[config.categoriesUrl];

    if (cachedCategories) {
        component.pushAdSlot(config, cachedCategories);
    } else {
        $.get({
            url: config.categoriesUrl,
            success:
                function(categories) {
                    component.pushAdSlot(config, categories);
                },
            error:
                function() {
                    component.pushAdSlot(config, {});
                }
        });
    }
};

Advertisements.pushAdSlot = function(config, categories) {
    const tag = window.googletag
        .defineSlot(config.adunitpath, config.sizedefault, config.id)
        .defineSizeMapping(config.sizemapping)
        .setCollapseEmptyDiv(true, true)
        .addService(window.googletag.pubads());

    for (let key in categories) {
        if (categories.hasOwnProperty(key)) {
            tag.setTargeting(key, categories[key]);
        }
    }

    window.googletag.display(config.id);
    window.googletag.pubads().refresh([tag]);
};

/**
 * Add a class to rendered ads - remove as soon as 100% released.
 * @param classname {string} - class name to add
 */
Advertisements.registerOnSlotRenderCallback = function(classname) {
    const isClassname = classname || '';

    window.googletag.cmd.push(function() {
        window.googletag.on('gpt-slot_rendered', (e, level, message, service, slot) => {
            const slotId = slot.getSlotId();
            const $slot = $('#' + slotId.getDomId());
            const $injectedFromGoogle = $slot.find('div[id^=google_ads_iframe]');

            // DFP adds two iframes, one for calling scripts and one for displaying the ad. we want the one that is not hidden
            Advertisements.addClassNameIfCreativeLoaded($slot, isClassname, $injectedFromGoogle.length > 0);
        });
    });
};

Advertisements.addClassNameIfCreativeLoaded = function($slot, classname, isAdInjectedFromGoogle) {
    if (isAdInjectedFromGoogle) {
        $slot.addClass('advertisement dfp-callback-successful slot-' + classname);
        // Needed to avoid jumping effect on search results
        if ($(SEARCH_AD_SELECTOR).length) { $slot.parents(SEARCH_AD_SELECTOR).addClass(SEARCH_AD_LOADED_CLASS); }
        $slot
            .find('iframe')
            .contents().find('body')
            .addClass('body-' + classname);
    }
};

/**
 * Detect if an element is on the screen.
 * @param {object} element
 * @returns {boolean} if object is on screen
 */
Advertisements.isOnScreen = function(element) {
    const reservedDistance = Advertisements.calculateReservedDistance(element);
    const rect = element.getBoundingClientRect();

    return ($(element).is(':visible') &&
            rect.top >= 0 &&
            rect.left >= 0 &&
            rect.bottom <= ($(window).height() + reservedDistance) &&
            rect.right <= (WINDOW_WIDTH));
};

/**
 * Figure out the layout on search, is there any element positioned absolute?
 * On search page, the element has ~100px difference
 * @param {object} element
 * @returns {boolean} if object is on screen
 */
Advertisements.calculateReservedDistance = function(element) {
    if ($(element).attr('data-advertisement-searchresults') !== undefined) {
        return 200;
    }
    return parseInt(element.getAttribute(LAZY_THRESHOLD) || LAZY_THRESHOLD_DEFAULT, 10);
};

/**
 * Check if there's any advertisement on the page
 */
if ($(COMPONENT_SELECTOR).length && WINDOW_WIDTH > BP_MEDIUM) {
    Advertisements.initialize(
        $(COMPONENT_SELECTOR + ':not(' + LAZY_INIT_SELECTOR + ')' + ':not(' + REFRESH_ONSCROLL_SELECTOR + ')' + ':not(' + SEARCHRESULT_SELECTOR + ')'), 'slot-ad-desktop'
    );
} else if ($(COMPONENT_SELECTOR).length && NO_DESKTOP) {
    Advertisements.initialize(
        $(COMPONENT_SELECTOR + ':not(' + LAZY_INIT_SELECTOR + ')' + ':not(' + REFRESH_ONSCROLL_SELECTOR + ')' + ':not(' + SEARCHRESULT_SELECTOR + ')'), 'slot-ad-non-desktop'
    );
}