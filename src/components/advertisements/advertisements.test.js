// Unit test for autocomplete component
// Mocha, Chai & Sinon are always included
// https://www.npmjs.com/package/karma-sinon
'use strict';

import $ from 'jquery';
import Advertisements from './advertisements';
import sinonTest from 'sinon-test';
sinon.test = sinonTest.configureTest(sinon);

let instance;
const fixture = getFixture();
const config = Advertisements.getAdConfig(fixture);

// only test if we correctly read configuration from data-attributes
// don't test other methods because they only test googletag api
describe('advertisements component', function() {

    it('should get `id` value from element', function() {
        (config.id).should.be.a('string').and.contain('ad-');
    });

    it('should get `adunit path` value from element', function() {
        (config.adunitpath).should.be.a('string').and.contain('/6835/');
    });

    it('should get `size mapping` length from element', function() {
        (config.sizemapping).should.be.an('array').with.lengthOf(3);
    });

    it('should get `size default` value from element', function() {
        (config.sizedefault).should.be.an('array').with.lengthOf(2);
    });

    it('should get `categories url` value from element', function() {
        (config.categoriesUrl).should.be.a('string')
            .to.contain('api/targeting')
            .and.contain('?payload=');
    });

});

describe('advertisements component', function() {
    const renderedClass = 'rendered';

    it('should be on the screen', function() {
        $('body').append(fixture);
        const isOnScreen = Advertisements.isOnScreen(fixture);
        expect(isOnScreen).to.be.true;
    });

    it('should push empty targeting-options when categories can not be fetched from url', sinon.test(function() {

        // arrange
        const pushAdSlotStub = sinon.stub(Advertisements, 'pushAdSlot');
        const ajaxStub = sinon.stub($, 'get').yieldsTo('error');

        // act
        Advertisements.createSlot(config);

        // assert
        const pushedCategories = pushAdSlotStub.firstCall.args[1];
        expect(pushedCategories).to.be.an('object').that.is.empty;

        // restore
        pushAdSlotStub.restore();
        ajaxStub.restore();

    }));

    it('should contain dfp-callback-sucessful class when dfp call is succesful', function() {
        let adSlot = getFixture();
        const expectedClass = 'advertisement dfp-callback-successful slot-' + renderedClass;

        Advertisements.addClassNameIfCreativeLoaded($(adSlot), renderedClass, true);
        expect(adSlot.className).contains(expectedClass);
    });

    it('should NOT contain dfp-callback-sucessful class when dfp call is NOT succesful', function() {
        let adSlot = getFixture();

        Advertisements.addClassNameIfCreativeLoaded($(adSlot), renderedClass, false);
        expect(adSlot.className).not.contains(renderedClass);
    });

});

function getFixture() {
    //set element (wrapper of the component)
    const element = document.createElement('aside');
    element.setAttribute('data-advertisement');
    element.setAttribute('data-advertisement-user-id', '31cce829-eacc-4309-b946-7b90e2a228fd');
    element.setAttribute('data-advertisement-adunit-path', '/6835/{client}/resultaatlijst');
    element.setAttribute('data-advertisement-size-mapping', '[[[750, 0],[[983, 152],[728, 90],"fluid"]],[[320, 0],[[320, 50],[320, 100],[983, 152],[300, 250],[320, 240],"fluid"]],[[0, 0],[]]]');
    element.setAttribute('data-advertisement-size-default', '[ 728, 90 ]');
    element.setAttribute('data-advertisement-categories-url', 'api/targeting/?payload=125');
    element.setAttribute('class', 'advertisement');
    element.setAttribute('data-advertisement-lazy-threshold', '200');
    element.setAttribute('data-advertisement-has-user-accepted-cookies', 'True');
    return element;

}
