# Advertisements component

## Functionality

The advertisements component targets all ad slot(s) on a view. The component initiated itself once and depending on elements with ads being present will serve advertisments from DFP.

If there are no ads on the page, the component will do nothing.

It is responsive, meaning that the component can serve multiple sizes when defined and only shows on screen > 768px. (Ad(script) must not be injected unless the element is visible)

## Usage

Add `data-advertisement` attribute on the element to trigger the functionality.

Necessary data attributes are:
- ID (Unique ID - automatically generated)
- `data-advertisement-lok` (Lok ID - check doc, if unsure - double check with the traffic team)
- `data-advertisement-adunit-path` (Local test use: "/6835/{client}/resultaatlijst")
- `data-advertisement-size-mapping` (Responsive ad unit)
- `data-advertisement-size-default` (Default slot size)
- `data-advertisement-categories-url` This attribute contains the api end point needed to call to get the current targeting categories. This url is generated in the backend(encrypted).
- `data-advertisement-refresh-on-scroll` This attributes refers to one of advertisements' feature behaviours, that is: Only refresh the ad slot when the user has scrolled almost at the bottom of the page and goes up.
- `data-advertisement-lazy` This attribute is optional.
lazyInit="data-advertisement-lazy" means the ads need to be explicitly initiated (e.g. only if the view is opened for the first time, on scroll event, etc)
