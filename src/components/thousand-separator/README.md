# Thousand Separator component

## Functionality

Will set the number 1000 to 1.000 and 1000000 to 1.000.000 or the other way around. 
From a numbered string (1.000) to an integer (1000).

## Usage

### ThousandSeparator.format(1000, '.')
Will format the integer to a string that has the thousand separator if needed

### ThousandSeparator.parse('1.000')
Will remove the separator and return an integer
