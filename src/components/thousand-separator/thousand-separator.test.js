// Unit test for thousand-separator component
// Mocha and Chai are always included
'use strict';

import ThousandSeparator from './thousand-separator';

describe('Test if a number stays the same.', function() {
    it('Should return the same number without a separator', function() {
        ThousandSeparator.format(99, '.').should.equal('99');
    });
});

describe('Test if a number get the Dutch separator.', function() {
    it('Should return a number with a thousand separator', function() {
        ThousandSeparator.format(1000, '.').should.equal('1.000');
    });
});

describe('Test if a million value is easy to read.', function() {
    it('Should return a number with a thousand separator', function() {
        ThousandSeparator.format(1000000, '.').should.equal('1.000.000');
    });
});

describe('Test if a number get the English separator.', function() {
    it('Should return a number with a thousand separator', function() {
        ThousandSeparator.format(1000, ',').should.equal('1,000');
    });
});

describe('Test if a numbered string can brought back to a integer', function() {
    it('Should remove the separator and return a integer', function() {
        ThousandSeparator.parse('1.000').should.equal(1000);
    });

    it('Should remove the separator and return a integer', function() {
        ThousandSeparator.parse('1,000').should.equal(1000);
    });

    it('Should remove the separator and return a integer', function() {
        ThousandSeparator.parse('1.000.000').should.equal(1000000);
    });
});
