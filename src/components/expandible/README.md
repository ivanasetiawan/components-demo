# Expandible component

## Functionality

Expand (show) or collapse (hide) parts of the UI when a user triggers a handle.
Those parts can initially either be visible or hidden, depending on what's desirable.
The changing of the state can be instant or animated, depending on what's desirable.

## Usage

Add `data-expandible` to the element wrapping the component to enhance it.
When the component is enhanced, an `is-expandible` class is added to the element.
When the state of the component changes (expands/collapses) an `is-expanded` class is toggled (added/removed respectively).
If you want the component to be expanded initially after it's been enhanced, simply add `is-expanded` in the initial HTML.

You can turn any element inside the component into a handle by adding the `data-expandible-handle` attribute to it.
Clicking on the handle will cause the state to toggle.
If for some reason a handle needs to live outside the component's root element, you can link an external handle by giving
it the same unique key as the root element. Eg. bind `data-expandible-handle="uniqueId"` to `data-expandible="uniqueId`.

You can use the class changes on the component element to show / hide (part) of the components content (including the handles)
and use animations / transitions wherever you like.

If you want the component to close on clicking anywhere outside it, add `data-expandible-on-blur` to the component and assign it the value `close`.