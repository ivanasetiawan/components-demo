// Unit test for expandible component
// Mocha and Chai are always included

'use strict';

import Expandible from './expandible';

let instance;
const fixture = getFixture();

describe('expandible component', function() {

    beforeEach('create instance', function() {
        instance = new Expandible(fixture);
    });

    it('should add `.is-expandible` when enhanced', function() {
        instance.$element.hasClass('is-expandible').should.equal(true);
    });

    it('should add `.is-expanded` on `toggleExpand`', function() {
        instance.toggleExpand();
        instance.$element.hasClass('is-expanded').should.equal(true);
    });

    it('should remove `.is-expanded` on second `toggleExpand`', function() {
        instance.toggleExpand();
        instance.$element.hasClass('is-expanded').should.equal(false);
    });

    it('should call observers', function() {
        let numCalls = 0;
        let value = undefined;

        //Define callback
        instance.onChange(function (isExpanded) {
            numCalls++;
            value = isExpanded;
        });

        //Call 1
        instance.toggleExpand(true);
        assert.equal(numCalls, 1);
        assert.equal(value, true);

        //Call 2
        instance.toggleExpand();
        assert.equal(numCalls, 2);
        assert.equal(value, false);

        //Call 3
        instance.toggleExpand(false);
        assert.equal(numCalls, 3);
        assert.equal(value, false);
    });
});

describe('expandible component', function() {

    let $handle;

    beforeEach('create instance', function() {
        instance = new Expandible(fixture);
        $handle = instance.$element.find('[data-expandible-handle]').first();
    });

    it('should add `.is-expanded` when clicking on a handle', function() {
        $handle.trigger('click');
        instance.$element.hasClass('is-expanded').should.equal(true);
    });

    it('should remove `.is-expanded` when clicking on handle again', function() {
        $handle.trigger('click');
        instance.$element.hasClass('is-expanded').should.equal(false);
    });
});

function getFixture() {
    let element = document.createElement('div');
    let handle = document.createElement('button');
    handle.setAttribute('data-expandible-handle', '');
    element.appendChild(handle);
    return element;
}
