# Login Dialog component

## Functionality

Handles the async login, opening the dialog if required.

## Usage

This component is used for all actions for which a user needs to be logged in, e.g.

Usage loginAsync().then(() => doAwesomeThings());

OR loginAsync(() => doAwesomeThings());

##Requirements

There needs to be a dialog component with name 'user-form'

    {{ dialog('user-form') }}
