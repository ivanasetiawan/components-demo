# Performance Indicator component

## Functionality

Gives an indication of how good the house is performing compared to similar ones.

## Usage

All variable content in the html has been parametrized. The possible values are:
* type = "excellent" | "good" | "average" | "improvable"
* iconClass = TODO Create and import performance indicator icons!
* sentenceText= translated sentence | "Ten op zichte van concurrende huizen presteert jouw huis:",
* typeText= translated performance | "gemiddeld",
* linkUrl="/path/to/suggestions/",
* linkText=translated link text | "Bekijk suggesties voor verveteringen."

