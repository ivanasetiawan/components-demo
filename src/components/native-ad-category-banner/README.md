# Native Category Banner component

## Functionality

To show how the category banner native ad look (example: ads on object details).

## Usage

Native format and styles can be found on DFP. (https://www.google.com/dfp/6835#delivery/ListNativeStyles)

For this ad, the format we're using:
	- [%Logo%] => ideally height 32px & max-width 140px
	- [%MainText%] => 30 characters.
	- [%ActionText%] => ~ 10 characters
	- [%BrandingColor%] => Syntax is `#B2BB1C`
	- %%DEST_URL%% is click through URL
	- [%1x1pixel%] is tracking pixel

## HTML template to be pasted on DFP creative templates (https://www.google.com/dfp/6835#delivery/ListCreativeTemplates)

	<a href="%%CLICK_URL_ESC%%%%DEST_URL%%" target="_blank" class="native-category-banner">
		<figure class="native-category-banner__img">
			<div class="native-category-banner__img-wrapper">
				<img title="[%AltTag%]" src="[%Logo%]" class="logo" alt="[%AltTag%]">
			</div>
		</figure>
		<div class="native-category-banner__content">
			<span class="native-category-banner__main-text">
				[%MainText%]
			</span>
			<span class="native-category-banner__action-text">
				<span>[%ActionText%]</span>
			</span>
		</div>
	</a>
	<div class="native-category-banner-colorhex" style="display: none;">[%BrandingColor%]</div>
	<img src="[%1x1pixel%]" width="1" height="1" border="0" style="display: none;">

	// JS to give the ad background based on their branding color
	<script type="text/javascript">
	function hexToRgb(hex) {
		var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
		hex = hex.replace(shorthandRegex, function(m, r, g, b) {
						return r + r + g + g + b + b;
		});

		var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
		return result ? {
						r: parseInt(result[1], 16),
						g: parseInt(result[2], 16),
						b: parseInt(result[3], 16)
		} : null;
	}

	var colorHex = document.querySelector(".native-category-banner-colorhex").innerHTML;
	var catBanner = document.querySelector(".native-category-banner");
	var r = hexToRgb(colorHex).r.toString();
	var g = hexToRgb(colorHex).g.toString();
	var b = hexToRgb(colorHex).b.toString();

	catBanner.style.backgroundColor = "rgba("+ r +","+ g +","+ b + "," + "0.1"+")";
	</script>