# Native Sponsorblock component

## Functionality

To show how the native ad look (example: Sponsorblock on object detail).

There are two variants possible for this sponsor block: one with and one without the "Geld lenen kost geld" disclaimer that is required by the AFM for loans.

## Usage

Native format and styles can be found on DFP. (https://www.google.com/dfp/6835#delivery/ListNativeStyles)

For this ad, the format we're using:
	- [%brandColor%] => will be used as border & title color
	- [%Title%] => 26 characters.
	- [%Subtitle%] => 36 characters.
	- [%CalltoAction%] => 10 characters.
	- [%CompanyLogo%]
	- %%DEST_URL%% is click through URL
	- [%1x1pixel%] is a tracking pixel

## HTML

Template to be pasted on DFP creative templates (https://www.google.com/dfp/6835#delivery/ListCreativeTemplates)

Standard native sponsor block:

	<a class="native-sponsorblock" style="border-color: #[%BrandColor%];" href="%%CLICK_URL_ESC%%%%DEST_URL%%" target="_blank">
		<img src="[%1x1pixel%]" width="1" height="1" border="0" style="display: none;">
	    <h2 class="native-sponsorblock__title" style="color: #[%BrandColor%];">[%Title%]</h2>
	    <p>
	    	[%Subtitle%]
	    	<span class="native-sponsorblock__cta">[%CalltoAction%]</span>
	    </p>

	    <div class="native-sponsorblock__logo">
	    	<img src="[%CompanyLogo%]" alt="[%Title%]">
	    </div>
	</a>

Native sponsor block including AFM warning "Geld Lenen Kost geld":

    <a class="native-sponsorblock" style="border-color: #[%BrandColor%];" href="%%CLICK_URL_ESC%%%%DEST_URL%%" target="_blank">
        <img src="[%1x1pixel%]" width="1" height="1" border="0" style="display: none;">
        <h2 class="native-sponsorblock__title" style="color: #[%BrandColor%];">[%Title%]</h2>
        <p>
            [%Subtitle%]
            <span class="native-sponsorblock__cta">[%CalltoAction%]</span>
            <br>
            <img src="http://assets.fstatic.nl/master_XXX/assets/components/native-sponsorblock/images/afm-msg.jpg" alt="[%Title%]" class="native-sponsorblock__afm-warning">
        </p>

        <div class="native-sponsorblock__logo">
            <img src="[%CompanyLogo%]" alt="[%Title%]">
        </div>
    </a>

Native sponsor block - Left aligned:

    <a class="native-sponsorblock" style="border-color: #[%BrandColor%];" href="%%CLICK_URL_ESC%%%%DEST_URL%%" target="_blank">
        <img src="[%1x1pixel%]" width="1" height="1" border="0" style="display: none;">
        <div class="native-sponsorblock__logo native-sponsorblock__logo--left">
            <img src="[%CompanyLogo%]" alt="[%Title%]">
        </div>
        <h2 class="native-sponsorblock__title" style="color: #[%BrandColor%];">[%Title%]</h2>
        <p>
            [%Subtitle%]
            <span class="native-sponsorblock__cta">[%CalltoAction%]</span>
        </p>
    </a>