# Maandlasten Indicator component

## Functionality

To provide users an approximate monthly mortgage for the current house they're interested in, sponsored by clients. 

## Usage

Add `data-maandlasten-indicator` attribute on the element to trigger the functionality.

Necessary data attributes are:
- `data-maandlasten-indicator-small` (URL that points to mobile version of the iframe)
- `data-maandlasten-indicator-large` (URL that points to desktop version of the iframe)
