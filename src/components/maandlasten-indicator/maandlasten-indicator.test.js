// Unit test for maandlasten-indicator component
// Mocha and Chai are always included
'use strict';

var MaandlastenIndicator = require('./maandlasten-indicator');

describe('maandlasten-indicator component', function() {
    var instance;

    beforeEach('create instance', function() {
        instance = new MaandlastenIndicator(getFixture());
    });

    it('should have `injectIframe` method', function() {
        (typeof instance.injectIframe).should.equal('function');
    });

    it('should have a value for small, that contains url', function() {
        instance.$element.attr('data-monthly-indicator-small').should.contain('https://www');
    });

    it('should have a value for large, that contains url', function() {
        instance.$element.attr('data-monthly-indicator-large').should.contain('https://www');
    });
});

function getFixture() {

    //set element (wrapper of the component)
    var element = document.createElement('div');
    element.setAttribute('data-maandlasten-indicator');
    element.setAttribute('data-monthly-indicator-small', 'https://www.hypotheker.nl/pages/{client}mlimobile/?vraagprijs=179000');
    element.setAttribute('data-monthly-indicator-large', 'https://www.hypotheker.nl/pages/{client}MLI/html/index.html?vraagprijs=179000&amp;straatnaam=Schonauwen&amp;huisnummer=11&amp;huisnummertoevoeging=&amp;postcode=9301SM&amp;woonplaats=Roden&amp;soortbouw=bestaand&amp;proxyurl=http%3a%2f%2fold.{client}.nl%2fPartner%2fpartnerproxy.html');

    return element;

}
