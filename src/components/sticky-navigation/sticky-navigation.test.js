'use strict';

import $ from 'jquery';
import StickyNavigation from './sticky-navigation';
import sinonTest from 'sinon-test';
sinon.test = sinonTest.configureTest(sinon);

const fixture = getFixture();
describe('sticky-navigation component', function() {
    context('initial state', function() {
        let instance;

        before('create instance', function() {
            instance = new StickyNavigation(fixture);
        });

        it('needs reserve the sticky layout on initial state', function() {
           expect(instance.setStickyWidth).to.be.a('function');
        });

        it('needs to listen to a scroll event', function() {
           expect(instance.registerOnScroll).to.be.a('function');
        });

        it('needs to listen to a resize event', function() {
           expect(instance.registeronResize).to.be.a('function');
        });
    })

    context('when a user resize the window', function() {
        let instance;

        before('create instance', function() {
            instance = new StickyNavigation(fixture);
            // Set the layout
            instance.$element.width(700);
            instance.$element.find('[data-sticky-navigation-links]').height(54);
            instance.setStickyWidth();
        });

        it('should recalculate the layout of the sticky element', sinon.test(function() {
            const $stickyNavEl = $(instance.$element);
            expect($stickyNavEl.height()).to.not.be.null;
            expect($stickyNavEl.height()).to.equal(54);
            expect($stickyNavEl.find('[data-sticky-navigation-links]').width()).to.not.be.null;
            expect($stickyNavEl.find('[data-sticky-navigation-links]').width()).to.equal(700);
        }));
    })

    context('when a user scroll the window', function() {
        let instance;

        before('create instance', function() {
            instance = new StickyNavigation(fixture);
        });

        it('should stick the navigation when necessary', sinon.test(function() {
            instance.sticky();
            expect($(instance.$element).find('[data-sticky-navigation-links]').attr('class')).to.equal('sticky-navigation sticky');
        }));

        it('should not stick the navigation when unnecessary', sinon.test(function() {
            instance.noSticky();
            expect($(instance.$element).find('[data-sticky-navigation-links]').attr('class')).to.equal('sticky-navigation');
        }));
    })
});

function getFixture() {
    const element = document.createElement('nav');
    element.setAttribute('data-sticky-navigation');

    const elementWrapper = document.createElement('div');
    elementWrapper.setAttribute('data-sticky-navigation-wrapper');
    elementWrapper.appendChild(element);

    const stickyNav = document.createElement('div');
    stickyNav.setAttribute('data-sticky-navigation-links');
    stickyNav.setAttribute('class', 'sticky-navigation');
    element.appendChild(stickyNav);

    const stickyNavLink = document.createElement('a');
    stickyNavLink.setAttribute('class', 'nieuwe-huis-page-checklist__nav-link');
    stickyNav.appendChild(stickyNavLink);

    return element;
}