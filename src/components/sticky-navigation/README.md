# Sticky Navigation component

## Functionality

A sticky feature for navigation. This sticky navigation component is currently used on Nieuwe huis checklist (http://localhost:3000/views/nieuwe-huis-checklist/nieuwe-huis-checklist.html) together with `Tabs` component.

## Usage

Include the macro when needed by doing:
    // Import the `stickyNavigation` macro
    {% from "components/sticky-navigation/sticky-navigation.html" import stickyNavigation %}

    // Use the `stickyNavigation` macro
    // `linkClassname` is the classname of the link of the navigation
    // `activeClassname` is the active state classname of the link. By default is `active`
    // `dataAttrForHandle` is the optional extra data-attribute
    {% macro stickyNavigation(
        linkClassname="",
        activeClassname="active",
        dataAttrForHandle=""
    ) %}

    Example:
    {{ stickyNavigation(linkClassname="tabs__link", dataAttrForHandle="data-tabs-handle") }}

