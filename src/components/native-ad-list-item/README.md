# Native List Item Ad component

## Functionality

To show how the native ad look (example: list item on search results list).

## Usage

Native format and styles can be found on DFP. (https://www.google.com/dfp/6835#delivery/ListNativeStyles)

For this ad, the format we're using:
	- [%MainImage%]
	- [%Title%]
	- [%Subtitle%]
	- [%TextLine1%]
	- [%TextLine2%]
	- [%CalltoAction%]
	- [%CompanyName%]
	- [%CompanyLogo%]
	- %%DEST_URL%% is click through URL

## HTML template to be pasted on DFP creative templates (https://www.google.com/dfp/6835#delivery/ListCreativeTemplates)

	<div class="native-list-item-ad">
		<figure class="native-list-item-ad__main-img">
			<a href="%%CLICK_URL_ESC%%%%DEST_URL%%" target="_blank"><img src="[%MainImage%]" alt="[%Title%]"></a>
		</figure>

		<div class="native-list-item-ad__main-content">
			<h3 class="native-list-item-ad__title"><a href="%%CLICK_URL_ESC%%%%DEST_URL%%" target="_blank">[%Title%]</a></h3>
			<p>[%Subtitle%]</p>

			<div class="native-list-item-ad__description">
				<p>[%TextLine1%]</p>
				<p>[%TextLine2%] <a href="%%CLICK_URL_ESC%%%%DEST_URL%%" target="_blank">[%CalltoAction%]</a></p>
				<p class="native-list-item-ad__url-style"><a href="%%CLICK_URL_ESC%%[%CompanyUrl%]" target="_blank">[%CompanyName%]</a></p>
				<div class="native-list-item-ad__company-logo">
					<a href="%%CLICK_URL_ESC%%[%CompanyUrl%]" target="_blank"><img src="[%CompanyLogo%]" alt="[%CompanyName%]"></a>
				</div>
			</div>
	  	</div>
		<span class="native-list-item-ad__label-adv">Adv</span>
	</div>

## HTML for AdX (see story https://jira.{client}.nl/browse/xx-xxxx). The HTML is fixed, only styling is possible.
	<div id='adunit' style='overflow: hidden;'>
	  <img src='[%Thirdpartyimpressiontracker%]' style='display:none'>
	  <div class='attribution'>Ad</div>
	  <div class='image'>
	    <a class='image-link' href='%%CLICK_URL_UNESC%%[%Thirdpartyclicktracker%]%%DEST_URL%%' target='_top'><img src='[%Image%]'></a>
	  </div>
	  <div class='title'>
	    <a class='title-link' href='%%CLICK_URL_UNESC%%[%Thirdpartyclicktracker%]%%DEST_URL%%' target='_top'>[%Headline%]</a>
	  </div>
	  <div class='body'>
	    <a class='body-link' href='%%CLICK_URL_UNESC%%[%Thirdpartyclicktracker%]%%DEST_URL%%' target='_top'>[%Body%]</a>
	  </div>
	  <div class='advertiser'> <!-- We hide this on prod -->
	    <a class='advertiser-link' href='%%CLICK_URL_UNESC%%[%Thirdpartyclicktracker%]%%DEST_URL%%' target='_top'>[%Attribution%]</a>
	  </div>
	  <div class='call-to-action'>
	    <a class='call-to-action-link' href='%%CLICK_URL_UNESC%%[%Thirdpartyclicktracker%]%%DEST_URL%%' target='_top'>[%Calltoaction%]</a>
	  </div>
	  <div class='logo'> <!-- We hide this on prod -->
	    <a class='logo-link' href='%%CLICK_URL_UNESC%%[%Thirdpartyclicktracker%]%%DEST_URL%%' target='_top'><img src='[%Secondaryimage%]'></a>
	  </div>
	</div>
