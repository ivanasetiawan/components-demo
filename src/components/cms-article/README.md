# Article Header component

## Functionality

To show the article header on CMS pages. 

## Usage

Include in Umbraco razor template.