# Nieuwe Huis Widget Plattegrond component

## Functionality

Plattegrond Widget component on the dashboard of Nieuwe Huis Overzicht.

## Usage

Include the component when needed.
{% include "components/nieuwe-huis-widget-plattegrond/nieuwe-huis-widget-plattegrond.html" %}