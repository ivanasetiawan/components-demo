import $ from 'jquery';
import TooltipInline from './tooltip-inline';

describe('tooltip component', function () {

    it('tooltip should be visible after clicking the handler', function () {
        const fixture = $(`
            <span class="tooltip--inline" data-tooltip-inline title="YOUR TOOLTIP TEXT HERE">
                <a data-tooltip-handle>{client} Websites</a>
            </span>
        `);

        const instance = new TooltipInline(fixture);
        instance.$handler.click();

        assert.equal(instance.$element.hasClass('is-expanded'), true);
    });
});
