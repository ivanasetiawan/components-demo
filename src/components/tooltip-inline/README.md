# Tooltip Inline component

## Functionality

If the need rises, to create one inline (so you don't need to worry about
tooltips hidding behind browser bounderies), you can use the following:

    <span class="tooltip--inline"
        data-tooltip-inline title="YOUR TOOLTIP TEXT HERE">

        <button data-tooltip-handle>{client} Websites</button>
    </span>
