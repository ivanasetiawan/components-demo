# Goto component

## Functionality

Animates a transition to an id. Example in the goto.html and cadeauflow.

## Usage

Add data attribute data-goto on the link element to initiate the functionality.

## Example

<a href="#id1" data-goto>If you click me I will go to the element with id "id1", eveny without JS</a>

<any id="id1">Here i am</any>
