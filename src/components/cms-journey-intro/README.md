# Cms Journey Intro component

## Functionality

To show the Journey intro text (10 steps) on CMS pages. 

## Usage

Include in Umbraco razor template.