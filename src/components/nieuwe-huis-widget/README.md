# Nieuwe Huis Widget component

## Functionality

Widget component on the dashboard of Nieuwe Huis Overzicht. "Adress change" is the most basic and default layout, therefore used for the basic "nieuwe-huis-widget"

## Usage

Include the component when needed. This component will be used many times on the dashboard. As for now, we use it for "Address wijzeging".
