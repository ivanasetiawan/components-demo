'use strict';

import NieuweHuisWidget from './nieuwe-huis-widget';
import $ from 'jquery';
import sinonTest from 'sinon-test';
sinon.test = sinonTest.configureTest(sinon);

let instance;
const fixture = getFixture();
const $fixture = $(fixture);

describe('nieuwe-huis-widget component', function () {
    beforeEach('create instance', function () {
        instance = new NieuweHuisWidget(fixture);
    });

    it('should have an `scanMq` method', function () {
        expect(instance.scanMq).to.be.a('function');
    });

    it('should initialize the layout properly', function () {
        // Arrange
        const scanMqSpy = sinon.spy(NieuweHuisWidget.prototype, 'scanMq');

        // Act
        new NieuweHuisWidget(fixture);

        // Assert
        assert(scanMqSpy.calledOnce);
    });

    it('should show double columns on large screen', function () {
        // Arrange
        const bricks = $fixture.find('[data-bricks]');
        const bpLarge = 200; // Define the large BP to 200 to simulate large breakpoint
        const handleMqLargeSpy = sinon.spy(NieuweHuisWidget.prototype, 'handleMqLarge');

        // Act
        instance.detectMqVersion(bricks.prevObject[0], bpLarge);

        // Assert
        assert(handleMqLargeSpy.calledOnce);
        expect($($fixture.find('[data-bricks]').prevObject[0]).attr('class')).to.contain('loaded-multiple-column');
    });

    it('should show single columns on medium & small screen', function () {
        // Arrange
        const bricks = $fixture.find('[data-bricks]');
        const bpLarge = 500; // Define the large BP to 500 to simulate non-large breakpoint
        const handleMqDefaultSpy = sinon.spy(NieuweHuisWidget.prototype, 'handleMqDefault');

        // Act
        instance.loadSingleColumnBricks(bricks.prevObject[0], bpLarge);

        // Assert
        assert(handleMqDefaultSpy.calledOnce);
        expect($($fixture.find('[data-bricks]').prevObject[0]).attr('class')).to.contain('loaded-single-column');
    });
});

describe('nieuwe-huis-widget on large breakpoint', function () {
    beforeEach('create instance', function() {
        instance = new NieuweHuisWidget(fixture);
    });

    it('should clean up the layout', function () {
        expect($fixture.find('.column').length).to.equal(0);
    });

    it('should create 2 columns (even and odd) layout', function () {
        const brickOnCols = $fixture.find('[data-bricks]');

        instance.prependBricksOnColumns(brickOnCols.prevObject[0], 'column cols-2');

        expect($fixture.find('.column').length).to.equal(2);
        expect($fixture.find('[data-bricks-even]').length).to.equal(1);
        expect($fixture.find('[data-bricks-odd]').length).to.equal(1);
    });

    it('show proper layout on large breakpoint', sinon.test(function() {
        // Arrange
        const bricks = $fixture.find('[data-bricks]');
        const mq = 'column cols-2';
        const prependBricksOnColumns = sinon.spy(NieuweHuisWidget.prototype, 'prependBricksOnColumns');
        const appendDataOnMultipleColumns = sinon.spy(NieuweHuisWidget.prototype, 'appendDataOnMultipleColumns');

        // Act
        instance.handleMqLarge(bricks, mq);

        // Assert
        assert(prependBricksOnColumns.calledOnce);
        assert(appendDataOnMultipleColumns.calledOnce);
    }));
});

describe('nieuwe-huis-widget on non large breakpoint', function() {
    beforeEach('create instance', function() {
        instance = new NieuweHuisWidget(fixture);
    });

    it('show proper layout on non large breakpoint', sinon.test(function() {
        // Arrange
        const bricks = $fixture.find('[data-bricks]');
        const mq = 'column cols-2';
        const prependBricks = sinon.spy(NieuweHuisWidget.prototype, 'prependBricks');
        const appendDataOnSingleColumn = sinon.spy(NieuweHuisWidget.prototype, 'appendDataOnSingleColumn');

        // Act
        instance.handleMqDefault(bricks, mq);

        // Assert
        assert(prependBricks.calledOnce);
        assert(appendDataOnSingleColumn.calledOnce);
    }));
});

function getFixture() {
    let element = document.createElement('div');
    element.setAttribute('data-bricks', '');
    element.setAttribute('class', 'nieuwe-huis-overzicht');

    let brick1 = document.createElement('div');
    brick1.setAttribute('class', 'nieuwe-huis-overzicht__card');
    brick1.setAttribute('data-brick');

    let brick2 = document.createElement('div');
    brick2.setAttribute('class', 'nieuwe-huis-overzicht__card');
    brick2.setAttribute('data-brick');

    let brick3 = document.createElement('div');
    brick3.setAttribute('class', 'nieuwe-huis-overzicht__card');
    brick3.setAttribute('data-brick');

    let brick4 = document.createElement('div');
    brick4.setAttribute('class', 'nieuwe-huis-overzicht__card');
    brick4.setAttribute('data-brick');

    element.appendChild(brick1);
    element.appendChild(brick2);
    element.appendChild(brick3);
    element.appendChild(brick4);
    return element;
}
