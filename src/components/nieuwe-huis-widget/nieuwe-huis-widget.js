// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

import $ from 'jquery';
export default NieuweHuisWidget;
import PlattegrondView from '../media-viewer-plattegrond/media-viewer-plattegrond';

// component configuration
const COMPONENT_SELECTOR = '[data-bricks]';
const BRICK_SELECTOR = '[data-brick]';
const BRICK_ODD_SELECTOR = '[data-bricks-odd]';
const BRICK_EVEN_SELECTOR = '[data-bricks-even]';
const COLUMN_CLASS = '.bricks-column';
const BP_MULTIPLE_COLUMNS = 1020;
const $window = $(window);

function NieuweHuisWidget(element) {
    const component = this;
    component.$element = $(element);
    component.$children = component.$element.find(BRICK_SELECTOR);
    component.scanMq(component.$element);
}

/*
    Method to create 2 columns on large BP
    `bricks` is an object `[data-bricks]`
    `mq` is a string, defined on styling.
*/
NieuweHuisWidget.prototype.prependBricksOnColumns = function(bricks, mq) {
    $(bricks).prepend('<div class=' + mq + ' data-bricks-even></div>');
    $(bricks).prepend('<div class=' + mq + ' data-bricks-odd></div>');
};

NieuweHuisWidget.prototype.prependBricks = function(bricks, mq) {
    $(bricks).prepend('<div class=' + mq + '></div>');
};

NieuweHuisWidget.prototype.appendDataOnMultipleColumns = function() {
    const component = this;
    component.$children.each(function(i, el) {
        if (i % 2 === 0) {
            $(el).appendTo(BRICK_ODD_SELECTOR);
        } else {
            $(el).appendTo(BRICK_EVEN_SELECTOR);
        }
    });
};

NieuweHuisWidget.prototype.appendDataOnSingleColumn = function() {
    const component = this;
    component.$children.each(function(i, el) {
        $(el).appendTo(COLUMN_CLASS);
    });
};

const PLATTEGROND_SELECTOR = '[data-media-viewer-plattegrond]';

NieuweHuisWidget.prototype.handleMqLarge = function(bricks, mq) {
    const component = this;
    component.prependBricksOnColumns(bricks, mq);
    component.appendDataOnMultipleColumns();

    $(PLATTEGROND_SELECTOR).each(function(index, element) {
        return new PlattegrondView(element);
    });
};

NieuweHuisWidget.prototype.handleMqDefault = function(bricks, mq) {
    const component = this;
    component.prependBricks(bricks, mq);
    component.appendDataOnSingleColumn();
};

NieuweHuisWidget.prototype.scanMq = function($el) {
    const component = this;
    $el.each(function() {
        component.detectMqVersion(this, BP_MULTIPLE_COLUMNS);
    });
};

/*
    Method to create 2 columns on large BP
    `bricks` is an object `[data-bricks]`
    `setBpForMultipleColumns` is a number (px) to define when to toggle the multiple columns layout.
*/
NieuweHuisWidget.prototype.detectMqVersion = function(bricks, setBpForMultipleColumns) {
    const component = this;
    let mediaquery = window.getComputedStyle(bricks, ':before').content;

    if ($window.width() >= setBpForMultipleColumns) {
        component.loadMultipleColumnsBricks(bricks, mediaquery);
    } else if (($(bricks).hasClass('loaded-multiple-column') && $window.width() >= setBpForMultipleColumns) || ($(bricks).hasClass('loaded-single-column') && $window.width() <= setBpForMultipleColumns)) {
        return;
    } else {
        component.loadSingleColumnBricks(bricks, mediaquery);
    }
};

NieuweHuisWidget.prototype.loadMultipleColumnsBricks = function(bricks, mediaquery) {
    const component = this;
    $(bricks).addClass('loaded-multiple-column');
    component.handleMqLarge(bricks, mediaquery);
};

NieuweHuisWidget.prototype.loadSingleColumnBricks = function(bricks, mediaquery) {
    const component = this;
    $(bricks).addClass('loaded-single-column');
    component.handleMqDefault(bricks, mediaquery);
};

// turn all elements with the default selector into components
$(COMPONENT_SELECTOR).each(function(index, element) {
    return new NieuweHuisWidget(element);
});
