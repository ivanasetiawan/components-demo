'use strict';

import NieuweHuisKoppelenSticky from './nieuwe-huis-koppelen-sticky';
import $ from 'jquery';
import sinonTest from 'sinon-test';
sinon.test = sinonTest.configureTest(sinon);

const fixture = getFixture();
const $fixture = $(fixture);

describe('nieuwe-huis-koppelen-hero component', function () {
    it('should listen to scroll event', sinon.test(function () {
        // Arrange
        const registerOnScrollEventHandlerSpy = sinon.spy(NieuweHuisKoppelenSticky.prototype, 'registerOnScrollEventHandler');

        // Act
        NieuweHuisKoppelenSticky.prototype.registerScroll($(fixture));

        // Assert
        assert(registerOnScrollEventHandlerSpy.calledOnce);
    }));

    it('should show the fixed CTA when no static CTA button available', function () {
        NieuweHuisKoppelenSticky.prototype.showStickyCTA();

        const ctaStickyisVisible = $(fixture).find('[data-nieuwe-huis-koppelen-sticky]').hasClass('visible');
        expect(ctaStickyisVisible).to.be.true;
    });

    it('should hide the fixed CTA when static CTA button available', sinon.test(function () {
        NieuweHuisKoppelenSticky.prototype.hideStickyCTA();

        const ctaStickyisHidden = $(fixture).find('[data-nieuwe-huis-koppelen-cta]').hasClass('visible');
        expect(ctaStickyisHidden).to.be.false;
    }));


    describe('isVisible method', function () {
        it('should return false when element is not defined', function () {

            const actual = NieuweHuisKoppelenSticky.prototype.isVisible();

            expect(actual).to.be.false;
        });

        it('should return true with a visible header', function () {
            const header = $fixture.find('[data-cta-header]')[0];

            const actual = NieuweHuisKoppelenSticky.prototype.isVisible(header);

            expect(actual).to.be.true;
        });
    });
});


describe('Check visibility of CTA', function () {
    it('should check if there is any header/footer CTA available on the page', function () {
        const ctaHeaderSelector = $(fixture).find('[data-cta-header]');
        const ctaFooterSelector = $(fixture).find('[data-cta-footer]');
        // On PhantomJS, elm.getBoundingClientRect() is {left: 0, right: 0, top: 0, height: 0, bottom: 0, width: 0}
        const ctaIsVisibleOnScreen = 0;
        const ctaIsHiddenOnScreen = 10;

        // Footer test
        expect(NieuweHuisKoppelenSticky.prototype.isVisible(ctaFooterSelector[0], ctaIsVisibleOnScreen)).to.be.true;
        expect(NieuweHuisKoppelenSticky.prototype.isVisible(ctaFooterSelector[0], ctaIsHiddenOnScreen)).to.be.false;

        // Header test
        expect(NieuweHuisKoppelenSticky.prototype.isVisible(ctaHeaderSelector[0], ctaIsVisibleOnScreen)).to.be.true;
        expect(NieuweHuisKoppelenSticky.prototype.isVisible(ctaHeaderSelector[0], ctaIsHiddenOnScreen)).to.be.false;
    });
});

function getFixture() {
    let element = document.createElement('div');
    element.setAttribute('data-nieuwe-huis-koppelen', '');

    // CTA on header
    let ctaHeader = document.createElement('a');
    ctaHeader.setAttribute('data-cta-header', '');

    // CTA on footer
    let ctaFooter = document.createElement('a');
    ctaFooter.setAttribute('data-cta-footer', '');

    // Sticky CTA
    let ctaSticky = document.createElement('div');
    ctaSticky.setAttribute('data-nieuwe-huis-koppelen-sticky', '');

    element.appendChild(ctaHeader);
    element.appendChild(ctaFooter);
    element.appendChild(ctaSticky);

    return element;
}
