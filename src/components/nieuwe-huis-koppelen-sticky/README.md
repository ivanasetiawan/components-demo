# Nieuwe Huis Koppelen Sticky component

## Functionality

A sticky component to show a quick infor regarding an object's address and CTA button.

## Usage

Include it using:
`{% include "components/nieuwe-huis-koppelen-sticky/nieuwe-huis-koppelen-sticky.html" %}`
