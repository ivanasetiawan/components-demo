// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

import $ from 'jquery';
import throttle from 'lodash/throttle';
export default NieuweHuisKoppelenSticky;

// component configuration
const COMPONENT_SELECTOR = '[data-nieuwe-huis-koppelen]';
const CTA_HEADER = '[data-cta-header]';
const CTA_FOOTER = '[data-cta-footer]';
const CTA_STICKY = '[data-nieuwe-huis-koppelen-sticky]';
const THRESHOLD_FOOTER = 10;
const THROTTLE_DELAY = 150;

function NieuweHuisKoppelenSticky(element) {
    const component = this;
    component.registerScroll($(element));
}

NieuweHuisKoppelenSticky.prototype.registerScroll = function (NieuweHuisKoppelenEl) {
    const component = this;
    component.$element = $(NieuweHuisKoppelenEl);
    component.ctaHeaderSelector = component.$element.find(CTA_HEADER);
    component.ctaFooterSelector = component.$element.find(CTA_FOOTER);
    component.ctaSticky = component.$element.find(CTA_STICKY);

    component.registerOnScrollEventHandler();
};

NieuweHuisKoppelenSticky.prototype.registerOnScrollEventHandler = function () {
    const component = this;

    $(window).on('scroll', throttle(function () {
        if (component.isVisible(component.ctaFooterSelector[0], THRESHOLD_FOOTER)
            || !component.isVisible(component.ctaHeaderSelector[0], 0, 'above')) {
            component.hideStickyCTA();
        } else {
            component.showStickyCTA();
        }
    }, THROTTLE_DELAY));
};

NieuweHuisKoppelenSticky.prototype.hideStickyCTA = function () {
    const component = this;
    component.ctaSticky.removeClass('visible');
};

NieuweHuisKoppelenSticky.prototype.showStickyCTA = function () {
    const component = this;
    component.ctaSticky.addClass('visible');
};

NieuweHuisKoppelenSticky.prototype.isVisible = function (element, threshold = 0, mode) {
    if (typeof element !== 'object') {
        return false;
    }
    const viewHeight = Math.max(document.documentElement.clientHeight, window.innerHeight);
    const isRect = element.getBoundingClientRect();
    const isAbove = isRect.bottom - threshold < 0;
    const isBelow = isRect.top - viewHeight + threshold >= 0;

    switch (mode) {
        case 'above':
            return isAbove;
        case 'below':
            return isBelow;
        default:
            return !isAbove && !isBelow;
    }
};

$(COMPONENT_SELECTOR).each(function(index, element) {
    if ($(element).is(':visible')) {
        return new NieuweHuisKoppelenSticky(element);
    }
});
