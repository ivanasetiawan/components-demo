// require this module where needed, either in a specific view or component or generically in src/index.js
'use strict';

import $ from 'jquery';
import debounce from 'lodash/debounce';

// what does this module expose?
export default Tabs;

// component configuration
const COMPONENT_SELECTOR = '[data-tabs]';
const TABS_TARGET_SELECTOR = '[data-tabs-target]';
const TABS_HANDLE_SELECTOR = '[data-tabs-handle]';
const $HTML_BODY = $('html, body');
const $window = $(window);
const DEBOUNCE_DELAY = 150;
const ANIMATE_DELAY = 500;

function Tabs(element) {
    const component = this;
    component.$element = $(element);
    component.$handle = component.$element.find(TABS_HANDLE_SELECTOR);
    component.$target = component.$element.find(TABS_TARGET_SELECTOR);
    component.registerOnClick();
    component.registerOnResize();
}

Tabs.prototype.registerOnClick = function() {
    const component = this;
    component.$handle.on('click', function(e) {
        e.preventDefault();
        const $this = $(this);
        const targetId = $this.attr('href');
        component.responseOnClick($this, targetId);
        component.scrollView();
    });
};

Tabs.prototype.responseOnClick = function($this, targetId) {
    const component = this;
    component.$handle.removeClass('active');
    component.$target.removeClass('active');
    $this.addClass('active');
    $(targetId).addClass('active');
};

Tabs.prototype.scrollView = function() {
    const component = this;
    const topPosition = component.$element.offset().top;
    $HTML_BODY.animate({
        scrollTop: topPosition
    }, ANIMATE_DELAY);
};

Tabs.prototype.registerOnResize = function() {
    const component = this;
    const windowWidth = $window.width();
    const windowHeight = $window.height();

    $window.on('resize', debounce(function () {
        component.checkWindowSize(windowWidth, windowHeight);
    }, DEBOUNCE_DELAY));
};

Tabs.prototype.checkWindowSize = function(windowWidth, windowHeight) {
    const component = this;
    if ($window.width() != windowWidth && $window.height() != windowHeight) {
        component.scrollView();
    }
};

// turn all elements with the default selector into components
$(COMPONENT_SELECTOR).each((index, element) => new Tabs(element));