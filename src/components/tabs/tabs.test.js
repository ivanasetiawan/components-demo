'use strict';

import Tabs from './tabs';
import $ from 'jquery';
import sinonTest from 'sinon-test';
sinon.test = sinonTest.configureTest(sinon);

let instance;
const fixture = getFixture();

describe('tabs component', function() {
    context('initial state', function() {
        before('create instance', function() {
            instance = new Tabs(fixture);
        });

        it('needs to listen to a click event', function() {
           expect(instance.registerOnClick).to.be.a('function');
        });

        it('needs to listen to a resize event', function() {
           expect(instance.registerOnResize).to.be.a('function');
        });
    })
});

describe('tabs component', function() {
    beforeEach('create instance', function() {
        instance = new Tabs(fixture);
    });

    it('should show the correct tab when a user clicks a handle button', function() {
        const $thisTest = $($(fixture).find('[data-tabs-handle]')[0]);
        const idString = $($thisTest).attr('href');
        const targetIdTest = $(fixture).find(idString);
        instance.responseOnClick($thisTest, targetIdTest);

        const tabsHandleWithIdVergelijken = $(fixture).find('[data-tabs-handle]')[0];
        const tabsTargetWithIdVergelijken = $(fixture).find('#vergelijkenTest')[0];
        assert.equal($(tabsHandleWithIdVergelijken).hasClass('active'), true);
        assert.equal($(tabsTargetWithIdVergelijken).hasClass('active'), true);
    });

    it('should listen to click event and redirect to the beginning of the tasks', sinon.test(function() {
        const scrollViewSpy = sinon.spy(Tabs.prototype, 'scrollView');
        const responseOnClickSpy = sinon.spy(Tabs.prototype, 'responseOnClick');

        instance.$handle.trigger('click');

        assert(scrollViewSpy.called);
        assert(responseOnClickSpy.called);
    }));
});

function getFixture() {
    const element = document.createElement('div');
    element.setAttribute('data-tabs');

    const tabsHandleOne = document.createElement('a');
    tabsHandleOne.setAttribute('data-tabs-handle');
    tabsHandleOne.setAttribute('href', '#vergelijkenTest');
    element.appendChild(tabsHandleOne);

    const tabsTargetOne = document.createElement('div');
    tabsTargetOne.setAttribute('data-tabs-target');
    tabsTargetOne.setAttribute('id', 'vergelijkenTest');
    element.appendChild(tabsTargetOne);

    const tabsHandleTwo = document.createElement('a');
    tabsHandleTwo.setAttribute('data-tabs-handle');
    tabsHandleTwo.setAttribute('href', '#aanvragTest');
    element.appendChild(tabsHandleTwo);

    const tabsTargetTwo = document.createElement('div');
    tabsTargetTwo.setAttribute('data-tabs-target');
    tabsTargetTwo.setAttribute('id', 'aanvragTest');
    element.appendChild(tabsTargetTwo);

    return element;
}