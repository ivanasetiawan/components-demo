# Tabs component

## Functionality

Component for basic tabs functionality. Tabs also listen to swipe event.

## Usage

Include the component by using `{% include "components/tabs/tabs.html" %}`
