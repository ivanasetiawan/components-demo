# Dev console

## Functionality

Avoid `console` errors in browsers that lack a console.

## Usage

Include before scripts in which `console` may be used.
