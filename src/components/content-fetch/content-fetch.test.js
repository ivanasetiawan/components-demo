'use strict';

import $ from 'jquery';
import ContentFetch from './content-fetch';
import sinonTest from 'sinon-test';
sinon.test = sinonTest.configureTest(sinon);

let instance;
const fixture = getFixture();

describe('content-fetch component', function() {
    it('should show the callback on complete', sinon.test(function() {
        // Arrange
        const url = $(fixture).attr('data-content-fetch-url');
        const onCompleteCallback = sinon.spy();
        // Act
        ContentFetch(fixture, url, onCompleteCallback());
        // Assert
        assert(onCompleteCallback.calledOnce);
    }));

    it('should hide the component when API is down or empty', sinon.test(function() {
        // Arrange
        const data = 'this is data from API';
        // Act
        ContentFetch.showData(fixture, data);
        // Assert
        expect($(fixture).text()).to.contain(data);
    }));
});

function getFixture() {
	const element = document.createElement('div');
	element.setAttribute('data-content-fetch');
	element.setAttribute('data-content-fetch-url', '/stubs/components/nieuwe-huis-widget-map/nieuwe-huis-map.html');
	return element;
}
