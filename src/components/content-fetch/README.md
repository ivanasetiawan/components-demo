# Content Fetch component

## Functionality

JS component to fetch content from API. This component is used on Hypotheek advice and as well on Nieuwe Huis Widget(s) (see: Map)

## Usage

Import JS component:
`import contentFetch from '../content-fetch/content-fetch';`

Usage:
	`element` is the element where you'd like to render the complete callback
	`urlFeed` is the URL API
	`onComplete` is the callback (function)

	Thus:
	contentFetch(element, component.urlFeed, handleComplete);