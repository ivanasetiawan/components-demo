# Cms Featured Articles component

## Functionality

The articles are loaded in the backend. This component contains only styling for those articles.

## Usage
Structure is:
	<div class="cms-featured-articles">
		<h2 class="cms-featured-articles-title">Title of the featured articles</h2>
		<div class="cms-featured-articles-container">
			// Main featured with big image
			<a href="" style="put background-image">
				<div class="cms-featured-main-content">
					<h2 class="cms-featured-main-title">Text of main featured article</h2>
					<p class="cms-featured-main-description">Description</p>
					<em class="cms-featured-main-read">Lees het artikel <span class="icon-arrow-right-white"></span></em>
				</div>
			</a>

			// This is the narrow articled on the right side
			<div class="cms-featured-narrow">
				<ul class="cms-featured-lists">
					// repeat as many as needed.
					<li class="cms-featured-list">
						<a href="">
							<div class="cms-featured-list-img">
								<img src="">
							</div>
							<h3 class="cms-featured-list-title">
								Title
							</h3>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
