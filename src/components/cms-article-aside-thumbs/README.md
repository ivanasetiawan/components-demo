# Cms Article Aside Thumbs component

## Functionality

A component to show list of related articles on the aside with thumbnails.
This component is currently used on MLI landing page.

## Usage
This component is mostly use the basic styling of `cms-featured-articles`(NARROW) on the homepage. Structure is the same except some classnames related `cms-article-aside-thumbs` are added.

	// This is the narrow articled on the right side
	<div class="cms-featured-narrow">
		<ul class="cms-featured-lists">
			// repeat as many as needed.
			<li class="cms-featured-list">
				<a href="">
					<div class="cms-featured-list-img">
						<img src="">
					</div>
					<h3 class="cms-featured-list-title">
						Title
					</h3>
				</a>
			</li>
		</ul>
	</div>
