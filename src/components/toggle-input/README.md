# Toggle component

## Functionality

CSS only toggle checkbox input that acts as a toggle switch (with on and off state).

When the input field is checked it shows a toggle switch with green background.
When the input field is unchecked, the background color is gray.

The name of the input field can be specified in the macro.
The initial state (checked / unchecked ) of the input field can be also set up in the macro.

Implementation remark: please leave value=true even when the current value is false. The checkbox send the value only if they are checked

