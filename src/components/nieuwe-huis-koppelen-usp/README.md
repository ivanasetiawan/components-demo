# Nieuwe Huis Koppelen Usp component

## Functionality

USP (Unique Selling Point) component on the Nieuwe Huis Koppelen page.

This component contains several types of articles.
- Default (Text only article)
- `usp-content-left` (Content on the left and icon on the right)
- `usp-content-right` (Content on the right and icon on the left)
- `usp-img` (Full width image with address on top of it)
- `usp-basic-button` (Contains text and CTA)

## Usage
[OPT]: You can add classname `nieuwe-huis-koppelen-usp-footer--no-img` on `nieuwe-huis-koppelen-usp-footer`

{% macro nieuweHuisKoppelenUsp(
	type = "",
	iconSrc = "/assets/components/nieuwe-huis-koppelen-usp/icon-usp-mortgage.svg",
	title = "Rond de financiering af",
	description = "<p>Je hebt waarschijnlijk al een adviesgesprek gehad en weet wat je kan lenen.</p><p>Nu moet je de financiering afronden. Wij helpen bij het overzicht houden van alletaken die je moet doen en alle documenten die je nodig hebt voor de aanvraag.</p>"
) %}

- `type` of the macro. There are 5 different types (see above)
- `iconSrc` accepts string (URL)
- `title` accepts string. It's the tittle of the article
- `description` accepts HTML. It's the description of the article
