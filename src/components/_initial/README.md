# Iniital js component

## Functionality

Initial js placed in the head of the html page in order:

- 'Cut the mustard': based on `document.querySelector` and `window.addEevntListener` we add the `js` class to the html element.
- Placeholder feature detection: check for support of the input placeholder. If so add the `supports-placeholder` class to the html element. Normally feature detection is placed inside components, but if done for placeholder the layout flickers. So we choose to move it to the head and check for the class in the component.

## Usage

Include inside `<head>` on first page view. The javascript sets a cookie `html-classes`. For subsequent page views if the cookie `html-classes` is present the backend should render the value from the cookie on the `html` element and exclude the initial script.
