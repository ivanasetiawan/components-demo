(function(window, document) {
    'use strict';

    if ('querySelector' in document && 'addEventListener' in window) {
        var htmlClasses = ' js';

        if ('placeholder' in document.createElement('input')) {
            htmlClasses += ' supports-placeholder';
        }

        document.documentElement.className += htmlClasses;
        setCookie('html-classes', htmlClasses);
    }

    function setCookie(name, value, days) {
        if (typeof useCookie === 'undefined') {
            // if value is a false boolean, we'll treat that as a delete
            if (value === false) {
                days = -1;
            }

            var expires;
            if (days) {
                var date = new Date();
                date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                expires = '; expires=' + date.toGMTString();
            } else {
                expires = '';
            }

            document.cookie = name + '=' + value + expires + '; path=/';
        }
    }

}(window, window.document));
