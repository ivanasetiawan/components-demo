# Nieuwe Huis Widget Map component

## Functionality

Map Widget component on the dashboard of Nieuwe Huis Overzicht.

## Usage

Include the component when needed.
{% include "components/nieuwe-huis-widget-map/nieuwe-huis-widget-map.html" %}