# Cookie Policy component

## Functionality

For the {client} site to work properly we occassionally use cookies.
This user informs the user about our cookie policy using a notification.
Once the user accepts the policy a cookie is set so we no longer show the notification,
until a new version of the policy is taken into effect.

## Usage

### If cookie not accepted

Include the component HTML with the following attributes:

* `data-cookie-policy`
* `data-cookie-policy-version="{versionNumber}"` (obviously, replace versionNumber)

By default any user interaction (eg. on links and inputs) accepts the policy.
To prevent this default behaviour, add the `data-cookie-policy-ignore` attribute
(eg. on the readmore link inside the cookie policy itself).

### If cookie is accepted

Once the user accepts the policy a cookie is set with the name `cookiesAccepted_{versionNumber}={versionNumber}`.

On server-side, detect if cookie is accepted. If it is, and the version number in the cookie matches the current
version of the policy, don't include the component HTML.
