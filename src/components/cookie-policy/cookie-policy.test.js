// Unit test for cookie-policy component
// Mocha and Chai are always included
'use strict';
import $ from 'jquery';
import CookiePolicy from './cookie-policy';

let instance;
const fixture = getFixture();

describe('cookie policy component', function() {

    beforeEach('create instance', function() {
        instance = new CookiePolicy(fixture);
    });

    it('should get `version` from data attribute', function() {
        (instance.version).should.equal('2');
    });

    it('should have `acceptPolicy` method', function() {
        (typeof instance.acceptPolicy).should.equal('function');
    });

    it('should have `setCookie` method', function() {
        (typeof instance.setCookie).should.equal('function');
        (typeof instance.setCookie()).should.equal('string');
    });

    it('should contains `cookiesAccepted_` ', function() {
        // We created a fixture with data-cookie-policy-version = 2
        const cookie = instance.setCookie();
        expect(cookie).to.contain('cookiesAccepted_2=2;');
        expect(cookie).to.contain('; expires=');
        expect(cookie).to.contain('; path= /');
    });

    it('should have `.is-acceptable` when shown', function() {
        (instance.$element.hasClass('is-acceptable')).should.equal(true);
    });

    it('should have `.is-accepted` when accepted', function() {
        instance.acceptPolicy();
        (instance.$element.hasClass('is-accepted')).should.equal(true);
    });

});

describe('cookie policy component', function() {
    let $handle;

    beforeEach('create instance', function() {
        instance = new CookiePolicy(fixture);
        $handle = instance.$element.find('[data-cookie-trigger]');
        instance.toggleExpand(fixture);
    });

    it('should hide cookie expand target on init state', function() {
        (instance.$element.find('[data-cookie-target]').attr('class')).should.equal('hidden');
    });

    it('should hide `data-cookie-trigger` when clicked and show `data-cookie-target`', function() {
        $($handle).trigger('click');
        (instance.$element.find('[data-cookie-trigger]').is(':hidden').should.equal(true));
        (instance.$element.find('[data-cookie-target]').attr('class').should.be.empty);
    });

});

function getFixture() {
    let element = document.createElement('aside');
    element.setAttribute('data-cookie-policy', '');
    element.setAttribute('data-cookie-policy-version', '2');

    let cookieToggle = document.createElement('div');
    cookieToggle.setAttribute('data-cookie-toggle', '');

    let cookieTarget = document.createElement('span');
    cookieTarget.setAttribute('data-cookie-target', '');

    let cookieTrigger = document.createElement('a');
    cookieTrigger.setAttribute('data-cookie-trigger', '');

    cookieToggle.appendChild(cookieTarget);
    cookieToggle.appendChild(cookieTrigger);

    element.appendChild(cookieToggle);

    return element;
}
