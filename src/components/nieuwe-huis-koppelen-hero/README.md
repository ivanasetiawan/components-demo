# Nieuwe Huis Koppelen Hero component

## Functionality

Hero of Nieuwe Huis Koppelen page.

## Usage

For no image styling, add classname `nieuwe-huis-koppelen-hero--no-img` on `nieuwe-huis-koppelen-hero`. Thus:
	<article class="nieuwe-huis-koppelen-hero nieuwe-huis-hero--no-img" data-nieuwe-huis-koppelen-hero>

Include it using:
`{% include "components/nieuwe-huis-koppelen-hero/nieuwe-huis-koppelen-hero.html" %}`
