# Native Textlink Banner component

## Functionality

To show how the textlink banner native ad look (example: ads on object details).

## Usage

Native format and styles can be found on DFP. (https://www.google.com/dfp/6835#delivery/ListNativeStyles)

For this ad, the format we're using:
	- [%Logo%] => max height & max height is 20
	- [%MainText%] => ~ 30 characters
	- [%AltTag%]
	- [%1x1pixel%] is tracking pixel

## HTML template to be pasted on DFP creative templates (https://www.google.com/dfp/6835#delivery/ListCreativeTemplates)

	<a class="native-textlink-banner" target="_blank" href="%%CLICK_URL_ESC%%%%DEST_URL%%">
		[%MainText%]
		<img title="Advertentie" src="[%Logo%]" alt="[%AltTag%]">
	</a>


