# App core styles

## Functionality

Foundation for style of the entire app, including color palette, typography, buttons, links, lists, forms and more.

## Usage

Visit the style guide for an overview of the core styles and instructions of how to use them.
