# Advertisements Lazy component

## Functionality

The Advertisements Lazy component is an extention of basic advertisement component. 

## Usage

Add `data-advertisement` & `data-advertisement-lazy` attributes on the element to trigger the functionality.

Optional data attributes are:
- `data-advertisement-lazy-onscroll` is lazy loading ad feature. This attribute makes sure that the ad(s) will only load when they're on the viewport. (only on screen < bpMedium - 1 (749))
- `data-advertisement-lazy-onscroll-onexpanded` is lazy loading ad feature. This attribute makes sure that the ad(s) will only load when kenmerken element is expanded and when they're on the viewport. (only on screen < bpMedium - 1 (749)) ~ On object detail page.
- `data-advertisement-lazy-threshold` threshold amount of pixels we start loading the ad before it goes into viewport. Defaults to 0, (ad loads when in viewport). 



# Advertisements Searchresults component

## Functionality

The Advertisements Searchresults component is an extention of basic advertisement component. This component is currently used only on search results page. 

## Usage

Add `data-advertisement` & `data-advertisement-searchresults` attributes on the element to trigger the functionality.
