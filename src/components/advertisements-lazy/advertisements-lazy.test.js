// Unit test for autocomplete component
// Mocha and Chai are always included
'use strict';

import $ from 'jquery';
import AdvertisementsLazy from './advertisements-lazy';
import sinonTest from 'sinon-test';
sinon.test = sinonTest.configureTest(sinon);

let instance;
const fixture = getFixture();

describe('advertisements lazy component', function() {
    it('should remove a namespace-scoped scroll event handler after the event has been handled', sinon.test(function() {
        //Arrange
        const configStub = AdvertisementsLazy.getAdConfig(fixture);
        const unregisterEventStub = sinon.stub(AdvertisementsLazy, 'unregisterEvent');

        //Act
        AdvertisementsLazy.unregisterScrollEventHandlerWhenNotNeeded();

        //Assert
        sinon.assert.calledWith(unregisterEventStub, 'scroll.advertisement');
    }));

    it('should not load the lazy ads with attribute `data-advertisement-lazy-onscroll-onexpanded` on scroll when on mobile', sinon.test(function() {
        // Arrange
        const configStub = AdvertisementsLazy.getAdConfig(fixture);
        const registerOnScrollEventHandlerSpy = sinon.spy(AdvertisementsLazy, 'registerOnScrollEventHandler');

        //Act
        AdvertisementsLazy.pushLazyAdSlotOnMobileOnKenmerkenExpanded(fixture, configStub, true);

        // Assert
        assert.isFalse(registerOnScrollEventHandlerSpy.calledOnce);
    }));

    it('should trigger the lazy ads with attribute `data-advertisement-lazy-onscroll-onexpanded` on kenmerkenExpanded when on mobile', sinon.test(function() {
        // Arrange
        const configStub = AdvertisementsLazy.getAdConfig(fixture);
        const unregisterScrollEventHandlerWhenNotNeededSpy = sinon.spy(AdvertisementsLazy, 'unregisterScrollEventHandlerWhenNotNeeded');

        //Act
        AdvertisementsLazy.pushLazyAdSlotOnMobileOnKenmerkenExpanded(fixture, configStub, true);

        // Assert
        expect($(fixture).attr('class')).to.contain('dfp-display-triggered');
        assert(unregisterScrollEventHandlerWhenNotNeededSpy.calledOnce);
    }));
});


describe('advertisement search results component', function() {
    const fixtureTop = adTop();
    let configTop = AdvertisementsLazy.getAdConfig(fixtureTop);

    it('should get `adunit path` value from top ad element', function() {
        (configTop.adunitpath).should.contain('resultaatlijst');
    });

    it('should have `size-mapping` value from top ad element', function() {
        (configTop.sizemapping).should.be.an('array').to.have.lengthOf(3);
    });

    it('should have `size-dafault` value from top ad element', function() {
        (configTop.sizedefault).should.be.an('array').to.eql([0, 0]);
    });

    it('should get `categories url` value from element', function() {
        (configTop.categoriesUrl).should.contain('dondraper.{client}.io/api/targeting/?');
    });
});

describe('advertisement search results component', function() {
    const fixtureBottom = adBottom();
    const configBottom = AdvertisementsLazy.getAdConfig(fixtureBottom);

    it('should get `adunit path` value from bottom ad element', function() {
        (configBottom.adunitpath).should.contain('resultaatlijst');
    });

    it('should have `size-mapping` value from bottom ad element', function() {
        (configBottom.sizemapping).should.be.an('array').to.have.lengthOf(2);
    });

    it('should have `size-dafault` value from bottom ad element', function() {
        (configBottom.sizedefault).should.be.an('array').to.eql([0, 0]);
    });

    it('should get `categories url` value from element', function() {
        (configBottom.categoriesUrl).should.contain('dondraper.{client}.io/api/targeting/?');
    });
});

// Create fixture for Lazy ad
function getFixture() {
    //set element (wrapper of the component)
    const element = document.createElement('aside');
    element.setAttribute('data-advertisement');
    element.setAttribute('data-advertisement-user-id', '31cce829-eacc-4309-b946-7b90e2a228fd');
    element.setAttribute('data-advertisement-adunit-path', '/6835/{client}.koop/resultaatlijst');
    element.setAttribute('data-advertisement-size-mapping', '[ [ [ 768, 0 ], [ [ 728, 90 ] ] ], [ [ 0, 0 ], [  ] ] ]');
    element.setAttribute('data-advertisement-size-default', '[ 728, 90 ]');
    element.setAttribute('data-advertisement-categories-url', '{{ paths.stubs }}components/advertisements/5000.json?searchQuery={searchQuery}&userID=31cce829-eacc-4309-b946-7b90e2a228fd&lokNumber=5000');
    element.setAttribute('class', 'advertisement');
    element.setAttribute('data-advertisement-lazy', '');
    element.setAttribute('data-advertisement-lazy-onscroll', '');
    element.setAttribute('data-advertisement-lazy-threshold', '200');
    element.setAttribute('data-advertisement-has-user-accepted-cookies', 'True');
    return element;

}

// Create fixture for Async ads
function adTop() {
    //set aside
    let adTop = document.createElement('aside');
    adTop.setAttribute('data-advertisement');
    adTop.setAttribute('data-advertisement-lok', '2001');
    adTop.setAttribute('data-advertisement-size-mapping', '[[[750, 0],[[983, 152],[728, 90],[983, 104]]],[[320, 0],[[320, 50],[320, 100],[983, 152]]],[[0, 0],[[983, 152]]]]');
    adTop.setAttribute('data-advertisement-size-default', '[0, 0]');
    adTop.setAttribute('data-advertisement-adunit-path', '/7276/{client}.koop/resultaatlijst');
    adTop.setAttribute('data-advertisement-categories-url', 'https://dondraper.{client}.io/api/targeting/?payload=6qYg%2boYHLMx4Pp9wZD7ub9%2fd8qVbaZzMBw%2bEpMAQnN5oc6r%2bLyl9scp9D2T40rsuQ%2fXJ1ZAy%2fbOAqJfA7IxNIXhg7UpJHgA%2bLDCJsGyR3ksdPlHlZNRW6CwfnT8xH85I1My1LnPPCrPMdOfpKy8Ky3fsRImJ7CT0FiZqu5PrFDnRypA2nS99RpAfytN6gdxLLSmTdFmCCDL8b4%2fpNmv%2fv8elBYr9pu5kRxTQWQQTyLPjPUMGKubIModir0yybCQ36wgyHF3YfcaQag%2bTlNDgX9vQxUAGageY0OAd4tTQOkYLrYS7%2fsoiAjPBDiSVN4cZUcq8zlA5mtFiAS75aCoU7GrPJ8vZ14EAYr%2bE%2bxZrScsd9oB4Fiwy3sjj8E1tCsAJ0O9O4si64n3p02yVmbjq88KdScbPVNqQvwQe1kYqX4bNzOxfBbBRoXjbjpaxSWE3XSPgzre17GYBReuSzS76y9jbfX2nQE0ZBVeu2fT%2f5HxjCeLVVCu%2fpd4%2bNn2GaIkDsXIKh%2ftmX7qcZ%2fIOIhKA302FngR2sUnddk0lycn7HIJSRTmG69Lb9FbEA3YFtezP');

    return adTop;
}

function adBottom() {
    //set aside
    let adBottom = document.createElement('aside');
    adBottom.setAttribute('data-advertisement');
    adBottom.setAttribute('data-advertisement-lok', '2002');
    adBottom.setAttribute('data-advertisement-size-mapping', '[ [ [ 750, 0 ], [ [ 728, 90 ], [ 580, 72 ], [ 468, 60 ] ] ], [ [ 0, 0 ], [ [ 320, 50], [ 320, 100] ] ] ]');
    adBottom.setAttribute('data-advertisement-size-default', '[ 0, 0 ]');
    adBottom.setAttribute('data-advertisement-adunit-path', '/7276/{client}.koop/resultaatlijst');
    adBottom.setAttribute('data-advertisement-categories-url', 'https://dondraper.{client}.io/api/targeting/?payload=6qYg%2boYHLMx4Pp9wZD7ub9%2fd8qVbaZzMBw%2bEpMAQnN5oc6r%2bLyl9scp9D2T40rsuQ%2fXJ1ZAy%2fbOAqJfA7IxNIXhg7UpJHgA%2bLDCJsGyR3ksdPlHlZNRW6CwfnT8xH85I1My1LnPPCrPMdOfpKy8Ky3fsRImJ7CT0FiZqu5PrFDnRypA2nS99RpAfytN6gdxLLSmTdFmCCDL8b4%2fpNmv%2fv8elBYr9pu5kRxTQWQQTyLPjPUMGKubIModir0yybCQ36wgyHF3YfcaQag%2bTlNDgX9vQxUAGageY0OAd4tTQOkYLrYS7%2fsoiAjPBDiSVN4cZUcq8zlA5mtFiAS75aCoU7GrPJ8vZ14EAYr%2bE%2bxZrScuQIQ9ur6l%2bOyz0p1FMajJJlqZyzsVkKZBzzkgnQYRvLu%2fTbfTmDLYgyOdCiteJZNVzwl1Ks5ZxnOdCkXU9UESLzXs7qUMAGOImk8fDodU31XmIc3uL2uxWgvPg%2fN3rPfFWPHL0iZEuhnvz%2ffu9eq7N8D6KmDw5kFwb8vIjFTnsXacgKeyDQZLlPJHacE2E9dklH06otrC2XFr7c80JsPnc');

    return adBottom;
}
