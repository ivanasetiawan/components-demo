# Cms Related Articles component

## Functionality

A CMS related component to show some articles related to the page. This article is used on the bottom of CMS article details as well on the `Thank you` page (bezichtiging confirmation).

## Usage

	// Block of independent component
	<div class="cms-related-articles">
		// Needs to have container because this component will always be outside the main component.
		<div class="container">
			<h2 class="cms-related-articles-title">Lees ook</h2>

			// Wrapper of the articles
			<div class="cms-related-articles-wrap">

				<article class="cms-related-article">
					<header>
						<a href="#" class="cms-related-article-thumb"></a>
					</header>
					<p>Simple description of the article</p>
				</article>
			</div>

		</div>
	</div>
