// Unit test for test component
// Mocha and Chai are always included
'use strict';

//todo: figure out how to prevent 404s from GET requests on stubs
import $ from 'jquery';
import AdvertisementHypotheekAdvies from './hypotheek-advies';

const fixture = getFixture();

describe('advertisement hypotheek advies component', function() {
    let instance;
    let server;

    before('create instance', function() {
        server = sinon.fakeServer.create();
        server.autoRespond = true;
    });

    beforeEach('create instance', function() {
        instance = new AdvertisementHypotheekAdvies(fixture);
    });

    it('should get `API url` from element', function() {
        (instance.urlFeed).should.be.a('string')
            .to.contain('/stubs/components/hypotheek-advies/hypotheek-form.html');
    });

    after(function() {
        server.restore();
    });
});

describe('Hypotheek leads form toggle',function(){

    it('should expand hypotheek Leads form ',function(){
        // arrange
        const trigger = $(fixture).find('[data-hypotheek-advies-handle]');

        // act
        AdvertisementHypotheekAdvies.prototype.checkedFeedback(trigger);

        // assert
        expect($(fixture).find('[data-hypotheek-advies-box]').hasClass('is-expanded')).to.be.true;
        expect($(trigger).prop('required')).to.be.true;
    });

    it('should collapse hypotheek Leads form ',function(){
        // arrange
        const trigger = $(fixture).find('[data-hypotheek-advies-handle]');

        // act
        AdvertisementHypotheekAdvies.prototype.unCheckedFeedback(trigger);

        // assert
        expect($(fixture).find('[data-hypotheek-advies-box]').hasClass('is-expanded')).to.be.false;
        expect($(trigger).prop('required')).to.be.false;
    });
});

function getFixture() {
    //set element (wrapper of the component)
    const element = document.createElement('div');
    element.setAttribute('data-hypotheek-advies');
    element.setAttribute('data-content-fetch-url', '/stubs/components/hypotheek-advies/hypotheek-form.html');

    const asideElement = document.createElement('aside');
    asideElement.setAttribute('class','');
    asideElement.setAttribute('data-hypotheek-advies-box','');

    //set Hypotheek Advies form element
    const formElement = document.createElement('input');
    formElement.type = 'checkbox';
    formElement.setAttribute('data-hypotheek-advies-handle','');

    element.appendChild(asideElement);
    asideElement.appendChild(formElement);

    return element;
}