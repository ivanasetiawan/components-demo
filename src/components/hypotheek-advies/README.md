# Advertisement hypotheek Advies component

## Functionality

The advertisement hypotheek advies component offers users to ask for a mortgage advice in regards to the house they're interested in.

## Usage

Add `Add data-hypotheek-advies` attribute on the element to trigger the functionality. 
Add `data-content-fetch-url` attribute to fetch the data.

Necessary data attributes are:
- `data-hypotheek-advies-trigger` This attribute is the trigger to toggle the hidden form. Put this on the label of the checkbox. 
- `data-hypotheek-advies-target` This attribute specify the element which will be shown or hidden based on the checkbox trigger.
