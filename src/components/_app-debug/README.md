# app debug

## Functionality

Allows you to annotate components in the previewer using the label icon.
The annotations outline the components and link to the individual component previews.

## Usage

* Add `data-component="component-name"` to the root element of each component.
* Include (inline) the component's CSS and JS just before the closing `</body>` tag.
* **Only use in the previewer. Don't use in production.**
