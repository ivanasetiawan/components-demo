'use strict';

// Mocha and Chai are always included
import $ from 'jquery';
import ToggleVisibility from './toggle-visibility';
import '../app-utils/app-utils-array-extensions';

describe('toggle-visibility component', function () {
    let component;
    let fixture;

    it('should toggle with 2 containers', function () {
        fixture = $(`
        <div data-toggle-visibility>
        
            <div data-toggle-visibility-container >First</div>        
            <div data-toggle-visibility-container class="is-hidden">Second</div>
        
            <button data-toggle-visibility-trigger>Trigger</button>
        </div>
        `);

        component = new ToggleVisibility(fixture);

        assert.equal(component.$$containers.length, 2);

        const $first = $(component.$$containers[0]);
        const $second = $(component.$$containers[1]);

        assert.equal($first.hasClass('is-hidden'), false);
        assert.equal($second.hasClass('is-hidden'), true);

        component.$triggers.click();

        assert.equal($first.hasClass('is-hidden'), true);
        assert.equal($second.hasClass('is-hidden'), false);

        component.$triggers.click();

        assert.equal($first.hasClass('is-hidden'), false);
        assert.equal($second.hasClass('is-hidden'), true);
    });

    it('should toggle with 3 containers', function () {
        fixture = $(`
        <div data-toggle-visibility>
        
            <div data-toggle-visibility-container >First</div>        
            <div data-toggle-visibility-container class="is-hidden">Second</div>
            <div data-toggle-visibility-container class="is-hidden">Third</div>
        
            <button data-toggle-visibility-trigger>Trigger</button>
        </div>
        `);

        component = new ToggleVisibility(fixture);

        assert.equal(component.$$containers.length, 3);

        const $first = $(component.$$containers[0]);
        const $second = $(component.$$containers[1]);
        const $third = $(component.$$containers[2]);

        assert.equal($first.hasClass('is-hidden'), false);
        assert.equal($second.hasClass('is-hidden'), true);
        assert.equal($third.hasClass('is-hidden'), true);

        component.$triggers.click();

        assert.equal($first.hasClass('is-hidden'), true);
        assert.equal($second.hasClass('is-hidden'), false);
        assert.equal($third.hasClass('is-hidden'), true);

        component.$triggers.click();

        assert.equal($first.hasClass('is-hidden'), true);
        assert.equal($second.hasClass('is-hidden'), true);
        assert.equal($third.hasClass('is-hidden'), false);

        component.$triggers.click();

        assert.equal($first.hasClass('is-hidden'), false);
        assert.equal($second.hasClass('is-hidden'), true);
        assert.equal($third.hasClass('is-hidden'), true);
    });

    it('should toggle with labelled containers', function () {
        fixture = $(`
        <div data-toggle-visibility>
        
            <div data-toggle-visibility-container="1" >First</div>        
            <div data-toggle-visibility-container="2" class="is-hidden">Second</div>
        
            <button data-toggle-visibility-trigger="1">Trigger</button>
            <button data-toggle-visibility-trigger="2">Trigger</button>
        </div>
        `);

        component = new ToggleVisibility(fixture);

        assert.equal(component.$$containers.length, 2);
        assert.equal(component.$triggers.length, 2);

        const $first = $(component.$$containers[0]);
        const $second = $(component.$$containers[1]);

        const $trigger1 = $(component.$triggers[0]);
        const $trigger2 = $(component.$triggers[1]);

        assert.equal($first.hasClass('is-hidden'), false);
        assert.equal($second.hasClass('is-hidden'), true);

        $trigger1.click();

       assert.equal($first.hasClass('is-hidden'), false);
       assert.equal($second.hasClass('is-hidden'), true);

        $trigger1.click();

       assert.equal($first.hasClass('is-hidden'), false);
       assert.equal($second.hasClass('is-hidden'), true);

        $trigger2.click();

       assert.equal($first.hasClass('is-hidden'), true);
       assert.equal($second.hasClass('is-hidden'), false);

        $trigger2.click();

       assert.equal($first.hasClass('is-hidden'), true);
       assert.equal($second.hasClass('is-hidden'), false);

        $trigger1.click();

       assert.equal($first.hasClass('is-hidden'), false);
       assert.equal($second.hasClass('is-hidden'), true);
    });
});
