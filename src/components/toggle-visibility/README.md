# Toggle Visibility component

## Functionality

Toggles visibility of html elements. 
If the trigger has a label, it will show the container with the same label. 
If the trigger does not have a label, it will show the next listed container (from top to bottom).

PLEASE NOTE: This is a JAVASCRIPT dependent component. If javascript is disabled the toggle won't work.
So it should be always used as an enhaced feature and not a critical toggle.

For instance, 
It can be used for displaying a preview when clicking a radio button, since it's an enhaced functionality.
It should not be used for pagination

## Usage

Let's see a basic example.

    <div data-toggle-visibility>
        <div data-toggle-visibility-container >First</div>
        <div data-toggle-visibility-container="Sec" class="is-hidden">Second</div>
        <div data-toggle-visibility-container class="is-hidden">Third</div>
    
        <button data-toggle-visibility-trigger>Trigger</button>
        <button data-toggle-visibility-trigger="Sec">Show sec (and hide others)</button>
    </div>
    
The scope of this component is defined with attribute `data-toggle-visibility`

Inside it, we define every container with attribute `data-toggle-visibility-container`.
The container that is initially displayed MUST NOT have "is-hidden" class. 
All other containers MUST HAVE initially "is-hidden" class.


### Display next

When the user clicks in element with attribute `data-toggle-visibility-trigger`, 
the next container (defined from top to bottom) will be the only one without "is-hidden" class.

### Labelling

If a container is labelled (like in `data-toggle-visibility-container="Sec" ` ) a trigger can 
display that specific element (and hide the other ones). You can do that by clicking an 
element with attribute `data-toggle-visibility-trigger="Sec"`


## Examples

Examples  can be found in the test file or in view user-reviews-write.
