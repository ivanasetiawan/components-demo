# Nieuwe Huis Widget Content component

## Functionality

To suggest related articles to the user.

## Usage

Include the component on the page.
    {% include "components/nieuwe-huis-widget-related-articles/nieuwe-huis-widget-related-articles.html" %}