# Class Observable component

## Functionality

JavaScript implementation of the Observer patterns with optional topics.

## Usage

Global example:

function Observed() {
    var publics = {};
    publics.observer = new Observable();
    
    var result = 0;
    publics.doThings = function(){
        result++;
        publics.observer.notify(result);
    }
    
    //TO BE CALLED BY THE OBSERVER
    publics.onThingsDone = function(observer){
        publics.observer.listen(observer);
    }
    
    return publics;
}

var observed = new Observed();
    
observed.onThingsDone(function(result) {
    console.debug(result);
}); 

observed.doThings(); // outputs 1 
observed.doThings(); // outputs 2
