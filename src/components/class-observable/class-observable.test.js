// Unit test for class-observable component
// Mocha and Chai are always included
'use strict';

import Observable from './class-observable';

let observer;

describe('class-observable component', function() {

    beforeEach('create instance', function() {
        observer = new Observable();
    });

    it('should call only global observers', function() {
        var callback1Called =false;
        observer.listen(function (message){
            callback1Called = message;
        });

        var callback2Called =false;
        observer.listen("THIS_IS_A_TOPIC", function (message){
            callback2Called = message;
        });

        var callback3Called =false;
        observer.listen(function (message){
            callback3Called = message;
        });

        observer.notify("hello");

        assert.equal(callback1Called, "hello", "Global callback should be called");
        assert.equal(callback2Called, false, "Topic callback should NOT be called");
        assert.equal(callback3Called, "hello", "Global callback should be called");
    });

    it('topic messages should go also to global observers', function() {
        let topic1 = "topic1";
        let topic2 = "topic2";

        var globalObserverCalls = 0;
        observer.listen(function (){
            globalObserverCalls++;
        });

        var topic1Calls = 0 ;
        observer.listen(topic1, function (){
            topic1Calls++;
        });

        var topic2Calls = 0 ;
        observer.listen(topic2, function (){
            topic2Calls++;
        });

        observer.notify(); // NOTIFY TO GLOBAL ONLY
        observer.notify(topic1); // NOTIFY TO GLOBAL & TOPIC 1
        observer.notify(topic2); // NOTIFY TO GLOBAL & TOPIC 2
        observer.notify(topic1); // NOTIFY TO GLOBAL & TOPIC 1

        assert.equal(globalObserverCalls, 4, "Global callback should be always called");
        assert.equal(topic1Calls, 2, "Topic callback should only be called with his topic");
        assert.equal(topic2Calls, 1, "Topic callback should only be called with his topic");
    });
});
