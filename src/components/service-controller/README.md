# Service Controller component

## Functionality

Established a 1-1 binding between an html element and a js controller. 

The service adds an unique identifier to the html.

## Usage

This:

$(COMPONENT_SELECTOR).each(function(index, element) {
    return new ComponentController(element);
});

Can be now replaced with:

controllerService.createAll(COMPONENT_SELECTOR, ComponentController);

 
