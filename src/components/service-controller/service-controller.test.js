// Unit test for service-controller component
// Mocha and Chai are always included
'use strict';

import controllerService from './service-controller';

let instance;

describe('service-controller component', function () {

    function TestClass() {
    }

    it('should create only one instance per element', function () {
        const fixture = document.createElement('diva');

        var a = controllerService.getInstance(TestClass, fixture);
        var b = controllerService.getInstance(TestClass, fixture);

        a.should.equal(b);
    });

    it('2 elements must have 2 controllers (1 per element)', function () {
        const fixture1 = document.createElement('diva');
        const fixture2 = document.createElement('diva');

        var a = controllerService.getInstance(TestClass, fixture1);
        var b = controllerService.getInstance(TestClass, fixture2);

        a.should.not.equal(b);
    });
});
