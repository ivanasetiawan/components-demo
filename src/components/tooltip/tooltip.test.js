'use strict';

import $ from 'jquery';
import Tooltip from './tooltip';

describe('Tooltip component', function() {
    let component;
    let fixture;
    let sandbox;

    beforeEach('create instance', function() {
        sandbox =  sinon.sandbox.create();
        fixture = createFixture();
        component = new Tooltip(fixture);
    });

    afterEach(function () {
        sandbox.restore();
    });

    it('should not initialize tooltip on small devices', function() {
        //Prepare
        sandbox.stub($.prototype,'outerWidth').returns(300);

        //Preassert
        assert.equal(undefined, component.initialInfo);

        //Act
        component.tooltipHandler();

        //Assert
        assert.equal(undefined, component.initialInfo);
    });

    it('should not initialize tooltip on unopened big devices', function() {
        //Prepare
        component.$element.removeClass("is-expanded");
        sandbox.stub($.prototype,'outerWidth').returns(600);

        //Preassert
        assert.equal(undefined, component.initialInfo);

        //Act
        component.tooltipHandler();

        //Assert
        assert.equal(undefined, component.initialInfo);
    });

    it('should initialize tooltip on opened big devices', function() {
        //Prepare
        sandbox.stub($.prototype,'outerWidth').returns(600);

        //Preassert
        assert.equal(undefined, component.initialInfo);

        //Act
        component.tooltipHandler();

        //Assert
        assert.equal('object', typeof component.initialInfo);
    });

    it('should remove title attr on load', function() {
        //Prepare
        sandbox.stub($.prototype,'outerWidth').returns(600);

        //Assert
        assert.equal('', component.$element.attr('title'));
        assert.equal('This is a tooltip', component.$element.attr('data-title'));
    });

    it('should restore initial possible if it fits', function() {
        //Prepare
        component.initialInfo = {
            isBottom : true,
            isReversed : true
        };
        sandbox.stub($.prototype,'outerWidth').returns(600);
        sandbox.stub(component,'doesInitialTooltipFitInCurrentWindow').returns(true);

        //Act
        component.tooltipHandler();

        //Assert
        assert.equal(true, component.$element.hasClass('bottom'));
        assert.equal(true, component.$element.hasClass('reverse'));
    });

    it('should NOT restore initial if it DOES NOT fit', function() {
        //Prepare
        component.initialInfo = {
            isBottom : true,
            isReversed : true
        };
        sandbox.stub($.prototype,'outerWidth').returns(600);
        sandbox.stub(component,'doesInitialTooltipFitInCurrentWindow').returns(false);

        //Act
        component.tooltipHandler();

        //Assert
        assert.equal(false, component.$element.hasClass('bottom'));
        assert.equal(false, component.$element.hasClass('reverse'));
    });

    it('should reverse if it does not fit', function() {
        //Prepare
        sandbox.stub($.prototype,'outerWidth').returns(600);
        sandbox.stub(component,'getTooltipAbsolutePosition').returns({
            bottom: -1,
            top: -1,
            left: -1,
            right: -1
        });

        //Act
        component.tooltipHandler();

        //Assert
        assert.equal(true, component.$element.hasClass('bottom'));
        assert.equal(true, component.$element.hasClass('reverse'));
    });

    it('should NOT reverse if it fits', function() {
        //Prepare
        sandbox.stub($.prototype,'outerWidth').returns(600);
        sandbox.stub(component,'getTooltipAbsolutePosition').returns({
            top: 1,
            left: 1,
            right: 1,
            bottom: 1
        });

        //Act
        component.tooltipHandler();

        //Assert
        assert.equal(false, component.$element.hasClass('bottom'));
        assert.equal(false, component.$element.hasClass('reverse'));
    });
});

function createFixture() {
    return $(`    
    <span class="tooltip is-expanded" data-tooltip data-expandible data-expandible-on-blur="close" title="This is a tooltip">
        <span data-expandible-handle><span class='icon icon-info-blue'></span></span>
    </span>
    `);
}
