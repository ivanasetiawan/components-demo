# Tooltip component

Tooltip component. It shows a tooltip on click. 

## Features

- When the tooltip is hovered, it shows it (default behaviour for title). Hover tooltip works without javascript.
- The tooltip is shown on click (and hover functionality is disabled)
- The tooltip will do its best to fit inside the content of the page. 
   - If it does not fit in the top it opens in the bottom.
   - If it does not fit to the right it opens to the left.
- By default, the tooltip tries to expand  top-right
   - For top-left add class `reverse` after `class="tooltip"` 
   - For bottom-right add class `bottom` after `class="tooltip"` 
   - For top-left add class `bottom reverse` after `class="tooltip"` 
   
## Usage

    <span class="tooltip <customTag>"
          data-tooltip
          data-expandible
          data-expandible-on-blur="close"
          title="<tooltipMessage>">          
        
        <span data-expandible-handle>              
            <trigger>
        </span>
        
    </span>
  
Where:
`<customTag>` can be `bottom`, `reverse`, or `bottom reverse`.
`<tooltipMessage>` is the actual tooltip content.
`<trigger>` should be the blue info icon `<span class='icon icon-info-blue'></span>`, but it can be overwritten.

## Examples

You can see all functionality in the tests or in the html file.

## CSS tweaks

In order to achieve perfect alignment of the tooltip and the trigger you might have to
add a custom css for horizontal alignment in your component using:

            //Adjust tooltip
            .tooltip.is-expanded {
                //Align tip
                &:before {
                    right: 32px;
                }
                //Align body
                &:after {
                    right: 0;
                }
            }
