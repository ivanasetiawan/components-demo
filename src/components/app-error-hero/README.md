# App Error Hero component

## Functionality

Provides a hero image that is the backdrop for an error message (icon, title and description) to explain the user what went wrong.

## Usage

Use this component as the first component inside the container on error pages like 404 or 500. See views for examples.
