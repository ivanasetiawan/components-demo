# Nieuwe Huis Page Checklist component

## Functionality

A checklist component to help new house owners on mijn {client}.

## Usage

Include this component by adding `{% include "components/nieuwe-huis-page-checklist/nieuwe-huis-page-checklist.html" %}`.

This component also uses stickyNavigation macro (from "components/sticky-navigation/sticky-navigation.html")
