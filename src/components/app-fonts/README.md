# App fonts component

## Functionality

Includes custom font "Proxima Nova" in the site.
Uses lazy load on first visit to prevent flash of invisible text (FOIT).

## Usage

If a cookie with `fonts-loaded` is present:

* Add `fonts-loaded` class to the document's `<html>` element.

If the cookie is not present, include the `app-fonts.html` partial:

* Include the `fontobserver` and `font-loader` scripts.
* Call `fontLoader()` with the custom font settings. (see `app-fonts.html`)

## Background info

* http://publishing-project.rivendellweb.net/loading-fonts/
* http://www.filamentgroup.com/lab/font-events.html
* http://github.com/bramstein/fontfaceobserver
