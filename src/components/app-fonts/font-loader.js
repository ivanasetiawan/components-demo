/*
 Fonts are loaded through @font-face rules in the CSS whenever an element references them.
 FontFaceObserver creates a referencing element to trigger the font request, and listen for font load events.
 When all fonts are loaded, we enable them by adding a class to the html element
 */
(function(window, document, FontFaceObserver) {
    'use strict';

    window.fontLoader = fontLoader;

    function fontLoader(config) {

        if (!window.Promise) {
            document.documentElement.className += ' ' + config.loadedClass;
            return;
        }

        // if the class is already set, we're good.
        if (document.documentElement.className.indexOf(config.loadedClass + ' ') > -1) {
            return;
        }

        window.setCookie = function(name, value, days) {
            if (typeof useCookie === 'undefined') {
                // if value is a false boolean, we'll treat that as a delete
                if (value === false) {
                    days = -1;
                }

                var expires;
                if (days) {
                    var date = new Date();
                    date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                    expires = '; expires=' + date.toGMTString();
                } else {
                    expires = '';
                }

                document.cookie = name + '=' + value + expires + '; path=/';
            }
        };

        window.Promise
            .all(config.fonts
                .map(function(font) {
                    return new FontFaceObserver(font.name, font.variant);
                })
                .map(function(observer) {
                    return observer.check();
                })
            )
            .then(function() {
                document.documentElement.className += ' ' + config.loadedClass;
                window.setCookie(config.loadedClass, true, 5 * 365);
            },

            function() {
                console.error('err while observing font');
            }
        );
    }

}(window, window.document, window.FontFaceObserver));
