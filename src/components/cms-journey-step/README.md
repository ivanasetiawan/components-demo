# Cms Journey Step component

## Functionality

To show the Journey steps (left & right alignment) on CMS pages.

## Usage

Include in Umbraco razor template.
Frontend: Please take a good look at the documentation on journey-path partial.