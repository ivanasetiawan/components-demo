// dependencies listed here are bundled separately
module.exports = [
    'jquery',
    'jquery-validation-unobtrusive',
    '{client}-slick-carousel',
    '{client}-quadia',
    'mousetrap',
    'shake.js'
];
