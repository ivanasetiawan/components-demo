const Server = require('karma').Server;

function runTest(done) {
    return new Server(
        {
            configFile: __dirname + '/karma.conf.js',
        }, done)
        .start();
}

function watchTest(done) {
    return new Server(
        {
            configFile: __dirname + '/karma.conf.js',
            singleRun: false,
            autoWatch: true
        }, done)
        .start();
}

module.exports = {
    runTest,
    watchTest
};
