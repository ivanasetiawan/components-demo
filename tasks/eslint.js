const cached = require('gulp-cached');
const eslint = require('gulp-eslint');
const filter = require('gulp-filter');
const gulp = require('gulp');
const gulpIf = require('gulp-if');

module.exports = function () {
    return gulp.src(global.paths.jsFiles, {base: global.paths.src})
        .pipe(filter(['**/*', '!**/*.test.js', '!**/vendor.js']))
        .pipe(cached('eslint')) // filter down to changed files only
        .pipe(eslint())
        .pipe(eslint.format())
        .pipe(gulpIf(!global.isWatching, eslint.failAfterError()))
        .pipe(gulp.dest(global.paths.distAssets));
};
