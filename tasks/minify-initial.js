const gulp = require('gulp');
const rename = require('gulp-rename');
const uglify = require('gulp-uglify');

module.exports = function () {
    return gulp.src(['src/components/_initial/_initial.js'])
        .pipe(uglify())
        .pipe(rename('_initial.min.js'))
        .pipe(gulp.dest('src/components/_initial/'));
};
