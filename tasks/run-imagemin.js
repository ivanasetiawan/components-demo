const gulp = require('gulp');
const imagemin = require('gulp-imagemin');
const imageminPngquant = require('imagemin-pngquant');
const newer = require('gulp-newer');
const rename = require('gulp-rename');

/**
 * All images placed in a `assets-raw/images/` folder will be optimised (if newer) and placed in `assets/images/`.
 * Examples:
 *  src/assets-raw/images/img.png                           >> src/assets/images/img.png
 *  src/components/my-component/assets-raw/images/img.png   >> src/components/my-component/assets/images/img.png
 *  src/views/my-view/assets-raw/images/img.png             >> src/views/my-view/assets/images/img.png
 *
 * If you don't want an image to be processed place it directly into `assets/images` instead of `assets-raw/images`.
 */
module.exports = function() {
    const relRawImageDirPath = global.paths.rawImagesPath;
    const relRawImageFilePath = relRawImageDirPath + '/**/*.{gif,jpg,jpeg,png,svg}';

    return gulp.src([
        global.paths.src + relRawImageFilePath,
        global.paths.srcComponents + '*/' + relRawImageFilePath,
        global.paths.srcViews + '*/' + relRawImageFilePath
    ], {base: global.paths.src})
    // only process image if the raw image is newer (need to check against renamed output file)
        .pipe(newer({
            dest: global.paths.src,
            map: (relativePath) => renameImageDir(relativePath)
        }))
        .pipe(imagemin([
            imagemin.jpegtran({progressive: true}),
            imagemin.svgo({plugins: []}),
            imageminPngquant()
        ]))
        .pipe(rename(function(path) {
            path.dirname = renameImageDir(path.dirname);
        }))
        .pipe(gulp.dest(global.paths.src));

    function renameImageDir(path) {
        return path.replace(relRawImageDirPath, 'assets/images');
    }
};
