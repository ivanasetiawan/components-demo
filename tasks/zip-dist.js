const gulp = require('gulp');
const zip = require('gulp-zip');

const pkg = require('../package.json');

/**
 *  Zips up the dist dir according to package name
 */
module.exports = function() {
    return gulp.src(global.paths.dist + '**/*')
        .pipe(zip(pkg.name + '.zip'))
        .pipe(gulp.dest(global.paths.dist));
};
