const browserSync = require('browser-sync').create();
const fs = require('fs');
const url = require('url');

/**
 * Start Browsersync server
 * @returns {*}
 */
module.exports = function() {
    browserSync.init({
        files: global.config.browserSyncFiles,
        online: false,
        open: false,
        server: {
            baseDir: global.paths.dist,
            middleware: [
                function(request, response, next) {
                    request.method = 'GET'; // force GET as Browsersync doesn't know what to do with POST, PUT, ...
                    response.setHeader('Access-Control-Allow-Origin', '*');
                    next();
                },
                {
                    route: "/dialog",
                    handle: dialogRouteHandler
                }
            ]
        },
        ui: false
    });
};

/**
 * Middleware handler designed to not care too much about the dialog component.
 * @param {Object} req - Request
 * @param {Object} res - Response
 * @param {function} next - Callback
 */
function dialogRouteHandler(req, res, next) {
    const queryData = url.parse(req.url, true).query;
    if('view' in queryData) {
        const reqFile = 'dist/components/' + queryData['view'];
        fs.exists(reqFile, function(exists) {
            if (exists) {
                const fileContent = fs.readFileSync(reqFile, 'utf-8');
                res.writeHead(200, {'Content-Type': 'application/json'});
                res.write(JSON.stringify({
                    Result: 'OK',
                    View: fileContent
                }));
                res.end();
            } else {
                next();
            }
        });
    }
}
