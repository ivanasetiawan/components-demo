const concat = require('gulp-concat');
const filter = require('gulp-filter');
const gulp = require('gulp');
const newer = require('gulp-newer');
const path = require('path');
const rename = require('gulp-rename');

module.exports = {
    assets: copyAssets,
    stubs: copyStubs,
    floorplanner: copyFloorplanner,
    highcharts: copyHighcharts,
    krpano: copyKrpano,
    otherFiles: copyOtherFiles
};

/**
 * Copy all files from `assets/` directories in source root & modules. Only copies file when newer.
 * The `assets/` string is removed from the original path as the destination is an `assets/` dir itself.
 */
function copyAssets() {
    global.paths.assetFiles.map(function(filePath) {
        return gulp.src(filePath, {base: global.paths.src})
            .pipe(filter(['**/*', '!**/*.less']))
            .pipe(newer(global.paths.distAssets))
            .pipe(rename(function(p) {
                p.dirname = p.dirname
                    .split(path.sep)
                    .filter(function(dir) {
                        return (dir !== 'assets');
                    })
                    .join(path.sep);
            }))
            .pipe(gulp.dest(global.paths.distAssets));
    });
}

/**
 * Copy all files from `stubs/` directories in source root & modules. Only copies file when newer.
 * The `stubs/` string is removed from the original path as the destination is a `stubs/` dir itself.
 */
function copyStubs() {
    global.paths.stubFiles.map(function(filePath) {
        return gulp.src(filePath, {base: global.paths.src})
            .pipe(newer(global.paths.distStubs))
            .pipe(rename(function(p) {
                p.dirname = p.dirname
                    .split(path.sep)
                    .filter(function(dir) {
                        return (dir !== 'stubs');
                    })
                    .join(path.sep);
            }))
            .pipe(gulp.dest(global.paths.distStubs));
    });
}

/**
 * Gets Floorplanner file + pixi.js dependency, concatenates them and moves the result to the assets directory.
 */
function copyFloorplanner() {
    return gulp.src([
        './node_modules/{Client}-floorplanner/pixi.min.js',
        './node_modules/{Client}-floorplanner/fp.viewer.min.js'
    ])
        .pipe(concat('vendor-floorplanner.js'))
        .pipe(gulp.dest(global.paths.distAssets));
}

/**
 * Gets Highcharts files, concatenates them and moves the file to the assets directory.
 */
function copyHighcharts() {
    return gulp.src([
        './node_modules/highcharts/highcharts.js',
        './node_modules/highcharts/highcharts-more.js'
    ])
        .pipe(concat('vendor-highcharts.js'))
        .pipe(gulp.dest(global.paths.distAssets));
}

function copyKrpano() {
    return gulp.src([
        './src/vendor/krpano.js'
    ])
        .pipe(rename('vendor-krpano.js'))
        .pipe(gulp.dest(global.paths.distAssets));
}

/**
 * Copy miscellaneous files
 */
function copyOtherFiles() {
    global.config.filesToCopy.map(function(file) {
        return gulp.src(file.filepath)
            .pipe(gulp.dest(file.toDir));
    });
}
