const browserify = require('browserify');
const buffer = require('vinyl-buffer');
const gulp = require('gulp');
const gulpIf = require('gulp-if');
const gutil = require('gulp-util');
const notify = require('gulp-notify');
const source = require('vinyl-source-stream');
const sourcemaps = require('gulp-sourcemaps');
const uglify = require('gulp-uglify');
const watchify = require('watchify');

/**
 * Compile & uglify main index JS and add external source map.
 * @returns {*}
 */
function buildJsApp() {
    const bundler = appBundler();
    return writeBundle(bundler, 'index.js');
}

/**
 * Compile & uglify vendor JS and add external source map.
 * @returns {*}
 */
function buildJsVendor() {
    const bundler = browserify();
    // add all modules listed in vendor.js to bundle
    bundler.require(require('../src/vendor.js'));
    return writeBundle(bundler, 'vendor.js');
}

function watchJs() {
    const bundler = watchify(appBundler());
    bundler.on('update', () => writeBundle(bundler, 'index.js', false)); // on any dep update, rebundle
    bundler.on('log', gutil.log); // output build logs to terminal
    return writeBundle(bundler, 'index.js', false)
}

/**
 * Configure and return browserify instance for app's index.
 * @returns {Object} browserify instance configured for app's index
 */
function appBundler() {
    const bundler = browserify({
        entries: global.paths.src + 'index.js',
        debug: true // sourcemaps
    });

    bundler.transform('babelify');
    // exclude all modules listed in vendor.js
    bundler.external(require('../src/vendor.js'));

    return bundler;
}

/**
 *
 * @param {Object} bundler - a browserify instance
 * @param outputFile
 * @param compress
 * @returns {*}
 */
function writeBundle (bundler, outputFile, compress) {
    compress = typeof compress !== 'undefined' ? compress : true;

    return bundler
        .bundle()
        .on('error', gulpIf(global.isWatching,
            // force relative paths for shorter messages
            notify.onError((err) => err.message.replace(__dirname, '')),
            gutil.log.bind(gutil, 'Browserify Error')
        ))
        .pipe(source(outputFile))
        .pipe(buffer())
        .pipe(sourcemaps.init({ loadMaps: true }))
        // Add transformation tasks to the pipeline here.
        .pipe(gulpIf(!global.isWatching, uglify({ compress })))
        .on('error', gutil.log)
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(global.paths.distAssets));
}

module.exports = {
    writeBundle,
    buildJsApp,
    buildJsVendor,
    watchJs
};
