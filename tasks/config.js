const config = {};

const paths = {
    src: 'src/',
    srcComponents: 'src/components/',
    srcVendor: 'src/vendor/',
    srcViews: 'src/views/',
    dist: 'dist/',
    distAssets: 'dist/assets/',
    distComponents: 'dist/components/',
    distStubs: 'dist/stubs/',
    distViews: 'dist/views/',
    karmaConfig: './test/karma.conf.js',
    rawImagesPath: 'assets-raw/images'
};

paths.assetFiles = [
    paths.src + 'assets/**/*.*',
    paths.srcComponents + '*/assets/**/*.*',
    paths.srcViews + '*/assets/**/*.*'
];

paths.stubFiles = [
    paths.srcComponents + '*/stubs/**/*.*',
    paths.srcViews + '*/stubs/**/*.*'
];

// source files are all files directly in module sub directories and core files,
// excluding abstract files/dirs starting with '_'.
paths.srcFiles = [
    paths.src + '*',
    paths.srcComponents + '*/*',
    paths.srcViews + '*/*',
    '!' + paths.src + '*/_template/*'
];

paths.htmlFiles = paths.srcFiles.map(function (path) {
    return path + '.html';
});

paths.jsFiles = paths.srcFiles.map(function (path) {
    return path + '.js';
});

paths.scssFiles = paths.srcFiles.map(function(path) {
    return path + '*/*.scss';
});

config.paths = paths;

config.browserSync = {
    server: {
        baseDir: paths.dist
    },
    ui: false,
    online: false,
    open: false
};

config.browserSyncFiles = [
    'dist/assets/index-clientwonen.css',
    'dist/assets/index-clientinbusiness.css',
    'dist/**/*.html',
    'dist/assets/*.js'
];

config.fileWatchInterval = 500;

// files that need to be copied individually
config.filesToCopy = [
    {
        filepath: 'src/.htaccess',
        toDir: paths.dist
    },
    {
        filepath: 'web.config',
        toDir: paths.dist
    }
];

config.autoprefixerConfig = {
    browsers: [
        '> 1% in NL',
        'last 2 versions',
        'Android 4',// linear gradient
        'ie 9',     // transform2D
        'iOS 8',    // transform2D
        'Safari 8'  // transform2D
    ]
};

config.pixBase = '16px';

module.exports = config;
