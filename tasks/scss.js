const autoprefixer = require('autoprefixer');
const bless = require('gulp-bless');
const cleanCss = require('gulp-clean-css');
const filter = require('gulp-filter');
const gulp = require('gulp');
const gulpIf = require('gulp-if');
const notify = require('gulp-notify');
const pixrem = require('pixrem');
const plumber = require('gulp-plumber');
const postcss = require('gulp-postcss');
const rename = require('gulp-rename');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');

/**
 * Compile & compress SCSS to CSS and add source map.
 * Split CSS into multiple files to address 4096 selectors limitation in <=IE9
 * @returns {function} -
 */
module.exports = function(scssFileName) {
    const filename = scssFileName + '.scss';
    const blessFileName = scssFileName + '.bless.css';

    return function() {
        return gulp.src(global.paths.src + filename)
            .pipe(gulpIf(global.isWatching, plumber({
                errorHandler: notify.onError((err) => {
                    return err.message
                        .replace(/.*(\/.*\.scss)/, '.. $1')
                        .replace('\n', ': ');
                })
            })))
            .pipe(sourcemaps.init())
            .pipe(gulpIf(global.isWatching,
                sass(), // if watching do uncompressed
                sass({  // else
                    outputStyle: 'compressed'
                })
            ))
            .pipe(postcss([
                pixrem(global.config.pixBase),
                autoprefixer(global.config.autoprefixerConfig)
            ]))
            .pipe(sourcemaps.write('./'))
            .pipe(gulp.dest(global.paths.distAssets))
            .pipe(filter(['**/*.css']))
            .pipe(rename(blessFileName))
            .pipe(gulpIf(!global.isWatching, bless({
                cacheBuster: false
            })))
            .pipe(gulpIf(!global.isWatching, cleanCss({
                inline: ['none']
            })))
            .pipe(gulp.dest(global.paths.distAssets));
    }
};
