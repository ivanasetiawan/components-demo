// Copied from client-front-end-guide, only the module/view create flow is left

var assign = require('lodash/assign');
var chalk = require('chalk');
var changeCase = require('change-case');
var fs = require('fs');
var gulp = require('gulp');
var inquirer = require('inquirer');
var merge = require('lodash/merge');
var path = require('path');
var Q = require('q');
var rename = require('gulp-rename');
var replace = require('gulp-replace');

//questions for create module task
var questions = function () {
    var moduleType, modulePath;
    return [{
        type: 'list',
        name: 'moduleType',
        message: 'What kind of module would you like to create?',
        choices: [{
            name: 'component',
            value: 'components'
        }, {
            name: 'view',
            value: 'views'
        }]
    }, {
        type: 'input',
        name: 'moduleName',
        message: 'Give the new module a name',
        validate: function validateModuleName(moduleName) {
            var validName = /^[a-z][a-z0-9-_]+$/.test(moduleName);
            modulePath = global.paths.src + moduleType + 's/' + moduleName;
            var validPath = !fs.existsSync(path.normalize(modulePath));
            if (!validName) {
                return ['bad', moduleType, 'name'].join(' ');
            } else if (!validPath) {
                return ['the', moduleType, 'already exists'].join(' ');
            }
            return true;
        }
    }, {
        type: 'checkbox',
        name: 'files',
        message: 'Which types of files do you want to include? Press enter when ready.',
        choices: [
            {name: 'HTML', value: 'html', checked: true},
            {name: 'SCSS/CSS', value: 'scss', checked: true},
            {name: 'JavaScript', value: 'js', checked: false},
            {name: 'README', value: 'md', checked: true}
        ],
        validate: function (input) {
            return (input.length) ? true : 'You must select at least one type of file';
        }
    }];
};
/**
 *
 * @param {Array} questions - objects containing questions to ask in prompt
 * @param {...object} questionOverride - overrides questions in questions array
 */
function inquire(questions) {
    var deferred = Q.defer(),
        overrides;
    if (!questions || !questions.length) {
        deferred.reject('no questions asked');
    }
    overrides = [].slice.call(arguments, 1);
    questions = !overrides.length ? questions : skipQuestions(questions, overrides.map(function (override) {
        return override.name;
    }));
    inquirer.prompt(questions, function promptCallback(answers) {
        deferred.resolve(replaceAnswers.apply(null, [answers].concat(overrides)));
    });
    return deferred.promise;
}
/**
 * removes questions
 * @param {Array} questions - original array of questions
 * @param {...string|Array} names properties of questions to remove from questions array
 */
function skipQuestions(questions) {
    var filters,
        rest = [].slice.call(arguments, 1),
        firstArg = rest.slice().shift();

    filters = Array.isArray(firstArg) ? firstArg : rest;
    return questions.filter(function (question) {
        return filters.indexOf(question.name) < 0;
    });
}
/**
 * replaces answers in given object with replacements
 * @param {object} answers
 * @param {...object} replacements
 *
 */
function replaceAnswers(answers) {
    if (Array.isArray(answers) || typeof answers !== 'object') {
        throw new Error('`answers` argument should be an Object');
    }
    var replacements,
        rest = [].slice.call(arguments, 1),
        firstArg = rest.slice().shift();

    replacements = Array.isArray(firstArg) ? firstArg : rest;
    if (!replacements.length) {
        return answers;
    }
    return merge(answers, replacements.reduce(function (prev, curr) {
        var next = {};
        var nextName = curr.name;
        if (nextName) {
            next[nextName] = curr.value;
        }
        return assign(prev, next);
    }, {}));
}

/**
 * checks if then given module exists and returns its path and an array of file names it contains if
 * it does exists, false if it doesn't
 * @param {string} path
 * @returns {object|boolean}
 */
function getModuleStatus(path) {
    return Q.nfcall(fs.readdir, path).then(function (result) {
        return {
            dir: path,
            files: result
        };
    }, function () {
        return false;
    });
}

/**
 * returns the file types that do not exist in a module
 *
 * @param {Array} fileTypes - files the user requested
 * @param {Array} fileNames - base paths that already exist in the module
 * @returns {Array} - file extensions the user requested which were not found in the base paths
 */
function getMissingFileTypes(fileTypes, fileNames) {
    if (!Array.isArray(fileTypes)) {
        throw new Error('`fileTypes` argument should be an Array');
    } else if (!Array.isArray(fileNames)) {
        throw new Error('`moduleStatus` argument should be an Object');
    }
    return addTestFile(fileTypes).filter(function (type) {
        return fileNames.map(function (file) {
                var extension;
                if (!/test/g.test(file)) {
                    extension = path.extname(file);
                } else {
                    extension = '.test.js';
                }
                return extension.substr(1);
            }).indexOf(type) < 0;
    });
}
/**
 * glob search skips '.test.js', so insert the js test file extension here if a js file is found.
 * @param {Array} fileTypes
 * @returns {Array}
 */
function addTestFile(fileTypes) {
    var adjustedTypes;
    if (fileTypes.indexOf('js') >= 0) {
        adjustedTypes = fileTypes.slice();
        adjustedTypes.push('test.js');
        return adjustedTypes;
    }
    return fileTypes;
}
/**
 * returns a module's normalized absolute path
 *
 * @param {String} moduleName
 * @param {String} moduleType
 * @returns {String}
 */
function resolveModuleDir(moduleType, moduleName) {
    return path.resolve(
        path.join(global.paths.src, moduleType, moduleName)
    );
}
/**
 * create a directory at given path
 * @param {string} path
 * @returns {object} a promise representing the result of directory creation
 */
function createModuleDir(path) {
    return Q.nfcall(fs.exists, path).then(function () {
        return Q.nfcall(fs.mkdir, path);
    }, function (err) {
        throw new Error('directory already exists!', err);
    });
}
/**
 *
 * @param {object} module - information about the module to create
 * @returns {*}
 */
function copyTemplateFiles(module) {
    //remove the 's' for a friendly type description in the output files
    var basePath = path.join(global.paths.src, module.type, '_template/*.');
    return gulp.src(module.files.map(function (type) {
        return basePath + type;
    }))
        .pipe(replace(/MODULE_NAME_PASCAL/g, changeCase.pascalCase(module.name)))
        .pipe(replace(/MODULE_NAME_CAMEL/g, changeCase.camelCase(module.name)))
        .pipe(replace(/MODULE_NAME_TITLE/g, changeCase.titleCase(module.name)))
        .pipe(replace(/MODULE_NAME/g, module.name))
        .pipe(replace(/data-MODULE-NAME/g, "data-" + module.name))
        .pipe(rename(function (p) {
            var isTest = /test$/.test(p.basename);
            if (p.basename !== 'README' && !isTest) {
                p.basename = module.name;
            }
            if (isTest) {
                p.basename = module.name;
                p.extname = '.test' + p.extname;
            }
        }))
        .pipe(gulp.dest(module.path));
}
/**
 * prompt the user for module creation.
 * @param {...object} object(s) representing questions to skip / answers to replace
 * todo: get args (use minimist)
 */
function createModule() {
    //ask what to create
    return inquire(questions()).then(function (answers) {
        var module = {
            name: answers.moduleName,
            type: answers.moduleType,
            files: answers.files,
            path: resolveModuleDir(answers.moduleType, answers.moduleName)
        };
        //check if the module already exists
        return getModuleStatus(module.path).then(function (status) {
            //the module already exists, create files that don't exist yet
            if (status) {
                module.files = getMissingFileTypes(module.files, status.files);
                return module;
            }
            //the module does not exist yet: create a directory for it
            return createModuleDir(module.path).then(function () {
                return module;
            });
        }).then(function (module) {
            //copy files from template directory to new module
            copyTemplateFiles(module);
            return module;
        });
    }).then(function (module) {
        console.log(chalk.bold.green('Successfully created module', module.name));
    }, function (error) {
        console.log(chalk.bold.red(error));
    });
}

function getModuleIndex() {
    return {
        components: listDirectories(global.paths.srcComponents).map(function (name) {
            var componentBasePath = 'components/' + name + '/' + name;
            var obj = {
                id: 'components/' + name,
                name: name,
                path: componentBasePath + '-preview.html',
                type: 'component'
            };
            fs.stat(path.resolve('src/' + componentBasePath + '-demo.html'), function(err, stats) {
                obj.hasDemo = !err && stats.isFile();
            });
            return obj;
        }),
        views: listDirectories(global.paths.srcViews).map(function (name) {
            return {
                id: 'views/' + name,
                name: name,
                path: 'views/' + name + '/' + name + '.html',
                type: 'view'
            };
        })
    };
}

function listDirectories(cwd) {
    return fs.readdirSync(cwd)
        .filter(function (file) {
            return fs.statSync(cwd + file).isDirectory();
        });
}

module.exports = {
    createModule: createModule,
    getModuleIndex: getModuleIndex
};
