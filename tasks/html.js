'use strict'; //todo: remove once build servers run Node>=6

const browserSync = require('browser-sync');
const extend = require('lodash/extend');
const filter = require('gulp-filter');
const fs = require('fs');
const gulp = require('gulp');
const gulpIf = require('gulp-if');
const path = require('path');
const plumber = require('gulp-plumber');
const rename = require('gulp-rename');

const moduleTasks = require('./module');
const nunjucksUtility = require('./nunjucks-utility');
const pkg = require('../package.json');

function buildHtml() {
    nunjucksUtility.configure();
    const moduleIndex = moduleTasks.getModuleIndex();
    return srcFiles('html')
        .pipe(plumber()) // prevent pipe break on nunjucks render error
        .pipe(nunjucksUtility.render(function(file) {
            return extend(
                htmlModuleData(file),
                {moduleIndex: moduleIndex}
            );
        }))
        .pipe(plumber.stop())
        .pipe(gulp.dest(global.paths.dist))
        // trigger a Browsersync reload if Browsersync is active
        .pipe(gulpIf(browserSync.active, browserSync.reload({stream: true})));
}

function srcFiles(filetype) {
    return gulp.src(global.paths.srcFiles, {base: global.paths.src})
        .pipe(filter('**/*.' + filetype));
}

function htmlModuleData(file) {
    let pathToRoot = path.relative(file.relative, '.');
    pathToRoot = pathToRoot.substring(0, pathToRoot.length - 2)
        .split(path.sep).join('/'); // URLs expect forward slashes

    // get critical css file and put in on module data to
    const pathObj = parsePath(file.relative);
    const criticalFile = global.paths.distAssets + pathObj.dirname + '/' + pathObj.basename + '-critical.css';
    let criticalCss = '';

    if (fs.existsSync(criticalFile)) {
        criticalCss = fs.readFileSync(criticalFile, 'utf8');
    }

    return {
        module: {
            id: path.dirname(file.relative),
            name: parsePath(file.relative).basename,
            html: file.contents.toString()
        },
        paths: {
            assets: pathToRoot + 'assets/',
            root: pathToRoot,
            stubs: pathToRoot + 'stubs/'
        },
        criticalCss: criticalCss,
        pkg: pkg
    };
}

function parsePath(filepath) {
    const extname = path.extname(filepath);
    return {
        dirname: path.dirname(filepath),
        basename: path.basename(filepath, extname),
        extname: extname
    };
}

function buildPreviews() {
    nunjucksUtility.configure();
    const templateHtml = fs.readFileSync(global.paths.srcViews + '_component-preview/component-preview.html', 'utf8');
    return gulp.src(global.paths.srcComponents + '*/*.html', {base: global.paths.src})
        .pipe(plumber()) // prevent pipe break on nunjucks render error
        .pipe(nunjucksUtility.render(htmlModuleData))
        .pipe(nunjucksUtility.render(htmlModuleData, templateHtml))
        .pipe(plumber.stop())
        .pipe(rename(function(p) {
            p.basename += '-preview';
        }))
        .pipe(gulp.dest(global.paths.dist));
}

module.exports = {
    buildHtml,
    buildPreviews
};
