const gutil = require('gulp-util');
const through = require('through2');
const nunjucks = require('nunjucks');

/**
 * @param {Function} [fn]   Function called for each file. Should return an options object, which is used as context
 *                          in renderString (http://mozilla.github.io/nunjucks/api.html#renderstring1).
 * @param {String} [sharedTemplateHtml] Used as string inside renderString instead of the contents of the current file.
 *                                      This allows you to use the file contents inside another (shared) template.
 * @returns {DestroyableTransform}  Stream object
 *
 *  Forked version of gulp-nunjucks render (https://github.com/carlosl/gulp-nunjucks-render).
 *  This module expects a function which is called for each file to be rendered, rather than a static options object.
 *  This module also allows you to render the content inside another template as the content is available in the first
 *  function using file.contents.toString() which you can put in the options for the shared template however you like.
 */
function render(fn, sharedTemplateHtml) {
    const defaults = {};

    return through.obj(function (file, enc, cb) {
        if (file.isNull()) {
            this.push(file);
            return cb();
        }

        if (file.isStream()) {
            this.emit('error', new gutil.PluginError('nunjucks-render', 'Streaming not supported'));
            return cb();
        }

        try {
            const options = fn ? fn(file) : defaults;
            const templateHtml = sharedTemplateHtml || file.contents.toString();
            file.contents = new Buffer(nunjucks.renderString(templateHtml, options));
            file.path = gutil.replaceExtension(file.path, '.html');

        } catch (err) {
            this.emit('error', new gutil.PluginError('nunjucks-render', err));
        }

        this.push(file);
        cb();
    });
}

/**
 * Match returns a filtered list, only containing those items that matched against the given pattern.
 *
 * To use this filter in a template, first register it in the Nunjucks environment:
 *   env.addFilter('match', match);
 *
 * @example <caption>Only items starting with lower case</caption>
 *   {% for item in ["alpha", "Beta", "charlie"] | match('^[a-z]') %}{{ item }}, {% endfor %}
 *   // outputs: alpha, charlie,
 *
 * @param {String[]} arr    Array of strings to match.
 * @param {String} pattern  Pattern to match array items against.
 * @param {String} [prop]   Property name (key) of array item to match against.
 * @returns {*}
 */
function match(arr, pattern, prop) {
    const regularExpression = new RegExp(pattern);
    return arr.filter(function (item) {
        if (prop) {
            return regularExpression.test(item[prop]);
        } else {
            return regularExpression.test(item);
        }
    });
}

function prettyJson(obj) {
    return JSON.stringify(obj, null, 4);
}

function configure() {
    const env = nunjucks.configure(global.paths.src, {watch: false});
    env.addFilter('match', match);
    env.addFilter('prettyJson', prettyJson);
}

module.exports = {
    nunjucks,
    render,
    match,
    prettyJson,
    configure
};
