const del = require('del');

/**
 * Deletes the dist directory synchronously
 * @return {Function}
 */
module.exports = function () {
    return del([global.paths.dist]);
};
